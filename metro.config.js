/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

 const path = require('path');

 module.exports = {
    projectRoot: path.join(__dirname, 'pkgs', 'mobile'),
    transformer: {
        getTransformOptions: async () => ({
            transform: {
                experimentalImportSupport: false,
                inlineRequires: true,
            },
        }),
    },
    resolver: {
        alias: {
            '@': path.resolve(__dirname, 'pkgs', 'mobile', 'src'),
        },
    },
    watchFolders: [
        path.resolve(__dirname, 'pkgs', 'core'),
        path.resolve(__dirname, 'pkgs', 'ui'),
        path.resolve(__dirname, 'node_modules'),
    ],
 };
 