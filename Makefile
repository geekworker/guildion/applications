include ./Makefile.variables

build-publish-app-dependency:
	$(eval SYSTEM_IMAGE_NAME := $(shell echo $(DOCKERHUB_PATH)/$(CI_DOCKER_IMAGE)))
	$(eval LATEST_SYSTEM_IMAGE := $(shell echo $(SYSTEM_IMAGE_NAME):latest))
	$(eval DOCKER_CACHE_OPTION := $(shell docker pull -q $(LATEST_DEPENDENCY_IMAGE_NAME) > /dev/null && echo "--cache-from $(LATEST_DEPENDENCY_IMAGE_NAME)" || echo ))

	DOCKER_BUILDKIT=1 docker build \
		$(shell echo $(DOCKER_CACHE_OPTION)) \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		--build-arg DEPENDENCY_IMAGE=$(LATEST_SYSTEM_IMAGE) \
		-t $(APP_DEPENDENCY_IMAGE_PATH):$(CI_COMMIT_SHA) \
		-t $(LATEST_DEPENDENCY_IMAGE_NAME) \
		.

	docker push $(LATEST_DEPENDENCY_IMAGE_NAME)
	docker push $(APP_DEPENDENCY_IMAGE_PATH):$(CI_COMMIT_SHA)

build-publish-ts-test-image:
	$(eval DOCKER_CACHE_OPTION := $(shell docker pull -q $(LATEST_TS_TEST_IMAGE_NAME) > /dev/null && echo "--cache-from $(LATEST_TS_TEST_IMAGE_NAME)" || echo ))

	DOCKER_BUILDKIT=1 docker build \
		$(shell echo $(DOCKER_CACHE_OPTION)) \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		-t $(TS_TEST_IMAGE_PATH):$(CI_COMMIT_SHA) \
		-t $(LATEST_TS_TEST_IMAGE_NAME) \
		-f Dockerfile.ts.test \
		.

build-publish-go-test-image:
	$(eval DOCKER_CACHE_OPTION := $(shell docker pull -q $(LATEST_GO_TEST_IMAGE_NAME) > /dev/null && echo "--cache-from $(LATEST_GO_TEST_IMAGE_NAME)" || echo ))

	DOCKER_BUILDKIT=1 docker build \
		$(shell echo $(DOCKER_CACHE_OPTION)) \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		-t $(GO_TEST_IMAGE_PATH):$(CI_COMMIT_SHA) \
		-t $(LATEST_GO_TEST_IMAGE_NAME) \
		-f Dockerfile.go.test \
		.