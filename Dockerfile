FROM alpine:3.13 as dependencies

WORKDIR /app
RUN apk add --no-cache nodejs yarn

COPY .yarn ./.yarn
COPY package.json yarn.lock tsconfig.json .yarnrc.yml metro.config.js .babelrc ./

COPY pkgs/abs/package.json ./pkgs/abs/package.json
COPY pkgs/api/package.json ./pkgs/api/package.json
COPY pkgs/blog/package.json ./pkgs/blog/package.json
COPY pkgs/connect ./pkgs/connect
COPY pkgs/sfu ./pkgs/sfu
COPY pkgs/core ./pkgs/core
COPY pkgs/db ./pkgs/db
COPY pkgs/data ./pkgs/data
COPY pkgs/mobile/package.json ./pkgs/mobile/package.json
COPY pkgs/next ./pkgs/next
COPY pkgs/node ./pkgs/node
COPY pkgs/ui ./pkgs/ui
COPY pkgs/web/package.json ./pkgs/web/package.json

RUN yarn
RUN yarn workspace @guildion/db checksum