#!/bin/bash

cd pkgs

for filename in */; do
    cd $filename
    exec "$@" & pid=$!
    wait pid
    cd ..
done