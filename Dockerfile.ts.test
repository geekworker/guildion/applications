FROM golang:1.14 AS go-test

WORKDIR /app/pkgs/sfu
COPY ./pkgs/sfu ./

WORKDIR /app/pkgs/connect
COPY ./pkgs/connect ./

RUN touch .env
WORKDIR /src/cmd/connect

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
  go build -a -installsuffix cgo -o guildion-connect \
  && mv ./guildion-connect /

FROM alpine:3.13 as dependencies

WORKDIR /app
RUN apk add --no-cache nodejs yarn

COPY . ./

RUN yarn

RUN yarn workspace @guildion/core build
RUN yarn workspace @guildion/db build
RUN yarn workspace @guildion/node build
RUN yarn workspace @guildion/next build
RUN yarn workspace @guildion/ui build
RUN yarn workspace @guildion/data build

RUN yarn workspace @guildion/db checksum

RUN yarn workspace @guildion/abs build

RUN if [ "${NODE_ENV}" = "staging" ]; then \
  yarn workspace @guildion/api build:staging; \
else \
  yarn workspace @guildion/api build:production; \
fi

RUN if [ "${NODE_ENV}" = "staging" ]; then \
  yarn workspace @guildion/web build:staging; \
else \
  yarn workspace @guildion/web build:production; \
fi

RUN if [ "${NODE_ENV}" = "staging" ]; then \
  yarn workspace @guildion/blog build:staging; \
else \
  yarn workspace @guildion/blog build:production; \
fi