import { CommandBase, CommandInterface } from "..";
import log4js from 'log4js';
import path from "path";
import fs from 'fs';
import { ContentType, FileExtension } from "@guildion/core";
import S3Adaptor from "@/modules/s3";
import Bluebird from "bluebird";

const logger = log4js.getLogger();
logger.level = 'all';

const bucketPrefix: string = 'abs/';

export interface AssetsFiles {
    files: AssetsFiles,
}

export default class ABSSyncAssetsCommand extends CommandBase implements CommandInterface {
    public entries: ContentType[] = [];
    private s3Adaptor: S3Adaptor;

    constructor(options?: { entries?: ContentType[] }) {
        super();
        this.entries = options?.entries || [];
        this.s3Adaptor = new S3Adaptor();
    }

    get entryExtensions(): string[] {
        return this.entries.map(entry => FileExtension(entry));
    }

    async run() {
        const paths: string[] = this.getAllFilePaths()
        logger.debug(`🌈 ALL ENTRIES ASSETS: [\n${paths.join("\n")}]`);
        await Bluebird.map(
            paths,
            (filepath) => this.findOrCreate(`${bucketPrefix}${filepath}`),
            { concurrency: 10 },
        )
        logger.debug(`🎊 ALL FILE UPLOAD SUCCESS`);
        const unusedKeys: string[] = await this.getUnusedFiles({ entries: paths });
        logger.debug(`🌈 ALL UNUSED ASSETS: [\n${unusedKeys.join("\n")}]`);
        await Bluebird.map(
            unusedKeys,
            (filepath) => this.delete(filepath),
            { concurrency: 10 },
        )
        logger.debug(`💯 ALL SYNC SUCCESS`);
    }

    async findOrCreate(provider_key: string): Promise<void> {
        const exists: boolean = await this.s3Adaptor.checkObjectExists(provider_key);
        if (!exists) {
            logger.debug(`🌏 FILE SYNC START: [${provider_key}]`);
            await this.s3Adaptor.putObjectFromURL(provider_key);
            logger.debug(`✅ FILE SYNC SUCCESS: [${provider_key}]`);
        }
    }

    async delete(provider_key: string): Promise<void> {
        logger.debug(`🌕 UNUSED FILE DELETE START: [${provider_key}]`);
        await this.s3Adaptor.deleteFileFromKey(provider_key);
        logger.debug(`✅ UNUSED FILE DELETE SUCCESS: [${provider_key}]`);
    }

    transformLocalFilePath(filepath: string) {
        return `file://${path.join(process.env.PWD!, 'assets', filepath)}`;
    }

    resolveAssetsPath(...rest: string[]) {
        return path.join(__dirname, '../../..', 'assets', ...rest);
    }

    getAllFilePaths(): string[] {
        const paths: string[] = this.readAssets();
        const filters: string[] = paths
            .filter((dirpath: string) => this.entryExtensions.filter(extension => dirpath.endsWith(`.${extension}`)).length > 0)
        return filters;
    }

    private readAssets(path?: string): string[] {
        const dirents: fs.Dirent[] = fs.readdirSync(this.resolveAssetsPath(path || ''), { withFileTypes: true });
        const files: string[] = dirents
            .filter((dirent: fs.Dirent) => dirent.isFile())
            .map((dirent: fs.Dirent) => path ? `${path}/${dirent.name}` : dirent.name);
        const dirs: string[][] = dirents
            .filter((dirent: fs.Dirent) => dirent.isDirectory())
            .map((dirent: fs.Dirent) => this.readAssets(path ? `${path}/${dirent.name}` : dirent.name))
        return [
            ...files,
            ...dirs.flat(),
        ];
    }

    async getUnusedFiles(options?: { entries?: string[] }): Promise<string[]> {
        const keys: string[] = await this.readS3Assets();
        const filters: string[] = keys
            .filter((key: string) => !(options?.entries || []).includes(key.replace(bucketPrefix, '')))
        return filters;
    }

    private async readS3Assets(path?: string): Promise<string[]> {
        const result: AWS.S3.Types.ListObjectsOutput = await this.s3Adaptor.getListObjects(path || bucketPrefix);
        const files: string[] = (result.Contents || [])
            .filter((object: AWS.S3.Types.Object) => !!object.Key && !object.Key.endsWith('/'))
            .map((object: AWS.S3.Types.Object) => object.Key)
            .filter((key?: string): key is string => !!key);
        const dirs: string[][] = await Bluebird.map(
            (result.Contents || [])
                .filter((object: AWS.S3.Types.Object) => !!object.Key && object.Key.endsWith('/'))
                .map((object: AWS.S3.Types.Object) => object.Key)
                .filter((key?: string): key is string => !!key),
            key => { return this.readS3Assets(key) },
            { concurrency: 10 }
        );
        return [
            ...files,
            ...dirs.flat(),
        ];
    }
}