import dotenv from 'dotenv';
import log4js from 'log4js';
import ABSSyncAssetsCommand from './commands/abs/syncAssets';
import { ContentType } from '@guildion/core';
import { configureAWS } from './modules/config';

const logger = log4js.getLogger();
logger.level = 'all';

dotenv.config();
configureAWS();

(async () => {
    const absSyncAssetsCommand = new ABSSyncAssetsCommand({
        entries: [
            ContentType.png,
            ContentType.jpg,
            ContentType.jpeg,
            ContentType.mov,
            ContentType.mp3,
            ContentType.mp4,
            ContentType.gif,
        ]
    });
    absSyncAssetsCommand.run();
})();