import AWS from 'aws-sdk';

export let s3: AWS.S3;
export let ec2: AWS.EC2;

export const configureAWS = () => {
    AWS.config.update({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        region: process.env.AWS_REGION,
        signatureVersion: 'v4',
        credentials: new AWS.Credentials(
            process.env.AWS_ACCESS_KEY_ID!,
            process.env.AWS_SECRET_ACCESS_KEY!
        ),
    });
    s3 = new AWS.S3({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        region: process.env.AWS_S3_REGION,
        signatureVersion: 'v4',
        credentials: new AWS.Credentials(
            process.env.AWS_ACCESS_KEY_ID!,
            process.env.AWS_SECRET_ACCESS_KEY!
        ),
    });
    ec2 = new AWS.EC2({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
        region: process.env.AWS_REGION,
        credentials: new AWS.Credentials(
            process.env.AWS_ACCESS_KEY_ID!,
            process.env.AWS_SECRET_ACCESS_KEY!
        ),
    });
}