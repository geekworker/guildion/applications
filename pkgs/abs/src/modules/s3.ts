import { safeURL } from '@guildion/core';
import AWS from 'aws-sdk';
import { s3 } from './config';
import { readFileSync } from 'fs';
import path from 'path';
export default class S3Adaptor {
    
    constructor() {
    }

    public guardFileKeyFromURL = (url: URL): string => {
        return this.guardFileKeyFromURLstring(url.toString());
    };

    public guardFileKeyFromURLstring = (urlstring: string): string => {
        const parser = safeURL(urlstring);
        if (!parser) return urlstring;
        return parser.pathname
            .replace(`/${process.env.AWS_S3_CDN_BUCKET}`, '')
            .replace(`/`, '');
    };

    async checkObjectExists(urlstring: string): Promise<boolean> {
        const key = this.guardFileKeyFromURLstring(urlstring);
        const params: AWS.S3.Types.GetObjectRequest = {
            Bucket: process.env.AWS_S3_CDN_BUCKET!,
            Key: key,
        };
        return new Promise((resolve, reject) => {
            s3.getObject(params, (err, result) => {
                if (err) resolve(false);
                resolve(true);
            });
        });
    };

    async getObject(urlstring: string): Promise<AWS.S3.Types.GetObjectOutput | undefined> {
        const key = this.guardFileKeyFromURLstring(urlstring);
        const params: AWS.S3.Types.GetObjectRequest = {
            Bucket: process.env.AWS_S3_CDN_BUCKET!,
            Key: key,
        };
        return new Promise((resolve, reject) => {
            s3.getObject(params, (err, result) => {
                if (err) resolve(undefined);
                resolve(result);
            });
        });
    };

    async putObjectFromURL(key: string): Promise<AWS.S3.Types.PutObjectOutput | undefined> {
        const params: AWS.S3.Types.PutObjectRequest = {
            Bucket: process.env.AWS_S3_CDN_BUCKET!,
            Key: key,
            Body: readFileSync(this.transformLocalFilePath(key)),
        };
        return new Promise((resolve, reject) => {
            s3.putObject(params, (err, result) => {
                if (err) resolve(undefined);
                resolve(result);
            });
        });
    };

    transformLocalFilePath(filepath: string) {
        return path.join(process.env.PWD!, 'assets', filepath.replace('abs/', ''));
    }

    async getListObjects(urlstring: string): Promise<AWS.S3.Types.ListObjectsOutput> {
        const path = this.guardFileKeyFromURLstring(urlstring);
        const params: AWS.S3.Types.ListObjectsRequest = {
            Bucket: process.env.AWS_S3_CDN_BUCKET!,
            Prefix: path,
        };
        return new Promise((resolve, reject) => {
            s3.listObjects(params, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        });
    };

    async getSignedURLForPut(key: string): Promise<string> {
        const params = {
            Bucket: process.env.AWS_S3_CDN_BUCKET!,
            Expires: 3600,
            Key: key,
        };
        const urlstring = s3.getSignedUrl('putObject', params);
        return urlstring;
    }

    async deleteFile(urlstring: string): Promise<AWS.S3.Types.DeleteObjectOutput | undefined> {
        return await this.deleteFileFromKey(this.guardFileKeyFromURLstring(urlstring));
    };

    async deleteFileFromKey(key: string): Promise<AWS.S3.Types.DeleteObjectOutput> {
        const params: AWS.S3.Types.DeleteObjectRequest = {
            Bucket: process.env.AWS_S3_CDN_BUCKET!,
            Key: key,
        };
        return new Promise((resolve, reject) => {
            s3.deleteObject(params, (err, result) => {
                if (err) reject(err)
                resolve(result);
            });
        });
    };

    async deleteDirectory(urlstring: string): Promise<AWS.S3.Types.DeleteObjectOutput[]> {
        const listOutput = await this.getListObjects(urlstring);
        const deleted = await Promise.all(
            (listOutput.Contents || [])
                .map(obj => obj.Key)
                .filter((key): key is string => !!key)
                .map(key => this.deleteFileFromKey(key))
        );
        return deleted;
    };
}