import { SingleBar } from "cli-progress";
import { Device, Endpoint, EndpointOfRequest, EndpointOfResponse, requestAPI } from "@guildion/core";
import { Scenario } from "@guildion/node/src/scenario/src/scenario";

export const APIScenario = <T extends Endpoint<any, any, any>>(endpoint: T) => 
    class extends Scenario<{
            data: ReturnType<EndpointOfRequest<T>["toJSON"]>,
        } & Partial<{
            device?: Device,
            accessToken?: string,
            csrfSecret?: string,
        }>,
        EndpointOfResponse<T>
    > {
        async run(
            props: {
                data: ReturnType<EndpointOfRequest<T>["toJSON"]>,
            } & Partial<{
                device?: Device,
                accessToken?: string,
                csrfSecret?: string,
            }>,
            { bar }: { bar: SingleBar }
        ): Promise<EndpointOfResponse<T>> {
                const result = await requestAPI(
                endpoint,
                {
                    device: props.device,
                    csrfSecret: props.csrfSecret,
                    accessToken: props.accessToken,
                    data: new endpoint.request({
                        ...props.data,
                    }),
                }
            );
            return result;
        }
    }