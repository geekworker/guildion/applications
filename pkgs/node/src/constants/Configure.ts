import { NODE_ENV } from "@guildion/core";

let LOCAL_IP_ADDRESS$: string = '';
let IS_MOBILE$: boolean = false;
let CURRENT_NODE_ENV$: NODE_ENV = NODE_ENV.DEVELOPMENT;
let JWT_SECRET$: string = 'example';

export function LOCAL_IP_ADDRESS(): string { return LOCAL_IP_ADDRESS$ };
export function IS_MOBILE(): boolean { return IS_MOBILE$ };
export function CURRENT_NODE_ENV(): NODE_ENV { return CURRENT_NODE_ENV$ };
export function JWT_SECRET(): string { return JWT_SECRET$ };

export function configure(values: { LOCAL_IP_ADDRESS?: string, IS_MOBILE?: boolean, CURRENT_NODE_ENV?: NODE_ENV, JWT_SECRET?: string }) {
    if (values.LOCAL_IP_ADDRESS) { LOCAL_IP_ADDRESS$ = values.LOCAL_IP_ADDRESS }
    if (values.IS_MOBILE) { IS_MOBILE$ = values.IS_MOBILE }
    if (values.CURRENT_NODE_ENV) { CURRENT_NODE_ENV$ = values.CURRENT_NODE_ENV }
    if (values.JWT_SECRET) { JWT_SECRET$ = values.JWT_SECRET }
}