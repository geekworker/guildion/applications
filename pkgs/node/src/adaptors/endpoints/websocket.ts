import WebSocket from 'websocket';
import { GET_CURRENT_WEBSOCKET_URL_STRING } from '@guildion/core';

export interface WebSocketClientDelegate {}

export class WebSocketClient {
    public delegates: WeakRef<WebSocketClientDelegate[]> = new WeakRef([]);
    private readonly client: WebSocket.client = new WebSocket.client();
    public connected: boolean = false;

    public inject(implementer: WebSocketClientDelegate) {
        this.delegates.deref()?.push(implementer);
    }

    public reject(implementer: WebSocketClientDelegate) {
        const index = this.delegates.deref()?.indexOf(implementer);
        if (index == -1 || !index) return;
        this.delegates.deref()?.splice(index, 1)
    }

    private invoke(method: (implementer: WebSocketClientDelegate) => void) {
        this.delegates.deref()?.forEach(delegate => {
            method(delegate);
        })
    }

    public connect() {
        if (this.connected) { return }
        this.client.connect(GET_CURRENT_WEBSOCKET_URL_STRING(), 'echo-protocol');
        this.connected = true;
    }

    public disconnect() {
        this.client.abort()
        this.connected = false;
    }
}