import chalk, { Color } from 'chalk';
import { HTTPStatusCode } from "@guildion/core";
import bytes from 'bytes';
import sizeof from 'object-sizeof';
import util from 'util';

const colorCodes: { [key: string]: typeof Color } = {
    '7': 'magenta',
    '5': 'red',
    '4': 'yellow',
    '3': 'cyan',
    '2': 'green',
    '1': 'green',
    '0': 'yellow'
};

export function NodeRequestLogger({ url, method, header, body }: { url: string, method: string, header: any, body: any }): number {
    const startTime = Date.now();
    const size = sizeof({ header, body });
    const length = size == null ? '-' : bytes(size).toLowerCase();
    console.log(
        '  ' +
            chalk.gray('<--') +
        ' ' +
            chalk.bold('%s') +
        ' ' +
            chalk.gray('%s') +
        ' ' +
            chalk.gray('%s'),
        method,
        url,
        length,
    );
    console.log('HEADER:', header);
    console.log('BODY:', body);
    return startTime;
}

export function NodeResponseLogger({ url, method, status, startMs, err, header, body }: { url: string, method: string, status: HTTPStatusCode, startMs: number, err?: Error, header: any, body: any }) {
    const upstream = err
        ? chalk.red('xxx')
        : chalk.gray('-->');

    const s = (status / 100) | 0;
    const color = colorCodes.hasOwnProperty(s) ? colorCodes[s]! : colorCodes[0];
    const size = sizeof({ header, body });
    const length = [204, 205, 304].includes(status)
        ? ''
        : size == null
        ? '-'
        : bytes(size).toLowerCase();

    console.log(
        '  ' +
            upstream +
            ' ' +
            chalk.bold('%s') +
            ' ' +
            chalk.gray('%s') +
            ' ' +
            chalk[color]('%s') +
            ' ' +
            chalk.gray('%s') +
            ' ' +
            chalk.gray('%s'),
        method,
        url,
        `${status}`,
        `${Date.now() - startMs}ms`,
        length
    );
    if (err) { 
        console.log(err.name);
        console.log(err.message);
        console.log(err.stack);
    } else {
        console.log('HEADER:', header);
        console.log('BODY:', util.inspect(body, {showHidden: false, depth: null, colors: true}));
    }
    
}