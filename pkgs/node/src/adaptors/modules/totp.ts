import totp from 'totp-generator';

export const DIGITS = 6;
export const ALGORITHM = 'SHA-512';
export const PERIOD_SC = 300;

function makeSalt(length: number) {
    var result           = [];
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
   }
   return result.join('');
}

export namespace TOTP {
    export type TOTPOptionType = {
        digits?: number,
        algorithm?: string,
        period?: number,
    }
    
    export function generateTokenFromSalt(salt: string, options: TOTPOptionType = {}) {
        return totp(salt, {
            digits: DIGITS,
            algorithm: ALGORITHM,
            period: PERIOD_SC,
            ...options,
        });
    }
    
    export function compare(token: string, salt: string, options: TOTPOptionType = {}): boolean {
        return token == `${generateTokenFromSalt(salt, options)}`;
    }
    
    export function generateToken(options: TOTPOptionType = {}) {
        const salt = makeSalt(32);
        return {
            token: `${generateTokenFromSalt(salt, options)}`,
            salt,
        };
    }
}
