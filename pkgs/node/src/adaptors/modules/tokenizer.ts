import * as jwt from 'jsonwebtoken';
import { JWT_SECRET } from '@guildion/node/src/constants/Configure';

export namespace Tokenizer {
    export const Action = {
        accessToken: 'accessToken',
    }
    export type Action = typeof Action[keyof typeof Action];

    export const sign = async (payload: { [key: string]: string | object }, action: Action, options?: jwt.SignOptions): Promise<string> => {
        payload.action = action;
        return await jwt.sign(payload, JWT_SECRET(), { ...options, algorithm: 'HS256' });
    }

    export const verify = async (token: string, action: Action): Promise<{ [key: string]: string | object }> => {
        return new Promise((resolve, reject) => {
            jwt.verify(token, JWT_SECRET(), (err: any, decoded: any) => {
                if (err || decoded.action != action) reject(err);
                resolve(decoded);
            })
        });
    }
}