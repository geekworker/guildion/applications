import axios, { AxiosRequestConfig } from "axios"
import { HTTPMethod, toAxiosMethod } from "@guildion/core";
import fs from 'fs';

export const uploadLocalFile = async ({
    localFilePath,
    presignedURL,
    onUploadProgress,
}: {
    localFilePath: string,
    presignedURL: string,
    onUploadProgress?: AxiosRequestConfig['onUploadProgress'],
}): Promise<boolean> => {
    const file = fs.readFileSync(localFilePath);
    const payload: AxiosRequestConfig = {
        url: presignedURL,
        data: file,
        onUploadProgress,
        method: toAxiosMethod(HTTPMethod.PUT),
    }
    const result = await axios.request(payload);
    return true;
}