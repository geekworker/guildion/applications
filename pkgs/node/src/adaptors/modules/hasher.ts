import sha256 from 'crypto-js/sha256';
import hmacSHA512 from 'crypto-js/hmac-sha512';
import Base64 from 'crypto-js/enc-base64';
import { v4 } from 'uuid';

export namespace Hasher {
    export interface EnhashPrivateKeyReturnType { 
        iv: string,
        salt: string,
        hash: string,
    }
    
    export const enhash = (salt: string, iv: string, privateKey: string): string => {
        return Base64.stringify(hmacSHA512(sha256(iv + salt), privateKey));
    }
    
    export const compare = (salt: string, iv: string, privateKey: string, hash: string): boolean => {
        return hash == enhash(salt, iv, privateKey);
    }
    
    export const enhashPrivateKey = (privateKey: string): EnhashPrivateKeyReturnType => {
        const iv = v4();
        const salt = v4();
        return {
            iv,
            salt,
            hash: enhash(salt, iv, privateKey),
        };
    }
}