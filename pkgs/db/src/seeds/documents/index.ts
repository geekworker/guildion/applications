import { DocumentAttributes, DocumentGroup, DocumentGroupStatus, DocumentSection, DocumentSectionAttributes, DocumentSectionStatus, DocumentStatus } from "@guildion/core"
import { buildSupports, buildLegals, buildPolicies } from "@guildion/data"
import Bluebird from "bluebird"
import { DocumentEntity } from "../../entities/DocumentEntity"
import { DocumentGroupEntity } from "../../entities/DocumentGroupEntity"
import { DocumentSectionEntity } from "../../entities/DocumentSectionEntity"

export const createOrUpdateSeedDocument = async ({ group, section, document }: { group: DocumentGroupEntity, section: DocumentSectionEntity, document: DocumentAttributes }): Promise<DocumentEntity> => {
    let exist = await DocumentEntity.findOne({
        where: {
            slug: document.slug,
            sectionId: section.id,
            groupId: group.id,
        },
    });
    if (exist) {
        exist.sectionId = section.id;
        exist.groupId = group.id;
        exist.enThumbnailId = document.enThumbnailId;
        exist.jaThumbnailId = document.jaThumbnailId;
        exist.slug = document.slug;
        exist.jaTitle = document.jaTitle;
        exist.enTitle = document.enTitle;
        exist.jaDescription = document.jaDescription;
        exist.enDescription = document.enDescription;
        exist.jaData = document.jaData;
        exist.enData = document.enData;
        exist.status = DocumentStatus.NEW === document.status ? DocumentStatus.PUBLISH : document.status;
        exist.type = document.type;
        exist.dataType = document.dataType;
        exist.index = document.index;
        exist.save();
    } else {
        exist = await DocumentEntity.create({
            sectionId: section.id,
            groupId: group.id,
            enThumbnailId: document.enThumbnailId,
            jaThumbnailId: document.jaThumbnailId,
            slug: document.slug,
            jaTitle: document.jaTitle,
            enTitle: document.enTitle,
            jaDescription: document.jaDescription,
            enDescription: document.enDescription,
            jaData: document.jaData,
            enData: document.enData,
            status: DocumentStatus.NEW === document.status ? DocumentStatus.PUBLISH : document.status,
            type: document.type,
            dataType: document.dataType,
            index: document.index,
        }).save();
    }
    return exist;
}

export const createOrUpdateSeedDocumentSection = async ({ group, section }: { group: DocumentGroupEntity, section: DocumentSectionAttributes }): Promise<{ section: DocumentSectionEntity, documents: DocumentEntity[] }> => {
    let exist = await DocumentSectionEntity.findOne({
        where: {
            groupId: group.id,
            slug: section.slug,
        },
    });
    if (exist) {
        exist.jaName = section.jaName;
        exist.enName = section.enName;
        exist.status = DocumentSectionStatus.NEW === section.status ? DocumentSectionStatus.PUBLISH : section.status;
        exist.type = section.type;
        exist.index = section.index;
        exist.groupId = group.id;
        exist.save();
    } else {
        exist = await DocumentSectionEntity.create({
            // sectionId: section.sectionId,
            groupId: group.id,
            slug: section.slug,
            jaName: section.jaName,
            enName: section.enName,
            status: DocumentSectionStatus.NEW === section.status ? DocumentSectionStatus.PUBLISH : section.status,
            type: section.type,
            index: section.index,
        }).save();
    }
    const documents = await Bluebird.map(
        section.documents ?? [],
        document => createOrUpdateSeedDocument({ group, section: exist!, document }),
        { concurrency: 10 }
    );
    return {
        section: exist,
        documents,
    };
}

export const createOrUpdateSeedDocumentGroup = async (group: DocumentGroup): Promise<{ group: DocumentGroupEntity, sections: DocumentSectionEntity[], documents: DocumentEntity[] }> => {
    let exist = await DocumentGroupEntity.findOne({
        where: {
            slug: group.slug,
        },
    });
    if (exist) {
        exist.jaName = group.jaName;
        exist.enName = group.enName;
        exist.status = DocumentGroupStatus.NEW === group.status ? DocumentGroupStatus.PUBLISH : group.status;
        exist.type = group.type;
        exist.save();
    } else {
        exist = await DocumentGroupEntity.create({
            ...group.attributes,
            status: DocumentGroupStatus.NEW === group.status ? DocumentGroupStatus.PUBLISH : group.status,
        }).save();
    }
    const results = await Bluebird.map(
        group.sections ?? group.records.sections?.toArray() ?? [],
        section => createOrUpdateSeedDocumentSection({ group: exist!, section }),
        { concurrency: 10 }
    );
    return {
        group: exist,
        sections: results.map(r => r.section),
        documents: results.map(r => r.documents).flat(),
    };
}

export const createOrUpdateSeedDocumentGroups = async () => {
    const legalsGroup = await createOrUpdateSeedDocumentGroup(buildLegals());
    const policiesGroup = await createOrUpdateSeedDocumentGroup(buildPolicies());
    const supportsGroup = await createOrUpdateSeedDocumentGroup(buildSupports());
    const groupIds = [legalsGroup.group.id, policiesGroup.group.id, supportsGroup.group.id];
    const sectionIds = [...legalsGroup.sections.map(v => v.id), ...policiesGroup.sections.map(v => v.id), ...supportsGroup.sections.map(v => v.id)];
    const documentIds = [...legalsGroup.documents.map(v => v.id), ...policiesGroup.documents.map(v => v.id), ...supportsGroup.documents.map(v => v.id)];

    const shouldDeleteGroups = await DocumentGroupEntity
        .getRepository()
        .createQueryBuilder('document_groups')
        .where('document_groups.id NOT IN (:...groupIds)', { groupIds })
        .getMany();
    
    await DocumentGroupEntity.remove(shouldDeleteGroups);
    
    const shouldDeleteSections = await DocumentSectionEntity
        .getRepository()
        .createQueryBuilder('document_sections')
        .where('document_sections.id NOT IN (:...sectionIds)', { sectionIds })
        .getMany();
    
    await DocumentSectionEntity.remove(shouldDeleteSections);

    const shouldDeleteDocuments = await DocumentEntity
        .getRepository()
        .createQueryBuilder('documents')
        .where('documents.id NOT IN (:...documentIds)', { documentIds })
        .getMany();
    
    await DocumentEntity.remove(shouldDeleteDocuments);
}