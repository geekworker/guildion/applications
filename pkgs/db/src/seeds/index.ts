import { FileEntity } from "../entities/FileEntity";
import { SeedEntity } from "../entities/SeedEntity";
import { createOrUpdateSeedDocumentGroups } from "./documents";
import { createOrUpdateSeedFiles } from "./files";
import { createOrUpdateSeedEmojiFiles } from "./files/emoji";
import { createOrUpdateSeedGuildCategories } from "./guildCategories";
import { createOrUpdateSeedBlogCategories } from './blogs';

export interface SeedOptions {
    afterFileCreate?: (file: FileEntity) => any | Promise<any>
};

export const createOrUpdateSeeds = async (options?: SeedOptions) => {
    if (await SeedEntity.checksumSeed()) return;
    await createOrUpdateSeedFiles({ afterCreate: options?.afterFileCreate });
    await createOrUpdateSeedEmojiFiles();
    await createOrUpdateSeedGuildCategories();
    await createOrUpdateSeedDocumentGroups();
    await createOrUpdateSeedBlogCategories();
    await SeedEntity.updateChecksum();
};