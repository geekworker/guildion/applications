import { GuildCategory, GuildCategories } from "@guildion/core";
import { GuildCategoryEntity } from "@guildion/db/src/entities/GuildCategoryEntity";

export let seedGuildCategories: GuildCategoryEntity[];

export const createOrUpdateSeedGuildCategory = async (guildCategory: GuildCategory): Promise<GuildCategoryEntity> => {
    const result = await GuildCategoryEntity.findOne({
        where: {
            slug: guildCategory.slug,
        }
    }) || await GuildCategoryEntity.create({
        ...guildCategory.attributes,
    }).save();
    return result;
};

export const createOrUpdateSeedGuildCategories = async (): Promise<GuildCategoryEntity[]> => {
    const guildCategories = GuildCategories.getSeed();
    seedGuildCategories = [];
    for (const guildCategory of guildCategories) {
        const result = await createOrUpdateSeedGuildCategory(guildCategory);
        seedGuildCategories.push(result);
    };
    return seedGuildCategories;
}