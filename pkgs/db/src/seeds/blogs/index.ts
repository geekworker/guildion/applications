import { BlogAttributes, BlogCategoryAttributes, BlogCategoryStatus, BlogStatus, IS_PROD } from "@guildion/core"
import { buildCodes, buildTest, buildApp } from "@guildion/data"
import Bluebird from "bluebird"
import { BlogEntity } from "../../entities/BlogEntity"
import { BlogCategoryEntity } from "../../entities/BlogCategoryEntity"

export const createOrUpdateSeedBlog = async ({ category, blog }: { category: BlogCategoryEntity, blog: BlogAttributes }): Promise<BlogEntity> => {
    let exist = await BlogEntity.findOne({
        where: {
            slug: blog.slug,
            categoryId: category.id,
        },
    });
    if (exist) {
        exist.categoryId = category.id;
        exist.enThumbnailURL = blog.enThumbnailURL;
        exist.jaThumbnailURL = blog.jaThumbnailURL;
        exist.slug = blog.slug;
        exist.jaTitle = blog.jaTitle;
        exist.enTitle = blog.enTitle;
        exist.jaDescription = blog.jaDescription;
        exist.enDescription = blog.enDescription;
        exist.jaData = blog.jaData;
        exist.enData = blog.enData;
        exist.status = BlogStatus.NEW === blog.status ? BlogStatus.PUBLISH : blog.status;
        exist.type = blog.type;
        exist.dataType = blog.dataType;
        exist.index = blog.index;
        exist.save();
    } else {
        exist = await BlogEntity.create({
            categoryId: category.id,
            enThumbnailURL: blog.enThumbnailURL,
            jaThumbnailURL: blog.jaThumbnailURL,
            slug: blog.slug,
            jaTitle: blog.jaTitle,
            enTitle: blog.enTitle,
            jaDescription: blog.jaDescription,
            enDescription: blog.enDescription,
            jaData: blog.jaData,
            enData: blog.enData,
            status: BlogStatus.NEW === blog.status ? BlogStatus.PUBLISH : blog.status,
            type: blog.type,
            dataType: blog.dataType,
            index: blog.index,
        }).save();
    }
    blogIds.push(exist.id);
    return exist;
}

export const createOrUpdateSeedBlogCategory = async ({ parent, category }: { parent?: BlogCategoryEntity, category: BlogCategoryAttributes }): Promise<{ category: BlogCategoryEntity, blogs: BlogEntity[], children: { category: BlogCategoryEntity, blogs: BlogEntity[] }[] }> => {
    let exist = await BlogCategoryEntity.findOne({
        where: {
            slug: category.slug,
        },
    });
    if (exist) {
        exist.parentId = parent?.id;
        exist.jaName = category.jaName;
        exist.enName = category.enName;
        exist.status = BlogCategoryStatus.NEW === category.status ? BlogCategoryStatus.PUBLISH : category.status;
        exist.index = category.index;
        exist.save();
    } else {
        exist = await BlogCategoryEntity.create({
            parentId: parent?.id,
            slug: category.slug,
            jaName: category.jaName,
            enName: category.enName,
            status: BlogCategoryStatus.NEW === category.status ? BlogCategoryStatus.PUBLISH : category.status,
            index: category.index,
        }).save();
    }
    categoryIds.push(exist.id);
    const blogs = await Bluebird.map(
        category.blogs ?? [],
        blog => createOrUpdateSeedBlog({ category: exist!, blog }),
        { concurrency: 10 }
    );
    const children = await Bluebird.map(
        category.children ?? [],
        child => createOrUpdateSeedBlogCategory({ parent: exist!, category: child }),
        { concurrency: 10 }
    );
    return {
        category: exist,
        children,
        blogs,
    };
}

let categoryIds: string[] = [];
let blogIds: string[] = [];

export const createOrUpdateSeedBlogCategories = async () => {
    categoryIds = [];
    blogIds = [];
    await createOrUpdateSeedBlogCategory({ category: buildApp() });
    await createOrUpdateSeedBlogCategory({ category: buildCodes() });
    if (!IS_PROD()) await createOrUpdateSeedBlogCategory({ category: buildTest() });
    const shouldDeleteCategorys = await BlogCategoryEntity
        .getRepository()
        .createQueryBuilder('blog_categorys')
        .where('blog_categorys.id NOT IN (:...categoryIds)', { categoryIds })
        .getMany();
    await BlogCategoryEntity.remove(shouldDeleteCategorys);

    const shouldDeleteBlogs = await BlogEntity
        .getRepository()
        .createQueryBuilder('blogs')
        .where('blogs.id NOT IN (:...blogIds)', { blogIds })
        .getMany();
    await BlogEntity.remove(shouldDeleteBlogs);
}