import { File, Files, FileStatus } from "@guildion/core";
import { FileEntity } from "@guildion/db/src/entities/FileEntity";
import Bluebird from "bluebird";

export let seedFileEntities: FileEntity[];
export let seedFiles: Files;

export const fetchOrFindSeedFiles = async (): Promise<FileEntity[]> => {
    if (!!seedFiles && !!seedFileEntities) return seedFileEntities;
    const files = Files.getSeed();
    const entities = await Promise.all(
        files.toArray().map(file => FileEntity.findOne({
            where: {
                url: file.url,
            }
        }))
    );
    seedFileEntities = entities.filter((val): val is FileEntity => !!val);
    seedFiles = new Files(await Promise.all(seedFileEntities.map(sf => sf.transform())));
    return seedFileEntities;
};

export const createOrUpdateSeedFile = async (file: File, options?: { afterCreate?: (file: FileEntity) => void | Promise<void> }): Promise<FileEntity> => {
    const result = await FileEntity.findOne({
        where: {
            url: file.url,
        }
    })
    if (!result) {
        const created = await FileEntity.create({
            ...file.attributes,
            status: FileStatus.UPLOADING,
        }).save();
        if (options?.afterCreate) await options.afterCreate(created);
        return created;
    } else {
        if (result.status === FileStatus.UPLOADED && options?.afterCreate) await options.afterCreate(result);
        return result;
    };
};

export const createOrUpdateSeedFiles = async (options?: { afterCreate?: (file: FileEntity) => void | Promise<void> }): Promise<FileEntity[]> => {
    const files = Files.getSeed();
    seedFileEntities = [];
    await Bluebird.map(
        files,
        async file => {
            const result = await createOrUpdateSeedFile(file, options);
            seedFileEntities.push(result);
        },
        { concurrency: 10 }
    );
    seedFiles = new Files(await Promise.all(seedFileEntities.map(sf => sf.transform())));
    return seedFileEntities;
}