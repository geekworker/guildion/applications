import { ContentType, Emoji, File, FileAccessType, FileExtension, Files, FileStatus, FileType, GET_CURRENT_API_URL_STRING, Provider } from "@guildion/core";
import { FileEntity } from "@guildion/db/src/entities/FileEntity";
import Bluebird from "bluebird";
import { EmojiEntity } from "../../entities/EmojiEntity";

export let seedEmojiEntities: FileEntity[];
export let seedEmojis: Files;

export const createOrUpdateSeedEmojiFile = async (emoji: Emoji, emojiName: keyof typeof Emoji): Promise<FileEntity> => {
    const providerKey = File.generateEmojiKey(emojiName);
    const url = `${GET_CURRENT_API_URL_STRING()}/${providerKey}`;
    const result = await FileEntity.findOne({
        where: {
            url,
            providerKey: providerKey,
            type: FileType.Emoji,
            provider: Provider.Guildion,
            isSystem: true,
        }
    });
    if (!result) {
        const created = await FileEntity.create({
            displayName: emojiName,
            description: emoji,
            url,
            type: FileType.Emoji,
            contentType: ContentType.other,
            extension: FileExtension(ContentType.other),
            status: FileStatus.UPLOADED,
            accessType: FileAccessType.PUBLIC,
            provider: Provider.Guildion,
            providerKey: providerKey,
            isSystem: true,
        }).save();
        await EmojiEntity.create({
            fileId: created.id,
            content: emoji,
        }).save();
        return created;
    } else {
        return result;
    };
};

export const createOrUpdateSeedEmojiFiles = async (): Promise<FileEntity[]> => {
    const emojiNames = Object.keys(Emoji);
    const emojis = Object.keys(Emoji).map((k) => Emoji[k as keyof typeof Emoji]);
    seedEmojiEntities = [];
    await Bluebird.map(
        emojis,
        async (emoji, i) => {
            const result = await createOrUpdateSeedEmojiFile(emoji, emojiNames[i] as keyof typeof Emoji);
            seedEmojiEntities.push(result);
        },
        { concurrency: 10 }
    );
    seedEmojis = new Files(await Promise.all(seedEmojiEntities.map(sf => sf.transform())));
    return seedEmojiEntities;
}