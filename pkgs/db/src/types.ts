import { type } from "os";
import { BaseEntity, Repository } from "typeorm";
import { FindConditions } from "typeorm";
import { DeepPartial } from "typeorm";
import { SaveOptions } from "typeorm";
import { FindOneOptions } from "typeorm";
import { RemoveOptions } from "typeorm";
import { FindManyOptions } from "typeorm";
import { Connection } from "typeorm";
import { ObjectType } from "typeorm";
import { SelectQueryBuilder } from "typeorm";
import { InsertResult } from "typeorm";
import { UpdateResult } from "typeorm";
import { DeleteResult } from "typeorm";
import { ObjectID } from "typeorm";

/**
 * Make all properties in T optional
 */
 export declare type QueryPartialEntity<T> = {
    [P in keyof T]?: T[P] | (() => string);
};
/**
 * Make all properties in T optional. Deep version.
 */
export declare type QueryDeepPartialEntity<T> = {
    [P in keyof T]?: (T[P] extends Array<infer U> ? Array<QueryDeepPartialEntity<U>> : T[P] extends ReadonlyArray<infer U> ? ReadonlyArray<QueryDeepPartialEntity<U>> : QueryDeepPartialEntity<T[P]>) | (() => string);
};

/**
 * Base abstract entity for all entities, used in ActiveRecord patterns.
 */
export type BaseEntityClass<T extends BaseEntity> = {
    new(...args: any): T

    useConnection(connection: Connection): void;
    /**
     * Gets current entity's Repository.
     */
    getRepository(this: ObjectType<T>): Repository<T>;
    /**
     * Returns object that is managed by this repository.
     * If this repository manages entity from schema,
     * then it returns a name of that schema instead.
     */
    get target(): Function | string;
    /**
     * Checks entity has an id.
     * If entity composite compose ids, it will check them all.
     */
    hasId(entity: BaseEntity): boolean;
    /**
     * Gets entity mixed id.
     */
    getId(this: ObjectType<T>, entity: T): any;
    /**
     * Creates a new query builder that can be used to build a sql query.
     */
    createQueryBuilder(this: ObjectType<T>, alias?: string): SelectQueryBuilder<T>;
    /**
     * Creates a new entity instance.
     */
    create(this: ObjectType<T>): T;
    /**
     * Creates a new entities and copies all entity properties from given objects into their new entities.
     * Note that it copies only properties that present in entity schema.
     */
    create(this: ObjectType<T>, entityLikeArray: DeepPartial<T>[]): T[];
    /**
     * Creates a new entity instance and copies all entity properties from this object into a new entity.
     * Note that it copies only properties that present in entity schema.
     */
    create(this: ObjectType<T>, entityLike: DeepPartial<T>): T;
    /**
     * Merges multiple entities (or entity-like objects) into a given entity.
     */
    merge(this: ObjectType<T>, mergeIntoEntity: T, ...entityLikes: DeepPartial<T>[]): T;
    /**
     * Creates a new entity from the given plain javascript object. If entity already exist in the database, then
     * it loads it (and everything related to it), replaces all values with the new ones from the given object
     * and returns this new entity. This new entity is actually a loaded from the db entity with all properties
     * replaced from the new object.
     *
     * Note that given entity-like object must have an entity id / primary key to find entity by.
     * Returns undefined if entity with given id was not found.
     */
    preload(this: ObjectType<T>, entityLike: DeepPartial<T>): Promise<T | undefined>;
    /**
     * Saves all given entities in the database.
     * If entities do not exist in the database then inserts, otherwise updates.
     */
    save(this: ObjectType<T>, entities: T[], options?: SaveOptions): Promise<T[]>;
    /**
     * Saves a given entity in the database.
     * If entity does not exist in the database then inserts, otherwise updates.
     */
    save(this: ObjectType<T>, entity: T, options?: SaveOptions): Promise<T>;
    /**
     * Removes a given entities from the database.
     */
    remove(this: ObjectType<T>, entities: T[], options?: RemoveOptions): Promise<T[]>;
    /**
     * Removes a given entity from the database.
     */
    remove(this: ObjectType<T>, entity: T, options?: RemoveOptions): Promise<T>;
    /**
     * Records the delete date of all given entities.
     */
    softRemove(this: ObjectType<T>, entities: T[], options?: SaveOptions): Promise<T[]>;
    /**
     * Records the delete date of a given entity.
     */
    softRemove(this: ObjectType<T>, entity: T, options?: SaveOptions): Promise<T>;
    /**
     * Inserts a given entity into the database.
     * Unlike save method executes a primitive operation without cascades, relations and other operations included.
     * Executes fast and efficient INSERT query.
     * Does not check if entity exist in the database, so query will fail if duplicate entity is being inserted.
     */
    insert(this: ObjectType<T>, entity: QueryDeepPartialEntity<T> | QueryDeepPartialEntity<T>[], options?: SaveOptions): Promise<InsertResult>;
    /**
     * Updates entity partially. Entity can be found by a given conditions.
     * Unlike save method executes a primitive operation without cascades, relations and other operations included.
     * Executes fast and efficient UPDATE query.
     * Does not check if entity exist in the database.
     */
    update(this: ObjectType<T>, criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<T>, partialEntity: QueryDeepPartialEntity<T>, options?: SaveOptions): Promise<UpdateResult>;
    /**
     * Deletes entities by a given criteria.
     * Unlike remove method executes a primitive operation without cascades, relations and other operations included.
     * Executes fast and efficient DELETE query.
     * Does not check if entity exist in the database.
     */
    delete(this: ObjectType<T>, criteria: string | string[] | number | number[] | Date | Date[] | ObjectID | ObjectID[] | FindConditions<T>, options?: RemoveOptions): Promise<DeleteResult>;
    /**
     * Counts entities that match given options.
     */
    count(this: ObjectType<T>, options?: FindManyOptions<T>): Promise<number>;
    /**
     * Counts entities that match given conditions.
     */
    count(this: ObjectType<T>, conditions?: FindConditions<T>): Promise<number>;
    /**
     * Finds entities that match given options.
     */
    find(this: ObjectType<T>, options?: FindManyOptions<T>): Promise<T[]>;
    /**
     * Finds entities that match given conditions.
     */
    find(this: ObjectType<T>, conditions?: FindConditions<T>): Promise<T[]>;
    /**
     * Finds entities that match given find options.
     * Also counts all entities that match given conditions,
     * but ignores pagination settings (from and take options).
     */
    findAndCount(this: ObjectType<T>, options?: FindManyOptions<T>): Promise<[T[], number]>;
    /**
     * Finds entities that match given conditions.
     * Also counts all entities that match given conditions,
     * but ignores pagination settings (from and take options).
     */
    findAndCount(this: ObjectType<T>, conditions?: FindConditions<T>): Promise<[T[], number]>;
    /**
     * Finds entities by ids.
     * Optionally find options can be applied.
     */
    findByIds(this: ObjectType<T>, ids: any[], options?: FindManyOptions<T>): Promise<T[]>;
    /**
     * Finds entities by ids.
     * Optionally conditions can be applied.
     */
    findByIds(this: ObjectType<T>, ids: any[], conditions?: FindConditions<T>): Promise<T[]>;
    /**
     * Finds first entity that matches given options.
     */
    findOne(this: ObjectType<T>, id?: string | number | Date | ObjectID, options?: FindOneOptions<T>): Promise<T | undefined>;
    /**
     * Finds first entity that matches given options.
     */
    findOne(this: ObjectType<T>, options?: FindOneOptions<T>): Promise<T | undefined>;
    /**
     * Finds first entity that matches given conditions.
     */
    findOne(this: ObjectType<T>, conditions?: FindConditions<T>, options?: FindOneOptions<T>): Promise<T | undefined>;
    /**
     * Finds first entity that matches given options.
     */
    findOneOrFail(this: ObjectType<T>, id?: string | number | Date | ObjectID, options?: FindOneOptions<T>): Promise<T>;
    /**
     * Finds first entity that matches given options.
     */
    findOneOrFail(this: ObjectType<T>, options?: FindOneOptions<T>): Promise<T>;
    /**
     * Finds first entity that matches given conditions.
     */
    findOneOrFail(this: ObjectType<T>, conditions?: FindConditions<T>, options?: FindOneOptions<T>): Promise<T>;
    /**
     * Executes a raw SQL query and returns a raw database results.
     * Raw query execution is supported only by relational databases (MongoDB is not supported).
     */
    query(this: ObjectType<T>, query: string, parameters?: any[]): Promise<any>;
    /**
     * Clears all the data from the given table/collection (truncates/drops it).
     */
    clear(this: ObjectType<T>): Promise<void>;
}

export type EntityName = 
    'AccessTokenEntity' |
    'AccountEntity' |
    'AdministratorEntity' |
    'BlogEntity' |
    'BlogCategoryEntity' |
    'ConnectionEntity' |
    'CSRFEntity' |
    'DeviceBlockEntity' |
    'DeviceConnectionEntity' |
    'DeviceEntity' |
    'DiscoveryEntity' |
    'DiscoveryGuildEntity' |
    'DiscoveryRoomEntity' |
    'DocumentEntity' |
    'DocumentGroupEntity' |
    'DocumentSectionEntity' |
    'EmojiEntity' |
    'FileEntity' |
    'FileReportEntity' |
    'FileRoleEntity' |
    'FilingEntity' |
    'FolderEntity' |
    'GuildActivityEntity' |
    'GuildBlockEntity' |
    'GuildCategoryEntity' |
    'GuildCategoryListEntity' |
    'GuildEntity' |
    'GuildFileEntity' |
    'GuildPremiumKeyEntity' |
    'GuildReportEntity' |
    'GuildRewardEntity' |
    'GuildRewardPaymentEntity' |
    'GuildStorageEntity' |
    'ImageEntity' |
    'InviteEntity' |
    'InviteMemberEntity' |
    'MemberConnectionEntity' |
    'MemberEntity' |
    'MemberRoleEntity' |
    'MessageAttachmentEntity' |
    'MessageEntity' |
    'MessageReactionEntity' |
    'MessageReadEntity' |
    'NewsLetterSubscriberEntity' |
    'NotificationEntity' |
    'PostAttachmentEntity' |
    'PostEntity' |
    'PostReactionEntity' |
    'PostReadEntity' |
    'PremiumKeyEntity' |
    'PremiumKeySubscriptionEntity' |
    'PremiumKeySubscriptionPaymentEntity' |
    'RoleEntity' |
    'ReportEntity' |
    'ReportReadEntity' |
    'RoomActivityEntity' |
    'RoomBlockEntity' |
    'RoomCategoryEntity' |
    'RoomCategoryListEntity' |
    'RoomEntity' |
    'RoomFileEntity' |
    'RoomLogEntity' |
    'RoomMemberConnectionEntity' |
    'RoomMemberEntity' |
    'RoomReportEntity' |
    'RoomRoleEntity' |
    'RoomStorageEntity' |
    'ScenarioContextEntity' |
    'SeedEntity' |
    'SyncVisionEntity' |
    'UserConnectionEntity' |
    'UserDeviceEntity' |
    'UserEntity' |
    'UserReportEntity' |
    'VideoEntity';