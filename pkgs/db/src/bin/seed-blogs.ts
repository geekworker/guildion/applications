import { createOrUpdateSeedBlogCategories } from '../seeds/blogs';
import { connect } from '../config/connection';
import dotenv from 'dotenv';

(async () => {
    dotenv.config();
    await connect({
        logging: true,
        DATABASE_USERNAME: process.env.DATABASE_USERNAME!,
        DATABASE_PASSWORD: process.env.DATABASE_PASSWORD,
        DATABASE_NAME: process.env.DATABASE_NAME!,
        DATABASE_HOST: process.env.DATABASE_HOST!,
        DATABASE_PORT: Number(process.env.DATABASE_PORT ?? 5432),
    })
    await createOrUpdateSeedBlogCategories();
})();