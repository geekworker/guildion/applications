import path from 'path';
import fs from 'fs';
import { hashElement } from "folder-hash";
import { SEED_CHECKSUM } from "../config/checksum";
import { DOCUMENTS_SEED_CHECKSUM } from '../seeds/documents/checksum';
import { BLOGS_SEED_CHECKSUM } from '../seeds/blogs/checksum';

const options = {
    folders: { exclude: ['.*', 'node_modules'] },
    files: { include: ['*.ts', '*.js', '*.json'] },
};

export const generateDocumentsChecksum = async (): Promise<string> => {
    const checksum = (await hashElement(path.join(__dirname, '..', '..', '..', 'data', 'src', 'public', 'documents'), options)).hash.toString();
    if (checksum == DOCUMENTS_SEED_CHECKSUM) return DOCUMENTS_SEED_CHECKSUM;
    let seedChecksum = fs.readFileSync(path.join(__dirname, '..', 'seeds', 'documents', 'checksum.ts'), "utf8");
    seedChecksum = `export const DOCUMENTS_SEED_CHECKSUM = "${checksum}";`;
    fs.writeFileSync(path.join(__dirname, '..', 'seeds', 'documents', 'checksum.ts'), seedChecksum);
    return checksum;
}

export const generateBlogsChecksum = async (): Promise<string> => {
    const checksum = (await hashElement(path.join(__dirname, '..', '..', '..', 'data', 'src', 'public', 'blogs'), options)).hash.toString();
    if (checksum == BLOGS_SEED_CHECKSUM) return BLOGS_SEED_CHECKSUM;
    let seedChecksum = fs.readFileSync(path.join(__dirname, '..', 'seeds', 'blogs', 'checksum.ts'), "utf8");
    seedChecksum = `export const BLOGS_SEED_CHECKSUM = "${checksum}";`;
    fs.writeFileSync(path.join(__dirname, '..', 'seeds', 'blogs', 'checksum.ts'), seedChecksum);
    return checksum;
}

export const generateChecksum = async (): Promise<string> => {
    await generateDocumentsChecksum();
    await generateBlogsChecksum();
    const checksum = (await hashElement(path.join(__dirname, '..', 'seeds'), options)).hash.toString();
    if (checksum == SEED_CHECKSUM) return SEED_CHECKSUM;
    let seedChecksum = fs.readFileSync(path.join(__dirname, '..', 'config', 'checksum.ts'), "utf8");
    seedChecksum = `export const SEED_CHECKSUM = "${checksum}";`;
    fs.writeFileSync(path.join(__dirname, '..', 'config', 'checksum.ts'), seedChecksum);
    return checksum;
}

(async () => {
    generateChecksum();
})();