import { FileAttributes, MemberAttributes, MessageAttributes, PostAttributes, RoleAttributes, RoomAttributes, RoomLogAttributes, RoomLogType, SyncVisionAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { MemberEntity } from "./MemberEntity";
import type { MessageEntity } from "./MessageEntity";
import type { PostEntity } from "./PostEntity";
import type { RoomActivityEntity } from "./RoomActivityEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomFileEntity } from "./RoomFileEntity";
import type { RoomRoleEntity } from "./RoomRoleEntity";
import type { SyncVisionEntity } from "./SyncVisionEntity";

@Entity({ name: "room_logs" })
export class RoomLogEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "message_id", type: "uuid", nullable: true })
    messageId?: string | null;

    @Column({ name: "post_id", type: "uuid", nullable: true })
    postId?: string | null;

    @Column({ name: "sync_vision_id", type: "uuid", nullable: true })
    syncVisionId?: string | null;

    @Column({ name: "room_role_id", type: "uuid", nullable: true })
    roomRoleId?: string | null;

    @Column({ name: "room_file_id", type: "uuid", nullable: true })
    roomFileId?: string | null;

    @Column({ name: "member_id", type: "uuid", nullable: true })
    memberId?: string | null;

    @Column({ name: "type" })
    type: RoomLogType;

    @Column({ name: "changed_column", type: "varchar", nullable: true })
    changedColumn?: string | null;

    @Column({ name: "before_diff", type: "text", nullable: true })
    beforeDiff?: string | null;

    @Column({ name: "after_diff", type: "text", nullable: true })
    afterDiff?: string | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.logs)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @OneToMany(() => resolveEntity<RoomActivityEntity>('RoomActivityEntity'), (activity) => activity.log)
    activities: Promise<RoomActivityEntity[]>;

    @ManyToOne(() => resolveEntity<SyncVisionEntity>('SyncVisionEntity'), (syncVision) => syncVision.logs)
    @JoinColumn({ name: "sync_vision_id" })
    syncVision: Promise<SyncVisionEntity | undefined>;

    @ManyToOne(() => resolveEntity<RoomRoleEntity>('RoomRoleEntity'), (roomRole) => roomRole.logs)
    @JoinColumn({ name: "room_role_id" })
    roomRole: Promise<RoomRoleEntity | undefined>;

    @ManyToOne(() => resolveEntity<RoomFileEntity>('RoomFileEntity'), (roomFile) => roomFile.logs)
    @JoinColumn({ name: "room_file_id" })
    roomFile: Promise<RoomFileEntity | undefined>;

    @ManyToOne(() => resolveEntity<MessageEntity>('MessageEntity'), (message) => message.logs)
    @JoinColumn({ name: "message_id" })
    message: Promise<MessageEntity | undefined>;

    @ManyToOne(() => resolveEntity<PostEntity>('PostEntity'), (post) => post.logs)
    @JoinColumn({ name: "post_id" })
    post: Promise<PostEntity | undefined>;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.logs)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity | undefined>;

    async transform(options: {
        room?: boolean | RoomAttributes,
        post?: boolean | PostAttributes,
        message?: boolean | MessageAttributes,
        syncVision?: boolean | SyncVisionAttributes,
        roomRole?: boolean | RoleAttributes,
        roomFile?: boolean | FileAttributes,
        member?: boolean | MemberAttributes,
    } = {}): Promise<RoomLogAttributes> {
        const [
            room,
            message,
            syncVision,
            post,
        ] = await Bluebird.all<RoomAttributes | undefined, MessageAttributes | undefined, SyncVisionAttributes | undefined, PostAttributes | undefined>([
            options.room == true ? await (await this.room).transform() : options.room || undefined,
            options.message == true ? await (await this.message)?.transform() : options.message || undefined,
            options.syncVision == true ? await (await this.syncVision)?.transform() : options.syncVision || undefined,
            options.post == true ? await (await this.post)?.transform() : options.post || undefined,
        ]);

        const [
            roomFile,
            roomRole,
            member,
        ] = await Bluebird.all<FileAttributes | undefined, RoleAttributes | undefined, MemberAttributes | undefined>([
            options.roomFile == true ? await (await this.roomFile)?.transform({ holder: true }) : options.roomFile || undefined,
            options.roomRole == true ? await (await this.roomRole)?.transform() : options.roomRole || undefined,
            options.member == true ? await (await this.member)?.transform() : options.member || undefined,
        ]);

        return {
            id: this.id,
            roomId: this.roomId,
            messageId: this.messageId,
            postId: this.postId,
            syncVisionId: this.syncVisionId,
            roomRoleId: this.roomRoleId,
            roomFileId: this.roomFileId,
            memberId: this.memberId,
            type: this.type,
            changedColumn: this.changedColumn,
            beforeDiff: this.beforeDiff,
            afterDiff: this.afterDiff,
            createdAt: this.createdAt?.toString(),
            updatedAt: this.updatedAt?.toString(),
            room,
            message,
            syncVision,
            post,
            file: roomFile,
            role: roomRole,
            member,
        };
    }
}