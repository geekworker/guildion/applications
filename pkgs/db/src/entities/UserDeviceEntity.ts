import { UserDeviceStatus, UserDeviceSessionStatus } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";

@Entity({ name: "user_devices" })
export class UserDeviceEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "user_id" })
    userId: string;

    @Column({ name: "device_id" })
    deviceId: string;

    @Column({ name: "status" })
    status: UserDeviceStatus;

    @Column({ name: "session_status" })
    sessionStatus: UserDeviceSessionStatus;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;
}