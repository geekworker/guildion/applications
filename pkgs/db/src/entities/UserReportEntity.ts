import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";

@Entity({ name: "user_reports" })
export class UserReportEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "user_id" })
    userId: string;

    @Column({ name: "report_id" })
    reportId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;
}