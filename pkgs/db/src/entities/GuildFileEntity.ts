import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { GuildEntity } from "./GuildEntity";

@Entity({ name: "guild_files" })
export class GuildFileEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "file_id" })
    fileId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.guildFiles)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.guildFiles)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;
}