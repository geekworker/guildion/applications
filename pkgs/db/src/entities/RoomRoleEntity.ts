import { RoleAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    ManyToOne,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { RoleEntity } from "./RoleEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomLogEntity } from "./RoomLogEntity";

@Entity({ name: "room_roles" })
export class RoomRoleEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "role_id" })
    roleId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.roomRoles)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<RoleEntity>('RoleEntity'), (role) => role.roomRoles, { eager: true })
    @JoinColumn({ name: "role_id" })
    role: RoleEntity;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.roomRole)
    logs: Promise<RoomLogEntity[]>;

    async transform(options?: {}): Promise<RoleAttributes> {
        options ??= {};
        return await this.role.transform({
            roomRole: this
        })
    }
}