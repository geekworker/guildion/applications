import { MessageReadStatus } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { MessageEntity } from "./MessageEntity";

@Entity({ name: "message_reads" })
export class MessageReadEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "message_id" })
    messageId: string;

    @Column({ name: "member_id" })
    memberId: string;

    @Column({ name: "room_member_id" })
    roomMemberId: string;

    @Column({ name: "status" })
    status: MessageReadStatus;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<MessageEntity>('MessageEntity'), (message) => message.reads)
    @JoinColumn({ name: "message_id" })
    message: Promise<MessageEntity>;
}