import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";
import { NewsLetterSubscriberAttributes, NewsLetterSubscriberStatus } from "@guildion/core";

@Entity({ name: "news_letter_subscribers" })
export class NewsLetterSubscriberEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "display_name" })
    displayName: string;

    @Column({ name: "email" })
    email: string;

    @Column({ name: "status" })
    status: NewsLetterSubscriberStatus;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    async transform(
        options?: {
        }
    ): Promise<NewsLetterSubscriberAttributes> {
        options ??= {};
        return {
            id: this.id,
            displayName: this.displayName,
            email: this.email,
            status: this.status,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
        };
    }
}