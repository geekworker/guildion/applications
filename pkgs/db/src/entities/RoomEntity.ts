import { GuildAttributes, RoomAttributes, RoomType, RoomStatus, LanguageCode, CountryCode, FileAttributes, MemberAttributes, RoleAttributes, WidgetAttributes, RoomActivityAttributes, InviteAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
    OneToOne,
    ManyToMany,
    JoinTable,
    LessThanOrEqual,
} from "typeorm";
import type { FileEntity } from "./FileEntity";
import type { DiscoveryRoomEntity } from "./DiscoveryRoomEntity";
import type { GuildEntity } from "./GuildEntity";
import type { InviteEntity } from "./InviteEntity";
import type { MemberEntity } from "./MemberEntity";
import type { MessageEntity } from "./MessageEntity";
import type { PostEntity } from "./PostEntity";
import type { RoleEntity } from "./RoleEntity";
import type { RoomActivityEntity } from "./RoomActivityEntity";
import type { RoomCategoryEntity } from "./RoomCategoryEntity";
import type { RoomCategoryListEntity } from "./RoomCategoryListEntity";
import type { RoomFileEntity } from "./RoomFileEntity";
import type { RoomLogEntity } from "./RoomLogEntity";
import type { RoomMemberEntity } from "./RoomMemberEntity";
import type { RoomRoleEntity } from "./RoomRoleEntity";
import type { RoomStorageEntity } from "./RoomStorageEntity";
import type { SyncVisionEntity } from "./SyncVisionEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "rooms" })
export class RoomEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'profile_id' })
    profileId: string;

    @Column({ name: 'guild_id' })
    guildId: string;

    @Column({ name: 'owner_id' })
    ownerId: string;

    @Column({ name: 'display_name' })
    displayName: string;

    @Column({ type: 'text' })
    description: string;

    @Column({ name: 'roomname' })
    roomname: string;

    @Column({ name: "type" })
    type: RoomType;

    @Column({ name: "status" })
    status: RoomStatus;

    @Column({ name: "language_code" })
    languageCode: LanguageCode;

    @Column({ name: "country_code" })
    countryCode: CountryCode;

    @Column({ name: "max_content_length" })
    maxContentLength: number;

    @Column({ name: "deleted_at", type: 'timestamptz', nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.members)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;
    
    @OneToMany(() => resolveEntity<RoomCategoryListEntity>('RoomCategoryListEntity'), (categoryList) => categoryList.room)
    categoryLists: Promise<RoomCategoryListEntity[]>;

    @ManyToMany(() => resolveEntity<RoomCategoryEntity>('RoomCategoryEntity'))
    @JoinTable({
        name: 'room_category_lists',
        joinColumn: {
            name: 'room_id',
        },
        inverseJoinColumn: {
            name: 'category_id',
        },
    })
    categories: Promise<RoomCategoryEntity[]>;

    @OneToOne(() => resolveEntity<RoomStorageEntity>('RoomStorageEntity'), (roomStorage) => roomStorage.room)
    roomStorage: Promise<RoomStorageEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.rooms)
    @JoinColumn({ name: "profile_id" })
    profile: Promise<FileEntity>;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (owner) => owner.rooms)
    @JoinColumn({ name: "owner_id" })
    owner: Promise<MemberEntity>;

    @OneToMany(() => resolveEntity<RoomMemberEntity>('RoomMemberEntity'), (roomMember) => roomMember.room)
    roomMembers: Promise<RoomMemberEntity[]>;

    @OneToMany(() => resolveEntity<SyncVisionEntity>('SyncVisionEntity'), (syncVision) => syncVision.file)
    syncVisions: Promise<SyncVisionEntity[]>;

    @OneToMany(() => resolveEntity<RoomRoleEntity>('RoomRoleEntity'), (roomRole) => roomRole.room)
    roomRoles: Promise<RoomRoleEntity[]>;

    @ManyToMany(() => resolveEntity<RoleEntity>('RoleEntity'))
    @JoinTable({
        name: 'room_roles',
        joinColumn: {
            name: 'room_id',
        },
        inverseJoinColumn: {
            name: 'role_id',
        },
    })
    roles: Promise<RoleEntity[]>;

    @OneToMany(() => resolveEntity<MessageEntity>('MessageEntity'), (message) => message.room)
    messages: Promise<MessageEntity[]>;

    @OneToMany(() => resolveEntity<PostEntity>('PostEntity'), (post) => post.room)
    posts: Promise<PostEntity[]>;

    @OneToMany(() => resolveEntity<InviteEntity>('InviteEntity'), (invite) => invite.room)
    invites: Promise<InviteEntity[]>;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.room)
    logs: Promise<RoomLogEntity[]>;

    @OneToMany(() => resolveEntity<RoomActivityEntity>('RoomActivityEntity'), (activity) => activity.room)
    activities: Promise<RoomActivityEntity[]>;

    @OneToMany(() => resolveEntity<DiscoveryRoomEntity>('DiscoveryRoomEntity'), (discoveryRoom) => discoveryRoom.discovery)
    discoveryRooms: Promise<DiscoveryRoomEntity[]>;

    @OneToMany(() => resolveEntity<RoomFileEntity>('RoomFileEntity'), (roomFile) => roomFile.room)
    roomFiles: Promise<RoomFileEntity[]>;

    @ManyToMany(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinTable({
        name: 'room_files',
        joinColumn: {
            name: 'room_id',
        },
        inverseJoinColumn: {
            name: 'file_id',
        },
    })
    files: Promise<FileEntity[]>;

    currentMember?: MemberEntity;
    currentRoomMember?: RoomMemberEntity;

    async getLatestActivity(ltAt?: Date): Promise<RoomActivityEntity | undefined> {
        const latests = await resolveEntity<RoomActivityEntity>('RoomActivityEntity').find({
            where: {
                roomId: this.id,
                createdAt: LessThanOrEqual(ltAt ?? new Date()),
            },
            take: 1,
            order: { createdAt: 'DESC' },
        });
        return latests.length == 0 ? undefined : latests[0];
    }

    async activate(): Promise<RoomActivityEntity> {
        return await (resolveEntity<RoomActivityEntity>('RoomActivityEntity') as any).activate(this.id)
    }

    async transform(options: {
        owner?: boolean | MemberAttributes,
        currentMember?: MemberAttributes,
        widgets?: WidgetAttributes[],
        storage?: boolean | FileAttributes,
        roles?: boolean | RoleAttributes[],
        guild?: boolean | GuildAttributes,
        invites?: boolean | InviteAttributes[],
        files?: boolean | FileAttributes[],
        messageNotificationsCount?: boolean | number,
        postNotificationsCount?: boolean | number,
        notificationsCount?: boolean | number,
        connectingMembers?: boolean | MemberAttributes[],
        activity?: boolean | RoomActivityAttributes,
        activedBaseAt?: Date,
    } = {}): Promise<RoomAttributes> {
        const [
            profile,
            storage,
            roles,
        ] = await Bluebird.all<FileAttributes | undefined, FileAttributes | undefined, RoleAttributes[] | undefined>([
            (async () => (await this.profile).transform())(),
            (async () => options.storage == true ? (await (await (await this.roomStorage).file).transform()) : options.storage || undefined)(),
            (async () => options.roles ? (await Promise.all((await this.roles).map(role => role.transform()))) : options.roles || undefined)(),
        ]);

        const [
            guild,
            invites,
            files,
        ] = await Bluebird.all<GuildAttributes | undefined, InviteAttributes[] | undefined, FileAttributes[] | undefined>([
            (async () => options.guild == true ? (await (await this.guild).transform()) : options.guild || undefined)(),
            (async () => options.invites == true ? await Promise.all((await this.invites).map(val => val.transform())) : options.invites || undefined)(),
            (async () => options.files == true ? await Promise.all((await this.files).map(val => val.transform())) : options.files || undefined)(),
        ]);

        return {
            id: this.id,
            profileId: this.profileId,
            guildId: this.guildId,
            ownerId: this.ownerId,
            displayName: this.displayName,
            description: this.description,
            roomname: this.roomname,
            languageCode: this.languageCode,
            countryCode: this.countryCode,
            status: this.status,
            type: this.type,
            maxContentLength: this.maxContentLength,
            deletedAt: this.deletedAt?.toString(),
            createdAt: this.createdAt?.toString(),
            updatedAt: this.updatedAt?.toString(),
            activity: options.activity == true ? (await (await this.getLatestActivity(options.activedBaseAt))?.transform({
                log: true,
                room: false,
                syncVision: true,
                messageNotificationsCount: options.messageNotificationsCount,
                postNotificationsCount: options.postNotificationsCount,
                notificationsCount: options.notificationsCount,
                connectingMembers: options.connectingMembers,
                currentMember: options.currentMember || await this.currentMember?.transform(),
            })) : options.activity || undefined,
            profile,
            owner: options.owner == true ? (await (await this.owner).transform()) : options.owner || undefined,
            currentMember: options.currentMember || await this.currentMember?.transform(),
            widgets: options.widgets,
            storage,
            roles,
            guild,
            invites,
            files,
        };
    }
}