import { MessageAttributes, MessageStatus, FileAttributes, MemberAttributes, RoomAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { MemberEntity } from "./MemberEntity";
import type { MessageReadEntity } from "./MessageReadEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomLogEntity } from "./RoomLogEntity";

@Entity({ name: "messages" })
export class MessageEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "parent_id" })
    parentId: string;

    @Column({ name: "sender_id" })
    senderId: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "location" })
    location: string;

    @Column({ type: "text" })
    body: string;

    @Column({ name: "status" })
    status: MessageStatus;

    @Column({ name: "comments_count" })
    commentsCount: number;

    @Column({ name: "is_commentable" })
    isCommentable: boolean;

    @Column({ name: "archived_at", type: "timestamptz" })
    archivedAt?: Date | null;

    @Column({ name: "deleted_at", type: "timestamptz" })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => MessageEntity, (parent) => parent.children)
    @JoinColumn({ name: "parent_id" })
    parent: Promise<MessageEntity>;

    @OneToMany(() => MessageEntity, (child) => child.parent)
    children: Promise<MessageEntity[]>;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.messages)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.messages)
    @JoinColumn({ name: "sender_id" })
    sender: Promise<MemberEntity>;

    @ManyToMany(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinTable({
        name: 'message_attachments',
        joinColumn: {
            name: 'message_id',
        },
        inverseJoinColumn: {
            name: 'file_id',
        },
    })
    files: Promise<FileEntity[]>;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.message)
    logs: Promise<RoomLogEntity[]>;

    @OneToMany(() => resolveEntity<MessageReadEntity>('MessageReadEntity'), (read) => read.message)
    reads: Promise<MessageReadEntity[]>;

    async transform(options: {
        sender?: boolean | MemberAttributes,
        room?: boolean | RoomAttributes,
        parent?: boolean | MessageAttributes,
        files?: boolean | FileAttributes[],
        children?: boolean | MessageAttributes[],
        read?: MessageReadEntity,
    } = {}): Promise<MessageAttributes> {
        return {
            id: this.id,
            senderId: this.senderId,
            roomId: this.roomId,
            parentId: this.parentId,
            status: this.status,
            body: this.status == MessageStatus.CREATED ? this.body : '',
            location: this.location,
            readStatus: options.read?.status,
            commentsCount: this.commentsCount,
            isCommentable: this.isCommentable,
            archivedAt: this.archivedAt?.toISOString(),
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            room: options.room == true ? (await (await this.room)?.transform()) : options.room || undefined,
            sender: options.sender == true ? (await (await this.sender)?.transform()) : options.sender || undefined,
            parent: options.parent == true ? (await (await this.parent)?.transform()) : options.parent || undefined,
            files: this.status == MessageStatus.CREATED ? options.files == true ? (await Promise.all((await this.files).map(file => file.transform()))) : options.files || undefined : undefined,
            children: options.children == true ? (await Promise.all((await this.children).map(child => child.transform()))) : options.children || undefined,
        };
    }

    hasMention(member: MemberEntity): boolean {
        return this.body.includes(member.id);
    }
}