import { GuildAttributes, GuildCategoryAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildCategoryListEntity } from "./GuildCategoryListEntity";
import type { GuildEntity } from "./GuildEntity";

@Entity({ name: "guild_categories" })
export class GuildCategoryEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'ja_name' })
    jaName: string;

    @Column({ name: 'en_name' })
    enName: string;

    @Column({ name: 'ja_groupname' })
    jaGroupname: string;

    @Column({ name: 'en_groupname' })
    enGroupname: string;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'index' })
    index: number;

    @Column({ name: 'hidden' })
    hidden: boolean;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToMany(() => resolveEntity<GuildCategoryListEntity>('GuildCategoryListEntity'), (list) => list.category)
    lists: Promise<GuildCategoryListEntity[]>;

    @ManyToMany(() => resolveEntity<GuildEntity>('GuildEntity'))
    @JoinTable({
        name: 'guild_category_lists',
        joinColumn: {
            name: 'category_id',
        },
        inverseJoinColumn: {
            name: 'guild_id',
        },
    })
    guilds: Promise<GuildEntity[]>;

    async transform(
        options: {
            guilds?: boolean | GuildAttributes[],
        } = {}
    ): Promise<GuildCategoryAttributes> {
        return {
            id: this.id,
            jaName: this.jaName,
            enName: this.enName,
            jaGroupname: this.jaGroupname,
            enGroupname: this.enGroupname,
            slug: this.slug,
            index: this.index,
            hidden: this.hidden,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            guilds: options.guilds == true ? (await Promise.all((await this.guilds).map(guild => guild.transform()))) : options.guilds || undefined,
        };
    }
}