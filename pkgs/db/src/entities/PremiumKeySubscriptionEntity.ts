import { MemberAttributes, PaymentMethod, PremiumKeyAttributes, PremiumKeySubscriptionAttributes, PremiumKeySubscriptionStatus } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildPremiumKeyEntity } from "./GuildPremiumKeyEntity";
import type { MemberEntity } from "./MemberEntity";
import type { PremiumKeySubscriptionPaymentEntity } from "./PremiumKeySubscriptionPaymentEntity";

@Entity({ name: "premium_key_subscriptions" })
export class PremiumKeySubscriptionEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'member_id' })
    memberId: string;

    @Column({ name: 'guild_premium_key_id' })
    guildPremiumKeyId: string;

    @Column({ name: 'status' })
    status: PremiumKeySubscriptionStatus;

    @Column({ name: 'method' })
    method: PaymentMethod;

    @Column({ name: 'provider_key' })
    providerKey: string;

    @Column({ name: "confirmed_at", type: "timestamptz", nullable: true })
    confirmedAt?: Date | null;

    @Column({ name: "actived_at", type: "timestamptz", nullable: true })
    activedAt?: Date | null;

    @Column({ name: "expired_at", type: "timestamptz", nullable: true })
    expiredAt?: Date | null;

    @Column({ name: "canceled_at", type: "timestamptz", nullable: true })
    canceledAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.premiumKeySubscriptions)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity>;

    @ManyToOne(() => resolveEntity<GuildPremiumKeyEntity>('GuildPremiumKeyEntity'), (guildPremiumKey) => guildPremiumKey.subscriptions)
    @JoinColumn({ name: "premium_key_id" })
    guildPremiumKey: Promise<GuildPremiumKeyEntity>;

    @OneToMany(() => resolveEntity<PremiumKeySubscriptionPaymentEntity>('PremiumKeySubscriptionPaymentEntity'), (payment) => payment.subscription)
    payments: Promise<PremiumKeySubscriptionPaymentEntity[]>;

    async transform(options?: {
        member?: MemberAttributes | boolean,
    }): Promise<PremiumKeySubscriptionAttributes> {
        options ??= {};
        const guildPremiumKey: GuildPremiumKeyEntity = await this.guildPremiumKey;
        const [
            premiumKey,
            member,
        ] = await Bluebird.all<PremiumKeyAttributes, MemberAttributes | undefined>([
            (async () => await guildPremiumKey.transform())(),
            (async () => options.member == true ? ((await this.member)?.transform()) : options.member || undefined)(),
        ]);
        return {
            id: this.id,
            memberId: this.memberId,
            premiumKeyId: guildPremiumKey.premiumKeyId,
            status: this.status,
            method: this.method,
            providerKey: this.providerKey,
            confirmedAt: this.confirmedAt?.toISOString(),
            activedAt: this.activedAt?.toISOString(),
            expiredAt: this.expiredAt?.toISOString(),
            canceledAt: this.canceledAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            member,
            premiumKey,
        };
    }
}