import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";

export const ScenarioContextType = {
    API: 'API',
    Other: 'OTHER',
} as const;

export type ScenarioContextType = typeof ScenarioContextType[keyof typeof ScenarioContextType];

@Entity({ name: "scenario_contexts" })
export class ScenarioContextEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ type: "text" })
    data: string;

    @Column({ name: "type" })
    type: ScenarioContextType;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;
}