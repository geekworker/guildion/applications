import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";

@Entity({ name: "emojis" })
export class EmojiEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "file_id" })
    fileId: string;

    @Column({ name: "content" })
    content: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.emoji)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;
}