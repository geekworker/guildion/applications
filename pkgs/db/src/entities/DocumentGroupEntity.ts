import { DocumentGroupAttributes, DocumentGroupType, DocumentGroupStatus, DocumentSectionAttributes, DocumentAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { DocumentEntity } from "./DocumentEntity";
import type { DocumentSectionEntity } from "./DocumentSectionEntity";

@Entity({ name: "document_groups" })
export class DocumentGroupEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'ja_name' })
    jaName: string;

    @Column({ name: 'en_name' })
    enName: string;

    @Column({ name: "status" })
    status: DocumentGroupStatus;

    @Column({ name: "type" })
    type: DocumentGroupType;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToMany(() => resolveEntity<DocumentSectionEntity>('DocumentSectionEntity'), (section) => section.group)
    sections: Promise<DocumentSectionEntity[]>;

    @OneToMany(() => resolveEntity<DocumentEntity>('DocumentEntity'), (document) => document.group)
    documents: Promise<DocumentEntity[]>;

    async transform(options?: {
        sections?: boolean | DocumentSectionAttributes[],
        documents?: boolean | DocumentAttributes[],
    }): Promise<DocumentGroupAttributes> {
        options ??= {};
        const [
            sections,
            documents,
        ] = await Bluebird.all<DocumentSectionAttributes[] | undefined, DocumentAttributes[] | undefined>([
            (async () => options.sections == true ? (await Bluebird.all((await this.sections).map(v => v.transform()))) : options.sections || undefined)(),
            (async () => options.documents == true ? (await Bluebird.all((await this.documents).map(v => v.transform()))) : options.documents || undefined)(),
        ]);
        return {
            id: this.id,
            slug: this.slug,
            jaName: this.jaName,
            enName: this.enName,
            status: this.status,
            type: this.type,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            sections,
            documents,
        };
    }
}