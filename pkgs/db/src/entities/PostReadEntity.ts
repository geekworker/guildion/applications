import { PostReadStatus } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { PostEntity } from "./PostEntity";

@Entity({ name: "post_reads" })
export class PostReadEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "post_id" })
    postId: string;

    @Column({ name: "member_id" })
    memberId: string;

    @Column({ name: "room_member_id" })
    roomMemberId: string;

    @Column({ name: "status" })
    status: PostReadStatus;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<PostEntity>('PostEntity'), (post) => post.reads)
    @JoinColumn({ name: "post_id" })
    post: Promise<PostEntity>;
}