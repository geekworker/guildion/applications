import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { GuildEntity } from "./GuildEntity";

@Entity({ name: "guild_storages" })
export class GuildStorageEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "file_id" })
    fileId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.guildStorage)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @OneToOne(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;
}