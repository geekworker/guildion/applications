import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { monotonicFactory } from "ulid";
import { resolveEntity } from "../config/resolveEntity";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { DeviceEntity } from "./DeviceEntity";

const monotonic = monotonicFactory();

@Entity({ name: "device_connections" })
export class DeviceConnectionEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "device_id" })
    deviceId: string;

    @Column({ name: "connection_id" })
    connectionId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<DeviceEntity>('DeviceEntity'), (device) => device.deviceConnections)
    @JoinColumn({ name: "device_id" })
    device: Promise<DeviceEntity>;

    @ManyToOne(() => resolveEntity<ConnectionEntity>('ConnectionEntity'), (connection) => connection.deviceConnections)
    @JoinColumn({ name: "connection_id" })
    connection: Promise<ConnectionEntity>;
}