import { MemberAttributes, RoomMemberStatus, RoomMemberType } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { RoomMemberConnectionEntity } from "./RoomMemberConnectionEntity";
import type { MemberEntity } from "./MemberEntity";
import type { MessageEntity } from "./MessageEntity";
import type { RoomEntity } from "./RoomEntity";
import type { PostEntity } from "./PostEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "room_members" })
export class RoomMemberEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "member_id" })
    memberId: string;

    @Column({ name: "type" })
    type: RoomMemberType;

    @Column({ name: "status" })
    status: RoomMemberStatus;

    @Column({ name: 'is_mute', type: "boolean", nullable: true })
    isMute?: boolean | null;

    @Column({ name: 'is_mute_mentions', type: "boolean", nullable: true })
    isMuteMentions?: boolean | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.roomMembers)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.roomMembers)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity>;

    @OneToMany(() => resolveEntity<RoomMemberConnectionEntity>('RoomMemberConnectionEntity'), (roomMemberConnection) => roomMemberConnection.roomMember)
    roomMemberConnections: Promise<RoomMemberConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<ConnectionEntity>('ConnectionEntity'))
    @JoinTable({
        name: 'room_member_connections',
        joinColumn: {
            name: 'room_member_id',
        },
        inverseJoinColumn: {
            name: 'connection_id',
        },
    })
    connections: Promise<ConnectionEntity[]>;

    async transform(options: {} = {}): Promise<MemberAttributes> {
        const member = await this.member;
        return await member!.transform({
            roomMember: this,
        })
    }

    async activateIfPossible(): Promise<RoomMemberEntity> {
        if (this.status == RoomMemberStatus.DELETED) {
            this.status = RoomMemberStatus.CREATED;
            return this.save();
        } else {
            return this;
        }
    }

    async activateForce(): Promise<RoomMemberEntity> {
        this.status = RoomMemberStatus.CREATED;
        return this.save();
    }

    async delete(): Promise<RoomMemberEntity> {
        if (this.status == RoomMemberStatus.CREATED) {
            this.status = RoomMemberStatus.DELETED;
            return this.save();
        } else {
            return this;
        }
    }

    async ban(): Promise<RoomMemberEntity> {
        this.status = RoomMemberStatus.BANNED;
        return this.save();
    }

    async lock(): Promise<RoomMemberEntity> {
        this.status = RoomMemberStatus.LOCKED;
        return this.save();
    }

    async unlock(): Promise<RoomMemberEntity> {
        if (this.status == RoomMemberStatus.LOCKED || this.status == RoomMemberStatus.BANNED) {
            this.status = RoomMemberStatus.DELETED;
            return this.save();
        } else {
            return this;
        }
    }

    async messageNotificatable(message : MessageEntity): Promise<boolean> {
        const member = await this.member;
        return message.roomId == this.roomId && (
            (!this.isMuteMentions && !this.isMute) || 
            (!this.isMuteMentions && message.hasMention(member)) ||
            !this.isMute
        );
    }

    async postNotificatable(post: PostEntity): Promise<boolean> {
        const member = await this.member;
        return post.roomId == this.roomId && (
            (!this.isMuteMentions && !this.isMute) || 
            (!this.isMuteMentions && post.hasMention(member)) ||
            !this.isMute
        );
    }
}