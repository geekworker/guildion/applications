import { CountryCode, LanguageCode, TimeZoneID, FileAttributes, AccountAttributes, UserStatus, UserAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    OneToOne,
    ManyToMany,
    JoinTable,
} from "typeorm";
import type { FileEntity } from "./FileEntity";
import type { AccountEntity } from "./AccountEntity";
import type { NotificationEntity } from "./NotificationEntity";
import type { AdministratorEntity } from "./AdministratorEntity";
import type { UserConnectionEntity } from "./UserConnectionEntity";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { ReportEntity } from "./ReportEntity";
import type { GuildEntity } from "./GuildEntity";
import type { MemberEntity } from "./MemberEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "users" })
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "profile_id" })
    profileId: string;

    @Column({ name: "status" })
    status: UserStatus;

    @Column({ name: "username" })
    username: string;

    @Column({ name: "display_name" })
    displayName: string;

    @Column({ type: "text" })
    description: string;

    @Column({ name: "language_code" })
    languageCode: LanguageCode;

    @Column({ name: "country_code" })
    countryCode: CountryCode;

    @Column({ name: "timezone" })
    timezone: TimeZoneID;

    @Column({ name: "deleted_at", type: 'timestamptz', nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<AccountEntity>('AccountEntity'), (account) => account.user)
    account: Promise<AccountEntity>;

    @OneToOne(() => resolveEntity<AdministratorEntity>('AdministratorEntity'), (administrator) => administrator.user)
    administrator: Promise<AdministratorEntity | undefined>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.users)
    @JoinColumn({ name: "profile_id" })
    profile: Promise<FileEntity>;

    @OneToMany(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.holder)
    files: Promise<FileEntity[]>;

    @OneToMany(() => resolveEntity<NotificationEntity>('NotificationEntity'), (notification) => notification.receiver)
    notifications: Promise<NotificationEntity[]>;

    @OneToMany(() => resolveEntity<ReportEntity>('ReportEntity'), (report) => report.sender)
    reports: Promise<ReportEntity[]>;

    @OneToMany(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.owner)
    guilds: Promise<GuildEntity[]>;

    @OneToMany(() => resolveEntity<UserConnectionEntity>('UserConnectionEntity'), (userConnection) => userConnection.user)
    userConnections: Promise<UserConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<ConnectionEntity>('ConnectionEntity'))
    @JoinTable({
        name: 'user_connections',
        joinColumn: {
            name: 'user_id',
        },
        inverseJoinColumn: {
            name: 'connection_id',
        },
    })
    connections: Promise<ConnectionEntity[]>;

    @OneToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.user)
    members: Promise<MemberEntity[]>;

    async transform(options: {
        profile?: boolean | FileAttributes,
        account?: boolean | AccountAttributes,
        isOnline?: boolean,
        isConnecting?: boolean,
    } = {}): Promise<UserAttributes> {
        return {
            id: this.id,
            profileId: this.profileId,
            status: this.status,
            username: this.username,
            displayName: this.displayName,
            description: this.description,
            languageCode: this.languageCode,
            countryCode: this.countryCode,
            timezone: this.timezone,
            isOnline: options.isOnline,
            isConnecting: options.isConnecting,
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            profile: await (await this.profile).transform(),
            account: options.account == true ? (await (await this.account).transform()) : options.account || undefined,
            administrator: undefined,
        };
    }
}