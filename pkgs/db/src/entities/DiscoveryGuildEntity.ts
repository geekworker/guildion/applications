import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { monotonicFactory } from "ulid";
import type { GuildEntity } from "./GuildEntity";
import type { DiscoveryEntity } from "./DiscoveryEntity";
import { resolveEntity } from "../config/resolveEntity";

const monotonic = monotonicFactory();

@Entity({ name: "discovery_guilds" })
export class DiscoveryGuildEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "discovery_id" })
    discoveryId: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<DiscoveryEntity>('DiscoveryEntity'), (discovery) => discovery.discoveryGuilds)
    @JoinColumn({ name: "discovery_id" })
    discovery: Promise<DiscoveryEntity>;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.discoveryGuilds)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;
}