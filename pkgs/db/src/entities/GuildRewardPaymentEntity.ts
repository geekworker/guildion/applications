import { RewardMethod, GuildRewardAttributes, GuildRewardPaymentAttributes, GuildRewardPaymentStatus } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildRewardEntity } from "./GuildRewardEntity";

@Entity({ name: "guild_reward_payments" })
export class GuildRewardPaymentEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'reward_id' })
    rewardId: string;

    @Column({ name: 'status' })
    status: GuildRewardPaymentStatus;

    @Column({ name: 'method' })
    method: RewardMethod;

    @Column({ name: 'provider_key' })
    providerKey: string;

    @Column({ name: "requested_at", type: "timestamptz", nullable: true })
    requestedAt?: Date | null;

    @Column({ name: "pay_at", type: "timestamptz", nullable: true })
    payAt?: Date | null;

    @Column({ name: "paid_at", type: "timestamptz", nullable: true })
    paidAt?: Date | null;

    @Column({ name: "canceled_at", type: "timestamptz", nullable: true })
    canceledAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildRewardEntity>('GuildRewardEntity'), (reward) => reward.payments)
    @JoinColumn({ name: "reward_id" })
    reward: Promise<GuildRewardEntity>;

    async transform(options?: {
        reward?: GuildRewardAttributes | boolean,
    }): Promise<GuildRewardPaymentAttributes> {
        options ??= {};
        const [
            reward,
        ] = await Bluebird.all<GuildRewardAttributes | undefined>([
            (async () => options.reward == true ? ((await this.reward)?.transform()) : options.reward || undefined)(),
        ]);
        return {
            id: this.id,
            rewardId: this.rewardId,
            status: this.status,
            method: this.method,
            providerKey: this.providerKey,
            requestedAt: this.requestedAt?.toISOString(),
            payAt: this.payAt?.toISOString(),
            paidAt: this.paidAt?.toISOString(),
            canceledAt: this.canceledAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            reward,
        };
    }
}