import { RoomMemberConnectionStatus } from "@guildion/core";
import { RoomEntity } from "./RoomEntity";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { monotonicFactory } from "ulid";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { RoomMemberEntity } from "./RoomMemberEntity";
import { resolveEntity } from "../config/resolveEntity";

const monotonic = monotonicFactory();

@Entity({ name: "room_member_connections" })
export class RoomMemberConnectionEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "room_member_id" })
    roomMemberId: string;

    @Column({ name: "connection_id" })
    connectionId: string;

    @Column({ name: "is_connecting" })
    isConnecting: boolean;

    @Column({ name: "status" })
    status: RoomMemberConnectionStatus;

    @Column({ name: "connected_at", type: 'timestamptz', nullable: true })
    connectedAt?: Date | null;

    @Column({ name: "disconnected_at", type: 'timestamptz', nullable: true })
    disconnectedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomMemberEntity>('RoomMemberEntity'), (roomMember) => roomMember.roomMemberConnections)
    @JoinColumn({ name: "room_member_id" })
    roomMember: Promise<RoomMemberEntity>;

    @ManyToOne(() => resolveEntity<ConnectionEntity>('ConnectionEntity'), (connection) => connection.roomMemberConnections)
    @JoinColumn({ name: "connection_id" })
    connection: Promise<ConnectionEntity>;

    public get room(): Promise<RoomEntity> {
        return (async () => {
            const roomMember = await this.roomMember;
            return roomMember.room
        })()
    }

    public async disconnect(): Promise<RoomMemberConnectionEntity> {
        if (!this.isConnecting || this.disconnectedAt) return this;
        this.isConnecting = false;
        this.disconnectedAt = new Date();
        this.status = RoomMemberConnectionStatus.DISCONNECTED;
        const room = await this.room;
        const result = await this.save();
        await room.activate();
        return result;
    }
}