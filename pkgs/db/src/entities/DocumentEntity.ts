import { DocumentAttributes, DocumentType, DocumentDataType, DocumentStatus, DocumentGroupAttributes, DocumentSectionAttributes, FileAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { DocumentGroupEntity } from "./DocumentGroupEntity";
import type { DocumentSectionEntity } from "./DocumentSectionEntity";
import type { FileEntity } from "./FileEntity";

@Entity({ name: "documents" })
export class DocumentEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "section_id", type: "uuid" })
    sectionId?: string;

    @Column({ name: "group_id", type: "uuid" })
    groupId: string;

    @Column({ name: "en_thumbnail_id", type: "uuid", nullable: true })
    enThumbnailId?: string | null;

    @Column({ name: "ja_thumbnail_id", type: "uuid", nullable: true })
    jaThumbnailId?: string | null;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'ja_title' })
    jaTitle: string;

    @Column({ name: 'en_title' })
    enTitle: string;

    @Column({ name: 'ja_description' })
    jaDescription: string;

    @Column({ name: 'en_description' })
    enDescription: string;

    @Column({ name: 'ja_data' })
    jaData: string;

    @Column({ name: 'en_data' })
    enData: string;

    @Column({ name: "status" })
    status: DocumentStatus;

    @Column({ name: "type" })
    type: DocumentType;

    @Column({ name: "data_type" })
    dataType: DocumentDataType;

    @Column({ name: "index" })
    index: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.enDocuments)
    @JoinColumn({ name: "en_thumbnail_id" })
    enThumbnail: Promise<FileEntity | undefined>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.jaDocuments)
    @JoinColumn({ name: "en_thumbnail_id" })
    jaThumbnail: Promise<FileEntity | undefined>;

    @ManyToOne(() => resolveEntity<DocumentSectionEntity>('DocumentSectionEntity'), (section) => section.documents)
    @JoinColumn({ name: "section_id" })
    section: Promise<DocumentSectionEntity>;

    @ManyToOne(() => resolveEntity<DocumentGroupEntity>('DocumentGroupEntity'), (group) => group.documents)
    @JoinColumn({ name: "group_id" })
    group: Promise<DocumentGroupEntity>;

    next = (): Promise<DocumentEntity | undefined> => new Promise((resolve, reject) => {
        resolve(
            DocumentEntity.findOne({
                where: {
                    groupId: this.groupId,
                    sectionId: this.sectionId,
                    index: this.index + 1,
                    status: DocumentStatus.PUBLISH,
                },
            })
        );
    });

    prev = (): Promise<DocumentEntity | undefined> => new Promise((resolve, reject) => {
        resolve(
            DocumentEntity.findOne({
                where: {
                    groupId: this.groupId,
                    sectionId: this.sectionId,
                    index: this.index - 1,
                    status: DocumentStatus.PUBLISH,
                },
            })
        );
    });

    async transform(options?: {
        section?: boolean | DocumentSectionAttributes,
        group?: boolean | DocumentGroupAttributes,
        next?: boolean | DocumentAttributes,
        prev?: boolean | DocumentAttributes,
    }): Promise<DocumentAttributes> {
        options ??= {};
        const [
            section,
            group,
            enThumbnail,
            jaThumbnail,
        ] = await Bluebird.all<DocumentSectionAttributes | undefined, DocumentGroupAttributes | undefined, FileAttributes | undefined, FileAttributes | undefined>([
            (async () => options.section == true ? ((await this.section)?.transform()) : options.section || undefined)(),
            (async () => options.group == true ? ((await this.group)?.transform()) : options.group || undefined)(),
            (async () => (await this.enThumbnail)?.transform())(),
            (async () => (await this.jaThumbnail)?.transform())(),
        ]);

        const [
            prev,
            next,
        ] = await Bluebird.all<DocumentAttributes | undefined, DocumentAttributes | undefined>([
            (async () => options.next == true ? ((await this.next())?.transform()) : options.next || undefined)(),
            (async () => options.prev == true ? ((await this.prev())?.transform()) : options.prev || undefined)(),
        ]);
        return {
            id: this.id,
            sectionId: this.sectionId,
            groupId: this.groupId,
            jaThumbnailId: this.jaThumbnailId,
            enThumbnailId: this.enThumbnailId,
            slug: this.slug,
            jaTitle: this.jaTitle,
            enTitle: this.enTitle,
            jaDescription: this.jaDescription,
            enDescription: this.enDescription,
            jaData: this.jaData,
            enData: this.enData,
            status: this.status,
            type: this.type,
            dataType: this.dataType,
            index: this.index,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            section,
            group,
            prev,
            next,
            enThumbnail,
            jaThumbnail,
        };
    }
}