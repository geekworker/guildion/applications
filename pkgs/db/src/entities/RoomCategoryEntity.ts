import { GuildAttributes, RoomAttributes, RoomCategoryAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
    ManyToMany,
    JoinTable,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildEntity } from "./GuildEntity";
import type { RoomCategoryListEntity } from "./RoomCategoryListEntity";
import type { RoomEntity } from "./RoomEntity";

@Entity({ name: "room_categories" })
export class RoomCategoryEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'guild_id' })
    guildId: string;

    @Column({ name: 'name' })
    name: string;

    @Column({ type: 'text' })
    description: string;

    @Column({ name: 'index' })
    index: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.roomCategories)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @OneToMany(() => resolveEntity<RoomCategoryListEntity>('RoomCategoryListEntity'), (list) => list.category)
    lists: Promise<RoomCategoryListEntity[]>;

    @ManyToMany(() => resolveEntity<RoomEntity>('RoomEntity'))
    @JoinTable({
        name: 'room_category_lists',
        joinColumn: {
            name: 'category_id',
        },
        inverseJoinColumn: {
            name: 'room_id',
        },
    })
    rooms: Promise<RoomEntity[]>;
    cacheRooms: RoomEntity[];

    async transform(
        options: {
            rooms?: boolean | RoomAttributes[],
            guild?: boolean | GuildAttributes,
        } = {}
    ): Promise<RoomCategoryAttributes> {
        return {
            id: this.id,
            guildId: this.guildId,
            name: this.name,
            description: this.description,
            index: this.index,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            guild: options.guild == true ? (await (await this.guild).transform()) : options.guild || undefined,
            rooms: options.rooms == true ? (await Promise.all((await this.rooms).map(room => room.transform()))) : options.rooms || undefined,
        };
    }
}