import { BlogAttributes, BlogType, BlogDataType, BlogStatus, BlogCategoryAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { BlogCategoryEntity } from "./BlogCategoryEntity";

@Entity({ name: "blogs" })
export class BlogEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "category_id", type: "uuid" })
    categoryId: string;

    @Column({ name: "en_thumbnail_url" })
    enThumbnailURL?: string;

    @Column({ name: "ja_thumbnail_url" })
    jaThumbnailURL?: string;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'ja_title' })
    jaTitle: string;

    @Column({ name: 'en_title' })
    enTitle: string;

    @Column({ name: 'ja_description' })
    jaDescription: string;

    @Column({ name: 'en_description' })
    enDescription: string;

    @Column({ name: 'ja_data' })
    jaData: string;

    @Column({ name: 'en_data' })
    enData: string;

    @Column({ name: "status" })
    status: BlogStatus;

    @Column({ name: "type" })
    type: BlogType;

    @Column({ name: "data_type" })
    dataType: BlogDataType;

    @Column({ name: "index" })
    index: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<BlogCategoryEntity>('BlogCategoryEntity'), (category) => category.blogs)
    @JoinColumn({ name: "category_id" })
    category: Promise<BlogCategoryEntity>;

    next = (): Promise<BlogEntity | undefined> => new Promise((resolve, reject) => {
        resolve(
            BlogEntity.findOne({
                where: {
                    categoryId: this.categoryId,
                    index: this.index + 1,
                    status: BlogStatus.PUBLISH,
                },
            })
        );
    });

    prev = (): Promise<BlogEntity | undefined> => new Promise((resolve, reject) => {
        resolve(
            BlogEntity.findOne({
                where: {
                    categoryId: this.categoryId,
                    index: this.index - 1,
                    status: BlogStatus.PUBLISH,
                },
            })
        );
    });

    async transform(options?: {
        category?: boolean | BlogCategoryAttributes,
        next?: boolean | BlogAttributes,
        prev?: boolean | BlogAttributes,
    }): Promise<BlogAttributes> {
        options ??= {};
        const [
            category,
            prev,
            next,
        ] = await Bluebird.all<BlogCategoryAttributes | undefined, BlogAttributes | undefined, BlogAttributes | undefined>([
            (async () => options.category == true ? ((await this.category)?.transform({ parent: true })) : options.category || undefined)(),
            (async () => options.next == true ? ((await this.next())?.transform()) : options.next || undefined)(),
            (async () => options.prev == true ? ((await this.prev())?.transform()) : options.prev || undefined)(),
        ]);

        return {
            id: this.id,
            categoryId: this.categoryId,
            jaThumbnailURL: this.jaThumbnailURL,
            enThumbnailURL: this.enThumbnailURL,
            slug: this.slug,
            jaTitle: this.jaTitle,
            enTitle: this.enTitle,
            jaDescription: this.jaDescription,
            enDescription: this.enDescription,
            jaData: this.jaData,
            enData: this.enData,
            status: this.status,
            type: this.type,
            dataType: this.dataType,
            index: this.index,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            category,
            prev,
            next,
        };
    }
}