import { MemberAttributes, PaymentMethod, PremiumKeyAttributes, PremiumKeySubscriptionAttributes, PremiumKeySubscriptionPaymentAttributes, PremiumKeySubscriptionPaymentStatus } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { PremiumKeySubscriptionEntity } from "./PremiumKeySubscriptionEntity";

@Entity({ name: "premium_key_subscription_payments" })
export class PremiumKeySubscriptionPaymentEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'subscription_id' })
    subscriptionId: string;

    @Column({ name: 'status' })
    status: PremiumKeySubscriptionPaymentStatus;

    @Column({ name: 'method' })
    method: PaymentMethod;

    @Column({ name: 'provider_key' })
    providerKey: string;

    @Column({ name: "requested_at", type: "timestamptz", nullable: true })
    requestedAt?: Date | null;

    @Column({ name: "paid_at", type: "timestamptz", nullable: true })
    paidAt?: Date | null;

    @Column({ name: "canceled_at", type: "timestamptz", nullable: true })
    canceledAt?: Date | null;

    @Column({ name: "refunded_at", type: "timestamptz", nullable: true })
    refundedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<PremiumKeySubscriptionEntity>('PremiumKeySubscriptionEntity'), (subscription) => subscription.payments)
    @JoinColumn({ name: "subscription_id" })
    subscription: Promise<PremiumKeySubscriptionEntity>;

    async transform(options?: {
        subscription?: PremiumKeySubscriptionAttributes | boolean,
    }): Promise<PremiumKeySubscriptionPaymentAttributes> {
        options ??= {};
        const [
            subscription,
        ] = await Bluebird.all<PremiumKeySubscriptionAttributes | undefined>([
            (async () => options.subscription == true ? ((await this.subscription)?.transform()) : options.subscription || undefined)(),
        ]);
        return {
            id: this.id,
            subscriptionId: this.subscriptionId,
            status: this.status,
            method: this.method,
            providerKey: this.providerKey,
            requestedAt: this.requestedAt?.toISOString(),
            paidAt: this.paidAt?.toISOString(),
            canceledAt: this.canceledAt?.toISOString(),
            refundedAt: this.refundedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            subscription,
        };
    }
}