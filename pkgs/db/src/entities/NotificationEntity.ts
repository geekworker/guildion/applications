import { NotificationAttributes, NotificationStatus, NotificationType, RoomLogAttributes, UserAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    ManyToOne,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { UserEntity } from "./UserEntity";

@Entity({ name: "notifications" })
export class NotificationEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "receiver_id" })
    receiverId: string;

    @Column({ name: "status" })
    status: NotificationStatus;

    @Column({ name: "type" })
    type: NotificationType;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<UserEntity>('UserEntity'), (receiver) => receiver.notifications)
    @JoinColumn({ name: "receiver_id" })
    receiver: Promise<UserEntity>;

    async transform(
        options: {
            receiver?: boolean | UserAttributes,
            roomLog?: RoomLogAttributes,
        } = {}
    ): Promise<NotificationAttributes> {
        return {
            id: this.id,
            receiverId: this.receiverId,
            roomLogId: options.roomLog?.id,
            type: this.type,
            status: this.status,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            roomLog: options.roomLog,
            receiver: options.receiver == true ? (await (await this.receiver)?.transform()) : options.receiver || undefined,
        };
    }
}