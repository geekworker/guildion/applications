import { AdministratorAttributes, AdministratorRole, UserAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { UserEntity } from "./UserEntity";

@Entity({ name: "administrators" })
export class AdministratorEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "user_id" })
    userId: string;

    @Column({ name: "role" })
    role: AdministratorRole;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<UserEntity>('UserEntity'), (user) => user.administrator)
    @JoinColumn({ name: "user_id" })
    user: Promise<UserEntity>;

    async transform(
        options: {
            user?: boolean | UserAttributes,
        } = {}
    ): Promise<AdministratorAttributes> {
        return {
            id: this.id,
            userId: this.userId,
            role: this.role,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            user: options.user == true ? (await (await this.user)?.transform()) : options.user || undefined,
        };
    }
}