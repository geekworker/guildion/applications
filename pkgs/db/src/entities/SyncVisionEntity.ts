import { FileAttributes, SyncVisionStatus, RoomAttributes, SyncVisionAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from "typeorm";
import type { FilingEntity } from "./FilingEntity";
import type { RoomFileEntity } from "./RoomFileEntity";
import type { FileEntity } from "./FileEntity";
import type { RoomActivityEntity } from "./RoomActivityEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomLogEntity } from "./RoomLogEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "sync_visions" })
export class SyncVisionEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "file_id" })
    fileId: string;

    @Column({ name: "filingId", type: "uuid", nullable: true })
    filingId?: string | null;

    @Column({ name: "status" })
    status: SyncVisionStatus;

    @Column({ name: "offset_duration_ms" })
    offsetDurationMs: number;

    @Column({ name: "connected_at", type: 'timestamptz', nullable: true })
    connectedAt?: Date | null;

    @Column({ name: "disconnected_at", type: 'timestamptz', nullable: true })
    disconnectedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.syncVisions)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.syncVisions)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;

    @ManyToOne(() => resolveEntity<FilingEntity>('FilingEntity'), (filing) => filing.syncVisions)
    @JoinColumn({ name: "filing_id" })
    filing: Promise<FilingEntity | undefined>;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.syncVision)
    logs: Promise<RoomLogEntity[]>;

    @OneToMany(() => resolveEntity<RoomActivityEntity>('RoomActivityEntity'), (activity) => activity.syncVision)
    activities: Promise<RoomActivityEntity[]>;

    async transform(options: {
        file?: boolean | FileAttributes,
        filing?: boolean | FilingEntity,
        room?: boolean | RoomAttributes,
        roomFile?: RoomFileEntity,
    } = {}): Promise<SyncVisionAttributes> {
        const filing = options.filing == true ? (await this.filing) : options.filing || undefined;
        const [
            file,
            room,
        ] = await Bluebird.all<FileAttributes | undefined, RoomAttributes | undefined>([
            (async () => options.file == true ? (await (await this.file)?.transform({ filing, roomFile: options.roomFile })) : options.file || undefined)(),
            (async () => options.room == true ? (await (await this.room)?.transform()) : options.room || undefined)(),
        ]);
        return {
            id: this.id,
            fileId: this.fileId,
            roomId: this.roomId,
            status: this.status,
            offsetDurationMs: this.offsetDurationMs,
            connectedAt: this.connectedAt?.toISOString(),
            disconnectedAt: this.disconnectedAt?.toISOString(),
            createdAt: this.createdAt.toISOString(),
            updatedAt: this.updatedAt.toISOString(),
            file,
            room,
        }
    }
}