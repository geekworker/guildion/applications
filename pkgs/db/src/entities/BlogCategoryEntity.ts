import { BlogAttributes, BlogCategoryAttributes, BlogCategoryStatus } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import type { BlogEntity } from "./BlogEntity";

@Entity({ name: "blog_categories" })
export class BlogCategoryEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "parent_id", type: "uuid", nullable: true })
    parentId?: string | null;

    @Column({ name: 'ja_name' })
    jaName: string;

    @Column({ name: 'en_name' })
    enName: string;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'index' })
    index: number;

    @Column({ name: "status" })
    status: BlogCategoryStatus;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => BlogCategoryEntity, (parent) => parent.children)
    @JoinColumn({ name: "parent_id" })
    parent: Promise<BlogCategoryEntity>;

    @OneToMany(() => BlogCategoryEntity, (child) => child.parent)
    children: Promise<BlogCategoryEntity[]>;

    blogs: Promise<BlogEntity[]>;

    async transform(
        options: {
            blogs?: boolean | BlogAttributes[],
            parent?: boolean | BlogCategoryAttributes,
            children?: boolean | BlogCategoryAttributes[],
        } = {}
    ): Promise<BlogCategoryAttributes> {
        return {
            id: this.id,
            parentId: this.parentId,
            jaName: this.jaName,
            enName: this.enName,
            slug: this.slug,
            index: this.index,
            status: this.status,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            blogs: options.blogs == true ? (await Promise.all((await this.blogs).map(blog => blog.transform()))) : options.blogs || undefined,
            parent: options.parent == true ? (await (await this.parent)?.transform({ parent: true })) : options.parent || undefined,
            children: options.children == true ? (await Promise.all((await this.children).map(child => child.transform()))) : options.children || undefined,
        };
    }
}