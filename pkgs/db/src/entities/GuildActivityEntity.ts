import { GuildActivityAttributes, GuildAttributes, MemberAttributes, RoomStatus, RoomType, MemberStatus, PostStatus, PostType } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import type { GuildEntity } from "./GuildEntity";
import type { PostEntity } from "./PostEntity";
import type { RoomEntity } from "./RoomEntity";
import type { MemberEntity } from "./MemberEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "guild_activities" })
export class GuildActivityEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "connects_count" })
    connectsCount: number;

    @Column({ name: "members_count" })
    membersCount: number;

    @Column({ name: "rooms_count" })
    roomsCount: number;

    @Column({ name: "public_rooms_count" })
    publicRoomsCount: number;

    @Column({ name: "posts_count" })
    postsCount: number;

    @Column({ name: "public_posts_count" })
    publicPostsCount: number;

    @Column({ name: "content_length" })
    contentLength: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.activities)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    public static async getRoomCounts(guildId: string): Promise<number> {
        const count = await resolveEntity<RoomEntity>('RoomEntity').count({
            where: {
                guildId,
                status: RoomStatus.CREATED,
            },
        });
        return count;
    }

    public static async getPostCounts(guildId: string): Promise<number> {
        const count = await resolveEntity<PostEntity>('PostEntity')
            .getRepository()
            .createQueryBuilder('posts')
            .innerJoinAndSelect(
                'posts.room',
                'room',
                `room.guild_id = :guildId AND
                room.status = :status`, {
                    guildId,
                    status: RoomStatus.CREATED,
                }
            )
            .andWhere('posts.status = :status', { status: PostStatus.CREATED })
            .getCount()
        return count;
    }

    public static async getPublicPostCounts(guildId: string): Promise<number> {
        const count = await resolveEntity<PostEntity>('PostEntity')
            .getRepository()
            .createQueryBuilder('posts')
            .innerJoinAndSelect(
                'posts.room',
                'room',
                `room.guild_id = :guildId AND
                room.type IN (:...types) AND
                room.status = :status`, {
                    guildId,
                    status: RoomStatus.CREATED,
                    types: [RoomType.PUBLIC, RoomType.OFFICIAL]
                }
            )
            .andWhere('posts.status = :status', { status: PostStatus.CREATED })
            .andWhere('posts.type IN (:...types)', { types: [PostType.PUBLIC] })
            .getCount()
        return count;
    }

    public static async getPublicRoomCounts(guildId: string): Promise<number> {
        const count = await resolveEntity<RoomEntity>('RoomEntity')
            .getRepository()
            .createQueryBuilder('rooms')
            .where('rooms.guild_id = :guildId', { guildId })
            .andWhere('rooms.status = :status', { status: RoomStatus.CREATED })
            .andWhere('rooms.type IN (:...types)', { types: [RoomType.PUBLIC, RoomType.OFFICIAL]})
            .getCount()
        return count;
    }

    public static async getMemberCounts(guildId: string): Promise<number> {
        const count = await resolveEntity<MemberEntity>('MemberEntity').count({
            where: {
                guildId,
                status: MemberStatus.CREATED,
            },
        });
        return count;
    }

    public static async getConnectingCounts(guildId: string): Promise<number> {
        const count = await resolveEntity<MemberEntity>('MemberEntity')
            .getRepository()
            .createQueryBuilder('members')
            .innerJoinAndSelect(
                'members.memberConnections',
                'member_connections',
                `member_connections.is_connecting = :isConnecting`, { isConnecting: true }
            )
            .where('members.guild_id = :guildId', { guildId: guildId })
            .andWhere("members.status NOT IN (:...statuses)", { statuses: [MemberStatus.BANNED, MemberStatus.LOCKED, MemberStatus.DELETED] })
            .getCount()
        return count;
    }

    public async getConnectingMembers(options?: { count?: number, offset?: number }): Promise<MemberEntity[]> {
        const { count, offset } = options ? options : { count: undefined, offset: undefined };
        const members = await resolveEntity<MemberEntity>('MemberEntity')
            .getRepository()
            .createQueryBuilder('members')
            .distinctOn(['member_connections.connection_id'])
            .innerJoinAndSelect(
                'members.memberConnections',
                'member_connections',
                `member_connections.connected_at <= :activedAt AND (member_connections.disconnected_at >= :activedAt OR member_connections.is_connecting = :isConnecting)`, { activedAt: this.createdAt.toISOString(), isConnecting: true }
            )
            .where('members.guild_id = :guildId', { guildId: this.guildId })
            .andWhere("members.status NOT IN (:...statuses)", { statuses: [MemberStatus.BANNED, MemberStatus.LOCKED, MemberStatus.DELETED] })
            .orderBy("member_connections.connection_id", 'DESC')
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .getMany()
        return members;
    }

    public async getNotificationsCount(currentMemberId: string): Promise<number> {
        return 0;
    }

    async transform(options: {
        guild?: boolean | GuildAttributes,
        connectingMembers?: boolean | MemberAttributes[],
        currentMember?: MemberEntity | MemberAttributes,
        notificationsCount?: boolean | number,
    } = {}): Promise<GuildActivityAttributes> {
        const [
            guild,
            connectingMembers,
            notificationsCount,
        ] = await Bluebird.all<GuildAttributes | undefined, MemberAttributes[] | undefined, number | undefined>([
            (async () => options.guild == true ? await (await this.guild).transform() : options.guild || undefined)(),
            (async () => options.connectingMembers == true ? await Bluebird.all((await this.getConnectingMembers()).map(v => v.transform())) : options.connectingMembers || undefined)(),
            (async () => options.notificationsCount == true && !!options.currentMember?.id ? await this.getNotificationsCount(options.currentMember.id) : options.notificationsCount == true ? 0 : options.notificationsCount || undefined)(),
        ]);

        return {
            id: this.id,
            guildId: this.guildId,
            connectsCount: this.connectsCount,
            roomsCount: this.roomsCount,
            publicRoomsCount: this.publicRoomsCount,
            membersCount: this.membersCount,
            postsCount: this.postsCount,
            publicPostsCount: this.publicPostsCount,
            contentLength: this.contentLength,
            createdAt: this.createdAt?.toString(),
            updatedAt: this.updatedAt?.toString(),
            guild,
            connectingMembers,
            notificationsCount,
        };
    }

    public static async activate(guildId: string): Promise<GuildActivityEntity> {
        return await GuildActivityEntity.create({
            guildId,
            connectsCount: await GuildActivityEntity.getConnectingCounts(guildId),
            roomsCount: await this.getRoomCounts(guildId),
            publicRoomsCount: await this.getPublicRoomCounts(guildId),
            postsCount: await this.getPostCounts(guildId),
            publicPostsCount: await this.getPublicPostCounts(guildId),
            membersCount: await this.getMemberCounts(guildId),
        }).save();
    }
}