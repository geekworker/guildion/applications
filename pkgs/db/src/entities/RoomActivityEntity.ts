import { MemberAttributes, PostStatus, PostType, RoomActivityAttributes, RoomAttributes, RoomLogAttributes, RoomMemberStatus, SyncVisionAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { MemberEntity } from "./MemberEntity";
import type { PostEntity } from "./PostEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomLogEntity } from "./RoomLogEntity";
import type { RoomMemberEntity } from "./RoomMemberEntity";
import type { SyncVisionEntity } from "./SyncVisionEntity";

@Entity({ name: "room_activities" })
export class RoomActivityEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "log_id", type: "uuid", nullable: true })
    logId?: string | null;

    @Column({ name: "sync_vision_id", type: "uuid", nullable: true })
    syncVisionId?: string | null;

    @Column({ name: "connects_count" })
    connectsCount: number;

    @Column({ name: "members_count" })
    membersCount: number;

    @Column({ name: "posts_count" })
    postsCount: number;

    @Column({ name: "public_posts_count" })
    publicPostsCount: number;
    
    @Column({ name: "content_length" })
    contentLength: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.activities)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.activities)
    @JoinColumn({ name: "log_id" })
    log: Promise<RoomLogEntity | undefined>;

    @ManyToOne(() => resolveEntity<SyncVisionEntity>('SyncVisionEntity'), (syncVision) => syncVision.activities)
    @JoinColumn({ name: "sync_vision_id" })
    syncVision: Promise<SyncVisionEntity | undefined>;

    public static async getLastLog(roomId: string): Promise<RoomLogEntity | undefined> {
        const logs = await resolveEntity<RoomLogEntity>('RoomLogEntity').find({
            where: {
                roomId,
            },
            order: { createdAt: 'DESC' },
            take: 1,
        });
        const lastLog = logs.length > 0 ? logs[0] : undefined;
        return lastLog;
    }

    public static async getCurrentSyncVision(roomId: string): Promise<SyncVisionEntity | undefined> {
        return undefined;
    }

    public static async getConnectingCounts(roomId: string): Promise<number> {
        const count = await resolveEntity<RoomMemberEntity>('RoomMemberEntity')
            .getRepository()
            .createQueryBuilder('room_members')
            .innerJoin('room_members.roomMemberConnections', 'room_member_connections', 'room_member_connections.is_connecting = :isConnecting', { isConnecting: true })
            .where('room_members.room_id = :roomId', { roomId })
            .andWhere('room_members.status = :status', { status: RoomMemberStatus.CREATED })
            .getCount();
        return count;
    }

    async getConnectingMembers(options?: { count?: number, offset?: number }): Promise<RoomMemberEntity[]> {
        const { count, offset } = options ? options : { count: undefined, offset: undefined };
        const roomMembers = await resolveEntity<RoomMemberEntity>('RoomMemberEntity')
            .getRepository()
            .createQueryBuilder('room_members')
            .distinctOn(['room_member_connections.connection_id'])
            .innerJoinAndSelect(
                'room_members.roomMemberConnections',
                'room_member_connections',
                `room_member_connections.connected_at <= :activedAt AND 
                (
                    room_member_connections.disconnected_at >= :activedAt OR
                    room_member_connections.is_connecting = :isConnecting
                )`, { activedAt: this.createdAt.toISOString(), isConnecting: true }
            )
            .where('room_members.room_id = :roomId', { roomId: this.roomId })
            .andWhere('room_members.status = :status', { status: RoomMemberStatus.CREATED })
            .orderBy("room_member_connections.connection_id", 'DESC')
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .getMany();
        return roomMembers;
    }

    public static async getMembersCounts(roomId: string): Promise<number> {
        const count = await resolveEntity<RoomMemberEntity>('RoomMemberEntity')
            .getRepository()
            .createQueryBuilder('room_members')
            .where('room_members.room_id = :roomId', { roomId })
            .andWhere('room_members.status = :status', { status: RoomMemberStatus.CREATED })
            .getCount();
        return count;
    }

    public static async getPostCounts(roomId: string): Promise<number> {
        const count = await resolveEntity<PostEntity>('PostEntity').count({
            where: {
                roomId,
                status: PostStatus.CREATED,
            },
        });
        return count;
    }

    public static async getPublicPostCounts(roomId: string): Promise<number> {
        const count = await resolveEntity<PostEntity>('PostEntity').count({
            where: {
                roomId,
                status: PostStatus.CREATED,
                type: PostType.PUBLIC,
            },
        });
        return count;
    }

    async getMessageNotificationsCount({ currentMemberId, isNotified }: { isNotified?: boolean, currentMemberId?: string }): Promise<number> {
        return 0;
    }

    async getPostNotificationsCount({ currentMemberId, isNotified }: { isNotified?: boolean, currentMemberId?: string }): Promise<number> {
        return 0;
    }

    async getNotificationsCount(currentMemberId?: string): Promise<number> {
        return 0;
    }

    public async getCurrentSyncVision(): Promise<SyncVisionEntity | undefined> {
        return undefined;
    }

    public static async activate(roomId: string): Promise<RoomActivityEntity> {
        return await RoomActivityEntity.create({
            roomId,
            logId: (await RoomActivityEntity.getLastLog(roomId))?.id,
            syncVisionId: (await RoomActivityEntity.getCurrentSyncVision(roomId))?.id,
            connectsCount: await RoomActivityEntity.getConnectingCounts(roomId),
            membersCount: await RoomActivityEntity.getMembersCounts(roomId),
        }).save();
    }

    async transform(options: {
        room?: boolean | RoomAttributes,
        log?: boolean | RoomLogAttributes,
        syncVision?: boolean | SyncVisionAttributes,
        connectingMembers?: boolean | MemberAttributes[],
        currentMember?: MemberEntity | MemberAttributes,
        messageNotificationsCount?: boolean | number,
        postNotificationsCount?: boolean | number,
        notificationsCount?: boolean | number,
    } = {}): Promise<RoomActivityAttributes> {
        const [
            room,
            log,
            syncVision,
            connectingMembers,
        ] = await Bluebird.all<RoomAttributes | undefined, RoomLogAttributes | undefined, SyncVisionAttributes | undefined, MemberAttributes[] | undefined>([
            (async () => options.room == true ? await (await this.room).transform() : options.room || undefined)(),
            (async () => options.log == true ? await (await this.log)?.transform() : options.log || undefined)(),
            (async () => options.syncVision == true ? await (await this.getCurrentSyncVision())?.transform() : options.syncVision || undefined)(),
            (async () => options.connectingMembers == true ? await Bluebird.all((await this.getConnectingMembers()).map(v => v.transform())) : options.connectingMembers || undefined)(),
        ]);

        const [
            notificationsCount,
            messageNotificationsCount,
            postNotificationsCount,
        ] = await Bluebird.all<number | undefined>([
            (async () => options.notificationsCount == true && !!options.currentMember?.id ? await this.getNotificationsCount(options.currentMember.id) : options.notificationsCount == true ? 0 : options.notificationsCount || undefined)(),
            (async () => options.messageNotificationsCount == true && !!options.currentMember?.id ? await this.getMessageNotificationsCount({ currentMemberId: options.currentMember.id, isNotified: false }) : options.messageNotificationsCount == true ? 0 : options.messageNotificationsCount || undefined)(),
            (async () => options.postNotificationsCount == true && !!options.currentMember?.id ? await this.getPostNotificationsCount({ currentMemberId: options.currentMember.id, isNotified: false }) : options.postNotificationsCount == true ? 0 : options.postNotificationsCount || undefined)(),
        ]);

        return {
            ...this,
            createdAt: this.createdAt?.toString(),
            updatedAt: this.updatedAt?.toString(),
            room,
            log,
            syncVision,
            connectingMembers,
            notificationsCount,
            messageNotificationsCount,
            postNotificationsCount,
        };
    }
}