import { ConnectionAttributes, Provider } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
    PrimaryColumn,
} from "typeorm";
import type { UserEntity } from "./UserEntity";
import type { UserConnectionEntity } from "./UserConnectionEntity";
import type { DeviceConnectionEntity } from "./DeviceConnectionEntity";
import type { DeviceEntity } from "./DeviceEntity";
import type { MemberConnectionEntity } from "./MemberConnectionEntity";
import type { MemberEntity } from "./MemberEntity";
import type { RoomMemberConnectionEntity } from "./RoomMemberConnectionEntity";
import type { RoomMemberEntity } from "./RoomMemberEntity";
import Bluebird from "bluebird";
import { monotonicFactory } from "ulid";
import { resolveEntity } from "../config/resolveEntity";

const monotonic = monotonicFactory();

@Entity({ name: "connections" })
export class ConnectionEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "is_connecting" })
    isConnecting: boolean;

    @Column({ name: "sec_websocket_key" })
    secWebsocketKey: string;

    @Column({ name: "provider" })
    provider: Provider;

    @Column({ name: "connected_at", type: 'timestamptz', nullable: true })
    connectedAt?: Date | null;

    @Column({ name: "disconnected_at", type: 'timestamptz', nullable: true })
    disconnectedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToMany(() => resolveEntity<UserConnectionEntity>('UserConnectionEntity'), (userConnection) => userConnection.connection)
    userConnections: Promise<UserConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<UserEntity>('UserEntity'))
    @JoinTable({
        name: 'user_connections',
        inverseJoinColumn: {
            name: 'connection_id',
        },
        joinColumn: {
            name: 'user_id',
        },
    })
    users: Promise<UserEntity[]>;

    @OneToMany(() => resolveEntity<DeviceConnectionEntity>('DeviceConnectionEntity'), (deviceConnection) => deviceConnection.connection)
    deviceConnections: Promise<DeviceConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<DeviceEntity>('DeviceEntity'))
    @JoinTable({
        name: 'device_connections',
        inverseJoinColumn: {
            name: 'connection_id',
        },
        joinColumn: {
            name: 'device_id',
        },
    })
    devices: Promise<DeviceEntity[]>;

    @OneToMany(() => resolveEntity<MemberConnectionEntity>('MemberConnectionEntity'), (memberConnection) => memberConnection.connection)
    memberConnections: Promise<MemberConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<MemberEntity>('MemberEntity'))
    @JoinTable({
        name: 'member_connections',
        inverseJoinColumn: {
            name: 'connection_id',
        },
        joinColumn: {
            name: 'member_id',
        },
    })
    members: Promise<MemberEntity[]>;

    @OneToMany(() => resolveEntity<RoomMemberConnectionEntity>('RoomMemberConnectionEntity'), (roomMemberConnection) => roomMemberConnection.connection)
    roomMemberConnections: Promise<MemberConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<RoomMemberEntity>('RoomMemberEntity'))
    @JoinTable({
        name: 'room_member_connections',
        inverseJoinColumn: {
            name: 'connection_id',
        },
        joinColumn: {
            name: 'room_member_id',
        },
    })
    roomMembers: Promise<RoomMemberEntity[]>;

    async transform(options?: {}): Promise<ConnectionAttributes> {
        return {
            id: this.id,
            isConnecting: this.isConnecting,
            secWebsocketKey: this.secWebsocketKey,
            provider: this.provider,
            connectedAt: this.connectedAt?.toISOString(),
            disconnectedAt: this.disconnectedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
        };
    }

    public async disconnect(): Promise<ConnectionEntity> {
        await Bluebird.all([
            (async () => {
                const connections = await resolveEntity<UserConnectionEntity>('UserConnectionEntity').find({
                    where: {
                        connectionId: this.id,
                        isConnecting: true,
                    }
                });
                return Promise.all(connections.map(c => c.disconnect()))
            })(),
            (async () => {
                const connections = await resolveEntity<MemberConnectionEntity>('MemberConnectionEntity').find({
                    where: {
                        connectionId: this.id,
                        isConnecting: true,
                    }
                });
                return Promise.all(connections.map(c => c.disconnect()))
            })(),
            (async () => {
                const connections = await resolveEntity<RoomMemberConnectionEntity>('RoomMemberConnectionEntity').find({
                    where: {
                        connectionId: this.id,
                        isConnecting: true,
                    }
                });
                return Promise.all(connections.map(c => c.disconnect()))
            })(),
        ]);
        this.isConnecting = false;
        this.disconnectedAt = new Date();
        await this.save();
        return this;
    }
}