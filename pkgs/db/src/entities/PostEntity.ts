import { PostAttributes, PostStatus, PostType, FileAttributes, MemberAttributes, RoomAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { MemberEntity } from "./MemberEntity";
import type { PostReactionEntity } from "./PostReactionEntity";
import type { PostReadEntity } from "./PostReadEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomLogEntity } from "./RoomLogEntity";
import type { RoomMemberEntity } from "./RoomMemberEntity";

@Entity({ name: "posts" })
export class PostEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "parent_id" })
    parentId: string;

    @Column({ name: "sender_id" })
    senderId: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "location" })
    location: string;

    @Column({ type: "text" })
    body: string;

    @Column({ name: "status" })
    status: PostStatus;

    @Column({ name: "type" })
    type: PostType;

    @Column({ name: "comments_count" })
    commentsCount: number;

    @Column({ name: "reactions_count" })
    reactionsCount: number;

    @Column({ name: "is_commentable" })
    isCommentable: boolean;

    @Column({ name: "is_reactable" })
    isReactable: boolean;

    @Column({ name: "archived_at", type: "timestamptz", nullable: true })
    archivedAt?: Date | null;

    @Column({ name: "deleted_at", type: "timestamptz", nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => PostEntity, (parent) => parent.children)
    @JoinColumn({ name: "parent_id" })
    parent: Promise<PostEntity>;

    @OneToMany(() => PostEntity, (child) => child.parent)
    children: Promise<PostEntity[]>;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.posts)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.posts)
    @JoinColumn({ name: "sender_id" })
    sender: Promise<MemberEntity>;

    @ManyToMany(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinTable({
        name: 'post_attachments',
        joinColumn: {
            name: 'post_id',
        },
        inverseJoinColumn: {
            name: 'file_id',
        },
    })
    files: Promise<FileEntity[]>;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.post)
    logs: Promise<RoomLogEntity[]>;

    @OneToMany(() => resolveEntity<PostReadEntity>('PostReadEntity'), (read) => read.post)
    reads: Promise<PostReadEntity[]>;

    currentMember?: MemberEntity;
    currentRoomMember?: RoomMemberEntity;

    async hasComment(): Promise<boolean> {
        const comment = await PostEntity.findOne({
            where: {
                parentId: this.id,
                status: PostStatus.CREATED,
            },
        });
        return !!comment;
    }

    async hasReaction(): Promise<boolean> {
        const reaction = await resolveEntity<PostReactionEntity>('PostReactionEntity').findOne({
            where: {
                postId: this.id,
            },
        });
        return !!reaction;
    }

    async hasYourComment(senderId: string): Promise<boolean> {
        const comment = await PostEntity.findOne({
            where: {
                senderId,
                parentId: this.id,
                status: PostStatus.CREATED,
            },
        });
        return !!comment;
    }

    async hasYourReaction(senderId: string): Promise<boolean> {
        const reaction = await resolveEntity<PostReactionEntity>('PostReactionEntity').findOne({
            where: {
                senderId,
                postId: this.id,
            },
        });
        return !!reaction;
    }

    async transform(options: {
        sender?: boolean | MemberAttributes,
        room?: boolean | RoomAttributes,
        parent?: boolean | PostAttributes,
        files?: boolean | FileAttributes[],
        children?: boolean | PostAttributes[],
        read?: PostReadEntity,
        currentMember?: MemberAttributes | MemberEntity,
    } = {}): Promise<PostAttributes> {
        return {
            id: this.id,
            senderId: this.senderId,
            parentId: this.parentId,
            roomId: this.roomId,
            status: this.status,
            type: this.type,
            body: this.body,
            location: this.location,
            commentsCount: this.commentsCount,
            reactionsCount: this.reactionsCount,
            isCommentable: this.isCommentable,
            isReactable: this.isReactable,
            hasComment: await this.hasComment(),
            hasReaction: await this.hasReaction(),
            hasYourComment: options.currentMember && !!options.currentMember.id && await this.hasYourComment(options.currentMember.id),
            hasYourReaction: options.currentMember && !!options.currentMember.id && await this.hasYourReaction(options.currentMember.id),
            readStatus: options.read?.status,
            archivedAt: this.archivedAt?.toISOString(),
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            room: options.room == true ? (await (await this.room)?.transform()) : options.room || undefined,
            sender: options.sender == true ? (await (await this.sender)?.transform()) : options.sender || undefined,
            parent: options.parent == true ? (await (await this.parent)?.transform()) : options.parent || undefined,
            files: options.files == true ? (await Promise.all((await this.files).map(file => file.transform()))) : options.files || undefined,
            comments: options.children == true ? (await Promise.all((await this.children).map(child => child.transform()))) : options.children || undefined,
        };
    }

    hasMention(member: MemberEntity): boolean {
        return this.body.includes(member.id);
    }
}