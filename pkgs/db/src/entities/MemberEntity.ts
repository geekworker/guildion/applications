import { GuildAttributes, MemberAttributes, MemberStatus, RoleAttributes, UserAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import type { MemberRoleEntity } from "./MemberRoleEntity";
import type { RoleEntity } from "./RoleEntity";
import type { RoomEntity } from "./RoomEntity";
import type { FileEntity } from "./FileEntity";
import type { GuildEntity } from "./GuildEntity";
import type { UserEntity } from "./UserEntity";
import type { RoomMemberEntity } from "./RoomMemberEntity";
import type { MessageEntity } from "./MessageEntity";
import type { InviteEntity } from "./InviteEntity";
import type { PostEntity } from "./PostEntity";
import type { RoomLogEntity } from "./RoomLogEntity";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { MemberConnectionEntity } from "./MemberConnectionEntity";
import type { PremiumKeySubscriptionEntity } from "./PremiumKeySubscriptionEntity";
import type { GuildRewardEntity } from "./GuildRewardEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "members" })
export class MemberEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "user_id" })
    userId: string;

    @Column({ name: "profile_id" })
    profileId: string;

    @Column({ name: "display_name" })
    displayName: string;

    @Column({ type: "text" })
    description: string;

    @Column({ name: "status" })
    status: MemberStatus;

    @Column({ name: 'is_mute', type: "boolean", nullable: true })
    isMute?: boolean | null;

    @Column({ name: 'is_mute_mentions', type: "boolean", nullable: true })
    isMuteMentions?: boolean | null;

    @Column({ name: "index" })
    index: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.members)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @ManyToOne(() => resolveEntity<UserEntity>('UserEntity'), (user) => user.members)
    @JoinColumn({ name: "user_id" })
    user: Promise<UserEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.members)
    @JoinColumn({ name: "profile_id" })
    profile: Promise<FileEntity>;

    @OneToMany(() => resolveEntity<MemberRoleEntity>('MemberRoleEntity'), (role) => role.member)
    memberRoles: Promise<MemberRoleEntity[]>;

    @OneToMany(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.owner)
    rooms: Promise<RoomEntity[]>;

    @OneToMany(() => resolveEntity<RoomMemberEntity>('RoomMemberEntity'), (roomMember) => roomMember.member)
    roomMembers: Promise<RoomMemberEntity[]>;

    @OneToMany(() => resolveEntity<PremiumKeySubscriptionEntity>('PremiumKeySubscriptionEntity'), (premiumKeySubscription) => premiumKeySubscription.member)
    premiumKeySubscriptions: Promise<PremiumKeySubscriptionEntity[]>;

    @OneToMany(() => resolveEntity<GuildRewardEntity>('GuildRewardEntity'), (reward) => reward.member)
    rewards: Promise<GuildRewardEntity[]>;

    @ManyToMany(() => resolveEntity<RoleEntity>('RoleEntity'))
    @JoinTable({
        name: 'member_roles',
        joinColumn: {
            name: 'member_id',
        },
        inverseJoinColumn: {
            name: 'role_id',
        },
    })
    roles: Promise<RoleEntity[]>;

    @OneToMany(() => resolveEntity<MessageEntity>('MessageEntity'), (message) => message.sender)
    messages: Promise<MessageEntity[]>;

    @OneToMany(() => resolveEntity<PostEntity>('PostEntity'), (post) => post.sender)
    posts: Promise<PostEntity[]>;

    @OneToMany(() => resolveEntity<InviteEntity>('InviteEntity'), (invite) => invite.member)
    invites: Promise<InviteEntity[]>;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.member)
    logs: Promise<RoomLogEntity[]>;

    @OneToMany(() => resolveEntity<MemberConnectionEntity>('MemberConnectionEntity'), (memberConnection) => memberConnection.member)
    memberConnections: Promise<MemberConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<ConnectionEntity>('ConnectionEntity'))
    @JoinTable({
        name: 'member_connections',
        joinColumn: {
            name: 'member_id',
        },
        inverseJoinColumn: {
            name: 'connection_id',
        },
    })
    connections: Promise<ConnectionEntity[]>;

    async transform(options?: {
        user?: boolean | UserAttributes,
        guild?: boolean | GuildAttributes,
        roles?: boolean | RoleAttributes[],
        roomMember?: RoomMemberEntity,
        isConnecting?: boolean,
    }): Promise<MemberAttributes> {
        options ??= {};
        return {
            id: this.id,
            profileId: this.profileId,
            userId: this.userId,
            guildId: this.guildId,
            status: options.roomMember?.status ?? this.status,
            displayName: this.displayName,
            description: this.description,
            index: this.index, 
            roomId: options.roomMember?.roomId,
            roomMemberId: options.roomMember?.id,
            isMute: options.roomMember?.isMute ?? this.isMute,
            isMuteMentions: options.roomMember?.isMuteMentions ?? this.isMuteMentions,
            isConnecting: options.isConnecting,
            createdAt: this.createdAt?.toString(),
            updatedAt: this.updatedAt?.toString(),
            profile: await (await this.profile).transform(),
            guild: options.guild == true ? (await (await this.guild).transform()) : options.guild || undefined,
            user: options.user == true ? (await (await this.user).transform()) : options.user || undefined,
            roles: options.roles == true ? await Promise.all((await this.roles).map(val => val.transform())) : options.roles || undefined,
        };
    }

    async activateIfPossible(): Promise<MemberEntity> {
        if (this.status == MemberStatus.DELETED) {
            this.status = MemberStatus.CREATED;
            return this.save();
        } else {
            return this;
        }
    }

    async activateForce(): Promise<MemberEntity> {
        this.status = MemberStatus.CREATED;
        return this.save();
    }

    async delete(): Promise<MemberEntity> {
        if (this.status == MemberStatus.CREATED) {
            this.status = MemberStatus.DELETED;
            return this.save();
        } else {
            return this;
        }
    }

    async ban(): Promise<MemberEntity> {
        this.status = MemberStatus.BANNED;
        return this.save();
    }

    async lock(): Promise<MemberEntity> {
        this.status = MemberStatus.LOCKED;
        return this.save();
    }

    async unlock(): Promise<MemberEntity> {
        if (this.status == MemberStatus.LOCKED || this.status == MemberStatus.BANNED) {
            this.status = MemberStatus.DELETED;
            return this.save();
        } else {
            return this;
        }
    }
}