import { GuildAttributes, RoleAttributes, RoleType } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    ManyToOne,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import type { GuildEntity } from "./GuildEntity";
import type { FileRoleEntity } from "./FileRoleEntity";
import type { FileEntity } from "./FileEntity";
import type { MemberRoleEntity } from "./MemberRoleEntity";
import type { MemberEntity } from "./MemberEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomRoleEntity } from "./RoomRoleEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "roles" })
export class RoleEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "type" })
    type: RoleType;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.roles)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @OneToMany(() => resolveEntity<FileRoleEntity>('FileRoleEntity'), (role) => role.file)
    fileRoles: Promise<FileRoleEntity[]>;

    @ManyToMany(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinTable({
        name: 'file_roles',
        joinColumn: {
            name: 'role_id',
        },
        inverseJoinColumn: {
            name: 'file_id',
        },
    })
    files: Promise<FileEntity[]>;

    @OneToMany(() => resolveEntity<MemberRoleEntity>('MemberRoleEntity'), (memberRole) => memberRole.role)
    memberRoles: Promise<MemberRoleEntity[]>;

    @ManyToMany(() => resolveEntity<MemberEntity>('MemberEntity'))
    @JoinTable({
        name: 'member_roles',
        joinColumn: {
            name: 'role_id',
        },
        inverseJoinColumn: {
            name: 'member_id',
        },
    })
    members: Promise<MemberEntity[]>;

    @OneToMany(() => resolveEntity<RoomRoleEntity>('RoomRoleEntity'), (roomRole) => roomRole.role)
    roomRoles: Promise<RoomRoleEntity[]>;

    @ManyToMany(() => resolveEntity<RoomEntity>('RoomEntity'))
    @JoinTable({
        name: 'room_roles',
        joinColumn: {
            name: 'role_id',
        },
        inverseJoinColumn: {
            name: 'room_id',
        },
    })
    rooms: Promise<RoomEntity[]>;

    async transform(
        options: {
            guild?: boolean | GuildAttributes,
            fileRole?: FileRoleEntity,
            roomRole?: RoomRoleEntity,
            memberRole?: MemberRoleEntity,
        } = {}
    ): Promise<RoleAttributes> {
        const guild = options.guild == true ? (await (await this.guild)?.transform()) : options.guild || undefined;
        return {
            id: this.id,
            guildId: this.guildId,
            fileId: options.fileRole?.fileId,
            memberId: options.memberRole?.memberId,
            roomId: options.roomRole?.roomId,
            type: this.type,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            guild,
            file: await (await options.fileRole?.file)?.transform(),
        };
    }
}