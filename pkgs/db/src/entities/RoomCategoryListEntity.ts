import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { RoomCategoryEntity } from "./RoomCategoryEntity";
import type { RoomEntity } from "./RoomEntity";

@Entity({ name: "room_category_list" })
export class RoomCategoryListEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'room_id' })
    roomId: string;

    @Column({ name: 'category_id' })
    category_id: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.categoryLists)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<RoomCategoryEntity>('RoomCategoryEntity'), (category) => category.lists)
    @JoinColumn({ name: "category_id" })
    category: Promise<RoomCategoryEntity>;
}