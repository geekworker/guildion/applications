import { GuildStatus, GuildType, UserAttributes, LanguageCode, CountryCode, FileAttributes, GuildCategoryAttributes, GuildAttributes, RoleAttributes, MemberAttributes, RoomAttributes, InviteAttributes, GuildActivityAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToOne,
    OneToMany,
    LessThanOrEqual,
    ManyToMany,
    JoinTable,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { DiscoveryGuildEntity } from "./DiscoveryGuildEntity";
import type { FileEntity } from "./FileEntity";
import type { GuildActivityEntity } from "./GuildActivityEntity";
import type { GuildCategoryEntity } from "./GuildCategoryEntity";
import type { GuildCategoryListEntity } from "./GuildCategoryListEntity";
import type { GuildFileEntity } from "./GuildFileEntity";
import type { GuildPremiumKeyEntity } from "./GuildPremiumKeyEntity";
import type { GuildStorageEntity } from "./GuildStorageEntity";
import type { InviteEntity } from "./InviteEntity";
import type { MemberEntity } from "./MemberEntity";
import type { RoleEntity } from "./RoleEntity";
import type { RoomCategoryEntity } from "./RoomCategoryEntity";
import type { RoomEntity } from "./RoomEntity";
import type { UserEntity } from "./UserEntity";

@Entity({ name: "guilds" })
export class GuildEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "owner_id" })
    ownerId: string;

    @Column({ name: "profile_id" })
    profileId: string;

    @Column({ name: "guildname" })
    guildname: string;

    @Column({ name: "display_name" })
    displayName: string;

    @Column({ type: "text" })
    description: string;

    @Column({ name: "type" })
    type: GuildType;

    @Column({ name: "status" })
    status: GuildStatus;

    @Column({ name: "language_code" })
    languageCode: LanguageCode;

    @Column({ name: "country_code" })
    countryCode: CountryCode;

    @Column({ name: "max_content_length" })
    maxContentLength: number;

    @Column({ name: "deleted_at", type: 'timestamptz', nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<UserEntity>('UserEntity'), (owner) => owner.guilds)
    @JoinColumn({ name: "owner_id" })
    owner: Promise<UserEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.guilds)
    @JoinColumn({ name: "profile_id" })
    profile: Promise<FileEntity>;

    @OneToMany(() => resolveEntity<GuildCategoryListEntity>('GuildCategoryListEntity'), (categoryList) => categoryList.guild)
    categoryLists: Promise<GuildCategoryListEntity[]>;

    @ManyToMany(() => resolveEntity<GuildCategoryEntity>('GuildCategoryEntity'))
    @JoinTable({
        name: 'guild_category_lists',
        joinColumn: {
            name: 'guild_id',
        },
        inverseJoinColumn: {
            name: 'category_id',
        },
    })
    categories: Promise<GuildCategoryEntity[]>;

    @OneToOne(() => resolveEntity<GuildStorageEntity>('GuildStorageEntity'), (guildStorage) => guildStorage.guild)
    guildStorage: Promise<GuildStorageEntity>;

    @OneToMany(() => resolveEntity<RoleEntity>('RoleEntity'), (role) => role.guild)
    roles: Promise<RoleEntity[]>;

    @OneToMany(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.guild)
    members: Promise<MemberEntity[]>;

    @OneToMany(() => resolveEntity<RoomCategoryEntity>('RoomCategoryEntity'), (roomCategory) => roomCategory.guild)
    roomCategories: Promise<RoomCategoryEntity[]>;

    @OneToMany(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.guild)
    rooms: Promise<RoomEntity[]>;

    @OneToMany(() => resolveEntity<InviteEntity>('InviteEntity'), (invite) => invite.guild)
    invites: Promise<InviteEntity[]>;

    @OneToMany(() => resolveEntity<GuildActivityEntity>('GuildActivityEntity'), (activity) => activity.guild)
    activities: Promise<GuildActivityEntity[]>;

    @OneToMany(() => resolveEntity<DiscoveryGuildEntity>('DiscoveryGuildEntity'), (discoveryGuild) => discoveryGuild.discovery)
    discoveryGuilds: Promise<DiscoveryGuildEntity[]>;

    @OneToMany(() => resolveEntity<GuildPremiumKeyEntity>('GuildPremiumKeyEntity'), (guildPremiumKey) => guildPremiumKey.guild)
    guildPremiumKeys: Promise<GuildPremiumKeyEntity[]>;

    @OneToMany(() => resolveEntity<GuildFileEntity>('GuildFileEntity'), (guildFile) => guildFile.guild)
    guildFiles: Promise<GuildFileEntity[]>;

    @ManyToMany(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinTable({
        name: 'guild_files',
        joinColumn: {
            name: 'guild_id',
        },
        inverseJoinColumn: {
            name: 'file_id',
        },
    })
    files: Promise<FileEntity[]>;

    currentMember?: MemberEntity;
    get ownerMember(): Promise<MemberEntity | undefined> {
        return resolveEntity<MemberEntity>('MemberEntity').findOne({
            where: {
                userId: this.ownerId,
                guildId: this.id,
            },
        })
    };

    async getLatestActivity(ltAt?: Date): Promise<GuildActivityEntity | undefined> {
        const latests = await resolveEntity<GuildActivityEntity>('GuildActivityEntity').find({
            where: {
                guildId: this.id,
                createdAt: LessThanOrEqual(ltAt ?? new Date()),
            },
            take: 1,
            order: { createdAt: 'DESC' },
        });
        return latests.length == 0 ? undefined : latests[0];
    }

    async transform(
        options: {
            owner?: boolean | UserAttributes,
            roles?: boolean | RoleAttributes[],
            members?: MemberAttributes[],
            rooms?: boolean | RoomAttributes[],
            ownerMember?: boolean | MemberAttributes,
            category?: boolean | GuildCategoryAttributes,
            currentMember?: MemberAttributes,
            storage?: boolean | FileAttributes,
            invites?: boolean | InviteAttributes[],
            activity?: boolean | GuildActivityAttributes,
            notificationsCount?: boolean | number,
            connectingMembers?: boolean | MemberAttributes[],
            activedBaseAt?: Date,
        } = {}
    ): Promise<GuildAttributes> {
        const [
            profile,
            owner,
            storage,
            roles
        ] = await Bluebird.all<FileAttributes | undefined, UserAttributes | undefined, FileAttributes | undefined, RoleAttributes[] | undefined>([
            (async () => (await this.profile).transform())(),
            (async () => options.owner == true ? ((await this.owner).transform()) : options.owner || undefined)(),
            (async () => options.storage == true ? ((await (await this.guildStorage).file).transform()) : options.storage || undefined)(),
            (async () => options.roles == true ? (await Bluebird.all((await this.roles).map(r => r.transform()))) : options.roles || undefined)(),
        ]);

        const [
            rooms,
            invites,
            ownerMember,
            currentMember,
        ] = await Bluebird.all<RoomAttributes[] | undefined, InviteAttributes[] | undefined, MemberAttributes | undefined, MemberAttributes | undefined>([
            (async () => options.rooms == true ? await Bluebird.all((await this.rooms).map(val => val.transform())) : options.rooms || undefined)(),
            (async () => options.invites == true ? await Bluebird.all((await this.invites).map(val => val.transform())) : options.invites || undefined)(),
            (async () => options.ownerMember == true ? ((await this.ownerMember)?.transform()) : options.ownerMember || undefined)(),
            (async () => options.currentMember || await this.currentMember?.transform())(),
        ]);

        return {
            id: this.id,
            profileId: this.profileId,
            ownerId: this.ownerId,
            status: this.status,
            type: this.type,
            displayName: this.displayName,
            description: this.description,
            guildname: this.guildname,
            languageCode: this.languageCode,
            countryCode: this.countryCode,
            maxContentLength: this.maxContentLength,
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            index: this.currentMember?.index,
            profile,
            owner,
            storage,
            roles,
            members: options.members,
            rooms,
            invites,
            currentMember,
            ownerMember,
            activity: options.activity == true ? (await (await this.getLatestActivity(options.activedBaseAt))?.transform({
                guild: false,
                currentMember: options.currentMember || await this.currentMember?.transform(),
                notificationsCount: options.notificationsCount,
                connectingMembers: options.connectingMembers,
            })) : options.activity || undefined,
        };
    }

    async activate(): Promise<GuildActivityEntity> {
        return await (resolveEntity<GuildActivityEntity>('GuildActivityEntity') as any).activate(this.id)
    }
}