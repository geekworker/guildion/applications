import { AccountStatus, AccountSessionStatus, UserAttributes, AccountAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
    OneToMany,
} from "typeorm";
import type { AccessTokenEntity } from "./AccessTokenEntity";
import type { UserEntity } from "./UserEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "accounts" })
export class AccountEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "user_id", type: "uuid", nullable: true })
    userId?: string | null;

    @Column({ name: "status" })
    status: AccountStatus;

    @Column({ name: "session_status" })
    sessionStatus: AccountSessionStatus;

    @Column({ name: "session_allowed_at", type: 'timestamptz', nullable: true })
    sessionAllowedAt?: Date | null;

    @Column({ name: "session_allowed_interval_sec", type: "integer", nullable: true })
    sessionAllowedIntervalSec?: number | null;

    @Column({ name: "deleted_at", type: 'timestamptz', nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<UserEntity>('UserEntity'), (user) => user.account)
    @JoinColumn({ name: "user_id" })
    user: Promise<UserEntity>;

    @OneToMany(() => resolveEntity<AccessTokenEntity>('AccessTokenEntity'), (accessToken) => accessToken.device)
    accessTokens: Promise<AccessTokenEntity[]>;

    async transform(
        options: {
            user?: boolean | UserAttributes,
        } = {}
    ): Promise<AccountAttributes> {
        return {
            id: this.id,
            userId: this.userId,
            status: this.status,
            sessionStatus: this.sessionStatus,
            sessionAllowedAt: this.sessionAllowedAt,
            sessionAllowedIntervalSec: this.sessionAllowedIntervalSec,
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            user: options.user == true ? (await (await this.user).transform()) : options.user || undefined,
        };
    }
}