import { CountryCode, LanguageCode, DeviceAttributes, DeviceStatus, TimeZoneID } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
} from "typeorm";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { AccessTokenEntity } from "./AccessTokenEntity";
import type { CSRFEntity } from "./CSRFEntity";
import type { DeviceConnectionEntity } from "./DeviceConnectionEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "devices" })
export class DeviceEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "udid" })
    udid: string;

    @Column({ name: "os" })
    os: string;

    @Column({ name: "model" })
    model: string;

    @Column({ name: "status" })
    status: DeviceStatus;

    @Column({ name: "language_code" })
    languageCode: LanguageCode;

    @Column({ name: "country_code" })
    countryCode: CountryCode;

    @Column({ name: "timezone" })
    timezone: TimeZoneID;

    @Column({ name: "onesignal_notification_id", type: 'varchar', nullable: true })
    onesignalNotificationId?: string | null;

    @Column({ name: "notification_token", type: 'varchar', nullable: true })
    notificationToken?: string | null;

    @Column({ name: "app_version" })
    appVersion: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToMany(() => resolveEntity<CSRFEntity>('CSRFEntity'), (csrf) => csrf.device)
    csrfs: Promise<CSRFEntity[]>;

    @OneToMany(() => resolveEntity<AccessTokenEntity>('AccessTokenEntity'), (accessToken) => accessToken.device)
    accessTokens: Promise<AccessTokenEntity[]>;

    @OneToMany(() => resolveEntity<DeviceConnectionEntity>('DeviceConnectionEntity'), (deviceConnection) => deviceConnection.device)
    deviceConnections: Promise<DeviceConnectionEntity[]>;

    @ManyToMany(() => resolveEntity<ConnectionEntity>('ConnectionEntity'))
    @JoinTable({
        name: 'device_connections',
        joinColumn: {
            name: 'device_id',
        },
        inverseJoinColumn: {
            name: 'connection_id',
        },
    })
    connections: Promise<ConnectionEntity[]>;

    async transform(options?: {}): Promise<DeviceAttributes> {
        return {
            id: this.id,
            udid: this.udid,
            os: this.os,
            model: this.model,
            status: this.status,
            languageCode: this.languageCode,
            countryCode: this.countryCode,
            timezone: this.timezone,
            onesignalNotificationId: this.onesignalNotificationId,
            notificationToken: this.notificationToken,
            appVersion: this.appVersion,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
        };
    }

    public async disconnect(): Promise<DeviceEntity> {
        const connections = await resolveEntity<ConnectionEntity>('ConnectionEntity')
            .getRepository()
            .createQueryBuilder("connections")
            .innerJoin("connections.deviceConnections", "device_connections", "device_connections.device_id = :deviceId", { deviceId: this.id })
            .where("connections.is_connecting = :isConnecting", { isConnecting: true })
            .getMany()
        
        await Promise.all(
            connections.map(async connection => connection.disconnect())
        );
        return this;
    }
}