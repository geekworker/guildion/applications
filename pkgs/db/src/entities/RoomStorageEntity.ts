import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { RoomEntity } from "./RoomEntity";

@Entity({ name: "room_storages" })
export class RoomStorageEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "file_id" })
    fileId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.roomStorage)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @OneToOne(() => resolveEntity<FileEntity>('FileEntity'))
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;
}