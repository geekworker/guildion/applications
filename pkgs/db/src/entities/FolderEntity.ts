import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
} from "typeorm";
import { FileStatus } from "@guildion/core";
import type { FileEntity } from "./FileEntity";
import type { FilingEntity } from "./FilingEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "folders" })
export class FolderEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "file_id" })
    fileId: string;

    @Column({ name: "files_count", type: "integer" })
    filesCount: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.folder)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;

    async updateFilesCount(): Promise<FolderEntity> {
        this.filesCount = await resolveEntity<FilingEntity>('FilingEntity')
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.file', 'file', 'file.status = :status', { status: FileStatus.UPLOADED })
            .where('filings.folder_id = :id', { id: this.fileId })
            .getCount();
        return this.save();
    }

    async calcContentLength(): Promise<number> {
        const file = await this.file;
        const result = await resolveEntity<FilingEntity>('FilingEntity')
            .getRepository()
            .createQueryBuilder('filings')
            .select("SUM(file.content_length)", "sum")
            .innerJoinAndSelect('filings.file', 'file', 'file.status = :status', { status: FileStatus.UPLOADED })
            .where('filings.folder_id = :id', { id: this.fileId })
            .groupBy('file.id')
            .getRawOne<{ sum: number }>();
        return result?.sum ? Number(result.sum) : file.contentLength;
    }

    async updateContentLength(): Promise<FileEntity> {
        const file = await this.file;
        file.contentLength = await this.calcContentLength();
        return file.save();
    }
}