import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { monotonicFactory } from "ulid";
import type { RoomEntity } from "./RoomEntity";
import type { DiscoveryEntity } from "./DiscoveryEntity";
import { resolveEntity } from "../config/resolveEntity";

const monotonic = monotonicFactory();

@Entity({ name: "discovery_rooms" })
export class DiscoveryRoomEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "discovery_id" })
    discoveryId: string;

    @Column({ name: "room_id" })
    roomId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<DiscoveryEntity>('DiscoveryEntity'), (discovery) => discovery.discoveryRooms)
    @JoinColumn({ name: "discovery_id" })
    discovery: Promise<DiscoveryEntity>;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.discoveryRooms)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;
}