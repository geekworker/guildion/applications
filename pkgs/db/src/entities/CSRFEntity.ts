import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    ManyToOne,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { DeviceEntity } from "./DeviceEntity";

@Entity({ name: "csrfs" })
export class CSRFEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "device_id" })
    deviceId: string;

    @Column({ name: "secret", type: "varchar", nullable: true })
    secret?: string | null;

    @Column({ name: "hash" })
    hash: string;

    @Column({ name: "salt" })
    salt: string;

    @Column({ name: "iv" })
    iv: string;

    @Column({ name: "is_active" })
    isActive: boolean;

    @Column({ name: "expired_at", type: 'timestamptz', nullable: true })
    expiredAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<DeviceEntity>('DeviceEntity'), (device) => device.csrfs)
    @JoinColumn({ name: "device_id" })
    device: Promise<DeviceEntity>;
}