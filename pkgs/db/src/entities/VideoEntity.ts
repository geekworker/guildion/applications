import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    OneToOne,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";

@Entity({ name: "videos" })
export class VideoEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "file_id" })
    fileId: string;

    @Column({ name: "thumbnail_id", type: "uuid", nullable: true })
    thumbnailId?: string | null;

    @Column({ name: "duration_ms", type: "integer" })
    durationMs: number;

    @Column({ name: "width_px", type: "decimal" })
    widthPx: number;

    @Column({ name: "height_px", type: "decimal" })
    heightPx: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.video)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;

    @OneToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.thumbnailVideo)
    @JoinColumn({ name: "thumbnail_id" })
    thumbnail: Promise<FileEntity>;
}