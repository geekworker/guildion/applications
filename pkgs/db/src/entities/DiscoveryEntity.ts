import { DiscoveryDataType, DiscoveryAttributes, DiscoveryStatus, DiscoveryType, RoomAttributes, GuildAttributes, FileAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToMany,
    JoinTable,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import type { GuildEntity } from "./GuildEntity";
import type { DiscoveryGuildEntity } from "./DiscoveryGuildEntity";
import type { DiscoveryRoomEntity } from "./DiscoveryRoomEntity";
import type { RoomEntity } from "./RoomEntity";
import type { FileEntity } from "./FileEntity";
import Bluebird from "bluebird";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "discoveries" })
export class DiscoveryEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "en_thumbnail_id", type: "uuid", nullable: true })
    enThumbnailId?: string | null;

    @Column({ name: "ja_thumbnail_id", type: "uuid", nullable: true })
    jaThumbnailId?: string | null;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'ja_title' })
    jaTitle: string;

    @Column({ name: 'en_title' })
    enTitle: string;

    @Column({ name: 'ja_description' })
    jaDescription: string;

    @Column({ name: 'en_description' })
    enDescription: string;

    @Column({ name: 'ja_data' })
    jaData: string;

    @Column({ name: 'en_data' })
    enData: string;

    @Column({ name: "status" })
    status: DiscoveryStatus;

    @Column({ name: "type" })
    type: DiscoveryType;

    @Column({ name: "data_type" })
    dataType: DiscoveryDataType;

    @Column({ name: "score" })
    score: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.enDiscoveries)
    @JoinColumn({ name: "en_thumbnail_id" })
    enThumbnail: Promise<FileEntity | undefined>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.jaDiscoveries)
    @JoinColumn({ name: "en_thumbnail_id" })
    jaThumbnail: Promise<FileEntity | undefined>;

    @OneToMany(() => resolveEntity<DiscoveryGuildEntity>('DiscoveryGuildEntity'), (discoveryGuild) => discoveryGuild.discovery)
    discoveryGuilds: Promise<DiscoveryGuildEntity[]>;

    @ManyToMany(() => resolveEntity<GuildEntity>('GuildEntity'))
    @JoinTable({
        name: 'discovery_guilds',
        joinColumn: {
            name: 'discovery_id',
        },
        inverseJoinColumn: {
            name: 'guild_id',
        },
    })
    guilds: Promise<GuildEntity[]>;

    @OneToMany(() => resolveEntity<DiscoveryRoomEntity>('DiscoveryRoomEntity'), (discoveryRoom) => discoveryRoom.discovery)
    discoveryRooms: Promise<DiscoveryRoomEntity[]>;

    @ManyToMany(() => resolveEntity<RoomEntity>('RoomEntity'))
    @JoinTable({
        name: 'discovery_rooms',
        joinColumn: {
            name: 'discovery_id',
        },
        inverseJoinColumn: {
            name: 'room_id',
        },
    })
    rooms: Promise<RoomEntity[]>;

    async transform(options: {
        guilds?: boolean | GuildAttributes[],
        rooms?: boolean | RoomAttributes[],
    } = {}): Promise<DiscoveryAttributes> {
        const [
            guilds,
            rooms,
            enThumbnail,
            jaThumbnail,
        ] = await Bluebird.all<GuildAttributes[] | undefined, RoomAttributes[] | undefined, FileAttributes | undefined, FileAttributes | undefined>([
            (async () => options.guilds == true ? (await Bluebird.all((await this.guilds).map(v => v.transform()))) : options.guilds || undefined)(),
            (async () => options.rooms == true ? (await Bluebird.all((await this.rooms).map(v => v.transform()))) : options.rooms || undefined)(),
            (async () => (await this.enThumbnail)?.transform())(),
            (async () => (await this.jaThumbnail)?.transform())(),
        ]);
        return {
            id: this.id,
            jaThumbnailId: this.jaThumbnailId,
            enThumbnailId: this.enThumbnailId,
            slug: this.slug,
            jaTitle: this.jaTitle,
            enTitle: this.enTitle,
            jaDescription: this.jaDescription,
            enDescription: this.enDescription,
            jaData: this.jaData,
            enData: this.enData,
            status: this.status,
            type: this.type,
            dataType: this.dataType,
            score: this.score,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            guilds,
            rooms,
            enThumbnail,
            jaThumbnail,
        };
    }
}