import { FileAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { SyncVisionEntity } from "./SyncVisionEntity";

@Entity({ name: "filings" })
export class FilingEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "file_id" })
    fileId: string;

    @Column({ name: "folder_id" })
    folderId: string;

    @Column({ name: "index" })
    index: number;

    @Column({ name: "random_index", type: "integer", nullable: true })
    randomIndex?: number | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.filings)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.folderFilings)
    @JoinColumn({ name: "folder_id" })
    folder: Promise<FileEntity>;

    @OneToMany(() => resolveEntity<SyncVisionEntity>('SyncVisionEntity'), (syncVision) => syncVision.file)
    syncVisions: Promise<SyncVisionEntity[]>;

    async transform(
        options: {
            folder?: boolean | FileAttributes,
            file?: boolean | FileAttributes,
            presignedURL?: string,
        } = {}
    ): Promise<FileAttributes> {
        const file = await this.file;
        const folder = await (await this.folder)?.transform();
        return await file.transform({
            filing: this,
            folder,
            presignedURL: options.presignedURL,
        });
    }
}