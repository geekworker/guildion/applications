import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { monotonicFactory } from "ulid";
import { resolveEntity } from "../config/resolveEntity";
import type { ConnectionEntity } from "./ConnectionEntity";
import type { MemberEntity } from "./MemberEntity";

const monotonic = monotonicFactory();

@Entity({ name: "member_connections" })
export class MemberConnectionEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "member_id" })
    memberId: string;

    @Column({ name: "connection_id" })
    connectionId: string;

    @Column({ name: "is_connecting" })
    isConnecting: boolean;

    @Column({ name: "connected_at", type: 'timestamptz', nullable: true })
    connectedAt?: Date | null;

    @Column({ name: "disconnected_at", type: 'timestamptz', nullable: true })
    disconnectedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.memberConnections)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity>;

    @ManyToOne(() => resolveEntity<ConnectionEntity>('ConnectionEntity'), (connection) => connection.memberConnections)
    @JoinColumn({ name: "connection_id" })
    connection: Promise<ConnectionEntity>;

    public async disconnect(): Promise<MemberConnectionEntity> {
        if (!this.isConnecting || this.disconnectedAt) return this;
        this.isConnecting = false;
        this.disconnectedAt = new Date();
        return await this.save()
    }
}