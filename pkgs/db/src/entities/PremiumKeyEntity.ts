import { GuildAttributes, PremiumKeyAttributes, PremiumKeyStatus, SubscriptionInterval } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildPremiumKeyEntity } from "./GuildPremiumKeyEntity";

@Entity({ name: "premium_keys" })
export class PremiumKeyEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'ja_name' })
    jaName: string;

    @Column({ name: 'en_name' })
    enName: string;

    @Column({ name: 'ja_description' })
    jaDescription: string;

    @Column({ name: 'en_description' })
    enDescription: string;

    @Column({ name: 'storage_content_length' })
    storageContentLength: number;

    @Column({ name: 'base_price' })
    basePrice: number;

    @Column({ name: 'web_commission_price' })
    webCommissionPrice: number;

    @Column({ name: 'ios_commission_price' })
    iosCommissionPrice: number;

    @Column({ name: 'android_commission_price' })
    androidCommissionPrice: number;

    @Column({ name: 'web_provider_key', type: "varchar", nullable: true })
    webProviderKey?: string | null;

    @Column({ name: 'ios_provider_key', type: "varchar", nullable: true })
    iosProviderKey?: string | null;

    @Column({ name: 'android_provider_key', type: "varchar", nullable: true })
    androidProviderKey?: string | null;

    @Column({ name: 'web_status' })
    webStatus: PremiumKeyStatus;

    @Column({ name: 'ios_status' })
    iosStatus: PremiumKeyStatus;

    @Column({ name: 'android_status' })
    androidStatus: PremiumKeyStatus;

    @Column({ name: 'interval' })
    interval: SubscriptionInterval;

    @Column({ name: "published_at", type: "timestamptz", nullable: true })
    publishedAt?: Date | null;

    @Column({ name: "deleted_at", type: "timestamptz", nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToMany(() => resolveEntity<GuildPremiumKeyEntity>('GuildPremiumKeyEntity'), (guildPremiumKey) => guildPremiumKey.premiumKey)
    guildPremiumKeys: Promise<GuildPremiumKeyEntity[]>;

    async transform(options?: {
        guildPremiumKey?: GuildPremiumKeyEntity,
        guild?: GuildAttributes,
    }): Promise<PremiumKeyAttributes> {
        options ??= {};
        return {
            id: this.id,
            guildId: options.guild?.id,
            guildPremiumKeyId: options.guildPremiumKey?.id,
            jaName: this.jaName,
            enName: this.enName,
            jaDescription: this.jaDescription,
            enDescription: this.enDescription,
            storageContentLength: this.storageContentLength,
            basePrice: this.basePrice,
            webCommissionPrice: this.webCommissionPrice,
            iosCommissionPrice: this.iosCommissionPrice,
            androidCommissionPrice: this.androidCommissionPrice,
            webProviderKey: this.webProviderKey,
            iosProviderKey: this.iosProviderKey,
            androidProviderKey: this.androidProviderKey,
            webStatus: this.webStatus,
            iosStatus: this.iosStatus,
            androidStatus: this.androidStatus,
            interval: this.interval,
            publishedAt: this.publishedAt?.toISOString(),
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            commissionRate: options.guildPremiumKey?.commissionRate,
            guildStatus: options.guildPremiumKey?.status,
            guild: options.guild,
        };
    }
}