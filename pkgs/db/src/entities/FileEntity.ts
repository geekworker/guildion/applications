import { ContentType, FileExtension, FileStatus, FileType, Provider, UserAttributes, FileAttributes, trimStoragePath, transformFromExtension, FileRecordAttributes, FileAccessType, SyncVisionAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
    ManyToMany,
    JoinTable,
    OneToOne,
} from "typeorm";
import type { FileRoleEntity } from "./FileRoleEntity";
import type { FilingEntity } from "./FilingEntity";
import type { GuildEntity } from "./GuildEntity";
import type { RoleEntity } from "./RoleEntity";
import type { UserEntity } from "./UserEntity";
import type { MemberEntity } from './MemberEntity';
import type { RoomEntity } from "./RoomEntity";
import type { SyncVisionEntity } from "./SyncVisionEntity";
import type { RoomFileEntity } from "./RoomFileEntity";
import type { DiscoveryEntity } from "./DiscoveryEntity";
import type { DocumentEntity } from "./DocumentEntity";
import type { FolderEntity } from "./FolderEntity";
import type { ImageEntity } from "./ImageEntity";
import type { VideoEntity } from "./VideoEntity";
import type { EmojiEntity } from "./EmojiEntity";
import type { GuildFileEntity } from "./GuildFileEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "files" })
export class FileEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "holder_id", type: "uuid", nullable: true })
    holderId?: string | null;

    @Column({ name: "display_name" })
    displayName: string;

    @Column({ type: "text" })
    description: string;

    @Column({ name: "url" })
    url: string;

    @Column({ name: "extension" })
    extension: FileExtension;

    @Column({ name: "content_type" })
    contentType: ContentType;

    @Column({ name: "type", type: "enum", enum: FileType })
    type: FileType;

    @Column({ name: "status", type: "enum", enum: FileStatus })
    status: FileStatus;

    @Column({ name: "access_type", type: "enum", enum: FileAccessType })
    accessType: FileAccessType;

    @Column({ name: "content_length" })
    contentLength: number;

    @Column({ name: "provider" })
    provider: Provider;

    @Column({ name: "provider_key" })
    providerKey: string;

    @Column({ name: "is_system" })
    isSystem: boolean;

    @Column({ name: "archived_at", type: 'timestamptz', nullable: true })
    archivedAt?: Date | null;

    @Column({ name: "deleted_at", type: 'timestamptz', nullable: true })
    deletedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToOne(() => resolveEntity<FolderEntity>('FolderEntity'), (folder) => folder.file)
    folder: Promise<FolderEntity | undefined>;

    @OneToOne(() => resolveEntity<ImageEntity>('ImageEntity'), (image) => image.file)
    image: Promise<ImageEntity | undefined>;

    @OneToOne(() => resolveEntity<VideoEntity>('VideoEntity'), (video) => video.file)
    video: Promise<VideoEntity | undefined>;

    @OneToOne(() => resolveEntity<VideoEntity>('VideoEntity'), (video) => video.thumbnail)
    thumbnailVideo: Promise<VideoEntity | undefined>;

    @OneToOne(() => resolveEntity<EmojiEntity>('EmojiEntity'), (emoji) => emoji.file)
    emoji: Promise<EmojiEntity | undefined>;

    @OneToMany(() => resolveEntity<UserEntity>('UserEntity'), (user) => user.profile)
    users: Promise<UserEntity[]>;

    @OneToMany(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.profile)
    guilds: Promise<GuildEntity[]>;

    @OneToMany(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.profile)
    rooms: Promise<RoomEntity[]>;

    @OneToMany(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.profile)
    members: Promise<MemberEntity[]>;

    @OneToMany(() => resolveEntity<DiscoveryEntity>('DiscoveryEntity'), (discovery) => discovery.enThumbnail)
    enDiscoveries: Promise<DiscoveryEntity[]>;

    @OneToMany(() => resolveEntity<DiscoveryEntity>('DiscoveryEntity'), (discovery) => discovery.jaThumbnail)
    jaDiscoveries: Promise<DiscoveryEntity[]>;

    @OneToMany(() => resolveEntity<DocumentEntity>('DocumentEntity'), (document) => document.enThumbnail)
    enDocuments: Promise<DocumentEntity[]>;

    @OneToMany(() => resolveEntity<DocumentEntity>('DocumentEntity'), (document) => document.jaThumbnail)
    jaDocuments: Promise<DocumentEntity[]>;

    @ManyToOne(() => resolveEntity<UserEntity>('UserEntity'), (user) => user.files)
    @JoinColumn({ name: "holder_id" })
    holder: Promise<UserEntity | undefined>;

    @OneToMany(() => resolveEntity<FilingEntity>('FilingEntity'), (filing) => filing.folder)
    folderFilings: Promise<FilingEntity[]>;

    @OneToMany(() => resolveEntity<FilingEntity>('FilingEntity'), (filing) => filing.file)
    filings: Promise<FilingEntity[]>;

    @ManyToMany(() => FileEntity)
    @JoinTable({
        name: 'filings',
        joinColumn: {
            name: 'folder_id',
        },
        inverseJoinColumn: {
            name: 'file_id',
        },
    })
    files: Promise<FileEntity[]>;

    @ManyToMany(() => FileEntity)
    @JoinTable({
        name: 'filings',
        joinColumn: {
            name: 'file_id',
        },
        inverseJoinColumn: {
            name: 'folder_id',
        },
    })
    folders: Promise<FileEntity[]>;

    @OneToMany(() => resolveEntity<FileRoleEntity>('FileRoleEntity'), (role) => role.file)
    fileRoles: Promise<FileRoleEntity[]>;

    @ManyToMany(() => resolveEntity<RoleEntity>('RoleEntity'))
    @JoinTable({
        name: 'file_roles',
        joinColumn: {
            name: 'file_id',
        },
        inverseJoinColumn: {
            name: 'role_id',
        },
    })
    roles: Promise<RoleEntity[]>;

    @OneToMany(() => resolveEntity<RoomFileEntity>('RoomFileEntity'), (roomFile) => roomFile.file)
    roomFiles: Promise<RoomFileEntity[]>;

    @ManyToMany(() => resolveEntity<RoomEntity>('RoomEntity'))
    @JoinTable({
        name: 'room_files',
        joinColumn: {
            name: 'file_id',
        },
        inverseJoinColumn: {
            name: 'room_id',
        },
    })
    roomsOfFile: Promise<RoomEntity[]>;

    @OneToMany(() => resolveEntity<GuildFileEntity>('GuildFileEntity'), (guildFile) => guildFile.file)
    guildFiles: Promise<GuildFileEntity[]>;

    @ManyToMany(() => resolveEntity<GuildEntity>('GuildEntity'))
    @JoinTable({
        name: 'guild_files',
        joinColumn: {
            name: 'file_id',
        },
        inverseJoinColumn: {
            name: 'guild_id',
        },
    })
    guildsOfFile: Promise<GuildEntity[]>;

    @OneToMany(() => resolveEntity<SyncVisionEntity>('SyncVisionEntity'), (syncVision) => syncVision.file)
    syncVisions: Promise<SyncVisionEntity[]>;

    async transform(
        options?: {
            holder?: boolean | UserAttributes,
            filing?: FilingEntity,
            folder?: FileAttributes,
            syncVision?: SyncVisionEntity,
            roomFile?: RoomFileEntity,
            signedURL?: string,
            presignedURL?: string,
        }
    ): Promise<FileAttributes> {
        options ??= {};
        const [
            meta,
            thumbnail,
            holder,
            syncVision,
        ] = await Promise.all<(FolderEntity | ImageEntity | VideoEntity | EmojiEntity | undefined), FileAttributes | undefined, UserAttributes | undefined, SyncVisionAttributes | undefined>([
            (async () => await this.meta)(),
            (async () => await (await this.getThumbnail())?.transform())(),
            (async () => options.holder == true ? (await (await this.holder)?.transform()) : options.holder || undefined)(),
            (async () => await options.syncVision?.transform({ room: (await (await options.roomFile?.room)?.transform()) ?? false }))(),
        ]);
        return {
            ...(meta ?? {}),
            ...this,
            id: this.id,
            holderId: this.holderId,
            thumbnailId: thumbnail?.id,
            displayName: this.displayName,
            description: this.description,
            url: this.url,
            status: this.status,
            type: this.type,
            accessType: this.accessType,
            extension: this.extension,
            contentType: this.contentType,
            contentLength: this.contentLength,
            provider: this.provider,
            providerKey: this.providerKey,
            isSystem: this.isSystem,
            filingId: options.filing?.id,
            folderId: options.filing?.folderId ?? options.folder?.id,
            roomFileId: options.roomFile?.id,
            roomId: options.roomFile?.roomId,
            signedURL: options.signedURL,
            presignedURL: options.presignedURL,
            index: options.filing?.index,
            randomIndex: options.filing?.randomIndex,
            showInGuild: options.roomFile?.showInGuild,
            archivedAt: this.archivedAt?.toISOString(),
            deletedAt: this.deletedAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            thumbnail,
            holder,
            folder: options.folder,
            syncVision,
        };
    }

    async upload(attributes: Omit<FileAttributes, keyof FileRecordAttributes>): Promise<FileEntity> {
        if (this.isSystem) return this;
        this.contentLength = attributes.contentLength;
        this.status = FileStatus.UPLOADED;
        return this.save();
    }

    async updateForDelete(attributes: Omit<FileAttributes, keyof FileRecordAttributes>): Promise<FileEntity> {
        if (this.isSystem || this.status === FileStatus.DELETED) return this;
        this.deletedAt = new Date();
        this.status = FileStatus.DELETED;
        return this.save();
    }

    async fail(attributes: Omit<FileAttributes, keyof FileRecordAttributes>): Promise<FileEntity> {
        if (this.isSystem) return this;
        this.contentLength = 0;
        this.status = FileStatus.FAILED;
        return this.save();
    }

    get meta(): Promise<(
        FolderEntity |
        ImageEntity |
        VideoEntity |
        EmojiEntity |
        undefined
    )> {
        switch(this.type) {
        default:
        case FileType.Other: return new Promise(resolve => resolve(undefined));
        case FileType.Album:
        case FileType.Playlist:
        case FileType.Folder: return this.folder;
        case FileType.Image: return this.image;
        case FileType.Video: return this.video;
        case FileType.Emoji: return this.emoji;
        }
    }

    async getThumbnail(): Promise<(
        FileEntity |
        undefined
    )> {
        switch(this.type) {
        default: return undefined;
        case FileType.Video:
            const video = await this.video;
            if (!video) return undefined;
            return video.thumbnail;
        }
    }

    get contentTypeFromExtension(): ContentType {
        return transformFromExtension(this.extension);
    }

    get storagePath(): string {
        return trimStoragePath(this.providerKey);
    }

    get storagePathURL(): string {
        return trimStoragePath(this.providerKey);
    }

    get destroyable(): boolean {
        return !this.isSystem;
    }

    get isFolder(): boolean {
        return this.type === FileType.Folder || this.type === FileType.Album || this.type === FileType.Playlist;
    }
}