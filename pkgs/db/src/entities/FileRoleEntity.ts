import { FileAttributes, RoleAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { RoleEntity } from "./RoleEntity";

@Entity({ name: "file_roles" })
export class FileRoleEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "role_id" })
    roleId: string;

    @Column({ name: "file_id" })
    fileId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.fileRoles)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;

    @ManyToOne(() => resolveEntity<RoleEntity>('RoleEntity'), (role) => role.fileRoles)
    @JoinColumn({ name: "role_id" })
    role: Promise<RoleEntity>;

    async transform(
        options: {
            role?: boolean | RoleAttributes,
            file?: boolean | FileAttributes,
        } = {}
    ): Promise<RoleAttributes> {
        const role = await this.role;
        return await role.transform({
            fileRole: this,
        });
    }
}