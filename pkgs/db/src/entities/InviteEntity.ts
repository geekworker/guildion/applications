import { InviteStatus, InviteType, InviteAttributes, MemberAttributes, GuildAttributes, RoomAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildEntity } from "./GuildEntity";
import type { MemberEntity } from "./MemberEntity";
import type { RoomEntity } from "./RoomEntity";

@Entity({ name: "invites" })
export class InviteEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "member_id" })
    memberId: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "room_id", type: "uuid", nullable: true })
    roomId?: string | null;

    @Column({ name: "type" })
    type: InviteType;

    @Column({ name: "status" })
    status: InviteStatus;

    @Column({ name: "max_members_count", type: "integer", nullable: true })
    maxMembersCount?: number | null;

    @Column({ name: "current_members_count" })
    currentMembersCount: number;

    @Column({ name: "expired_at", type: 'timestamptz', nullable: true })
    expiredAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.invites)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity>;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.invites)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.invites)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity | undefined>;

    async transform(
        options: {
            member?: boolean | MemberAttributes,
            guild?: boolean | GuildAttributes,
            room?: boolean | RoomAttributes,
        } = {}
    ): Promise<InviteAttributes> {
        return {
            id: this.id,
            memberId: this.memberId,
            guildId: this.guildId,
            roomId: this.roomId,
            status: this.status,
            type: this.type,
            maxMembersCount: this.maxMembersCount,
            currentMembersCount: this.currentMembersCount,
            expiredAt: this.expiredAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            member: options.member == true ? (await (await this.member)?.transform()) : options.member || undefined,
            guild: options.guild == true ? (await (await this.guild)?.transform()) : options.guild || undefined,
            room: options.room == true ? (await (await this.room)?.transform()) : options.room || undefined,
        };
    }
}