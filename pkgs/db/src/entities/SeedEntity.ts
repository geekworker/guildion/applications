import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";
import { SEED_CHECKSUM } from "../config/checksum";

@Entity({ name: "seeds" })
export class SeedEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ type: "text" })
    checksum: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    static async checksumSeed(): Promise<boolean> {
        const checksum = SEED_CHECKSUM;
        const result = await SeedEntity.findOne({
            where: {
                checksum,
            },
        });
        return !!result;
    }

    static async updateChecksum(): Promise<boolean> {
        const checksum = SEED_CHECKSUM;
        const result = await SeedEntity.findOne({
            where: {
                checksum,
            },
        });
        if (!result) await SeedEntity.create({ checksum }).save();
        return !!result;
    }
}