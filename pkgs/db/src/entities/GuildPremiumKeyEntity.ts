import { PremiumKeyAttributes, GuildPremiumKeyStatus, GuildAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildEntity } from "./GuildEntity";
import type { PremiumKeyEntity } from "./PremiumKeyEntity";
import type { PremiumKeySubscriptionEntity } from "./PremiumKeySubscriptionEntity";

@Entity({ name: "guild_premium_keys" })
export class GuildPremiumKeyEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "guild_id" })
    guildId: string;

    @Column({ name: "premium_key_id" })
    premiumKeyId: string;

    @Column({ name: 'status' })
    status: GuildPremiumKeyStatus;

    @Column({ name: 'commission_rate' })
    commissionRate: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<GuildEntity>('GuildEntity'), (guild) => guild.guildPremiumKeys)
    @JoinColumn({ name: "guild_id" })
    guild: Promise<GuildEntity>;

    @ManyToOne(() => resolveEntity<PremiumKeyEntity>('PremiumKeyEntity'), (premiumKey) => premiumKey.guildPremiumKeys)
    @JoinColumn({ name: "premium_key_id" })
    premiumKey: Promise<PremiumKeyEntity>;

    @OneToMany(() => resolveEntity<PremiumKeySubscriptionEntity>('PremiumKeySubscriptionEntity'), (premiumKeySubscription) => premiumKeySubscription.guildPremiumKey)
    subscriptions: Promise<PremiumKeySubscriptionEntity[]>;

    async transform(options?: {
        guild?: boolean | GuildAttributes,
    }): Promise<PremiumKeyAttributes> {
        options ??= {};
        const guild = options.guild == true ? (await (await this.guild).transform()) : options.guild || undefined;
        const premiumKey = await this.premiumKey;
        return premiumKey.transform({
            guild,
            guildPremiumKey: this,
        });
    }
}