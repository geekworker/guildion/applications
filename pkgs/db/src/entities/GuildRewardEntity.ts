import { MemberAttributes, RewardMethod, GuildRewardAttributes, GuildRewardStatus } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { GuildRewardPaymentEntity } from "./GuildRewardPaymentEntity";
import type { MemberEntity } from "./MemberEntity";

@Entity({ name: "guild_rewards" })
export class GuildRewardEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: 'member_id' })
    memberId: string;

    @Column({ name: 'status' })
    status: GuildRewardStatus;

    @Column({ name: 'method' })
    method: RewardMethod;

    @Column({ name: 'provider_key' })
    providerKey: string;

    @Column({ name: 'commission_rate' })
    commissionRate: number;

    @Column({ name: "confirmed_at", type: "timestamptz", nullable: true })
    confirmedAt?: Date | null;

    @Column({ name: "actived_at", type: "timestamptz", nullable: true })
    activedAt?: Date | null;

    @Column({ name: "canceled_at", type: "timestamptz", nullable: true })
    canceledAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.rewards)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity>;

    @OneToMany(() => resolveEntity<GuildRewardPaymentEntity>('GuildRewardPaymentEntity'), (payment) => payment.reward)
    payments: Promise<GuildRewardPaymentEntity[]>;

    async transform(options?: {
        member?: MemberAttributes | boolean,
    }): Promise<GuildRewardAttributes> {
        options ??= {};
        const [
            member,
        ] = await Bluebird.all<MemberAttributes | undefined>([
            (async () => options.member == true ? ((await this.member)?.transform()) : options.member || undefined)(),
        ]);
        return {
            id: this.id,
            memberId: this.memberId,
            status: this.status,
            method: this.method,
            providerKey: this.providerKey,
            commissionRate: this.commissionRate,
            confirmedAt: this.confirmedAt?.toISOString(),
            activedAt: this.activedAt?.toISOString(),
            canceledAt: this.canceledAt?.toISOString(),
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            member,
        };
    }
}