import { RoleAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { MemberEntity } from "./MemberEntity";
import type { RoleEntity } from "./RoleEntity";

@Entity({ name: "member_roles" })
export class MemberRoleEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "member_id" })
    memberId: string;

    @Column({ name: "role_id" })
    roleId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<MemberEntity>('MemberEntity'), (member) => member.memberRoles)
    @JoinColumn({ name: "member_id" })
    member: Promise<MemberEntity>;

    @ManyToOne(() => resolveEntity<RoleEntity>('RoleEntity'), (role) => role.memberRoles)
    @JoinColumn({ name: "role_id" })
    role: Promise<RoleEntity>;

    async transform(options: {} = {}): Promise<RoleAttributes> {
        const role = await this.role;
        return await role.transform({
            memberRole: this,
        })
    }
}