import { ReportAttributes, ReportStatus, ReportType, UserAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    ManyToOne,
} from "typeorm";
import type { UserReportEntity } from "./UserReportEntity";
import type { FileReportEntity } from "./FileReportEntity";
import type { GuildReportEntity } from "./GuildReportEntity";
import type { RoomReportEntity } from "./RoomReportEntity";
import type { UserEntity } from "./UserEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "reports" })
export class ReportEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "sender_id" })
    senderId: string;

    @Column({ name: "status" })
    status: ReportStatus;

    @Column({ name: "type" })
    type: ReportType;

    @Column({ name: 'title' })
    title: string;

    @Column({ type: 'text' })
    body: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<UserEntity>('UserEntity'), (sender) => sender.reports)
    @JoinColumn({ name: "sender_id" })
    sender: Promise<UserEntity>;

    async transform(
        options: {
            sender?: boolean | UserAttributes,
            associated?: boolean,
        } = {}
    ): Promise<ReportAttributes> {
        const associatedIds = !!options.associated ? await this.findAssociatedReport() : {};
        return {
            id: this.id,
            senderId: this.senderId,
            status: this.status,
            type: this.type,
            title: this.title,
            body: this.body,
            ...associatedIds,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            sender: options.sender == true ? (await (await this.sender)?.transform()) : options.sender || undefined,
        };
    }


    async findAssociatedReport(): Promise<{
        fileId?: string,
        guildId?: string,
        roomId?: string,
        userId?: string,
    }> {
        switch(this.type) {
            case ReportType.File: return {
                fileId: (await resolveEntity<FileReportEntity>('FileReportEntity').findOne({ where: { reportId: this.id } }))?.id
            };
            case ReportType.User: return {
                userId: (await resolveEntity<UserReportEntity>('UserReportEntity').findOne({ where: { reportId: this.id } }))?.id
            };
            case ReportType.Guild: return {
                guildId: (await resolveEntity<GuildReportEntity>('GuildReportEntity').findOne({ where: { reportId: this.id } }))?.id
            };
            case ReportType.Room: return {
                roomId: (await resolveEntity<RoomReportEntity>('RoomReportEntity').findOne({ where: { reportId: this.id } }))?.id
            };
            default: return {}
        }
    }
}