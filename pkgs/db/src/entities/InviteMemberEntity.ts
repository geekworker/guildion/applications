import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
} from "typeorm";

@Entity({ name: "invite_members" })
export class InviteMemberEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "invite_id" })
    inviteId: string;

    @Column({ name: "member_id" })
    memberId: string;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;
}