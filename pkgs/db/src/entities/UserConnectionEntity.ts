import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    ManyToOne,
    JoinColumn,
    PrimaryColumn,
} from "typeorm";
import type { UserEntity } from "./UserEntity";
import type { ConnectionEntity } from "./ConnectionEntity";
import { monotonicFactory } from "ulid";
import { resolveEntity } from "../config/resolveEntity";

const monotonic = monotonicFactory();

@Entity({ name: "user_connections" })
export class UserConnectionEntity extends BaseEntity {
    @PrimaryColumn('varchar', { length: 128, default: () => `'${monotonic()}'` })
    id: string;

    @Column({ name: "user_id" })
    userId: string;

    @Column({ name: "connection_id" })
    connectionId: string;

    @Column({ name: "is_connecting" })
    isConnecting: boolean;

    @Column({ name: "connected_at", type: 'timestamptz', nullable: true })
    connectedAt?: Date | null;

    @Column({ name: "disconnected_at", type: 'timestamptz', nullable: true })
    disconnectedAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<UserEntity>('UserEntity'), (user) => user.userConnections)
    @JoinColumn({ name: "user_id" })
    user: Promise<UserEntity>;

    @ManyToOne(() => resolveEntity<ConnectionEntity>('ConnectionEntity'), (connection) => connection.userConnections)
    @JoinColumn({ name: "connection_id" })
    connection: Promise<ConnectionEntity>;

    public async disconnect(): Promise<UserConnectionEntity> {
        if (!this.isConnecting || this.disconnectedAt) return this;
        this.isConnecting = false;
        this.disconnectedAt = new Date();
        return await this.save()
    }
}