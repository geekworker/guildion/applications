import { AccessTokenStatus, Account } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    JoinColumn,
    ManyToOne,
} from "typeorm";
import type { DeviceEntity } from "./DeviceEntity";
import type { AccountEntity } from "./AccountEntity";
import { resolveEntity } from "../config/resolveEntity";

@Entity({ name: "access_tokens" })
export class AccessTokenEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "device_id" })
    deviceId: string;

    @Column({ name: "account_id" })
    accountId: string;

    @Column({ name: "secret", type: "varchar", nullable: true })
    secret?: string | null;

    @Column({ name: "status", default: AccessTokenStatus.NEW })
    status: AccessTokenStatus;

    @Column({ name: "expired_at", type: 'timestamptz', nullable: true })
    expiredAt?: Date | null;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => resolveEntity<DeviceEntity>('DeviceEntity'), (device) => device.accessTokens)
    @JoinColumn({ name: "device_id" })
    device: Promise<DeviceEntity>;

    @ManyToOne(() => resolveEntity<AccountEntity>('AccountEntity'), (account) => account.accessTokens)
    @JoinColumn({ name: "account_id" })
    account: Promise<AccountEntity>;
}