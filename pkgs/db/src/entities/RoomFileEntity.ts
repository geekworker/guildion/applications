import { FileAttributes, UserAttributes } from "@guildion/core";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    OneToMany,
    ManyToOne,
    JoinColumn,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { FileEntity } from "./FileEntity";
import type { FilingEntity } from "./FilingEntity";
import type { RoomEntity } from "./RoomEntity";
import type { RoomLogEntity } from "./RoomLogEntity";
import type { SyncVisionEntity } from "./SyncVisionEntity";

@Entity({ name: "room_files" })
export class RoomFileEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "room_id" })
    roomId: string;

    @Column({ name: "file_id" })
    fileId: string;

    @Column({ name: "show_in_guild" })
    showInGuild: boolean;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @OneToMany(() => resolveEntity<RoomLogEntity>('RoomLogEntity'), (log) => log.roomFile)
    logs: Promise<RoomLogEntity[]>;

    @ManyToOne(() => resolveEntity<RoomEntity>('RoomEntity'), (room) => room.roomFiles)
    @JoinColumn({ name: "room_id" })
    room: Promise<RoomEntity>;

    @ManyToOne(() => resolveEntity<FileEntity>('FileEntity'), (file) => file.roomFiles)
    @JoinColumn({ name: "file_id" })
    file: Promise<FileEntity>;

    async transform(
        options: {
            file?: FileEntity,
            holder?: boolean | UserAttributes,
            storage?: boolean | FileAttributes,
            filing?: FilingEntity,
            folder?: FileAttributes,
            syncVision?: SyncVisionEntity,
            signedURL?: string,
        } = {}
    ): Promise<FileAttributes> {
        const file = options.file ?? await this.file;
        return await file.transform({
            ...options,
            roomFile: this,
        });
    }
}