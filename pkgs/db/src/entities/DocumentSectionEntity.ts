import { DocumentSectionAttributes, DocumentSectionType, DocumentSectionStatus, DocumentGroupAttributes, DocumentAttributes } from "@guildion/core";
import Bluebird from "bluebird";
import {
    Entity,
    Column,
    BaseEntity,
    CreateDateColumn,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn,
    OneToMany,
} from "typeorm";
import { resolveEntity } from "../config/resolveEntity";
import type { DocumentEntity } from "./DocumentEntity";
import type { DocumentGroupEntity } from "./DocumentGroupEntity";

@Entity({ name: "document_sections" })
export class DocumentSectionEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column({ name: "section_id", type: "uuid", nullable: true })
    sectionId?: string | null;

    @Column({ name: "group_id", type: "uuid" })
    groupId: string;

    @Column({ name: 'slug' })
    slug: string;

    @Column({ name: 'ja_name' })
    jaName: string;

    @Column({ name: 'en_name' })
    enName: string;

    @Column({ name: "status" })
    status: DocumentSectionStatus;

    @Column({ name: "type" })
    type: DocumentSectionType;

    @Column({ name: "index" })
    index: number;

    @CreateDateColumn()
    @Column({ name: "created_at", type: "timestamptz" })
    readonly createdAt: Date;

    @Column({ name: "updated_at", type: "timestamptz", nullable: true })
    readonly updatedAt: Date;

    @ManyToOne(() => DocumentSectionEntity, (section) => section.sections)
    @JoinColumn({ name: "section_id" })
    section: Promise<DocumentSectionEntity | undefined>;

    @ManyToOne(() => resolveEntity<DocumentSectionEntity>('DocumentGroupEntity'), (group) => group.sections)
    @JoinColumn({ name: "group_id" })
    group: Promise<DocumentGroupEntity>;

    @OneToMany(() => DocumentSectionEntity, (section) => section.section)
    sections: Promise<DocumentSectionEntity[]>;

    @OneToMany(() => resolveEntity<DocumentEntity>('DocumentEntity'), (document) => document.section)
    documents: Promise<DocumentEntity[]>;

    async transform(options?: {
        section?: boolean | DocumentSectionAttributes,
        group?: boolean | DocumentGroupAttributes,
        sections?: boolean | DocumentSectionAttributes[],
        documents?: boolean | DocumentAttributes[],
    }): Promise<DocumentSectionAttributes> {
        options ??= {};
        const [
            section,
            group,
            sections,
            documents,
        ] = await Bluebird.all<DocumentSectionAttributes | undefined, DocumentGroupAttributes | undefined, DocumentSectionAttributes[] | undefined, DocumentAttributes[] | undefined>([
            (async () => options.section == true ? ((await this.section)?.transform()) : options.section || undefined)(),
            (async () => options.group == true ? ((await this.group)?.transform()) : options.group || undefined)(),
            (async () => options.sections == true ? (await Bluebird.all((await this.sections).map(v => v.transform()))) : options.sections || undefined)(),
            (async () => options.documents == true ? (await Bluebird.all((await this.documents).map(v => v.transform()))) : options.documents || undefined)(),
        ]);
        return {
            id: this.id,
            sectionId: this.sectionId,
            groupId: this.groupId,
            slug: this.slug,
            jaName: this.jaName,
            enName: this.enName,
            status: this.status,
            type: this.type,
            index: this.index,
            createdAt: this.createdAt?.toISOString(),
            updatedAt: this.updatedAt?.toISOString(),
            section,
            group,
            sections,
            documents,
        };
    }
}