import { createOrUpdateSeeds } from "../seeds";
import type { SeedOptions } from "../seeds";
import { createConnection, Connection } from "typeorm";
import { AccessTokenEntity } from '../entities/AccessTokenEntity';
import { AccountEntity } from '../entities/AccountEntity';
import { AdministratorEntity } from '../entities/AdministratorEntity';
import { BlogEntity } from '../entities/BlogEntity';
import { BlogCategoryEntity } from '../entities/BlogCategoryEntity';
import { ConnectionEntity } from '../entities/ConnectionEntity';
import { CSRFEntity } from '../entities/CSRFEntity';
import { DeviceBlockEntity } from '../entities/DeviceBlockEntity';
import { DeviceConnectionEntity } from '../entities/DeviceConnectionEntity';
import { DeviceEntity } from '../entities/DeviceEntity';
import { DiscoveryEntity } from '../entities/DiscoveryEntity';
import { DiscoveryGuildEntity } from '../entities/DiscoveryGuildEntity';
import { DiscoveryRoomEntity } from '../entities/DiscoveryRoomEntity';
import { DocumentEntity } from '../entities/DocumentEntity';
import { DocumentGroupEntity } from '../entities/DocumentGroupEntity';
import { DocumentSectionEntity } from '../entities/DocumentSectionEntity';
import { EmojiEntity } from '../entities/EmojiEntity';
import { FileEntity } from '../entities/FileEntity';
import { FileReportEntity } from '../entities/FileReportEntity';
import { FileRoleEntity } from '../entities/FileRoleEntity';
import { FilingEntity } from '../entities/FilingEntity';
import { FolderEntity } from '../entities/FolderEntity';
import { GuildActivityEntity } from '../entities/GuildActivityEntity';
import { GuildBlockEntity } from '../entities/GuildBlockEntity';
import { GuildCategoryEntity } from '../entities/GuildCategoryEntity';
import { GuildCategoryListEntity } from '../entities/GuildCategoryListEntity';
import { GuildEntity } from '../entities/GuildEntity';
import { GuildFileEntity } from '../entities/GuildFileEntity';
import { GuildPremiumKeyEntity } from '../entities/GuildPremiumKeyEntity';
import { GuildReportEntity } from '../entities/GuildReportEntity';
import { GuildRewardEntity } from '../entities/GuildRewardEntity';
import { GuildRewardPaymentEntity } from '../entities/GuildRewardPaymentEntity';
import { GuildStorageEntity } from '../entities/GuildStorageEntity';
import { ImageEntity } from '../entities/ImageEntity';
import { InviteEntity } from '../entities/InviteEntity';
import { InviteMemberEntity } from '../entities/InviteMemberEntity';
import { MemberConnectionEntity } from '../entities/MemberConnectionEntity';
import { MemberEntity } from '../entities/MemberEntity';
import { MemberRoleEntity } from '../entities/MemberRoleEntity';
import { MessageAttachmentEntity } from '../entities/MessageAttachmentEntity';
import { MessageEntity } from '../entities/MessageEntity';
import { MessageReactionEntity } from '../entities/MessageReactionEntity';
import { MessageReadEntity } from '../entities/MessageReadEntity';
import { NotificationEntity } from '../entities/NotificationEntity';
import { PostAttachmentEntity } from '../entities/PostAttachmentEntity';
import { PostEntity } from '../entities/PostEntity';
import { PostReactionEntity } from '../entities/PostReactionEntity';
import { PostReadEntity } from '../entities/PostReadEntity';
import { PremiumKeyEntity } from '../entities/PremiumKeyEntity';
import { PremiumKeySubscriptionEntity } from '../entities/PremiumKeySubscriptionEntity';
import { PremiumKeySubscriptionPaymentEntity } from '../entities/PremiumKeySubscriptionPaymentEntity';
import { RoleEntity } from '../entities/RoleEntity';
import { ReportEntity } from '../entities/ReportEntity';
import { ReportReadEntity } from '../entities/ReportReadEntity';
import { RoomActivityEntity } from '../entities/RoomActivityEntity';
import { RoomBlockEntity } from '../entities/RoomBlockEntity';
import { RoomCategoryEntity } from '../entities/RoomCategoryEntity';
import { RoomCategoryListEntity } from '../entities/RoomCategoryListEntity';
import { RoomEntity } from '../entities/RoomEntity';
import { RoomFileEntity } from '../entities/RoomFileEntity';
import { RoomLogEntity } from '../entities/RoomLogEntity';
import { RoomMemberConnectionEntity } from '../entities/RoomMemberConnectionEntity';
import { RoomMemberEntity } from '../entities/RoomMemberEntity';
import { RoomReportEntity } from '../entities/RoomReportEntity';
import { RoomRoleEntity } from '../entities/RoomRoleEntity';
import { RoomStorageEntity } from '../entities/RoomStorageEntity';
import { ScenarioContextEntity } from '../entities/ScenarioContextEntity';
import { SeedEntity } from '../entities/SeedEntity';
import { SyncVisionEntity } from '../entities/SyncVisionEntity';
import { UserConnectionEntity } from '../entities/UserConnectionEntity';
import { UserDeviceEntity } from '../entities/UserDeviceEntity';
import { UserEntity } from '../entities/UserEntity';
import { UserReportEntity } from '../entities/UserReportEntity';
import { VideoEntity } from '../entities/VideoEntity';
import { NewsLetterSubscriberEntity } from '../entities/NewsLetterSubscriberEntity';

export type ConnectionParams = {
    logging: boolean,
    DATABASE_USERNAME: string,
    DATABASE_PASSWORD?: string,
    DATABASE_NAME: string,
    DATABASE_HOST: string,
    DATABASE_PORT: number,
    options?: SeedOptions,
}

export let params: ConnectionParams;
export let connection: Connection;

export const connect = async (_params: ConnectionParams) => {
    params = _params;
    connection = await createConnection({
        type: "postgres",
        host: params.DATABASE_HOST,
        port: params.DATABASE_PORT || 5432,
        username: params.DATABASE_USERNAME,
        password: params.DATABASE_PASSWORD,
        database: params.DATABASE_NAME,
        entities: [
            AccessTokenEntity,
            AccountEntity,
            AdministratorEntity,
            BlogEntity,
            BlogCategoryEntity,
            ConnectionEntity,
            CSRFEntity,
            DeviceBlockEntity,
            DeviceConnectionEntity,
            DeviceEntity,
            DiscoveryEntity,
            DiscoveryGuildEntity,
            DiscoveryRoomEntity,
            DocumentEntity,
            DocumentGroupEntity,
            DocumentSectionEntity,
            EmojiEntity,
            FileEntity,
            FileReportEntity,
            FileRoleEntity,
            FilingEntity,
            FolderEntity,
            GuildActivityEntity,
            GuildBlockEntity,
            GuildCategoryEntity,
            GuildCategoryListEntity,
            GuildEntity,
            GuildFileEntity,
            GuildPremiumKeyEntity,
            GuildReportEntity,
            GuildRewardEntity,
            GuildRewardPaymentEntity,
            GuildStorageEntity,
            ImageEntity,
            InviteEntity,
            InviteMemberEntity,
            MemberConnectionEntity,
            MemberEntity,
            MemberRoleEntity,
            MessageAttachmentEntity,
            MessageEntity,
            MessageReactionEntity,
            MessageReadEntity,
            NewsLetterSubscriberEntity,
            NotificationEntity,
            PostAttachmentEntity,
            PostEntity,
            PostReactionEntity,
            PostReadEntity,
            PremiumKeyEntity,
            PremiumKeySubscriptionEntity,
            PremiumKeySubscriptionPaymentEntity,
            RoleEntity,
            ReportEntity,
            ReportReadEntity,
            RoomActivityEntity,
            RoomBlockEntity,
            RoomCategoryEntity,
            RoomCategoryListEntity,
            RoomEntity,
            RoomFileEntity,
            RoomLogEntity,
            RoomMemberConnectionEntity,
            RoomMemberEntity,
            RoomReportEntity,
            RoomRoleEntity,
            RoomStorageEntity,
            ScenarioContextEntity,
            SeedEntity,
            SyncVisionEntity,
            UserConnectionEntity,
            UserDeviceEntity,
            UserEntity,
            UserReportEntity,
            VideoEntity,
        ],
        logging: params.logging,
    });
    await createOrUpdateSeeds(params.options);
};