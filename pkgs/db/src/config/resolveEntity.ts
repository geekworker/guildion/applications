import { BaseEntity } from "typeorm";
import { BaseEntityClass, EntityName } from "../types";

export const resolveEntity = <E extends BaseEntity>(name: EntityName): BaseEntityClass<E> => {
    return require(`../entities/${name}`)[name];
}