CREATE TABLE device_connections (
    id varchar(128) NOT NULL PRIMARY KEY DEFAULT generate_ulid(),
    device_id uuid NOT NULL REFERENCES devices(id) ON UPDATE CASCADE ON DELETE CASCADE,
    connection_id varchar(128) NOT NULL REFERENCES connections(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_device_connections_tri BEFORE UPDATE ON device_connections for each ROW EXECUTE PROCEDURE set_update_time();