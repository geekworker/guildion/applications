CREATE OR REPLACE FUNCTION check_folder_key(folder_id uuid)
RETURNS int
AS $$
BEGIN
   IF EXISTS (
       SELECT 1
       FROM files
       WHERE id = folder_id AND (type = 'folder' OR type = 'playlist' OR type = 'album')
    ) THEN
       return 1;
    END IF;
    RETURN 0;
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE TABLE filings (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    folder_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    index integer NOT NULL DEFAULT 0,
    random_index integer,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT check_filing_folder_fk CHECK (check_folder_key(folder_id) = 1)
);

CREATE trigger update_filings_tri BEFORE UPDATE ON filings for each ROW EXECUTE PROCEDURE set_update_time();
CREATE INDEX filings_index_idx ON filings (index);
CREATE INDEX filings_random_index_idx ON filings (random_index);