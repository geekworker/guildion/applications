CREATE TYPE account_status AS ENUM (
    'CREATED', 'VERIFIED', 'LOCKED', 'DELETING', 'DELETED'
);

CREATE TYPE account_session_status AS ENUM (
    'REJECTED', 'ALLOWED'
);

CREATE TABLE accounts (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status account_status NOT NULL DEFAULT 'CREATED',
    session_status account_session_status NOT NULL DEFAULT 'REJECTED',
    session_allowed_interval_sec integer,
    session_allowed_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger accounts_tri BEFORE UPDATE ON accounts for each ROW EXECUTE PROCEDURE set_update_time();