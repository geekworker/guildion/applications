CREATE TYPE document_section_type AS ENUM (
    'default',
    'all_in_one'
);

CREATE TYPE document_section_status AS ENUM (
    'DRAFT',
    'PUBLISH'
);

CREATE TABLE document_sections (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    group_id uuid NOT NULL REFERENCES document_groups(id) ON UPDATE CASCADE ON DELETE CASCADE,
    section_id uuid REFERENCES document_sections(id) ON UPDATE CASCADE ON DELETE SET NULL,
    slug varchar(768) NOT NULL,
    ja_name varchar(768) NOT NULL DEFAULT '',
    en_name varchar(768) NOT NULL DEFAULT '',
    type document_section_type NOT NULL DEFAULT 'default',
    status document_section_status NOT NULL DEFAULT 'DRAFT',
    index integer NOT NULL DEFAULT 0,
    published_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT document_sections_unique_fk UNIQUE (group_id, section_id, slug)
);

CREATE trigger document_sections_tri BEFORE UPDATE ON document_sections for each ROW EXECUTE PROCEDURE set_update_time();