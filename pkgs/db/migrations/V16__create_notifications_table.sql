
CREATE TYPE notification_type AS ENUM (
    'welcome', 'room_log'
);

CREATE TYPE notification_status AS ENUM (
    'CREATED', 'NOTIFIED', 'CHECKED'
);

CREATE TABLE notifications (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    receiver_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    type notification_type,
    status notification_status,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger notifications_tri BEFORE UPDATE ON notifications for each ROW EXECUTE PROCEDURE set_update_time();