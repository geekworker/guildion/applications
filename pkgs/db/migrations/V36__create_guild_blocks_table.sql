CREATE TABLE guild_blocks (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    receiver_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT guild_blocks_unique_fk UNIQUE (guild_id, receiver_id)
);

CREATE trigger guild_blocks_tri BEFORE UPDATE ON guild_blocks for each ROW EXECUTE PROCEDURE set_update_time();