CREATE TYPE sync_vision_status AS ENUM (
    'STARTED', 'PLAYING', 'PAUSED', 'FINISHED'
);

CREATE TABLE sync_visions (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    filing_id uuid REFERENCES filings(id) ON UPDATE CASCADE ON DELETE SET NULL,
    status sync_vision_status NOT NULL DEFAULT 'STARTED',
    offset_duration_ms integer NOT NULL DEFAULT 0,
    connected_at timestamptz,
    disconnected_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_sync_visions_tri BEFORE UPDATE ON sync_visions for each ROW EXECUTE PROCEDURE set_update_time();