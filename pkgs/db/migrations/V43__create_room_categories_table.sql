CREATE TABLE room_categories (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    name varchar(768) NOT NULL DEFAULT '',
    description text NOT NULL DEFAULT '',
    index integer NOT NULL DEFAULT 0,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger room_categories_tri BEFORE UPDATE ON room_categories for each ROW EXECUTE PROCEDURE set_update_time();
CREATE INDEX room_categories_index_idx ON room_categories (index);