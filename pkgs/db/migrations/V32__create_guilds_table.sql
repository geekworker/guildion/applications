CREATE TYPE guild_status AS ENUM (
    'CREATED', 'BANNED', 'LOCKED', 'DELETING', 'DELETED'
);

CREATE TYPE guild_type AS ENUM (
    'PUBLIC', 'PRIVATE', 'INDIVIDUAL', 'OFFICIAL'
);

CREATE TABLE guilds (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    owner_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    profile_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    type guild_type NOT NULL DEFAULT 'PUBLIC',
    status guild_status NOT NULL DEFAULT 'CREATED',
    display_name varchar(768) NOT NULL DEFAULT generate_ulid(),
    description text NOT NULL DEFAULT '',
    guildname namekey UNIQUE NOT NULL DEFAULT '',
    language_code language_code NOT NULL DEFAULT 'en',
    country_code country_code NOT NULL DEFAULT 'US',
    max_content_length integer,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guilds_tri BEFORE UPDATE ON guilds for each ROW EXECUTE PROCEDURE set_update_time();