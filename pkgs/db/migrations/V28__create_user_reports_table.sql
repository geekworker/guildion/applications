CREATE TABLE user_reports (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    report_id uuid NOT NULL REFERENCES reports(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT user_reports_unique_fk UNIQUE (user_id, report_id)
);

CREATE trigger user_reports_tri BEFORE UPDATE ON user_reports for each ROW EXECUTE PROCEDURE set_update_time();