CREATE TYPE room_status AS ENUM (
    'CREATED', 'BANNED', 'LOCKED', 'DELETING', 'DELETED'
);

CREATE TYPE room_type AS ENUM (
    'PUBLIC', 'PRIVATE', 'DM', 'INDIVIDUAL', 'OFFICIAL'
);

CREATE TABLE rooms (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    profile_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    owner_id uuid REFERENCES members(id) ON UPDATE CASCADE ON DELETE SET NULL,
    type room_type NOT NULL DEFAULT 'PUBLIC',
    status room_status NOT NULL DEFAULT 'CREATED',
    display_name varchar(768) NOT NULL DEFAULT generate_ulid(),
    description text NOT NULL DEFAULT '',
    roomname namekey UNIQUE NOT NULL DEFAULT '',
    language_code language_code NOT NULL DEFAULT 'en',
    country_code country_code NOT NULL DEFAULT 'US',
    max_content_length integer,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger rooms_tri BEFORE UPDATE ON rooms for each ROW EXECUTE PROCEDURE set_update_time();