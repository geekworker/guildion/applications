CREATE TABLE discovery_rooms (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    discovery_id uuid NOT NULL REFERENCES discoveries(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT discovery_rooms_unique_fk UNIQUE (discovery_id, room_id)
);

CREATE trigger discovery_rooms_tri BEFORE UPDATE ON discovery_rooms for each ROW EXECUTE PROCEDURE set_update_time();