CREATE TYPE guild_reward_payment_status AS ENUM (
    'NOT_YET_PAID',
    'PAID',
    'CANCELED'
);

CREATE TABLE guild_reward_payments (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    reward_id uuid NOT NULL REFERENCES guild_rewards(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    status guild_reward_payment_status NOT NULL DEFAULT 'NOT_YET_PAID',
    method reward_method_type NOT NULL,
    provider_key varchar(768) NOT NULL,
    requested_at timestamptz,
    pay_at timestamptz,
    paid_at timestamptz,
    canceled_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guild_reward_payments_tri BEFORE UPDATE ON guild_reward_payments for each ROW EXECUTE PROCEDURE set_update_time();