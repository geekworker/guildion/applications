CREATE TABLE csrfs (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    device_id uuid NOT NULL REFERENCES devices(id) ON UPDATE CASCADE ON DELETE CASCADE,
    secret varchar(768),
    hash varchar(768) NOT NULL DEFAULT '',
    salt varchar(768) NOT NULL DEFAULT '',
    iv varchar(768) NOT NULL DEFAULT '',
    is_active boolean NOT NULL DEFAULT false,
    expired_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger csrfs_tri BEFORE UPDATE ON csrfs for each ROW EXECUTE PROCEDURE set_update_time();