CREATE TABLE seeds (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    checksum text NOT NULL,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger seeds_tri BEFORE UPDATE ON seeds for each ROW EXECUTE PROCEDURE set_update_time();