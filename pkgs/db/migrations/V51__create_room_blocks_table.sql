CREATE TABLE room_blocks (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    receiver_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT room_blocks_unique_fk UNIQUE (room_id, receiver_id)
);

CREATE trigger room_blocks_tri BEFORE UPDATE ON room_blocks for each ROW EXECUTE PROCEDURE set_update_time();