CREATE TYPE role_type AS ENUM (
    'members',
    'trial',
    'premium',
    'creator',
    'admin',
    'owner'
);

CREATE TABLE roles (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    type role_type NOT NULL DEFAULT 'members',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger roles_tri BEFORE UPDATE ON roles for each ROW EXECUTE PROCEDURE set_update_time();