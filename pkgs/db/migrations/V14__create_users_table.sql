CREATE TYPE user_status AS ENUM (
    'CREATED', 'VERIFIED', 'LOCKED', 'DELETING', 'DELETED'
);

CREATE TABLE users (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    profile_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status user_status,
    username namekey UNIQUE NOT NULL DEFAULT generate_ulid(),
    display_name varchar(768) NOT NULL,
    description text NOT NULL DEFAULT '',
    language_code language_code NOT NULL DEFAULT 'en',
    country_code country_code NOT NULL DEFAULT 'US',
    timezone timezone_id NOT NULL DEFAULT 'America/Los_Angeles',
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_users_tri BEFORE UPDATE ON users for each ROW EXECUTE PROCEDURE set_update_time();