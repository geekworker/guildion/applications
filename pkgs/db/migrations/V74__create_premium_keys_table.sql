CREATE TYPE premium_key_status AS ENUM (
    'DRAFT',
    'PUBLISH',
    'PENDING',
    'DELETED'
);

CREATE TABLE premium_keys (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    ja_name varchar(768) NOT NULL DEFAULT '',
    en_name varchar(768) NOT NULL DEFAULT '',
    ja_description text NOT NULL DEFAULT '',
    en_description text NOT NULL DEFAULT '',
    storage_content_length integer,
    base_price integer NOT NULL DEFAULT 0,
    web_commission_price integer NOT NULL DEFAULT 0,
    ios_commission_price integer NOT NULL DEFAULT 0,
    android_commission_price integer NOT NULL DEFAULT 0,
    web_provider_key varchar(768),
    ios_provider_key varchar(768),
    android_provider_key varchar(768),
    web_status premium_key_status NOT NULL DEFAULT 'DRAFT',
    ios_status premium_key_status NOT NULL DEFAULT 'DRAFT',
    android_status premium_key_status NOT NULL DEFAULT 'DRAFT',
    interval subscription_interval_type NOT NULL DEFAULT 'monthly',
    published_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger premium_keys_tri BEFORE UPDATE ON premium_keys for each ROW EXECUTE PROCEDURE set_update_time();