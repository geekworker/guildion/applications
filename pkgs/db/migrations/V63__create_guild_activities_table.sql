CREATE TABLE guild_activities (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    connects_count integer NOT NULL DEFAULT 0,
    rooms_count integer NOT NULL DEFAULT 0,
    public_rooms_count integer NOT NULL DEFAULT 0,
    posts_count integer NOT NULL DEFAULT 0,
    public_posts_count integer NOT NULL DEFAULT 0,
    members_count integer NOT NULL DEFAULT 0,
    content_length integer NOT NULL DEFAULT 0,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guild_activities_tri BEFORE UPDATE ON guild_activities for each ROW EXECUTE PROCEDURE set_update_time();