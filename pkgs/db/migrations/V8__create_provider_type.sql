CREATE TYPE provider AS ENUM (
    'other',
    's3',
    'connect',
    'onesignal',
    'guildion'
);