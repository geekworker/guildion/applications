CREATE TYPE webrtc_status AS ENUM (
    'CONNECTING', 'VOICE', 'VIDEO', 'SCREENSHARE', 'VOICE_VIDEO', 'VOICE_SCREENSHARE', 'DISCONNECTED'
);

CREATE TABLE room_member_connections (
    id varchar(128) NOT NULL PRIMARY KEY DEFAULT generate_ulid(),
    room_member_id uuid NOT NULL REFERENCES room_members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    connection_id varchar(128) NOT NULL REFERENCES connections(id) ON UPDATE CASCADE ON DELETE CASCADE,
    is_connecting boolean NOT NULL DEFAULT FALSE,
    status webrtc_status NOT NULL DEFAULT 'CONNECTING',
    connected_at timestamptz,
    disconnected_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_room_member_connections_tri BEFORE UPDATE ON room_member_connections for each ROW EXECUTE PROCEDURE set_update_time();