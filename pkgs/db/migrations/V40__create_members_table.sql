CREATE TYPE member_status AS ENUM (
    'CREATED', 'BANNED', 'LOCKED', 'DELETED', 'GUEST'
);

CREATE TABLE members (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    profile_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status member_status NOT NULL DEFAULT 'CREATED',
    display_name varchar(768) NOT NULL DEFAULT '',
    description text NOT NULL DEFAULT '',
    index integer NOT NULL DEFAULT 0,
    is_mute boolean,
    is_mute_mentions boolean,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT members_unique_fk UNIQUE (user_id, guild_id)
);

CREATE trigger members_tri BEFORE UPDATE ON members for each ROW EXECUTE PROCEDURE set_update_time();
CREATE INDEX members_index_idx ON members (index);