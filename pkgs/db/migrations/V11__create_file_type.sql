CREATE TYPE file_type AS ENUM (
    'video',
    'music',
    'image',
    'youtube',
    'emoji',
    'folder',
    'playlist',
    'album',
    'other'
);