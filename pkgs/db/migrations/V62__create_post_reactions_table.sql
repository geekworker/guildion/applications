CREATE TABLE post_reactions (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    post_id uuid NOT NULL REFERENCES posts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    sender_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT post_reactions_unique_fk UNIQUE (post_id, file_id, sender_id)
);

CREATE trigger post_reactions_tri BEFORE UPDATE ON post_reactions for each ROW EXECUTE PROCEDURE set_update_time();