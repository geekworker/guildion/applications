CREATE TABLE connections (
    id varchar(128) NOT NULL PRIMARY KEY DEFAULT generate_ulid(),
    is_connecting boolean NOT NULL DEFAULT FALSE,
    sec_websocket_key varchar(768) NOT NULL,
    provider provider NOT NULL DEFAULT 'connect',
    connected_at timestamptz,
    disconnected_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger connections_tri BEFORE UPDATE ON connections for each ROW EXECUTE PROCEDURE set_update_time();