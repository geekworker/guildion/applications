CREATE TABLE member_connections (
    id varchar(128) NOT NULL PRIMARY KEY DEFAULT generate_ulid(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    connection_id varchar(128) NOT NULL REFERENCES connections(id) ON UPDATE CASCADE ON DELETE CASCADE,
    is_connecting boolean NOT NULL DEFAULT FALSE,
    connected_at timestamptz,
    disconnected_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_member_connections_tri BEFORE UPDATE ON member_connections for each ROW EXECUTE PROCEDURE set_update_time();