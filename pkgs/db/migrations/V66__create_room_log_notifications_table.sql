CREATE TABLE room_log_notifications (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_log_id uuid NOT NULL REFERENCES room_logs(id) ON UPDATE CASCADE ON DELETE CASCADE,
    notification_id uuid REFERENCES notifications(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT room_log_notifications_unique_fk UNIQUE (room_log_id, notification_id)
);

CREATE trigger room_log_notifications_tri BEFORE UPDATE ON room_log_notifications for each ROW EXECUTE PROCEDURE set_update_time();