CREATE TYPE file_status AS ENUM (
    'UPLOADING', 'UPLOADED', 'FAILED', 'LOCKED', 'ARCHIVED', 'DELETING', 'DELETED'
);

CREATE TYPE file_access_type AS ENUM (
    'PUBLIC', 'PRIVATE'
);

CREATE TABLE files (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    display_name varchar(768) NOT NULL DEFAULT '',
    description text NOT NULL DEFAULT '',
    url url UNIQUE NOT NULL,
    content_length integer NOT NULL DEFAULT 0,
    extension file_extension NOT NULL DEFAULT 'other',
    content_type content_type NOT NULL DEFAULT 'other',
    status file_status NOT NULL DEFAULT 'FAILED',
    type file_type NOT NULL DEFAULT 'other',
    access_type file_access_type NOT NULL DEFAULT 'PUBLIC',
    is_system boolean NOT NULL DEFAULT FALSE,
    provider provider NOT NULL DEFAULT 'other',
    provider_key varchar(768) NOT NULL DEFAULT '',
    archived_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_files_tri BEFORE UPDATE ON files for each ROW EXECUTE PROCEDURE set_update_time();