CREATE TABLE message_reactions (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    message_id uuid NOT NULL REFERENCES messages(id) ON UPDATE CASCADE ON DELETE CASCADE,
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    sender_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT message_reactions_unique_fk UNIQUE (message_id, file_id, sender_id)
);

CREATE trigger message_reactions_tri BEFORE UPDATE ON message_reactions for each ROW EXECUTE PROCEDURE set_update_time();