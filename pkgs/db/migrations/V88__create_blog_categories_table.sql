CREATE TYPE blog_category_status AS ENUM (
    'DRAFT',
    'PUBLISH'
);

CREATE TABLE blog_categories (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    parent_id uuid REFERENCES blog_categories(id) ON UPDATE CASCADE ON DELETE CASCADE,
    ja_name varchar(768) NOT NULL DEFAULT '',
    en_name varchar(768) NOT NULL DEFAULT '',
    slug varchar(768) NOT NULL UNIQUE,
    index integer NOT NULL DEFAULT 0,
    status blog_category_status NOT NULL DEFAULT 'DRAFT',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger blog_categories_tri BEFORE UPDATE ON blog_categories for each ROW EXECUTE PROCEDURE set_update_time();
CREATE INDEX blog_categories_index_idx ON blog_categories (index);