CREATE TABLE guild_reports (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    report_id uuid NOT NULL REFERENCES reports(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT guild_reports_unique_fk UNIQUE (guild_id, report_id)
);

CREATE trigger guild_reports_tri BEFORE UPDATE ON guild_reports for each ROW EXECUTE PROCEDURE set_update_time();