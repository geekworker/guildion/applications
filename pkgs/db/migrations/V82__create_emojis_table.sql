CREATE OR REPLACE FUNCTION check_emoji_relation(new_file_id uuid)
RETURNS int
AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM files
        WHERE id = new_file_id AND type = 'emoji'
    ) THEN
        return 1;
    END IF;
    RETURN 0;
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE OR REPLACE FUNCTION before_check_emoji_file()
RETURNS trigger
AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM emojis
        WHERE file_id = NEW.id
    ) AND NEW.type = 'emoji' THEN
        RETURN NEW;
    ELSE
        IF NOT EXISTS (
            SELECT 1
            FROM emojis
            WHERE file_id = NEW.id
        ) THEN
            RETURN NEW;
        END IF;
    END IF;
    RAISE EXCEPTION USING ERRCODE = '20100', MESSAGE = 'You can not update file type before changing meta table';
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE TABLE emojis (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id uuid NOT NULL UNIQUE REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    content emoji NOT NULL,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_emojis_tri BEFORE UPDATE ON emojis for each ROW EXECUTE PROCEDURE set_update_time();
CREATE trigger update_file_emojis_tri BEFORE UPDATE ON files for each ROW EXECUTE PROCEDURE before_check_emoji_file();
ALTER TABLE emojis ADD CONSTRAINT check_emoji_file_fk CHECK (check_emoji_relation(file_id) = 1);