CREATE TYPE administrator_role AS ENUM (
    'OWNER',
    'CONNECT_SERVER',
    'ADMIN',
    'MEMBER'
);

CREATE TABLE administrators (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL UNIQUE REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    role administrator_role NOT NULL DEFAULT 'MEMBER',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger administrators_tri BEFORE UPDATE ON administrators for each ROW EXECUTE PROCEDURE set_update_time();