CREATE TABLE room_activities (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    log_id uuid REFERENCES room_logs(id) ON UPDATE CASCADE ON DELETE SET NULL,
    sync_vision_id uuid REFERENCES sync_visions(id) ON UPDATE CASCADE ON DELETE SET NULL,
    connects_count integer NOT NULL DEFAULT 0,
    members_count integer NOT NULL DEFAULT 0,
    posts_count integer NOT NULL DEFAULT 0,
    public_posts_count integer NOT NULL DEFAULT 0,
    content_length integer NOT NULL DEFAULT 0,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger room_activities_tri BEFORE UPDATE ON room_activities for each ROW EXECUTE PROCEDURE set_update_time();