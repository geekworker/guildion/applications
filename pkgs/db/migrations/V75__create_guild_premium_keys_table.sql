CREATE TYPE guild_premium_key_status AS ENUM (
    'DRAFT',
    'PUBLISH',
    'PENDING',
    'DELETING',
    'DELETED'
);

CREATE TABLE guild_premium_keys (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    premium_key_id uuid NOT NULL REFERENCES premium_keys(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    status guild_premium_key_status NOT NULL DEFAULT 'DRAFT',
    commission_rate NUMERIC(3,2) NOT NULL DEFAULT 0.00,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT guild_premium_keys_unique_fk UNIQUE (guild_id, premium_key_id)
);

CREATE trigger guild_premium_keys_tri BEFORE UPDATE ON guild_premium_keys for each ROW EXECUTE PROCEDURE set_update_time();