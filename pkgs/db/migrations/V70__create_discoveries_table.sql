CREATE TYPE discovery_type AS ENUM (
    'default',
    'guild',
    'guilds'
);

CREATE TYPE discovery_data_type AS ENUM (
    'html',
    'url'
);

CREATE TYPE discovery_status AS ENUM (
    'DRAFT',
    'PUBLISH'
);

CREATE TABLE discoveries (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    ja_thumbnail_id uuid REFERENCES files(id) ON UPDATE CASCADE ON DELETE SET NULL,
    en_thumbnail_id uuid REFERENCES files(id) ON UPDATE CASCADE ON DELETE SET NULL,
    slug varchar(768) NOT NULL UNIQUE,
    ja_title varchar(768) NOT NULL DEFAULT '',
    en_title varchar(768) NOT NULL DEFAULT '',
    ja_description text NOT NULL DEFAULT '',
    en_description text NOT NULL DEFAULT '',
    ja_data text NOT NULL DEFAULT '',
    en_data text NOT NULL DEFAULT '',
    type discovery_type NOT NULL DEFAULT 'default',
    data_type discovery_data_type NOT NULL DEFAULT 'html',
    status discovery_status NOT NULL DEFAULT 'DRAFT',
    score integer NOT NULL DEFAULT 0,
    published_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger discoveries_tri BEFORE UPDATE ON discoveries for each ROW EXECUTE PROCEDURE set_update_time();