CREATE TABLE member_roles (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    role_id uuid NOT NULL REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT member_roles_unique_fk UNIQUE (role_id, member_id)
);

CREATE trigger member_roles_tri BEFORE UPDATE ON member_roles for each ROW EXECUTE PROCEDURE set_update_time();