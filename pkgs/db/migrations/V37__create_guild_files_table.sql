CREATE TABLE guild_files (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id uuid NOT NULL UNIQUE REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guild_files_tri BEFORE UPDATE ON guild_files for each ROW EXECUTE PROCEDURE set_update_time();