CREATE TYPE invite_status AS ENUM (
    'CREATED', 'OPEN', 'EXCEEDED', 'EXPIRED'
);

CREATE TYPE invite_type AS ENUM (
    'guild', 'room'
);

CREATE TABLE invites (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_id uuid REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status invite_status NOT NULL DEFAULT 'CREATED',
    type invite_type NOT NULL DEFAULT 'guild',
    max_members_count integer,
    current_members_count integer NOT NULL DEFAULT 0,
    expired_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT invites_guild_type CHECK (room_id IS NULL AND type = 'guild'),
    CONSTRAINT invites_room_type CHECK (room_id IS NOT NULL AND type = 'room')
);

CREATE trigger invites_tri BEFORE UPDATE ON invites for each ROW EXECUTE PROCEDURE set_update_time();