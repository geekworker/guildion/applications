CREATE TABLE device_blocks (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    device_id uuid NOT NULL REFERENCES devices(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,
    CONSTRAINT device_blocks_unique_fk UNIQUE (device_id, user_id)
);

CREATE trigger device_blocks_tri BEFORE UPDATE ON device_blocks for each ROW EXECUTE PROCEDURE set_update_time();