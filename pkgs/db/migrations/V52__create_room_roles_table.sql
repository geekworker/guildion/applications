CREATE TABLE room_roles (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    role_id uuid NOT NULL REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT room_roles_unique_fk UNIQUE (role_id, room_id)
);

CREATE trigger room_roles_tri BEFORE UPDATE ON room_roles for each ROW EXECUTE PROCEDURE set_update_time();