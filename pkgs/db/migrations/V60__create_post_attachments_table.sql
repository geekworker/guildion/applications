CREATE TABLE post_attachments (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    post_id uuid NOT NULL REFERENCES posts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT post_attachments_unique_fk UNIQUE (post_id, file_id)
);

CREATE trigger post_attachments_tri BEFORE UPDATE ON post_attachments for each ROW EXECUTE PROCEDURE set_update_time();