CREATE TYPE document_group_type AS ENUM (
    'default'
);

CREATE TYPE document_group_status AS ENUM (
    'DRAFT',
    'PUBLISH'
);

CREATE TABLE document_groups (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    slug varchar(768) NOT NULL UNIQUE,
    ja_name varchar(768) NOT NULL DEFAULT '',
    en_name varchar(768) NOT NULL DEFAULT '',
    type document_group_type NOT NULL DEFAULT 'default',
    status document_group_status NOT NULL DEFAULT 'DRAFT',
    published_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger document_groups_tri BEFORE UPDATE ON document_groups for each ROW EXECUTE PROCEDURE set_update_time();