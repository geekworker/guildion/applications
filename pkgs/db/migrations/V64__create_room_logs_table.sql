CREATE TYPE room_logs_type AS ENUM (
    'welcome',
    'message_create',
    'post_create',
    'sync_vision_create',
    'room_file_create',
    'room_role_create',
    'member_create',
    'member_destroy'
);

CREATE TABLE room_logs (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    message_id uuid REFERENCES messages(id) ON UPDATE CASCADE ON DELETE CASCADE,
    post_id uuid REFERENCES posts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    sync_vision_id uuid REFERENCES sync_visions(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_file_id uuid REFERENCES room_files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_role_id uuid REFERENCES room_roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
    member_id uuid REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    type room_logs_type NOT NULL DEFAULT 'welcome',
    changed_column varchar,
    before_diff text,
    after_diff text,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger room_logs_tri BEFORE UPDATE ON room_logs for each ROW EXECUTE PROCEDURE set_update_time();