CREATE OR REPLACE FUNCTION check_image_relation(new_file_id uuid)
RETURNS int
AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM files
        WHERE id = new_file_id AND type = 'image'
    ) THEN
        return 1;
    END IF;
    RETURN 0;
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE OR REPLACE FUNCTION before_check_image_file()
RETURNS trigger
AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM images
        WHERE file_id = NEW.id
    ) AND NEW.type = 'image' THEN
        RETURN NEW;
    ELSE
        IF NOT EXISTS (
            SELECT 1
            FROM images
            WHERE file_id = NEW.id
        ) THEN
            RETURN NEW;
        END IF;
    END IF;
    RAISE EXCEPTION USING ERRCODE = '20100', MESSAGE = 'You can not update file type before changing meta table';
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE TABLE images (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id uuid NOT NULL UNIQUE REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    width_px decimal(65, 1) NOT NULL DEFAULT 0,
    height_px decimal(65, 1) NOT NULL DEFAULT 0,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_images_tri BEFORE UPDATE ON images for each ROW EXECUTE PROCEDURE set_update_time();
CREATE trigger update_file_images_tri BEFORE UPDATE ON files for each ROW EXECUTE PROCEDURE before_check_image_file();
ALTER TABLE images ADD CONSTRAINT check_image_file_fk CHECK (check_image_relation(file_id) = 1);