CREATE TABLE room_reports (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    report_id uuid NOT NULL REFERENCES reports(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT room_reports_unique_fk UNIQUE (room_id, report_id)
);

CREATE trigger room_reports_tri BEFORE UPDATE ON room_reports for each ROW EXECUTE PROCEDURE set_update_time();