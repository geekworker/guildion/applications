CREATE TABLE room_storages (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid UNIQUE NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    file_id uuid UNIQUE NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger room_storages_tri BEFORE UPDATE ON room_storages for each ROW EXECUTE PROCEDURE set_update_time();