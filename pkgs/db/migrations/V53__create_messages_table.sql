CREATE TYPE message_status AS ENUM (
    'CREATED', 'ARCHIVED', 'DELETED'
);

CREATE TABLE messages (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    parent_id uuid REFERENCES messages(id) ON UPDATE CASCADE ON DELETE SET NULL,
    sender_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    body text NOT NULL DEFAULT '',
    location text NOT NULL DEFAULT '',
    status message_status NOT NULL DEFAULT 'CREATED',
    comments_count integer NOT NULL DEFAULT 0,
    is_commentable boolean NOT NULL DEFAULT TRUE,
    archived_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger messages_tri BEFORE UPDATE ON messages for each ROW EXECUTE PROCEDURE set_update_time();