CREATE TYPE guild_reward_status AS ENUM (
    'CREATED',
    'CONFIRMED',
    'ACTIVE',
    'REJECTED',
    'CANCELING',
    'CANCELED'
);

CREATE TABLE guild_rewards (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    status guild_reward_status NOT NULL DEFAULT 'CREATED',
    method reward_method_type NOT NULL,
    provider_key varchar(768) NOT NULL,
    commission_rate NUMERIC(3,2) NOT NULL DEFAULT 0.00,
    confirmed_at timestamptz,
    actived_at timestamptz,
    canceled_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guild_rewards_tri BEFORE UPDATE ON guild_rewards for each ROW EXECUTE PROCEDURE set_update_time();