CREATE TYPE post_status AS ENUM (
    'CREATED', 'ARCHIVED', 'DELETED'
);

CREATE TYPE post_type AS ENUM (
    'PUBLIC', 'MEMBERS', 'GUESTS'
);

CREATE TABLE posts (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    parent_id uuid REFERENCES posts(id) ON UPDATE CASCADE ON DELETE SET NULL,
    sender_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    body text NOT NULL DEFAULT '',
    location text NOT NULL DEFAULT '',
    comments_count integer NOT NULL DEFAULT 0,
    is_commentable boolean NOT NULL DEFAULT TRUE,
    reactions_count integer NOT NULL DEFAULT 0,
    is_reactable boolean NOT NULL DEFAULT TRUE,
    status post_status NOT NULL DEFAULT 'CREATED',
    type post_type NOT NULL DEFAULT 'PUBLIC',
    archived_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger posts_tri BEFORE UPDATE ON posts for each ROW EXECUTE PROCEDURE set_update_time();