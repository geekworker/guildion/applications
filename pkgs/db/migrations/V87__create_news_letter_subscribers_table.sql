CREATE TYPE news_letter_subscriber_status AS ENUM (
    'CREATED', 'REJECTED'
);

CREATE TABLE news_letter_subscribers (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    display_name varchar NOT NULL DEFAULT '',
    email email NOT NULL UNIQUE,
    status news_letter_subscriber_status NOT NULL DEFAULT 'CREATED',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger news_letter_subscribers_tri BEFORE UPDATE ON news_letter_subscribers for each ROW EXECUTE PROCEDURE set_update_time();