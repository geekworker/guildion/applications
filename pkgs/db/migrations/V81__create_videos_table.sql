CREATE OR REPLACE FUNCTION check_video_relation(new_file_id uuid)
RETURNS int
AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM files
        WHERE id = new_file_id AND type = 'video'
    ) THEN
        return 1;
    END IF;
    RETURN 0;
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE OR REPLACE FUNCTION before_check_video_file()
RETURNS trigger
AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM videos
        WHERE file_id = NEW.id
    ) AND NEW.type = 'video' THEN
        RETURN NEW;
    ELSE
        IF NOT EXISTS (
            SELECT 1
            FROM videos
            WHERE file_id = NEW.id
        ) THEN
            RETURN NEW;
        END IF;
    END IF;
    RAISE EXCEPTION USING ERRCODE = '20100', MESSAGE = 'You can not update file type before changing meta table';
END
$$
LANGUAGE plpgsql
VOLATILE;

CREATE TABLE videos (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id uuid NOT NULL UNIQUE REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    thumbnail_id uuid REFERENCES files(id) ON UPDATE CASCADE ON DELETE SET NULL,
    width_px decimal(65, 1) NOT NULL DEFAULT 0,
    height_px decimal(65, 1) NOT NULL DEFAULT 0,
    duration_ms integer NOT NULL,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_videos_tri BEFORE UPDATE ON videos for each ROW EXECUTE PROCEDURE set_update_time();
CREATE trigger update_file_videos_tri BEFORE UPDATE ON files for each ROW EXECUTE PROCEDURE before_check_video_file();
ALTER TABLE videos ADD CONSTRAINT check_video_file_fk CHECK (check_video_relation(file_id) = 1);