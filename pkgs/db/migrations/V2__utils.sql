CREATE EXTENSION IF NOT EXISTS ltree;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

create function set_update_time() returns trigger as '
  begin
    new.updated_at := ''now'';
    return new;
  end;
' language 'plpgsql';

CREATE DOMAIN hex AS varchar
  CHECK (VALUE ~ '^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$');

CREATE DOMAIN alphanumeric AS varchar
  CHECK (VALUE ~ '^[0-9a-zA-Z]+$');

CREATE DOMAIN namekey AS varchar
  CHECK (VALUE ~ '^[0-9a-zA-Z_]+$');
COMMENT ON DOMAIN namekey IS 'namekey means uniqueable name. this value will use for like username, guildname and so on';

CREATE DOMAIN url AS varchar(768)
  CHECK (VALUE ~ 'https?://[\w!\?/\+\-_~=;\.,\*&@#\$%\(\)\[\]]+');
COMMENT ON DOMAIN url IS 'match URLs (http or https)';

CREATE DOMAIN domain AS varchar
  CHECK (VALUE ~ '^([a-z][a-z0-9-]+(\.|-*\.))+[a-z]{2,6}$');
COMMENT ON DOMAIN domain IS 'match a domain name. www in front is not allowed';

CREATE DOMAIN emoji AS varchar(16)
  CHECK (VALUE ~ '^.*$');
COMMENT ON DOMAIN domain IS 'match a emoji';

CREATE DOMAIN email AS varchar
  CHECK (VALUE ~ '^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+[.][A-Za-z]+$');
COMMENT ON DOMAIN email IS 'match a email name';