CREATE TYPE message_read_status AS ENUM (
    'CREATED',
    'NOTIFIED',
    'CHECKED'
);

CREATE TABLE message_reads (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    message_id uuid NOT NULL REFERENCES messages(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_member_id uuid NOT NULL REFERENCES room_members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status message_read_status,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT message_reads_unique_fk UNIQUE (message_id, member_id, room_member_id),
    CONSTRAINT room_message_reads_unique_fk UNIQUE (message_id, room_member_id),
    CONSTRAINT member_message_reads_unique_fk UNIQUE (message_id, member_id)
);

CREATE trigger message_reads_tri BEFORE UPDATE ON message_reads for each ROW EXECUTE PROCEDURE set_update_time();