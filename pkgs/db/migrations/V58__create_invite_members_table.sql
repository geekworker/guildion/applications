CREATE TABLE invite_members (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    invite_id uuid NOT NULL REFERENCES invites(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT invite_members_unique_fk UNIQUE (member_id, invite_id)
);

CREATE trigger invite_members_tri BEFORE UPDATE ON invite_members for each ROW EXECUTE PROCEDURE set_update_time();