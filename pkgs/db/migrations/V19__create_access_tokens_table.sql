CREATE TYPE access_token_status AS ENUM (
    'ACTIVE', 'ONETIME', 'EXPIRED'
);

CREATE TABLE access_tokens (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    account_id uuid NOT NULL REFERENCES accounts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    device_id uuid NOT NULL REFERENCES devices(id) ON UPDATE CASCADE ON DELETE CASCADE,
    secret varchar(768),
    status access_token_status NOT NULL DEFAULT 'EXPIRED',
    expired_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger access_tokens_tri BEFORE UPDATE ON access_tokens for each ROW EXECUTE PROCEDURE set_update_time();