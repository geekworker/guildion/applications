CREATE TYPE room_member_type AS ENUM (
    'MEMBER',
    'GUEST'
);

CREATE TYPE room_member_status AS ENUM (
    'CREATED',
    'BANNED',
    'LOCKED',
    'DELETED'
);

CREATE TABLE room_members (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_id uuid NOT NULL REFERENCES rooms(id) ON UPDATE CASCADE ON DELETE CASCADE,
    type room_member_type NOT NULL DEFAULT 'MEMBER',
    status room_member_status NOT NULL DEFAULT 'CREATED',
    is_mute boolean,
    is_mute_mentions boolean,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT room_members_unique_fk UNIQUE (member_id, room_id)
);

CREATE trigger room_members_tri BEFORE UPDATE ON room_members for each ROW EXECUTE PROCEDURE set_update_time();