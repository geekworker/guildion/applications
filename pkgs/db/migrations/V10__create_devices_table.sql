CREATE TYPE device_status AS ENUM (
    'CREATED', 'LOCKED'
);

CREATE TABLE devices (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    udid varchar(768) NOT NULL UNIQUE,
    status device_status NOT NULL DEFAULT 'CREATED',
    os varchar(768) NOT NULL,
    model varchar(768) NOT NULL,
    language_code language_code NOT NULL DEFAULT 'en',
    country_code country_code NOT NULL DEFAULT 'US',
    timezone timezone_id NOT NULL DEFAULT 'America/Los_Angeles',
    onesignal_notification_id varchar,
    notification_token varchar,
    app_version varchar(768) NOT NULL,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

create trigger update_devices_tri before update on devices for each row execute procedure set_update_time();