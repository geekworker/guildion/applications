CREATE TABLE file_reports (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    report_id uuid NOT NULL REFERENCES reports(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT file_reports_unique_fk UNIQUE (file_id, report_id)
);

CREATE trigger file_reports_tri BEFORE UPDATE ON file_reports for each ROW EXECUTE PROCEDURE set_update_time();