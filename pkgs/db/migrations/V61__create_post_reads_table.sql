CREATE TYPE post_read_status AS ENUM (
    'CREATED', 'NOTIFIED', 'CHECKED'
);

CREATE TABLE post_reads (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    post_id uuid NOT NULL REFERENCES posts(id) ON UPDATE CASCADE ON DELETE CASCADE,
    room_member_id uuid NOT NULL REFERENCES room_members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status post_read_status,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT post_reads_unique_fk UNIQUE (post_id, member_id, room_member_id),
    CONSTRAINT room_post_reads_unique_fk UNIQUE (post_id, room_member_id),
    CONSTRAINT member_post_reads_unique_fk UNIQUE (post_id, member_id)
);

CREATE trigger post_reads_tri BEFORE UPDATE ON post_reads for each ROW EXECUTE PROCEDURE set_update_time();