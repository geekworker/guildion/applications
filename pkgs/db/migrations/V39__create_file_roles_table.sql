CREATE TABLE file_roles (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    file_id uuid NOT NULL REFERENCES files(id) ON UPDATE CASCADE ON DELETE CASCADE,
    role_id uuid NOT NULL REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT file_roles_unique_fk UNIQUE (file_id, role_id)
);

CREATE trigger file_roles_tri BEFORE UPDATE ON file_roles for each ROW EXECUTE PROCEDURE set_update_time();