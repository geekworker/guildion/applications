CREATE TABLE user_connections (
    id varchar(128) NOT NULL PRIMARY KEY DEFAULT generate_ulid(),
    user_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    connection_id varchar(128) NOT NULL REFERENCES connections(id) ON UPDATE CASCADE ON DELETE CASCADE,
    is_connecting boolean NOT NULL DEFAULT FALSE,
    connected_at timestamptz,
    disconnected_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger update_user_connections_tri BEFORE UPDATE ON user_connections for each ROW EXECUTE PROCEDURE set_update_time();