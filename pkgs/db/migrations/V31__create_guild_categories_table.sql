CREATE TABLE guild_categories (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    ja_name varchar(768) NOT NULL DEFAULT '',
    en_name varchar(768) NOT NULL DEFAULT '',
    ja_groupname varchar(768) NOT NULL DEFAULT '',
    en_groupname varchar(768) NOT NULL DEFAULT '',
    slug varchar(768)  NOT NULL UNIQUE,
    index integer NOT NULL DEFAULT 0,
    hidden boolean NOT NULL DEFAULT FALSE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guild_categories_tri BEFORE UPDATE ON guild_categories for each ROW EXECUTE PROCEDURE set_update_time();
CREATE INDEX guild_categories_index_idx ON guild_categories (index);