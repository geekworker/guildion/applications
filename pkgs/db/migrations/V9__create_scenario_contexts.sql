CREATE TYPE scenario_context_type AS ENUM (
    'ADMIN',
    'API',
    'WEB',
    'OTHER'
);

CREATE TABLE scenario_contexts (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    data text NOT NULL DEFAULT '',
    type scenario_context_type NOT NULL DEFAULT 'OTHER',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger scenario_contexts_tri BEFORE UPDATE ON scenario_contexts for each ROW EXECUTE PROCEDURE set_update_time();