CREATE TYPE report_type AS ENUM (
    'user', 'file', 'guild', 'room', 'app', 'other'
);

CREATE TYPE report_status AS ENUM (
    'OPEN', 'RESOLVED', 'CLOSED', 'REJECTED'
);

CREATE TABLE reports (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    sender_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    type report_type NOT NULL DEFAULT 'other',
    status report_status NOT NULL DEFAULT 'OPEN',
    title VARCHAR NOT NULL DEFAULT '',
    body TEXT NOT NULL DEFAULT '',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger reports_tri BEFORE UPDATE ON reports for each ROW EXECUTE PROCEDURE set_update_time();