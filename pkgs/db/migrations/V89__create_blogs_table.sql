CREATE TYPE blog_type AS ENUM (
    'default'
);

CREATE TYPE blog_data_type AS ENUM (
    'html'
);

CREATE TYPE blog_status AS ENUM (
    'DRAFT',
    'PUBLISH'
);

CREATE TABLE blogs (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    category_id uuid NOT NULL REFERENCES blog_categories(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    ja_thumbnail_url url,
    en_thumbnail_url url,
    slug varchar(768) NOT NULL UNIQUE,
    ja_title varchar(768) NOT NULL DEFAULT '',
    en_title varchar(768) NOT NULL DEFAULT '',
    ja_description text NOT NULL DEFAULT '',
    en_description text NOT NULL DEFAULT '',
    ja_data text NOT NULL DEFAULT '',
    en_data text NOT NULL DEFAULT '',
    type blog_type NOT NULL DEFAULT 'default',
    data_type blog_data_type NOT NULL DEFAULT 'html',
    status blog_status NOT NULL DEFAULT 'DRAFT',
    index integer NOT NULL DEFAULT 0,
    published_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger blogs_tri BEFORE UPDATE ON blogs for each ROW EXECUTE PROCEDURE set_update_time();