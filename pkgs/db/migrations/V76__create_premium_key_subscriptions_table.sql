CREATE TYPE premium_key_subscription_status AS ENUM (
    'CREATED',
    'CONFIRMED',
    'ACTIVE',
    'EXPIRED',
    'REJECTED',
    'CANCELING',
    'CANCELED'
);

CREATE TABLE premium_key_subscriptions (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    member_id uuid NOT NULL REFERENCES members(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    guild_premium_key_id uuid NOT NULL REFERENCES guild_premium_keys(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    status premium_key_subscription_status NOT NULL DEFAULT 'CREATED',
    method payment_method_type NOT NULL,
    provider_key varchar(768) NOT NULL,
    confirmed_at timestamptz,
    actived_at timestamptz,
    expired_at timestamptz,
    canceled_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT premium_key_subscriptions_unique_fk UNIQUE (member_id, guild_premium_key_id)
);

CREATE trigger premium_key_subscriptions_tri BEFORE UPDATE ON premium_key_subscriptions for each ROW EXECUTE PROCEDURE set_update_time();