CREATE TYPE premium_key_subscription_payment_status AS ENUM (
    'NOT_YET_PAID',
    'PAID',
    'CANCELED',
    'REFUNDED'
);

CREATE TABLE premium_key_subscription_payments (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    subscription_id uuid NOT NULL REFERENCES premium_key_subscriptions(id) ON UPDATE CASCADE ON DELETE RESTRICT,
    status premium_key_subscription_payment_status NOT NULL DEFAULT 'NOT_YET_PAID',
    method payment_method_type NOT NULL,
    provider_key varchar(768) NOT NULL,
    requested_at timestamptz,
    paid_at timestamptz,
    canceled_at timestamptz,
    refunded_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger premium_key_subscription_payments_tri BEFORE UPDATE ON premium_key_subscription_payments for each ROW EXECUTE PROCEDURE set_update_time();