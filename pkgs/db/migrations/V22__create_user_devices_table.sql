CREATE TYPE user_device_status AS ENUM (
    'CREATED', 'TRUSTED', 'LOCKED'
);

CREATE TYPE user_device_session_status AS ENUM (
    'LOGOUT', 'LOGIN', 'SHOULD_LOGOUT'
);

CREATE TABLE user_devices (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    user_id uuid NOT NULL REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE,
    device_id uuid NOT NULL REFERENCES devices(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status user_device_status NOT NULL DEFAULT 'CREATED',
    session_status user_device_session_status NOT NULL DEFAULT 'LOGOUT',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT user_devices_unique_fk UNIQUE (device_id, user_id)
);

CREATE trigger user_devices_tri BEFORE UPDATE ON user_devices for each ROW EXECUTE PROCEDURE set_update_time();