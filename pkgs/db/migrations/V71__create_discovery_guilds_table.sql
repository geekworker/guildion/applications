CREATE TABLE discovery_guilds (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    discovery_id uuid NOT NULL REFERENCES discoveries(id) ON UPDATE CASCADE ON DELETE CASCADE,
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT discovery_guilds_unique_fk UNIQUE (discovery_id, guild_id)
);

CREATE trigger discovery_guilds_tri BEFORE UPDATE ON discovery_guilds for each ROW EXECUTE PROCEDURE set_update_time();