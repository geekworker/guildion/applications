CREATE TYPE subscription_interval_type AS ENUM (
    'daily',
    'weekly',
    'monthly',
    'yearly',
    'forever'
);

CREATE TYPE payment_method_type AS ENUM (
    'STRIPE',
    'IOS',
    'ANDROID'
);

CREATE TYPE reward_method_type AS ENUM (
    'WISE'
);