CREATE TABLE guild_category_lists (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    guild_id uuid NOT NULL REFERENCES guilds(id) ON UPDATE CASCADE ON DELETE CASCADE,
    category_id uuid NOT NULL REFERENCES guild_categories(id) ON UPDATE CASCADE ON DELETE CASCADE,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger guild_category_lists_tri BEFORE UPDATE ON guild_category_lists for each ROW EXECUTE PROCEDURE set_update_time();