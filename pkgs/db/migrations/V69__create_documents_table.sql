CREATE TYPE document_type AS ENUM (
    'default'
);

CREATE TYPE document_data_type AS ENUM (
    'html'
);

CREATE TYPE document_status AS ENUM (
    'DRAFT',
    'PUBLISH'
);

CREATE TABLE documents (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    group_id uuid NOT NULL REFERENCES document_groups(id) ON UPDATE CASCADE ON DELETE CASCADE,
    section_id uuid NOT NULL REFERENCES document_sections(id) ON UPDATE CASCADE ON DELETE CASCADE,
    ja_thumbnail_id uuid REFERENCES files(id) ON UPDATE CASCADE ON DELETE SET NULL,
    en_thumbnail_id uuid REFERENCES files(id) ON UPDATE CASCADE ON DELETE SET NULL,
    slug varchar(768) NOT NULL,
    ja_title varchar(768) NOT NULL DEFAULT '',
    en_title varchar(768) NOT NULL DEFAULT '',
    ja_description text NOT NULL DEFAULT '',
    en_description text NOT NULL DEFAULT '',
    ja_data text NOT NULL DEFAULT '',
    en_data text NOT NULL DEFAULT '',
    type document_type NOT NULL DEFAULT 'default',
    data_type document_data_type NOT NULL DEFAULT 'html',
    status document_status NOT NULL DEFAULT 'DRAFT',
    index integer NOT NULL DEFAULT 0,
    published_at timestamptz,
    deleted_at timestamptz,
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp,

    CONSTRAINT documents_unique_fk UNIQUE (group_id, section_id, slug)
);

CREATE trigger documents_tri BEFORE UPDATE ON documents for each ROW EXECUTE PROCEDURE set_update_time();