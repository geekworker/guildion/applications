CREATE TYPE report_read_status AS ENUM (
    'CREATED', 'NOTIFIED', 'CHECKED'
);

CREATE TABLE report_reads (
    id uuid NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(),
    report_id uuid NOT NULL REFERENCES reports(id) ON UPDATE CASCADE ON DELETE CASCADE,
    administrator_id uuid NOT NULL REFERENCES administrators(id) ON UPDATE CASCADE ON DELETE CASCADE,
    status report_read_status NOT NULL DEFAULT 'CREATED',
    created_at timestamptz NOT NULL DEFAULT current_timestamp,
    updated_at timestamptz NOT NULL DEFAULT current_timestamp
);

CREATE trigger report_reads_tri BEFORE UPDATE ON report_reads for each ROW EXECUTE PROCEDURE set_update_time();