import { BlogCategories, BlogCategory, Blogs } from "@guildion/core";
import { AppCategory } from "./app";
import { AppDiscordBlog } from "./app/discord-about";
import { AppPinterestBlog } from "./app/pinterest-about";
import { AppReminiBlog } from "./app/remini-about";
import { CodeCategory } from "./code";
import { InfraCategory } from "./code/infra";
import { InfraSelfPipelineOptimizedBlog } from "./code/infra/self-pipeline-optimized";
import { ReactCategory } from "./code/react";
import { ReactAnimationLibrariesBlog } from "./code/react/animation-libraries";
import { TestCategory } from "./test";
import { Test1Blog } from "./test/test1";

export const buildApp = (): BlogCategory => {
    const category = AppCategory.setRecord(
        'blogs',
        new Blogs([
            AppReminiBlog,
            AppDiscordBlog,
            AppPinterestBlog,
        ])
    );
    return category;
};

export const buildCodes = (): BlogCategory => {
    const category = CodeCategory.setRecord(
        'children',
        new BlogCategories([
            ReactCategory.setRecord(
                'blogs',
                new Blogs([
                    ReactAnimationLibrariesBlog,
                ])
            ),
            InfraCategory.setRecord(
                'blogs',
                new Blogs([
                    InfraSelfPipelineOptimizedBlog,
                ])
            ),
        ])
    );
    return category;
};

export const buildTest = (): BlogCategory => {
    const category = TestCategory.setRecord(
        'blogs',
        new Blogs([
            Test1Blog,
        ])
    );
    return category;
};