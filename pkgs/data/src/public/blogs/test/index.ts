import { BlogCategory, BlogCategoryStatus } from '@guildion/core';

export const TestCategory = new BlogCategory({
    slug: 'test',
    jaName: 'Test',
    enName: 'Test',
    index: 999,
    status: BlogCategoryStatus.DRAFT,
});