import { BlogType, BlogDataType, BlogStatus, Blog, CDN_PROXY_URL_STRING } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const Test1Blog = new Blog({
    slug: 'test1',
    jaTitle: "TITLE TITLE TITLE TITLE",
    enTitle: "",
    jaDescription: `What is Lorem Ipsum?
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
`,
    enDescription: `What is Lorem Ipsum?
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum`,
    jaData: jaHTML,
    enData: enHTML,
    enThumbnailURL: undefined,
    jaThumbnailURL: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/ja/react-animation-libraries.png`,
    index: 0,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.HTML,
    status: BlogStatus.DRAFT,
});
