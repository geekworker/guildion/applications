export const enHTML = `
<h2 id="ddddcdsd">前提条件</h2>
<ul>
    <li>
        <p>fdsfsdfds<a href="//developer.android.com/sdk" class="external">Android Studio</a>fdsafdsfdsafdsafsa</p>
    </li>
    <li>
        <p>fdsfsdfds</p>
        <ul>
            <li>
                <p>fdsfsfdsadfds</p>
            </li>
            <li>
                <p>fdsfsfdsadfds</p>
            </li>
            <li>
                <p>fdsfsdfds<a href="//developer.android.com/sdk" class="external">Android Studio</a>fdsafdsfdsafdsafsa</p>
            </li>
        </ul>
    </li>
</ul>
<h2>
    sampleeeee
</h2>
<p>fdsafsdafdsa</p>
<div class="video-wrapper">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLx0sYbCqOb8TBPRdmBHs5Iftvv9TPboYG" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<ul>
    <li><code translate="no" dir="ltr">com.android.tools.build:gradle</code> v3.2.1 以降</li>
    <li><code translate="no" dir="ltr">compileSdkVersion</code> 28 以降</li>
    <li><p>物理デバイスを設定するか、エミュレータを使用してアプリを実行します。<br>
        <a href="//developer.android.com/studio/run/managing-avds" target="_blank">エミュレータ</a>
        では Google Play のエミュレータ イメージを使用する必要があります。
        </p>
    </li>
</ul>
<p><br><strong>Android アプリは以下のいずれかの方法で Firebase に接続できます。</strong></p>
<ul>
    <li><a href="/docs/android/setup#console"><strong>オプション 1</strong></a>: （推奨）<em></em>Firebase コンソールの設定ワークフローを使用します。</li>
    <li><a href="/docs/android/setup#assistant"><strong>オプション 2</strong></a>: Android Studio Firebase Assistant を使用します（追加構成が必要な場合があります）。</li>
</ul>
<hr/>
<h2 id="console"><strong>オプション 1</strong>: Firebase コンソールを使用して Firebase を追加する</h2>
<p>
    fsdfsdafdfsdafdsafsdafhasdldslkaflkdsajhflkdsjflkdjsafljdslkafjdlksahfldsahflsd
</p>
<aside class="note"><strong>注:</strong><span> Firebase プロジェクトにアプリを追加するベスト プラクティス、考慮事項（複数のビルド バリアントの扱い方など）の詳細については、<a href="/docs/projects/learn-more#best-practices">Firebase プロジェクトについて理解する</a>をご覧ください。</span></aside>
<p>
    fdsffdsafdsafdsafhldshfdlsafhdshafldsahfdlksahflksahfla
    fsdfafdsafsa
</p>
<aside class="danger"><strong>注:</strong><span> Firebase プロジェクトにアプリを追加するベスト プラクティス、考慮事項（複数のビルド バリアントの扱い方など）の詳細については、<a href="/docs/projects/learn-more#best-practices">Firebase プロジェクトについて理解する</a>をご覧ください。</span></aside>


<h2>
    Test of code block and ol
</h2>
<ol>
    <li><p>Firebase Android 構成ファイルをアプリに追加します。</p>

    <ol>
        <li><p>[<strong>google-services.json をダウンロード</strong>] をクリックして、Firebase Android 構成ファイル（<code translate="no" dir="ltr"><nobr>google-services.json</nobr></code>）を取得します。</p></li>
        <li><p>構成ファイルをアプリのモジュール（アプリレベル）ディレクトリに移動します。</p></li>
    </ol>

        <p></p><devsite-expandable is-upgraded="" id="expandable-4"><a class="exw-control" aria-controls="expandable-4" aria-expanded="false" tabindex="0" role="button"><p class="showalways">この構成ファイルについて知っておくべきことは何ですか？</p></a>
        <p></p>

        <blockquote>
        <ul>
        <li><p>Firebase 構成ファイルには、プロジェクト用の機密ではない一意の識別子が含まれています。この構成ファイルの詳細については、<a href="/docs/projects/learn-more#config-files-objects">Firebase プロジェクトについて理解する</a>をご覧ください。</p></li>
        <li><p><a href="//support.google.com/firebase/answer/7015592" class="external">Firebase 構成ファイル</a>はいつでも再ダウンロードできます。</p></li>
        <li><p>構成ファイル名に <code translate="no" dir="ltr">(2)</code> などの文字が追加されていないことを確認します。</p></li>
        </ul>
        </blockquote>

        <p></p></devsite-expandable><p></p></li>
        <li><p>アプリで Firebase プロダクトを有効にするには、Gradle ファイルに <a href="//developers.google.com/android/guides/google-services-plugin" class="external">google-services プラグイン</a>を追加します。</p>

    <ol>
        <li><p>ルートレベル（プロジェクト レベル）の Gradle ファイル（<code translate="no" dir="ltr">build.gradle</code>）に、Google サービス プラグインを含めるためのルールを追加します。Google の Maven リポジトリがあることも確認してください。</p>

        <devsite-code data-copy-event-label="">
            <pre class="devsite-code-highlight" translate="no" dir="ltr" is-upgraded=""><span class="pln">buildscript </span><span class="pun">{</span><span class="pln"><br><br>&nbsp; repositories </span><span class="pun">{</span><span class="pln"><br>&nbsp; &nbsp; </span><strong><span class="com">// Check that you have the following line (if not, add it):</span><span class="pln"><br>&nbsp; &nbsp; google</span><span class="pun">()</span><span class="pln"> &nbsp;</span><span class="com">// Google's Maven repository</span></strong><span class="pln"><br>&nbsp; </span><span class="pun">}</span><span class="pln"><br><br>&nbsp; dependencies </span><span class="pun">{</span><span class="pln"><br>&nbsp; &nbsp; </span><span class="com">// ...</span><span class="pln"><br><br>&nbsp; &nbsp; </span><strong><span class="com">// Add the following line:</span><span class="pln"><br>&nbsp; &nbsp; classpath </span><span class="str">'com.google.gms:google-services:4.3.4'</span><span class="pln"> &nbsp;</span><span class="com">// Google Services plugin</span></strong><span class="pln"><br>&nbsp; </span><span class="pun">}</span><span class="pln"><br></span><span class="pun">}</span><span class="pln"><br><br>allprojects </span><span class="pun">{</span><span class="pln"><br>&nbsp; </span><span class="com">// ...</span><span class="pln"><br><br>&nbsp; repositories </span><span class="pun">{</span><span class="pln"><br>&nbsp; &nbsp; </span><strong><span class="com">// Check that you have the following line (if not, add it):</span><span class="pln"><br>&nbsp; &nbsp; google</span><span class="pun">()</span><span class="pln"> &nbsp;</span><span class="com">// Google's Maven repository</span></strong><span class="pln"><br>&nbsp; &nbsp; </span><span class="com">// ...</span><span class="pln"><br>&nbsp; </span><span class="pun">}</span><span class="pln"><br></span><span class="pun">}</span><span class="pln"><br></span></pre></devsite-code></li>
        <li><p>モジュール（アプリレベル）の Gradle ファイル（通常は <code translate="no" dir="ltr">app/build.gradle</code>）で、Google サービスの Gradle プラグインを適用します。</p>

        <devsite-code data-copy-event-label=""><div class="devsite-code-buttons-container" role="group" aria-label="操作ボタン"><button type="button" class="gc-analytics-event material-icons devsite-icon-code-dark devsite-toggle-dark" data-category="Site-Wide Custom Events" data-label="Dark Code Toggle" track-type="exampleCode" track-name="darkCodeToggle" aria-label="コードをダークテーマで表示" data-title="コードをダークテーマで表示"></button><button type="button" class="gc-analytics-event material-icons devsite-icon-code-light devsite-toggle-light" data-category="Site-Wide Custom Events" data-label="Light Code Toggle" track-type="exampleCode" track-name="lightCodeToggle" aria-label="コードをライトテーマで表示" data-title="コードをライトテーマで表示"></button><button type="button" class="gc-analytics-event material-icons devsite-icon-copy" data-category="Site-Wide Custom Events" data-label="Click To Copy" track-type="exampleCode" track-name="clickToCopy" aria-label="コードサンプルをコピー" data-title="コードサンプルをコピー"></button></div><pre class="devsite-code-highlight" translate="no" dir="ltr" is-upgraded=""><span class="pln">apply plugin</span><span class="pun">:</span><span class="pln"> </span><span class="str">'com.android.application'</span><span class="pln"><br></span><strong><span class="com">// Add the following line:</span><span class="pln"><br>apply plugin</span><span class="pun">:</span><span class="pln"> </span><span class="str">'com.google.gms.google-services'</span><span class="pln"> &nbsp;</span><span class="com">// Google Services plugin</span></strong><span class="pln"><br><br>android </span><span class="pun">{</span><span class="pln"><br>&nbsp; </span><span class="com">// ...</span><span class="pln"><br></span><span class="pun">}</span><span class="pln"><br></span></pre></devsite-code></li>
    </ol>
    </li>
</ol>

<h2>table test</h2>
<table>
    <thead>
    <tr>
        <th width="32%">サービスまたはプロダクト</th>
        <th width="45%">Gradle 依存関係</th>
        <th>最新<br>バージョン</th>
        <th width="10%"><abbr data-title="For an optimal experience using this product, add the Firebase SDK for Google Analytics.">アナリティクスの追加</abbr></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><a href="/docs/android/learn-more#bom">Firebase Android BoM<br>（部品構成表）</a></td>
        <td>com.google.firebase:firebase-bom
        <br>
        <p>最新バージョンの Firebase BoM には、各 Firebase Android ライブラリの最新バージョンが含まれます。特定の BoM バージョンにマッピングされているライブラリ バージョンを確認するには、その BoM バージョンのリリースノートをご覧ください。</p>
        </td>
        <td>25.12.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/admob/android/quick-start">AdMob</a></td>
        <td>com.google.android.gms:play-services-ads</td>
        <td>19.4.0</td>
        <td><span class="compare-yes"></span></td>
    </tr>
    <tr>
        <td><a href="/docs/analytics/get-started?platform=android">アナリティクス</a></td>
        <td>com.google.firebase:firebase-analytics</td>
        <td>17.6.0</td>
        <td><span class="compare-yes"></span></td>
    </tr>
    <tr>
        <td><a href="/docs/app-indexing/android/app">App Indexing</a></td>
        <td>com.google.firebase:firebase-appindexing</td>
        <td>19.1.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/auth/android/start">Authentication</a></td>
        <td>com.google.firebase:firebase-auth</td>
        <td>19.4.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/firestore">Cloud Firestore</a></td>
        <td>com.google.firebase:firebase-firestore</td>
        <td>21.7.1</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/functions">Cloud Functions for Firebase Client SDK</a></td>
        <td>com.google.firebase:firebase-functions</td>
        <td>19.1.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/cloud-messaging/android/client">Cloud Messaging</a></td>
        <td>com.google.firebase:firebase-messaging</td>
        <td>20.3.0</td>
        <td><span class="compare-yes"></span></td>
    </tr>
    <tr>
        <td><a href="/docs/storage/android/start">Cloud Storage</a></td>
        <td>com.google.firebase:firebase-storage</td>
        <td>19.2.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/crashlytics/get-started?platform=android">Crashlytics</a></td>
        <td>com.google.firebase:firebase-crashlytics</td>
        <td>17.2.2</td>
        <td><span class="compare-yes"></span></td>
    </tr>
    <tr>
        <td><a href="/docs/dynamic-links/android/create">Dynamic Links</a></td>
        <td>com.google.firebase:firebase-dynamic-links</td>
        <td>19.1.1</td>
        <td><span class="compare-yes"></span></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML Vision API</a></td>
        <td>com.google.firebase:firebase-ml-vision</td>
        <td>24.1.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML カスタムモデル API</a></td>
        <td>com.google.firebase:firebase-ml-model-interpreter</td>
        <td>22.0.4</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/in-app-messaging">アプリ内メッセージング</a></td>
        <td>com.google.firebase:firebase-inappmessaging</td>
        <td>19.1.1</td>
        <td><span class="compare-yes"></span><br><em></em>（必須）</td>
    </tr>
    <tr>
        <td><a href="/docs/in-app-messaging">アプリ内メッセージング表示</a></td>
        <td>com.google.firebase:firebase-inappmessaging-display</td>
        <td>19.1.1</td>
        <td><span class="compare-yes"></span><br><em></em>（必須）</td>
    </tr>
    <tr>
        <td><a href="/docs/perf-mon/get-started-android">Performance Monitoring</a></td>
        <td>com.google.firebase:firebase-perf</td>
        <td>19.0.9</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/database">Realtime Database</a></td>
        <td>com.google.firebase:firebase-database</td>
        <td>19.5.0</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/remote-config/use-config-android">Remote Config</a></td>
        <td>com.google.firebase:firebase-config</td>
        <td>19.2.0</td>
        <td><span class="compare-yes"></span></td>
    </tr>
    <tr>
        <td><a href="//developers.google.com/android/guides/google-services-plugin" class="external">Google Play 開発者サービス プラグイン</a></td>
        <td>com.google.gms:google-services</td>
        <td>4.3.4</td>
        <td></td>
    </tr>
    </tbody>
    <thead>
    <tr>
        <th colspan="4">非推奨ライブラリ</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: 画像ラベル付けモデル</a></td>
        <td>com.google.firebase:firebase-ml-vision-image-label-model</td>
        <td>20.0.2</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: オブジェクトの検出とトラッキング モデル</a></td>
        <td>com.google.firebase:firebase-ml-vision-object-detection-model</td>
        <td>19.0.6</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: 顔検出モデル</a></td>
        <td>com.google.firebase:firebase-ml-vision-face-model</td>
        <td>20.0.2</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: バーコード スキャンモデル</a></td>
        <td>com.google.firebase:firebase-ml-vision-barcode-model</td>
        <td>16.1.2</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: AutoML Vision Edge API</a></td>
        <td>com.google.firebase:firebase-ml-vision-automl</td>
        <td>18.0.6</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: Natural Language API</a></td>
        <td>com.google.firebase:firebase-ml-natural-language</td>
        <td>22.0.1</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: 言語識別モデル</a></td>
        <td>com.google.firebase:firebase-ml-natural-language-language-id-model</td>
        <td>20.0.8</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: 翻訳モデル</a></td>
        <td>com.google.firebase:firebase-ml-natural-language-translate-model</td>
        <td>20.0.9</td>
        <td></td>
    </tr>
    <tr>
        <td><a href="/docs/ml-kit">Firebase ML: スマート リプライモデル</a></td>
        <td>com.google.firebase:firebase-ml-natural-language-smart-reply-model</td>
        <td>20.0.8</td>
        <td></td>
    </tr>
    </tbody>
</table>

<hr/>

<h2 id="next_steps" data-text="次のステップ" tabindex="0">次のステップ</h2>
<p><strong>Firebase サービスをアプリに追加します。</strong></p>

<ul>
    <li><p><a href="/docs/analytics/android/start">アナリティクス</a>でユーザー行動を把握する。</p></li>
    <li><p><a href="/docs/auth/android/start">Authentication</a> でユーザー認証フローを設定する。</p></li>
    <li><p><a href="/docs/firestore/quickstart">Cloud Firestore</a> または <a href="/docs/database/android/start">Realtime Database</a> にユーザー情報などのデータを保存する。</p></li>
    <li><p><a href="/docs/storage/android/start">Cloud Storage</a> に写真や動画などのファイルを保存する。</p></li>
    <li><p><a href="/docs/functions/callable#call_the_function">Cloud Functions</a> を使用した安全な環境でバックエンド コードをトリガーする。</p></li>
    <li><p><a href="/docs/cloud-messaging/android/client">Cloud Messaging</a> を使用して通知を送信する。</p></li>
    <li><p><a href="/docs/crashlytics">Crashlytics</a> を使用してアプリがクラッシュする場所と理由を確認する。</p></li>
</ul>

<p><strong>以下で Firebase の詳細を確認します。</strong></p>

<ul>
    <li><p>Firebase プロジェクトとプロジェクトに関するベスト プラクティスについては、<a href="/docs/projects/learn-more">Firebase プロジェクトについて理解する</a>をご覧ください。</p></li>
    <li><p>なじみのないコンセプトや、Firebase および Android 開発に独自のコンセプトについて不明点がある場合は、<a href="/docs/android/learn-more">Android と Firebase の詳細</a>をご覧ください。</p></li>
    <li><p><a href="//github.com/firebase/quickstart-android" class="external">Firebase アプリのサンプル</a>を確認する。</p></li>
    <li><p><a href="//codelabs.developers.google.com/codelabs/firebase-android/" class="external">Firebase Android Codelab</a> を使用して実際に体験する。</p></li>
    <li><p><a href="//udacity.com/course/ud0352" class="external">Firebase in a Weekend</a> コースで詳しく学習する。</p></li>
    <li><p>アプリを起動する準備をする。</p>

    <ul>
        <li>GCP Console でプロジェクトの<a href="//cloud.google.com/billing/docs/how-to/budgets" class="external">予算アラート</a>を設定する。</li>
        <li>Firebase コンソールで<a href="//console.firebase.google.com/project/_/usage" class="external">使用量と請求ダッシュボード</a>をモニタリングする。</li>
        <li><a href="/support/guides/launch-checklist">Firebase リリース チェックリスト</a>を確認する。</li>
    </ul></li>
</ul>
`