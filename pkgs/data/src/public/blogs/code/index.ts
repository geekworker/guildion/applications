import { BlogCategory, BlogCategoryStatus } from '@guildion/core';

export const CodeCategory = new BlogCategory({
    slug: 'code',
    jaName: 'Code',
    enName: 'Code',
    index: 1,
    status: BlogCategoryStatus.PUBLISH,
});