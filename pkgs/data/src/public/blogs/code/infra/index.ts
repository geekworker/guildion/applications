import { BlogCategory, BlogCategoryStatus } from '@guildion/core';

export const InfraCategory = new BlogCategory({
    slug: 'infrastructure',
    jaName: 'インフラ',
    enName: 'Infra',
    index: 0,
    status: BlogCategoryStatus.PUBLISH,
});