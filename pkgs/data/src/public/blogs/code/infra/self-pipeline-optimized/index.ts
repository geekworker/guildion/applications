import { BlogType, BlogDataType, BlogStatus, Blog, CDN_PROXY_URL_STRING } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const InfraSelfPipelineOptimizedBlog = new Blog({
    slug: 'self-pipeline-optimized',
    jaTitle: "CI/CD Runnerを自前でホストしてPushからデプロイを数分で終わらせる方法",
    enTitle: "",
    jaDescription: `今回はCI/CD Runnerの自前ホストとその最適化処理や構成についてざっくりと書きました。上級者向けの記事となっており内容も濃いです。そしてKubernetesの魅力を語りまくっています。`,
    enDescription: ``,
    jaData: jaHTML,
    enData: enHTML,
    enThumbnailURL: undefined,
    jaThumbnailURL: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/self-pipeline-optimized/thumbnail-ja.png`,
    index: 0,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.HTML,
    status: BlogStatus.PUBLISH,
});