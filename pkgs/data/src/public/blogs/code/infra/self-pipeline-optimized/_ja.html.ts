import { CDN_PROXY_URL_STRING, HTMLEmbed, makeTransformSrc } from "@guildion/core"

export const jaHTML = `
<h2 id="1">
    はじめに
</h2>
<p>
    今回はセルフホストしたCI/CDパイプラインが超高速だったので、その知識を共有しようと思います。
</p>
<p>
    今回はノウハウ共有のため具体的な手法などはまた別記事にまとめます。
</p>

<h2 id="2" >
    CI/CD Pipelineとは
</h2>

<p>
    CIはContinuous Integration（継続的インテグレーション）、CDはContinuous Delivery（継続的デリバリー）の略称です。
</p>
<p>
    今回の記事では<strong>リモートのGitにpushするなど何かアクションを行なった後に、デプロイやテストなどを自動でバックグラウンドで行ってくれるもの</strong>という認識で大丈夫です。
</p>
<p>
    主な技術例だとCircleCI, Gitlab CI, Github actionsなどが当てはまります。
</p>
<p>
    多分CI/CDを使ったことがない人はデプロイや何らかの実行を自分のPCでやっていたりしたことがあると思います。
</p>
<p>
    ですがPCのスペックには限りがあり、そこそこのタスクをいくつかPCにやらせるとフィンが「うぃんうぃん」鳴り始めて、作業が中断すると思います。
</p>
<p>
    CI/CDを使えばそんなめんどくさいことも自動化でき<strong>デプロイ中にランチ</strong>を食べることもできます。🍔
</p>

<h2 id="3">
    実際の使用技術
</h2>
<ul>
    <li>
        CI/CD: <strong>Gitlab CI self-host runner</strong>
    </li>
    <li>
        Runner環境: <strong>Kubernetes ( 以下「K8Sと略称部分あり )</strong>
    </li>
    <li>
        インフラ: <strong>AWSのEKS</strong>
    </li>
    <li>
        コンテナ: <strong>Docker</strong>
    </li>
</ul>
<p>
    後ほど解説しますが、今回はGitlab Runnerを自分のKubernetes Cluster上に展開して、Gitlabにagentを使用してconnectしています。
</p>
<p>
    また、どのようにPipelineやクラスターを自身で所有するための具体的なコマンドなどはまた別の記事で話そうと思います。
</p>
<p>
    またPipelineは毎回Remote GitからレポジトリをCloneしてコマンドを実行します。
</p>
<p>
    <strong>もちろんレポジトリのサイズが大きければ実行速度も落ちますが、今回の自身のレポジトリのサイズは約2GBとそこそこ重たいのですが、それでも難なく短時間で実行できています。</strong>
</p>

<h2 id="4">
    AWSの構成
</h2>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/self-pipeline-optimized/eks-designer.png`, { dw: '800', of: 'webp' }),
    width: 1100,
    height: 870,
    layout: 'responsive',
})}
<p>
    使用したのはAWSのEKSとNodeにEC2です。
</p>
<p>
    Fargateは個人的にはあまり好きではないので普段から使いません。
</p>
<p>
    <strong>Fargateを使わない理由は簡単で自由度がないのと費用が高いからです。</strong>
</p>
<p>
    EC2であればssh接続からのターミナルの実行ができ、色々コマンドを実行したり、緊急や検証でのコマンド入力などができます。
</p>
<p>
    しかし普通に使っているとEC2がFargateよりも安いという印象はないと思います。
</p>
<p>
    今回はSpot Instanceという格安でEC2サーバーが使用できるAWSのプログラムを使用しています。
</p>
<p>
    Spot Instanceは本当におすすめです。すこし魅力を語らせてください。
</p>

<h2>
    AWSの最強の節約術「Spot Instance」
</h2>
<p>
    Spot Instanceは通常のOn demand instanceと違って、<strong>使用されていない時間帯のサーバーをAWSが自動で割り振ってくれるため格安となるというプログラムです。</strong>
</p>
<p>
    ちなみにSpot InstanceだとOn demandの<strong>半額から8割ほど安い</strong>です。
</p>
<p>
    デメリットとして予期せぬ中断があると公式では言っていますが、個人的な感想を言わせていただくと全く問題ないです。
</p>
<p>
    <strong>中断頻度は数ヶ月に1回くらいですし、複数サーバーを立ち上げておけば特に問題もありません。</strong>
</p>
<p>
    詳しいリアルタイムの値段帯は公式サイトから確認してください。
</p>
<p>
    <a href="https://aws.amazon.com/jp/ec2/spot/pricing/" target="_blank">Amazon EC2 スポットインスタンスの料金</a>
</p>
<p>
    今回利用したSpot Instanceの種類はt2.largeとt3.largeです。
</p>
<p>
    スペックは2CPUに8Gメモリでストレージは60GBです。
</p>

<h2 id="5">
    K8S Clusterの構成
</h2>
<p>
    <strong>Gitlab Runnerを自前でホストするためにはEKSとGitlabを接続しなければなりません。</strong>
</p>
<p>
    EKSはcloudformationを使って、Infrastructure as a codeで構築しています。
</p>
<p>
    その既存の構築されたEKS ClusterはGitlab Agentと呼ばれる新型の方法でGitlabと接続しています。
</p>
<p>
    ちなみにGitlabのRunnerに関する日本語解説記事はすごく少ないです。いつか解説記事書きます。笑
</p>
<p>
    とりあえず公式ドキュメントを貼っておきます。
</p>
<p>
    <a href="https://docs.gitlab.com/ee/user/clusters/agent/" target="_blank">Connecting a Kubernetes cluster with GitLab</a>
</p>

<h2 id="6">
    Runnerの構成
</h2>
<p>
    今回はRunner展開用&Cluster接続用のレポジトリと実際のアプリやサイトのソースコードを書いているレポジトリと分割しています。
</p>
<p>
    なぜ分割しているかというと、<strong>Gitlabには優秀なRunnerやよく使うツールをK8Sに構築する専用のテンプレートを用意してくれています。</strong>
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/self-pipeline-optimized/gitlab-template.png`, { dw: '800', of: 'webp' }),
    width: 1000,
    height: 100,
    layout: 'responsive',
})}
<p>
    <a href="https://gitlab.com/gitlab-org/project-templates/cluster-management" target="_blank">GitLabクラスター管理レポジトリ</a>
</p>
<p>
    <strong>このテンプレートを使って、Agent接続するだけでGitlab Runnerの自前ホストは一発で構築できてしまいます。</strong>
</p>
<p>
    ちなみにEKSによるGitlab Runner立ち上げのチュートリアル記事は英語でも日本語でも皆無でした。これもいつか解説記事書きます。笑
</p>

<h2 id="7">
    タスク構成
</h2>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/self-pipeline-optimized/pipeline-jobs.png`, { dw: '800', of: 'webp' }),
    width: 1500,
    height: 250,
    layout: 'responsive',
})}
<p>
    今回の例として出しているプロジェクトは<strong>Monorepoと呼ばれる複数のプロジェクトを一つのレポジトリで管理するという構成</strong>をとっています。
</p>
<p>
    そのため全体の共通部分のDockerのビルドと、デプロイしたいアプリそれぞれのDockerのビルドをそれぞれ分割することで最適化しています。
</p>
<p>
    <strong>このようにすることで変更箇所のみピンポイントでビルドでき、不要なジョブを実行せずに済みます。</strong>
</p>
<ol>
    <li>
        共通部分のDocker Build
    </li>
    <li>
        アプリ部分のDocker Build
    </li>
    <li>
        Infrastructure as a codeの実行
    </li>
    <li>
        K8Sにアプリのデプロイ
    </li>
</ol>

<h2 id="8">
    実行結果
</h2>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/self-pipeline-optimized/pipeline-result.png`, { dw: '800', of: 'webp' }),
    width: 870,
    height: 100,
    layout: 'responsive',
})}
<p>
    高速です。<strong>7分でPushからデプロイを全て終了させるという驚きのスピード</strong>です。
</p>
<p>
    費用としては、EKSの月料金7500円とSpot Instanceの月料金2000~3000円で月1万円でできてしまいます。
</p>
<p>
    本格的なサービスを個人開発していない限り高いと思われますが、この手法は僕は仕事先の会社で構築できます！
</p>
<p>
    会社からすると驚きの安さです。
</p>
<p>
    この構成を自分で構築してしまえば、会社の開発者からは泣いて喜ばれるか、高く評価してくれるかのどっちかです。
</p>
<p>
    そしてこれが安いと言える理由が一つあり、格安の高スペックのサーバーにKubernetes Clusterを展開してしまえるのもあります。
</p>
<p>
    <strong>Kubernetesがあれば、たった１つのサーバーでアプリやサーバーやRunnerを何十個も展開できるからです。</strong>
</p>
<p>
    これがKubernetes先生のサーバー最適化という１つの強みです。
</p>

<h2 id="9">
    おわりに
</h2>
<p>
    今回はノウハウだけ共有しましたが、どのように構築したのか具体的なコマンドやもっと初心者に優しい解説が欲しいという要望があれば、そういう記事も書こうと思います。
</p>
<p>
    お疲れ様でした。
</p>
`