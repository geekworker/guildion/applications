import { CDN_PROXY_URL_STRING, HTMLEmbed, makeTransformSrc } from "@guildion/core"

export const jaHTML = `
<h2 id="1">
    はじめに
</h2>
<p>
    アニメーションはwebサイトを作る上で重要なUX要素の一つです。
</p>
<p>
    アニメーションにはサイトで伝えたいことをより、直感的にユーザーに伝える効果があります。
</p>
<p>
    文字や画像だけでは表現しづらいこともアニメーションであればより的確に伝わるはずです。
</p>
<p>
    ですがアニメーションはよく実装の際には面倒に思われやすいタスクの一つです。
</p>
<p>
    そこでWeb構築での主流のフレームワークの一つ「React」で簡単にエレガントなアニメーションを作れる最新のライブラリーツールを５つご紹介します。
</p>
<p>
    おまけに昔は良かったけど今はおすすめできないアニメーションライブラリーも載せておきます。笑
</p>

<h2 id="2" >
    Framer Motion
</h2>
${HTMLEmbed.ReferenceImage.stringify({
    src: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/react-animation-libraries/framer-motion.jpg`,
    width: 900,
    height: 500,
    layout: 'responsive',
    href: 'https://www.framer.com/motion',
    name: 'Framer Motion',
})}

<p>
    <a href="https://www.framer.com/motion/" target="_blank">
        Framer Motion
    </a>
    は有名なReactのアニメーションライブラリーの一つです。
</p>
<p>
    なぜFramer Motionと呼ぶのかというと、<a href="https://www.npmjs.com/package/framer" target="_blank">Framer</a>と呼ばれるAdobe XDやFigmaのようなUI構築ツールのAPIと、今から紹介するMotionと呼ばれるアニメーションツールを合体したものだからです。
</p>

<h3>
    Framer motionの特徴
</h3>
<ul>
    <li>
        <a href="https://github.com/framer/motion" target="_blank">Github上では13000スター超え</a>
    </li>
    <li>
        使い方はmotionのコンポーネントにアニメーションの設定をするだけ
    </li>
    <li>
        React Hooksと連携してアニメーションの幅が無限大
    </li>
    <li>
        SVGのアニメーションも簡単
    </li>
    <li>
        React Routerとも連携できる
    </li>
</ul>

<p>
    これがFramer Motionのデモアニメーションです。このブログの404ページのアニメーション部分です
</p>
${HTMLEmbed.Custom.stringify({
    type: '404',
})}

<p>
    よく開発現場でもFramer motionを使ってアニメーションを作ることが多いほど、筆者一押しのアニメーションライブラリーでした。
</p>

<h2 id="3">
    React Spring
</h2>

${HTMLEmbed.ReferenceImage.stringify({
    src: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/react-animation-libraries/react-spring.jpg`,
    width: 900,
    height: 500,
    layout: 'responsive',
    href: 'https://react-spring.io/',
    name: 'React Spring',
})}
<p>
    <a href="https://react-spring.io/" target="_blank">
        React Spring
    </a>
    も代表的なReactアニメーションライブラリの一つです。
</p>
<p>
    React Springは名前の通りSpringのアニメーションを得意とします。
</p>
<p>
    Springアニメーションとは一定時間の間、どのようにアニメーションを動かすかの関数の一つで、<strong>主にバネのように跳ねる動作が特徴的なアニーメーション</strong>を指します。
</p>

<p>
    このReact Springのデモアニメーションを見るとわかると思うのですが、一度左に行って右に行きますよね。
</p>

${HTMLEmbed.ReferenceImage.stringify({
    src: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/react-animation-libraries/react-spring-demo.gif`,
    width: 900,
    height: 500,
    layout: 'responsive',
    href: 'https://react-spring.io/',
    name: 'React Spring demo',
})}

<p>
    このように少し複雑なアニメーションが得意です。
</p>

<h3>
    React Springの特徴
</h3>
<ul>
    <li>
        <a href="https://github.com/pmndrs/react-spring" target="_blank">Github上では22000スター超え</a>
    </li>
    <li>
        React Hooksを中心としたアニメーション・複雑なアニメーション向け
    </li>
    <li>
        Next.jsで標準的に使われるほど安定性・機能性が優れている
    </li>
    <li>
        アニメーションの種類はReact Springが用意してくれているものを使うだけ
    </li>
    <li>
        React Routerとも連携できる
    </li>
</ul>

<p>
    React Springは使っている開発現場も多く、できるようになっていて損はないライブラリーです。
</p>
<p>
    少し複雑なアニメーションもReact Springなら即解決です。
</p>
<p>
    ただ個人的にはクセがあると感じ、アニメーションの表現の幅もFramer Motionに劣る印象がありますが、根強い人気があるのは確かです。
</p>

<h2 id="4">
    React Motion
</h2>

<p>
    こちらはすごくシンプルなアニメーションライブラリーです。
</p>

<p>
    公式のデモはこちらの<strong>８種類！</strong>
</p>

<ul>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo0-simple-transition/" target="_blank">
            Simple Transition (シンプルアニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo1-chat-heads/" target="_blank">
            Chat Heads (プロフィール画像アニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo2-draggable-balls/" target="_blank">
            Draggable Balls (ドラッグ可能ボールアニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo3-todomvc-list-transition/" target="_blank">
            TodoMVC List Transition (タスクアプリの一覧画面のアニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo4-photo-gallery/" target="_blank">
            Photo Gallery (写真ライブラリアニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo5-spring-parameters-chooser/" target="_blank">
            Spring Parameters Chooser (調整可能アニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo7-water-ripples/" target="_blank">
            Water Ripples (波アニメーション)
        </a>
    </li>
    <li>
        <a href="http://chenglou.github.io/react-motion/demos/demo8-draggable-list/" target="_blank">
            Draggable List (リスト並び替えアニメーション)
        </a>
    </li>
</ul>
<p>
    ReactのアニメーションライブラリーはReact Hooksが使える前提でのものが多く、React SpringとFramer Motionもその一種にあたります。
</p>
<p>
    しかしReact Motionは<strong>React Hooksができない状態でも簡単に扱える、比較的初心者向けのかつシンプルなライブラリー</strong>です。
</p>

<h3>
    React Motionの特徴
</h3>
<ul>
    <li>
        <a href="https://github.com/chenglou/react-motion" target="_blank">Github上では20000スター超え</a>
    </li>
    <li>
        React Hooksを使わなくても使える初心者向け・簡単なアニメーションをすぐ作れる
    </li>
    <li>
        アニメーションの種類はReact Motionが用意してくれているものを使うだけ
    </li>
</ul>

<p>
    React初心者でアニメーションがやりたいという方はおすすめです。
</p>
<p>
    ただReact Hooksを使わなくても良いというのは、応用的なアニメーションを作る際にはデメリットになるので複雑なアニメーションには不向きです。
</p>

<h2 id="5">
    さいごに
</h2>

<p>
    いかがでしたでしょうか？筆者個人的には基本から応用まで扱える<a href="#2">Framer Motion</a>が個人的には好きです。
</p>
<p>
    もっと簡単なものが良いなら<a href="#4">React Motion</a>。より応用的な複雑なアニメーションを求めるなら<a href="#2">React Spring</a>という感じで選ぶと良いです。
</p>
<p>
    みなさんも自分にあったアニメーションライブラリーを見つけてみてください。
</p>
<p>
    今回は特別に<strong>おまけ</strong>で僕の失敗談に基づくおすすめしないアニメーションライブラリーもあるのでそちらも見ていってください！
</p>
<p>
    ではよいプログラミングライフを！
</p>

<h2 id="6">
    【おまけ】失敗談
</h2>

<p>
    他のブログではこれらのライブラリーは良いと書かれていることが多いですが、実際に使ってみた一個人としておすすめできないアニメーションライブラリをここで紹介します。
</p>
<p>
    あくまで個人的な意見ということを念頭にいれて参考にして欲しいです。
</p>

<h3>
    Scroll Reveal
</h3>

${HTMLEmbed.ReferenceImage.stringify({
    src: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/react-animation-libraries/scroll-reveal.png`,
    width: 900,
    height: 500,
    layout: 'responsive',
    href: 'https://scrollrevealjs.org/',
    name: 'Scroll Reveal',
})}

<p>
    <a href="https://scrollrevealjs.org/" target="_blank">Scroll Reveal</a>はスクロールと連携しており、スクロールしたら上下左右色々なアニメーションで要素を出現させるようなアニメーションが簡単に作れます。
</p>

<p>
    一見、良さそうにも思えるこのライブラリーがなぜおすすめできないかというと、<strong>商用利用したり、多くのリクエストが発生する場合はライセンスを購入しなければいけないからです。</strong>
</p>

<p>
    別に機能的には特に可もなく不可もなくですが、他のライブラリーで代用できてしまうのに、わざわざこのライブラリーのライセンスを買うインセンティブは普通はないと思っただけです。
</p>
<p>
    自分がこのライブラリーを使用している開発現場にいたことがあり、ある日急に「商用利用にはライセンスが必要」エラーを吐き動かなくなり、結局あとでFramer motionにリプレイス( 置き換え )したので、個人的におすすめできないです。
</p>
<p>
    昔は使っている人も多く、数年前のブログではこのライブラリーをイチオシしてたりしますが、すこし時代遅れ感は否めませんね。
</p>
<p>
    プライベートで遊ぶ分には問題ないでしょう。
</p>

<h3>
    Slick CarouselなどのjQuery製のアニメーションライブラリー
</h3>

${HTMLEmbed.ReferenceImage.stringify({
    src: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/react-animation-libraries/jquery-logo-banner.png`,
    width: 680,
    height: 260,
    layout: 'responsive',
    href: 'https://jquery.com/',
    name: 'jQuery',
})}

<p>
    例としてこのライブラリーをあげているだけですがjQueryをしれっと使っているライブラリーは多くあります。
</p>

<p><a href="https://github.com/kenwheeler/slick" target="_blank">https://github.com/kenwheeler/slick</a></p>

<p>
    おすすめできない理由は２つです。
</p>
<ol>
    <li>
        Reactと一緒に使うとバグが多い
    </li>
    <li>
        パフォーマンスが悪くカクカクする
    </li>
</ol>

<p>
    それだけです。
</p>
<p>
    そもそもReactとjQueryを比較したときや共存したときになぜjQueryが悪く言われるかというと、両者はどのように画面レイアウト( DOM )を構築するかのアルゴリズムが全く違うからです。
</p>
<p>
    jQueryがすでにHTMLで表現されたDOMを後から操作するので、一部のDOMを変更したいときでも全体のDOMに影響がでることが多いです。
</p>
<p>
    それに比較してReactはDOMの構築をHTMLに頼らずに自身で行うため、DOM情報を全て自分が持ち、DOM変更の最適化を行っています。
</p>
<p>
    そのためReactとjQueryは共存した途端パフォーマンスが悪くなります。
</p>
<p>
    package.jsonにjqueryに関連する文字を見つけたら、使わないことをお勧めします。
</p>
`