import { BlogType, BlogDataType, BlogStatus, Blog, CDN_PROXY_URL_STRING } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const ReactAnimationLibrariesBlog = new Blog({
    slug: 'react-animation-libraries',
    jaTitle: "【React】万能アニメーションライブラリ2022年度まとめ3選！",
    enTitle: "",
    jaDescription: `Reactの万能ライブラリーを3つ選ばせていただきました。
実際に自分が使用してみて、開発体験が良いものを挙げさせていただきました。
多くの開発現場でも使用されているものなので、安心して使用することができます。
これでアニメーションで悩むことはもうありません。
`,
    enDescription: ``,
    jaData: jaHTML,
    enData: enHTML,
    enThumbnailURL: undefined,
    jaThumbnailURL: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/react-animation-libraries/thumbnail-ja.png`,
    index: 0,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.HTML,
    status: BlogStatus.PUBLISH,
});