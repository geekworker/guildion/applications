import { BlogCategory, BlogCategoryStatus } from '@guildion/core';

export const ReactCategory = new BlogCategory({
    slug: 'react',
    jaName: 'React',
    enName: 'React',
    index: 0,
    status: BlogCategoryStatus.PUBLISH,
});