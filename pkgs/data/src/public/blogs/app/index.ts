import { BlogCategory, BlogCategoryStatus } from '@guildion/core';

export const AppCategory = new BlogCategory({
    slug: 'app',
    jaName: 'アプリ',
    enName: 'App',
    index: 0,
    status: BlogCategoryStatus.PUBLISH,
});