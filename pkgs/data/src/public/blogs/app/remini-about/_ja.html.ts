import { CDN_PROXY_URL_STRING, HTMLEmbed, makeTransformSrc } from "@guildion/core";

export const jaHTML = `
<h2 id="1">
    はじめに
</h2>

<p>2022年3月上旬に大幅なアップデートが来た、話題の高画質化アプリReminiというアプリをご存知でしょうか？</p>
<p>普段から筆者も画質が微妙な画像を高画質にしたり、上手く撮れなかったけど思い出いっぱいの写真を高画質化するなど普段からお世話になっています。</p>

<p>新しく生まれ変わり、そして筆者愛用アプリであるReminiの魅力をこの記事では伝えられたらと思っています。</p>

<h2 id="2">
    インストール
</h2>
<p>ReminiはAppStoreとGoogleStoreの両方で利用可能です。</p>
${HTMLEmbed.AppDownloadButton.stringify({ iosHref: 'https://apps.apple.com/jp/app/remini-photo-enhancer/id1470373330?uo=4' , androidHref: 'https://play.google.com/store/apps/details?id=com.bigwinepot.nwdn.international'})}

<h2 id="3">
    画像を高画質にする方法
</h2>

<p>まずはギャラリーから高画質化したい写真を選びましょう。</p>

<p>今回はサンプルとして少し荒いフリー素材の女性画像を使います。</p>

${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-remini-about/before.jpg`, { dw: '200', of: 'webp' }),
    width: 400,
    height: 600,
    layout: 'fill',
})}

<p>画像を選択したら上のボタンを押して<strong>無料</strong>で画像を高画質化しましょう！</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-remini-about/enhance.jpg`, { dw: '400', of: 'webp' }),
    width: 400,
    height: 800,
    layout: 'fill',
})}

<p>ちなみにしたのボタンはPro(有料版)のようです。</p>
<p>無料版と有料版の違いは<a href="#4">後ほど</a>解説します。</p>

<p>広告が終了すると画像をダウンロードできます。</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-remini-about/save.jpg`, { dh: '400', of: 'webp' }),
    width: 400,
    height: 400,
    layout: 'fill',
})}
<p>無料版の場合は画像のダウンロード時にも広告が必要となるので、<strong>無料版では最低でも2回の広告の視聴が必須です。</strong></p>

<p>完成形はこちら！</p>
${HTMLEmbed.Image.stringify({
    src: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-remini-about/after.jpg`,
    width: 400,
    height: 600,
    layout: 'fill',
})}
<p>とても高画質になっています😁</p>

<h2 id="4">
    無料版と有料版の違いとは
</h2>

<p>無料でも十分使うことができるReminiですが、課金プランであるProになるとどのようなメリットがあるのでしょうか？</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-remini-about/pro.jpg`, { of: 'webp' }),
    width: 400,
    height: 200,
    layout: 'responsive',
})}
<p>まずは料金体制から確認していきましょう。</p>

<h3>
    Proはサブスクで月額120円
</h3>
<p>Remini Proはサブスクリプションによる課金体制をとっており、月額は120円とかなりお得です。</p>

<h3>
    メリット1: 広告を見なくてよい
</h3>
<p>無料版であると、1枚の画像の高画質化(enhance)にあたり、2つの広告を見なくてはなりません。</p>
<p>また、1広告あたり30秒の時間がかかります。</p>
<p>そのため<strong>複数枚の高画質化を行いたい人にはProはおすすめできます。</strong></p>

<h3>
    メリット2: 無制限の保存
</h3>
<p>無料版であると、画像の保存に一日あたりの制限があります。</p>
<p>しかしプロにすると無制限に保存が可能です。</p>
<p>そのため<strong>複数枚の高画質化を行いたい人にはProはおすすめできます。</strong></p>


<h3>
    メリット3: さらに高画質な画像を保存できる
</h3>
<p>画像を高画質化したときに「2X」「3X」のような文字が散見できると思います。</p>
<p>これはProのみが可能な２倍、３倍高画質の画像を保存できるオプションです。</p>
<p>無料版でも１枚の写真を何回も高画質化(Enhance)すればそれと近い結果が得られますが、回数制限などもあり、一日あたり1枚か2枚が限界でしょう。</p>
<p>やはり、<strong>複数枚の高画質化を行いたい人にはProはおすすめできます。</strong></p>


<h2 id="5">
    おわりに
</h2>
<p>Reminiいかかでしたでしょうか？</p>
<p>画質にこだわりのある方はぜひインストールしてお試しください！</p>
<p>有料版も<strong>月120円とかなりリーズナブルな料金体制</strong>なので、有料版もご検討ください！</p>
${HTMLEmbed.AppDownloadButton.stringify({ iosHref: 'https://apps.apple.com/jp/app/remini-photo-enhancer/id1470373330?uo=4' , androidHref: 'https://play.google.com/store/apps/details?id=com.bigwinepot.nwdn.international'})}
`