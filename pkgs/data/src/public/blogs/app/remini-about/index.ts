import { BlogType, BlogDataType, BlogStatus, Blog, CDN_PROXY_URL_STRING } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const AppReminiBlog = new Blog({
    slug: 'app-remini-about',
    jaTitle: "Reminiとは?高画質化AIアプリReminiを徹底解説",
    enTitle: "",
    jaDescription: `今巷で話題と高画質化アプリReminiについてまとめました。筆者も普段から使用しているので１ユーザーからの信頼できる声として参考にしてください。`,
    enDescription: ``,
    jaData: jaHTML,
    enData: enHTML,
    enThumbnailURL: undefined,
    jaThumbnailURL: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-remini-about/thumbnail-ja.png`,
    index: 0,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.HTML,
    status: BlogStatus.PUBLISH,
});