import { BlogType, BlogDataType, BlogStatus, Blog, CDN_PROXY_URL_STRING } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const AppDiscordBlog = new Blog({
    slug: 'app-discord-about',
    jaTitle: "Discordとは？ゲーム特化型高音質ボイスチャットアプリを初心者向け解説",
    enTitle: "",
    jaDescription: `大人気アプリDiscordについてまとめました。筆者も普段から使用しているので１ユーザーからの信頼できる声として参考にしてください。`,
    enDescription: ``,
    jaData: jaHTML,
    enData: enHTML,
    enThumbnailURL: undefined,
    jaThumbnailURL: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-discord-about/thumbnail-ja.png`,
    index: 1,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.HTML,
    status: BlogStatus.PUBLISH,
});