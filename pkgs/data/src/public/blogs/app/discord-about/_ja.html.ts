import { CDN_PROXY_URL_STRING, HTMLEmbed, makeTransformSrc } from "@guildion/core";

export const jaHTML = `
<h2 id="1">
    Discordとは
</h2>

<p>Discord（ディスコード）とは、ゲーム特化型のチャットアプリです。</p>

<a href="https://ja.m.wikipedia.org/wiki/Discord_(%E3%82%BD%E3%83%95%E3%83%88%E3%82%A6%E3%82%A7%E3%82%A2)" target="_blank">Wikipedia</a>によるとアメリカ合衆国で開発されており、2019年時点でユーザー数が2億5000万人に達しているそうです。

<h2 id="2">
    主な用途
</h2>

<ul>
    <li>
        ボイスチャットを繋いで皆んなで楽しくゲームをする。
    </li>
    <li>
        仲間とのプレイがしたい時にゲーム仲間を探す。
    </li>
    <li>
        ゲームでの最新情報をやり取りする。
    </li>
</ul>
<p>などなどです。</p>
<p>これらの用途はゲームに特化したお話でしたが、他にも様々な種類のサーバーがあり用途は無限大です。</p>

<h3>
    ゲーム以外の主なコミュニティ
</h3>
<ul>
    <li>
        プログラミング
    </li>
    <li>
        ビジネス
    </li>
    <li>
        仮想通貨
    </li>
    <li>
        学校などのプライベートなグループ
    </li>
    <li>
        出会い系
    </li>
    <li>
        アニメ
    </li>
    <li>
        イラスト
    </li>
    <li>
        社会人
    </li>
    <li>
        作業仲間
    </li>
    <li>
        カラオケ
    </li>
    <li>
        勉強
    </li>
    <li>
        語学学習
    </li>
    <li>
        英語
    </li>
</ul>

<p>などなどすごい多岐に渡っています。</p>
<p>では早速そんなDiscordの世界に飛び込んでみましょう。</p>

<h2 id="3">
    インストール
</h2>
<p>iOS・Androidからのインストールが可能です。</p>

${HTMLEmbed.AppDownloadButton.stringify({ iosHref: 'https://apps.apple.com/us/app/discord-chat-for-games/id985746746?attemptId=68e17106-6822-44bb-ad7f-037d4359c60c&fingerprint=891665142859857961.guaxGL5qoUcFFkLu4O3sSb2h9LM' , androidHref: 'https://play.google.com/store/apps/details?id=com.discord&fingerprint=891665142859857961.guaxGL5qoUcFFkLu4O3sSb2h9LM&attemptId=dc4865f2-3c38-4e3e-9084-6ebc37acb807'})}

<p>
    そのほか<a href="https://discord.com/download" target="_blank">Windows・Mac</a>でのインストールも可能です。
</p>
<p>
    詳しくは<a href="https://discord.com/download" target="_blank">公式サイト</a>で確認してみてください。
</p>

<h2 id="4">
    会員登録
</h2>
<p>
    アプリをインストールして開いたら下記のボタンから会員登録をしましょう。
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-discord-about/register.jpg`, { of: 'webp', dh: '500' }),
    width: 300,
    height: 300,
    layout: 'fill',
})}

<h2 id="5">
    サーバーに参加
</h2>
<p>
    会員登録が済んだら早速サーバーに飛び込んでみましょう！
</p>
<p>
    1から自分でサーバーを作っても、人がいないと楽しくないので今回はdisboardと呼ばれるdiscordサーバーの掲示板アプリを使っていきます。
</p>
${HTMLEmbed.ReferenceImage.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-discord-about/disboard.png`, { of: 'webp', dh: '500' }),
    width: 1200,
    height: 630,
    layout: 'responsive',
    href: 'https://disboard.org/ja',
    name: 'Disboard'
})}

<p>
    disboardの中から気になるサーバーを探して参加ボタンから参加してみましょう！
</p>

<h2 id="6">
    サーバー内で交流を楽しむ
</h2>

<p>
    サーバーに参加するとこのような画面が出てくると思います。
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-discord-about/server.png`, { of: 'webp', dh: '500' }),
    width: 1920,
    height: 1080,
    layout: 'responsive',
})}
<p>
    左が自身の参加しているサーバー、その隣に現在開いているサーバーのチャンネルの一覧が見えると思います。
</p>
<p>
    ※サーバーによっては参加するために手順を踏まなくてはいけないものもあるのでそれらをまずは終わらしてください。
</p>

<h2 id="7">
    チャンネルの種類
</h2>


<h3 style="text-align:center;padding-bottom:12px;padding-top:20px;">
    テキストチャンネル
</h3>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-discord-about/text.png`, { of: 'webp', dh: '300' }),
    width: 200,
    height: 200,
    layout: 'fill',
})}

<p style="padding-top:20px;">
    ハッシュタグのマークをしたチャンネルはテキストチャンネルです。
</p>
<p>
    文字や画像などLINEのようなチャットが楽しめます。
</p>

<h3 style="text-align:center;padding-bottom:12px;padding-top:20px;">
    ボイスチャンネル
</h3>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-discord-about/voice.png`, { of: 'webp', dh: '300' }),
    width: 200,
    height: 200,
    layout: 'fill',
})}

<p style="padding-top:20px;">
    拡声器のマークをしたチャンネルはボイスチャンネルです。
</p>
<p>
    クリックするとそのチャンネルにいるユーザー同士でボイスチャットやビデオチャット、画面共有などが楽しめます
</p>

<h2 id="8">
    おわりに
</h2>

<p>
    Discordはいかがでしたでしょうか？
</p>
<p>
    多機能なので初めは混乱すると思いますが使っていって慣れてください。
</p>
<p>
    またdisboardを使ってお気に入りのサーバーを見つけてみてください！
</p>
<p>
    以上です！良いインターネットライフを！
</p>

${HTMLEmbed.AppDownloadButton.stringify({ iosHref: 'https://apps.apple.com/us/app/discord-chat-for-games/id985746746?attemptId=68e17106-6822-44bb-ad7f-037d4359c60c&fingerprint=891665142859857961.guaxGL5qoUcFFkLu4O3sSb2h9LM' , androidHref: 'https://play.google.com/store/apps/details?id=com.discord&fingerprint=891665142859857961.guaxGL5qoUcFFkLu4O3sSb2h9LM&attemptId=dc4865f2-3c38-4e3e-9084-6ebc37acb807'})}
`