import { BlogType, BlogDataType, BlogStatus, Blog, CDN_PROXY_URL_STRING } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const AppPinterestBlog = new Blog({
    slug: 'app-pinterest-about',
    jaTitle: "Pinterestとは？眺めるだけでアイデア万歳になる見る専門の新活用方法",
    enTitle: "",
    jaDescription: `大人気アプリPinterestについてまとめました。筆者も普段から使用しているので１ユーザーからの信頼できる声として参考にしてください。`,
    enDescription: ``,
    jaData: jaHTML,
    enData: enHTML,
    enThumbnailURL: undefined,
    jaThumbnailURL: `${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/thumbnail-ja.png`,
    index: 2,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.HTML,
    status: BlogStatus.PUBLISH,
});