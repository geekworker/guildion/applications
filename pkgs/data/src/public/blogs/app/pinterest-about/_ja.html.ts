import { CDN_PROXY_URL_STRING, HTMLEmbed, makeTransformSrc } from "@guildion/core";

export const jaHTML = `
<h2 id="1">
    Pinterestとは
</h2>
<p>
    Pinterestはお気に入りの写真や動画をピンとしてブックマークして、自分のアイデアマップにできるアプリです。
</p>
<p>
    日々自分に合った画像や動画が更新されて無限にレコメンドしてくれるものです。
</p>

<h2 id="2">
    主な用途
</h2>

<p>
    1番Pinterestをおすすめできる人は何かアイデアが欲しい時やアイデア不足で悩んだりしてる人です。
</p>
<p>
    例えば家の家具の配置で悩んだり、新しいポスターの構成やレイアウトで悩んだり、明日の晩御飯の献立を悩んだり、日々何かアイデアが欲しいという人は多いと思います。
</p>
<p>
    そこでPinterestで自分の興味のあるカテゴリーをフォローすれば無限にPinterestがアイデアをレコメンドし続けてくれるという優れものです。
</p>

<h2 id="3">
    使う場面
</h2>
<p>
    自分は仕事でUI/UXデザイナーをしていて、よくアイデア不足に陥りがちです。
</p>
<p>
    そこでPinterestを開くと無限にデザインのヒントとなるような画像や動画をレコメンドし続けてくれます。
</p>
<p>
    なので自分は少しでもレイアウトに悩んだらPinterestを開いています。笑
</p>
<p>
    Pinterestの魅力をこれでもかと語ったので早速使ってみましょう！
</p>

<h2 id="4">
    インストール
</h2>
<p>
    PinterestはiOSとAndroidでインストール可能です。
</p>
${HTMLEmbed.AppDownloadButton.stringify({ iosHref: 'https://apps.apple.com/jp/app/pinterest-%E3%81%8A%E3%81%97%E3%82%83%E3%82%8C%E3%81%AA%E7%94%BB%E5%83%8F%E3%82%84%E5%86%99%E7%9C%9F%E3%82%92%E6%A4%9C%E7%B4%A2/id429047995' , androidHref: 'https://play.google.com/store/apps/details?id=com.pinterest&hl=ja&gl=US'})}
<p>
    アプリをインストールしなくても<a href="https://www.pinterest.jp/" target="_blank">デスクトップ</a>からもアクセスできます。
</p>
<p>
    <a href="https://www.pinterest.jp/" target="_blank">https://www.pinterest.jp/</a>
</p>
${HTMLEmbed.ReferenceImage.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/logo.jpg`, { dh: '400', of: 'webp' }),
    width: 400,
    height: 240,
    layout: 'responsive',
    href: 'https://www.pinterest.jp',
    name: 'Pinterest'
})}

<h2 id="5">
    会員登録
</h2>
<p>
    まずはこのボタンを押して会員登録を済ませましょう！
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/register.jpg`, { dh: '500', of: 'webp' }),
    width: 300,
    height: 500,
    layout: 'fill',
})}

<h2 id="6">
    興味のあるカテゴリーの登録
</h2>
<p>
    自分のがレコメンドして欲しいと思うカテゴリーをフォローしたり、ピンしたりしましょう。
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/follow.jpg`, { dh: '500', of: 'webp' }),
    width: 300,
    height: 500,
    layout: 'fill',
})}

<h2 id="7">
    気に入ったアイデアをピンする
</h2>
<p>
    まずは保存しておきたいアイデアをクリックしてみましょう。
</p>
<p>
    すると詳細画面に進むのでPinボタンを押してお気に入りに登録しましょう。
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/pin.jpg`, { dh: '500', of: 'webp' }),
    width: 300,
    height: 500,
    layout: 'fill',
})}
<p>
    その際、フォルダ分けしておきたい場合は自身でフォルダ名を決めるかレコメンドされた名前からフォルダ名を決めましょう。
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/folder.jpg`, { dh: '500', of: 'webp' }),
    width: 300,
    height: 500,
    layout: 'fill',
})}

<h2 id="8">
    アイデアレコメンドマシンの完成
</h2>
<p>
    たったこれだけのステップで無限にアイデアをくれるレコメンドマシンの完成です✨
</p>
<p>
    ホーム画面を開くだけで自分にあったアイデアをレコメンドしてくれます。
</p>
${HTMLEmbed.Image.stringify({
    src: makeTransformSrc(`${CDN_PROXY_URL_STRING}/abs/geeklize/blogs/app-pinterest-about/home.jpg`, { dh: '600', of: 'webp' }),
    width: 400,
    height: 680,
    layout: 'fill',
})}
<p>
    気に入ったアイデアを発見し次第、Pinしていつでも確認できるようにしておきましょう。
</p>
<p>
    一度Pinするのを逃すと、再び同じ投稿を見つけるのは困難なので積極的にPinして保存していきましょう。
</p>

<h2 id="9">
    おわりに
</h2>

<p>
    Pinterestはいかがでしたでしょうか？
</p>
<p>
    シンプルで使いやすいと思うので、ぜひ自分にあったホーム画面にカスタマイズしていってください！
</p>
<p>
    以上です！良いインターネットライフを！
</p>

${HTMLEmbed.AppDownloadButton.stringify({ iosHref: 'https://apps.apple.com/jp/app/pinterest-%E3%81%8A%E3%81%97%E3%82%83%E3%82%8C%E3%81%AA%E7%94%BB%E5%83%8F%E3%82%84%E5%86%99%E7%9C%9F%E3%82%92%E6%A4%9C%E7%B4%A2/id429047995' , androidHref: 'https://play.google.com/store/apps/details?id=com.pinterest&hl=ja&gl=US'})}
`