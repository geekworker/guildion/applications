import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyProvisionDocument = new Document({
    slug: 'provision',
    jaTitle: "第七条 個人情報の提供",
    enTitle: "7. Provision of Personal Data",
    jaData: jaHTML,
    enData: enHTML,
    index: 7,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});