export const enHTML = `
<div>
    <h2 id="1">Provision of Personal Data</h2>
    <ol>
        <li>
            We will not provide personal information to a third party without the prior consent of the user, except in the following cases. However, this does not apply when permitted by the Personal Information Protection Law in Japan and other laws and regulations.
            <ol>
                <li>
                    When it is necessary to protect the life, body or property of a person and it is difficult to obtain the consent of the person.
                </li>
                <li>
                    When it is particularly necessary to improve public health or promote the sound development of children, and it is difficult to obtain the consent of the person.
                </li>
                <li>
                    When it is necessary for a national institution or a local public body or a person entrusted with it to cooperate in carrying out the affairs stipulated by laws and regulations, obtaining the consent of the person will hinder the performance of the affairs. When there is a risk of exerting
                </li>
                <li>
                    When the following matters are announced or announced in advance and we notify the Personal Information Protection Commission
                    <ol>
                        <li>
                            Include provision to third parties in the purpose of use
                        </li>
                        <li>Items of data provided to third parties</li>
                        <li>Means or method of provision to a third party</li>
                        <li>
                            Stop providing personal information to third parties at the request of the person
                        </li>
                        <li>How to accept the request of the person</li>
                    </ol>
                </li>
            </ol>
        </li>
        <li>
            Notwithstanding the provisions of the preceding paragraph, in the following cases, the information is provided to a third party.
            <ol>
                <li>
                    When we outsource all or part of the handling of personal information to the extent necessary to achieve the purpose of use
                </li>
                <li>
                    When personal information is provided due to business succession due to merger or other reasons
                </li>
                <li>
                    When personal information is used jointly with a specific person, the fact, the items of personal information used jointly, the range of people who jointly use it, the purpose of use of the person who uses it, and When the person is notified in advance of the name or name of the person responsible for the management of the personal information, or the person is placed in a state where the person can easily know it.
                </li>
            </ol>
        </li>
    </ol>
</div>
`