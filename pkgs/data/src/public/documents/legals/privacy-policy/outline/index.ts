import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyOutlineDocument = new Document({
    slug: 'outline',
    jaTitle: "プライバシーポリシー - 目次 -",
    enTitle: "Privacy Policies - Outline -",
    jaData: jaHTML,
    enData: enHTML,
    index: 0,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});