export const jaHTML = `
<div>
    <h2 id="1">はじめに</h2>
    <p>
        このプライバシーポリシー（以下、「本プライバシーポリシー」といいます。）は、「Geek Worker」（以下「当方」といいます。）がこのウェブサイト「Guildion」および「Guildion」を通じて提供されるサービス（以下,「本サービス」といいます。）やその他の製品のユーザーの個人情報の取扱いについての利用規約を定めるものです。本サービス利用者の皆さま（以下，「ユーザー」といいます。）には、本ポリシーの利用規約と当方の利用規約に同意いただくことで、ユーザーは本サービスを利用することができます。
    </p>

    <hr/>

    <h2 id="2">目次</h2>
    <ol>
        <li>
            <a href="/ja/documents/legal/privacy-policy/definition">定義</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/to-be-collected">収集する個人情報</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/purpose">個人情報を収集する目的</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/cookie">クッキー</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/google-analytics">Google Analytics</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/third-party-link">外部リンク・メディア</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/provision">個人情報の提供</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/modification">個人情報の訂正・利用停止</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/foreign-country">日本国外のユーザー</a>
        </li>
        <li>
            <a href="/ja/documents/legal/privacy-policy/update">プライバシーポリシーの変更</a>
        </li>
    </ol>

    <h2 id="3">発行日</h2>
    <p>2020年3月14日</p>
</div>
`