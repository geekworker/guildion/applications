export const enHTML = `
<div>
    <div>
        <h2>
            <strong>Causion</strong>
        </h2>
        <aside class="danger">
            <strong>Note:</strong>
            <span>
                This documents are not completely translated to English. Then Japanese words or unnatural English words will some times appear. If you discover part of you not understanding, we recommend you to check Japanese versions. We prioritize Japanese version if documents have conflicting content with other langedge versions.
            </span>
        </aside>
    </div>

    <div>
        <h2 id="1"><strong>Firstly</strong></h2>
        <p>
            Members managing Guildion (“we” or “us” or “our”, depending upon the context) respects the privacy of our users (“user” or “you”, depending upon the context). This Privacy Policy explains how we collect, use, disclose, and safeguard your information when you visit any and all products and services (collectively, the “Services”) provided by us. Please read this Privacy Policy carefully. IF YOU DO NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY, PLEASE DO NOT ACCESS THE SERVICES.
        </p>
        <hr/>

        <h2 id="2"><strong>Outline</strong></h2>
        <ol>
            <li>
                <a href="/en/documents/legal/privacy-policy/definition">Definition</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/to-be-collected-privacy">Personal Data to be collected</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/purposePurpose of collecting Personal Data</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/cookie">Cookie</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/google-analytics">Google Analytics</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/third-party-link">Links to Other Websites</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/provision">Provision of Personal Data</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/modification">Modification and Deletion of Personal Data</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/foreign-country">Users outside Japan</a>
            </li>
            <li>
                <a href="/en/documents/legal/privacy-policy/update">Modification to these Privacy Policies</a>
            </li>
        </ol>
    </div>

    <h2 id="3">Effective</h2>
    <p>March 14, 2021</p>
</div>
`