import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyCookieDocument = new Document({
    slug: 'cookie',
    jaTitle: "第四条 クッキー",
    enTitle: "4. Cookie",
    jaData: jaHTML,
    enData: enHTML,
    index: 4,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});