export const jaHTML = `
<div>
    <h2 id="1">Google Analytics</h2>
    <p>
        本サービスでは、Googleによるアクセス解析ツール「Googleアナリティクス」を利用しています。このGoogleアナリティクスはトラフィックデータの収集のためにCookieを使用しています。このトラフィックデータは匿名で収集されており、個人を特定するものではありません。この機能はCookieを無効にすることで収集を拒否することが出来ますので、お使いのブラウザの設定をご確認ください。この規約に関して、詳しくは<a
            target="_blank"
            href="https://www.google.com/analytics/terms/jp.html"
        >
            ここをクリック
        </a>してください。
    </p>
</div>
`