export const enHTML = `
<div>
    <h2 id="1">Google Analytics</h2>
    <p>
        This service uses the access analysis tool "Google Analytics" by Google. This Google Analytics uses cookies to collect traffic data. This traffic data is collected anonymously and is not personally identifiable. This function can refuse collection by disabling cookies, so please check the settings of your browser.
        <a
            target="_blank"
            href="https://marketingplatform.google.com/about/analytics/terms/us/"
        >
            Click here
        </a> for more information on these terms
    </p>
</div>
`