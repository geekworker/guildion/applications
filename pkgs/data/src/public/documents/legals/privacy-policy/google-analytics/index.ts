import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyGoogleAnalyticsDocument = new Document({
    slug: 'google-analytics',
    jaTitle: "第五条 Google Analytics",
    enTitle: "5. Google Analytics",
    jaData: jaHTML,
    enData: enHTML,
    index: 5,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});