import { DocumentSection, DocumentSectionType } from '@guildion/core';

export const PrivacyPolicySection = new DocumentSection({
    slug: 'privacy-policy',
    jaName: 'プライバシーポリシー',
    enName: 'Privacy Policy',
    index: 1,
    type: DocumentSectionType.ALL_IN_ONE,
});