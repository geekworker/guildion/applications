import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyForeignCountryDocument = new Document({
    slug: 'foreign-country',
    jaTitle: "第九条 日本国外のユーザー",
    enTitle: "9. Users outside Japan",
    jaData: jaHTML,
    enData: enHTML,
    index: 9,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});