export const enHTML = `
<div>
    <h2 id="2"><strong>User of Your Information</strong></h2>
    <p>
        The following items are reasons why we collect your personal data
    </p>
    <ol>
        <li>For provisioning and managing our services</li>
        <li>
            For replying user's contact (including identification and personal authentication)
        </li>
        <li>
            For sending information on new features, updates, campaigns, etc. of the service that the user is using and information on other services provided by us.
        </li>
        <li>
            For maintenance, important notices, etc. to contact you as needed
        </li>
        <li>For personal authentication of the user</li>
        <li>For research, statistics and analysis of marketing data</li>
        <li>
            To identify users who have violated the service against our <a href="/documents/legal/terms-of-service" target="_blank">
                Terms of Service
            </a> or who intend to use the service for fraudulent or unreasonable purposes, and refuse to use the service.
        </li>
        <li>
            To allow users to view, change, delete, and view usage status of their own registration information.
        </li>
        <li>
            To charge the user a usage fee for a paid service
        </li>
        <li>Purpose incidental to the above purpose of use</li>
    </ol>
</div>
`