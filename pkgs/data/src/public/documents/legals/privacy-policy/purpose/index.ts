import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyPurposeDocument = new Document({
    slug: 'purpose',
    jaTitle: "第三条 個人情報を収集する目的",
    enTitle: "3. Purpose of collecting Personal Data",
    jaData: jaHTML,
    enData: enHTML,
    index: 3,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});