export const enHTML = `
<div>
    <h2 id="1">Modification and Deletion of Personal Data</h2>
    <ol>
        <li>
            If the user's personal information held by us is incorrect, the user will correct, add or delete the personal information to us according to the procedure established by us (hereinafter referred to as "correction, etc."). ) Can be charged.
        </li>
        <li>
            If we receive the request set forth in the preceding paragraph from the user and determine that it is necessary to respond to the request, we shall correct the personal information, etc. without delay.
        </li>
        <li>
            We will notify the user without delay when we make corrections, etc. based on the provisions of the preceding paragraph, or when we decide not to make corrections, etc.
        </li>
    </ol>
</div>
`