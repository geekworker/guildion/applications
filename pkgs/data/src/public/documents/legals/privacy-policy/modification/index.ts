import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyModificationDocument = new Document({
    slug: 'modification',
    jaTitle: "第八条 個人情報の訂正・利用停止",
    enTitle: "8. Modification and Deletion of Personal Data",
    jaData: jaHTML,
    enData: enHTML,
    index: 8,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});