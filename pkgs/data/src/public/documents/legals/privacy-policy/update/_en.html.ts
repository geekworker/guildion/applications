export const enHTML = `
<div>
    <h2 id="1">Modification to these Privacy Policies</h2>
    <ol>
        <li>
            The contents of this policy may be changed without notifying the user, except for laws and regulations and other matters specified otherwise in this policy.
        </li>
        <li>
            Unless otherwise specified by us, the changed privacy policy shall take effect from the time it is posted on this website.
        </li>
    </ol>
</div>
`