import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyUpdateDocument = new Document({
    slug: 'update',
    jaTitle: "第十条 プライバシーポリシーの変更",
    enTitle: "10. Modification to these Privacy Policies",
    jaData: jaHTML,
    enData: enHTML,
    index: 10,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});