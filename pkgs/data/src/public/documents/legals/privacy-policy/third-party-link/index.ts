import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyThirdPartyLinkDocument = new Document({
    slug: 'third-party-link',
    jaTitle: "第六条 外部リンク・メディア",
    enTitle: "6. Links to Other Websites",
    jaData: jaHTML,
    enData: enHTML,
    index: 6,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});