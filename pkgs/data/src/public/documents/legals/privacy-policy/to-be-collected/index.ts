import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const PrivacyPolicyToBeCollectedDocument = new Document({
    slug: 'to-be-collected',
    jaTitle: "第二条 収集する個人情報",
    enTitle: "2. Personal Data to be collected",
    jaData: jaHTML,
    enData: enHTML,
    index: 2,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});