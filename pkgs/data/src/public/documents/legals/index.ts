import { DocumentGroup, DocumentGroupType } from '@guildion/core';

export const LegalsGroup = new DocumentGroup({
    slug: 'legal',
    jaName: '法務',
    enName: 'Legal',
    type: DocumentGroupType.DEFAULT,
});