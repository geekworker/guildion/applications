export const enHTML = `
<div>
    <h2 id="1"><strong>Member Registration</strong></h2>
    <ol>
        <li>
            In this service, the member registration shall be completed if who submit it by the method specified by us agree these terms and conditions and we approved it.
        </li>
        <li>
            The applicant for registration shall be deny if does not meet the examination criteria set by us, or when we consider it inappropriate.
        </li>
        <li>
            We will deny the member registration if who submit it apply below cases. We have no responsibility to teach the reasons.
            <ol>
                <li>
                    When the member registration have false matters.
                </li>
                <li>
                    When the person who submit the member registration have violated this agreement.
                </li>
                <li>When the person who submit the member registration is under 13 years old.</li>
                <li>
                    Maintaining and managing antisocial forces, etc. (meaning gangsters, gangsters, right-wing groups, antisocial forces, and other equivalents; the same shall apply hereinafter), or through funding and other means. Or a user who we judge to have some kind of interaction or involvement with antisocial forces such as cooperating or being involved in management.
                </li>
            </ol>
        </li>
    </ol>
    <h2 id="2"><strong>Account Management</strong></h2>
    <ol>
        <li>
            If Users register any authentication information when using the Services, they must exercise due care in handling such information at their own responsibility to ensure that such information is not used in an unlawful manner. We may treat any and all activities conducted under the authentication information as activities that have been conducted by the User with whom the authentication information is registered.
        </li>
        <li>
            Any User who has registered for the Services may delete such User’s account and cancel the Services at any time.
        </li>
        <li>
            Any and all rights of a User to use the Service shall cease to exist when such User’s account has been deleted for any reason. Please take note that an account cannot be retrieved even if a User has accidentally deleted their account.
        </li>
        <li>
            Each account in the Services is for exclusive use and belongs solely to the User of such account. Users may not transfer, lease or otherwise dispose their rights to use the Service to any third party, nor may the same be inherited or succeeded to by any third party.
        </li>
    </ol>
</div>
`