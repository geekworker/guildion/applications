import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSAccountDocument = new Document({
    slug: 'account',
    jaTitle: "第四条 アカウント管理",
    enTitle: "4. Account",
    jaData: jaHTML,
    enData: enHTML,
    index: 4,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});