export const jaHTML = `
<div>
    <h2 id="1"><strong>会員登録</strong></h2>
    <ol>
        <li>
            本サービスにおいては、登録希望者が本規約に同意の上、当方の定める方法によって利用登録を申請し、当方がこれを承認することによって、利用登録が完了するものとします。
        </li>
        <li>
            登録希望者が当方が定める審査基準に合致しない場合その他当方が不適当と考える場合には、会員登録希望者へ理由を開示することなく、当該登録を拒否することができるものとします。
        </li>
        <li>
            当方は、利用登録の申請者に以下の事由があると判断した場合、利用登録の申請を承認しないことがあり、その理由については一切の開示義務を負わないものとします。
            <ol>
                <li>
                    利用登録の申請に際して虚偽の事項を届け出た場合
                </li>
                <li>
                    本規約に違反したことがある者からの申請である場合
                </li>
                <li>13歳未満のユーザー</li>
                <li>
                    反社会的勢力等（暴力団、暴力団員、右翼団体、反社会的勢力、その他これに準ずる者を意味します。以下同じ。）である、または資金提供その他を通じて反社会的勢力等の維持、運営若しくは経営に協力若しくは関与する等反社会的勢力等との何らかの交流もしくは関与を行っていると当方が判断したユーザー
                </li>
            </ol>
        </li>
    </ol>
    <h2 id="2"><strong>アカウント管理について</strong></h2>
    <ol>
        <li>
            ユーザーは自己の責任と費用負担によって、認証情報および登録情報の管理を行うものとします。
        </li>
        <li>
            ユーザーは、いつでもアカウントを削除して退会することができます。
        </li>
        <li>
            アカウントを削除すると、ユーザーの本サービスにおける全ての利用権は消滅します。誤ってアカウントを削除した場合であっても、削除された情報は復旧できません。そのため、アカウントの削除は自己責任であり、これにより生じた損害に対する責任は当方にはないものとします。
        </li>
        <li>
            本サービスのアカウントの権利はユーザーに帰属します。ユーザーの本サービスにおけるすべての利用権は、いかなる場合にも、認証情報および登録情報を第三者に譲渡または貸与し、もしくは第三者と共用することはできません。
        </li>
    </ol>
</div>
`