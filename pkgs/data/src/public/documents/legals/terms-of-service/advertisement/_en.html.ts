export const enHTML = `
<div>
    <h2 id="1"><strong>Advertisement in this service</strong></h2>
    <ol>
        <li>
            This service does not provide any advertisement.
        </li>
    </ol>

    <h2 id="2"><strong>Advertisement in Third-Party Services</strong></h2>
    <ol>
        <li>
            When using the external service, the user shall comply with the terms and conditions regarding advertising of the external service.
        </li>
    </ol>
</div>
`