export const jaHTML = `
<div>
    <h2 id="1"><strong>本サービスにおける広告</strong></h2>
    <ol>
        <li>
            本サービスには広告は掲載されていません。
        </li>
    </ol>

    <h2 id="2"><strong>第三者サービスによる広告</strong></h2>
    <ol>
        <li>
            ユーザーは、外部サービスの利用に当たっては、外部サービスの広告に関する規約等を遵守するものとします。
        </li>
    </ol>
</div>
`