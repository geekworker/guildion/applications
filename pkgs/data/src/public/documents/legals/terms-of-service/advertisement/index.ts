import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSAdvertisementDocument = new Document({
    slug: 'advertisement',
    jaTitle: "第十条 広告表示",
    enTitle: "10. Advertisement",
    jaData: jaHTML,
    enData: enHTML,
    index: 10,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});