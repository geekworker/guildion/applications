import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSPrivacyDocument = new Document({
    slug: 'privacy',
    jaTitle: "第五条 個人情報等の取り扱い",
    enTitle: "5. Privacy",
    jaData: jaHTML,
    enData: enHTML,
    index: 5,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});