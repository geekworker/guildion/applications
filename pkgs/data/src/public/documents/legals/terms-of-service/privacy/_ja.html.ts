export const jaHTML = `
<div>
    <h2 id="1"><strong>プライバシー</strong></h2>
    <ol>
        <li>当方は、ユーザーのプライバシーを尊重しています。</li>
        <li>
            当方は、本サービスの利用によって取得する個人情報については、当方<a href="/ja/documents/legal/privacy-policy" target="_blank">プライバシーポリシー</a>に従い適切に取り扱うものとします。
        </li>
        <li>当方は、ユーザーから収集した情報を安全に管理するために最大限の注意を払っています。</li>
    </ol>
</div>
`