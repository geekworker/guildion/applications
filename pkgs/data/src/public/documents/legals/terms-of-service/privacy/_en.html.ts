export const enHTML = `
<div>
    <h2 id="1"><strong>Privacy</strong></h2>
    <ol>
        <li>We care about data privacy and security.</li>
        <li>Please review our <a href="/en/documents/legal/privacy-policy" target="_blank">Privacy Policy</a></li>
        <li>
            By using the service, you agree to be bound by our <a href="/en/documents/legal/privacy-policy" target="_blank">Privacy Policy</a>, which is incorporated into these Terms of Service.
        </li>
    </ol>
</div>
`