export const enHTML = `
<div>
    <h2 id="1"><strong>Definition</strong></h2>
    <p>
        The following words and terms shall have the meanings set forth below when they are used in these Terms of Service.
    </p>
    <ol>
        <li>
            “We“, “Our“, “Us“：Members managing “Guildion“, depending upon the context, depending upon the context
        </li>
        <li>
            “Services”： all products and services provided by us
        </li>
        <li>
            “Users”：Members and everyone who uses those services
        </li>
        <li>
            ”Registration information”：All information provided to us by applicants for membership registration or users for the purpose of receiving membership services
        </li>
        <li>
            ”Authentication information”：Of the registered information, ID and password and other information necessary for us to authenticate the connection from the member
        </li>
        <li>
            “User deletion“：The member deletes the registration information and terminates the provision of the member service by the method specified by this agreement and the Services.
        </li>
        <li>
            ”Contents”：Information content such as messages, attachments, playlists, requests to other users or reading materials posted by users in Guildion in this service
        </li>
        <li>
            ”Intellectual property rights, etc.”：All copyrights, patents, trademarks and other property rights related to this service.
        </li>
    </ol>
</div>
`