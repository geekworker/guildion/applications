import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSDefinitionDocument = new Document({
    slug: 'definition',
    jaTitle: "第一条 定義",
    enTitle: "1. Definition",
    jaData: jaHTML,
    enData: enHTML,
    index: 1,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});