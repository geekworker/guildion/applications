export const enHTML = `
<div>
    <h2 id="1"><strong>Agreement with Terms of Service</strong></h2>
    <ol>
        <li>
            All Users shall use the Services in accordance with these Terms of Service. Users may not use the Services unless they agree to these Terms of Service.
        </li>
        <li>
            If there are Separate Terms of Service applicable to the Services, Users shall also comply with such Separate Terms of Service as well as these Terms of Service in using the Services.
        </li>
    </ol>
</div>
`