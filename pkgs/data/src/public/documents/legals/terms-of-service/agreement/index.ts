import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSAgreementDocument = new Document({
    slug: 'agreement',
    jaTitle: "第二条 規約への同意",
    enTitle: "2. Agreement to these Terms of Service",
    jaData: jaHTML,
    enData: enHTML,
    index: 2,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});