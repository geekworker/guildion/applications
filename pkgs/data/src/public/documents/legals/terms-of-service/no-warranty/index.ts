import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSNoWarrantyDocument = new Document({
    slug: 'no-warranty',
    jaTitle: "第十四条 非保証",
    enTitle: "14. NO WARRANTY",
    jaData: jaHTML,
    enData: enHTML,
    index: 14,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});