export const jaHTML = `
<div>
    <h2 id="1"><strong>非保証</strong></h2>
    <ol>
        <li>
            当方は、本サービスに事実上または法律上の瑕疵（安全性、信頼性、正確性、完全性、有効性、特定の目的への適合性、セキュリティなどに関する欠陥、エラーやバグ、権利侵害などを含みます。）がないことを明示的にも黙示的にも保証しておりません。
        </li>
        <li>
            本サービス上で表示または提供される情報について、当方は、その内容の正確性、完全性、有用性、信頼性、適時性等につき、いかなる保証をも提供するものではありません。
        </li>
        <li>
            ユーザーは、コンピューター上で動作するすべてのソフトウェア（本サービスの一部として提供されるものを含みます）には、既知及び未知の脆弱性が存在することを予め了解し、ユーザーの判断と責任において、コンピューターセキュリティ上必要な措置を講じます。当方は、本サービスに関して合理的なセキュリティ措置を講じていますが、当方サービスへの不正アクセス又は当方サービスの不正利用を完全に防止することについて、いかなる保証も行うものではありません。
        </li>
    </ol>
</div>
`