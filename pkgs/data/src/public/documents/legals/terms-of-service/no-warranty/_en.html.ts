export const enHTML = `
<div>
    <h2 id="1"><strong>NO WARRANTY</strong></h2>
    <ol>
        <li>
            We have virtually or legal defects in this service (safety, reliability, accuracy, completeness, effectiveness, suitability for a specific purpose, security defects, errors and bugs, infringement of rights, etc. Includes.) We do not guarantee, either explicitly or implicitly, that there is no such thing.
        </li>
        <li>
            We do not guarantee the accuracy, completeness, usefulness, reliability, timeliness, etc. of the information displayed or provided on this service.
        </li>
        <li>
            The user understands in advance that all software running on the computer (including those provided as part of this service) contains known and unknown vulnerabilities, and at the user's discretion and responsibility. , Take necessary measures for computer security. Although we have taken reasonable security measures regarding this service, we do not guarantee that we will completely prevent unauthorized access to our service or unauthorized use of our service.
        </li>
    </ol>
</div>
`