import { DocumentSection, DocumentSectionType } from '@guildion/core';

export const TOSSection = new DocumentSection({
    slug: 'terms-of-service',
    jaName: '利用規約',
    enName: 'Terms of Service',
    index: 0,
    type: DocumentSectionType.ALL_IN_ONE,
});