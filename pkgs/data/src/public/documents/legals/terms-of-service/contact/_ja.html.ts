export const jaHTML = `
<div>
    <h2 id="1"><strong>当方とユーザー間との連絡・通知</strong></h2>
    <ol>
        <li>
            本サービスに関する当方からユーザーの通知・連絡は、当方が運営するウェブサイト・アプリ内の適宜の場所への掲示その他、当方が適当と判断する方法により行ないます。
        </li>
        <li>
            ユーザーは当方からの連絡・通知を受信できるよう登録情報を正しく維持するものとし、当方からの連絡・通知が受信できなかったためにユーザーが被った不利益については、ユーザー自身に責任があるものとし、当方は一切の責任を負いません。
        </li>
        <li>
            本サービスに関するユーザーから当方への連絡は、<a href="/ja/documents/supports/contacts/us">当方が別途定める連絡窓口</a>により行っていただきます。またこの連絡窓口以外からの連絡は不当であるとみなし、当方はその連絡に応じる義務はないものとします。
        </li>
    </ol>
</div>
`