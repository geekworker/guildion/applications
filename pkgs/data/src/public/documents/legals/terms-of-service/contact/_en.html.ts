export const enHTML = `
<div>
    <h2 id="1"><strong>Contact and Notification</strong></h2>
    <ol>
        <li>
            We will notify and contact the user regarding this service by posting it at an appropriate place in the website and application operated by us or by any other method that we deem appropriate.
        </li>
        <li>
            The user shall maintain the registration information correctly so that he / she can receive the contact / notification from us, and he / she shall be responsible for the disadvantage suffered by the user due to the failure to receive the contact / notification from us. , We do not take any responsibility.
        </li>
        <li>
            Users regarding this service will contact us through <a href="/en/documents/supports/contacts/us">the contact point separately determined by us</a>. In addition, we consider that the contact from other than this contact point is unreasonable, and we shall not be obliged to respond to the contact.
        </li>
    </ol>
</div>
`