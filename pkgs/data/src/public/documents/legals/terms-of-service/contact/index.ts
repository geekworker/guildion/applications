import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSContactDocument = new Document({
    slug: 'contact',
    jaTitle: "第十五条 当方とユーザー間との連絡・通知",
    enTitle: "15. Contact and Notification",
    jaData: jaHTML,
    enData: enHTML,
    index: 15,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});