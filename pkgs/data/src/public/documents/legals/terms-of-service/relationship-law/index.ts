import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSRelationshipLawDocument = new Document({
    slug: 'relationship-law',
    jaTitle: "第十六条 本規約と法令の関係",
    enTitle: "16. Relationship between these Terms of Service and Laws and Regulations",
    jaData: jaHTML,
    enData: enHTML,
    index: 16,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});