export const enHTML = `
<div>
    <h2 id="1"><strong>LIMITATION LIABILITY</strong></h2>
    <ol>
        <li>
            We are not responsible for any damage caused to the user due to this service. However, if the contract between us and the user regarding this service (including this agreement) is a consumer contract stipulated in the Consumer Contract Law, this disclaimer does not apply.
        </li>
        <li>
            Even in the case prescribed in the provision of the preceding paragraph, we are the damage caused by special circumstances among the damage caused to the user due to default or illegal act due to our negligence (excluding gross negligence) (we or the user). We do not take any responsibility for foreseeing or foreseeing the occurrence of damage. In addition, compensation for damages caused to the user due to default or illegal acts due to our negligence (excluding gross negligence) shall be limited to the amount of usage fee received from the user in the month in which the damage occurred.
        </li>
        <li>
            We are not responsible for any transactions, communications or disputes that occur between the user and other users or third parties regarding this service.
        </li>
        <li>
            If a user damages the honor of another person, infringes the right to privacy, violates copyright law, or infringes the rights of another person, the user must resolve it at his / her own risk and expense. We must take no responsibility.
        </li>
        <li>
            In using our service, it is assumed that the user has prepared the Internet environment, personal computer, tablet terminal, and smartphone necessary for that purpose. We shall not be liable for any loss, compensation, etc. due to the user's lack of preparation of the environment.
        </li>
        <li>
            We are not responsible for any damage suffered by the user when the provision of this service is stopped or terminated.
        </li>
        <li>
            We are liable or liable for any errors or omissions in any Content, and for any loss or damage incurred as a result of posting, emailing, transmitting or otherwise using the Content made available through the Services. I will not bear it.
        </li>
        <li>
            If you move from this site to another site by a link or banner, we will not take any responsibility for the information, services, etc. provided at the destination site.
        </li>
    </ol>
</div>
`