import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSLimitationLiabilityDocument = new Document({
    slug: 'limitation-liability',
    jaTitle: "第十二条 当方の免責事項",
    enTitle: "12. LIMITATION LIABILITY",
    jaData: jaHTML,
    enData: enHTML,
    index: 12,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});