import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSThirdPartiesDocument = new Document({
    slug: 'third-parties',
    jaTitle: "第九条 第三者のサービス",
    enTitle: "9. Third-Party Services",
    jaData: jaHTML,
    enData: enHTML,
    index: 9,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});