export const enHTML = `
<div>
    <h2 id="1"><strong>Third-Party Services</strong></h2>
    <ol>
        <li>
            When using the external service, the user shall comply with the terms of use of the external service.
        </li>
        <li>
            The user shall use the external service at his / her own risk, and we shall not be liable for any damage caused to the user in connection with the use of the external service.
        </li>
        <li>
            This service is also linked with services that we do not manage, and some damage occurs to the user due to the inability to use this service due to sudden stoppages, changes, etc. of services that we do not manage. Even if you do, the user agrees in advance that we do not guarantee the user.
        </li>
        <li>
            We shall be able to suspend, change, discontinue or terminate the use of external services in this service due to changes in the specifications of external services or other reasons without prior notice to the user.
        </li>
    </ol>
</div>
`