import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSUpdateDocument = new Document({
    slug: 'update',
    jaTitle: "第三条 規約の更新・変更",
    enTitle: "3. Modification to these Terms of Service",
    jaData: jaHTML,
    enData: enHTML,
    index: 3,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});