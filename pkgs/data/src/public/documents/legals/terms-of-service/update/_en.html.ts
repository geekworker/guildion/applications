export const enHTML = `
<div>
    <h2 id="1"><strong>Modification to these Terms of Service</strong></h2>
    <p>
        We will endeavor to notify you of any changes to this agreement as much as possible, but if we deem it necessary, we may change this agreement at any time without notifying the user. If you start using this service after changing this agreement, the user will be deemed to have agreed to the changed agreement.
    </p>
</div>
`