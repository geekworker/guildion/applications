import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSContentsDocument = new Document({
    slug: 'contents',
    jaTitle: "第七条 コンテンツ",
    enTitle: "7. Contents",
    jaData: jaHTML,
    enData: enHTML,
    index: 7,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});