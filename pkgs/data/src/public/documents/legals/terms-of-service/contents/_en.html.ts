export const enHTML = `
<div>
    <h2 id="1"><strong>Contents provided by us</strong></h2>
    <ol>
        <li>
            All intellectual property rights related to the content provided by this service belong to us or the person who has licensed us.
        </li>
        <li>
            We grant the user private use of all the content provided by us through this service within the scope of use of this service, but we can freely use, earn, and dispose of it to the user. We do not transfer or recognize rights similar to ownership.
        </li>
        <li>
            Trademarks, logos, service marks, etc (”trademarks”). may be displayed on this service, but we do not transfer or license the trademarks to users or other third parties.
        </li>
    </ol>

    <h2 id="2"><strong>Contents provided by Users (UGC) </strong></h2>
    <ol>
        <li>
            We prohibit posting content that infringes the intellectual property rights of third parties, including copyrights and trademark rights.
        </li>
        <li>
            All copyrights (including the rights stipulated in Articles 21 to 28 of the Copyright Act) regarding content (still images, videos, text information and all other information) posted or otherwise transmitted by the user on this service. (Including the copyright of) belongs to the user. However, at the time of sending the content, the user is non-exclusive to host, use, distribute, modify, operate, copy, perform, publish, translate and create derivative works of the content in Japan and abroad. , Transferable, sublicensable, and free of charge.
        </li>
        <li>
            The user shall not exercise the moral rights of the author within this service.
        </li>
        <li>
            Users are responsible for backing up the posted content. We are not obligated to back up the posted content.
        </li>
        <li>
            This service may include a function that allows multiple users to post, modify, delete, etc. In this case, the user authorizes other users to edit the posted content.
        </li>
    </ol>
</div>
`