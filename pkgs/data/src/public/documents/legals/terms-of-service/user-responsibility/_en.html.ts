export const enHTML = `
<div>
    <h2 id="1"><strong>User Reposponsibility</strong></h2>
    <ol>
        <li>
            The user shall use this service at his / her own risk, and shall bear all responsibility for all actions performed in this service and the results thereof.
        </li>

        <li>
            The user shall investigate whether the use of this service violates the laws and regulations applicable to the user, the internal rules of industry groups, etc. based on his / her own responsibility and expense, and we shall investigate the book by the user. We do not guarantee that the use of the service will comply with the laws and regulations applicable to users, the internal rules of industry groups, etc.
        </li>
        <li>
            If we find that the user is using this service in violation of this agreement, we will take measures that we deem necessary and appropriate. However, we have no obligation to prevent or correct such violations.
        </li>
        <li>
            The user directly or indirectly suffers some damage (attorney's fee) due to the use of this service (including the case where we receive a complaint from a third party due to such use). If you incur the burden of), you must immediately compensate for it according to our request.
        </li>
        <li>
            All equipment, software, and communication means for the user to use this service must be properly prepared, installed, and operated by the user himself / herself at his / her own risk and expense. We are not involved in the user's access environment and are not responsible for their preparation and operation.
        </li>
        <li>
            We do not guarantee the validity or compatibility of websites and services with respect to the user's computer equipment and environment, and are caused by the environment of the personal computer, smartphone terminal, mobile terminal, line, software, etc. used by the user, or computer virus infection, etc. We shall not be liable for any damages caused by this.
        </li>
    </ol>
</div>
`