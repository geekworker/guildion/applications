import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSUserResponsibilityDocument = new Document({
    slug: 'user-responsibility',
    jaTitle: "第十三条 ユーザーの責任",
    enTitle: "13. User Responsibility",
    jaData: jaHTML,
    enData: enHTML,
    index: 13,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});