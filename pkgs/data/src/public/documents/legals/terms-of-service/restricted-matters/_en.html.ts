export const enHTML = `
<div>
    <h2 id="1"><strong>Restricted Matters</strong></h2>
    <p>
        When using this service, the user shall not perform the following acts, and shall not perform any act that causes a third party to perform them. In addition, if we admit that the user has violated the prohibited matters, we shall be able to take measures that we deem necessary, such as suspension of use, deletion of posts, etc., and the user agrees without objection. Suppose.
    </p>
    <ol>
        <li>Acts that violate this agreement</li>
        <li>Acts of including falsehood or error in registration information</li>
        <li>Act of impersonating a third party and registering as a member</li>
        <li>Acts of using this service by pretending to be a third party</li>
        <li>Acts that violate the law or public order and morals</li>
        <li>Acts related to criminal acts</li>
        <li>
            Acts that destroy or interfere with the functions of us, other users of this service, or the server or network of a third party
        </li>
        <li>Acts of falsifying information that can be used for this service</li>
        <li>
            The act of transmitting data that exceeds a certain amount of data specified by us through this service
        </li>
        <li>Solicitation to other registered users such as advertisement distribution</li>
        <li>Acts that may interfere with the operation of our service</li>
        <li>
            Acts of collecting or accumulating personal information about other users
        </li>
        <li>Unauthorized access or attempting this</li>
        <li>Acts of impersonating another user</li>
        <li>
            Acts that directly or indirectly benefit antisocial forces in connection with our services
        </li>
        <li>
            Acts that infringe the intellectual property rights, portrait rights, privacy, honor or other rights or interests of us, other users of this service or third parties
        </li>
        <li>
            Acts of posting or transmitting on this service the contents that we judge to include or include the following expressions
            <ol>
                <li>Overly violent expression</li>
                <li>
                    A fraudulent expression using means that can mislead others.
                </li>
                <li>An expression that inspires the gambling spirit of others.</li>
                <li>Explicit sexual expression</li>
                <li>
                    Expressions that lead to discrimination based on race, nationality, beliefs, gender, social status, family origin, etc.
                </li>
                <li>
                    Expressions that induce or encourage suicide, self-harm, or substance abuse
                </li>
                <li>
                    Expressions that make others uncomfortable, including other antisocial content
                </li>
            </ol>
        </li>
        <li>
            Acts that we judge to be or to be the following
            <ol>
                <li>
                    Sales, advertising, advertising, solicitation, and other acts for the purpose of profit (excluding those approved by us)
                </li>
                <li>Acts aimed at sexual or obscene acts</li>
                <li>
                    The act of disseminating unreasonable facts that are not directly experienced or recognized to an unspecified number of people
                </li>
                <li>
                    Acts aimed at meeting or dating an unfamiliar opposite sex
                </li>
                <li>
                    Acts aimed at harassing or slandering other users
                </li>
                <li>
                    Acts aimed at causing disadvantage, damage or discomfort to us, other users of this service, or third parties
                </li>
                <li>
                    Other acts of using this service for a purpose different from the intended purpose of use of this service
                </li>
            </ol>
        </li>
        <li>Religious activities or solicitations to religious groups</li>
        <li>Other acts that we deem inappropriate</li>
    </ol>
</div>
`