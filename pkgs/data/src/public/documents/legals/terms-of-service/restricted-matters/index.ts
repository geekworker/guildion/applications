import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSRestrictedMattersDocument = new Document({
    slug: 'restricted-matters',
    jaTitle: "第十一条 禁止事項",
    enTitle: "11. Restricted Matters",
    jaData: jaHTML,
    enData: enHTML,
    index: 11,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});