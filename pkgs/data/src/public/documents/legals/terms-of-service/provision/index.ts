import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSProvisionDocument = new Document({
    slug: 'provision',
    jaTitle: "第六条 サービスの提供",
    enTitle: "6. Provision of the Service",
    jaData: jaHTML,
    enData: enHTML,
    index: 6,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});