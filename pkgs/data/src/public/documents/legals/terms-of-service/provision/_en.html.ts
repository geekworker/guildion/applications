export const enHTML = `
<div>
    <h2 id="1"><strong>Provision of the service</strong></h2>
    <ol>
        <li>
            In using our service, it is assumed that the user has prepared the Internet environment, personal computer, tablet terminal, and smartphone necessary for that purpose. We shall not be liable for any loss, compensation, etc. due to the user's lack of preparation of the environment.
        </li>
        <li>
            All or part of this service can only be used by users who meet the age, identity verification, registration information, and other conditions that we deem necessary.
        </li>
        <li>
            We may change all or part of the contents of this service at any time without notifying the user in advance.
        </li>
        <li>
            We may discontinue the provision of all or part of this service without notifying the user in advance if any of the following applies.
            <ol>
                <li>
                    System maintenance and system repair or restoration
                </li>
                <li>
                    When the computer, communication line, etc. stop due to an accident
                </li>
                <li>
                    When this service cannot be operated due to force majeure such as fire, power outage, or natural disaster.
                </li>
                <li>
                    When trouble, interruption or suspension of service provision, suspension of cooperation with this service, specification change, etc. occur in the external service
                </li>
                <li>
                    In addition, when we judge that it is necessary to stop or suspend
                </li>
            </ol>
        </li>
    </ol>
    <h2 id="2"><strong>Emergency</strong></h2>
    <ol>
        <li>
            If the user discovers an abnormality, failure or failure regarding this service, he / she shall endeavor to contact us promptly, but he / she shall not be obliged to contact us. In addition, we do not take any responsibility for the damage caused to the user by this. However, this does not apply in case of emergency.
        </li>
    </ol>
</div>
`