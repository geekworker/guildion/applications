export const jaHTML = `
<div>
    <h2 id="1"><strong>準拠法・裁判管轄</strong></h2>
    <ol>
        <li>
            本規約の解釈にあたっては、日本法を準拠法とします。
        </li>
        <li>
            本サービスに関して紛争が生じた場合には、東京地方裁判所を専属的合意管轄とします。
        </li>
    </ol>
</div>
`