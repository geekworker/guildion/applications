import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSGoverningLawAndJurisdictionDocument = new Document({
    slug: 'governing-law-and-jurisdiction',
    jaTitle: "第十七条 準拠法・裁判管轄",
    enTitle: "17. Governing Law and Jurisdiction",
    jaData: jaHTML,
    enData: enHTML,
    index: 17,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});