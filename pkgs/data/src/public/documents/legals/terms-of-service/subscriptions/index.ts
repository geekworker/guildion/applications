import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSSubscriptionsDocument = new Document({
    slug: 'subscriptions',
    jaTitle: "第八条 サブスクリプションサービスの提供",
    enTitle: "8. Provision of the Subscription Services",
    jaData: jaHTML,
    enData: enHTML,
    index: 8,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});