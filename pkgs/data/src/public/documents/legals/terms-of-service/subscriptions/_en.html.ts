export const enHTML = `
<div>
    <h2 id="1"><strong>Subscription Services</strong></h2>
    <ol>
        <li>Users may receive services in exchange for payment of a fee or under the terms and conditions prescribed by us, wherein the Users pay a certain amount of considerations to use certain Subject Contents designated by us for a certain period of time (the “Subscription Services”). Users shall abide by these Terms of Service, the fees for the Subscription Services, the payment method thereof, as well as other terms and conditions of use posted on the Services or on our website or application.</li>
        <li>
            Users may take procedures for the cancellation of Subscription Services at any time, however, even if a User takes the procedures for cancellation prior to the intended period of use, the User may not change such period of use, nor may the User cancel the purchase of the Subscription Services. In such case, the fees already paid will not be refunded, nor will there be any refund on a pro rata basis. However, the foregoing will not apply if laws or regulations require otherwise.
        </li>
        <li>
            If a User does not complete their cancellation procedure by the designated date and time, the period of use of the Subscription Services may be automatically renewed in accordance with the terms prescribed by us even after the end of the period of use of such Subscription Services.
        </li>
    </ol>
</div>
`