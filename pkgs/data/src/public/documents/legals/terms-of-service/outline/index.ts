import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const TOSOutlineDocument = new Document({
    slug: 'outline',
    jaTitle: "利用規約 - 目次 -",
    enTitle: "Terms of Service - Outline -",
    jaData: jaHTML,
    enData: enHTML,
    index: 0,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});