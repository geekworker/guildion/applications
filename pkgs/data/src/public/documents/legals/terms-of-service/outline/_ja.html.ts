export const jaHTML = `
<div>
    <h2 id="1">はじめに</h2>
    <p>
        この利用規約（以下、「本規約」といいます。）は、「Geek Worker」（以下「当方」といいます。）がこのウェブサイトやアプリ「Guildion」および「Guildion」を通じて提供されるサービス（以下,「本サービス」といいます。）やその他の製品の利用条件を定めるものです。本サービス利用者の皆さま（以下、「ユーザー」といいます。）には、本規約に同意いただくことで、本サービスを利用することができます。
    </p>
    <hr/>

    <h2 id="2">目次</h2>
    <ol>
        <li>
            <a href="/ja/documents/legal/terms-of-service/definition">定義</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/agreement">規約への同意</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/update">規約の更新・変更</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/account">アカウント管理</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/privacy">個人情報等の取り扱い</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/provision">サービスの提供</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/contents">コンテンツ</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/subscriptions">サブスクリプションサービスの提供</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/third-parties">第三者のサービス</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/advertisement">広告表示</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/restricted-matters">禁止行為</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/limitation-liability">当方の免責事項</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/user-responsibility">ユーザーの責任</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/no-warranty">非保証</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/contact">当方とユーザー間との連絡・通知</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/relationship-law">本規約と法令の関係</a>
        </li>
        <li>
            <a href="/ja/documents/legal/terms-of-service/governing-law-and-jurisdiction">準拠法・裁判管轄</a>
        </li>
    </ol>

    <h2 id="3">発行日</h2>
    <p>2021年3月14日</p>
</div>
`