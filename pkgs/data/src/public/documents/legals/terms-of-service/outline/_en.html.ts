export const enHTML = `
<div>
    <div>
        <h2>
            <strong>Causion</strong>
        </h2>
        <aside class="danger">
            <strong>Note:</strong>
            <span>
                This documents are not completely translated to English. Then Japanese words or unnatural English words will some times appear. If you discover part of you not understanding, we recommend you to check Japanese versions. We prioritize Japanese version if documents have conflicting content with other langedge versions.
            </span>
        </aside>
    </div>

    <div>
        <h2 id="1"><strong>Firstly</strong></h2>
        <p>
            These Guildion Terms of Service of Use (these “Terms of Service“) set forth the terms and conditions for the use of any and all products and services (collectively, the “Services”) provided by us (“we”, “us”, or “our”, depending upon the context) to users of the Services (the “User“ or “Users“, depending upon the context).
        </p>
        <hr/>

        <h2 id="2"><strong>Outline</strong></h2>

        <ol>
            <li>
                <a href="/en/documents/legal/terms-of-service/definition">Definition</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/agreement">Agreement to these Terms of Service</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/update">Modification to these Terms of Service</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/account">Account</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/privacy">Privacy</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/provision">Provision of the Service</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/contents">Contents</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/subscriptions">Provision of the Subscription Services</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/third-parties">Third-Party Services</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/advertisement">Advertisements</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/restricted-matters">Restricted Matters</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/limitation-liability">LIMITATION LIABILITY</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/user-responsibility">User Responsibility</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/no-warranty">NO WARRANTY</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/contact">Contact and Notification</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/relationship-law">Relationship between these Terms of Service and Laws and Regulations</a>
            </li>
            <li>
                <a href="/en/documents/legal/terms-of-service/governing-law-and-jurisdiction">Governing Law and Jurisdiction</a>
            </li>
        </ol>
    </div>

    <h2 id="3">Effective</h2>
    <p>March 14, 2021</p>
</div>
`