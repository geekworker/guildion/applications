import { DocumentGroup, Documents, DocumentSections } from "@guildion/core";
import { LegalsGroup } from "./legals";
import { TOSSection } from "./legals/terms-of-service";
import { TOSAccountDocument } from "./legals/terms-of-service/accounts";
import { TOSAdvertisementDocument } from "./legals/terms-of-service/advertisement";
import { TOSAgreementDocument } from "./legals/terms-of-service/agreement";
import { TOSContentsDocument } from "./legals/terms-of-service/contents";
import { TOSDefinitionDocument } from "./legals/terms-of-service/definition";
import { TOSGoverningLawAndJurisdictionDocument } from "./legals/terms-of-service/governing-law-and-jurisdiction";
import { TOSLimitationLiabilityDocument } from "./legals/terms-of-service/limitation-liability";
import { TOSNoWarrantyDocument } from "./legals/terms-of-service/no-warranty";
import { TOSOutlineDocument } from "./legals/terms-of-service/outline";
import { TOSPrivacyDocument } from "./legals/terms-of-service/privacy";
import { TOSProvisionDocument } from "./legals/terms-of-service/provision";
import { TOSRelationshipLawDocument } from "./legals/terms-of-service/relationship-law";
import { TOSRestrictedMattersDocument } from "./legals/terms-of-service/restricted-matters";
import { TOSSubscriptionsDocument } from "./legals/terms-of-service/subscriptions";
import { TOSThirdPartiesDocument } from "./legals/terms-of-service/third-parties";
import { TOSUpdateDocument } from "./legals/terms-of-service/update";
import { TOSUserResponsibilityDocument } from "./legals/terms-of-service/user-responsibility";
import { PrivacyPolicySection } from "./legals/privacy-policy";
import { PrivacyPolicyCookieDocument } from "./legals/privacy-policy/cookie";
import { PrivacyPolicyDefinitionDocument } from "./legals/privacy-policy/definition";
import { PrivacyPolicyForeignCountryDocument } from "./legals/privacy-policy/foreign-country";
import { PrivacyPolicyGoogleAnalyticsDocument } from "./legals/privacy-policy/google-analytics";
import { PrivacyPolicyModificationDocument } from "./legals/privacy-policy/modification";
import { PrivacyPolicyOutlineDocument } from "./legals/privacy-policy/outline";
import { PrivacyPolicyProvisionDocument } from "./legals/privacy-policy/provision";
import { PrivacyPolicyPurposeDocument } from "./legals/privacy-policy/purpose";
import { PrivacyPolicyThirdPartyLinkDocument } from "./legals/privacy-policy/third-party-link";
import { PrivacyPolicyToBeCollectedDocument } from "./legals/privacy-policy/to-be-collected";
import { PrivacyPolicyUpdateDocument } from "./legals/privacy-policy/update";
import { CommunityGuidelinesSection } from "./policies/community-guidelines";
import { CommunityGuidelinesOutlineDocument } from "./policies/community-guidelines/outline";
import { CommunityGuidelinesMembersRuleDocument } from "./policies/community-guidelines/members-rule";
import { CommunityGuidelinesPolicyDocument } from "./policies/community-guidelines/policy";
import { CommunityGuidelinesContentsRuleDocument } from "./policies/community-guidelines/contents-rule";
import { CommunityGuidelinesServiceRuleDocument } from "./policies/community-guidelines/service-rule";
import { PoliciesGruop } from "./policies";
import { SupportsGruop } from "./supports";
import { ContactsUsDocument } from "./supports/contacts/us";
import { ContactsSection } from "./supports/contacts";
import { TOSContactDocument } from "./legals/terms-of-service/contact";

export const buildLegals = (): DocumentGroup => {
    const sections = new DocumentSections([
        TOSSection.setRecord(
            'documents',
            new Documents([
                TOSOutlineDocument,
                TOSDefinitionDocument,
                TOSAgreementDocument,
                TOSUpdateDocument,
                TOSAccountDocument,
                TOSPrivacyDocument,
                TOSProvisionDocument,
                TOSContentsDocument,
                TOSSubscriptionsDocument,
                TOSThirdPartiesDocument,
                TOSAdvertisementDocument,
                TOSRestrictedMattersDocument,
                TOSLimitationLiabilityDocument,
                TOSUserResponsibilityDocument,
                TOSNoWarrantyDocument,
                TOSContactDocument,
                TOSRelationshipLawDocument,
                TOSGoverningLawAndJurisdictionDocument,
            ])
        ),
        PrivacyPolicySection.setRecord(
            'documents',
            new Documents([
                PrivacyPolicyOutlineDocument,
                PrivacyPolicyDefinitionDocument,
                PrivacyPolicyToBeCollectedDocument,
                PrivacyPolicyPurposeDocument,
                PrivacyPolicyCookieDocument,
                PrivacyPolicyGoogleAnalyticsDocument,
                PrivacyPolicyThirdPartyLinkDocument,
                PrivacyPolicyProvisionDocument,
                PrivacyPolicyModificationDocument,
                PrivacyPolicyForeignCountryDocument,
                PrivacyPolicyUpdateDocument,
            ])
        ),
    ]);
    const group = LegalsGroup.setRecord('sections', sections);
    return group;
}

export const buildPolicies = (): DocumentGroup => {
    const sections = new DocumentSections([
        CommunityGuidelinesSection.setRecord(
            'documents',
            new Documents([
                CommunityGuidelinesOutlineDocument,
                CommunityGuidelinesPolicyDocument,
                CommunityGuidelinesMembersRuleDocument,
                CommunityGuidelinesContentsRuleDocument,
                CommunityGuidelinesServiceRuleDocument,
            ])
        ),
    ]);
    const group = PoliciesGruop.setRecord('sections', sections);
    return group;
}

export const buildSupports = (): DocumentGroup => {
    const sections = new DocumentSections([
        ContactsSection.setRecord(
            'documents',
            new Documents([
                ContactsUsDocument,
            ])
        ),
    ]);
    const group = SupportsGruop.setRecord('sections', sections);
    return group;
}