import { DocumentGroup, DocumentGroupType } from '@guildion/core';

export const SupportsGruop = new DocumentGroup({
    slug: 'supports',
    jaName: 'サポートセンター',
    enName: 'Support Center',
    type: DocumentGroupType.DEFAULT,
});