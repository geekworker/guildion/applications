export const enHTML = `
<div>
    <h2 id="1">Contact us</h2>

    <aside class="danger">
        <strong>Note:</strong>
        <span>
            We are currently in a state where it cannot afford to operate. Therefore, the reply date and time is indefinite. However, we are also trying to respond as soon as possible.
        </span>
    </aside>

    <p>
        <a href="https://forms.gle/EMrfVTWwz8kLrkMT7" target="_blank" >Click here</a> to contact us.
    </p>
    <br/>

    <p>
        <strong>However, we have no responsibility to reply for contact not related our services.</strong>
    </p>
</div>
`