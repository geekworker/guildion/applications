import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const ContactsUsDocument = new Document({
    slug: 'us',
    jaTitle: "運営にお問い合わせ",
    enTitle: "Contact for us",
    jaData: jaHTML,
    enData: enHTML,
    index: 0,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});