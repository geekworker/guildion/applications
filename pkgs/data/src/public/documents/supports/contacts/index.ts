import { DocumentSection, DocumentSectionType } from '@guildion/core';

export const ContactsSection = new DocumentSection({
    slug: 'contacts',
    jaName: 'お問い合わせ',
    enName: 'Contact',
    index: 0,
    type: DocumentSectionType.DEFAULT,
});