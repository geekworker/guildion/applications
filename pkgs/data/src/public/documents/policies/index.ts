import { DocumentGroup, DocumentGroupType } from '@guildion/core';

export const PoliciesGruop = new DocumentGroup({
    slug: 'policies',
    jaName: '規定とポリシー',
    enName: 'Regulations and Policies',
    type: DocumentGroupType.DEFAULT,
});