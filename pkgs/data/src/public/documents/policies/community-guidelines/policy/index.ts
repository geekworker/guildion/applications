import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const CommunityGuidelinesPolicyDocument = new Document({
    slug: 'policy',
    jaTitle: "行動指針",
    enTitle: "Guildion Community Policy",
    jaData: jaHTML,
    enData: enHTML,
    index: 1,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});