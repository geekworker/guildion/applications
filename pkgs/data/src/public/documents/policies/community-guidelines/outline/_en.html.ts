export const enHTML = `
<div>
    <h2 id="1">Firstly</h2>
    <p>
        Guildion's Community Guidelines are designed to keep your community safe. These guidelines stipulate that Guildion is permitted or prohibited, and applies to all types of Guild content. All community guidelines can be found below.
    </p>
    <hr/>

    <h2 id="2">Outline</h2>

    <ol>
        <li>
            <a href="/en/documents/policies/community-guideline/policy">Guildion Community Policy</a>
        </li>
        <li>
            <a href="/en/documents/policies/community-guideline/members-rule">Members Communication Rule</a>
        </li>
        <li>
            <a href="/en/documents/policies/community-guideline/contents-rule">Guildion Contents Rule</a>
        </li>
        <li>
            <a href="/en/documents/policies/community-guideline/service-rule">Guildion Service Rule</a>
        </li>
    </ol>
</div>
`