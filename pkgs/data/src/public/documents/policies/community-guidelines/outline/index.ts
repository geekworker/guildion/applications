import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const CommunityGuidelinesOutlineDocument = new Document({
    slug: 'outline',
    jaTitle: "概要",
    enTitle: "Outline",
    jaData: jaHTML,
    enData: enHTML,
    index: 0,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});