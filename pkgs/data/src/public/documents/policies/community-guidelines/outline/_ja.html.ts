export const jaHTML = `
<div>
    <h2 id="1">はじめに</h2>
    <p>
        Guildionのコミュニティーガイドラインはコミュニティーを安全に保つために作成されています。このガイドラインではGuildionで許可されること禁止されることが定められており、ギルドのあらゆる種類のコンテンツに適用されます。
        すべてのコミュニティーガイドラインは、以下でご覧いただけます
    </p>
    <hr/>

    <h2 id="2">目次</h2>

    <ol>
        <li>
            <a href="/ja/documents/policies/community-guideline/policy">行動指針</a>
        </li>
        <li>
            <a href="/ja/documents/policies/community-guideline/members-rule">メンバー同士の交流のルール</a>
        </li>
        <li>
            <a href="/ja/documents/policies/community-guideline/contents-rule">コンテンツに関するルール</a>
        </li>
        <li>
            <a href="/ja/documents/policies/community-guideline/service-rule">Guildionのサービス上でのルール</a>
        </li>
    </ol>
</div>
`