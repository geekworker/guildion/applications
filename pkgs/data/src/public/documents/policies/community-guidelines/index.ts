import { DocumentSection, DocumentSectionType } from '@guildion/core';

export const CommunityGuidelinesSection = new DocumentSection({
    slug: 'community-guidelines',
    jaName: 'コミュニティーガイドライン',
    enName: 'Community Guideline',
    index: 0,
    type: DocumentSectionType.DEFAULT,
});