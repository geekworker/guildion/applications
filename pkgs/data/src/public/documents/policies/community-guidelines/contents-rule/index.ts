import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const CommunityGuidelinesContentsRuleDocument = new Document({
    slug: 'contents-rule',
    jaTitle: "コンテンツに関するルール",
    enTitle: "Guildion Contents Rule",
    jaData: jaHTML,
    enData: enHTML,
    index: 3,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});