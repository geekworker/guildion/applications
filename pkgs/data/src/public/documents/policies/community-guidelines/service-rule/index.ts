import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const CommunityGuidelinesServiceRuleDocument = new Document({
    slug: 'service-rule',
    jaTitle: "Guildionのサービス上でのルール",
    enTitle: "Guildion Service Rule",
    jaData: jaHTML,
    enData: enHTML,
    index: 4,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});