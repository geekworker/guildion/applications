export const enHTML = `
<div>
    <h2 id="1"><strong>Selling accounts, guilds and other accompanying information is prohibited</strong></h2>
    <p>
        Information in Guildion and services is not for sale.
    </p>
    <br/>
    <h2 id="2"><strong>Prohibition of spamming</strong></h2>
    <p>
        Making malicious false reports, making multiple reports of the same issue, or asking a group of users to report the same content for everyone can lead to action on your account.
    </p>
    <br/>
</div>
`