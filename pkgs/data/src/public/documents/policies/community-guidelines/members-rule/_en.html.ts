export const enHTML = `
<div>
    <h2 id="1"><strong>Respect Each Other</strong></h2>
    <p>
        We think it is important to respect each other for building good relationship with members.
    </p>
    <p>
       All members are human. Each has various personalities, traits, and ideas.
    </p>
    <p>
        Guildion is an internet community service, so it is not possible to grasp all humanity.
    </p>
    <p>
        So if anyone thinks it doesn't suit you, don't hurt them. Because there are many things that happen to make you feel bad, and you don't have enough information to decide who you are.
    </p>
    <p>
        Let's build a warming community, keeping in mind that the good points and bad points coexist with each other.
    </p>

    <br/>

    <h2 id="2"><strong>Share the fun</strong></h2>
    <p>
        By creating a guild, Guildion allows you to share various enjoyments of the Internet with everyone in real time.
    </p>
    <p>
        We specialize in sharing videos, but you can also share content other than videos.
    </p>
    <p>
        The common sense that the Internet is used alone is firmly rooted, and Guildion believes that it can be rewritten.
    </p>
    <p>
        Let's enjoy the internet in real time with everyone at Guildion.
    </p>

    <br/>

    <h2 id="3"><strong>Don't talk about hurting someone</strong></h2>
    <p>
        Many members may belong to the guild.
    </p>
    <p>
        If you put someone's secret on that public channel, you'll get a bad impression of someone that all the members haven't seen directly.
    </p>
    <p>
        Guildion is not a service to hurt anyone.
    </p>
    <p>
        Never make a statement that would hurt someone.
    </p>

    <br/>

    <h2 id="4"><strong>Be careful for Personal Data</strong></h2>
    <p>
        Even if you make good friends in the guild, don't be afraid to manage your personal information.
    </p>
    <p>
        There is a saying in Japanese that ”there is courtesy to close friends.”
    </p>
    <p>
        Since the management of personal information is at your own risk, it is not a good idea to share it.
    </p>
    <p>
        Also, do not post anything that would disperse someone's personal information.
    </p>
    <p>
        Guildion respects personal information. That's why Guildion is not a service that undermines someone's privacy.
    </p>

    <br/>

    <h2 id="5"><strong>Stop harassment</strong></h2>
    <p>
        Guildion also has the ability to block, mute, kick, ban and keep distance from others and content.
    </p>
    <p>
        Each member has various sensibilities.
    </p>
    <p>
        Respect the sensibilities of the members, and if you receive an action that will keep you away, acknowledge the situation and keep a distance.
    </p>
    <p>
        There should be other members that suit you.
    </p>
    <p>
        In addition, content, accounts, etc. that fall under harassment or harassment are subject to deletion or restriction.
    </p>
    <br/>
</div>
`