import { DocumentType, DocumentDataType, Document } from '@guildion/core';
import { jaHTML } from './_ja.html';
import { enHTML } from './_en.html';

export const CommunityGuidelinesMembersRuleDocument = new Document({
    slug: 'members-rule',
    jaTitle: "メンバー同士の交流のルール",
    enTitle: "Members Communication Rule",
    jaData: jaHTML,
    enData: enHTML,
    index: 2,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.HTML,
});