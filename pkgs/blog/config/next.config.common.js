/* eslint-disable
    @typescript-eslint/no-var-requires,
    @typescript-eslint/explicit-function-return-type
*/

const path = require('path');
const { withPlugins, extend } = require('next-compose-plugins');

const withBundleAnalyzer = require("@next/bundle-analyzer")({
    enabled: process.env.ANALYZE === "true",
});
const withTM = require('next-transpile-modules')([
    "@guildion/core",
    "@guildion/next",
    "@guildion/ui",
]);

const baseConfig = (NODE_ENV) => {
    return {
        reactStrictMode: true,
        resolveSymlinks: false,
        webpack(config, options) {
            config.resolve.alias = {
                ...config.resolve.alias,
                '@': path.join(__dirname, 'src'),
            };
            return config;
        },
        env: {
            ENVIRONMENT: NODE_ENV,
            GOOGLE_ANALYTICS_ID: 'G-HP8RERDBM7',
        },
        serverRuntimeConfig: {
            SSR: true,
        },
        publicRuntimeConfig: {
            RUNTIME: true,
        },
        i18n: {
            locales: ["ja"],
            defaultLocale: "ja",
        },
        images: {
            domains: ['cdn.guildion.co', 'pcdn.guildion.co'],
        },
        swcMinify: true,
        optimizeFonts: true,
        async headers() {
            return [
                {
                    source: "/fonts/Montserrat-Medium.woff2",
                    headers: [
                        {
                            key: "Cache-control",
                            value: "public, immutable, max-age=31536000",
                        },
                    ],
                },
                {
                    source: "/fonts/Montserrat-Bold.woff2",
                    headers: [
                        {
                            key: "Cache-control",
                            value: "public, immutable, max-age=31536000",
                        },
                    ],
                },
                {
                    source: "/fonts/ZenKakuGothicAntique-Regular.woff2",
                    headers: [
                        {
                            key: "Cache-control",
                            value: "public, immutable, max-age=31536000",
                        },
                    ],
                },
                {
                    source: "/fonts/ZenKakuGothicAntique-Bold.woff2",
                    headers: [
                        {
                            key: "Cache-control",
                            value: "public, immutable, max-age=31536000",
                        },
                    ],
                },
                {
                    source: '/:all*(svg|jpg|png)',
                    locale: false,
                    headers: [
                        {
                            key: 'Cache-Control',
                            value: 'public, max-age=9999999999, must-revalidate',
                        }
                    ],
                },
            ];
        },
        async redirects() {
            const redirects = [];
            if (NODE_ENV === 'production') redirects.push(
                {
                    source: "/blogs/:id/draft",
                    destination: "/blogs/:id",
                    permanent: true,
                },
            );
            return redirects;
        },
        async rewrites() {
            return [
                {
                    source: '/sitemap.xml',
                    destination: '/api/sitemap.xml',
                },
            ]
        },
    }
}

module.exports = (NODE_ENV) =>
    process.env.ANALYZE === "true" ?
    withBundleAnalyzer(withTM(moduleExports(NODE_ENV))) :
    withTM(moduleExports);

module.exports = (NODE_ENV) => module.exports = withPlugins(
    process.env.ANALYZE === "true" ? [
        withBundleAnalyzer,
        withTM
    ] : [
        withTM
    ],
    baseConfig(NODE_ENV)
);