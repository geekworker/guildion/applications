/* eslint-disable
    @typescript-eslint/no-var-requires,
    @typescript-eslint/explicit-function-return-type
*/

const commonModules = require('./config/next.config.common.js')

module.exports = commonModules('development');