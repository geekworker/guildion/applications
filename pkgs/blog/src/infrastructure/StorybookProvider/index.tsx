import '@guildion/next/styles/global.scss';
import '@/presentation/styles/globals.scss';
import { Provider } from 'react-redux';
import React from 'react';
import { makeStoreWithoutContext } from '@/presentation/redux/MakeStore';

interface Props {
    children?: React.ReactNode,
}

const StorybookProvider = ({ children }: Props) => {
    const store = React.useMemo(() => makeStoreWithoutContext(), []);
    return (
        <Provider store={store}>
            <div className={`theme-dark`}>
                {children}
            </div>
        </Provider>
    );
}

export default StorybookProvider;