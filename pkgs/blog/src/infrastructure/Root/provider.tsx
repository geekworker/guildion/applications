import React from 'react';
import RootRedux from './redux';

interface Props {
    children?: React.ReactNode,
}

const RootProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        RootRedux.reducer,
        RootRedux.initialState,
    );
    return (
        <RootRedux.Context.Provider value={{ state, dispatch }} >
            {children}
        </RootRedux.Context.Provider>
    );
};

export default RootProvider;
