import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { AppTheme, appThemeToString, fallbackAppTheme } from "@guildion/ui";
import { Action as ReduxAction } from "redux";
import GlobalLocalStorage from "@/shared/modules/GlobalLocalStorage";

namespace RootRedux {
    export class State extends Record<{
        appTheme: AppTheme,
    }>({
        appTheme: fallbackAppTheme,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        setAppTheme: actionCreator<AppTheme>('SET_APP_THEME'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.setAppTheme, (state, payload) => {
            state = state.set('appTheme', payload);
            GlobalLocalStorage.instance().save({ appTheme: appThemeToString(payload) })
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: RootRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });

    export type Context = {
        state: RootRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    };
}

export default RootRedux