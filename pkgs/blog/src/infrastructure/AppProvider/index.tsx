import React, { ReactNode } from 'react';
import RootProvider from '../Root/provider';
import configureCore from '@/infrastructure/Core';
import configureENV from '@/infrastructure/ENV';
import HeaderProvider from '@/presentation/components/molecules/Header/provider';
import FooterProvider from '@/presentation/components/molecules/Footer/provider';

configureENV();
configureCore();

interface Props {
    children?: ReactNode,
}

const AppProvider: React.FC<Props> = ({ children }: Props) => {
    return (
        <RootProvider>
            <HeaderProvider>
                <FooterProvider>
                    {children}
                </FooterProvider>
            </HeaderProvider>
        </RootProvider>
    );
}

export default AppProvider;