import { getAppConfig } from '@/shared/constants/AppConfig';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import { configure } from '@guildion/core';

export default function configureCore() {
    configure({ CURRENT_NODE_ENV: CURRENT_ENV.NODE_ENV, IS_MOBILE: false, GUILDION_API_SERVICE_HOST: getAppConfig().GUILDION_API_SERVICE_HOST });
}