import { AppAction } from "@/presentation/redux/App/AppReducer";
import { AppError, Endpoint, EndpointOfResponse, EndpointAPIProps, isAppError, isNetworkError, NetworkError, requestAPISafe } from "@guildion/core";
import { call, put } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";
import { List } from "immutable";
import { retryDecorator } from "ts-retry-promise";

export default class BaseRepository {
    constructor() {}

    static decorateAPI<E extends Endpoint<any, any, any>>(options?: {}) {
        const decorated = retryDecorator(requestAPISafe, {
            until: (t: EndpointOfResponse<E> | AppError | NetworkError) => {
                if (isNetworkError(t)) {
                    return false;
                } else if (isAppError(t)) {
                    return true;
                }
                return true
            },
            delay: 1000,
        });
        return decorated;
    }

    static *callAPI<E extends Endpoint<any, any, any>>(endpoint: E, args: EndpointAPIProps<E>, options?: { ignoreError?: boolean }): SagaIterator<EndpointOfResponse<E>> {
        const decorated = BaseRepository.decorateAPI<E>({ ...options });
        const result: EndpointOfResponse<E> | AppError | NetworkError = yield call(decorated, endpoint, args);
        if (isAppError(result) && !options?.ignoreError) yield put(AppAction.displayErrors({ errors: List([result]) }))
        if (isNetworkError(result) || isAppError(result)) throw result;
        return result;
    }
}