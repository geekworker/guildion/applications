import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { Blog, BlogDraftRequest, BlogDraftResponse, BlogsEndpoint, BlogShowRequest, BlogShowResponse, BlogsListRequest, BlogsListResponse, SortDirection, SortType } from '@guildion/core';
import { call } from 'redux-saga/effects';

export default class BlogsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *get({ slug }: { slug: string }): SagaIterator<Blog> {
        const result: BlogShowResponse = yield call(BaseRepository.callAPI, 
            BlogsEndpoint.Show,
            {
                data: new BlogShowRequest({ 
                    slug,
                }),
            }
        );
        return result.getBlog();
    }

    *draft({ slug }: { slug: string }): SagaIterator<Blog> {
        const result: BlogDraftResponse = yield call(BaseRepository.callAPI, 
            BlogsEndpoint.Draft,
            {
                data: new BlogDraftRequest({ 
                    slug,
                }),
            }
        );
        return result.getBlog();
    }

    *gets({
        categorySlug,
        slug,
        count,
        offset,
        sort,
    }: {
        categorySlug?: string,
        slug?: string,
        count?: number,
        offset?: number,
        sort?: {
            direction?: SortDirection,
            type?: SortType,
        },
    }): SagaIterator<BlogsListResponse> {
        const result: BlogsListResponse = yield call(BaseRepository.callAPI, 
            BlogsEndpoint.List,
            {
                data: new BlogsListRequest({ 
                    categorySlug,
                    slug,
                    count,
                    offset,
                    sort,
                }),
            }
        );
        return result;
    }
}