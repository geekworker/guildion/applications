import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import BlogsRepository from '../repositories/BlogsRepository';
import { SagaIterator } from 'redux-saga';
import { call, select } from 'redux-saga/effects';
import { Blog, BlogCategory, Blogs, BlogsListResponse } from '@guildion/core';
import { blogIndexPageSelector } from '../../presentation/redux/Blog/BlogSelector';
import { BlogsPageState } from '../../presentation/redux/Blog/BlogReducer';

export default class BlogsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ categorySlug }: { categorySlug?: string }): SagaIterator<{ blogs: Blogs, category?: BlogCategory, paginatable: boolean }> {
        const result: BlogsListResponse = yield call(new BlogsRepository().gets, { count: 10, offset: 0, categorySlug });
        return {
            blogs: result.getBlogs(),
            paginatable: result.paginatable,
            category: result.getCategory(),
        };
    }

    *fetchMoreIndex({ categorySlug }: { categorySlug?: string }): SagaIterator<{ blogs: Blogs, category?: BlogCategory, paginatable: boolean }> {
        const pageState: BlogsPageState | undefined = yield select(state => blogIndexPageSelector(state, { categorySlug }));
        const last = pageState?.get('blogs').last;
        const blogs = pageState?.get('blogs');
        if (!pageState || !blogs || !last || !pageState.get('paginatable')) return { blogs: new Blogs([]), paginatable: false };
        const result: BlogsListResponse = yield call(new BlogsRepository().gets, { count: 10, offset: blogs.size, categorySlug });
        return {
            blogs: result.getBlogs(),
            paginatable: result.paginatable,
            category: result.getCategory(),
        };
    }

    *fetchShow({ slug }: { slug: string }): SagaIterator<{ blog: Blog }> {
        const blog: Blog = yield call(new BlogsRepository().get, { slug });
        return {
            blog,
        };
    }

    *fetchDraft({ slug }: { slug: string }): SagaIterator<{ blog: Blog }> {
        const blog: Blog = yield call(new BlogsRepository().draft, { slug });
        return {
            blog,
        };
    }
}