export type HTMLAnchor = {
    id: string,
    displayName: string,
    children: HTMLAnchor[],
};

export const generateAnchors = (html: string): HTMLAnchor[] => {
    if (!process.browser) return [];
    const results: HTMLAnchor[] = [];
    const doc = new DOMParser().parseFromString(html, "text/html")
    const h1Elements = doc.getElementsByTagName('h1');
    const h2Elements = doc.getElementsByTagName('h2');
    for (var i = 0; i < h1Elements.length; i++) {
        const element1 = h1Elements[i];
        if (!!element1.id) {
            const anchor = {
                displayName: element1.textContent,
                id: `#${element1.id}`,
                children: [],
            } as HTMLAnchor;
            for (var i = 0; i < h2Elements.length; i++) {
                const element2 = h2Elements[i];
                if (!!element2.id && !!element2.id.startsWith(`${element1.id}-`)) {
                    const childAnchor = {
                        displayName: element2.textContent,
                        id: `#${element2.id}`,
                        children: [],
                    } as HTMLAnchor;
                    anchor.children.push(childAnchor);
                }
            }
        }
    }

    for (var i = 0; i < h2Elements.length; i++) {
        const element2 = h2Elements[i];
        if (!!element2.id && !results.find(r => r.id === element2.id)) {
            const childAnchor = {
                displayName: element2.textContent,
                id: `#${element2.id}`,
                children: [],
            } as HTMLAnchor;
            results.push(childAnchor);
        }
    }

    return results;
}