import React from 'react';
import useInterval from 'use-interval';
import { useScroll } from './useScroll';
import { useWindowSize } from './useWindowSize';

export const usePositionElement = <E extends HTMLElement>(condition?: () => boolean) => {
    const ref = React.useRef<E>(null);
    const window = useWindowSize();
    const scrollInfo = useScroll();
    const [scrollTop, setScrollTop] = React.useState(0);
    const [scrollHeight, setScrollHeight] = React.useState(0);
    const [offsetHeight, setOffsetHeight] = React.useState(0);
    const [offsetLeft, setOffsetLeft] = React.useState(0);
    const [offsetTop, setOffsetTop] = React.useState(0);
    const [offsetWidth, setOffsetWidth] = React.useState(0);
    const [clientHeight, setClientHeight] = React.useState(0);
    const [width, setWidth] = React.useState(0);
    const [height, setHeight] = React.useState(0);
    const [left, setLeft] = React.useState(0);
    const [top, setTop] = React.useState(0);
    const [bottom, setBottom] = React.useState(0);
    const [right, setRight] = React.useState(0);
    const [x, setX] = React.useState(0);
    const [y, setY] = React.useState(0);

    const [isIntersecting, setIntersecting] = React.useState(false);

    const observer = React.useMemo(() => {
        if (!process.browser || typeof window === 'undefined') return undefined;
        return new IntersectionObserver(
            ([entry]) => setIntersecting(entry.isIntersecting)
        );
    }, [process.browser])

    React.useEffect(() => {
        if (!ref.current) return;
        observer?.observe(ref.current);
        return () => { observer?.disconnect() }
    }, [ref.current, observer]);

    useInterval(() => {
        if (!!ref.current && condition && !!condition()) {
            const { width, height, left, top, bottom, right, x, y } = ref.current.getBoundingClientRect();
            setScrollTop(ref.current.scrollHeight);
            setScrollHeight(ref.current.scrollHeight);
            setOffsetHeight(ref.current.offsetHeight);
            setOffsetLeft(ref.current.offsetLeft);
            setOffsetTop(ref.current.offsetTop);
            setOffsetWidth(ref.current.offsetWidth);
            setOffsetWidth(ref.current.clientHeight);
            setClientHeight(ref.current.clientHeight);
            setWidth(width);
            setHeight(height);
            setLeft(left);
            setTop(top);
            setBottom(bottom);
            setRight(right);
            setX(x);
            setY(y);
        }
    }, window.width < 440 ? 100 : 0);

    React.useEffect(() => {
        if (!ref.current || scrollInfo.scrollTopOver < 0 || !isIntersecting || (condition && !condition())) return;
        // if (window.width > 0 && window.height > 0 && window.width < 440 && window.widthDiff < 200 && window.heightDiff < 200) return;
        const { width, height, left, top, bottom, right, x, y } = ref.current.getBoundingClientRect();
        setScrollTop(ref.current.scrollHeight);
        setScrollHeight(ref.current.scrollHeight);
        setOffsetHeight(ref.current.offsetHeight);
        setOffsetLeft(ref.current.offsetLeft);
        setOffsetTop(ref.current.offsetTop);
        setOffsetWidth(ref.current.offsetWidth);
        setOffsetWidth(ref.current.clientHeight);
        setClientHeight(ref.current.clientHeight);
        setWidth(width);
        setHeight(height);
        setLeft(left);
        setTop(top);
        setBottom(bottom);
        setRight(right);
        setX(x);
        setY(y);
    }, [ref.current, window, isIntersecting]);
    return {
        scrollTop,
        scrollHeight,
        offsetHeight,
        offsetLeft,
        offsetTop,
        offsetWidth,
        clientHeight,
        ref,
        width,
        height,
        left,
        top,
        bottom,
        right,
        x,
        y,
    }
};