import React from 'react';

export const useScroll = () => {
    const [scrolling, setScrolling] = React.useState(false);
    const [scrollTop, setScrollTop] = React.useState(0);
    const [scrollTopOver, setScrollTopOver] = React.useState(0);
    const [scrollHeight, setScrollHeight] = React.useState(0);
    React.useEffect(() => {
        const onScroll = (e: any) => {
            setScrollTopOver(e.target.documentElement.scrollTop);
            setScrollTop(Math.max(0, e.target.documentElement.scrollTop));
            setScrollHeight(e.target.documentElement.scrollHeight);
            setScrolling(Math.max(e.target.documentElement.scrollTop, 0) > scrollTop);
        };
        window.addEventListener("scroll", onScroll);
        return () => window.removeEventListener("scroll", onScroll);
    }, [scrollTop]);
    return {
        scrolling,
        scrollTop,
        scrollHeight,
        scrollTopOver,
    }
};