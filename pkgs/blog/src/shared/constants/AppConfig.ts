import getConfig from 'next/config';

export interface AppConfig {
    SSR: boolean,
    CSR: boolean,
    BUILD: boolean,
    GUILDION_API_SERVICE_HOST?: string,
};

export const getAppConfig = (): AppConfig => {
    const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
    const CSR = !serverRuntimeConfig.SSR && !!publicRuntimeConfig.RUNTIME;
    const NONE_BUILD: boolean = !!process.env.NEXT_START || CSR;
    return {
        SSR: !!serverRuntimeConfig.SSR,
        CSR,
        BUILD: !NONE_BUILD,
        GUILDION_API_SERVICE_HOST: process.env.GUILDION_API_SERVICE_HOST,
    }
}