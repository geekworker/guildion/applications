import { NODE_ENV, GET_NODE_ENV } from "@guildion/core";

export type ENV = {
    NODE_ENV: NODE_ENV,
    GOOGLE_ANALYTICS_ID?: string,
}

export let CURRENT_ENV: ENV;

export const UPDATE_CURRENT_ENV = () => {
    CURRENT_ENV = {
        NODE_ENV: GET_NODE_ENV(process.env.ENVIRONMENT!),
        GOOGLE_ANALYTICS_ID: process.env.GOOGLE_ANALYTICS_ID,
    };
};