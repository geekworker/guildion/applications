import { all, fork } from 'redux-saga/effects';
import { appSaga } from '@/presentation/redux/App';
import { blogSaga } from '@/presentation/redux/Blog';

export const rootSaga = function* root() {
    yield all([
        fork(appSaga),
        fork(blogSaga),
    ]);
};
