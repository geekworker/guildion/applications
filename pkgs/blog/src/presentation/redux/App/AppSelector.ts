import { createSelector } from "reselect";
import { AppError, ErrorType } from '@guildion/core';
import { List } from 'immutable';
import { RootState } from '../RootReducer';

export const errorsSelector = (state: RootState): List<AppError> => state.app.errors;
export const globalErrorsSelector = createSelector(
    errorsSelector,
    (errors) => errors.filter(e => e.type == ErrorType.global)
);
export const globalErrorSelector = createSelector(
    globalErrorsSelector,
    (errors) => errors.map(e => e.message).join("\n")
);
export const accountUsernameErrorsSelector = createSelector(
    errorsSelector,
    (errors) => errors.filter(e => e.type == ErrorType.accountUsername)
);
export const accountUsernameErrorSelector = createSelector(
    accountUsernameErrorsSelector,
    (errors) => errors.map(e => e.message).join("\n")
);