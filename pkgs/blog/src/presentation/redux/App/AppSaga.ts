import AppUseCase from '@/domain/usecases/AppUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { AppAction } from './AppReducer';

export default function* AppSaga() {
}