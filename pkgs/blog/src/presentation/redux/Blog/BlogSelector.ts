import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';
import { BlogPageStates, BlogsPageStates } from './BlogReducer';

export const blogIndexPageStatesSelector = (state: RootState) => state.blog.pages.index;
export const blogIndexPageSelector = createSelector(
    blogIndexPageStatesSelector,
    (_: RootState, { categorySlug }: { categorySlug?: string }) => { return { categorySlug } },
    (index: BlogsPageStates, { categorySlug }: { categorySlug?: string }) => index.find(p => p.get('categorySlug') == categorySlug)
);

export const blogShowPageStatesSelector = (state: RootState) => state.blog.pages.show;
export const blogShowPageSelector = createSelector(
    blogShowPageStatesSelector,
    (_: RootState, { slug }: { slug: string }) => { return { slug } },
    (show: BlogPageStates, { slug }: { slug: string }) => show.find(p => p.get('blog').slug == slug)
);

export const blogDraftPageStatesSelector = (state: RootState) => state.blog.pages.draft;
export const blogDraftPageSelector = createSelector(
    blogDraftPageStatesSelector,
    (_: RootState, { slug }: { slug: string }) => { return { slug } },
    (draft: BlogPageStates, { slug }: { slug: string }) => draft.find(p => p.get('blog').slug == slug)
);