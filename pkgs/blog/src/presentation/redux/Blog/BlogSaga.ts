import BlogsUseCase from '@/domain/usecases/BlogsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { BlogAction } from './BlogReducer';

export default function* BlogSaga() {
    yield takeEvery(BlogAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(BlogAction.fetchShow, { skipStartedAction: true })(new BlogsUseCase().fetchShow), payload)
    });
    yield takeEvery(BlogAction.fetchDraft.started, function*({ payload }) {
        yield call(bindAsyncAction(BlogAction.fetchDraft, { skipStartedAction: true })(new BlogsUseCase().fetchDraft), payload)
    });
    yield takeEvery(BlogAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(BlogAction.fetchIndex, { skipStartedAction: true })(new BlogsUseCase().fetchIndex), payload)
    });
    yield takeEvery(BlogAction.fetchMoreIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(BlogAction.fetchMoreIndex, { skipStartedAction: true })(new BlogsUseCase().fetchMoreIndex), payload)
    });
}