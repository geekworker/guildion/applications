import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { ImmutableMap, safeJSONParse, Blog, Blogs, BlogCategory } from '@guildion/core';
import { GlobalAction } from '../GlobalAction';

const actionCreator = actionCreatorFactory('Blog');

export type BlogsPageStateAttributes = { categorySlug?: string, category?: BlogCategory, blogs: Blogs, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMore?: boolean };
export type BlogsPageState = ImmutableMap<BlogsPageStateAttributes>;
export type BlogsPageStates = List<BlogsPageState>;
export type BlogPageStateAttributes = { blog: Blog, loading?: boolean, success?: boolean };
export type BlogPageState = ImmutableMap<BlogPageStateAttributes>;
export type BlogPageStates = List<BlogPageState>;

export const BlogAction = {
    fetchIndex: actionCreator.async<{ categorySlug?: string }, { category?: BlogCategory, blogs: Blogs, paginatable: boolean }>('BLOG_FETCH_INDEX'),
    fetchMoreIndex: actionCreator.async<{ categorySlug?: string }, { category?: BlogCategory, blogs: Blogs, paginatable: boolean }>('BLOG_FETCH_MORE_INDEX'),
    fetchShow: actionCreator.async<{ slug: string }, { blog: Blog }>('BLOG_FETCH_SHOW'),
    setShow: actionCreator<{ blog: Blog, slug: string }>('BLOG_SET_SHOW'),
    fetchDraft: actionCreator.async<{ slug: string }, { blog: Blog }>('BLOG_FETCH_DRAFT'),
    setDraft: actionCreator<{ blog: Blog, slug: string }>('BLOG_SET_DRAFT'),
};

export type BlogAction = typeof BlogAction[keyof typeof BlogAction];

export class BlogState extends Record<{
    pages: {
        index: BlogsPageStates,
        show: BlogPageStates,
        draft: BlogPageStates,
    },
}>({
    pages: {
        index: List([]),
        show: List([]),
        draft: List([]),
    },
}) {
    static deserializeState(state: any) {
        const parsed = safeJSONParse<ReturnType<BlogState['toJSON']>>(state);
        return new BlogState({
            ...parsed,
            pages: {
                index: parsed?.pages.index ? List(
                    parsed.pages.index.map(
                        (s: any) => Map({ ...s, blogs: new Blogs(s.blogs), category: s.category ? new BlogCategory(s.category) : undefined })
                    )
                ) : List([]),
                show: parsed?.pages.show ? List(
                    parsed.pages.show.map(
                        (s: any) => Map({ ...s, blog: new Blog(s.blog) })
                    )
                ) : List([]),
                draft: parsed?.pages.draft ? List(
                    parsed.pages.draft.map(
                        (s: any) => Map({ ...s, blog: new Blog(s.blog) })
                    )
                ) : List([]),
            },
        });
    }
    public serializeState(): string {
        return JSON.stringify({
            ...this.toJSON(),
            pages: {
                index: this.pages.index.toArray().map(s => s.toJSON()).map(s => { return { ...s, blogs: s.blogs.toJSON(), category: s.category?.toJSON() } }),
                show: this.pages.show.toArray().map(s => s.toJSON()).map(s => { return { ...s, blog: s.blog.toJSON() } }),
                draft: this.pages.draft.toArray().map(s => s.toJSON()).map(s => { return { ...s, blog: s.blog.toJSON() } }),
            },
        });
    }
}

const BlogReducer = reducerWithInitialState(new BlogState())
    .case(GlobalAction.HYDRATE, (state, payload) => {
        return payload.blog;
    })
    .case(BlogAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('categorySlug') == payload.categorySlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    categorySlug: payload.categorySlug,
                    blogs: new Blogs([]),
                    category: payload.categorySlug ? new BlogCategory({ slug: payload.categorySlug }) : undefined,
                    loading: true,
                    loadingMore: false,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('loadingMore', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(BlogAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('categorySlug') == payload.params.categorySlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    categorySlug: payload.params.categorySlug,
                    blogs: payload.result.blogs,
                    category: payload.params.categorySlug ? new BlogCategory({ slug: payload.params.categorySlug }) : undefined,
                    loading: false,
                    loadingMore: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('blogs', payload.result.blogs);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('category', payload.params.categorySlug ? new BlogCategory({ slug: payload.params.categorySlug }) : undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMore', false);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(BlogAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('categorySlug') == payload.params.categorySlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    categorySlug: payload.params.categorySlug,
                    blogs: new Blogs([]),
                    category: payload.params.categorySlug ? new BlogCategory({ slug: payload.params.categorySlug }) : undefined,
                    loading: false,
                    loadingMore: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('category', new BlogCategory());
            targetPage = targetPage.set('blogs', new Blogs([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('loadingMore', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(BlogAction.fetchMoreIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('categorySlug') == payload.categorySlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    categorySlug: payload.categorySlug,
                    blogs: new Blogs([]),
                    category: payload.categorySlug ? new BlogCategory({ slug: payload.categorySlug }) : undefined,
                    loading: true,
                    loadingMore: true,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('loadingMore', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(BlogAction.fetchMoreIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('categorySlug') == payload.params.categorySlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    categorySlug: payload.params.categorySlug,
                    blogs: payload.result.blogs,
                    category: payload.params.categorySlug ? new BlogCategory({ slug: payload.params.categorySlug }) : undefined,
                    loading: false,
                    loadingMore: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                    basedAt: new Date(),
                })
            );
        } else {
            targetPage = targetPage.set('blogs', targetPage.get('blogs').concat(payload.result.blogs));
            targetPage = targetPage.set('category', payload.params.categorySlug ? new BlogCategory({ slug: payload.params.categorySlug }) : undefined);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMore', false);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(BlogAction.fetchMoreIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('categorySlug') == payload.params.categorySlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    categorySlug: payload.params.categorySlug,
                    blogs: new Blogs([]),
                    category: payload.params.categorySlug ? new BlogCategory({ slug: payload.params.categorySlug }) : undefined,
                    loading: false,
                    loadingMore: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('loadingMore', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(BlogAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: new Blog({ slug: payload.slug }), loading: true, success: false }));
        } else {
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(BlogAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.params.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: payload.result.blog, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('blog', payload.result.blog);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(BlogAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.params.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: new Blog({ slug: payload.params.slug }), loading: false, success: false }));
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(BlogAction.setShow, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.blog.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: payload.blog, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('blog', payload.blog);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(BlogAction.fetchDraft.started, (state, payload) => {
        let pageState = state.pages.draft;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: new Blog({ slug: payload.slug }), loading: true, success: false }));
        } else {
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'draft'], pageState);
        return state;
    })
    .case(BlogAction.fetchDraft.done, (state, payload) => {
        let pageState = state.pages.draft;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.params.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: payload.result.blog, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('blog', payload.result.blog);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'draft'], pageState);
        return state;
    })
    .case(BlogAction.fetchDraft.failed, (state, payload) => {
        let pageState = state.pages.draft;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.params.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: new Blog({ slug: payload.params.slug }), loading: false, success: false }));
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'draft'], pageState);
        return state;
    })
    .case(BlogAction.setDraft, (state, payload) => {
        let pageState = state.pages.draft;
        const index = pageState.findIndex(p => p.get('blog').slug == payload.blog.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ blog: payload.blog, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('blog', payload.blog);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'draft'], pageState);
        return state;
    })

export default BlogReducer;