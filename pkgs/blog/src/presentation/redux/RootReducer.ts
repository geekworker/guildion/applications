import { combineReducers, Reducer } from 'redux';
import { GlobalAction } from './GlobalAction';
import appReducer, { AppAction, AppState } from '@/presentation/redux/App/AppReducer';
import blogReducer, { BlogAction, BlogState } from './Blog/BlogReducer';

export interface RootState {
    app: AppState,
    blog: BlogState,
}

export type RootAction = GlobalAction | AppAction | BlogAction;

export const rootReducer = (): Reducer => {
    return combineReducers({
        app: appReducer,
        blog: blogReducer,
    });
}

export const initRootState = (): Partial<RootState> => {
    return {
        app: new AppState(),
        blog: new BlogState(),
    };
};