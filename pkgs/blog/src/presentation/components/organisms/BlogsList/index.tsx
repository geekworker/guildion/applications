import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { Blogs } from '@guildion/core';
import BlogCard from '../../molecules/BlogCard';

type Props = {
    blogs: Blogs,
} & LayoutProps;

const BlogsList: React.FC<Props> = ({
    style,
    className,
    blogs,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['items']}>
                {blogs.toArray().map((blog, key) => (
                    <div className={styles['item']} key={key}>
                        <BlogCard blog={blog} />
                    </div>
                ))}
            </div>
        </div>
    );
};

export default React.memo(BlogsList, compare);