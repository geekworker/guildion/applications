import BlogHTMLViewerHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { Localizer, Blog } from '@guildion/core';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { useLocalizer } from '@/shared/hooks/useLocalizer';

namespace BlogHTMLViewerPresenter {
    export type Input = {
        blog: Blog,
    } & LayoutProps;
    
    export type Output = {
        localizer: Localizer,
        appTheme: AppTheme,
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {} = BlogHTMLViewerHooks.useState(props);
        return {
            ...props,
            appTheme: useAppTheme(),
            localizer: useLocalizer(),
        }
    }
}

export default BlogHTMLViewerPresenter;