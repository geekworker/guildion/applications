import { useClassNames } from '@/shared/hooks/useClassNames';
import React from 'react';
import HTMLEmbedRender from '../HTMLEmbedRender';
import BlogHTMLViewerPresenter from './presenter';

const BlogHTMLViewerComponent = ({ style, className, blog, localizer, appTheme }: BlogHTMLViewerPresenter.Output) => {
    const containerClassName = useClassNames('blog-html-viewer', className);
    return (
        <div className={containerClassName} style={style}>
            <HTMLEmbedRender
                className="blog-html-viewer__inner"
                html={blog.getData(localizer.lang)}
            />
        </div>
    );
};

export default React.memo(BlogHTMLViewerComponent, BlogHTMLViewerPresenter.outputAreEqual);