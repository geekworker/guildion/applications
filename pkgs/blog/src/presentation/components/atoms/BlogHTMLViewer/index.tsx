import React from 'react';
import BlogHTMLViewerComponent from './component';
import BlogHTMLViewerPresenter from './presenter';

const BlogHTMLViewer = (props: BlogHTMLViewerPresenter.Input) => {
    const output = BlogHTMLViewerPresenter.usePresenter(props);
    return <BlogHTMLViewerComponent {...output} />;
};

export default React.memo(BlogHTMLViewer, BlogHTMLViewerPresenter.inputAreEqual);