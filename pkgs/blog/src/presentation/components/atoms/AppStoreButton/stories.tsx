import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import AppStoreButton from '.';

export default {
    component: AppStoreButton,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof AppStoreButton>;

export const Default: ComponentStoryObj<typeof AppStoreButton> = {
    args: {
    },
}