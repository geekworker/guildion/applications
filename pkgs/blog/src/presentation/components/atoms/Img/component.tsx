import Skeleton from 'react-loading-skeleton'
import 'react-loading-skeleton/dist/skeleton.css';
import React from 'react';
import ImgPresenter from './presenter';
import Image from "next/image";
import { useClassNames } from '@/shared/hooks/useClassNames';
import styles from './styles.module.scss';

const ImgComponent = ({ src, transformations, loading, className, ...rest }: ImgPresenter.Output) => {
    const imgClassName = useClassNames(className, { [styles['container']]: loading });
    return (
        <Image
            src={src}
            {...rest}
            layout={rest.layout ?? (rest.width === undefined && rest.height === undefined ? 'fill' : undefined)}
            unoptimized
            className={imgClassName}
        />
    );
};

export default React.memo(ImgComponent, ImgPresenter.outputAreEqual);