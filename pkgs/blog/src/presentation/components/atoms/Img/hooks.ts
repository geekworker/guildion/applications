import React from 'react';
import { CDN_PROXY_URL_STRING, PCDN_PROXY_URL_STRING } from '@guildion/core';
import type ImgPresenter from './presenter';
import { ImgTransformation } from './types';

namespace ImgHooks {
    export const smallLightRegExp = /\/small_light[^/]*\//
    export const generateTransformPath = (transformations: ImgTransformation) => {
        return `small_light(${
            Object.keys(transformations)
                .filter(k => !!transformations[k])
                .map(k => `${k}=${transformations[k]}`)
                .join(',')
        })`;
    }
    export const useState = (props: ImgPresenter.Input) => {
        const [loading, setLoading] = React.useState(props.loading ?? true);
        const transformedPath = React.useMemo(() => props.transformations ? generateTransformPath(props.transformations) : '', [props.transformations]);
        const src = React.useMemo(() => {
            if (!props.transformations) return props.src;
            if (props.src.startsWith(CDN_PROXY_URL_STRING)) {
                const baseSrc = props.src
                    .replace(`${CDN_PROXY_URL_STRING}/`, '')
                    .replace(smallLightRegExp, '');
                return `${CDN_PROXY_URL_STRING}/${transformedPath}/${baseSrc}`;
            } else if (props.src.startsWith(PCDN_PROXY_URL_STRING)) {
                const baseSrc = props.src
                    .replace(`${PCDN_PROXY_URL_STRING}/`, '')
                    .replace(smallLightRegExp, '');
                return `${PCDN_PROXY_URL_STRING}/${transformedPath}/${baseSrc}`;
            } else {
                return props.src;
            }
        }, [props.src, props.transformations]);

        const onLoadStart = React.useCallback(() => {
            setLoading(true);
        }, []);

        const onLoadingComplete = React.useCallback(() => {
            setLoading(false);
        }, []);

        return {
            src,
            loading,
            onLoadStart,
            onLoadingComplete,
        };
    }
}

export default ImgHooks;