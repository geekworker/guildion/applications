import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import ImgPresenter from '../Img/presenter';
import Img from '../Img';
import Link from 'next/link';
import { useLocalizer } from '@/shared/hooks/useLocalizer';

type Props = {
    name: string,
    href: string,
} & LayoutProps & ImgPresenter.Input;

const ReferenceImg: React.FC<Props> = ({
    style,
    className,
    name,
    href,
    ...rest
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    const localizer = useLocalizer();
    return (
        <div
            className={containerClassName}
            style={style}
        >
            <Link href={href}>
                <a target="_blank">
                    <div className={styles['image']}>
                        <Img {...rest} />
                    </div>
                </a>
            </Link>
            <div className={styles['text']}>
                <div className={styles['text-title']}>
                    {localizer.dic.blog.attr.source}:
                </div>
                <Link href={href}>
                    <a target="_blank" className={styles['text-link']}>
                        {name}
                    </a>
                </Link>
            </div>
        </div>
    );
};

export default React.memo(ReferenceImg, compare);