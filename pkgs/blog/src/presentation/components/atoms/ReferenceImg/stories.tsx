import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import ReferenceImg from '.';

export default {
    component: ReferenceImg,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof ReferenceImg>;

export const Default: ComponentStoryObj<typeof ReferenceImg> = {
    args: {
    },
}