import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import { LayoutProps } from "@guildion/next";
import Link from "next/link";
import { CDN_PROXY_URL_STRING, LanguageCode } from "@guildion/core";
import Image from "next/image";

const GooglePlayStoreButton: React.FC<Omit<IconProps, 'color'> & LayoutProps & { href: string, lang: LanguageCode }> = ({ width, height, className, style, lang, href }) => {
    return (
        <Link href={href}>
            <a target="_blank" style={{ width: `${width}px`, height: `${height}px`, position: 'relative' }}>
                <Image layout={'fill'} src={`${CDN_PROXY_URL_STRING}/abs/google-play-store/${lang}.png`} />
            </a>
        </Link>
    );
};

export default React.memo(GooglePlayStoreButton, compare);