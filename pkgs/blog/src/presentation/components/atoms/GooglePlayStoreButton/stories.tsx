import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import GooglePlayStoreButton from '.';

export default {
    component: GooglePlayStoreButton,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof GooglePlayStoreButton>;

export const Default: ComponentStoryObj<typeof GooglePlayStoreButton> = {
    args: {
    },
}