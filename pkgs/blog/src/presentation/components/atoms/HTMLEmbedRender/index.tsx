import React from 'react';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import Link from 'next/link';
import { HTMLEmbed, HTMLEmbedSeparator } from '@guildion/core';
import Custom404Text from '../../icons/Custom404Text';
import Img from '../Img';
import ReferenceImg from '../ReferenceImg';
import styles from './styles.module.scss';
import AppStoreButton from '../AppStoreButton';
import GooglePlayStoreButton from '../GooglePlayStoreButton';
import { useLocalizer } from '@/shared/hooks/useLocalizer';

type Props = {
    html: string,
} & LayoutProps;

const HTMLEmbedRender: React.FC<Props> = ({
    style,
    className,
    html,
}) => {
    const containerClassName = useClassNames(className);
    const strings = React.useMemo(() => html.split(HTMLEmbedSeparator), [html]);
    const localizer = useLocalizer();
    return (
        <div className={containerClassName} style={style}>
            {strings.map((str, key) => {
                if (str.startsWith(HTMLEmbed.Link.value)) {
                    const json = str.replace(HTMLEmbed.Link.value, '');
                    const options = HTMLEmbed.Link.parse(json);
                    return (
                        <Link key={key} href={options.href}><a className={options.className}></a></Link>
                    );
                } else if (str.startsWith(HTMLEmbed.Image.value)) {
                    const json = str.replace(HTMLEmbed.Image.value, '');
                    const options = HTMLEmbed.Image.parse(json);
                    return options.layout === 'fill' ? (
                        <div key={key} style={{
                            maxWidth: `${options.width}px`,
                            maxHeight: `${options.height}px`,
                            position: 'relative',
                            margin: '0 auto',
                        }}>
                            <Img {...options} layout={'responsive'} />
                        </div>
                    ) : (
                        <Img key={key} {...options} />
                    );
                } else if (str.startsWith(HTMLEmbed.ReferenceImage.value)) {
                    const json = str.replace(HTMLEmbed.ReferenceImage.value, '');
                    const options = HTMLEmbed.ReferenceImage.parse(json);
                    return (
                        <ReferenceImg key={key} {...options} />
                    );
                } else if (str.startsWith(HTMLEmbed.AppDownloadButton.value)) {
                    const json = str.replace(HTMLEmbed.AppDownloadButton.value, '');
                    const options = HTMLEmbed.AppDownloadButton.parse(json);
                    return (
                        <div className={styles['store-buttons']} key={key}>
                            {options.iosHref && (<AppStoreButton width={240} height={120} href={options.iosHref} lang={localizer.lang} />)}
                            {options.androidHref && (<GooglePlayStoreButton width={280} height={120} href={options.androidHref} lang={localizer.lang} />)}
                        </div>
                    );
                } else if (str.startsWith(HTMLEmbed.Custom.value)) {
                    const json = str.replace(HTMLEmbed.Custom.value, '');
                    const options = HTMLEmbed.Custom.parse(json);
                    switch (options.type) {
                    case '404': return <Custom404Text key={key} className={styles['custom-404']} />;
                    default: return <div key={key} />;
                    }
                } else {
                    return (
                        <div
                            key={key}
                            dangerouslySetInnerHTML={{
                                __html: str,
                            }}
                        />
                    )
                }
            })}
        </div>
    );
};

export default React.memo(HTMLEmbedRender, compare);