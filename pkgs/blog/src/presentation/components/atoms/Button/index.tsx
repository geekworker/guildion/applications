import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import Link from 'next/link';
import { DOMAttributes } from 'react';

type Props = {
    disabled?: boolean,
    loading?: boolean,
    text?: string,
    icon?: React.ReactNode,
    iconRight?: React.ReactNode,
    disabledClassName?: string,
    textClassName?: string,
    href?: string,
    onClick?: DOMAttributes<HTMLAnchorElement>['onClick'],
} & LayoutProps;

const Button: React.FC<Props> = ({
    style,
    className,
    disabledClassName,
    disabled,
    loading,
    text,
    icon,
    iconRight,
    textClassName,
    href,
    onClick,
}) => {
    const containerClassName = useClassNames(className, styles['container'], { [styles['disabled']]: disabled }, disabledClassName ? { [disabledClassName]: disabled } : {});
    const $textClassName = useClassNames(textClassName, styles['text']);
    const inner = (
        <div className={styles['container-inner']}>
            <div className={styles['icon']}>
                {icon}
            </div>
            <div className={$textClassName}>
                {text}
            </div>
            <div className={styles['icon-right']}>
                {iconRight}
            </div>
        </div>
    );
    return (
        <div className={containerClassName} style={style}>
            {href ? (
                <Link href={href}>
                    <a onClick={onClick}>
                        {inner}
                    </a>
                </Link>
            ) : (
                <a onClick={onClick}>
                    {inner}
                </a>
            )}
        </div>
    );
};

export default React.memo(Button, compare);