import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { Blog, BlogEndpoints, TimeZone } from '@guildion/core';
import Img from '../../atoms/Img';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import Link from 'next/link';

type Props = {
    blog: Blog,
} & LayoutProps;

const BlogCard: React.FC<Props> = ({
    style,
    className,
    blog,
}) => {
    const localier = useLocalizer();
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <Link href={BlogEndpoints.blogShowRouter.toPath({ slug: blog.slug })}>
            <a className={containerClassName} style={style}>
                <div className={styles['header']}>
                    <Img
                        src={blog.getThumbnailURL(localier.lang)!}
                        transformations={{
                            dh: '600',
                        }}
                        width={500}
                        height={250}
                        layout={'responsive'}
                    />
                </div>
                <div className={styles['body']}>
                    <div className={styles['body-category']}>
                        {blog.records.category?.getName(localier.lang)}
                    </div>
                    <div className={styles['body-title']}>
                        {blog.getTitle(localier.lang)}
                    </div>
                    <div className={styles['body-date']}>
                        {blog.getCreatedFormatedAt(TimeZone.Tokyo)}
                    </div>
                </div>
            </a>
        </Link>
    );
};

export default React.memo(BlogCard, compare);