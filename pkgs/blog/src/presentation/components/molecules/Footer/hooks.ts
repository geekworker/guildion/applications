import React from 'react';
import { FooterType } from './constants';
import type FooterPresenter from './presenter';
import FooterRedux from './redux';

namespace FooterHooks {
    export const useState = (props: FooterPresenter.Input) => {
        const context = React.useContext(FooterRedux.Context);
        return {
            type: context.state.type,
        };
    }

    export const useDocument = () => {
        const context = React.useContext(FooterRedux.Context);
        React.useEffect(() => {
            context.dispatch(FooterRedux.Action.setType(FooterType.Document));
            return () => context.dispatch(FooterRedux.Action.setType(FooterType.Default));
        }, []);
    }
}

export default FooterHooks;