import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import Footer from '.';

export default {
    component: Footer,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof Footer>;

export const Default: ComponentStoryObj<typeof Footer> = {
    args: {
    },
}