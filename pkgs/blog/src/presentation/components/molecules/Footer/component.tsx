import React from 'react';
import { FooterLink, FooterType, getFooterLinks } from './constants';
import FooterPresenter from './presenter';
import Link from 'next/link';
import styles from './styles.module.scss';
import { BLOG_NAME, WebEndpoints } from '@guildion/core';
import GeeklizeIcon from '../../icons/GeeklizeIcon';

const FooterComponent = ({ type, appTheme, localizer }: FooterPresenter.Output) => {
    const footerLinks = React.useMemo(
        () => getFooterLinks(localizer),
        [localizer]
    );

    const renderItem = (items: FooterLink[]) => items.map((data, key) => (
        <li className={styles['default-section_item']} key={`footer-item-${key}`}>
            <Link href={data.link}>
                <a className={styles['default-section_item-link']}>
                    {data.name()}
                </a>
            </Link>
        </li>
    ))

    const renderSection = () => footerLinks.map((data, key) => (
        <div className={styles['default-section']} key={`footer-section-${key}`}>
            <h4 className={styles['default-section_heading']}>
                {data.name()}
            </h4>
            <ul className={styles['default-section_items']}>
                {renderItem(data.links)}
            </ul>
        </div>
    ));

    const defaultFooter = (
        <div className={styles['default']} id={styles['footer']}>
            <div className={styles['default-inner']}>
                {/* <div className={styles['default-sections']}>
                    {renderSection()}
                </div>
                <div className={styles['default-separator']}/> */}
                <Link href={WebEndpoints.homeRouter.path}>
                    <a className={styles['default-logo']}>
                        <GeeklizeIcon width={20} height={20} color={appTheme.element.default} />
                        <span className={styles['default-logo_name']}>
                            {BLOG_NAME}
                        </span>
                    </a>
                </Link>
                <div className={styles['default-addendum']}>
                    <p className={styles['default-addendum_copyright']}>
                        Copyright © 2022 Zack
                    </p>
                    <ul className={styles['default-addendum_links']}>
                        <li>
                            <Link href={`https://policies.google.com/technologies/partner-sites?hl=${localizer.lang}`}>
                                <a className={styles['default-addendum_link']} target="_blank" rel="noopener">
                                    {localizer.dic.blog.footer.googleAnalytics}
                                </a>
                            </Link>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );

    switch(type) {
    case FooterType.Document:
        return (
            <div className={styles['document']}>
                {defaultFooter}
            </div>
        );
    default:
    case FooterType.Default:
        return defaultFooter;
    };
};

export default React.memo(FooterComponent, FooterPresenter.outputAreEqual);