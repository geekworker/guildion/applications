import { Localizer } from "@guildion/core";

export const FooterType = {
    Default: "Default",
    Document: "Document",
} as const;
  
export type FooterType = typeof FooterType[keyof typeof FooterType];

export interface FooterLink {
    name: () => string,
    link: string,
}

export interface FooterLinks {
    name: () => string,
    links: FooterLink[],
}

export const getFooterLinks = (localizer: Localizer): FooterLinks[] => [
];