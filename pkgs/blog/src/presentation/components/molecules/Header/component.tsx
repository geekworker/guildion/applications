import React from 'react';
import { HeaderType } from './constants';
import HeaderPresenter from './presenter';
import styles from './styles.module.scss';
import Link from 'next/link';
import { BLOG_NAME, WebEndpoints } from '@guildion/core';
import { useClassNames } from '@/shared/hooks/useClassNames';
import GeeklizeIcon from '../../icons/GeeklizeIcon';

const HeaderComponent = ({ type, appTheme, height, scrolling, className, style, localizer }: HeaderPresenter.Output) => {
    const defaultClassName = useClassNames(className, styles['default'], { [styles['default-hidden']]: scrolling && type !== HeaderType.Fixed });
    const defaultHeader = (
        <div style={{ ...style, paddingTop: height }} id={styles['header']}>
            <div className={defaultClassName}>
                <div className={styles['default-inner']}>
                    <Link href={WebEndpoints.homeRouter.path}>
                        <a className={styles['default-logo']}>
                            <GeeklizeIcon width={20} height={20} color={appTheme.element.default} />
                            <span className={styles['default-logo_name']}>
                                {BLOG_NAME}
                            </span>
                        </a>
                    </Link>
                    <span className={styles['default-description']}>
                        {localizer.dic.blog.description}
                    </span>
                </div>
            </div>
        </div>
    );

    switch(type) {
    case HeaderType.Fixed:
    default:
    case HeaderType.Default:
        return defaultHeader;
    };
};

export default React.memo(HeaderComponent, HeaderPresenter.outputAreEqual);