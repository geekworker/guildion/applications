import React from 'react';
import HeaderComponent from './component';
import HeaderPresenter from './presenter';

const Header = (props: HeaderPresenter.Input) => {
    const output = HeaderPresenter.usePresenter(props);
    return <HeaderComponent {...output} />;
};

export default React.memo(Header, HeaderPresenter.inputAreEqual);