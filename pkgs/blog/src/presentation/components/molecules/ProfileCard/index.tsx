import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { CDN_PROXY_URL_STRING } from '@guildion/core';
import Link from 'next/link';
import TwitterIcon from '../../icons/TwitterIcon';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import PayPalIcon from '../../icons/PayPalIcon';
import Img from '../../atoms/Img';

type Props = {
} & LayoutProps;

const ProfileCard: React.FC<Props> = ({
    style,
    className,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    const appTheme = useAppTheme();
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['container-profile']}>
                <Img
                    src={`${CDN_PROXY_URL_STRING}/abs/zack/original.png`}
                    transformations={{
                        dw: '240',
                        of: 'webp',
                    }}
                />
            </div>
            <div className={styles['container-name']}>
                Zack
            </div>
            <div className={styles['container-description']}>
                {`フリーランスエンジニア・UI/UXデザイナー。\n日本在住ですが開発している会社は英語です。\n色々縁があり快適なフリーランスライフを送っています。\n少し前はブロックチェーンのサービスで起業したり、海外の大学の教授をやっていました。\n今はGuildionという動画中心のクリエイターコミュニティーサービスを作っています。\n普段から色々なギークな知識に触れることが多いので、その一部を還元できたらなと思ってブログを書いています。\n何卒よろしくお願いします。`}
            </div>
            <div className={styles['container-social']}>
                <Link href={'https://twitter.com/__I_OXO_I__'}>
                    <a className={styles['container-social_link']} target="_blank" rel="noopener">
                        <TwitterIcon width={24} height={24} color={appTheme.element.subp1} />
                    </a>
                </Link>
                {/* <Link href={GET_CURRENT_APP_URL_STRING()}>
                    <a className={styles['container-social_link']} target="_blank" rel="noopener">
                        <GuildionIcon width={24} height={24} color={appTheme.element.subp1} />
                    </a>
                </Link> */}
                <Link href={'https://paypal.me/guildionzack'}>
                    <a className={styles['container-social_link']} target="_blank" rel="noopener">
                        <PayPalIcon width={24} height={24} color={appTheme.element.subp1} lightColor={appTheme.element.subp2} />
                    </a>
                </Link>
            </div>
        </div>
    );
};

export default React.memo(ProfileCard, compare);