import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import ProfileSideBar from '.';

export default {
    component: ProfileSideBar,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof ProfileSideBar>;

export const Default: ComponentStoryObj<typeof ProfileSideBar> = {
    args: {
    },
}