import React from 'react';
import type HomePresenter from './presenter';

namespace HomeHooks {
    export const useState = (props: HomePresenter.MergedProps) => {
        const blogs = React.useMemo(() => props.page?.get('blogs'), [props.page]);
        return {
            blogs,
        };
    }
}

export default HomeHooks;