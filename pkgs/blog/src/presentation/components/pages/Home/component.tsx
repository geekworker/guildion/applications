import { useClassNames } from '@/shared/hooks/useClassNames';
import styles from './styles.module.scss';
import React from 'react';
import BlogsIndexTemplate from '../../templates/BlogsIndexTemplate';
import HomePresenter from './presenter';

const HomeComponent = ({ style, className, blogs }: HomePresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            {blogs && (
                <BlogsIndexTemplate blogs={blogs}/>
            )}
        </div>
    );
};

export default React.memo(HomeComponent, HomePresenter.outputAreEqual);