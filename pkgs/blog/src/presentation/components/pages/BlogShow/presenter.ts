import BlogShowHooks from './hooks';
import { Action, bindActionCreators } from 'redux';
import { RootState } from '@/presentation/redux/RootReducer';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { Blog, Localizer } from '@guildion/core';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { BlogPageState } from '@/presentation/redux/Blog/BlogReducer';
import { blogDraftPageSelector, blogShowPageSelector } from '../../../redux/Blog/BlogSelector';

namespace BlogShowPresenter {
    export type StateProps = {
        page?: BlogPageState,
    }
    
    export type DispatchProps = {
    }
    
    export type Input = {
        slug: string,
        draft?: boolean,
    } & LayoutProps;
    
    export type Output = {
        appTheme: AppTheme,
        localizer: Localizer,
        blog?: Blog,
    } & Omit<MergedProps, ''>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            page: ownProps.draft ? blogDraftPageSelector(state, { slug: ownProps.slug }) : blogShowPageSelector(state, { slug: ownProps.slug }),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.app.csr == next.app.csr &&
            prev.blog.pages.show == next.blog.pages.show &&
            prev.blog.pages.draft == next.blog.pages.draft
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const {
            blog,
        } = BlogShowHooks.useState(props);
        return {
            ...props,
            blog,
            appTheme: useAppTheme(),
            localizer: useLocalizer(),
        }
    }
}

export default BlogShowPresenter;