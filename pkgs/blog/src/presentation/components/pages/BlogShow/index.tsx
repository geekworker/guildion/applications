import React from 'react';
import BlogShowPresenter from './presenter';
import BlogShowComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: BlogShowPresenter.MergedProps) => {
    const output = BlogShowPresenter.usePresenter(props);
    return <BlogShowComponent {...output} />;
};

const BlogShow = connect<BlogShowPresenter.StateProps, BlogShowPresenter.DispatchProps, BlogShowPresenter.Input, BlogShowPresenter.MergedProps, RootState>(
    BlogShowPresenter.mapStateToProps,
    BlogShowPresenter.mapDispatchToProps,
    BlogShowPresenter.mergeProps,
    BlogShowPresenter.connectOptions,
)(Container);

export default BlogShow;