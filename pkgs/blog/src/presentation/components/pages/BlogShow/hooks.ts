import React from 'react';
import type BlogShowPresenter from './presenter';

namespace BlogShowHooks {
    export const useState = (props: BlogShowPresenter.MergedProps) => {
        const blog = React.useMemo(() => props.page?.get('blog'), [props.page]);
        return {
            blog,
        };
    }
}

export default BlogShowHooks;