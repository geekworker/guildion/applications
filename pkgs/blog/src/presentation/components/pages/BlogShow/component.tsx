import React from 'react';
import styles from './styles.module.scss';
import BlogShowPresenter from './presenter';
import OGP from '@/pages/OGP';
import { BlogEndpoints } from '@guildion/core';
import BlogShowTemplate from '../../templates/BlogShowTemplate';

const BlogShowComponent = ({ appTheme, className, style, page, localizer, slug, blog }: BlogShowPresenter.Output) => {
    return (
        <>
            <OGP
                title={blog?.getTitle(localizer.lang)}
                description={blog?.getDescription(localizer.lang)}
                imgSrc={blog?.getThumbnailURL(localizer.lang)}
                url={BlogEndpoints.blogShowRouter.toPath({ slug })}
            />
            {blog && (
                <BlogShowTemplate blog={blog} className={className} style={style} />
            )}
        </>
    );
};

export default React.memo(BlogShowComponent, BlogShowPresenter.outputAreEqual);