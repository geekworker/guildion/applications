import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import GeeklizeIcon from '.';

export default {
    component: GeeklizeIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof GeeklizeIcon>;

export const Default: ComponentStoryObj<typeof GeeklizeIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}