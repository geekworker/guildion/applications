import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import { LayoutProps } from "@guildion/next";

const GeeklizeIcon: React.FC<IconProps & LayoutProps> = ({ width, height, color, className, style }) => {
    return (
        <svg
            viewBox="0 0 391.8 500"
            width={width}
            height={height}
            className={className}
            style={style}
        >
            <path fill={color} d="M267.14-.13,54,133.71V321.46c12.56,2.64,38.21,10.07,57,32.82a88.21,88.21,0,0,1,9.92,14.88q1.23-88,2.48-176l119-74.36S239.88,27.13,267.14-.13Z" transform="translate(-54 0.13)"/>
            <path fill={color} d="M232.65,499.87,445.8,366V178.28c-12.57-2.64-38.21-10.07-57-32.82a87.66,87.66,0,0,1-9.91-14.87q-1.25,88-2.48,176l-119,74.36S259.91,472.61,232.65,499.87Z" transform="translate(-54 0.13)"/>
            <path fill={color} d="M378.88,131.23,220.26,247.71c2.91,1.75,6-1.86,8.69,1.21,11.65,13.19,17.11,39.73,16.09,43.4,47.81-35,86-61.67,133.84-96.65" transform="translate(-54 0.13)"/>
        </svg>
    );
};

export default React.memo(GeeklizeIcon, compare);