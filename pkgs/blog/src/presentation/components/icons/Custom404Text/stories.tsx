import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import Custom404Text from '.';

export default {
    component: Custom404Text,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof Custom404Text>;

export const Default: ComponentStoryObj<typeof Custom404Text> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}