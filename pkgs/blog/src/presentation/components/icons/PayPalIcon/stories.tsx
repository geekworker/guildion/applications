import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import PayPalIcon from '.';

export default {
    component: PayPalIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof PayPalIcon>;

export const Default: ComponentStoryObj<typeof PayPalIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof PayPalIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}