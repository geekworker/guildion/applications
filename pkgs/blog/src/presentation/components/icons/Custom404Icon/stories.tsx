import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import Custom404Icon from '.';

export default {
    component: Custom404Icon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof Custom404Icon>;

export const Default: ComponentStoryObj<typeof Custom404Icon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}