import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import GuildionIcon from '.';

export default {
    component: GuildionIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof GuildionIcon>;

export const Default: ComponentStoryObj<typeof GuildionIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof GuildionIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
        animationType: 'fadeIn',
    },
}