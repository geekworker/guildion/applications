import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import YouTubeIcon from '.';

export default {
    component: YouTubeIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof YouTubeIcon>;

export const Default: ComponentStoryObj<typeof YouTubeIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof YouTubeIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}