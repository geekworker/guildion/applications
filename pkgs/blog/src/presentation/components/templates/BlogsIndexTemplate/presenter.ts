import BlogsIndexTemplateHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { Blogs, Localizer } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';

namespace BlogsIndexTemplatePresenter {
    export type Input = {
        blogs: Blogs,
    } & LayoutProps;
    
    export type Output = {
        localizer: Localizer,
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {} = BlogsIndexTemplateHooks.useState(props);
        const localizer = useLocalizer();
        return {
            ...props,
            localizer,
        }
    }
}

export default BlogsIndexTemplatePresenter;