import { useClassNames } from '@/shared/hooks/useClassNames';
import React from 'react';
import styles from './styles.module.scss';
import BlogsIndexTemplatePresenter from './presenter';
import ProfileSideBar from '../../molecules/ProfileSideBar';
import ProfileCard from '../../molecules/ProfileCard';
import BlogsList from '../../organisms/BlogsList';

const BlogsIndexTemplateComponent = ({ style, className, blogs, localizer }: BlogsIndexTemplatePresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['aside']}>
                <div className={styles['aside-inner']}>
                    <div className={styles['contents']}>
                        <div className={styles['contents-title']}>
                            {localizer.dic.blog.home.title}
                        </div>
                        <div className={styles['contents-items']}>
                            <BlogsList blogs={blogs} />
                        </div>
                        <div className={styles['contents-profile']}>
                            <ProfileCard/>
                        </div>
                    </div>
                    <div className={styles['side']}>
                        <div className={styles['side-inner']}>
                            <ProfileSideBar/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(BlogsIndexTemplateComponent, BlogsIndexTemplatePresenter.outputAreEqual);