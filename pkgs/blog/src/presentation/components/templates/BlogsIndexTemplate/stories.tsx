import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import BlogsIndexTemplate from '.';

export default {
    component: BlogsIndexTemplate,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof BlogsIndexTemplate>;

export const Default: ComponentStoryObj<typeof BlogsIndexTemplate> = {
    args: {
    },
}