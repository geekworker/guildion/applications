import React from 'react';
import BlogsIndexTemplateComponent from './component';
import BlogsIndexTemplatePresenter from './presenter';

const BlogsIndexTemplate = (props: BlogsIndexTemplatePresenter.Input) => {
    const output = BlogsIndexTemplatePresenter.usePresenter(props);
    return <BlogsIndexTemplateComponent {...output} />;
};

export default React.memo(BlogsIndexTemplate, BlogsIndexTemplatePresenter.inputAreEqual);