import BlogShowTemplateHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { Blog, Localizer } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { HTMLAnchor } from '@/shared/modules/HTMLAnchorizer';

namespace BlogShowTemplatePresenter {
    export type Input = {
        blog: Blog,
    } & LayoutProps;
    
    export type Output = {
        appTheme: AppTheme,
        localizer: Localizer,
        anchors: HTMLAnchor[],
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {} = BlogShowTemplateHooks.useState(props);
        const anchors = BlogShowTemplateHooks.useAnchors(props);
        return {
            ...props,
            appTheme: useAppTheme(),
            localizer: useLocalizer(),
            anchors,
        }
    }
}

export default BlogShowTemplatePresenter;