import React from 'react';
import BlogShowTemplateComponent from './component';
import BlogShowTemplatePresenter from './presenter';

const BlogShowTemplate = (props: BlogShowTemplatePresenter.Input) => {
    const output = BlogShowTemplatePresenter.usePresenter(props);
    return <BlogShowTemplateComponent {...output} />;
};

export default React.memo(BlogShowTemplate, BlogShowTemplatePresenter.inputAreEqual);