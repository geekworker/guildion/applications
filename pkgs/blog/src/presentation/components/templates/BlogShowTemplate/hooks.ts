import React from 'react';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import type BlogShowTemplatePresenter from './presenter';
import { generateAnchors } from '../../../../shared/modules/HTMLAnchorizer';

namespace BlogShowTemplateHooks {
    export const useState = (props: BlogShowTemplatePresenter.Input) => {
        return {};
    }

    export const useAnchors = (props: BlogShowTemplatePresenter.Input) => {
        const localizer = useLocalizer();
        const anchors = React.useMemo(() => generateAnchors(props.blog.getData(localizer.lang)), [props.blog, localizer.lang]);
        return anchors;
    }
}

export default BlogShowTemplateHooks;