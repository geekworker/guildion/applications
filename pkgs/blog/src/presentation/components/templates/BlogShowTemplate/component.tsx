import React from 'react';
import styles from './styles.module.scss';
import BlogShowTemplatePresenter from './presenter';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { CDN_PROXY_URL_STRING, GET_CURRENT_APP_URL_STRING, TimeZone } from '@guildion/core';
import BlogHTMLViewer from '../../atoms/BlogHTMLViewer';
import { HTMLAnchor } from '@/shared/modules/HTMLAnchorizer';
import Link from 'next/link';
import Img from '../../atoms/Img';
import TwitterIcon from '../../icons/TwitterIcon';
import GuildionIcon from '../../icons/GuildionIcon';
import PayPalIcon from '../../icons/PayPalIcon';
import ProfileCard from '../../molecules/ProfileCard';
import ProfileSideBar from '../../molecules/ProfileSideBar';

const BlogShowTemplateComponent = ({ style, className, blog, localizer, anchors, appTheme }: BlogShowTemplatePresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    const thumbnailURL = React.useMemo(() => blog.getThumbnailURL(localizer.lang), [blog, localizer.lang]);

    const renderAnchors = (anchors: HTMLAnchor[]) => (
        <ul className={styles['summary-sections']}>
            {anchors.map((a, i) => (
                <li className={styles['summary-section']} key={i}>
                    <Link href={`${a.id}`}>
                        <a>
                            {a.displayName}
                        </a>
                    </Link>
                    {renderAnchors(a.children)}
                </li>
            ))}
        </ul>
    );

    const renderIndexAnchors = (anchors: HTMLAnchor[]) => (
        <ul className={styles['contents-summary-sections']}>
            {anchors.map((a, i) => (
                <li className={styles['contents-summary-section']} key={i}>
                    <Link href={`${a.id}`}>
                        <a>
                            {a.displayName}
                        </a>
                    </Link>
                    {renderIndexAnchors(a.children)}
                </li>
            ))}
        </ul>
    );

    return (
        <div className={containerClassName} style={style}>
            <div className={styles['aside']}>
                <div className={styles['aside-inner']}>
                    <div className={styles['summary']}>
                        <div className={styles['summary-inner']}>
                            {renderAnchors(anchors)}
                        </div>
                    </div>
                    <div className={styles['contents']}>
                        <div className={styles['contents-inner']}>
                            <div className={styles['contents-header']}>
                                <p className={styles['contents-header_timestamp']}>
                                    {blog.getCreatedFormatedAt(TimeZone.Tokyo)}
                                </p>
                                <h1 className={styles['contents-header_title']}>
                                    {blog.getTitle(localizer.lang)}
                                </h1>
                                {thumbnailURL && (
                                    <div className={styles['contents-header_thumbnail']}>
                                        <Img
                                            src={thumbnailURL}
                                            transformations={{
                                                dh: '500',
                                                of: 'webp',
                                            }}
                                            width={'500'}
                                            height={'250'}
                                            layout={'responsive'}
                                        />
                                    </div>
                                )}
                                <div className={styles['contents-summary']}>
                                    <div className={styles['contents-summary-inner']}>
                                        <h2 className={styles['contents-summary-title']}>
                                            {localizer.dic.blog.attr.index}
                                        </h2>
                                        {renderIndexAnchors(anchors)}
                                    </div>
                                </div>
                                <BlogHTMLViewer blog={blog} />
                                <div className={styles['contents-profile']}>
                                    <ProfileCard/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={styles['side']}>
                        <div className={styles['side-inner']}>
                            <ProfileSideBar/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(BlogShowTemplateComponent, BlogShowTemplatePresenter.outputAreEqual);