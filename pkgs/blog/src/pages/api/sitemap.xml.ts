import { healthCheckAPI, HTTPStatusCode, BlogsEndpoint, BlogsShowPathsRequest, requestAPI, LanguageCode, GET_CURRENT_BLOG_URL_STRING } from "@guildion/core";
import type { NextApiRequest, NextApiResponse } from 'next';
import { getAppConfig } from "../../shared/constants/AppConfig";
import configureCore from '@/infrastructure/Core';
import configureENV from '@/infrastructure/ENV';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    configureENV();
    configureCore();
    res.setHeader("Cache-Control", "s-maxage=86400, stale-while-revalidate");
    res.setHeader("Content-Type", "text/xml");
    res.statusCode = HTTPStatusCode.OK;
    const xml = await generateSitemapXML();
    res.end(xml);
};

const generateSitemapXML = async (): Promise<string> => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return '';
    const blogShows = await requestAPI(BlogsEndpoint.ShowPaths, {
        data: new BlogsShowPathsRequest({}),
    });
    const xml = `<?xml version="1.0" encoding="UTF-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
            <url>
                <loc>${GET_CURRENT_BLOG_URL_STRING()}</loc>
                <lastmod>${new Date().toISOString()}</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.8</priority>
            </url>
            ${blogShows.paths
                .filter(path => path.locale === LanguageCode.Japanese)
                .map((path) => `
                    <url>
                        <loc>${GET_CURRENT_BLOG_URL_STRING()}/blogs/${path.params.slug}</loc>
                        <lastmod>${new Date().toISOString()}</lastmod>
                        <changefreq>weekly</changefreq>
                    </url>
                    <url>
                        <loc>${GET_CURRENT_BLOG_URL_STRING()}/${path.locale}/blogs/${path.params.slug}</loc>
                        <lastmod>${new Date().toISOString()}</lastmod>
                        <changefreq>weekly</changefreq>
                    </url>
                `)
                .join("")
            }
        </urlset>
    `;
    return xml;
}