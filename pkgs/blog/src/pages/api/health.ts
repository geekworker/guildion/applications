import { HTTPStatusCode } from "@guildion/core";
import type { NextApiRequest, NextApiResponse } from 'next';

export default function handler(req: NextApiRequest, res: NextApiResponse) {
    res.status(
        HTTPStatusCode.OK
    ).json({
        healthStatus: 'healthy'
    });
};
