import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { GET_CURRENT_BLOG_URL_STRING, CDN_PROXY_URL_STRING } from '@guildion/core';
import { useClassNames } from '../../shared/hooks/useClassNames';
import { useAppTheme } from '../../shared/hooks/useAppTheme';
import { useLocalizer } from '../../shared/hooks/useLocalizer';
import Custom404Icon from '../../presentation/components/icons/Custom404Icon';
import Custom404Text from '@/presentation/components/icons/Custom404Text';
import OGP from '@/pages/OGP';

type Props = {
} & LayoutProps;

const Custom404: React.FC<Props> = ({
    style,
    className,
}) => {
    const containerClassName = useClassNames(styles['container'], className);
    const appTheme = useAppTheme();
    const localizer = useLocalizer();
    return (
        <>
            <OGP
                description={localizer.dic.blog.description}
                imgSrc={`${CDN_PROXY_URL_STRING}/abs/geeklize/ogp.png`}
                imgWidth={1200}
                imgHeight={680}
                url={GET_CURRENT_BLOG_URL_STRING()}
            />
            <div className={containerClassName} style={style}>
                <div className={styles['text']}>
                    <Custom404Text/>
                </div>
                <div className={styles['icon']}>
                    <Custom404Icon width={300} height={200} color={appTheme.element.subp1} />
                </div>
            </div>
        </>
    );
};

export default React.memo(Custom404, compare);