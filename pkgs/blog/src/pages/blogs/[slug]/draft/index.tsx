import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import React from 'react';
import { BlogsEndpoint, BlogsDraftPathsRequest, healthCheckAPI, requestAPI } from '@guildion/core';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import { BlogAction } from '@/presentation/redux/Blog/BlogReducer';
import { getAppConfig } from '@/shared/constants/AppConfig';
import BlogShow from '../../../../presentation/components/pages/BlogShow';

type Props = {
    rebuild?: boolean,
    slug: string,
};

const BlogDraftContainer: React.FC<Props> = ({ rebuild, slug }) => {
    if (rebuild && process.browser && !!window) window.location.reload();
    return (
        <>
            {slug && (
                <BlogShow slug={slug} draft />
            )}
        </>
    );
};

export const getStaticPaths = async () => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return { paths: [], fallback: true };
    const result = await requestAPI(BlogsEndpoint.DraftPaths, {
        data: new BlogsDraftPathsRequest({}),
    })
    return { paths: result.paths, fallback: false };
}

const revalidate =
    CURRENT_ENV.NODE_ENV === "staging" ||
    CURRENT_ENV.NODE_ENV === "production"
        ? 86400 * 1
        : 60;

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (context: { params: { slug: string } }) => {
    const { slug } = context.params;
    if (getAppConfig().BUILD || !await healthCheckAPI()) return {
        props: {
            rebuild: true,
            slug,
        },
        revalidate: 1,
    };
    store.dispatch(BlogAction.fetchDraft.started({ slug }));
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
            slug,
        },
        revalidate,
    };
});

export default BlogDraftContainer;