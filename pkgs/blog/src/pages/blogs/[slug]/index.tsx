import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import React from 'react';
import { BlogsEndpoint, BlogsShowPathsRequest, healthCheckAPI, requestAPI } from '@guildion/core';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import { BlogAction } from '@/presentation/redux/Blog/BlogReducer';
import { getAppConfig } from '@/shared/constants/AppConfig';
import BlogShow from '../../../presentation/components/pages/BlogShow';

type Props = {
    rebuild?: boolean,
    slug: string,
};

const BlogShowContainer: React.FC<Props> = ({ rebuild, slug }) => {
    if (rebuild && process.browser && !!window) window.location.reload();
    return (
        <>
            {slug && (
                <BlogShow slug={slug} />
            )}
        </>
    );
};

export const getStaticPaths = async () => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return { paths: [], fallback: true };
    const result = await requestAPI(BlogsEndpoint.ShowPaths, {
        data: new BlogsShowPathsRequest({}),
    })
    return { paths: result.paths, fallback: false };
}

const revalidate =
    CURRENT_ENV.NODE_ENV === "staging" ||
    CURRENT_ENV.NODE_ENV === "production"
        ? 86400 * 1
        : 600;

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (context: { params: { slug: string } }) => {
    const { slug } = context.params;
    if (getAppConfig().BUILD || !await healthCheckAPI()) return {
        props: {
            rebuild: true,
            slug,
        },
        revalidate: 1,
    };
    store.dispatch(BlogAction.fetchShow.started({ slug }));
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
            slug,
        },
        revalidate,
    };
});

export default BlogShowContainer;