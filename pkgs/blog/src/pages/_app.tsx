import AppProvider from '@/infrastructure/AppProvider';
import { wrapper } from '@/presentation/redux/MakeStore';
import '@guildion/next/styles/global.scss';
import '@guildion/next/styles/blog.scss';
import '@/presentation/styles/globals.scss';
import { AppProps } from 'next/app';
import Head from 'next/head';
import NextNProgress from "nextjs-progressbar";
import Layout from './layout';

const App = ({ Component, pageProps }: AppProps) => (
    <>
        <AppProvider>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
            </Head>
            <NextNProgress
                color="#47B4A5"
                startPosition={0.3}
                stopDelayMs={200}
                height={3}
                showOnShallow={true}
                options={{ easing: "ease", speed: 500, showSpinner: false }}
            />
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </AppProvider>
    </>
);

export default wrapper.withRedux(App);