import { VFC } from 'react'
import Head from 'next/head'
import { BLOG_SEO_NAME } from '@guildion/core';

interface MetaData {
    title?: string,
    description?: string,
    url?: string,
    imgSrc?: string
    imgWidth?: number
    imgHeight?: number,
}

const OGP: VFC<MetaData> = ({
  title,
  description,
  url,
  imgSrc,
  imgWidth,
  imgHeight
}) => {
  return (
        <Head>
            <title>{title ? `${title} | ${BLOG_SEO_NAME}` : BLOG_SEO_NAME}</title>
            <meta name="viewport" content="width=device-width,initial-scale=1.0" />
            <meta name="description" content={description} />
            <meta name="thumbnail" content={imgSrc} />
            <meta property="og:url" content={url} />
            <meta property="og:title" content={title ? `${title} | ${BLOG_SEO_NAME}` : BLOG_SEO_NAME} />
            <meta property="og:site_name" content={BLOG_SEO_NAME} />
            <meta property="og:description" content={description} />
            <meta property="og:type" content="website" />
            <meta property="og:image" content={imgSrc} />
            <meta property="og:image:width" content={String(imgWidth ?? 1280)} />
            <meta property="og:image:height" content={String(imgHeight ?? 640)} />
            <meta name="twitter:card" content="summary_large_image"/>
            <link rel="preconnect" href="https://fonts.gstatic.com" />
            <link rel="canonical" href={url} />
        </Head>
    )
}

export default OGP;