import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import React from 'react';
import { CDN_PROXY_URL_STRING, GET_CURRENT_BLOG_URL_STRING, healthCheckAPI } from '@guildion/core';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import { BlogAction } from '@/presentation/redux/Blog/BlogReducer';
import { getAppConfig } from '@/shared/constants/AppConfig';
import OGP from './OGP';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import Home from '@/presentation/components/pages/Home';

type Props = {
    rebuild?: boolean,
};

const HomeContainer: React.FC<Props> = ({ rebuild }) => {
    if (rebuild && process.browser && !!window) window.location.reload();
    const localizer = useLocalizer();
    return (
        <>
            <OGP
                description={localizer.dic.blog.description}
                imgSrc={`${CDN_PROXY_URL_STRING}/abs/geeklize/ogp.png`}
                imgWidth={1200}
                imgHeight={680}
                url={GET_CURRENT_BLOG_URL_STRING()}
            />
            <Home/>
        </>
    );
};

const revalidate =
    CURRENT_ENV.NODE_ENV === "staging" ||
    CURRENT_ENV.NODE_ENV === "production"
        ? 86400 * 1
        : 600;

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (_) => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return {
        props: {
            rebuild: true,
        },
        revalidate: 1,
    };
    store.dispatch(BlogAction.fetchIndex.started({}));
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {},
        revalidate,
    };
});

export default HomeContainer;