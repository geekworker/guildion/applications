import { guardCase } from '@guildion/core/src/interfaces/CaseIterable';
import { LanguageCode, fallbackLanguageCode } from '../constants/LanguageCode';
import { Dictionary, EN_Dictionary, JA_Dictionary, fallbackDictionary } from './dictionary';
import { Locale } from 'date-fns';
import ja from 'date-fns/locale/ja';
import enUS from 'date-fns/locale/en-US';

export const idPrefix = '-hreflang';

export const SUPPORTED_LANGUAGES = [
    LanguageCode.Japanese,
    LanguageCode.English,
];

export type SUPPORTED_LANGUAGES_KEY = keyof Localizer['dictionaries']

export const localizablePath: string = `(${SUPPORTED_LANGUAGES.map(lang => `\/${lang}`).join('|')})?`

export class Localizer {
    private lang$: LanguageCode = fallbackLanguageCode;
    private dictionary$: Dictionary = fallbackDictionary;
    private dictionaries$!: { [key: string]: Dictionary };
    
    constructor(lang: LanguageCode) {
        this.setLang(lang);
    }

    getDictionary(lang: LanguageCode): Dictionary {
        switch(guardCase(lang, { cases: LanguageCode, fallback: fallbackLanguageCode })) {
        default:
        case LanguageCode.English: return EN_Dictionary;
        case LanguageCode.Japanese: return JA_Dictionary;
        }
    }

    setLang(lang: LanguageCode) {
        this.lang$ = guardCase(lang, { cases: LanguageCode, fallback: fallbackLanguageCode });
        this.dictionary$ = this.getDictionary(lang);
    }

    get lang(): LanguageCode {
        return this.lang$;
    }

    get localeDateFns(): Locale {
        switch (this.lang$) {
        case LanguageCode.Japanese: return ja;
        default:
        case LanguageCode.English: return enUS;
        }
    }

    get dictionary(): Dictionary {
        return this.dictionary$;
    }

    get dic(): Dictionary {
        return this.dictionary$;
    }

    get dictionaries(): { [key: string]: Dictionary } {
        if (!this.dictionaries$) {
            this.dictionaries$ = {};
            SUPPORTED_LANGUAGES.map(lang => { this.dictionaries$[lang] = this.getDictionary(lang) });
        }
        return this.dictionaries$;
    }

    static get dictionaries(): { [key: string]: Dictionary } {
        const dictionaries: { [key: string]: Dictionary } = {};
        SUPPORTED_LANGUAGES.map(lang => { dictionaries[lang] = new Localizer(lang).getDictionary(lang) });
        return dictionaries;
    }
}