export const formatEnDictionary = {
    date: {
        getRestShortTime: (ms: number): string => {
            const s = ms % 1000;
            ms = (ms - s) / 1000;
            const secs = ms % 60;
            ms = (ms - secs) / 60;
            const mins = ms % 60;
            const hrs = (ms - mins) / 60;
            if (hrs > 0) {
                return `${hrs} hours`
            } else if (mins > 0) {
                return `${mins} minutes`
            } else {
                return `${secs} seconds`
            }
        },
    },
};
