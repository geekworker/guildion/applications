import { formatEnDictionary } from "./en";

export const formatJaDictionary: typeof formatEnDictionary = {
    date: {
        getRestShortTime: (ms: number): string => {
            const s = ms % 1000;
            ms = (ms - s) / 1000;
            const secs = ms % 60;
            ms = (ms - secs) / 60;
            const mins = ms % 60;
            const hrs = (ms - mins) / 60;
            if (hrs > 0) {
                return `残り${hrs}時間`
            } else if (mins > 0) {
                return `残り${mins}分`
            } else {
                return `残り${secs}秒`
            }
        },
    },
};