import { gEnDictionary } from "./en";

export const gJaDictionary: typeof gEnDictionary = {
    back: '戻る',
    next: '次へ',
    share: '共有',
    seeMore: 'さらに見る',
    start: '始める',
    tryIt: '試してみる',
    noSelection: '指定なし',
    fan: 'ファン',
    follower: 'フォロワー',
    subscriber: '登録者',
    network: {
        stable: 'インターネット接続ができました',
        unstable: 'インターネット接続状況が不安定です',
        unavailable: 'インターネットが利用できません',
    },
    date: {
        today: '今日',
        yesterday: '昨日',
    },
    db: {
        playlists: 'プレイリスト',
        albums: 'アルバム',
        rooms: 'ルーム',
        videos: '動画',
        images: '画像',
        menus: 'メニュー',
        guild: 'ギルド',
        room: 'ルーム',
        community: 'コミュニティー',
        creatorCommunity: 'クリエイターコミュニティー',
        audience: '視聴者',
    },
};
