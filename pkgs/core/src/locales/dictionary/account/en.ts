export const accountEnDictionary = {
    attr: {
        username: 'Username',
        privacyPolicy: 'Privacy Policy',
        terms: 'Terms of Service',
    },
    authentication: {
        title: 'Log in',
        submit: 'Next',
    },
    verify: {
        title: 'Verify Login',
        headline: 'Verify login attempt',
        description: 'Wait until trusted device allow your login attempt.',
        resend: 'Resend request',
        submit: 'Next',
    },
    new: {
        title: 'Create Account',
        agree: (thing: string) => `I accept ${thing}`,
    },
    loading: {
        headline: 'Loading…'
    },
};
