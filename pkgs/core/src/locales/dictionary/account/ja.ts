export const accountJaDictionary = {
    attr: {
        username: 'ユーザー名',
        privacyPolicy: 'プラバシーボリシー',
        terms: '利用規約',
    },
    authentication: {
        title: 'ログイン',
        submit: '次へ',
    },
    verify: {
        title: 'ログイン検証',
        headline: 'このログインを許可しましょう',
        description: '信頼した端末からこのログインが許可されるまで待ちましょう。',
        resend: 'リクエストを再送する',
        submit: '次へ',
    },
    new: {
        title: 'アカウント登録',
        agree: (thing: string) => `${thing}に同意します`,
    },
    loading: {
        headline: 'ロード中…'
    },
};