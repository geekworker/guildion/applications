import { welcomeEnDictionary } from "./en";

export const welcomeJaDictionary: typeof welcomeEnDictionary = {
    title: 'あなたの居場所を見つけよう',
    description: 'クリエイティブコミュニティーを作りましょう',
    login: 'ログイン',
    register: '会員登録',
};
