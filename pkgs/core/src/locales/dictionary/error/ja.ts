import { Account } from "@guildion/core/src/adaptors/models/Account";
import { File } from "@guildion/core/src/adaptors/models/File";
import { Guild } from "@guildion/core/src/adaptors/models/Guild";
import { Member } from "@guildion/core/src/adaptors/models/Member";
import { Role } from "@guildion/core/src/adaptors/models/Role";
import { Room } from "@guildion/core/src/adaptors/models/Room";
import { RoomCategory } from "@guildion/core/src/adaptors/models/RoomCategory";
import { User } from "@guildion/core/src/adaptors/models/User";
import { Device } from "@guildion/core/src/adaptors/models/Device";
import { errorEnDictionary } from "./en";

export const errorJaDictionary: typeof errorEnDictionary = {
    title: 'エラーが発生しました',
    common: {
        internalError: 'バグによる内部エラーが確認されました。申し訳ございません。',
        invalidRequest: 'サーバーから不当なレスポンスが返ってきました',
        invalidEmailFormat: 'メールアドレスが正しいフォーマットではありません',
        invalidURL: 'URLが正しいフォーマットではありません',
        requireDevice: '端末情報は必須です',
        requireAccessToken: 'アクセストークンは必須です',
        networkError: 'インターネットに接続できませんでした',
    },
    account: {
        invalidAuthenticate: 'ユーザー名またはパスワードが正しくないです',
        sessionLockedError: `現在このアカウントは他端末からのログインを許可していません`,
        notLoginError: `ログインしてください`,
        password: {
            invalidCharactersRange: `パスワードは${Account.passwordMinLimit}文字から${Account.passwordMaxLimit}文字までです`,
            invalidSpace: 'パスワードに空白文字を含めることはできません',
            invalidFormat: 'パスワードは半角英数字・記号のみ有効です',
        },
        username: {
            alreadyExists: 'このユーザー名はすでに使用されています',
            invalidCharactersRange: `ユーザー名は${User.usernameMinLimit}文字から${User.usernameMaxLimit}文字までです`,
            invalidSpace: 'ユーザー名に空白文字を含めることはできません',
            invalidFormat: 'ユーザー名は半角英数字・アンダースコアのみ有効です',
        },
    },
    user: {
        notExists: 'アカウントが存在しません',
        lockedError: 'アカウントは凍結されました',
        deletedError: 'アカウントは削除済みです',
        ownerNotDeletableError: 'オーナーであるギルドがあるためアカウントは削除できませんでした',
        displayName: {
            invalidCharactersRange: `アカウントの表示名は${User.displayNameMinLimit}文字から${User.displayNameMaxLimit}文字までです`,
        },
    },
    device: {
        requireDevice: '端末情報は必須です',
        relatedError: 'この端末はまだこのアカウントと関連付けられていません',
        trustedError: 'この端末はまだこのアカウントに信頼されていません',
        lockedError: 'この端末はサーバーからロックされています',
        blockedError: 'この端末はこのアカウントからブロックされています',
        deletedError: 'この端末の情報はサーバーから削除されています',
        maxSessionError: `この端末に関連付けられるアカウントの上限数は${Device.accountMaxLimit}までです`,
    },
    connection: {
        notExists: '接続情報が存在しません',
        disconnecedError: 'この端末はすでにサーバーと切断されています',
    },
    accessToken: {
        invalidToken: '不正なリクエストです',
    },
    csrf: {
        invalidToken: '不正なリクエストです',
    },
    otp: {
        invalidLoginInformation: 'この端末のログイン用のコードが発行されていません',　
        invalidLoginCode: 'ログイン用の認証コードは正しくありません',
    },
    guild: {
        notExists: 'ギルドが存在しません',
        lockedError: 'このギルドは凍結されています',
        deletedError: 'このギルドはすでに削除されています',
        permitionError: 'このギルドはメンバーに直接招待してもらうことで参加できます',
        alreadyJoinedError: 'あなたはすでにこのギルドに参加しています',
        displayName: {
            invalidCharactersRange: `ギルドの表示名は${Guild.displayNameMinLimit}文字から${Guild.displayNameMaxLimit}文字までです`,
        },
    },
    invite: {
        notExists: '招待が存在しません',
        lockedError: 'この招待は凍結されています',
        deletedError: 'この招待はすでに削除されています',
        expiredError: 'この招待は有効期限切れです',
        limitGuildError: 'この招待からギルドに参加できるメンバー数の上限に達しました',
        limitRoomError: 'この招待からルームに参加できるメンバー数の上限に達しました',
    },
    member: {
        notExists: 'メンバーが存在しません',
        lockedError: 'このメンバーは凍結されています',
        deletedError: 'このメンバーはすでに削除されています',
        bannedError: 'このメンバーはギルドからBANされています',
        guestError: 'このメンバーはゲストです',
        ownerValidationError: 'このメンバーはこのギルドのオーナーです',
        roomOwnerValidationError: 'このメンバーはこのメンバーのオーナーです',
        notOwnerError: 'あなたはこのギルドのオーナーではありません',
        notRoomOwnerError: 'あなたはこのルームのオーナーではありません',
        displayName: {
            invalidCharactersRange: `メンバーの表示名は${Member.displayNameMinLimit}文字から${Member.displayNameMaxLimit}文字までです`,
        },
    },
    room: {
        notExists: 'ルームが存在しません',
        lockedError: 'このルームは凍結されています',
        deletedError: 'このルームはすでに削除されています',
        permitionError: 'このルームはメンバーに直接招待してもらうことで参加できます',
        bannedError: 'このルームはギルドからBANされています',
        alreadyJoinedError: 'あなたはすでにこのルームに参加しています',
        displayName: {
            invalidCharactersRange: `ルームの表示名は${Room.displayNameMinLimit}文字から${Room.displayNameMaxLimit}文字までです`,
        },
    },
    role: {
        notExists: 'ロールが存在しません',
        lockedError: 'このメンバーは凍結されています',
        permissionError: '権限がありません',
        notDeletableError: 'ロールは削除できません',
        displayName: {
            invalidCharactersRange: `ロールの表示名は${Role.displayNameMinLimit}文字から${Role.displayNameMaxLimit}文字までです`,
        },
        room: {
            noCreatableError: 'ルーム作成権限はありません',
            noManagableError: 'ルーム管理権限はありません',
            noInvitableError: '招待権限はありません',
            noViewableError: 'このルームの閲覧権限はありません',
            noSendableError: 'このルームでのメッセージ送信権限はありません',
            noPostableError: 'このルームでの投稿権限はありません',
        },
        guild: {
            noInvitableError: '招待権限はありません',
        },
    },
    widget: {
        notExists: 'ウィジェットは存在しません',
        lockedError: 'ウィジェットは停止されています',
        deletedError: 'ウィジェットは削除済みです',
    },
    file: {
        alreadyExists: 'ファイルはすでに存在します',
        notExists: 'ファイルは存在しません',
        lockedError: 'ファイルは停止されています',
        deletedError: 'ファイルは削除済みです',
        failUploadError: 'ファイルアップロードに失敗しました',
        noUploadError: 'このファイルはアップロードされていません',
        archivedError: 'ファイルはアーカイブ済みです',
        uploadableError: 'このファイルはアップロードできません',
        notVideoError: 'このファイルは動画ではありません',
        profileTypeError: 'このファイルはプロフィール画像にできません',
        displayName: {
            invalidCharactersRange: `ファイルの表示名は${File.displayNameMinLimit}文字から${File.displayNameMaxLimit}文字までです`,
        },
        providerKey: {
            invalidKeyError: 'ファイルのキー指定は正しくありません',
        },
        youtube: {
            invalidFormatError: 'このファイルはYouTubeではありません',
        },
    },
    folder: {
        notExists: 'フォルダは存在しません',
        lockedError: 'フォルダは停止されています',
        deletedError: 'フォルダは削除済みです',
        failUploadError: 'フォルダアップロードに失敗しました',
        notPlaylistError: 'フォルダはプレイリストではありません',
        notAlbumError: 'フォルダはアルバムではありません',
        displayName: {
            invalidCharactersRange: `フォルダの表示名は${File.displayNameMinLimit}文字から${File.displayNameMaxLimit}文字までです`,
        },
        playlist: {
            fileFormatError: 'プレイリストは動画ファイルのみ追加可能です',
        },
        album: {
            fileFormatError: 'アルバムは写真ファイルのみ追加可能です',
        },
    },
    storage: {
        alreadyExists: 'ストレージはすでに存在します',
        notExists: 'ストレージは存在しません',
        lockedError: 'ストレージは停止されています',
        deletedError: 'ストレージは削除済みです',
        invalidKeyError: 'ストレージ指定が正しくないです',
        failUploadError: 'ファイルアップロードに失敗しました',
        maxSizeError: 'ギルドのストレージの容量が不足しています',
    },
    message: {
        requiredError: 'メッセージは必須です',
        alreadyExists: 'メッセージはすでに存在します',
        notExists: 'メッセージは存在しません',
        deletedError: 'メッセージは削除済みです',
        archivedError: 'メッセージはアーカイブ済みです',
        body: {
            emptyError: 'メッセージは必須です',
        },
    },
    post: {
        requiredError: '投稿は必須です',
        alreadyExists: '投稿はすでに存在します',
        notExists: '投稿は存在しません',
        deletedError: '投稿は削除済みです',
        archivedError: '投稿はアーカイブ済みです',
        body: {
            emptyError: '投稿は必須です',
        },
    },
    postComment: {
        noCommentableError: 'この投稿にコメントはできません',
        requiredError: 'コメントは必須です',
        notExists: 'コメントは存在しません',
        deletedError: 'コメントは削除済みです',
        body: {
            emptyError: 'コメントは必須です',
        },
    },
    roomCategory: {
        requiredError: 'カテゴリーは必須です',
        notExists: 'カテゴリーは存在しません',
        deletedError: 'カテゴリーは削除済みです',
        name: {
            invalidCharactersRange: `カテゴリーは${RoomCategory.nameMinLimit}文字から${RoomCategory.nameMaxLimit}文字までです`,
        },
    },
    guildCategory: {
        requiredError: 'カテゴリーは必須です',
        notExists: 'カテゴリーは存在しません',
        deletedError: 'カテゴリーは削除済みです',
    },
    category: {
        requiredError: 'カテゴリーは必須です',
        notExists: 'カテゴリーは存在しません',
        deletedError: 'カテゴリーは削除済みです',
    },
    reaction: {
        requiredError: 'リアクションは必須です',
        notExists: 'リアクションは存在しません',
        deletedError: 'リアクションは削除済みです',
        archivedError: 'リアクションはアーカイブ済みです',
    },
};
