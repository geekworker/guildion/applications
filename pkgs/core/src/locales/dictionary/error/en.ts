import { Account } from "@guildion/core/src/adaptors/models/Account";
import { Guild } from "@guildion/core/src/adaptors/models/Guild";
import { Member } from "@guildion/core/src/adaptors/models/Member";
import { Role } from "@guildion/core/src/adaptors/models/Role";
import { Room } from "@guildion/core/src/adaptors/models/Room";
import { User } from "@guildion/core/src/adaptors/models/User";
import { File } from "@guildion/core/src/adaptors/models/File";
import { RoomCategory } from "@guildion/core/src/adaptors/models/RoomCategory";
import { Device } from "@guildion/core/src/adaptors/models/Device";

export const errorEnDictionary = {
    title: 'An error has occurred',
    common: {
        internalError: 'Oops, INTERNAL ERROR BY SOME BUGS',
        invalidRequest: 'Invalid response from server',
        invalidEmailFormat: 'Email is not correct format',
        invalidURL: 'URL is not correct format',
        requireDevice: 'Require device information',
        requireAccessToken: 'Require access token',
        networkError: 'Couldn\'t connect with network',
    },
    account: {
        invalidAuthenticate: 'Username or Password is incorrect',
        sessionLockedError: `This account doesn't allow to login from other device now`,
        notLoginError: `Please login`,
        password: {
            invalidCharactersRange: `Password should be from ${Account.passwordMinLimit} to ${Account.passwordMaxLimit} characters`,
            invalidSpace: 'Password should not contain any whitespace characters',
            invalidFormat: 'Password should be half-width alphanumeric characters or symbols',
        },
        username: {
            alreadyExists: 'This username is already exists',
            invalidCharactersRange: `Username should be from ${User.usernameMinLimit} to ${User.usernameMaxLimit} characters`,
            invalidSpace: 'Username should not contain any whitespace characters',
            invalidFormat: 'Username should be half-width alphanumeric characters or underscore',
        },
    },
    user: {
        notExists: 'Account is not exists',
        lockedError: 'This account was suspended',
        deletedError: 'This account was already deleted',
        ownerNotDeletableError: 'Failed to delete account because you are one or more guild owners',
        displayName: {
            invalidCharactersRange: `Display name should be from ${User.displayNameMinLimit} to ${User.displayNameMaxLimit} characters`,
        },
    },
    device: {
        requireDevice: 'Require device information',
        relatedError: 'This device is not related with this account',
        trustedError: 'This device is not trusted from this account',
        lockedError: 'This device was suspended from server',
        blockedError: 'This device was blocked from this account',
        deletedError: 'This device was already deleted from server',
        maxSessionError: `The limit number of accounts related with this device is ${Device.accountMaxLimit}`,
    },
    connection: {
        notExists: 'Connection information is not exists',
        disconnecedError: 'This device is already disconnected with server',
    },
    accessToken: {
        invalidToken: 'Invalid request',
    },
    csrf: {
        invalidToken: 'Invalid request',
    },
    otp: {
        invalidLoginInformation: 'No code has been issued to log in to this device',
        invalidLoginCode: 'The verification code for login is incorrect',
    },
    guild: {
        notExists: 'Guild is not exists',
        lockedError: 'This guild was suspended',
        deletedError: 'This guild was already deleted',
        permitionError: 'You can join to this guild by getting invitation',
        alreadyJoinedError: 'You have already joined this guild',
        displayName: {
            invalidCharactersRange: `Display name should be from ${Guild.displayNameMinLimit} to ${Guild.displayNameMaxLimit} characters`,
        },
    },
    invite: {
        notExists: 'Invite is not exists',
        lockedError: 'This invite was suspended',
        deletedError: 'This invite was already deleted',
        expiredError: 'This invite was expired',
        limitGuildError: 'Reach limit number of members can join this guild from this invite',
        limitRoomError: 'Reach limit number of members can join this room from this invite',
    },
    member: {
        notExists: 'Member is not exists',
        lockedError: 'This member was suspended',
        deletedError: 'This member was already deleted',
        bannedError: 'This member is banned from the guild',
        guestError: 'This member is guest',
        ownerValidationError: 'This member is owner of this guild',
        roomOwnerValidationError: 'This member is owner of this room',
        notOwnerError: 'You are not owner of this guild',
        notRoomOwnerError: 'You are not owner of this room',
        displayName: {
            invalidCharactersRange: `Display name should be from ${Member.displayNameMinLimit} to ${Member.displayNameMaxLimit} characters`,
        },
    },
    room: {
        notExists: 'Room is not exists',
        lockedError: 'This room was suspended',
        deletedError: 'This room was already deleted',
        permitionError: 'You can join to this room by getting invitation',
        bannedError: 'This room is banned from the guild',
        alreadyJoinedError: 'You have already joined this room',
        displayName: {
            invalidCharactersRange: `Display name should be from ${Room.displayNameMinLimit} to ${Room.displayNameMaxLimit} characters`,
        },
    },
    role: {
        notExists: 'Role is not exists',
        lockedError: 'This role was suspended',
        permissionError: 'You have no permission',
        notDeletableError: 'This role can not delete',
        displayName: {
            invalidCharactersRange: `Display name should be from ${Role.displayNameMinLimit} to ${Role.displayNameMaxLimit} characters`,
        },
        room: {
            noCreatableError: 'You have no permission to create room',
            noManagableError: 'You have no permission to manage room',
            noInvitableError: 'You have no permission to invite',
            noViewableError: 'You have no permission to view this room',
            noSendableError: 'You have no permission to send message',
            noPostableError: 'You have no permission to post in this room',
        },
        guild: {
            noInvitableError: 'You have no permission to invite',
        },
    },
    widget: {
        notExists: 'Widget is not exists',
        lockedError: 'This widget was suspended',
        deletedError: 'This widget has already deleted',
    },
    file: {
        alreadyExists: 'File is already exists',
        notExists: 'File is not exists',
        lockedError: 'This file was suspended',
        deletedError: 'This file has already deleted',
        failUploadError: 'Failed to upload file',
        noUploadError: 'This file has not uploaded yet',
        archivedError: 'This file has already archived',
        uploadableError: 'This file is not uploadable',
        notVideoError: 'This file is not video',
        profileTypeError: 'This file is invalid for profile',
        displayName: {
            invalidCharactersRange: `Display name should be from ${File.displayNameMinLimit} to ${File.displayNameMaxLimit} characters`,
        },
        providerKey: {
            invalidKeyError: 'This file key is invalid',
        },
        youtube: {
            invalidFormatError: 'This file is not YouTube',
        },
    },
    folder: {
        notExists: 'Folder is not exists',
        lockedError: 'This folder was suspended',
        deletedError: 'This folder has already deleted',
        failUploadError: 'Failed to upload folder',
        notPlaylistError: 'This folder is not playlist',
        notAlbumError: 'This folder is not album',
        displayName: {
            invalidCharactersRange: `Display name should be from ${File.displayNameMinLimit} to ${File.displayNameMaxLimit} characters`,
        },
        playlist: {
            fileFormatError: 'Playlist can add only video file',
        },
        album: {
            fileFormatError: 'Playlist can add only image file',
        },
    },
    storage: {
        alreadyExists: 'Storage is already exists',
        notExists: 'Storage is not exists',
        lockedError: 'This storage was suspended',
        deletedError: 'This storage has already deleted',
        invalidKeyError: 'This storage key is invalid',
        failUploadError: 'Failed to upload file to storage',
        maxSizeError: 'The guild is out of storage space',
    },
    message: {
        requiredError: 'Message is required',
        alreadyExists: 'Message is already exists',
        notExists: 'Message is not exists',
        deletedError: 'This message has already deleted',
        archivedError: 'This message has already archived',
        body: {
            emptyError: 'Message is required',
        },
    },
    post: {
        requiredError: 'Post is required',
        alreadyExists: 'Post is already exists',
        notExists: 'Post is not exists',
        deletedError: 'This post has already deleted',
        archivedError: 'This post has already archived',
        body: {
            emptyError: 'Post is required',
        },
    },
    postComment: {
        noCommentableError: 'This post not allow to comment',
        requiredError: 'Comment is required',
        notExists: 'Comment is not exists',
        deletedError: 'This comment has already deleted',
        body: {
            emptyError: 'Comment is required',
        },
    },
    roomCategory: {
        requiredError: 'Category is required',
        notExists: 'Category is not exists',
        deletedError: 'This category has already deleted',
        name: {
            invalidCharactersRange: `Category should be from ${RoomCategory.nameMinLimit} to ${RoomCategory.nameMaxLimit} characters`,
        },
    },
    guildCategory: {
        requiredError: 'Category is required',
        notExists: 'Category is not exists',
        deletedError: 'This category has already deleted',
    },
    category: {
        requiredError: 'Category is required',
        notExists: 'Category is not exists',
        deletedError: 'This category has already deleted',
    },
    reaction: {
        requiredError: 'Reaction is required',
        notExists: 'Reaction is not exists',
        deletedError: 'This reaction has already deleted',
        archivedError: 'This reaction has already archived',
    },
};
