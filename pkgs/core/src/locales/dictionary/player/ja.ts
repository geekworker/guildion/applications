import { playerEnDictionary } from "./en";

export const playerJaDictionary: typeof playerEnDictionary = {
    tabs: {
        playNext: '再生リスト',
        detail: '詳細',
    },
};
