export const notificationEnDictionary = {
    otp: {
        yourOTPLogin: 'Your Two-Factor Sign In Code',
        yourOTPResetPassword: 'Your Confirmation Code For Reset Password',
    },
};
