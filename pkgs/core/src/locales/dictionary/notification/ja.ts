import { notificationEnDictionary } from "./en";

export const notificationJaDictionary: typeof notificationEnDictionary = {
    otp: {
        yourOTPLogin: 'ログイン用二段階認証コード',
        yourOTPResetPassword: 'パスワードリセット用二段階認証コード',
    },
};