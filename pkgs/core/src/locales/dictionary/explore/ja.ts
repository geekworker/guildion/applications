import { exploreEnDictionary } from "./en";

export const exploreJaDictionary: typeof exploreEnDictionary = {
    attr: {
        results: '検索結果',
        rooms: 'ルーム',
        guilds: 'ギルド',
        posts: '投稿',
    },
    name: 'エクスプローラー',
    title: 'ようこそ。新たなクリエイティブワールドへ。',
    description: 'ギルドを作って、先駆者になりましょう。',
    placeholder: 'ギルド・ルーム・投稿を検索',
    notfound: {
        title: '検索結果なし',
        description: 'コンテンツが見つかりませんでした。'
    },
};
