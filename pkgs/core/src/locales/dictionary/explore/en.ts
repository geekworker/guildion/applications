export const exploreEnDictionary = {
    attr: {
        results: 'Results',
        rooms: 'Rooms',
        guilds: 'Guilds',
        posts: 'Posts',
    },
    name: 'Explore',
    title: 'Welcome to new creative world',
    description: 'Let become a pioneer',
    placeholder: 'Search guilds・rooms・posts',
    notfound: {
        title: 'NotFound',
        description: 'sorry for there is no contents.'
    },
};
