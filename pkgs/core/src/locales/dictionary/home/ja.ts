import { homeEnDictionary } from "./en";

export const homeJaDictionary: typeof homeEnDictionary = {
    seo: {
        title: '動画・画像特化型コミュニティーアプリ | Guildion',
        description: 'Guildionはクリエイターに特化したコミュニティーアプリです。ギルドを作って招待するだけでクリエイターの活動が広がります。多彩なルーム機能を使ってクリエイターのコンテンツをメンバー同士でリアルタイムに楽しんでください。',
    },
    attr: {
        donwloads: 'ダウンロード',
        seeMore: '詳細を見る',
    },
    page: {
        title: 'コンテンツをもっと楽しく\n',
        description: '動画に特化したあなただけのコミュニティーを作りファンを盛り上げましょう',
        comment: 'フォロワー・視聴者・ファン専用コミュニティー',
        headline: {
            title: 'Guildionで新しいクリエイターライフを',
            description: 'Guildionならクリエイターとファンの一体化を実現することができます。\nこれが大きな化学反応を生むことになります。\nそして新しい世界を作るのがこんなに簡単なことだっととはと自覚することになるでしょう。',
            story1: {
                label: '特徴 1',
                title: 'ギルドを作って招待をするだけ',
                comment: '特別な準備は不必要',
            },
            story2: {
                label: '特徴 2',
                title: 'ルーム機能で\nコンテンツを\nもっと魅力的に',
                comment: 'コンテンツを楽しむ新しい手段',
            },
            story3: {
                label: '特徴 3',
                title: 'メンバーが\nクリエイターを\n拡散できます',
                comment: '活動をより大規模に',
            },
            story4: {
                label: '特徴 4',
                title: 'メンバーは\nクリエイターの収益に\n貢献できます',
                comment: '新しいクリエイターライフを',
            },
        },
        guild: {
            title: 'あなただけのギルドを作りましょう',
            description: 'ギルドは動画中心のコミュニティーです',
            story1: {
                label: 'ギルドを新規作成',
                title: 'ニーズに合わせてギルドのタイプを選択してください',
            },
            story2: {
                label: 'クリエイターとファン専用',
                title: 'クリエイターギルド',
                description: 'クリエイターとそのファンのための最適化がしてあります',
            },
            story3: {
                label: '非クリエイターでも大丈夫',
                title: 'メンバーズギルド',
                description: 'どなたでもご利用いただけるプランです',
            },
            story4: {
                label: 'ルームを探す/作る',
                title: '様々なルームでギルドを盛り上げましょう',
            },
            rooms: [
                {
                    name: '視聴ルーム1',
                    log: 'この動画見ない？',
                },
                {
                    name: '視聴ルーム2',
                    log: 'この動画いいね！',
                },
                {
                    name: 'フリールーム',
                    log: 'この画像どこからダウンロードした？',
                },
                {
                    name: '仕事部屋',
                    log: 'とりま、このBGM流しとこ',
                },
                {
                    name: '勉強部屋',
                    log: 'この動画、勉強になるよ',
                },
                {
                    name: 'チャットルーム',
                    log: 'あの動画、誰かアップロードしておいて',
                },
            ],
        },
        room: {
            title: 'メンバーはギルド内に好きなルームを作成できます',
            description: 'メンバーは好きなメンバー同士でコミュニケーションが取れます',
            introduce: 'ようこそ、Guildionのルーム機能へ。',
            features: [
                {
                    title: '同期プレイヤー',
                    description: '同じルームにいるメンバーとリアルタイムで動画・画像の視聴が可能です。\nお気に入りのコンテンツや話し合いたい内容をメンバーで共有して、他の場所では不可能だった実りあるトークをしましょう。',
                },
                {
                    title: 'チャット',
                    description: '他のサービス同様にテキストチャットができます。\nただし一つ違うのは、好きなメンバーと好きな動画・画像を視聴しながら、という理想的な環境の中で好きなトークに花を咲かせることができる点です。',
                },
                {
                    title: '投稿機能',
                    description: '投稿をすることで、魅力的なコンテンツを、ルーム外の人たちにオススメすることができます。\nこの機能によりより多くの人にコンテンツをリーチさせることができます。',
                },
                {
                    title: 'ボイスチャット',
                    description: '好きなメンバーと好きな動画・画像を楽しみながら、音声でのやり取りが可能です。\n新しいインターネットライフが待っています。',
                },
                {
                    title: 'リアクション',
                    description: '絵文字だけでメンバーとリアルタイムでリアクションを共有することができます。\n音声やテキストでのやり取りが面倒な場合でも、最小限の方法でルームを楽しむことができます。',
                },
                {
                    title: 'スーパーメンション',
                    description: 'ギルドの運営者やクリエイターに対して、あなたの好きな課金分でメッセージを直接届けることが可能です。\nたとえライブ配信をしていなくても、あなたの好きなペースでです。',
                },
            ],
        },
        footer: {
            title: 'クリエイターコミュニティーサービス',
            description: 'Guildionはファン同士をアクティブにするツールによりクリエイターをサポートします。\nぜひ彼らとの化学反応を生み出して行ってください。',
        },
    },
    footer: {
        service: {
            name: 'Guildion',
            site: 'Guildionとは',
            pricing: '料金プラン',
            premium: 'プレミアム',
        },
        platform: {
            name: 'プラットフォーム',
            ios: 'iOS',
            android: 'Android',
        },
        // forUsers: {
        //     name: 'ユーザー向け'
        // },
        forCreators: {
            name: 'クリエイター向け',
            premium: 'プレミアムギルド',
        },
        support: {
            name: 'サポート',
            contact: 'お問い合わせ'
        },
        legals: {
            name: '法務',
            privacyPolicy: 'プライバシーポリシー',
            termsOfUse: '利用規約',
            actOnSpecifiedCommercialTransactions: "特定商品取引法に基づく表記",
            actOnSpecifiedCommercialTransactionsInformation: "特定商品取引法に基づく表記",
        },
        connect: {
            name: 'つながる',
        },
    },
};
