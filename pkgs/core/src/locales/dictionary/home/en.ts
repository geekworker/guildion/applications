export const homeEnDictionary = {
    seo: {
        title: 'Creator Community App | Guildion',
        description: 'Guildion is Creator Community Application. You only have to do is to create guild and invite member in you SNS. Guildion room feature will make creator contents more joyful by connecting realtime with each members.',
    },
    attr: {
        donwloads: 'Download',
        seeMore: 'See more',
    },
    page: {
        title: 'The Easiest Way to Build\n',
        description: 'Build your video community and activate your audience',
        comment: 'For Follower, Subscriber, Fan',
        headline: {
            title: 'New creator life with Guildion',
            description: 'Guildion can realize to make one with creator and its fans.\nIt will cause a big chemical reaction.\nAnd you will find out how easy it is for you to create a new world.',
            story1: {
                label: 'FEATURE 1',
                title: 'Create Guild\nand\ninvite members',
                comment: 'Don\'t have to ready anything',
            },
            story2: {
                label: 'FEATURE 2',
                title: 'Room makes\nyour contents\nmore attractive',
                comment: 'New way for enjoy with contents',
            },
            story3: {
                label: 'FEATURE 3',
                title: 'Member can\npromote more\ncreator activity',
                comment: 'Broaden your activities',
            },
            story4: {
                label: 'FEATURE 4',
                title: 'Members can\ncontribute to\nprofit of creator',
                comment: 'Let enjoy more good creator life',
            },
        },
        guild: {
            title: 'Build Your Creative Guild',
            description: 'This is new video community and fan club',
            story1: {
                label: 'Create New Guild',
                title: 'Select guild type of suiting with your needs',
            },
            story2: {
                label: 'For creator and its fans',
                title: 'Creator Guild',
                description: 'Creator Guild has many optimize for video/illust creator and its fans',
            },
            story3: {
                label: 'If you are not creator',
                title: 'Members Guild',
                description: 'Anyone can use powerful community tools',
            },
            story4: {
                label: 'Create/Explore new room',
                title: 'Let make guild activate by various of rooms',
            },
            rooms: [
                {
                    name: 'Watch Room 1',
                    log: 'Let watch this video',
                },
                {
                    name: 'Watch Room 2',
                    log: 'What a great video !',
                },
                {
                    name: 'Free Room',
                    log: 'Thank you',
                },
                {
                    name: 'Task Room',
                    log: 'Let listen this bgm while this task',
                },
                {
                    name: 'Study Room',
                    log: 'This video is useful',
                },
                {
                    name: 'Chat Room',
                    log: 'OK',
                },
            ],
        },
        room: {
            title: 'Member can create Room freely',
            description: 'The members can communicate with each other',
            introduce: 'Welcome to Room Feature',
            features: [
                {
                    title: 'Sync Vision',
                    description: 'You can watch videos and images in real time with members in the same room.\nShare your favorite content and what you want to discuss with your members and have a wonderful talk that wasn\'t possible elsewhere.',
                },
                {
                    title: 'Text Chat',
                    description: 'You can use text chat like any other service.\nHowever, one difference is that you can make your favorite talk bloom in the ideal environment of watching your favorite videos and images with your favorite members.',
                },
                {
                    title: 'Post Feature',
                    description: 'This feature enable you to recommend attractive content to people outside the room.\nThis feature allows you to reach more people with your content.',
                },
                {
                    title: 'Voice Chat',
                    description: 'You can communicate by voice while enjoying your favorite videos and images with your favorite members.\nA new internet life is waiting for you.',
                },
                {
                    title: 'Reaction',
                    description: 'You can share your reaction with members in real time with just emoji. You can enjoy your room in a minimal way, even if you don\'t want to interact with voice or text.',
                },
                {
                    title: 'Super Mention',
                    description: 'You can send messages directly to guild operators and creators for your favorite billing at your own pace.\nEven if they don\'t start not livestreaming.',
                },
            ],
        },
        footer: {
            title: 'Creator Community Service',
            description: 'Guildion supports creators with tools to activate fans, followers and subscribers.\nDiscover all chemical reactions with them',
        },
    },
    footer: {
        service: {
            name: 'Guildion',
            site: 'About Guildion',
            pricing: 'Pricing',
            premium: 'Premium',
        },
        platform: {
            name: 'Platform',
            ios: 'iOS',
            android: 'Android',
        },
        // forUsers: {
        //     name: 'For Users',
        //     policies: 'Policies',
        // },
        forCreators: {
            name: 'For Creators',
            premium: 'Premium Guild',
        },
        support: {
            name: 'Support',
            contact: 'Contact',
        },
        legals: {
            name: 'Legal',
            privacyPolicy: 'Privacy Policy',
            termsOfUse: 'Terms of use',
            actOnSpecifiedCommercialTransactions: "Specified Commercial Transactions",
            actOnSpecifiedCommercialTransactionsInformation: "Information Provided Pursuant to the Act on Specified Commercial Transactions",
        },
        connect: {
            name: 'Connect',
        },
    },
};
