export const blogEnDictionary = {
    description: 'Enter deep any knowledge with this blog',
    attr: {
        googleAnalytics: 'Google Analytics',
        index: 'Contents',
        source: 'Source',
    },
    home: {
        title: 'Newest Blogs',
    },
    footer: {
        googleAnalytics: 'This site is using Google Analytics',
    },
};