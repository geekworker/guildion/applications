import { blogEnDictionary } from "./en";

export const blogJaDictionary: typeof blogEnDictionary = {
    description: 'とりあえず知識を深堀りするブログ',
    attr: {
        googleAnalytics: 'Google Analytics',
        index: '目次',
        source: '引用元',
    },
    home: {
        title: '最新の記事一覧',
    },
    footer: {
        googleAnalytics: 'このサイトはGoogle Analyticsを使用しています',
    },
}