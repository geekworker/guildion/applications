export const roomEnDictionary = {
    attr: {
        roomname: 'Room Name',
    },
    db: {
        connectingCounter: (count: number) => `${count} connecting`,
        membersCounter: (count: number) => `${count} members`,
        postsCounter: (count: number) => `${count} posts`,
        defaultName: (name: string) => `${name}'s Room`,
    },
    home: {
        create: 'Create Room',
        explore: 'Explore Room',
        empty: {
            title: 'There is no room yet',
            description: 'Please create room.'
        },
        notJoin: {
            title: 'There is no room yet',
            description: 'Please create or join room.'
        },
    },
    show: {
        syncVision: {
            emptyTitle: 'No media sharing now',
            selectTitle: 'Select media',
        },
        logs: {
            title: 'Activities',
            emptyText: 'There is no activities yet.',
        },
        widgets: {
            title: 'Widgets',
        },
        controls: {
            voice: 'Voice',
            mute: 'Mute',
            reaction: 'Reaction',
            invite: 'Invite',
            file: 'Create',
        },
        join: 'Join',
        member: 'Member',
    },
    textChat: {
        durationAutoComplete: 'Insert current play time',
    },
    mediaLibrary: {
        empty: {
            title: 'There is no media yet',
            description: 'Please create media.'
        },
    },
    posts: {
        title: 'Posts',
        show: {
            commentTitle: 'Comments',
        },
        new: {
            placeholder: 'Write comment',
        },
    },
    new: {
        welcome: {
            title: 'Create New Room',
            description: 'Room is your place to communicate with members.',
        },
        input: {
            title: 'Create New Room',
        },
        tutorial: {
            invite: 'Let\'s Invite your friend to your new room',
        },
    },
};
