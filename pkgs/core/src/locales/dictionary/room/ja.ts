import { roomEnDictionary } from "./en";

export const roomJaDictionary: typeof roomEnDictionary = {
    attr: {
        roomname: 'ルーム名',
    },
    db: {
        connectingCounter: (count: number) => `${count}人接続中`,
        membersCounter: (count: number) => `${count}メンバー`,
        postsCounter: (count: number) => `${count}件の投稿`,
        defaultName: (name: string) => `${name}ルーム`,
    },
    home: {
        create: 'ルーム作成',
        explore: 'ルームを探す',
        empty: {
            title: 'ルームがまだありません',
            description: 'ルームを作成しましょう'
        },
        notJoin: {
            title: 'ルームがまだありません',
            description: 'ルームを作成・探索しましょう'
        },
    },
    show: {
        syncVision: {
            emptyTitle: '現在同期再生はされていません',
            selectTitle: 'メディアを選択',
        },
        logs: {
            title: 'アクティビティー',
            emptyText: 'まだアクティビティーはありません',
        },
        widgets: {
            title: 'ウィジェット',
        },
        controls: {
            voice: 'ボイス',
            mute: 'ミュート',
            reaction: 'リアクション',
            invite: '招待',
            file: 'メディアを追加',
        },
        join: '参加',
        member: 'メンバー',
    },
    textChat: {
        durationAutoComplete: '現在の再生時刻を追加',
    },
    mediaLibrary: {
        empty: {
            title: 'メディアがまだありません',
            description: 'メディアを作成しましょう'
        },
    },
    posts: {
        title: '投稿一覧',
        show: {
            commentTitle: 'コメント',
        },
        new: {
            placeholder: 'コメントを書く',
        },
    },
    new: {
        welcome: {
            title: 'ルーム新規作成',
            description: 'ルームはメンバーとの交流場所です。',
        },
        input: {
            title: 'ルーム新規作成',
        },
        tutorial: {
            invite: '新しいルームにメンバーを招待しましょう！',
        },
    },
};
