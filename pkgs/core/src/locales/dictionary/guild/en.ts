export const guildEnDictionary = {
    attr: {
        guildname: 'Guild Name',
        members: 'Members',
    },
    db: {
        defaultName: (name: string) => `${name}'s Guild`,
    },
    home: {
        create: 'Create Guild',
        dashboard: 'Dashboard',
        empty: {
            title: 'There is no guild yet',
            description: 'Please create guild.'
        },
    },
    files: {
        index: {
            title: 'Guild Media Library',
            listTitle: 'Recently Featured',
            searchPlaceholder: 'Search files in playlist, album and room',
        },
    },
    new: {
        welcome: {
            title: 'Create New Guild',
            description: 'Guild is your creator community. Let\' build new internet group.',
        },
        input: {
            title: 'Create New Guild',
        },
        tutorial: {
            roomNew: 'Let Create Your First Room',
        },
    },
    invite: {
        urlTitle: 'Invite URL',
        settingTitle: 'Invite Setting',
        expire: {
            title: 'Expire',
            none: '∞',
            d7: '7 day',
            d1: '1 day',
            hd1: '12 hours',
        },
        usersLimit: {
            title: 'Capacity of member',
            none: '∞',
        },
        share: {
            title: (name: string) => `${name} invitation`,
            message: (name: string) => `Let's join ${name} by this invitation`,
        },
    },
};
