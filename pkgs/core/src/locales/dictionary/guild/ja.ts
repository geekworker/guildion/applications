import { guildEnDictionary } from "./en";

export const guildJaDictionary: typeof guildEnDictionary = {
    attr: {
        guildname: 'ギルド名',
        members: 'メンバー',
    },
    db: {
        defaultName: (name: string) => `${name}ギルド`,
    },
    home: {
        create: 'ギルド作成',
        dashboard: 'ダッシュボード',
        empty: {
            title: 'ギルドがまだありません',
            description: 'ギルドを作成してください'
        },
    },
    files: {
        index: {
            title: 'ギルドメディアライブラリー',
            listTitle: '最近フューチャリングされたファイル',
            searchPlaceholder: 'ファイルを、プレイリスト・アルバム・ルームから探す',
        },
    },
    new: {
        welcome: {
            title: 'ギルド新規作成',
            description: 'ギルドがあなたのクリエイティブコミュニティーとなるものです。新しいネットグループを作りましょう。',
        },
        input: {
            title: 'ギルド新規作成',
        },
        tutorial: {
            roomNew: '最初のルームを作りましょう。',
        },
    },
    invite: {
        urlTitle: '招待リンク',
        settingTitle: '招待設定',
        expire: {
            title: '期限',
            none: '∞',
            d7: '7日',
            d1: '1日',
            hd1: '12時間',
        },
        usersLimit: {
            title: '定員',
            none: '∞',
        },
        share: {
            title: (name: string) => `${name}の招待`,
            message: (name: string) => `${name}に参加しましょう！`,
        },
    },
};
