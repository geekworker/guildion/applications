import { gEnDictionary } from "./g/en";
import { gJaDictionary } from "./g/ja";
import { accountEnDictionary } from "./account/en";
import { accountJaDictionary } from "./account/ja";
import { blogEnDictionary } from "./blog/en";
import { blogJaDictionary } from "./blog/ja";
import { errorEnDictionary } from "./error/en";
import { errorJaDictionary } from "./error/ja";
import { formatEnDictionary } from "./format/en";
import { formatJaDictionary } from "./format/ja";
import { playerEnDictionary } from "./player/en";
import { playerJaDictionary } from "./player/ja";
import { notificationEnDictionary } from "./notification/en";
import { notificationJaDictionary } from "./notification/ja";
import { successEnDictionary } from "./success/en";
import { successJaDictionary } from "./success/ja";
import { warningEnDictionary } from "./warning/en";
import { warningJaDictionary } from "./warning/ja";
import { welcomeEnDictionary } from "./welcome/en";
import { welcomeJaDictionary } from "./welcome/ja";
import { guildEnDictionary } from "./guild/en";
import { guildJaDictionary } from "./guild/ja";
import { roomEnDictionary } from "./room/en";
import { roomJaDictionary } from "./room/ja";
import { exploreEnDictionary } from "./explore/en";
import { exploreJaDictionary } from "./explore/ja";
import { homeEnDictionary } from "./home/en";
import { homeJaDictionary } from "./home/ja";

export type Dictionary = {
    g: typeof gEnDictionary,
    account: typeof accountEnDictionary,
    blog: typeof blogEnDictionary,
    error: typeof errorEnDictionary,
    explore: typeof exploreEnDictionary,
    format: typeof formatEnDictionary,
    home: typeof homeEnDictionary,
    notification: typeof notificationEnDictionary,
    player: typeof playerEnDictionary,
    success: typeof successEnDictionary,
    warning: typeof warningEnDictionary,
    welcome: typeof welcomeEnDictionary,
    guild: typeof guildEnDictionary,
    room: typeof roomEnDictionary,
}

export const EN_Dictionary: Dictionary = {
    g: gEnDictionary,
    account: accountEnDictionary,
    blog: blogEnDictionary,
    error: errorEnDictionary,
    explore: exploreEnDictionary,
    format: formatEnDictionary,
    home: homeEnDictionary,
    notification: notificationEnDictionary,
    player: playerEnDictionary,
    success: successEnDictionary,
    warning: warningEnDictionary,
    welcome: welcomeEnDictionary,
    guild: guildEnDictionary,
    room: roomEnDictionary,
};

export const JA_Dictionary: Dictionary = {
    g: gJaDictionary,
    account: accountJaDictionary,
    blog: blogJaDictionary,
    error: errorJaDictionary,
    explore: exploreJaDictionary,
    format: formatJaDictionary,
    home: homeJaDictionary,
    notification: notificationJaDictionary,
    player: playerJaDictionary,
    success: successJaDictionary,
    warning: warningJaDictionary,
    welcome: welcomeJaDictionary,
    guild: guildJaDictionary,
    room: roomJaDictionary,
};

export const fallbackDictionary: Dictionary = EN_Dictionary;
