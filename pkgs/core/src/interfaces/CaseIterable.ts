import { SingleHash } from "../types/SingleHash";

export interface CaseIterable<V> {
    [key: string]: V;
}

export type ValueOfCaseIterable<T extends CaseIterable<any>> = T extends CaseIterable<infer V> ? V : any;

export function allCases<V>(hash: CaseIterable<V>): V[] {
    return Object.values(hash).map((val: V) => val);
}

export function allKeys<P extends CaseIterable<ValueOfCaseIterable<P>>>(hash: P): (keyof P)[] {
    return Object.keys(hash).map((val: keyof P) => val);
}

export function allCasesHash<V>(hash: CaseIterable<V>): SingleHash<V>[] {
    const values = Object.values(hash).map((val: V) => val);
    const keys = Object.keys(hash).map((key: string) => key);
    return values.map((val: V, i: number) => {
        return {
            key: keys[i],
            value: val,
        }
    })
}

export function guardCase<T extends CaseIterable<ValueOfCaseIterable<T>>>(val: any, {
    cases,
    fallback,
}:{
    cases: T,
    fallback: ValueOfCaseIterable<T>
}): ValueOfCaseIterable<T> {
    if (!val) return fallback;
    const isValue = (v: any): v is ValueOfCaseIterable<T> => !!allCases(cases).find(c => c == v)
    return isValue(val) ? val : fallback;
}

export function guardOptionalCase<T extends CaseIterable<ValueOfCaseIterable<T>>>(val: any, {
    cases,
}:{
    cases: T,
}): ValueOfCaseIterable<T> | undefined {
    if (!val) return undefined;
    const isValue = (v: any): v is ValueOfCaseIterable<T> => !!allCases(cases).find(c => c == v)
    return isValue(val) ? val : undefined;
}

export function unwrapCase<T extends CaseIterable<ValueOfCaseIterable<T>>>(val: any, {
    cases,
}:{
    cases: T,
}): val is ValueOfCaseIterable<T> {
    if (!val) return false;
    const isValue = (v: any): v is ValueOfCaseIterable<T> => !!allCases(cases).find(c => c == v)
    return isValue(val) ? true : false;
}