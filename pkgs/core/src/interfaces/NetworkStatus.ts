import { Localizer } from '@guildion/core/src/locales';

export const NetworkStatus = {
    Stable: {
        value: 'Stable',
        translate: (localizer: Localizer) => localizer.dictionary.g.network.stable,
    },
    UnStable: {
        value: 'UnStable',
        translate: (localizer: Localizer) => localizer.dictionary.g.network.unstable,
    },
    UnAvailable: {
        value: 'UnAvailable',
        translate: (localizer: Localizer) => localizer.dictionary.g.network.unavailable,
    },
} as const;

export type NetworkStatus = typeof NetworkStatus[keyof typeof NetworkStatus];