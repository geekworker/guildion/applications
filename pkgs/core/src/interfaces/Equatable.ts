export interface Equatable<T> {
    equals(this: T, object: T): boolean
}