import { Records } from "@guildion/core/src/extension/Records";
import { Record } from "immutable";

export type RecordClass<T extends Record<any>> = new (...args: any) => T;
export type RecordsClass<T extends Records<any>> = new (...args: any) => T
// export type AttributesOfRecord<T extends Record<any>> = T extends Record<infer Attributes> ? Attributes : any;