export type SingleHash<T> = {
    key: string,
    value: T,
}