import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files, FileStatus } from "@guildion/core/src/adaptors/models/File";
import { FileType } from "@guildion/core/src/constants/FileType";
import { ContentType } from "@guildion/core/src/constants/ContentType";

export class RoomFilesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/files/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomFilesListRequestAttributes extends RequestParameters {
    id: string,
    count?: number,
    offset?: number,
    types?: FileType[],
    statuses?: FileStatus[],
    contentTypes?: ContentType[],
}

export class RoomFilesListRequest extends Record<RoomFilesListRequestAttributes>({
    id: '',
    count: undefined,
    offset: undefined,
    types: undefined,
    statuses: undefined,
}) {
    constructor(args: RoomFilesListRequestAttributes) {
        super(args);
    }
}

export interface RoomFilesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files: FileAttributes[],
    paginatable: boolean,
}

export class RoomFilesListResponse extends Record<RoomFilesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: [],
    paginatable: false,
}) {
    constructor(args: RoomFilesListResponseAttributes) {
        super(args);
    }

    public getFiles(): Files {
        const result = this.get('files');
        return !!result ? new Files(result) : new Files([]);
    }
}