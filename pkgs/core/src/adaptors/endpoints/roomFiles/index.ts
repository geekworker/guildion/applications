import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RoomFilesListRequest, RoomFilesListResponse, RoomFilesListRouter } from "./list";
import { RoomFileCreateRequest, RoomFileCreateResponse, RoomFileCreateRouter } from "./create";

export namespace RoomFilesEndpoint {
    export const List: Endpoint<RoomFilesListRouter, RoomFilesListRequest, RoomFilesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomFilesListRouter,
        request: RoomFilesListRequest,
        response: RoomFilesListResponse,
        options: { deviceRequired: true, accessTokenRequired: true, skipLogging: false, },
    }
    export type List = typeof List;

    export const Create: Endpoint<RoomFileCreateRouter, RoomFileCreateRequest, RoomFileCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomFileCreateRouter,
        request: RoomFileCreateRequest,
        response: RoomFileCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true, skipLogging: false, },
    }
    export type Create = typeof Create;
}