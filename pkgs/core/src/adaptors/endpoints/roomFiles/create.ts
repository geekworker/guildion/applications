import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { File, FileAttributes } from "@guildion/core/src/adaptors/models/File";

export class RoomFileCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/file/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomFileCreateRequestAttributes extends RequestParameters {
    file: FileAttributes,
}

export class RoomFileCreateRequest extends Record<RoomFileCreateRequestAttributes>({
    file: new File().toJSON(),
}) {
    constructor(args: RoomFileCreateRequestAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}

export interface RoomFileCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    file: FileAttributes,
}

export class RoomFileCreateResponse extends Record<RoomFileCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    file: new File().toJSON(),
}) {
    constructor(args: RoomFileCreateResponseAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}