import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomCategory, RoomCategoryAttributes } from "@guildion/core/src/adaptors/models/RoomCategory";

export class RoomCategoryCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/category/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomCategoryCreateRequestAttributes extends RequestParameters {
    roomCategory: RoomCategoryAttributes,
}

export class RoomCategoryCreateRequest extends Record<RoomCategoryCreateRequestAttributes>({
    roomCategory: new RoomCategory().toJSON(),
}) {
    constructor(args: RoomCategoryCreateRequestAttributes) {
        super(args);
    }

    public getRoomCategory(): RoomCategory | undefined {
        const result = this.get('roomCategory');
        return result ? (new RoomCategory(result)) : undefined;
    }
}

export interface RoomCategoryCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    roomCategory: RoomCategoryAttributes,
}

export class RoomCategoryCreateResponse extends Record<RoomCategoryCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    roomCategory: new RoomCategory().toJSON(),
}) {
    constructor(args: RoomCategoryCreateResponseAttributes) {
        super(args);
    }

    public getRoomCategory(): RoomCategory | undefined {
        const result = this.get('roomCategory');
        return result ? (new RoomCategory(result)) : undefined;
    }
}