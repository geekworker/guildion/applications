import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomCategoryAttributes, RoomCategories } from "@guildion/core/src/adaptors/models/RoomCategory";

export class RoomCategoriesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/categories/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomCategoriesListRequestAttributes extends RequestParameters {
    guildId: string,
    count?: number,
    offset?: number,
    isPublic?: boolean,
    roomsCount?: number,
}

export class RoomCategoriesListRequest extends Record<RoomCategoriesListRequestAttributes>({
    guildId: '',
    count: undefined,
    offset: undefined,
    isPublic: undefined,
    roomsCount: undefined,
}) {
    constructor(args: RoomCategoriesListRequestAttributes) {
        super(args);
    }
}

export interface RoomCategoriesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    roomCategories: RoomCategoryAttributes[],
    paginatable: boolean,
}

export class RoomCategoriesListResponse extends Record<RoomCategoriesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    roomCategories: [],
    paginatable: false,
}) {
    constructor(args: RoomCategoriesListResponseAttributes) {
        super(args);
    }

    public getRoomCategories(): RoomCategories {
        const result = this.get('roomCategories');
        return new RoomCategories(result);
    }
}