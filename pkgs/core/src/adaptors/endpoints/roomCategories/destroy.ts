import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomCategoryDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/category/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomCategoryDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class RoomCategoryDestroyRequest extends Record<RoomCategoryDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: RoomCategoryDestroyRequestAttributes) {
        super(args);
    }
}

export interface RoomCategoryDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomCategoryDestroyResponse extends Record<RoomCategoryDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomCategoryDestroyResponseAttributes) {
        super(args);
    }
}