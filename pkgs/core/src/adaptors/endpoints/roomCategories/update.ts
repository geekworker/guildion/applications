import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomCategory, RoomCategoryAttributes } from "@guildion/core/src/adaptors/models/RoomCategory";

export class RoomCategoryUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/category/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomCategoryUpdateRequestAttributes extends RequestParameters {
    roomCategory: Partial<RoomCategoryAttributes> & { id: string },
}

export class RoomCategoryUpdateRequest extends Record<RoomCategoryUpdateRequestAttributes>({
    roomCategory: { id: '', ...new RoomCategory().toJSON() },
}) {
    constructor(args: RoomCategoryUpdateRequestAttributes) {
        super(args);
    }

    public getRoomCategory(): RoomCategory | undefined {
        const result = this.get('roomCategory');
        return result ? (new RoomCategory(result)) : undefined;
    }
}

export interface RoomCategoryUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    roomCategory: RoomCategoryAttributes,
}

export class RoomCategoryUpdateResponse extends Record<RoomCategoryUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    roomCategory: new RoomCategory().toJSON(),
}) {
    constructor(args: RoomCategoryUpdateResponseAttributes) {
        super(args);
    }

    public getRoomCategory(): RoomCategory | undefined {
        const result = this.get('roomCategory');
        return result ? (new RoomCategory(result)) : undefined;
    }
}