import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RoomCategoriesListRequest, RoomCategoriesListResponse, RoomCategoriesListRouter } from "./list";
import { RoomCategoryCreateRequest, RoomCategoryCreateResponse, RoomCategoryCreateRouter } from "./create";
import { RoomCategoryUpdateRequest, RoomCategoryUpdateResponse, RoomCategoryUpdateRouter } from "./update";
import { RoomCategoryDestroyRequest, RoomCategoryDestroyResponse, RoomCategoryDestroyRouter } from "./destroy";

export namespace RoomCategoriesEndpoint {
    export const List: Endpoint<RoomCategoriesListRouter, RoomCategoriesListRequest, RoomCategoriesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomCategoriesListRouter,
        request: RoomCategoriesListRequest,
        response: RoomCategoriesListResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type List = typeof List;

    export const Create: Endpoint<RoomCategoryCreateRouter, RoomCategoryCreateRequest, RoomCategoryCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomCategoryCreateRouter,
        request: RoomCategoryCreateRequest,
        response: RoomCategoryCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;
    
    export const Update: Endpoint<RoomCategoryUpdateRouter, RoomCategoryUpdateRequest, RoomCategoryUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomCategoryUpdateRouter,
        request: RoomCategoryUpdateRequest,
        response: RoomCategoryUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;
    
    export const Destroy: Endpoint<RoomCategoryDestroyRouter, RoomCategoryDestroyRequest, RoomCategoryDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomCategoryDestroyRouter,
        request: RoomCategoryDestroyRequest,
        response: RoomCategoryDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;
}