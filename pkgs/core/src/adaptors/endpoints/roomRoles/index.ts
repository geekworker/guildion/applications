import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RoomRolesListRequest, RoomRolesListResponse, RoomRolesListRouter } from "./list";
import { RoomRoleShowRequest, RoomRoleShowResponse, RoomRoleShowRouter } from "./show";
import { RoomRoleCreateRequest, RoomRoleCreateResponse, RoomRoleCreateRouter } from "./create";
import { RoomRoleUpdateRequest, RoomRoleUpdateResponse, RoomRoleUpdateRouter } from "./update";
import { RoomRoleDestroyRequest, RoomRoleDestroyResponse, RoomRoleDestroyRouter } from "./destroy";

export namespace RoomRolesEndpoint {
    export const List: Endpoint<RoomRolesListRouter, RoomRolesListRequest, RoomRolesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomRolesListRouter,
        request: RoomRolesListRequest,
        response: RoomRolesListResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type List = typeof List;

    export const Show: Endpoint<RoomRoleShowRouter, RoomRoleShowRequest, RoomRoleShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomRoleShowRouter,
        request: RoomRoleShowRequest,
        response: RoomRoleShowResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Show = typeof Show;

    export const Create: Endpoint<RoomRoleCreateRouter, RoomRoleCreateRequest, RoomRoleCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomRoleCreateRouter,
        request: RoomRoleCreateRequest,
        response: RoomRoleCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;

    export const Update: Endpoint<RoomRoleUpdateRouter, RoomRoleUpdateRequest, RoomRoleUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomRoleUpdateRouter,
        request: RoomRoleUpdateRequest,
        response: RoomRoleUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<RoomRoleDestroyRouter, RoomRoleDestroyRequest, RoomRoleDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomRoleDestroyRouter,
        request: RoomRoleDestroyRequest,
        response: RoomRoleDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;
}