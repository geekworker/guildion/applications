import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Role, RoleAttributes } from "@guildion/core/src/adaptors/models/Role";

export class RoomRoleCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/role/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomRoleCreateRequestAttributes extends RequestParameters {
    roomId: string,
    roleId: string,
}

export class RoomRoleCreateRequest extends Record<RoomRoleCreateRequestAttributes>({
    roomId: '',
    roleId: '',
}) {
    constructor(args: RoomRoleCreateRequestAttributes) {
        super(args);
    }
}

export interface RoomRoleCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    role: RoleAttributes,
}

export class RoomRoleCreateResponse extends Record<RoomRoleCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    role: new Role().toJSON(),
}) {
    constructor(args: RoomRoleCreateResponseAttributes) {
        super(args);
    }

    public getRole(): Role | undefined {
        const result = this.get('role');
        return result ? (new Role(result)) : undefined;
    }
}