import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Role, RoleAttributes } from "@guildion/core/src/adaptors/models/Role";

export class RoomRoleUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/role/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomRoleUpdateRequestAttributes extends RequestParameters {
    role: Partial<RoleAttributes> & { id: string, roomId: string }
}

export class RoomRoleUpdateRequest extends Record<RoomRoleUpdateRequestAttributes>({
    role: { id: '', roomId: '', ...new Role().toJSON() },
}) {
    constructor(args: RoomRoleUpdateRequestAttributes) {
        super(args);
    }
}

export interface RoomRoleUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    role: RoleAttributes,
}

export class RoomRoleUpdateResponse extends Record<RoomRoleUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    role: new Role().toJSON(),
}) {
    constructor(args: RoomRoleUpdateResponseAttributes) {
        super(args);
    }

    public getRole(): Role | undefined {
        const result = this.get('role');
        return result ? (new Role(result)) : undefined;
    }
}