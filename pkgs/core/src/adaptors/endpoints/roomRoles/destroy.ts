import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomRoleDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/role/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomRoleDestroyRequestAttributes extends RequestParameters {
    roomId: string,
    roleId: string,
}

export class RoomRoleDestroyRequest extends Record<RoomRoleDestroyRequestAttributes>({
    roomId: '',
    roleId: '',
}) {
    constructor(args: RoomRoleDestroyRequestAttributes) {
        super(args);
    }
}

export interface RoomRoleDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomRoleDestroyResponse extends Record<RoomRoleDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomRoleDestroyResponseAttributes) {
        super(args);
    }
}