import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoleAttributes, Roles } from "@guildion/core/src/adaptors/models/Role";

export class RoomRolesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/roles/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomRolesListRequestAttributes extends RequestParameters {
    roomId: string,
}

export class RoomRolesListRequest extends Record<RoomRolesListRequestAttributes>({
    roomId: '',
}) {
    constructor(args: RoomRolesListRequestAttributes) {
        super(args);
    }
}

export interface RoomRolesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    roles: RoleAttributes[],
}

export class RoomRolesListResponse extends Record<RoomRolesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    roles: [],
}) {
    constructor(args: RoomRolesListResponseAttributes) {
        super(args);
    }

    public getRoles(): Roles {
        const result = this.get('roles');
        return new Roles(result);
    }
}