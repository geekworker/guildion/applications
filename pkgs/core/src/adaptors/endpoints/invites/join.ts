import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Member, MemberAttributes } from "@guildion/core/src/adaptors/models/Member";
import { Guild, GuildAttributes } from "@guildion/core/src/adaptors/models/Guild";
import { Room, RoomAttributes } from "@guildion/core/src/adaptors/models/Room";

export class InviteJoinRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/invite/join',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface InviteJoinRequestAttributes extends RequestParameters {
    id: string,
}

export class InviteJoinRequest extends Record<InviteJoinRequestAttributes>({
    id: '',
}) {
    constructor(args: InviteJoinRequestAttributes) {
        super(args);
    }
}

export interface InviteJoinResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guild: GuildAttributes,
    room?: RoomAttributes,
    member: MemberAttributes,
}

export class InviteJoinResponse extends Record<InviteJoinResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guild: new Guild().toJSON(),
    room: undefined,
    member: new Member().toJSON(),
}) {
    constructor(args: InviteJoinResponseAttributes) {
        super(args);
    }

    public getGuild(): Guild | undefined {
        const result = this.get('guild');
        return result ? (new Guild(result)) : undefined;
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? (new Member(result)) : undefined;
    }

    public getRoom(): Room | undefined {
        const result = this.get('room');
        return result ? (new Room(result)) : undefined;
    }
}