import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { InviteAttributes, Invites } from "@guildion/core/src/adaptors/models/Invite";

export class InvitesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/invites/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface InvitesListRequestAttributes extends RequestParameters {
    guildId: string,
    roomId?: string,
    count?: number,
    offset?: number,
}

export class InvitesListRequest extends Record<InvitesListRequestAttributes>({
    guildId: '',
    roomId: undefined,
    count: undefined,
    offset: undefined,
}) {
    constructor(args: InvitesListRequestAttributes) {
        super(args);
    }
}

export interface InvitesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    invites: InviteAttributes[],
    paginatable: boolean,
}

export class InvitesListResponse extends Record<InvitesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    invites: [],
    paginatable: false,
}) {
    constructor(args: InvitesListResponseAttributes) {
        super(args);
    }

    public getInvites(): Invites {
        const result = this.get('invites');
        return new Invites(result);
    }
}