import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { InviteAttributes, Invite } from "@guildion/core/src/adaptors/models/Invite";

export class InviteShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/invite/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface InviteShowRequestAttributes extends RequestParameters {
    id: string,
}

export class InviteShowRequest extends Record<InviteShowRequestAttributes>({
    id: '',
}) {
    constructor(args: InviteShowRequestAttributes) {
        super(args);
    }
}

export interface InviteShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    invite: InviteAttributes,
}

export class InviteShowResponse extends Record<InviteShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    invite: new Invite().toJSON(),
}) {
    constructor(args: InviteShowResponseAttributes) {
        super(args);
    }

    public getInvite(): Invite {
        const result = this.get('invite');
        return new Invite(result);
    }
}