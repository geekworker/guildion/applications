import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class InviteDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/invite/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface InviteDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class InviteDestroyRequest extends Record<InviteDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: InviteDestroyRequestAttributes) {
        super(args);
    }
}

export interface InviteDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class InviteDestroyResponse extends Record<InviteDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: InviteDestroyResponseAttributes) {
        super(args);
    }
}