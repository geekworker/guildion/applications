import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Invite, InviteAttributes } from "@guildion/core/src/adaptors/models/Invite";

export class InviteCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/invite/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface InviteCreateRequestAttributes extends RequestParameters {
    invite: InviteAttributes,
}

export class InviteCreateRequest extends Record<InviteCreateRequestAttributes>({
    invite: new Invite().toJSON(),
}) {
    constructor(args: InviteCreateRequestAttributes) {
        super(args);
    }

    public getInvite(): Invite | undefined {
        const result = this.get('invite');
        return result ? (new Invite(result)) : undefined;
    }
}

export interface InviteCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    invite: InviteAttributes,
}

export class InviteCreateResponse extends Record<InviteCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    invite: new Invite().toJSON(),
}) {
    constructor(args: InviteCreateResponseAttributes) {
        super(args);
    }

    public getInvite(): Invite | undefined {
        const result = this.get('invite');
        return result ? (new Invite(result)) : undefined;
    }
}