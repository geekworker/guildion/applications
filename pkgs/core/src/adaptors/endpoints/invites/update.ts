import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Invite, InviteAttributes } from "@guildion/core/src/adaptors/models/Invite";

export class InviteUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/invite/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface InviteUpdateRequestAttributes extends RequestParameters {
    invite: Partial<InviteAttributes> & { id: string },
}

export class InviteUpdateRequest extends Record<InviteUpdateRequestAttributes>({
    invite: { id: '', ...new Invite().toJSON() },
}) {
    constructor(args: InviteUpdateRequestAttributes) {
        super(args);
    }

    public getInvite(): Invite | undefined {
        const result = this.get('invite');
        return result ? (new Invite(result)) : undefined;
    }
}

export interface InviteUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    invite: InviteAttributes,
}

export class InviteUpdateResponse extends Record<InviteUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    invite: new Invite().toJSON(),
}) {
    constructor(args: InviteUpdateResponseAttributes) {
        super(args);
    }

    public getInvite(): Invite | undefined {
        const result = this.get('invite');
        return result ? (new Invite(result)) : undefined;
    }
}