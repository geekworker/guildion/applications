import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { InviteCreateRequest, InviteCreateResponse, InviteCreateRouter } from "./create";
import { InviteUpdateRequest, InviteUpdateResponse, InviteUpdateRouter } from "./update";
import { InviteDestroyRequest, InviteDestroyResponse, InviteDestroyRouter } from "./destroy";
import { InviteJoinRequest, InviteJoinResponse, InviteJoinRouter } from "./join";
import { InvitesListRequest, InvitesListResponse, InvitesListRouter } from "./list";
import { InviteShowRequest, InviteShowResponse, InviteShowRouter } from "./show";

export namespace InvitesEndpoint {
    export const List: Endpoint<InvitesListRouter, InvitesListRequest, InvitesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: InvitesListRouter,
        request: InvitesListRequest,
        response: InvitesListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;

    export const Show: Endpoint<InviteShowRouter, InviteShowRequest, InviteShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: InviteShowRouter,
        request: InviteShowRequest,
        response: InviteShowResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Show = typeof Show;

    export const Create: Endpoint<InviteCreateRouter, InviteCreateRequest, InviteCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: InviteCreateRouter,
        request: InviteCreateRequest,
        response: InviteCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;
    
    export const Update: Endpoint<InviteUpdateRouter, InviteUpdateRequest, InviteUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: InviteUpdateRouter,
        request: InviteUpdateRequest,
        response: InviteUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<InviteDestroyRouter, InviteDestroyRequest, InviteDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: InviteDestroyRouter,
        request: InviteDestroyRequest,
        response: InviteDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const Join: Endpoint<InviteJoinRouter, InviteJoinRequest, InviteJoinResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: InviteJoinRouter,
        request: InviteJoinRequest,
        response: InviteJoinResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Join = typeof Join;
}