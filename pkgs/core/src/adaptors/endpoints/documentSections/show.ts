import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { DocumentSectionAttributes, DocumentSection } from "@guildion/core/src/adaptors/models/DocumentSection";
import { DocumentGroup, DocumentGroupAttributes } from "../../models/DocumentGroup";
import { Document, DocumentAttributes } from "../../models/Document";

export class DocumentSectionShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/document/section/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentSectionShowRequestAttributes extends RequestParameters {
    groupSlug: string,
    slug: string,
}

export class DocumentSectionShowRequest extends Record<DocumentSectionShowRequestAttributes>({
    groupSlug: '',
    slug: '',
}) {
    constructor(args: DocumentSectionShowRequestAttributes) {
        super(args);
    }
}

export interface DocumentSectionShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    group: DocumentGroupAttributes,
    section: DocumentSectionAttributes,
    document: DocumentAttributes,
}

export class DocumentSectionShowResponse extends Record<DocumentSectionShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    group: new DocumentGroup().toJSON(),
    section: new DocumentSection().toJSON(),
    document: new Document().toJSON(),
}) {
    constructor(args: DocumentSectionShowResponseAttributes) {
        super(args);
    }

    public getGroup(): DocumentGroup {
        const result = this.get('group');
        return new DocumentGroup(result);
    }

    public getSection(): DocumentSection {
        const result = this.get('section');
        return new DocumentSection(result);
    }

    public getDocument(): Document {
        const result = this.get('document');
        return new Document(result);
    }
}