import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { LanguageCode } from "@guildion/core/src/constants/LanguageCode";

export class DocumentSectionsShowPathsRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/document/section/show/paths',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentSectionsShowPathsRequestAttributes extends RequestParameters {
}

export class DocumentSectionsShowPathsRequest extends Record<DocumentSectionsShowPathsRequestAttributes>({
}) {
    constructor(args: DocumentSectionsShowPathsRequestAttributes) {
        super(args);
    }
}

export interface DocumentSectionsShowPathsResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    paths: { params: { groupSlug: string, sectionSlug: string }, locale: LanguageCode }[],
}

export class DocumentSectionsShowPathsResponse extends Record<DocumentSectionsShowPathsResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    paths: [],
}) {
    constructor(args: DocumentSectionsShowPathsResponseAttributes) {
        super(args);
    }
}