import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { DocumentSectionAttributes, DocumentSections } from "@guildion/core/src/adaptors/models/DocumentSection";
import { DocumentGroup, DocumentGroupAttributes } from "../../models/DocumentGroup";

export class DocumentSectionsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/documents/sections/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentSectionsListRequestAttributes extends RequestParameters {
    groupSlug: string,
    count?: number,
    offset?: number,
    includes?: {
        documents?: boolean,
    },
}

export class DocumentSectionsListRequest extends Record<DocumentSectionsListRequestAttributes>({
    groupSlug: '',
    count: undefined,
    offset: undefined,
    includes: undefined,
}) {
    constructor(args: DocumentSectionsListRequestAttributes) {
        super(args);
    }
}

export interface DocumentSectionsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    group: DocumentGroupAttributes,
    sections: DocumentSectionAttributes[],
    paginatable: boolean,
}

export class DocumentSectionsListResponse extends Record<DocumentSectionsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    group: new DocumentGroup().toJSON(),
    sections: [],
    paginatable: false,
}) {
    constructor(args: DocumentSectionsListResponseAttributes) {
        super(args);
    }

    public getGroup(): DocumentGroup {
        const result = this.get('group');
        return new DocumentGroup(result);
    }

    public getSections(): DocumentSections {
        const result = this.get('sections');
        return new DocumentSections(result);
    }
}