import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { DocumentSectionsListRequest, DocumentSectionsListResponse, DocumentSectionsListRouter } from "./list";
import { DocumentSectionShowRequest, DocumentSectionShowResponse, DocumentSectionShowRouter } from "./show";
import { DocumentSectionsShowPathsRequest, DocumentSectionsShowPathsResponse, DocumentSectionsShowPathsRouter } from "./showPaths";

export namespace DocumentSectionsEndpoint {
    export const List: Endpoint<DocumentSectionsListRouter, DocumentSectionsListRequest, DocumentSectionsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentSectionsListRouter,
        request: DocumentSectionsListRequest,
        response: DocumentSectionsListResponse,
        options: { deviceRequired: false },
    }
    export type List = typeof List;

    export const Show: Endpoint<DocumentSectionShowRouter, DocumentSectionShowRequest, DocumentSectionShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentSectionShowRouter,
        request: DocumentSectionShowRequest,
        response: DocumentSectionShowResponse,
        options: { deviceRequired: false },
    }
    export type Show = typeof Show;

    export const ShowPaths: Endpoint<DocumentSectionsShowPathsRouter, DocumentSectionsShowPathsRequest, DocumentSectionsShowPathsResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentSectionsShowPathsRouter,
        request: DocumentSectionsShowPathsRequest,
        response: DocumentSectionsShowPathsResponse,
        options: { deviceRequired: false },
    }
    export type ShowPaths = typeof ShowPaths;
}