import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { MessageAttributes, Messages } from "@guildion/core/src/adaptors/models/Message";

export class MessagesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/messages/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MessagesListRequestAttributes extends RequestParameters {
    roomId: string,
    messageId?: string,
    count?: number,
    offset?: number,
    gtAt?: string,
    ltAt?: string,
}

export class MessagesListRequest extends Record<MessagesListRequestAttributes>({
    roomId: '',
    messageId: undefined,
    count: undefined,
    offset: undefined,
    gtAt: undefined,
    ltAt: undefined,
}) {
    constructor(args: MessagesListRequestAttributes) {
        super(args);
    }

    public getGtAt(): Date | undefined {
        const result = this.get('gtAt');
        return result ? new Date(result) : undefined;
    }

    public getLtAt(): Date | undefined {
        const result = this.get('ltAt');
        return result ? new Date(result) : undefined;
    }
}

export interface MessagesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    messages: MessageAttributes[],
    paginatable: boolean,
}

export class MessagesListResponse extends Record<MessagesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    messages: [],
    paginatable: false,
}) {
    constructor(args: MessagesListResponseAttributes) {
        super(args);
    }

    public getMessages(): Messages {
        const result = this.get('messages');
        return new Messages(result);
    }
}