import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class MessageReadRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/message/read',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MessageReadRequestAttributes extends RequestParameters {
    roomId: string,
}

export class MessageReadRequest extends Record<MessageReadRequestAttributes>({
    roomId: '',
}) {
    constructor(args: MessageReadRequestAttributes) {
        super(args);
    }
}

export interface MessageReadResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class MessageReadResponse extends Record<MessageReadResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: MessageReadResponseAttributes) {
        super(args);
    }
}