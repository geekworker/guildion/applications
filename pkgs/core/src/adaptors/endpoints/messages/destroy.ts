import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class MessageDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/message/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MessageDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class MessageDestroyRequest extends Record<MessageDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: MessageDestroyRequestAttributes) {
        super(args);
    }
}

export interface MessageDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class MessageDestroyResponse extends Record<MessageDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: MessageDestroyResponseAttributes) {
        super(args);
    }
}