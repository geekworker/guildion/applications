import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Emoji } from "@guildion/core/src/constants/Emoji";

export class MessageReactionRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/message/reaction',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MessageReactionRequestAttributes extends RequestParameters {
    id: string,
    fileId: string,
    emoji?: Emoji,
}

export class MessageReactionRequest extends Record<MessageReactionRequestAttributes>({
    id: '',
    fileId: '',
    emoji: undefined,
}) {
    constructor(args: MessageReactionRequestAttributes) {
        super(args);
    }
}

export interface MessageReactionResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    isCreated: boolean,
}

export class MessageReactionResponse extends Record<MessageReactionResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    isCreated: false,
}) {
    constructor(args: MessageReactionResponseAttributes) {
        super(args);
    }
}