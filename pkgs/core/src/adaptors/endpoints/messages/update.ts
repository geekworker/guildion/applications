import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Message, MessageAttributes } from "@guildion/core/src/adaptors/models/Message";

export class MessageUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/message/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MessageUpdateRequestAttributes extends RequestParameters {
    message: Partial<MessageAttributes> & { id: string },
}

export class MessageUpdateRequest extends Record<MessageUpdateRequestAttributes>({
    message: { ...new Message().toJSON(), id: '', body: '' },
}) {
    constructor(args: MessageUpdateRequestAttributes) {
        super(args);
    }

    public getMessage(): Message | undefined {
        const result = this.get('message');
        return result ? (new Message(result)) : undefined;
    }
}

export interface MessageUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    message: MessageAttributes,
}

export class MessageUpdateResponse extends Record<MessageUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    message: new Message().toJSON(),
}) {
    constructor(args: MessageUpdateResponseAttributes) {
        super(args);
    }

    public getMessage(): Message | undefined {
        const result = this.get('message');
        return result ? (new Message(result)) : undefined;
    }
}