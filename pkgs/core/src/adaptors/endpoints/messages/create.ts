import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Message, MessageAttributes } from "@guildion/core/src/adaptors/models/Message";

export class MessageCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/message/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MessageCreateRequestAttributes extends RequestParameters {
    message: MessageAttributes,
}

export class MessageCreateRequest extends Record<MessageCreateRequestAttributes>({
    message: new Message().toJSON(),
}) {
    constructor(args: MessageCreateRequestAttributes) {
        super(args);
    }

    public getMessage(): Message | undefined {
        const result = this.get('message');
        return result ? (new Message(result)) : undefined;
    }
}

export interface MessageCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    message: MessageAttributes,
}

export class MessageCreateResponse extends Record<MessageCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    message: new Message().toJSON(),
}) {
    constructor(args: MessageCreateResponseAttributes) {
        super(args);
    }

    public getMessage(): Message | undefined {
        const result = this.get('message');
        return result ? (new Message(result)) : undefined;
    }
}