import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { MessagesListRequest, MessagesListResponse, MessagesListRouter } from "./list";
import { MessageCreateRequest, MessageCreateResponse, MessageCreateRouter } from "./create";
import { MessageUpdateRequest, MessageUpdateResponse, MessageUpdateRouter } from "./update";
import { MessageDestroyRequest, MessageDestroyResponse, MessageDestroyRouter } from "./destroy";
import { MessageReactionRequest, MessageReactionResponse, MessageReactionRouter } from "./reaction";
import { MessageReadRequest, MessageReadResponse, MessageReadRouter } from "./read";

export namespace MessagesEndpoint {
    export const List: Endpoint<MessagesListRouter, MessagesListRequest, MessagesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MessagesListRouter,
        request: MessagesListRequest,
        response: MessagesListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;

    export const Create: Endpoint<MessageCreateRouter, MessageCreateRequest, MessageCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MessageCreateRouter,
        request: MessageCreateRequest,
        response: MessageCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;

    export const Update: Endpoint<MessageUpdateRouter, MessageUpdateRequest, MessageUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MessageUpdateRouter,
        request: MessageUpdateRequest,
        response: MessageUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<MessageDestroyRouter, MessageDestroyRequest, MessageDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MessageDestroyRouter,
        request: MessageDestroyRequest,
        response: MessageDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const Reaction: Endpoint<MessageReactionRouter, MessageReactionRequest, MessageReactionResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MessageReactionRouter,
        request: MessageReactionRequest,
        response: MessageReactionResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Reaction = typeof Reaction;

    export const Read: Endpoint<MessageReadRouter, MessageReadRequest, MessageReadResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MessageReadRouter,
        request: MessageReadRequest,
        response: MessageReadResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Read = typeof Read;
}