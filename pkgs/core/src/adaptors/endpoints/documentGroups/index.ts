import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { DocumentGroupShowRequest, DocumentGroupShowResponse, DocumentGroupShowRouter } from "./show";
import { DocumentGroupsShowPathsRequest, DocumentGroupsShowPathsResponse, DocumentGroupsShowPathsRouter } from "./showPaths";

export namespace DocumentGroupsEndpoint {
    export const Show: Endpoint<DocumentGroupShowRouter, DocumentGroupShowRequest, DocumentGroupShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentGroupShowRouter,
        request: DocumentGroupShowRequest,
        response: DocumentGroupShowResponse,
        options: { deviceRequired: false },
    }
    export type Show = typeof Show;

    export const ShowPaths: Endpoint<DocumentGroupsShowPathsRouter, DocumentGroupsShowPathsRequest, DocumentGroupsShowPathsResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentGroupsShowPathsRouter,
        request: DocumentGroupsShowPathsRequest,
        response: DocumentGroupsShowPathsResponse,
        options: { deviceRequired: false },
    }
    export type ShowPaths = typeof ShowPaths;
}