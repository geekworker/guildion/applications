import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { LanguageCode } from "@guildion/core/src/constants/LanguageCode";

export class DocumentGroupsShowPathsRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/document/group/show/paths',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentGroupsShowPathsRequestAttributes extends RequestParameters {
}

export class DocumentGroupsShowPathsRequest extends Record<DocumentGroupsShowPathsRequestAttributes>({
}) {
    constructor(args: DocumentGroupsShowPathsRequestAttributes) {
        super(args);
    }
}

export interface DocumentGroupsShowPathsResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    paths: { params: { groupSlug: string }, locale: LanguageCode }[],
}

export class DocumentGroupsShowPathsResponse extends Record<DocumentGroupsShowPathsResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    paths: [],
}) {
    constructor(args: DocumentGroupsShowPathsResponseAttributes) {
        super(args);
    }
}