import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { DocumentGroupAttributes, DocumentGroup } from "@guildion/core/src/adaptors/models/DocumentGroup";

export class DocumentGroupShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/document/group/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentGroupShowRequestAttributes extends RequestParameters {
    slug: string,
}

export class DocumentGroupShowRequest extends Record<DocumentGroupShowRequestAttributes>({
    slug: '',
}) {
    constructor(args: DocumentGroupShowRequestAttributes) {
        super(args);
    }
}

export interface DocumentGroupShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    group: DocumentGroupAttributes,
}

export class DocumentGroupShowResponse extends Record<DocumentGroupShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    group: new DocumentGroup().toJSON(),
}) {
    constructor(args: DocumentGroupShowResponseAttributes) {
        super(args);
    }

    public getGroup(): DocumentGroup {
        const result = this.get('group');
        return new DocumentGroup(result);
    }
}