import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files, FileStatus } from "@guildion/core/src/adaptors/models/File";
import { FileType } from "@guildion/core/src/constants/FileType";
import { ContentType } from "@guildion/core/src/constants/ContentType";

export class GuildFilesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/files/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildFilesListRequestAttributes extends RequestParameters {
    id: string,
    count?: number,
    offset?: number,
    types?: FileType[],
    statuses?: FileStatus[],
    contentTypes?: ContentType[],
}

export class GuildFilesListRequest extends Record<GuildFilesListRequestAttributes>({
    id: '',
    count: undefined,
    offset: undefined,
    types: undefined,
    statuses: undefined,
}) {
    constructor(args: GuildFilesListRequestAttributes) {
        super(args);
    }
}

export interface GuildFilesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files: FileAttributes[],
    paginatable: boolean,
}

export class GuildFilesListResponse extends Record<GuildFilesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: [],
    paginatable: false,
}) {
    constructor(args: GuildFilesListResponseAttributes) {
        super(args);
    }

    public getFiles(): Files {
        const result = this.get('files');
        return !!result ? new Files(result) : new Files([]);
    }
}