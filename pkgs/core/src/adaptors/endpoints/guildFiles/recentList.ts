import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files, FileStatus } from "@guildion/core/src/adaptors/models/File";
import { FileType } from "@guildion/core/src/constants/FileType";
import { ContentType } from "@guildion/core/src/constants/ContentType";

export class GuildFilesRecentListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/files/list/recent',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildFilesRecentListRequestAttributes extends RequestParameters {
    id: string,
    count?: number,
    offset?: number,
    types?: FileType[],
    statuses?: FileStatus[],
    contentTypes?: ContentType[],
    roomId?: string,
}

export class GuildFilesRecentListRequest extends Record<GuildFilesRecentListRequestAttributes>({
    id: '',
    count: undefined,
    offset: undefined,
    types: undefined,
    statuses: undefined,
    roomId: undefined,
}) {
    constructor(args: GuildFilesRecentListRequestAttributes) {
        super(args);
    }
}

export interface GuildFilesRecentListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files: FileAttributes[],
    paginatable: boolean,
}

export class GuildFilesRecentListResponse extends Record<GuildFilesRecentListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: [],
    paginatable: false,
}) {
    constructor(args: GuildFilesRecentListResponseAttributes) {
        super(args);
    }

    public getFiles(): Files {
        const result = this.get('files');
        return !!result ? new Files(result) : new Files([]);
    }
}