import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { File, FileAttributes } from "@guildion/core/src/adaptors/models/File";

export class GuildFileCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/file/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildFileCreateRequestAttributes extends RequestParameters {
    file: FileAttributes,
}

export class GuildFileCreateRequest extends Record<GuildFileCreateRequestAttributes>({
    file: new File().toJSON(),
}) {
    constructor(args: GuildFileCreateRequestAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}

export interface GuildFileCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    file: FileAttributes,
}

export class GuildFileCreateResponse extends Record<GuildFileCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    file: new File().toJSON(),
}) {
    constructor(args: GuildFileCreateResponseAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}