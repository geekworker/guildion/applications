import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { GuildFilesListRequest, GuildFilesListResponse, GuildFilesListRouter } from "./list";
import { GuildFilesRecentListRequest, GuildFilesRecentListResponse, GuildFilesRecentListRouter } from "./recentList";
import { GuildFileCreateRequest, GuildFileCreateResponse, GuildFileCreateRouter } from "./create";

export namespace GuildFilesEndpoint {
    export const List: Endpoint<GuildFilesListRouter, GuildFilesListRequest, GuildFilesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildFilesListRouter,
        request: GuildFilesListRequest,
        response: GuildFilesListResponse,
        options: { deviceRequired: true, accessTokenRequired: true, skipLogging: false, },
    }
    export type List = typeof List;

    export const RecentList: Endpoint<GuildFilesRecentListRouter, GuildFilesRecentListRequest, GuildFilesRecentListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildFilesRecentListRouter,
        request: GuildFilesRecentListRequest,
        response: GuildFilesRecentListResponse,
        options: { deviceRequired: true, accessTokenRequired: true, skipLogging: false, },
    }
    export type RecentList = typeof RecentList;

    export const Create: Endpoint<GuildFileCreateRouter, GuildFileCreateRequest, GuildFileCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildFileCreateRouter,
        request: GuildFileCreateRequest,
        response: GuildFileCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true, skipLogging: false, },
    }
    export type Create = typeof Create;
}