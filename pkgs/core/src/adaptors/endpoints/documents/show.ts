import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { DocumentAttributes, Document } from "@guildion/core/src/adaptors/models/Document";

export class DocumentShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/document/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentShowRequestAttributes extends RequestParameters {
    groupSlug: string,
    sectionSlug: string,
    slug: string,
}

export class DocumentShowRequest extends Record<DocumentShowRequestAttributes>({
    groupSlug: '',
    sectionSlug: '',
    slug: '',
}) {
    constructor(args: DocumentShowRequestAttributes) {
        super(args);
    }
}

export interface DocumentShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    document: DocumentAttributes,
}

export class DocumentShowResponse extends Record<DocumentShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    document: new Document().toJSON(),
}) {
    constructor(args: DocumentShowResponseAttributes) {
        super(args);
    }

    public getDocument(): Document {
        const result = this.get('document');
        return new Document(result);
    }
}