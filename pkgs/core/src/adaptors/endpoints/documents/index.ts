import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { DocumentShowRequest, DocumentShowResponse, DocumentShowRouter } from "./show";
import { DocumentsShowPathsRequest, DocumentsShowPathsResponse, DocumentsShowPathsRouter } from "./showPaths";

export namespace DocumentsEndpoint {
    export const Show: Endpoint<DocumentShowRouter, DocumentShowRequest, DocumentShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentShowRouter,
        request: DocumentShowRequest,
        response: DocumentShowResponse,
        options: { deviceRequired: false },
    }
    export type Show = typeof Show;

    export const ShowPaths: Endpoint<DocumentsShowPathsRouter, DocumentsShowPathsRequest, DocumentsShowPathsResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DocumentsShowPathsRouter,
        request: DocumentsShowPathsRequest,
        response: DocumentsShowPathsResponse,
        options: { deviceRequired: false },
    }
    export type ShowPaths = typeof ShowPaths;
}