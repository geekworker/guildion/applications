import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { LanguageCode } from "@guildion/core/src/constants/LanguageCode";

export class DocumentsShowPathsRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/document/show/paths',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DocumentsShowPathsRequestAttributes extends RequestParameters {
}

export class DocumentsShowPathsRequest extends Record<DocumentsShowPathsRequestAttributes>({
}) {
    constructor(args: DocumentsShowPathsRequestAttributes) {
        super(args);
    }
}

export interface DocumentsShowPathsResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    paths: { params: { groupSlug: string, sectionSlug: string, slug: string }, locale: LanguageCode }[],
}

export class DocumentsShowPathsResponse extends Record<DocumentsShowPathsResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    paths: [],
}) {
    constructor(args: DocumentsShowPathsResponseAttributes) {
        super(args);
    }
}