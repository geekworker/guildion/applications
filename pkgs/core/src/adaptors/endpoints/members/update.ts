import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Member, MemberAttributes } from "@guildion/core/src/adaptors/models/Member";

export class MemberUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/member/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MemberUpdateRequestAttributes extends RequestParameters {
    member: Partial<MemberAttributes> & { id: string },
}

export class MemberUpdateRequest extends Record<MemberUpdateRequestAttributes>({
    member: { id: '', ...new Member().toJSON() },
}) {
    constructor(args: MemberUpdateRequestAttributes) {
        super(args);
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? (new Member(result)) : undefined;
    }
}

export interface MemberUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    member: MemberAttributes,
}

export class MemberUpdateResponse extends Record<MemberUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    member: new Member().toJSON(),
}) {
    constructor(args: MemberUpdateResponseAttributes) {
        super(args);
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? (new Member(result)) : undefined;
    }
}