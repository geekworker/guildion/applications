import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class MemberDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/member/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MemberDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class MemberDestroyRequest extends Record<MemberDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: MemberDestroyRequestAttributes) {
        super(args);
    }
}

export interface MemberDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class MemberDestroyResponse extends Record<MemberDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: MemberDestroyResponseAttributes) {
        super(args);
    }
}