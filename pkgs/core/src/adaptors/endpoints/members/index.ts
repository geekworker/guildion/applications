import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { MembersListRequest, MembersListResponse, MembersListRouter } from "./list";
import { MembersSearchRequest, MembersSearchResponse, MembersSearchRouter } from "./search";
import { MemberKickRequest, MemberKickResponse, MemberKickRouter } from "./kick";
import { MemberUpdateRequest, MemberUpdateResponse, MemberUpdateRouter } from "./update";
import { MemberDestroyRequest, MemberDestroyResponse, MemberDestroyRouter } from "./destroy";

export namespace MembersEndpoint {
    export const List: Endpoint<MembersListRouter, MembersListRequest, MembersListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MembersListRouter,
        request: MembersListRequest,
        response: MembersListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;

    export const Search: Endpoint<MembersSearchRouter, MembersSearchRequest, MembersSearchResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MembersSearchRouter,
        request: MembersSearchRequest,
        response: MembersSearchResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Search = typeof Search;
    
    export const Kick: Endpoint<MemberKickRouter, MemberKickRequest, MemberKickResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MemberKickRouter,
        request: MemberKickRequest,
        response: MemberKickResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Kick = typeof Kick;

    export const Update: Endpoint<MemberUpdateRouter, MemberUpdateRequest, MemberUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MemberUpdateRouter,
        request: MemberUpdateRequest,
        response: MemberUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<MemberDestroyRouter, MemberDestroyRequest, MemberDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MemberDestroyRouter,
        request: MemberDestroyRequest,
        response: MemberDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;
}