import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { MemberAttributes, Members } from "@guildion/core/src/adaptors/models/Member";

export class MembersSearchRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/members/search',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MembersSearchRequestAttributes extends RequestParameters {
    guildId: string,
    query: string,
    count?: number,
    offset?: number,
}

export class MembersSearchRequest extends Record<MembersSearchRequestAttributes>({
    guildId: '',
    query: '',
    count: undefined,
    offset: undefined,
}) {
    constructor(args: MembersSearchRequestAttributes) {
        super(args);
    }
}

export interface MembersSearchResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    members: MemberAttributes[],
    paginatable: boolean,
}

export class MembersSearchResponse extends Record<MembersSearchResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    members: [],
    paginatable: false,
}) {
    constructor(args: MembersSearchResponseAttributes) {
        super(args);
    }

    public getMembers(): Members {
        const result = this.get('members');
        return new Members(result);
    }
}