import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "../..";
import { Router } from "../../router";
import { GET_CURRENT_WEBSOCKET_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class ConnectionCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/connection/create/:deviceId',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(deviceId: string): string {
        return GET_CURRENT_WEBSOCKET_URL_STRING() + this.toPath$({ deviceId });
    }
}

export interface ConnectionCreateRequestAttributes extends RequestParameters {
}

export class ConnectionCreateRequest extends Record<ConnectionCreateRequestAttributes>({
}) {
    constructor(args: ConnectionCreateRequestAttributes) {
        super(args);
    }
}

export interface ConnectionCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
}

export class ConnectionCreateResponse extends Record<ConnectionCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
}) {
    constructor(args: ConnectionCreateResponseAttributes) {
        super(args);
    }
}