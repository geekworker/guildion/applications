import { Endpoint } from "@guildion/core/src/adaptors/endpoints"
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { ConnectionCreateRequest, ConnectionCreateResponse, ConnectionCreateRouter } from "./create";

export namespace ConnectionsEndpoint {
    export const Create: Endpoint<ConnectionCreateRouter, ConnectionCreateRequest, ConnectionCreateResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: ConnectionCreateRouter,
        request: ConnectionCreateRequest,
        response: ConnectionCreateResponse,
        options: { deviceRequired: true },
    }
    export type Create = typeof Create;
}