import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "../..";
import { Router } from "../../router";
import { GET_CURRENT_CONNECT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomMsgsCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/msgs/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_CONNECT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomMsgsCreateRequestAttributes extends RequestParameters {
    id: string,
    message: string,
}

export class RoomMsgsCreateRequest extends Record<RoomMsgsCreateRequestAttributes>({
    id: '',
    message: '',
}) {
    constructor(args: RoomMsgsCreateRequestAttributes) {
        super(args);
    }
}

export interface RoomMsgsCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomMsgsCreateResponse extends Record<RoomMsgsCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomMsgsCreateResponseAttributes) {
        super(args);
    }
}