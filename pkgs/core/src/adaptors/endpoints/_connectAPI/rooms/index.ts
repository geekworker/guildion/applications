import { Endpoint } from "@guildion/core/src/adaptors/endpoints"
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RoomMsgsCreateRequest, RoomMsgsCreateResponse, RoomMsgsCreateRouter } from "./msgsCreate";

export namespace RoomsEndpoint {
    export const MsgsCreate: Endpoint<RoomMsgsCreateRouter, RoomMsgsCreateRequest, RoomMsgsCreateResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomMsgsCreateRouter,
        request: RoomMsgsCreateRequest,
        response: RoomMsgsCreateResponse,
        options: { deviceRequired: true },
    }
    export type MsgsCreate = typeof MsgsCreate;
}