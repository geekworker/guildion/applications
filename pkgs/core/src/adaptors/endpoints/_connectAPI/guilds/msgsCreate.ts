import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "../..";
import { Router } from "../../router";
import { GET_CURRENT_CONNECT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class GuildMsgsCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/msgs/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_CONNECT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildMsgsCreateRequestAttributes extends RequestParameters {
    id: string,
    message: string,
}

export class GuildMsgsCreateRequest extends Record<GuildMsgsCreateRequestAttributes>({
    id: '',
    message: '',
}) {
    constructor(args: GuildMsgsCreateRequestAttributes) {
        super(args);
    }
}

export interface GuildMsgsCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildMsgsCreateResponse extends Record<GuildMsgsCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildMsgsCreateResponseAttributes) {
        super(args);
    }
}