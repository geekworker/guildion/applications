import { Endpoint } from "@guildion/core/src/adaptors/endpoints"
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { GuildMsgsCreateRequest, GuildMsgsCreateResponse, GuildMsgsCreateRouter } from "./msgsCreate";

export namespace GuildsEndpoint {
    export const MsgsCreate: Endpoint<GuildMsgsCreateRouter, GuildMsgsCreateRequest, GuildMsgsCreateResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildMsgsCreateRouter,
        request: GuildMsgsCreateRequest,
        response: GuildMsgsCreateResponse,
        options: { deviceRequired: true },
    }
    export type MsgsCreate = typeof MsgsCreate;
}