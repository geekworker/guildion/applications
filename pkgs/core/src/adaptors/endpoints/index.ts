import { ContentType } from "@guildion/core/src/constants/ContentType";
import { Class } from "@guildion/core/src/types/Class";
import { HTTPMethod, toAxiosMethod } from "@guildion/core/src/constants/HttpMethod"
import axios, { AxiosRequestConfig } from 'axios';
import { LanguageCode, fallbackLanguageCode } from "@guildion/core/src/constants/LanguageCode";
import { Localizer } from "../../locales";
import { Record } from "immutable";
import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Device } from "@guildion/core/src/adaptors/models/Device";
import { Router } from "./router";
import { AppError, isAppError, NetworkError } from "@guildion/core/src/extension/Error";
import { guardCase } from "@guildion/core/src/interfaces/CaseIterable";
import { AdministratorRole } from "@guildion/core/src/adaptors/models/Administrator";
import { GET_CURRENT_API_URL_STRING, GET_CURRENT_CONNECT_API_URL_STRING } from "../../constants/ApplicationConfig";
import { GUILDION_API_SERVICE_HOST } from "../../constants/Configure";

export type RequestParameters = {
}

export type ResponseParameters = {
    status: HTTPStatusCode,
    error?: Error,
}

export type RequestPathParameters = {
}

export type RequestQueryParameters = {
}

export type Endpoint<Factory extends Router.Factory<any>, Req extends Record<any>, Res extends Record<any>> = {
    router: Class<Factory>,
    method: HTTPMethod,
    requestType: ContentType,
    responseType: ContentType,
    request: Class<Req>,
    response: Class<Res>,
    options: {
        deviceRequired?: boolean,
        accessTokenRequired?: boolean,
        accessTokenOptional?: boolean,
        adminRoles?: AdministratorRole[],
        skipLogging?: boolean,
    },
}

export const HEADER_APPLICATION_ACCESS_TOKEN = 'application-accesstoken';
export const HEADER_APPLICATION_DEVICE = 'application-device';
export const HEADER_APPLICATION_CSRF = 'application-csrf';
export type EndpointOfRouter<T extends Endpoint<any, any, any>> = T extends Endpoint<infer Factory, any, any> ? Factory : any;
export type EndpointOfRequest<T extends Endpoint<any, any, any>> = T extends Endpoint<any, infer Req, any> ? Req : any;
export type EndpointOfResponse<T extends Endpoint<any, any, any>> = T extends Endpoint<any, any, infer Res> ? Res : any;
export type EndpointAPIProps<T extends Endpoint<any, any, any>> = {
    params?: Router.FactoryOfParameters<EndpointOfRouter<T>>,
    data: EndpointOfRequest<T>,
    device?: Device,
    accessToken?: string,
    csrfSecret?: string,
    lang?: LanguageCode,
    logger?: boolean,
};

export async function requestAPI<T extends Endpoint<any, any, any>>(
    endpoint: T,
    {
        params,
        data,
        device,
        accessToken,
        csrfSecret,
        lang,
        logger,
    }: EndpointAPIProps<T>,
): Promise<EndpointOfResponse<T>> {
    const localizer = new Localizer(lang || fallbackLanguageCode);
    if (endpoint.options.deviceRequired && !device) throw new Error(localizer.dictionary.error.common.requireDevice);
    if (endpoint.options.accessTokenRequired && !accessToken) throw new Error(localizer.dictionary.error.accessToken.invalidToken);
    if (endpoint.options.adminRoles && !accessToken) throw new Error(localizer.dictionary.error.accessToken.invalidToken);
    const path: string = (new endpoint.router).toPath(params || {});
    const payload: AxiosRequestConfig = {
        url: path,
        headers: {
            'Content-Type': endpoint.requestType,
            'Accept-Language': guardCase(lang, { cases: LanguageCode, fallback: fallbackLanguageCode }),
        },
        method: toAxiosMethod(endpoint.method),
        data: data.toJSON(),
    }
    if (!!endpoint.options.deviceRequired) {
        if (!device || !csrfSecret || csrfSecret.length == 0) throw new Error(localizer.dictionary.error.common.requireDevice);
        payload.headers[HEADER_APPLICATION_DEVICE] = JSON.stringify(device.toJSON());
        payload.headers[HEADER_APPLICATION_CSRF] = csrfSecret;
    }
    if (!!endpoint.options.accessTokenRequired || !!endpoint.options.accessTokenOptional || !!endpoint.options.adminRoles) {
        if (accessToken) payload.headers[HEADER_APPLICATION_ACCESS_TOKEN] = accessToken;
    }
    if (logger != false) console.log('[API REQUEST] -->', payload);
    const result = await axios.request<ReturnType<EndpointOfResponse<T>['toJSON']>>(payload);
    if (logger != false) console.log('[API RESPONSE] <--', result.data);
    return new endpoint.response(result.data);
}

export async function requestAPISafe<T extends Endpoint<any, any, any>>(
    endpoint: T,
    {
        params,
        data,
        device,
        accessToken,
        csrfSecret,
        lang,
        logger,
    }: EndpointAPIProps<T>,
): Promise<EndpointOfResponse<T> | AppError | NetworkError> {
    try {
        return await requestAPI<T>(endpoint, {
            params,
            data,
            device,
            accessToken,
            csrfSecret,
            lang,
            logger,
        });
    } catch (e) {
        const localizer = new Localizer(lang || fallbackLanguageCode);
        if (axios.isAxiosError(e)) {
            if (!e.response) return new NetworkError(localizer.dictionary.error.common.networkError);
            const error = new Error(e.response.data);
            if (isAppError(error) || !!e.response.data.isAppError) {
                return new AppError(e.response.data.message, { type: e.response.data.type });
            } else {
                return new AppError(localizer.dictionary.error.common.invalidRequest)    
            }
        } else {
            return new AppError(localizer.dictionary.error.common.invalidRequest)
        }
    }
}

export async function healthCheckAPI(): Promise<boolean> {
    try {
        const path: string = GUILDION_API_SERVICE_HOST() ? GET_CURRENT_API_URL_STRING() + '/health' : GET_CURRENT_API_URL_STRING() + '/api/v1/health';
        console.log('[API HEALTH CHECK REQUEST] -->', path);
        const payload: AxiosRequestConfig = {
            url: path,
            headers: {
                'Content-Type': ContentType.json,
                'Accept-Language': LanguageCode.English,
            },
            method: toAxiosMethod(HTTPMethod.GET),
            data: {},
        }
        const result = await axios.request(payload);
        console.log('[API HEALTH CHECK RESPONSE] <-- ✅ HEALTY');
        return true;
    } catch {
        console.log('[API HEALTH CHECK RESPONSE] <-- ❌ UNHEALTY');
        return false;
    }
}

export async function healthCheckConnect(): Promise<boolean> {
    try {
        const path: string = GET_CURRENT_CONNECT_API_URL_STRING() + '/health';
        console.log('[CONNECT HEALTH CHECK REQUEST] -->', path);
        const payload: AxiosRequestConfig = {
            url: path,
            headers: {
                'Content-Type': ContentType.json,
                'Accept-Language': LanguageCode.English,
            },
            method: toAxiosMethod(HTTPMethod.GET),
            data: {},
        }
        const result = await axios.request(payload);
        console.log('[CONNECT HEALTH CHECK RESPONSE] <-- ✅ HEALTY');
        return true;
    } catch {
        console.log('[CONNECT HEALTH CHECK RESPONSE] <-- ❌ UNHEALTY');
        return false;
    }
}