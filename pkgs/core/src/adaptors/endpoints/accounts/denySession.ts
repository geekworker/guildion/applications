import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Record } from "immutable";

export class AccountDenySessionRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/session/deny',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountDenySessionRequestAttributes extends RequestParameters {
}

export class AccountDenySessionRequest extends Record<AccountDenySessionRequestAttributes>({
}) {
    constructor(args: AccountDenySessionRequestAttributes) {
        super(args);
    }
}

export interface AccountDenySessionResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class AccountDenySessionResponse extends Record<AccountDenySessionResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: AccountDenySessionResponseAttributes) {
        super(args);
    }
}