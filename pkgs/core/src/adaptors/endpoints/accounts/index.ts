import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { AccountAuthenticateRequest, AccountAuthenticateResponse, AccountAuthenticateRouter } from "./authenticate";
import { AccountRegisterRequest, AccountRegisterResponse, AccountRegisterRouter } from "./register";
import { AccountLogoutRequest, AccountLogoutResponse, AccountLogoutRouter } from "./logout";
import { AccountShowRequest, AccountShowResponse, AccountShowRouter } from "./show";
import { AccountUpdateRequest, AccountUpdateResponse, AccountUpdateRouter } from "./update";
import { AccountAllowSessionRequest, AccountAllowSessionResponse, AccountAllowSessionRouter } from "./allowSession";
import { AccountDenySessionRequest, AccountDenySessionResponse, AccountDenySessionRouter } from "./denySession";

export namespace AccountsEndpoint {
    export const Authenticate: Endpoint<AccountAuthenticateRouter, AccountAuthenticateRequest, AccountAuthenticateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountAuthenticateRouter,
        request: AccountAuthenticateRequest,
        response: AccountAuthenticateResponse,
        options: { deviceRequired: true },
    }
    export type Authenticate = typeof Authenticate;

    export const Register: Endpoint<AccountRegisterRouter, AccountRegisterRequest, AccountRegisterResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountRegisterRouter,
        request: AccountRegisterRequest,
        response: AccountRegisterResponse,
        options: { deviceRequired: true },
    }
    export type Register = typeof Register;

    export const Logout: Endpoint<AccountLogoutRouter, AccountLogoutRequest, AccountLogoutResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountLogoutRouter,
        request: AccountLogoutRequest,
        response: AccountLogoutResponse,
        options: { deviceRequired: true },
    }
    export type Logout = typeof Logout;

    export const Show: Endpoint<AccountShowRouter, AccountShowRequest, AccountShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountShowRouter,
        request: AccountShowRequest,
        response: AccountShowResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Show = typeof Show;

    export const Update: Endpoint<AccountUpdateRouter, AccountUpdateRequest, AccountUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountUpdateRouter,
        request: AccountUpdateRequest,
        response: AccountUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const AllowSession: Endpoint<AccountAllowSessionRouter, AccountAllowSessionRequest, AccountAllowSessionResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountAllowSessionRouter,
        request: AccountAllowSessionRequest,
        response: AccountAllowSessionResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type AllowSession = typeof AllowSession;

    export const DenySession: Endpoint<AccountDenySessionRouter, AccountDenySessionRequest, AccountDenySessionResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: AccountDenySessionRouter,
        request: AccountDenySessionRequest,
        response: AccountDenySessionResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type DenySession = typeof DenySession;
}