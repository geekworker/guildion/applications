import { User, UserAttributes } from "@guildion/core/src/adaptors/models/User";
import { Account, AccountAttributes } from "@guildion/core/src/adaptors/models/Account";
import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class AccountLogoutRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/logout',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountLogoutRequestAttributes extends RequestParameters {
    user: UserAttributes,
}

export class AccountLogoutRequest extends Record<AccountLogoutRequestAttributes>({
    user: new User().toJSON(),
}) {
    constructor(args: AccountLogoutRequestAttributes) {
        super(args);
    }
}

export interface AccountLogoutResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class AccountLogoutResponse extends Record<AccountLogoutResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: AccountLogoutResponseAttributes) {
        super(args);
    }
}