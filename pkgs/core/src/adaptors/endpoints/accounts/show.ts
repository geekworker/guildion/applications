import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { AccountAttributes, Account } from "@guildion/core/src/adaptors/models/Account";

export class AccountShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountShowRequestAttributes extends RequestParameters {
}

export class AccountShowRequest extends Record<AccountShowRequestAttributes>({
}) {
    constructor(args: AccountShowRequestAttributes) {
        super(args);
    }
}

export interface AccountShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    account: AccountAttributes,
    success: boolean,
}

export class AccountShowResponse extends Record<AccountShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    account: new Account().toJSON(),
    success: false,
}) {
    constructor(args: AccountShowResponseAttributes) {
        super(args);
    }

    public getAccount(): Account {
        const result = this.get('account');
        return new Account(result);
    }
}