import { User, UserAttributes } from "@guildion/core/src/adaptors/models/User";
import { Account, AccountAttributes } from "@guildion/core/src/adaptors/models/Account";
import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class AccountRegisterRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/register',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountRegisterRequestAttributes extends RequestParameters {
    user: UserAttributes,
}

export class AccountRegisterRequest extends Record<AccountRegisterRequestAttributes>({
    user: new User().toJSON(),
}) {
    constructor(args: AccountRegisterRequestAttributes) {
        super(args);
    }

    public getUser(): User | undefined {
        const result = this.get('user');
        return result ? (new User(result)) : undefined;
    }
}

export interface AccountRegisterResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    account: AccountAttributes,
    user: UserAttributes,
    accessToken: string,
}

export class AccountRegisterResponse extends Record<AccountRegisterResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    account: new Account().toJSON(),
    user: new User().toJSON(),
    accessToken: '',
}) {
    constructor(args: AccountRegisterResponseAttributes) {
        super(args);
    }

    public getUser(): User | undefined {
        const result = this.get('user');
        return result ? (new User(result)) : undefined;
    }

    public getAccount(): Account | undefined {
        const result = this.get('account');
        return result ? (new Account(result)) : undefined;
    }
}