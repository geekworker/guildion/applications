import { User, UserAttributes } from "@guildion/core/src/adaptors/models/User";
import { Account, AccountAttributes } from "@guildion/core/src/adaptors/models/Account";
import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class AccountAuthenticateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/authenticate',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountAuthenticateRequestAttributes extends RequestParameters {
    username: string,
}

export class AccountAuthenticateRequest extends Record<AccountAuthenticateRequestAttributes>({
    username: '',
}) {
    constructor(args: AccountAuthenticateRequestAttributes) {
        super(args);
    }
}

export interface AccountAuthenticateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    account?: AccountAttributes,
    user?: UserAttributes,
    accessToken: string,
    shouldTwoFactorAuth: boolean,
}

export class AccountAuthenticateResponse extends Record<AccountAuthenticateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    account: undefined,
    user: undefined,
    accessToken: '',
    shouldTwoFactorAuth: false,
}) {
    constructor(args: AccountAuthenticateResponseAttributes) {
        super(args);
    }

    public getUser(): User | undefined {
        const result = this.get('user');
        return result ? (new User(result)) : undefined;
    }

    public getAccount(): Account | undefined {
        const result = this.get('account');
        return result ? (new Account(result)) : undefined;
    }
}