import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Record } from "immutable";

export class AccountAllowSessionRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/session/allow',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountAllowSessionRequestAttributes extends RequestParameters {
    intervalSec: number,
}

export class AccountAllowSessionRequest extends Record<AccountAllowSessionRequestAttributes>({
    intervalSec: 0,
}) {
    constructor(args: AccountAllowSessionRequestAttributes) {
        super(args);
    }
}

export interface AccountAllowSessionResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class AccountAllowSessionResponse extends Record<AccountAllowSessionResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: AccountAllowSessionResponseAttributes) {
        super(args);
    }
}