import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Account, AccountAttributes } from "@guildion/core/src/adaptors/models/Account";

export class AccountUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountUpdateRequestAttributes extends RequestParameters {
    account: Partial<AccountAttributes>,
}

export class AccountUpdateRequest extends Record<AccountUpdateRequestAttributes>({
    account: new Account().toJSON(),
}) {
    constructor(args: AccountUpdateRequestAttributes) {
        super(args);
    }

    public getAccount(): Account | undefined {
        const result = this.get('account');
        return result ? (new Account(result)) : undefined;
    }
}

export interface AccountUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    account: AccountAttributes,
}

export class AccountUpdateResponse extends Record<AccountUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    account: new Account().toJSON(),
}) {
    constructor(args: AccountUpdateResponseAttributes) {
        super(args);
    }

    public getAccount(): Account | undefined {
        const result = this.get('account');
        return result ? (new Account(result)) : undefined;
    }
}