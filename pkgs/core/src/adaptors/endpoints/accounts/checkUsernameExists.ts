import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class AccountCheckUsernameExistsRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/account/username/exists',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface AccountCheckUsernameExistsRequestAttributes extends RequestParameters {
    username: string,
}

export class AccountCheckUsernameExistsRequest extends Record<AccountCheckUsernameExistsRequestAttributes>({
    username: '',
}) {
    constructor(args: AccountCheckUsernameExistsRequestAttributes) {
        super(args);
    }
}

export interface AccountCheckUsernameExistsResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    isExists?: boolean,
}

export class AccountCheckUsernameExistsResponse extends Record<AccountCheckUsernameExistsResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
}) {
    constructor(args: AccountCheckUsernameExistsResponseAttributes) {
        super(args);
    }
}