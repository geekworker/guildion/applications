import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class DeviceBlockRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/device/block',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DeviceBlockRequestAttributes extends RequestParameters {
    id: string,
}

export class DeviceBlockRequest extends Record<DeviceBlockRequestAttributes>({
    id: '',
}) {
    constructor(args: DeviceBlockRequestAttributes) {
        super(args);
    }
}

export interface DeviceBlockResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class DeviceBlockResponse extends Record<DeviceBlockResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: DeviceBlockResponseAttributes) {
        super(args);
    }
}