import { DeviceAttributes, Device } from "@guildion/core/src/adaptors/models/Device";
import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class DeviceRegisterRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/device/register',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DeviceRegisterRequestAttributes extends RequestParameters {
    device: DeviceAttributes,
}

export class DeviceRegisterRequest extends Record<DeviceRegisterRequestAttributes>({
    device: new Device().toJSON(),
}) {
    constructor(args: DeviceRegisterRequestAttributes) {
        super(args);
    }
    public getDevice(): Device | undefined {
        const result = this.get('device');
        return result ? (new Device(result)) : undefined;
    }
}

export interface DeviceRegisterResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    device: DeviceAttributes,
    csrfSecret: string,
}

export class DeviceRegisterResponse extends Record<DeviceRegisterResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    device: new Device().toJSON(),
    csrfSecret: '',
}) {
    constructor(args: DeviceRegisterResponseAttributes) {
        super(args);
    }

    public getDevice(): Device | undefined {
        const result = this.get('device');
        return result ? (new Device(result)) : undefined;
    }
}