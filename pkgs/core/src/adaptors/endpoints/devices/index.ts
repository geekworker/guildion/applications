import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { DeviceRegisterRequest, DeviceRegisterResponse, DeviceRegisterRouter } from "./register";
import { DeviceTrustRequest, DeviceTrustResponse, DeviceTrustRouter } from "./trust";
import { DeviceBlockRequest, DeviceBlockResponse, DeviceBlockRouter } from "./block";
import { DeviceUnBlockRequest, DeviceUnBlockResponse, DeviceUnBlockRouter } from "./unblock";
import { DevicesListRequest, DevicesListResponse, DevicesListRouter } from "./list";
import { DeviceBlocksListRequest, DeviceBlocksListResponse, DeviceBlocksListRouter } from "./blocksList";

export namespace DevicesEndpoint {
    export const Register: Endpoint<DeviceRegisterRouter, DeviceRegisterRequest, DeviceRegisterResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DeviceRegisterRouter,
        request: DeviceRegisterRequest,
        response: DeviceRegisterResponse,
        options: {},
    }
    export type Register = typeof Register;

    export const Trust: Endpoint<DeviceTrustRouter, DeviceTrustRequest, DeviceTrustResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DeviceTrustRouter,
        request: DeviceTrustRequest,
        response: DeviceTrustResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Trust = typeof Trust;

    export const Block: Endpoint<DeviceBlockRouter, DeviceBlockRequest, DeviceBlockResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DeviceBlockRouter,
        request: DeviceBlockRequest,
        response: DeviceBlockResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Block = typeof Block;

    export const UnBlock: Endpoint<DeviceUnBlockRouter, DeviceUnBlockRequest, DeviceUnBlockResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DeviceUnBlockRouter,
        request: DeviceUnBlockRequest,
        response: DeviceUnBlockResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type UnBlock = typeof UnBlock;

    export const List: Endpoint<DevicesListRouter, DevicesListRequest, DevicesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DevicesListRouter,
        request: DevicesListRequest,
        response: DevicesListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;

    export const BlocksList: Endpoint<DeviceBlocksListRouter, DeviceBlocksListRequest, DeviceBlocksListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DeviceBlocksListRouter,
        request: DeviceBlocksListRequest,
        response: DeviceBlocksListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type BlocksList = typeof BlocksList;
}