import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class DeviceTrustRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/device/trust',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DeviceTrustRequestAttributes extends RequestParameters {
    id: string,
}

export class DeviceTrustRequest extends Record<DeviceTrustRequestAttributes>({
    id: '',
}) {
    constructor(args: DeviceTrustRequestAttributes) {
        super(args);
    }
}

export interface DeviceTrustResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class DeviceTrustResponse extends Record<DeviceTrustResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: DeviceTrustResponseAttributes) {
        super(args);
    }
}