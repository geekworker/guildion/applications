import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { DeviceAttributes, Devices } from "@guildion/core/src/adaptors/models/Device";

export class DeviceBlocksListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/device/blocks/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DeviceBlocksListRequestAttributes extends RequestParameters {
    count?: number,
    offset?: number,
}

export class DeviceBlocksListRequest extends Record<DeviceBlocksListRequestAttributes>({
    count: undefined,
    offset: undefined,
}) {
    constructor(args: DeviceBlocksListRequestAttributes) {
        super(args);
    }
}

export interface DeviceBlocksListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    devices: DeviceAttributes[],
    paginatable: boolean,
}

export class DeviceBlocksListResponse extends Record<DeviceBlocksListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    devices: [],
    paginatable: false,
}) {
    constructor(args: DeviceBlocksListResponseAttributes) {
        super(args);
    }

    public getDevices(): Devices {
        const result = this.get('devices');
        return new Devices(result);
    }
}