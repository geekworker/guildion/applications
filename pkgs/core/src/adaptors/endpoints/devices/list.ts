import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { DeviceAttributes, Devices } from "@guildion/core/src/adaptors/models/Device";

export class DevicesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/devices/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DevicesListRequestAttributes extends RequestParameters {
    count?: number,
    offset?: number,
}

export class DevicesListRequest extends Record<DevicesListRequestAttributes>({
    count: undefined,
    offset: undefined,
}) {
    constructor(args: DevicesListRequestAttributes) {
        super(args);
    }
}

export interface DevicesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    devices: DeviceAttributes[],
    paginatable: boolean,
}

export class DevicesListResponse extends Record<DevicesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    devices: [],
    paginatable: false,
}) {
    constructor(args: DevicesListResponseAttributes) {
        super(args);
    }

    public getDevices(): Devices {
        const result = this.get('devices');
        return new Devices(result);
    }
}