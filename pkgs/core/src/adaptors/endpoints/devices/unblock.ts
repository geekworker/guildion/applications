import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class DeviceUnBlockRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/device/unblock',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DeviceUnBlockRequestAttributes extends RequestParameters {
    id: string,
}

export class DeviceUnBlockRequest extends Record<DeviceUnBlockRequestAttributes>({
    id: '',
}) {
    constructor(args: DeviceUnBlockRequestAttributes) {
        super(args);
    }
}

export interface DeviceUnBlockResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class DeviceUnBlockResponse extends Record<DeviceUnBlockResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: DeviceUnBlockResponseAttributes) {
        super(args);
    }
}