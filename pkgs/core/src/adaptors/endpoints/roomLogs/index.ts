import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RoomLogsListRequest, RoomLogsListResponse, RoomLogsListRouter } from "./list";

export namespace RoomLogsEndpoint {
    export const List: Endpoint<RoomLogsListRouter, RoomLogsListRequest, RoomLogsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomLogsListRouter,
        request: RoomLogsListRequest,
        response: RoomLogsListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;
}