import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomLogAttributes, RoomLogs } from "@guildion/core/src/adaptors/models/RoomLog";

export class RoomLogsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/logs/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomLogsListRequestAttributes extends RequestParameters {
    roomId: string,
    count?: number,
    offset?: number,
    gtAt?: string,
    ltAt?: string,
}

export class RoomLogsListRequest extends Record<RoomLogsListRequestAttributes>({
    roomId: '',
    count: undefined,
    offset: undefined,
    gtAt: undefined,
    ltAt: undefined,
}) {
    constructor(args: RoomLogsListRequestAttributes) {
        super(args);
    }

    public getGtAt(): Date | undefined {
        const result = this.get('gtAt');
        return result ? new Date(result) : undefined;
    }

    public getLtAt(): Date | undefined {
        const result = this.get('ltAt');
        return result ? new Date(result) : undefined;
    }
}

export interface RoomLogsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    logs: RoomLogAttributes[],
    paginatable: boolean,
}

export class RoomLogsListResponse extends Record<RoomLogsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    logs: [],
    paginatable: false,
}) {
    constructor(args: RoomLogsListResponseAttributes) {
        super(args);
    }

    public getLogs(): RoomLogs {
        const result = this.get('logs');
        return new RoomLogs(result);
    }
}