import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class MemberRoleMakeAdminRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/member/role/admin/make',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MemberRoleMakeAdminRequestAttributes extends RequestParameters {
    memberId: string,
}

export class MemberRoleMakeAdminRequest extends Record<MemberRoleMakeAdminRequestAttributes>({
    memberId: '',
}) {
    constructor(args: MemberRoleMakeAdminRequestAttributes) {
        super(args);
    }
}

export interface MemberRoleMakeAdminResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class MemberRoleMakeAdminResponse extends Record<MemberRoleMakeAdminResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: MemberRoleMakeAdminResponseAttributes) {
        super(args);
    }
}