import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { MemberRolesListRequest, MemberRolesListResponse, MemberRolesListRouter } from "./list";
import { MemberRoleMakeAdminRequest, MemberRoleMakeAdminResponse, MemberRoleMakeAdminRouter } from "./makeAdmin";
import { MemberRoleRevertMembersRequest, MemberRoleRevertMembersResponse, MemberRoleRevertMembersRouter } from "./revertMembers";

export namespace MemberRolesEndpoint {
    export const List: Endpoint<MemberRolesListRouter, MemberRolesListRequest, MemberRolesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MemberRolesListRouter,
        request: MemberRolesListRequest,
        response: MemberRolesListResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type List = typeof List;

    export const MakeAdmin: Endpoint<MemberRoleMakeAdminRouter, MemberRoleMakeAdminRequest, MemberRoleMakeAdminResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MemberRoleMakeAdminRouter,
        request: MemberRoleMakeAdminRequest,
        response: MemberRoleMakeAdminResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type MakeAdmin = typeof MakeAdmin;

    export const RevertMembers: Endpoint<MemberRoleRevertMembersRouter, MemberRoleRevertMembersRequest, MemberRoleRevertMembersResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: MemberRoleRevertMembersRouter,
        request: MemberRoleRevertMembersRequest,
        response: MemberRoleRevertMembersResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type RevertMembers = typeof RevertMembers;
}