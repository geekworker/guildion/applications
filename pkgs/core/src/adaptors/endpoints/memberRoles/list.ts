import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoleAttributes, Roles } from "@guildion/core/src/adaptors/models/Role";

export class MemberRolesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/member/roles/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MemberRolesListRequestAttributes extends RequestParameters {
    memberId: string,
}

export class MemberRolesListRequest extends Record<MemberRolesListRequestAttributes>({
    memberId: '',
}) {
    constructor(args: MemberRolesListRequestAttributes) {
        super(args);
    }
}

export interface MemberRolesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    roles: RoleAttributes[],
}

export class MemberRolesListResponse extends Record<MemberRolesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    roles: [],
}) {
    constructor(args: MemberRolesListResponseAttributes) {
        super(args);
    }

    public getRoles(): Roles {
        const result = this.get('roles');
        return new Roles(result);
    }
}