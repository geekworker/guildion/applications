import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class MemberRoleRevertMembersRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/member/role/members/revert',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface MemberRoleRevertMembersRequestAttributes extends RequestParameters {
    memberId: string,
}

export class MemberRoleRevertMembersRequest extends Record<MemberRoleRevertMembersRequestAttributes>({
    memberId: '',
}) {
    constructor(args: MemberRoleRevertMembersRequestAttributes) {
        super(args);
    }
}

export interface MemberRoleRevertMembersResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class MemberRoleRevertMembersResponse extends Record<MemberRoleRevertMembersResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: MemberRoleRevertMembersResponseAttributes) {
        super(args);
    }
}