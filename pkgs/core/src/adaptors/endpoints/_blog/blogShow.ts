import { GET_CURRENT_BLOG_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class BlogShowRouter extends Router.Factory<{ slug: string }, {}> {
    constructor() {
        super(
            '/blogs/:slug',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath({ slug }: { slug: string }): string {
        return GET_CURRENT_BLOG_URL_STRING() + this.toPath$({ slug });
    }
}

export const blogShowRouter = new BlogShowRouter();