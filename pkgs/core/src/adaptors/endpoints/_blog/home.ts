import { GET_CURRENT_BLOG_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class HomeRouter extends Router.Factory<{}, {}> {
    constructor() {
        super(
            '/',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_BLOG_URL_STRING() + this.toPath$({});
    }
}

export const homeRouter = new HomeRouter();