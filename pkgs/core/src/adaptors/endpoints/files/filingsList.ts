import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files } from "@guildion/core/src/adaptors/models/File";

export class FileFilingsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/filings/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileFilingsListRequestAttributes extends RequestParameters {
    id: string,
    count?: number,
    offset?: number,
    isRandom?: boolean,
}

export class FileFilingsListRequest extends Record<FileFilingsListRequestAttributes>({
    id: '',
    count: undefined,
    offset: undefined,
    isRandom: undefined,
}) {
    constructor(args: FileFilingsListRequestAttributes) {
        super(args);
    }
}

export interface FileFilingsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files: FileAttributes[],
    paginatable: boolean,
}

export class FileFilingsListResponse extends Record<FileFilingsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: [],
    paginatable: false,
}) {
    constructor(args: FileFilingsListResponseAttributes) {
        super(args);
    }

    public getFiles(): Files {
        const result = this.get('files');
        return !!result ? new Files(result) : new Files([]);
    }
}