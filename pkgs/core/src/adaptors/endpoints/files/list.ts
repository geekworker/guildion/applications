import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files } from "@guildion/core/src/adaptors/models/File";

export class FilesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/files/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FilesListRequestAttributes extends RequestParameters {
    isJSONString: boolean,
}

export class FilesListRequest extends Record<FilesListRequestAttributes>({
    isJSONString: false,
}) {
    constructor(args: FilesListRequestAttributes) {
        super(args);
    }
}

export interface FilesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files?: FileAttributes[],
    filesjsonstring?: string,
}

export class FilesListResponse extends Record<FilesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: undefined,
    filesjsonstring: undefined,
}) {
    constructor(args: FilesListResponseAttributes) {
        super(args);
    }

    public getFiles(): Files | undefined {
        const result = this.get('files');
        return !!result ? new Files(result) : undefined;
    }

    public getFilesFromJSON(): Files | undefined {
        const result = this.get('filesjsonstring');
        return !!result ? new Files(JSON.parse(result)) : undefined;
    }
}