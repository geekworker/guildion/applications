import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { File, FileAttributes } from "@guildion/core/src/adaptors/models/File";

export class FileUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/file/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileUpdateRequestAttributes extends RequestParameters {
    file: Partial<FileAttributes> & { id: string },
}

export class FileUpdateRequest extends Record<FileUpdateRequestAttributes>({
    file: { ...new File().toJSON(), id: '' },
}) {
    constructor(args: FileUpdateRequestAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}

export interface FileUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    file: FileAttributes,
}

export class FileUpdateResponse extends Record<FileUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    file: new File().toJSON(),
}) {
    constructor(args: FileUpdateResponseAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}