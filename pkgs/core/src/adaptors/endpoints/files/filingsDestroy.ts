import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class FileFilingsDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/filings/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileFilingsDestroyRequestAttributes extends RequestParameters {
    filingIds: string[],
}

export class FileFilingsDestroyRequest extends Record<FileFilingsDestroyRequestAttributes>({
    filingIds: [],
}) {
    constructor(args: FileFilingsDestroyRequestAttributes) {
        super(args);
    }
}

export interface FileFilingsDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class FileFilingsDestroyResponse extends Record<FileFilingsDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: FileFilingsDestroyResponseAttributes) {
        super(args);
    }
}