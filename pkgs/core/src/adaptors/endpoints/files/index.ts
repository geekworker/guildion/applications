import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { FilesListRequest, FilesListResponse, FilesListRouter } from "./list";
import { FileFilingsCreateRequest, FileFilingsCreateResponse, FileFilingsCreateRouter } from "./filingsCreate";
import { FileFilingsUpdateRequest, FileFilingsUpdateResponse, FileFilingsUpdateRouter } from "./filingsUpdate";
import { FileFilingsDestroyRequest, FileFilingsDestroyResponse, FileFilingsDestroyRouter } from "./filingsDestroy";
import { FileFilingsListRequest, FileFilingsListResponse, FileFilingsListRouter } from "./filingsList";
import { FileProfilePresignRequest, FileProfilePresignResponse, FileProfilePresignRouter } from "./profilePresign";
import { FileShowRequest, FileShowResponse, FileShowRouter } from "./show";
import { FileUploadRequest, FileUploadResponse, FileUploadRouter } from "./upload";
import { FileUpdateRequest, FileUpdateResponse, FileUpdateRouter } from "./update";
import { FileDestroyRequest, FileDestroyResponse, FileDestroyRouter } from "./destroy";

export namespace FilesEndpoint {
    export const List: Endpoint<FilesListRouter, FilesListRequest, FilesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FilesListRouter,
        request: FilesListRequest,
        response: FilesListResponse,
        options: { deviceRequired: true, accessTokenRequired: false, skipLogging: true, },
    }
    export type List = typeof List;

    export const FilingsList: Endpoint<FileFilingsListRouter, FileFilingsListRequest, FileFilingsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileFilingsListRouter,
        request: FileFilingsListRequest,
        response: FileFilingsListResponse,
        options: { deviceRequired: true, accessTokenRequired: false, },
    }
    export type FilingsList = typeof FilingsList;

    export const FilingsCreate: Endpoint<FileFilingsCreateRouter, FileFilingsCreateRequest, FileFilingsCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileFilingsCreateRouter,
        request: FileFilingsCreateRequest,
        response: FileFilingsCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true, },
    }
    export type FilingsCreate = typeof FilingsCreate;

    export const FilingsUpdate: Endpoint<FileFilingsUpdateRouter, FileFilingsUpdateRequest, FileFilingsUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileFilingsUpdateRouter,
        request: FileFilingsUpdateRequest,
        response: FileFilingsUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true, },
    }
    export type FilingsUpdate = typeof FilingsUpdate;

    export const FilingsDestroy: Endpoint<FileFilingsDestroyRouter, FileFilingsDestroyRequest, FileFilingsDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileFilingsDestroyRouter,
        request: FileFilingsDestroyRequest,
        response: FileFilingsDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true, },
    }
    export type FilingsDestroy = typeof FilingsDestroy;

    export const Show: Endpoint<FileShowRouter, FileShowRequest, FileShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileShowRouter,
        request: FileShowRequest,
        response: FileShowResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Show = typeof Show;

    export const Update: Endpoint<FileUpdateRouter, FileUpdateRequest, FileUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileUpdateRouter,
        request: FileUpdateRequest,
        response: FileUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type Update = typeof Update;
    
    export const Upload: Endpoint<FileUploadRouter, FileUploadRequest, FileUploadResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileUploadRouter,
        request: FileUploadRequest,
        response: FileUploadResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type Upload = typeof Upload;

    export const ProfilePresign: Endpoint<FileProfilePresignRouter, FileProfilePresignRequest, FileProfilePresignResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileProfilePresignRouter,
        request: FileProfilePresignRequest,
        response: FileProfilePresignResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type ProfilePresign = typeof ProfilePresign;

    export const Destroy: Endpoint<FileDestroyRouter, FileDestroyRequest, FileDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: FileDestroyRouter,
        request: FileDestroyRequest,
        response: FileDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type Destroy = typeof Destroy;
}