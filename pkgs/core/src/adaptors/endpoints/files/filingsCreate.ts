import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files } from "@guildion/core/src/adaptors/models/File";

export class FileFilingsCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/filings/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileFilingsCreateRequestAttributes extends RequestParameters {
    fileIds: string[],
    folderId: string,
}

export class FileFilingsCreateRequest extends Record<FileFilingsCreateRequestAttributes>({
    fileIds: [],
    folderId: '',
}) {
    constructor(args: FileFilingsCreateRequestAttributes) {
        super(args);
    }
}

export interface FileFilingsCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files: FileAttributes[],
}

export class FileFilingsCreateResponse extends Record<FileFilingsCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: [],
}) {
    constructor(args: FileFilingsCreateResponseAttributes) {
        super(args);
    }

    public getFiles(): Files {
        const result = this.get('files');
        return !!result ? new Files(result) : new Files([]);
    }
}