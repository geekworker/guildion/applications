import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class FileUploadRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/file/upload',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileUploadRequestAttributes extends RequestParameters {
    id: string,
}

export class FileUploadRequest extends Record<FileUploadRequestAttributes>({
    id: '',
}) {
    constructor(args: FileUploadRequestAttributes) {
        super(args);
    }
}

export interface FileUploadResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class FileUploadResponse extends Record<FileUploadResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: FileUploadResponseAttributes) {
        super(args);
    }
}