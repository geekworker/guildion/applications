import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, Files } from "@guildion/core/src/adaptors/models/File";

export class FileFilingsUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/filings/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileFilingsUpdateRequestAttributes extends RequestParameters {
    files: (Partial<FileAttributes> & { filingId: string })[],
    folderId: string,
}

export class FileFilingsUpdateRequest extends Record<FileFilingsUpdateRequestAttributes>({
    files: [],
    folderId: '',
}) {
    constructor(args: FileFilingsUpdateRequestAttributes) {
        super(args);
    }
}

export interface FileFilingsUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    files: FileAttributes[],
}

export class FileFilingsUpdateResponse extends Record<FileFilingsUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    files: [],
}) {
    constructor(args: FileFilingsUpdateResponseAttributes) {
        super(args);
    }

    public getFiles(): Files {
        const result = this.get('files');
        return !!result ? new Files(result) : new Files([]);
    }
}