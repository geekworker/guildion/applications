import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { File, FileAttributes } from "@guildion/core/src/adaptors/models/File";

export class FileProfilePresignRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/profile/presign',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileProfilePresignRequestAttributes extends RequestParameters {
    file: FileAttributes,
}

export class FileProfilePresignRequest extends Record<FileProfilePresignRequestAttributes>({
    file: new File().toJSON(),
}) {
    constructor(args: FileProfilePresignRequestAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}

export interface FileProfilePresignResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    file: FileAttributes,
}

export class FileProfilePresignResponse extends Record<FileProfilePresignResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    file: new File().toJSON(),
}) {
    constructor(args: FileProfilePresignResponseAttributes) {
        super(args);
    }

    public getFile(): File | undefined {
        const result = this.get('file');
        return result ? (new File(result)) : undefined;
    }
}