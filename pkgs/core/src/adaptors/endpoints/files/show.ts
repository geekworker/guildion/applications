import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { FileAttributes, File } from "@guildion/core/src/adaptors/models/File";

export class FileShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/file/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileShowRequestAttributes extends RequestParameters {
    id: string,
}

export class FileShowRequest extends Record<FileShowRequestAttributes>({
    id: '',
}) {
    constructor(args: FileShowRequestAttributes) {
        super(args);
    }
}

export interface FileShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    file: FileAttributes,
}

export class FileShowResponse extends Record<FileShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    file: new File().toJSON(),
}) {
    constructor(args: FileShowResponseAttributes) {
        super(args);
    }

    public getFile(): File {
        const result = this.get('file');
        return new File(result);
    }
}