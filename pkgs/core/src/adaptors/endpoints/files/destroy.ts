import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class FileDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/file/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface FileDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class FileDestroyRequest extends Record<FileDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: FileDestroyRequestAttributes) {
        super(args);
    }
}

export interface FileDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class FileDestroyResponse extends Record<FileDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: FileDestroyResponseAttributes) {
        super(args);
    }
}