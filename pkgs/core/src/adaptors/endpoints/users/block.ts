import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class UserBlockRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/block',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserBlockRequestAttributes extends RequestParameters {
    id: string,
}

export class UserBlockRequest extends Record<UserBlockRequestAttributes>({
    id: '',
}) {
    constructor(args: UserBlockRequestAttributes) {
        super(args);
    }
}

export interface UserBlockResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class UserBlockResponse extends Record<UserBlockResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: UserBlockResponseAttributes) {
        super(args);
    }
}