import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { UserUpdateRequest, UserUpdateResponse, UserUpdateRouter } from "./update";
import { UserDestroyRequest, UserDestroyResponse, UserDestroyRouter } from "./destroy";
import { UserBlockRequest, UserBlockResponse, UserBlockRouter } from "./block";
import { UserUnBlockRequest, UserUnBlockResponse, UserUnBlockRouter } from "./unblock";
import { UserMuteRequest, UserMuteResponse, UserMuteRouter } from "./mute";
import { UserUnMuteRequest, UserUnMuteResponse, UserUnMuteRouter } from "./unmute";
import { UserConnectRequest, UserConnectResponse, UserConnectRouter } from "./connect";
import { UserDisconnectRequest, UserDisconnectResponse, UserDisconnectRouter } from "./disconnect";

export namespace UsersEndpoint {
    export const Update: Endpoint<UserUpdateRouter, UserUpdateRequest, UserUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserUpdateRouter,
        request: UserUpdateRequest,
        response: UserUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<UserDestroyRouter, UserDestroyRequest, UserDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserDestroyRouter,
        request: UserDestroyRequest,
        response: UserDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const Block: Endpoint<UserBlockRouter, UserBlockRequest, UserBlockResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserBlockRouter,
        request: UserBlockRequest,
        response: UserBlockResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Block = typeof Block;

    export const UnBlock: Endpoint<UserUnBlockRouter, UserUnBlockRequest, UserUnBlockResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserUnBlockRouter,
        request: UserUnBlockRequest,
        response: UserUnBlockResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type UnBlock = typeof UnBlock;

    export const Mute: Endpoint<UserMuteRouter, UserMuteRequest, UserMuteResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserMuteRouter,
        request: UserMuteRequest,
        response: UserMuteResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Mute = typeof Mute;
    
    export const UnMute: Endpoint<UserUnMuteRouter, UserUnMuteRequest, UserUnMuteResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserUnMuteRouter,
        request: UserUnMuteRequest,
        response: UserUnMuteResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type UnMute = typeof UnMute;

    export const Connect: Endpoint<UserConnectRouter, UserConnectRequest, UserConnectResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserConnectRouter,
        request: UserConnectRequest,
        response: UserConnectResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Connect = typeof Connect;

    export const Disconnect: Endpoint<UserDisconnectRouter, UserDisconnectRequest, UserDisconnectResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: UserDisconnectRouter,
        request: UserDisconnectRequest,
        response: UserDisconnectResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Disconnect = typeof Disconnect;
}