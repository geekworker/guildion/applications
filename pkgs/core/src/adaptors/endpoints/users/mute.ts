import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class UserMuteRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/mute',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserMuteRequestAttributes extends RequestParameters {
    id: string,
}

export class UserMuteRequest extends Record<UserMuteRequestAttributes>({
    id: '',
}) {
    constructor(args: UserMuteRequestAttributes) {
        super(args);
    }
}

export interface UserMuteResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class UserMuteResponse extends Record<UserMuteResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: UserMuteResponseAttributes) {
        super(args);
    }
}