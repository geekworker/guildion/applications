import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class UserDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class UserDestroyRequest extends Record<UserDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: UserDestroyRequestAttributes) {
        super(args);
    }
}

export interface UserDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class UserDestroyResponse extends Record<UserDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: UserDestroyResponseAttributes) {
        super(args);
    }
}