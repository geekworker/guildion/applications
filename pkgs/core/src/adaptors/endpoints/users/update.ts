import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { User, UserAttributes } from "@guildion/core/src/adaptors/models/User";

export class UserUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserUpdateRequestAttributes extends RequestParameters {
    user: Partial<UserAttributes>,
}

export class UserUpdateRequest extends Record<UserUpdateRequestAttributes>({
    user: { ...new User().toJSON() },
}) {
    constructor(args: UserUpdateRequestAttributes) {
        super(args);
    }

    public getUser(): User | undefined {
        const result = this.get('user');
        return result ? (new User(result)) : undefined;
    }
}

export interface UserUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    user: UserAttributes,
}

export class UserUpdateResponse extends Record<UserUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    user: new User().toJSON(),
}) {
    constructor(args: UserUpdateResponseAttributes) {
        super(args);
    }

    public getUser(): User | undefined {
        const result = this.get('user');
        return result ? (new User(result)) : undefined;
    }
}