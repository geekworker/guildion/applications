import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class UserDisconnectRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/disconnect',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserDisconnectRequestAttributes extends RequestParameters {
}

export class UserDisconnectRequest extends Record<UserDisconnectRequestAttributes>({
}) {
    constructor(args: UserDisconnectRequestAttributes) {
        super(args);
    }
}

export interface UserDisconnectResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class UserDisconnectResponse extends Record<UserDisconnectResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: UserDisconnectResponseAttributes) {
        super(args);
    }
}