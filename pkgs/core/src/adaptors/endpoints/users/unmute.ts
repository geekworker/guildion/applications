import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class UserUnMuteRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/unmute',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserUnMuteRequestAttributes extends RequestParameters {
    id: string,
}

export class UserUnMuteRequest extends Record<UserUnMuteRequestAttributes>({
    id: '',
}) {
    constructor(args: UserUnMuteRequestAttributes) {
        super(args);
    }
}

export interface UserUnMuteResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class UserUnMuteResponse extends Record<UserUnMuteResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: UserUnMuteResponseAttributes) {
        super(args);
    }
}