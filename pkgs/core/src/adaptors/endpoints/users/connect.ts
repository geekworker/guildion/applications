import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class UserConnectRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/user/connect',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface UserConnectRequestAttributes extends RequestParameters {
}

export class UserConnectRequest extends Record<UserConnectRequestAttributes>({
}) {
    constructor(args: UserConnectRequestAttributes) {
        super(args);
    }
}

export interface UserConnectResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class UserConnectResponse extends Record<UserConnectResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: UserConnectResponseAttributes) {
        super(args);
    }
}