import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { ConnectionCreateRequest, ConnectionCreateResponse, ConnectionCreateRouter } from "./create";
import { ConnectionDestroyRequest, ConnectionDestroyResponse, ConnectionDestroyRouter } from "./destroy";
import { AdministratorRole } from "@guildion/core/src/adaptors/models/Administrator";

export namespace ConnectionsEndpoint {
    export const Create: Endpoint<ConnectionCreateRouter, ConnectionCreateRequest, ConnectionCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: ConnectionCreateRouter,
        request: ConnectionCreateRequest,
        response: ConnectionCreateResponse,
        options: { deviceRequired: true, adminRoles: [AdministratorRole.CONNECT_SERVER] },
    }
    export type Create = typeof Create;
    export const Destroy: Endpoint<ConnectionDestroyRouter, ConnectionDestroyRequest, ConnectionDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: ConnectionDestroyRouter,
        request: ConnectionDestroyRequest,
        response: ConnectionDestroyResponse,
        options: { deviceRequired: true, adminRoles: [AdministratorRole.CONNECT_SERVER] },
    }
    export type Destroy = typeof Destroy;
}