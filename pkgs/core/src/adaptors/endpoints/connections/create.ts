import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Connection, ConnectionAttributes } from "@guildion/core/src/adaptors/models/Connection";

export class ConnectionCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/connection/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface ConnectionCreateRequestAttributes extends RequestParameters {
    connection: ConnectionAttributes,
    deviceId: string,
}

export class ConnectionCreateRequest extends Record<ConnectionCreateRequestAttributes>({
    connection: new Connection().toJSON(),
    deviceId: '',
}) {
    constructor(args: ConnectionCreateRequestAttributes) {
        super(args);
    }

    public getConnection(): Connection | undefined {
        const result = this.get('connection');
        return result ? (new Connection(result)) : undefined;
    }
}

export interface ConnectionCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    connection: ConnectionAttributes,
}

export class ConnectionCreateResponse extends Record<ConnectionCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    connection: new Connection().toJSON(),
}) {
    constructor(args: ConnectionCreateResponseAttributes) {
        super(args);
    }

    public getConnection(): Connection | undefined {
        const result = this.get('connection');
        return result ? (new Connection(result)) : undefined;
    }
}