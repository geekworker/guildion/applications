import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class ConnectionDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/connection/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface ConnectionDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class ConnectionDestroyRequest extends Record<ConnectionDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: ConnectionDestroyRequestAttributes) {
        super(args);
    }
}

export interface ConnectionDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class ConnectionDestroyResponse extends Record<ConnectionDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: ConnectionDestroyResponseAttributes) {
        super(args);
    }
}