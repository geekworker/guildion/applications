import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Discoveries, DiscoveryAttributes } from "../../models/Discovery";

export class DiscoveriesRecommendRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/discoveries/recommend',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface DiscoveriesRecommendRequestAttributes extends RequestParameters {
    count?: number,
    offset?: number,
}

export class DiscoveriesRecommendRequest extends Record<DiscoveriesRecommendRequestAttributes>({
    count: undefined,
    offset: undefined,
}) {
    constructor(args: DiscoveriesRecommendRequestAttributes) {
        super(args);
    }
}

export interface DiscoveriesRecommendResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    discoveries: DiscoveryAttributes[],
    paginatable: boolean,
}

export class DiscoveriesRecommendResponse extends Record<DiscoveriesRecommendResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    discoveries: [],
    paginatable: false,
}) {
    constructor(args: DiscoveriesRecommendResponseAttributes) {
        super(args);
    }

    public getDiscoveries(): Discoveries {
        const result = this.get('discoveries');
        return !!result ? new Discoveries(result) : new Discoveries([]);
    }
}