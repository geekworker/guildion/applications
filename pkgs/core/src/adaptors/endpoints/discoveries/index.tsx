import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { DiscoveriesRecommendRequest, DiscoveriesRecommendResponse, DiscoveriesRecommendRouter } from "./recommend";

export namespace DiscoveriesEndpoint {
    export const Recommend: Endpoint<DiscoveriesRecommendRouter, DiscoveriesRecommendRequest, DiscoveriesRecommendResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: DiscoveriesRecommendRouter,
        request: DiscoveriesRecommendRequest,
        response: DiscoveriesRecommendResponse,
        options: { deviceRequired: true },
    }
    export type Recommend = typeof Recommend;
}