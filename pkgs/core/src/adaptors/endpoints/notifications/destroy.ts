import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class NotificationDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/notification/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface NotificationDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class NotificationDestroyRequest extends Record<NotificationDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: NotificationDestroyRequestAttributes) {
        super(args);
    }
}

export interface NotificationDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class NotificationDestroyResponse extends Record<NotificationDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: NotificationDestroyResponseAttributes) {
        super(args);
    }
}