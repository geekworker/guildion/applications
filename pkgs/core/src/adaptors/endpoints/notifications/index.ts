import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { NotificationsListRequest, NotificationsListResponse, NotificationsListRouter } from "./list";
import { NotificationDestroyRequest, NotificationDestroyResponse, NotificationDestroyRouter } from "./destroy";
import { NotificationReadRequest, NotificationReadResponse, NotificationReadRouter } from "./read";

export namespace NotificationsEndpoint {
    export const List: Endpoint<NotificationsListRouter, NotificationsListRequest, NotificationsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: NotificationsListRouter,
        request: NotificationsListRequest,
        response: NotificationsListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;

    export const Destroy: Endpoint<NotificationDestroyRouter, NotificationDestroyRequest, NotificationDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: NotificationDestroyRouter,
        request: NotificationDestroyRequest,
        response: NotificationDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const Read: Endpoint<NotificationReadRouter, NotificationReadRequest, NotificationReadResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: NotificationReadRouter,
        request: NotificationReadRequest,
        response: NotificationReadResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Read = typeof Read;
}