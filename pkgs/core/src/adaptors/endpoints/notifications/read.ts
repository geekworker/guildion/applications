import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class NotificationReadRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/notification/read',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface NotificationReadRequestAttributes extends RequestParameters {
}

export class NotificationReadRequest extends Record<NotificationReadRequestAttributes>({
}) {
    constructor(args: NotificationReadRequestAttributes) {
        super(args);
    }
}

export interface NotificationReadResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class NotificationReadResponse extends Record<NotificationReadResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: NotificationReadResponseAttributes) {
        super(args);
    }
}