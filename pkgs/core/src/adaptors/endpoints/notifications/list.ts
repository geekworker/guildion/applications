import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { NotificationAttributes, Notifications } from "@guildion/core/src/adaptors/models/Notification";

export class NotificationsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/notifications/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface NotificationsListRequestAttributes extends RequestParameters {
    count?: number,
    offset?: number,
}

export class NotificationsListRequest extends Record<NotificationsListRequestAttributes>({
    count: undefined,
    offset: undefined,
}) {
    constructor(args: NotificationsListRequestAttributes) {
        super(args);
    }
}

export interface NotificationsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    notifications: NotificationAttributes[],
    paginatable: boolean,
}

export class NotificationsListResponse extends Record<NotificationsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    notifications: [],
    paginatable: false,
}) {
    constructor(args: NotificationsListResponseAttributes) {
        super(args);
    }

    public getNotifications(): Notifications {
        const result = this.get('notifications');
        return new Notifications(result);
    }
}