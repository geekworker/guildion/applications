import { pathToRegexp, compile, PathFunction, match, MatchFunction } from 'path-to-regexp';
import querystring from 'querystring';
import { localizablePath } from '../../locales';

export namespace Router {
    export type FactoryOfParameters<T extends Factory<any, any>> = T extends Factory<infer Params, any> ? Params : any;
    export type FactoryOfQuery<T extends Factory<any, any>> = T extends Factory<any, infer Query> ? Query : any;

    export class Factory<P, Q = {}> {
        public static ESC_PARAMS: string = ':';
        public static ESC_OPTIONAL: string = '?';

        protected path$: string;
        public compiler: PathFunction;
        public matcher: MatchFunction;
        public regexp: RegExp;
        public isNotFound: boolean = false;
        public isRoot: boolean = false;
        public isDevelopment: boolean = false;
        public localizable: boolean = false;

        constructor(
            path: string,
            {
                isNotFound,
                isRoot,
                isDevelopment,
                localizable,
            }: {
                isNotFound?: boolean,
                isRoot?: boolean,
                isDevelopment?: boolean,
                localizable?: boolean,
            }
        ) {
            if (localizable && !isRoot) {
                path = localizablePath + path;
            }
            this.path$ = path;
            this.compiler = compile(path, { encode: encodeURIComponent });
            this.regexp = pathToRegexp(path);
            this.matcher = match(path, { decode: decodeURIComponent });
            this.isNotFound = isNotFound || false;
            this.isRoot = isRoot || false;
            this.isDevelopment = isDevelopment || false;
            this.localizable = localizable || false;
        }

        public toPath(params: P): string { return '' }

        protected toPath$(params?: P, query?: Q): string {
            const queries = query ? this.generateQuery$(query) : '';
            return this.compiler(params || {}) + queries;
        }

        protected generateQuery$(query: Q): string {
            return `${Factory.ESC_OPTIONAL + querystring.stringify(query as any)}`;
        }

        public resolve(cp: string): boolean {
            return !!this.matcher(cp);
        }

        public get path(): string {
            return this.path$;
        }
    }

    export class Factories {
        public readonly items: Factory<any>[];
        public readonly routePath: string = '/';
        public readonly notfoundPath: string = '/notfound';
        public readonly routeFactory: Factory<any>;
        public readonly notfoundFactory: Factory<any>;

        constructor(
            items: Factory<any>[],
            routePath?: string,
            notfoundPath?: string,
        ) {
            this.items = items;
            this.routePath = routePath || '/';
            this.notfoundPath = notfoundPath || '/notfound';
            this.routeFactory = this.items.filter(item => item.isRoot)[0];
            this.notfoundFactory = this.items.filter(item => item.isNotFound)[0];
        }
    
        resolve(path: string): Factory<any> {
            const results = this.items.filter(val => val.resolve(path))
            return results.length > 0 ? results[0] : this.notfoundFactory;
        }
    }
}