import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { BlogAttributes, Blogs } from "@guildion/core/src/adaptors/models/Blog";
import { SortDirection } from "../../../constants/SortDirection";
import { SortType } from "../../../constants/SortType";
import { BlogCategory, BlogCategoryAttributes } from "../../models/BlogCategory";

export class BlogsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/blogs/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface BlogsListRequestAttributes extends RequestParameters {
    categorySlug?: string,
    slug?: string,
    count?: number,
    offset?: number,
    sort?: {
        direction?: SortDirection,
        type?: SortType,
    },
}

export class BlogsListRequest extends Record<BlogsListRequestAttributes>({
    categorySlug: undefined,
    slug: undefined,
    count: undefined,
    offset: undefined,
    sort: undefined,
}) {
    constructor(args: BlogsListRequestAttributes) {
        super(args);
    }
}

export interface BlogsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    blogs: BlogAttributes[],
    category?: BlogCategoryAttributes,
    paginatable: boolean,
}

export class BlogsListResponse extends Record<BlogsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    blogs: [],
    category: undefined,
    paginatable: false,
}) {
    constructor(args: BlogsListResponseAttributes) {
        super(args);
    }

    public getBlogs(): Blogs {
        const result = this.get('blogs');
        return new Blogs(result);
    }

    public getCategory(): BlogCategory | undefined {
        const result = this.get('category');
        return result ? new BlogCategory(result) : undefined;
    }
}