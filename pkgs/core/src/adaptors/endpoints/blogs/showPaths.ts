import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { LanguageCode } from "@guildion/core/src/constants/LanguageCode";

export class BlogsShowPathsRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/blog/show/paths',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface BlogsShowPathsRequestAttributes extends RequestParameters {
}

export class BlogsShowPathsRequest extends Record<BlogsShowPathsRequestAttributes>({
}) {
    constructor(args: BlogsShowPathsRequestAttributes) {
        super(args);
    }
}

export interface BlogsShowPathsResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    paths: { params: { slug: string }, locale: LanguageCode }[],
}

export class BlogsShowPathsResponse extends Record<BlogsShowPathsResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    paths: [],
}) {
    constructor(args: BlogsShowPathsResponseAttributes) {
        super(args);
    }
}