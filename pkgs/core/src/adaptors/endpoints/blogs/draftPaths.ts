import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { LanguageCode } from "@guildion/core/src/constants/LanguageCode";

export class BlogsDraftPathsRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/blog/draft/paths',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface BlogsDraftPathsRequestAttributes extends RequestParameters {
}

export class BlogsDraftPathsRequest extends Record<BlogsDraftPathsRequestAttributes>({
}) {
    constructor(args: BlogsDraftPathsRequestAttributes) {
        super(args);
    }
}

export interface BlogsDraftPathsResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    paths: { params: { slug: string }, locale: LanguageCode }[],
}

export class BlogsDraftPathsResponse extends Record<BlogsDraftPathsResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    paths: [],
}) {
    constructor(args: BlogsDraftPathsResponseAttributes) {
        super(args);
    }
}