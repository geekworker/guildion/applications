import { Endpoint } from "..";
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { BlogsListRequest, BlogsListResponse, BlogsListRouter } from "./list";
import { BlogShowRequest, BlogShowResponse, BlogShowRouter } from "./show";
import { BlogDraftRequest, BlogDraftResponse, BlogDraftRouter } from "./draft";
import { BlogsShowPathsRequest, BlogsShowPathsResponse, BlogsShowPathsRouter } from "./showPaths";
import { BlogsDraftPathsRequest, BlogsDraftPathsResponse, BlogsDraftPathsRouter } from "./draftPaths";

export namespace BlogsEndpoint {
    export const List: Endpoint<BlogsListRouter, BlogsListRequest, BlogsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: BlogsListRouter,
        request: BlogsListRequest,
        response: BlogsListResponse,
        options: { deviceRequired: false },
    }
    export type List = typeof List;

    export const Show: Endpoint<BlogShowRouter, BlogShowRequest, BlogShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: BlogShowRouter,
        request: BlogShowRequest,
        response: BlogShowResponse,
        options: { deviceRequired: false },
    }
    export type Show = typeof Show;
    
    export const Draft: Endpoint<BlogDraftRouter, BlogDraftRequest, BlogDraftResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: BlogDraftRouter,
        request: BlogDraftRequest,
        response: BlogDraftResponse,
        options: { deviceRequired: false },
    }
    export type Draft = typeof Draft;

    export const ShowPaths: Endpoint<BlogsShowPathsRouter, BlogsShowPathsRequest, BlogsShowPathsResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: BlogsShowPathsRouter,
        request: BlogsShowPathsRequest,
        response: BlogsShowPathsResponse,
        options: { deviceRequired: false },
    }
    export type ShowPaths = typeof ShowPaths;

    export const DraftPaths: Endpoint<BlogsDraftPathsRouter, BlogsDraftPathsRequest, BlogsDraftPathsResponse> = {
        method: HTTPMethod.GET,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: BlogsDraftPathsRouter,
        request: BlogsDraftPathsRequest,
        response: BlogsDraftPathsResponse,
        options: { deviceRequired: false },
    }
    export type DraftPaths = typeof DraftPaths;
}