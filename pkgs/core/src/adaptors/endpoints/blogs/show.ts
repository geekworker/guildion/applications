import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { BlogAttributes, Blog } from "@guildion/core/src/adaptors/models/Blog";

export class BlogShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/blog/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface BlogShowRequestAttributes extends RequestParameters {
    slug: string,
}

export class BlogShowRequest extends Record<BlogShowRequestAttributes>({
    slug: '',
}) {
    constructor(args: BlogShowRequestAttributes) {
        super(args);
    }
}

export interface BlogShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    blog: BlogAttributes,
}

export class BlogShowResponse extends Record<BlogShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    blog: new Blog().toJSON(),
}) {
    constructor(args: BlogShowResponseAttributes) {
        super(args);
    }

    public getBlog(): Blog {
        const result = this.get('blog');
        return new Blog(result);
    }
}