import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { BlogAttributes, Blog } from "@guildion/core/src/adaptors/models/Blog";

export class BlogDraftRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/blog/draft',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface BlogDraftRequestAttributes extends RequestParameters {
    slug: string,
}

export class BlogDraftRequest extends Record<BlogDraftRequestAttributes>({
    slug: '',
}) {
    constructor(args: BlogDraftRequestAttributes) {
        super(args);
    }
}

export interface BlogDraftResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    blog: BlogAttributes,
}

export class BlogDraftResponse extends Record<BlogDraftResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    blog: new Blog().toJSON(),
}) {
    constructor(args: BlogDraftResponseAttributes) {
        super(args);
    }

    public getBlog(): Blog {
        const result = this.get('blog');
        return new Blog(result);
    }
}