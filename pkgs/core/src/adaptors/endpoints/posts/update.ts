import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Post, PostAttributes } from "@guildion/core/src/adaptors/models/Post";

export class PostUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/post/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostUpdateRequestAttributes extends RequestParameters {
    post: Partial<PostAttributes> & { id: string },
}

export class PostUpdateRequest extends Record<PostUpdateRequestAttributes>({
    post: { ...new Post().toJSON(), id: '', body: '' },
}) {
    constructor(args: PostUpdateRequestAttributes) {
        super(args);
    }

    public getPost(): Post | undefined {
        const result = this.get('post');
        return result ? (new Post(result)) : undefined;
    }
}

export interface PostUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    post: PostAttributes,
}

export class PostUpdateResponse extends Record<PostUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    post: new Post().toJSON(),
}) {
    constructor(args: PostUpdateResponseAttributes) {
        super(args);
    }

    public getPost(): Post | undefined {
        const result = this.get('post');
        return result ? (new Post(result)) : undefined;
    }
}