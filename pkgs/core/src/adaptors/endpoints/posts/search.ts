import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { PostAttributes, Posts } from "@guildion/core/src/adaptors/models/Post";

export class PostsSearchRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/posts/search',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostsSearchRequestAttributes extends RequestParameters {
    query: string,
    roomId?: string,
    count?: number,
    offset?: number,
}

export class PostsSearchRequest extends Record<PostsSearchRequestAttributes>({
    query: '',
    roomId: undefined,
    count: undefined,
    offset: undefined,
}) {
    constructor(args: PostsSearchRequestAttributes) {
        super(args);
    }
}

export interface PostsSearchResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    posts: PostAttributes[],
    paginatable: boolean,
}

export class PostsSearchResponse extends Record<PostsSearchResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    posts: [],
    paginatable: false,
}) {
    constructor(args: PostsSearchResponseAttributes) {
        super(args);
    }

    public getPosts(): Posts {
        const result = this.get('posts');
        return new Posts(result);
    }
}