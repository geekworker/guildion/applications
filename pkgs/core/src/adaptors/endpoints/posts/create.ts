import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Post, PostAttributes } from "@guildion/core/src/adaptors/models/Post";

export class PostCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/post/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostCreateRequestAttributes extends RequestParameters {
    post: PostAttributes,
}

export class PostCreateRequest extends Record<PostCreateRequestAttributes>({
    post: new Post().toJSON(),
}) {
    constructor(args: PostCreateRequestAttributes) {
        super(args);
    }

    public getPost(): Post | undefined {
        const result = this.get('post');
        return result ? (new Post(result)) : undefined;
    }
}

export interface PostCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    post: PostAttributes,
}

export class PostCreateResponse extends Record<PostCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    post: new Post().toJSON(),
}) {
    constructor(args: PostCreateResponseAttributes) {
        super(args);
    }

    public getPost(): Post | undefined {
        const result = this.get('post');
        return result ? (new Post(result)) : undefined;
    }
}