import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Emoji } from "@guildion/core/src/constants/Emoji";

export class PostReactionRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/post/reaction',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostReactionRequestAttributes extends RequestParameters {
    id: string,
    fileId: string,
    emoji?: Emoji,
}

export class PostReactionRequest extends Record<PostReactionRequestAttributes>({
    id: '',
    fileId: '',
    emoji: undefined,
}) {
    constructor(args: PostReactionRequestAttributes) {
        super(args);
    }
}

export interface PostReactionResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    isCreated: boolean,
}

export class PostReactionResponse extends Record<PostReactionResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    isCreated: false,
}) {
    constructor(args: PostReactionResponseAttributes) {
        super(args);
    }
}