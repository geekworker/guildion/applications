import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class PostReadRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/post/read',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostReadRequestAttributes extends RequestParameters {
    roomId: string,
}

export class PostReadRequest extends Record<PostReadRequestAttributes>({
    roomId: '',
}) {
    constructor(args: PostReadRequestAttributes) {
        super(args);
    }
}

export interface PostReadResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class PostReadResponse extends Record<PostReadResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: PostReadResponseAttributes) {
        super(args);
    }
}