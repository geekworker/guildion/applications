import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { PostsListRequest, PostsListResponse, PostsListRouter } from "./list";
import { PostCreateRequest, PostCreateResponse, PostCreateRouter } from "./create";
import { PostUpdateRequest, PostUpdateResponse, PostUpdateRouter } from "./update";
import { PostDestroyRequest, PostDestroyResponse, PostDestroyRouter } from "./destroy";
import { PostReactionRequest, PostReactionResponse, PostReactionRouter } from "./reaction";
import { PostShowRequest, PostShowResponse, PostShowRouter } from "./show";
import { PostReadRequest, PostReadResponse, PostReadRouter } from "./read";
import { PostsSearchRequest, PostsSearchResponse, PostsSearchRouter } from "./search";

export namespace PostsEndpoint {
    export const List: Endpoint<PostsListRouter, PostsListRequest, PostsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostsListRouter,
        request: PostsListRequest,
        response: PostsListResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type List = typeof List;

    export const Show: Endpoint<PostShowRouter, PostShowRequest, PostShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostShowRouter,
        request: PostShowRequest,
        response: PostShowResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Show = typeof Show;

    export const Create: Endpoint<PostCreateRouter, PostCreateRequest, PostCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostCreateRouter,
        request: PostCreateRequest,
        response: PostCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;

    export const Update: Endpoint<PostUpdateRouter, PostUpdateRequest, PostUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostUpdateRouter,
        request: PostUpdateRequest,
        response: PostUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<PostDestroyRouter, PostDestroyRequest, PostDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostDestroyRouter,
        request: PostDestroyRequest,
        response: PostDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const Reaction: Endpoint<PostReactionRouter, PostReactionRequest, PostReactionResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostReactionRouter,
        request: PostReactionRequest,
        response: PostReactionResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Reaction = typeof Reaction;

    export const Read: Endpoint<PostReadRouter, PostReadRequest, PostReadResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostReadRouter,
        request: PostReadRequest,
        response: PostReadResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Read = typeof Read;

    export const Search: Endpoint<PostsSearchRouter, PostsSearchRequest, PostsSearchResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: PostsSearchRouter,
        request: PostsSearchRequest,
        response: PostsSearchResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Search = typeof Search;
}