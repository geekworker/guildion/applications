import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { PostAttributes, Post } from "@guildion/core/src/adaptors/models/Post";

export class PostShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/post/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostShowRequestAttributes extends RequestParameters {
    id: string,
}

export class PostShowRequest extends Record<PostShowRequestAttributes>({
    id: '',
}) {
    constructor(args: PostShowRequestAttributes) {
        super(args);
    }
}

export interface PostShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    post: PostAttributes,
}

export class PostShowResponse extends Record<PostShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    post: new Post().toJSON(),
}) {
    constructor(args: PostShowResponseAttributes) {
        super(args);
    }

    public getPost(): Post {
        const result = this.get('post');
        return new Post(result);
    }
}