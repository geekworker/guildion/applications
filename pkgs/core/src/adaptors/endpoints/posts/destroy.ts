import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class PostDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/post/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class PostDestroyRequest extends Record<PostDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: PostDestroyRequestAttributes) {
        super(args);
    }
}

export interface PostDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class PostDestroyResponse extends Record<PostDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: PostDestroyResponseAttributes) {
        super(args);
    }
}