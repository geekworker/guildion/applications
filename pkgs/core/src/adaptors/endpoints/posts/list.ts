import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { PostAttributes, Posts } from "@guildion/core/src/adaptors/models/Post";

export class PostsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/posts/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface PostsListRequestAttributes extends RequestParameters {
    roomId: string,
    postId?: string,
    count?: number,
    offset?: number,
    isPublic?: boolean,
}

export class PostsListRequest extends Record<PostsListRequestAttributes>({
    roomId: '',
    postId: undefined,
    count: undefined,
    offset: undefined,
    isPublic: undefined,
}) {
    constructor(args: PostsListRequestAttributes) {
        super(args);
    }
}

export interface PostsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    posts: PostAttributes[],
    paginatable: boolean,
}

export class PostsListResponse extends Record<PostsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    posts: [],
    paginatable: false,
}) {
    constructor(args: PostsListResponseAttributes) {
        super(args);
    }

    public getPosts(): Posts {
        const result = this.get('posts');
        return new Posts(result);
    }
}