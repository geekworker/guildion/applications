import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RolesListRequest, RolesListResponse, RolesListRouter } from "./list";
import { RoleShowRequest, RoleShowResponse, RoleShowRouter } from "./show";

export namespace RolesEndpoint {
    export const List: Endpoint<RolesListRouter, RolesListRequest, RolesListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RolesListRouter,
        request: RolesListRequest,
        response: RolesListResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type List = typeof List;

    export const Show: Endpoint<RoleShowRouter, RoleShowRequest, RoleShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoleShowRouter,
        request: RoleShowRequest,
        response: RoleShowResponse,
        options: { deviceRequired: true, accessTokenRequired: false },
    }
    export type Show = typeof Show;
}