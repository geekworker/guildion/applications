import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoleAttributes, Role } from "@guildion/core/src/adaptors/models/Role";

export class RoleShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/role/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoleShowRequestAttributes extends RequestParameters {
    id: string,
}

export class RoleShowRequest extends Record<RoleShowRequestAttributes>({
    id: '',
}) {
    constructor(args: RoleShowRequestAttributes) {
        super(args);
    }
}

export interface RoleShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    role: RoleAttributes,
}

export class RoleShowResponse extends Record<RoleShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    role: new Role().toJSON(),
}) {
    constructor(args: RoleShowResponseAttributes) {
        super(args);
    }

    public getRole(): Role {
        const result = this.get('role');
        return new Role(result);
    }
}