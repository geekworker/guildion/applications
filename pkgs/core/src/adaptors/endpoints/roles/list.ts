import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoleAttributes, Roles } from "@guildion/core/src/adaptors/models/Role";

export class RolesListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/roles/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RolesListRequestAttributes extends RequestParameters {
    guildId: string,
}

export class RolesListRequest extends Record<RolesListRequestAttributes>({
    guildId: '',
}) {
    constructor(args: RolesListRequestAttributes) {
        super(args);
    }
}

export interface RolesListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    roles: RoleAttributes[],
}

export class RolesListResponse extends Record<RolesListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    roles: [],
}) {
    constructor(args: RolesListResponseAttributes) {
        super(args);
    }

    public getRoles(): Roles {
        const result = this.get('roles');
        return new Roles(result);
    }
}