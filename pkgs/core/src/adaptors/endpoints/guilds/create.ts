import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Guild, GuildAttributes } from "@guildion/core/src/adaptors/models/Guild";

export class GuildCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildCreateRequestAttributes extends RequestParameters {
    guild: GuildAttributes,
}

export class GuildCreateRequest extends Record<GuildCreateRequestAttributes>({
    guild: new Guild().toJSON(),
}) {
    constructor(args: GuildCreateRequestAttributes) {
        super(args);
    }

    public getGuild(): Guild | undefined {
        const result = this.get('guild');
        return result ? (new Guild(result)) : undefined;
    }
}

export interface GuildCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guild: GuildAttributes,
}

export class GuildCreateResponse extends Record<GuildCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guild: new Guild().toJSON(),
}) {
    constructor(args: GuildCreateResponseAttributes) {
        super(args);
    }

    public getGuild(): Guild | undefined {
        const result = this.get('guild');
        return result ? (new Guild(result)) : undefined;
    }
}