import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { GuildCreateRequest, GuildCreateResponse, GuildCreateRouter } from "./create";
import { GuildUpdateRequest, GuildUpdateResponse, GuildUpdateRouter } from "./update";
import { GuildDestroyRequest, GuildDestroyResponse, GuildDestroyRouter } from "./destroy";
import { GuildJoinRequest, GuildJoinResponse, GuildJoinRouter } from "./join";
import { GuildLeaveRequest, GuildLeaveResponse, GuildLeaveRouter } from "./leave";
import { GuildBlockRequest, GuildBlockResponse, GuildBlockRouter } from "./block";
import { GuildUnBlockRequest, GuildUnBlockResponse, GuildUnBlockRouter } from "./unblock";
import { GuildsListRequest, GuildsListResponse, GuildsListRouter } from "./list";
import { GuildShowRequest, GuildShowResponse, GuildShowRouter } from "./show";
import { GuildsSearchRequest, GuildsSearchResponse, GuildsSearchRouter } from "./search";
import { GuildConnectRequest, GuildConnectResponse, GuildConnectRouter } from "./connect";
import { GuildDisconnectRequest, GuildDisconnectResponse, GuildDisconnectRouter } from "./disconnect";
import { GuildConnectionsListRequest, GuildConnectionsListResponse, GuildConnectionsListRouter } from "./connectionsList";

export namespace GuildsEndpoint {
    export const List: Endpoint<GuildsListRouter, GuildsListRequest, GuildsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildsListRouter,
        request: GuildsListRequest,
        response: GuildsListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type List = typeof List;

    export const Show: Endpoint<GuildShowRouter, GuildShowRequest, GuildShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildShowRouter,
        request: GuildShowRequest,
        response: GuildShowResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Show = typeof Show;

    export const Search: Endpoint<GuildsSearchRouter, GuildsSearchRequest, GuildsSearchResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildsSearchRouter,
        request: GuildsSearchRequest,
        response: GuildsSearchResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Search = typeof Search;

    export const Create: Endpoint<GuildCreateRouter, GuildCreateRequest, GuildCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildCreateRouter,
        request: GuildCreateRequest,
        response: GuildCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;

    export const Update: Endpoint<GuildUpdateRouter, GuildUpdateRequest, GuildUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildUpdateRouter,
        request: GuildUpdateRequest,
        response: GuildUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<GuildDestroyRouter, GuildDestroyRequest, GuildDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildDestroyRouter,
        request: GuildDestroyRequest,
        response: GuildDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const Join: Endpoint<GuildJoinRouter, GuildJoinRequest, GuildJoinResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildJoinRouter,
        request: GuildJoinRequest,
        response: GuildJoinResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Join = typeof Join;

    export const Leave: Endpoint<GuildLeaveRouter, GuildLeaveRequest, GuildLeaveResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildLeaveRouter,
        request: GuildLeaveRequest,
        response: GuildLeaveResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Leave = typeof Leave;

    export const Block: Endpoint<GuildBlockRouter, GuildBlockRequest, GuildBlockResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildBlockRouter,
        request: GuildBlockRequest,
        response: GuildBlockResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Block = typeof Block;

    export const UnBlock: Endpoint<GuildUnBlockRouter, GuildUnBlockRequest, GuildUnBlockResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildUnBlockRouter,
        request: GuildUnBlockRequest,
        response: GuildUnBlockResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type UnBlock = typeof UnBlock;

    export const Connect: Endpoint<GuildConnectRouter, GuildConnectRequest, GuildConnectResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildConnectRouter,
        request: GuildConnectRequest,
        response: GuildConnectResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Connect = typeof Connect;

    export const Disconnect: Endpoint<GuildDisconnectRouter, GuildDisconnectRequest, GuildDisconnectResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildDisconnectRouter,
        request: GuildDisconnectRequest,
        response: GuildDisconnectResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Disconnect = typeof Disconnect;

    export const ConnectionsList: Endpoint<GuildConnectionsListRouter, GuildConnectionsListRequest, GuildConnectionsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: GuildConnectionsListRouter,
        request: GuildConnectionsListRequest,
        response: GuildConnectionsListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type ConnectionsList = typeof ConnectionsList;
}