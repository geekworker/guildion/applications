import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Guild, GuildAttributes } from "@guildion/core/src/adaptors/models/Guild";
import { Member, MemberAttributes } from "@guildion/core/src/adaptors/models/Member";

export class GuildJoinRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/join',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildJoinRequestAttributes extends RequestParameters {
    id: string,
}

export class GuildJoinRequest extends Record<GuildJoinRequestAttributes>({
    id: '',
}) {
    constructor(args: GuildJoinRequestAttributes) {
        super(args);
    }
}

export interface GuildJoinResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guild: GuildAttributes,
    member: MemberAttributes,
}

export class GuildJoinResponse extends Record<GuildJoinResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guild: new Guild().toJSON(),
    member: new Member().toJSON(),
}) {
    constructor(args: GuildJoinResponseAttributes) {
        super(args);
    }

    public getGuild(): Guild | undefined {
        const result = this.get('guild');
        return result ? (new Guild(result)) : undefined;
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? (new Member(result)) : undefined;
    }
}