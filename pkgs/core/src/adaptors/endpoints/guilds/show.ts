import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { GuildAttributes, Guild } from "@guildion/core/src/adaptors/models/Guild";

export class GuildShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildShowRequestAttributes extends RequestParameters {
    id: string,
    isPublic?: boolean,
}

export class GuildShowRequest extends Record<GuildShowRequestAttributes>({
    id: '',
    isPublic: undefined,
}) {
    constructor(args: GuildShowRequestAttributes) {
        super(args);
    }
}

export interface GuildShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guild: GuildAttributes,
}

export class GuildShowResponse extends Record<GuildShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guild: new Guild().toJSON(),
}) {
    constructor(args: GuildShowResponseAttributes) {
        super(args);
    }

    public getGuild(): Guild {
        const result = this.get('guild');
        return new Guild(result);
    }
}