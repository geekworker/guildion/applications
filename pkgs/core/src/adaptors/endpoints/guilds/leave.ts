import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class GuildLeaveRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/leave',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildLeaveRequestAttributes extends RequestParameters {
    id: string,
}

export class GuildLeaveRequest extends Record<GuildLeaveRequestAttributes>({
    id: '',
}) {
    constructor(args: GuildLeaveRequestAttributes) {
        super(args);
    }
}

export interface GuildLeaveResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildLeaveResponse extends Record<GuildLeaveResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildLeaveResponseAttributes) {
        super(args);
    }
}