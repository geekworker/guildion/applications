import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { ConnectionAttributes, Connections } from "@guildion/core/src/adaptors/models/Connection";

export class GuildConnectionsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/connections/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildConnectionsListRequestAttributes extends RequestParameters {
    id: string,
    count?: number,
    offset?: number,
    gtAt?: string,
    ltAt?: string,
}

export class GuildConnectionsListRequest extends Record<GuildConnectionsListRequestAttributes>({
    id: '',
    count: undefined,
    offset: undefined,
    gtAt: undefined,
    ltAt: undefined,
}) {
    constructor(args: GuildConnectionsListRequestAttributes) {
        super(args);
    }

    public getGtAt(): Date | undefined {
        const result = this.get('gtAt');
        return result ? new Date(result) : undefined;
    }

    public getLtAt(): Date | undefined {
        const result = this.get('ltAt');
        return result ? new Date(result) : undefined;
    }
}

export interface GuildConnectionsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    connections: ConnectionAttributes[],
    paginatable: boolean,
}

export class GuildConnectionsListResponse extends Record<GuildConnectionsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    connections: [],
    paginatable: false,
}) {
    constructor(args: GuildConnectionsListResponseAttributes) {
        super(args);
    }

    public getConnections(): Connections | undefined {
        const result = this.get('connections');
        return result ? (new Connections(result)) : undefined;
    }
}