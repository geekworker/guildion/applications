import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { GuildAttributes, Guilds } from "@guildion/core/src/adaptors/models/Guild";

export class GuildsSearchRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guilds/search',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildsSearchRequestAttributes extends RequestParameters {
    query: string,
    count?: number,
    offset?: number,
}

export class GuildsSearchRequest extends Record<GuildsSearchRequestAttributes>({
    query: '',
    count: undefined,
    offset: undefined,
}) {
    constructor(args: GuildsSearchRequestAttributes) {
        super(args);
    }
}

export interface GuildsSearchResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guilds: GuildAttributes[],
    paginatable: boolean,
}

export class GuildsSearchResponse extends Record<GuildsSearchResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guilds: [],
    paginatable: false,
}) {
    constructor(args: GuildsSearchResponseAttributes) {
        super(args);
    }

    public getGuilds(): Guilds {
        const result = this.get('guilds');
        return new Guilds(result);
    }
}