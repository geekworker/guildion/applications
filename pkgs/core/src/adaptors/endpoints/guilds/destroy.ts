import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Guild, GuildAttributes } from "@guildion/core/src/adaptors/models/Guild";

export class GuildDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class GuildDestroyRequest extends Record<GuildDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: GuildDestroyRequestAttributes) {
        super(args);
    }
}

export interface GuildDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildDestroyResponse extends Record<GuildDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildDestroyResponseAttributes) {
        super(args);
    }
}