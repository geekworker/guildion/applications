import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class GuildDisconnectRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/disconnect',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildDisconnectRequestAttributes extends RequestParameters {
}

export class GuildDisconnectRequest extends Record<GuildDisconnectRequestAttributes>({
}) {
    constructor(args: GuildDisconnectRequestAttributes) {
        super(args);
    }
}

export interface GuildDisconnectResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildDisconnectResponse extends Record<GuildDisconnectResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildDisconnectResponseAttributes) {
        super(args);
    }
}