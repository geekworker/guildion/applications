import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class GuildUnBlockRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/unblock',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildUnBlockRequestAttributes extends RequestParameters {
    id: string,
    receiverId: string,
}

export class GuildUnBlockRequest extends Record<GuildUnBlockRequestAttributes>({
    id: '',
    receiverId: '',
}) {
    constructor(args: GuildUnBlockRequestAttributes) {
        super(args);
    }
}

export interface GuildUnBlockResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildUnBlockResponse extends Record<GuildUnBlockResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildUnBlockResponseAttributes) {
        super(args);
    }
}