import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Guild, GuildAttributes } from "@guildion/core/src/adaptors/models/Guild";

export class GuildUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildUpdateRequestAttributes extends RequestParameters {
    guild: Partial<GuildAttributes> & { id: string },
}

export class GuildUpdateRequest extends Record<GuildUpdateRequestAttributes>({
    guild: { id: '', ...new Guild().toJSON() },
}) {
    constructor(args: GuildUpdateRequestAttributes) {
        super(args);
    }

    public getGuild(): Guild | undefined {
        const result = this.get('guild');
        return result ? (new Guild(result)) : undefined;
    }
}

export interface GuildUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guild: GuildAttributes,
}

export class GuildUpdateResponse extends Record<GuildUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guild: new Guild().toJSON(),
}) {
    constructor(args: GuildUpdateResponseAttributes) {
        super(args);
    }

    public getGuild(): Guild | undefined {
        const result = this.get('guild');
        return result ? (new Guild(result)) : undefined;
    }
}