import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class GuildConnectRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/connect',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildConnectRequestAttributes extends RequestParameters {
    id: string,
}

export class GuildConnectRequest extends Record<GuildConnectRequestAttributes>({
    id: '',
}) {
    constructor(args: GuildConnectRequestAttributes) {
        super(args);
    }
}

export interface GuildConnectResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildConnectResponse extends Record<GuildConnectResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildConnectResponseAttributes) {
        super(args);
    }
}