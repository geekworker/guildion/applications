import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { GuildAttributes, Guilds } from "@guildion/core/src/adaptors/models/Guild";

export class GuildsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guilds/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildsListRequestAttributes extends RequestParameters {
    count?: number,
    offset?: number,
}

export class GuildsListRequest extends Record<GuildsListRequestAttributes>({
    count: undefined,
    offset: undefined,
}) {
    constructor(args: GuildsListRequestAttributes) {
        super(args);
    }
}

export interface GuildsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    guilds: GuildAttributes[],
    paginatable: boolean,
}

export class GuildsListResponse extends Record<GuildsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    guilds: [],
    paginatable: false,
}) {
    constructor(args: GuildsListResponseAttributes) {
        super(args);
    }

    public getGuilds(): Guilds {
        const result = this.get('guilds');
        return new Guilds(result);
    }
}