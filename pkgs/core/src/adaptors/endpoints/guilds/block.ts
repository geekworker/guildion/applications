import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class GuildBlockRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/guild/block',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface GuildBlockRequestAttributes extends RequestParameters {
    id: string,
    receiverId: string,
}

export class GuildBlockRequest extends Record<GuildBlockRequestAttributes>({
    id: '',
    receiverId: '',
}) {
    constructor(args: GuildBlockRequestAttributes) {
        super(args);
    }
}

export interface GuildBlockResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class GuildBlockResponse extends Record<GuildBlockResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: GuildBlockResponseAttributes) {
        super(args);
    }
}