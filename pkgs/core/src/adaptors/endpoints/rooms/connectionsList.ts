import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { ConnectionAttributes, Connections } from "@guildion/core/src/adaptors/models/Connection";

export class RoomConnectionsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/connections/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomConnectionsListRequestAttributes extends RequestParameters {
    id: string,
    count?: number,
    offset?: number,
    gtAt?: string,
    ltAt?: string,
}

export class RoomConnectionsListRequest extends Record<RoomConnectionsListRequestAttributes>({
    id: '',
    count: undefined,
    offset: undefined,
    gtAt: undefined,
    ltAt: undefined,
}) {
    constructor(args: RoomConnectionsListRequestAttributes) {
        super(args);
    }

    public getGtAt(): Date | undefined {
        const result = this.get('gtAt');
        return result ? new Date(result) : undefined;
    }

    public getLtAt(): Date | undefined {
        const result = this.get('ltAt');
        return result ? new Date(result) : undefined;
    }
}

export interface RoomConnectionsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    connections: ConnectionAttributes[],
    paginatable: boolean,
}

export class RoomConnectionsListResponse extends Record<RoomConnectionsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    connections: [],
    paginatable: false,
}) {
    constructor(args: RoomConnectionsListResponseAttributes) {
        super(args);
    }

    public getConnections(): Connections | undefined {
        const result = this.get('connections');
        return result ? (new Connections(result)) : undefined;
    }
}