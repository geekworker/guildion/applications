import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Room, RoomAttributes } from "@guildion/core/src/adaptors/models/Room";
import { Member, MemberAttributes } from "@guildion/core/src/adaptors/models/Member";

export class RoomJoinRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/join',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomJoinRequestAttributes extends RequestParameters {
    id: string,
}

export class RoomJoinRequest extends Record<RoomJoinRequestAttributes>({
    id: '',
}) {
    constructor(args: RoomJoinRequestAttributes) {
        super(args);
    }
}

export interface RoomJoinResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    room: RoomAttributes,
    member: MemberAttributes,
}

export class RoomJoinResponse extends Record<RoomJoinResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    room: new Room().toJSON(),
    member: new Member().toJSON(),
}) {
    constructor(args: RoomJoinResponseAttributes) {
        super(args);
    }

    public getRoom(): Room | undefined {
        const result = this.get('room');
        return result ? (new Room(result)) : undefined;
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? (new Member(result)) : undefined;
    }
}