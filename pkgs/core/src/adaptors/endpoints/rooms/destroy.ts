import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomDestroyRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/destroy',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomDestroyRequestAttributes extends RequestParameters {
    id: string,
}

export class RoomDestroyRequest extends Record<RoomDestroyRequestAttributes>({
    id: '',
}) {
    constructor(args: RoomDestroyRequestAttributes) {
        super(args);
    }
}

export interface RoomDestroyResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomDestroyResponse extends Record<RoomDestroyResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomDestroyResponseAttributes) {
        super(args);
    }
}