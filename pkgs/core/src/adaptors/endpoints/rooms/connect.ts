import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomConnectRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/connect',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomConnectRequestAttributes extends RequestParameters {
    id: string,
}

export class RoomConnectRequest extends Record<RoomConnectRequestAttributes>({
    id: '',
}) {
    constructor(args: RoomConnectRequestAttributes) {
        super(args);
    }
}

export interface RoomConnectResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomConnectResponse extends Record<RoomConnectResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomConnectResponseAttributes) {
        super(args);
    }
}