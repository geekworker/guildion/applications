import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomDisconnectRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/disconnect',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomDisconnectRequestAttributes extends RequestParameters {
}

export class RoomDisconnectRequest extends Record<RoomDisconnectRequestAttributes>({
}) {
    constructor(args: RoomDisconnectRequestAttributes) {
        super(args);
    }
}

export interface RoomDisconnectResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomDisconnectResponse extends Record<RoomDisconnectResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomDisconnectResponseAttributes) {
        super(args);
    }
}