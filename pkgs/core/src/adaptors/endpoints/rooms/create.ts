import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Room, RoomAttributes } from "@guildion/core/src/adaptors/models/Room";

export class RoomCreateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/create',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomCreateRequestAttributes extends RequestParameters {
    room: RoomAttributes,
}

export class RoomCreateRequest extends Record<RoomCreateRequestAttributes>({
    room: new Room().toJSON(),
}) {
    constructor(args: RoomCreateRequestAttributes) {
        super(args);
    }

    public getRoom(): Room | undefined {
        const result = this.get('room');
        return result ? (new Room(result)) : undefined;
    }
}

export interface RoomCreateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    room: RoomAttributes,
}

export class RoomCreateResponse extends Record<RoomCreateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    room: new Room().toJSON(),
}) {
    constructor(args: RoomCreateResponseAttributes) {
        super(args);
    }

    public getRoom(): Room | undefined {
        const result = this.get('room');
        return result ? (new Room(result)) : undefined;
    }
}