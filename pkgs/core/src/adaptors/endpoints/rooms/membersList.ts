import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { MemberAttributes, Members } from "@guildion/core/src/adaptors/models/Member";

export class RoomMembersListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/members/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomMembersListRequestAttributes extends RequestParameters {
    roomId: string,
    count?: number,
    offset?: number,
}

export class RoomMembersListRequest extends Record<RoomMembersListRequestAttributes>({
    roomId: '',
    count: undefined,
    offset: undefined,
}) {
    constructor(args: RoomMembersListRequestAttributes) {
        super(args);
    }
}

export interface RoomMembersListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    members: MemberAttributes[],
    paginatable: boolean,
}

export class RoomMembersListResponse extends Record<RoomMembersListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    members: [],
    paginatable: false,
}) {
    constructor(args: RoomMembersListResponseAttributes) {
        super(args);
    }

    public getMembers(): Members {
        const result = this.get('members');
        return new Members(result);
    }
}