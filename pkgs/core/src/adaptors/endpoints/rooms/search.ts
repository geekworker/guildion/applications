import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomAttributes, Rooms } from "@guildion/core/src/adaptors/models/Room";

export class RoomsSearchRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/rooms/search',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomsSearchRequestAttributes extends RequestParameters {
    query: string,
    guildId?: string,
    count?: number,
    offset?: number,
}

export class RoomsSearchRequest extends Record<RoomsSearchRequestAttributes>({
    query: '',
    guildId: undefined,
    count: undefined,
    offset: undefined,
}) {
    constructor(args: RoomsSearchRequestAttributes) {
        super(args);
    }
}

export interface RoomsSearchResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    rooms: RoomAttributes[],
    paginatable: boolean,
}

export class RoomsSearchResponse extends Record<RoomsSearchResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    rooms: [],
    paginatable: false,
}) {
    constructor(args: RoomsSearchResponseAttributes) {
        super(args);
    }

    public getRooms(): Rooms {
        const result = this.get('rooms');
        return new Rooms(result);
    }
}