import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { MemberAttributes, Member } from "@guildion/core/src/adaptors/models/Member";

export class RoomMemberKickRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/member/kick',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomMemberKickRequestAttributes extends RequestParameters {
    id: string,
}

export class RoomMemberKickRequest extends Record<RoomMemberKickRequestAttributes>({
    id: '',
}) {
    constructor(args: RoomMemberKickRequestAttributes) {
        super(args);
    }
}

export interface RoomMemberKickResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomMemberKickResponse extends Record<RoomMemberKickResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomMemberKickResponseAttributes) {
        super(args);
    }
}