import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Room, RoomAttributes } from "@guildion/core/src/adaptors/models/Room";

export class RoomUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomUpdateRequestAttributes extends RequestParameters {
    room: Partial<RoomAttributes> & { id: string },
}

export class RoomUpdateRequest extends Record<RoomUpdateRequestAttributes>({
    room: { id: '', ...new Room().toJSON() },
}) {
    constructor(args: RoomUpdateRequestAttributes) {
        super(args);
    }

    public getRoom(): Room | undefined {
        const result = this.get('room');
        return result ? (new Room(result)) : undefined;
    }
}

export interface RoomUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    room: RoomAttributes,
}

export class RoomUpdateResponse extends Record<RoomUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    room: new Room().toJSON(),
}) {
    constructor(args: RoomUpdateResponseAttributes) {
        super(args);
    }

    public getRoom(): Room | undefined {
        const result = this.get('room');
        return result ? (new Room(result)) : undefined;
    }
}