import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { MemberAttributes, Member } from "@guildion/core/src/adaptors/models/Member";

export class RoomMemberUpdateRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/member/update',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomMemberUpdateRequestAttributes extends RequestParameters {
    member: Partial<MemberAttributes> & { id: string, roomId: string },
}

export class RoomMemberUpdateRequest extends Record<RoomMemberUpdateRequestAttributes>({
    member: { id: '', roomId: '', ...new Member().toJSON() },
}) {
    constructor(args: RoomMemberUpdateRequestAttributes) {
        super(args);
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? new Member(result) : undefined;
    }
}

export interface RoomMemberUpdateResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    member: MemberAttributes,
}

export class RoomMemberUpdateResponse extends Record<RoomMemberUpdateResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    member: new Member(),
}) {
    constructor(args: RoomMemberUpdateResponseAttributes) {
        super(args);
    }

    public getMember(): Member | undefined {
        const result = this.get('member');
        return result ? new Member(result) : undefined;
    }
}