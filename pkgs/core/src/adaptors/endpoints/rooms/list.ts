import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomAttributes, Rooms } from "@guildion/core/src/adaptors/models/Room";

export class RoomsListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/rooms/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomsListRequestAttributes extends RequestParameters {
    guildId: string,
    count?: number,
    offset?: number,
    ltAt?: string,
    gtAt?: string,
    isPublic?: boolean,
}

export class RoomsListRequest extends Record<RoomsListRequestAttributes>({
    guildId: '',
    count: undefined,
    offset: undefined,
    ltAt: undefined,
    gtAt: undefined,
    isPublic: undefined,
}) {
    constructor(args: RoomsListRequestAttributes) {
        super(args);
    }

    public getLtAt(): Date | undefined {
        const result = this.get('ltAt');
        return result ? new Date(result) : undefined;
    }

    public getGtAt(): Date | undefined {
        const result = this.get('gtAt');
        return result ? new Date(result) : undefined;
    }
}

export interface RoomsListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    rooms: RoomAttributes[],
    paginatable: boolean,
}

export class RoomsListResponse extends Record<RoomsListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    rooms: [],
    paginatable: false,
}) {
    constructor(args: RoomsListResponseAttributes) {
        super(args);
    }

    public getRooms(): Rooms {
        const result = this.get('rooms');
        return new Rooms(result);
    }
}