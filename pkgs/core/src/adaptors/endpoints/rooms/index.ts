import { Endpoint } from ".."
import { ContentType } from "@guildion/core/src/constants/ContentType";
import { HTTPMethod } from "@guildion/core/src/constants/HttpMethod";
import { RoomCreateRequest, RoomCreateResponse, RoomCreateRouter } from "./create";
import { RoomUpdateRequest, RoomUpdateResponse, RoomUpdateRouter } from "./update";
import { RoomDestroyRequest, RoomDestroyResponse, RoomDestroyRouter } from "./destroy";
import { RoomsListRequest, RoomsListResponse, RoomsListRouter } from "./list";
import { RoomShowRequest, RoomShowResponse, RoomShowRouter } from "./show";
import { RoomsSearchRequest, RoomsSearchResponse, RoomsSearchRouter } from "./search";
import { RoomJoinRequest, RoomJoinResponse, RoomJoinRouter } from "./join";
import { RoomLeaveRequest, RoomLeaveResponse, RoomLeaveRouter } from "./leave";
import { RoomMembersListRequest, RoomMembersListResponse, RoomMembersListRouter } from "./membersList";
import { RoomConnectingMembersListRequest, RoomConnectingMembersListResponse, RoomConnectingMembersListRouter } from "./connectingMembersList";
import { RoomMemberUpdateRequest, RoomMemberUpdateResponse, RoomMemberUpdateRouter } from "./memberUpdate";
import { RoomMemberKickRequest, RoomMemberKickResponse, RoomMemberKickRouter } from "./memberKick";
import { RoomConnectRequest, RoomConnectResponse, RoomConnectRouter } from "./connect";
import { RoomDisconnectRequest, RoomDisconnectResponse, RoomDisconnectRouter } from "./disconnect";
import { RoomConnectionsListRequest, RoomConnectionsListResponse, RoomConnectionsListRouter } from "./connectionsList";

export namespace RoomsEndpoint {
    export const Create: Endpoint<RoomCreateRouter, RoomCreateRequest, RoomCreateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomCreateRouter,
        request: RoomCreateRequest,
        response: RoomCreateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Create = typeof Create;

    export const Update: Endpoint<RoomUpdateRouter, RoomUpdateRequest, RoomUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomUpdateRouter,
        request: RoomUpdateRequest,
        response: RoomUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Update = typeof Update;

    export const Destroy: Endpoint<RoomDestroyRouter, RoomDestroyRequest, RoomDestroyResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomDestroyRouter,
        request: RoomDestroyRequest,
        response: RoomDestroyResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Destroy = typeof Destroy;

    export const List: Endpoint<RoomsListRouter, RoomsListRequest, RoomsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomsListRouter,
        request: RoomsListRequest,
        response: RoomsListResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type List = typeof List;

    export const Search: Endpoint<RoomsSearchRouter, RoomsSearchRequest, RoomsSearchResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomsSearchRouter,
        request: RoomsSearchRequest,
        response: RoomsSearchResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Search = typeof Search;

    export const Show: Endpoint<RoomShowRouter, RoomShowRequest, RoomShowResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomShowRouter,
        request: RoomShowRequest,
        response: RoomShowResponse,
        options: { deviceRequired: true, accessTokenOptional: true },
    }
    export type Show = typeof Show;

    export const ConnectingMembersList: Endpoint<RoomConnectingMembersListRouter, RoomConnectingMembersListRequest, RoomConnectingMembersListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomConnectingMembersListRouter,
        request: RoomConnectingMembersListRequest,
        response: RoomConnectingMembersListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type ConnectingMembersList = typeof ConnectingMembersList;

    export const MembersList: Endpoint<RoomMembersListRouter, RoomMembersListRequest, RoomMembersListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomMembersListRouter,
        request: RoomMembersListRequest,
        response: RoomMembersListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type MembersList = typeof MembersList;

    export const MemberUpdate: Endpoint<RoomMemberUpdateRouter, RoomMemberUpdateRequest, RoomMemberUpdateResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomMemberUpdateRouter,
        request: RoomMemberUpdateRequest,
        response: RoomMemberUpdateResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type MemberUpdate = typeof MemberUpdate;

    export const MemberKick: Endpoint<RoomMemberKickRouter, RoomMemberKickRequest, RoomMemberKickResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomMemberKickRouter,
        request: RoomMemberKickRequest,
        response: RoomMemberKickResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type MemberKick = typeof MemberKick;

    export const Join: Endpoint<RoomJoinRouter, RoomJoinRequest, RoomJoinResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomJoinRouter,
        request: RoomJoinRequest,
        response: RoomJoinResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Join = typeof Join;

    export const Leave: Endpoint<RoomLeaveRouter, RoomLeaveRequest, RoomLeaveResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomLeaveRouter,
        request: RoomLeaveRequest,
        response: RoomLeaveResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Leave = typeof Leave;

    export const Connect: Endpoint<RoomConnectRouter, RoomConnectRequest, RoomConnectResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomConnectRouter,
        request: RoomConnectRequest,
        response: RoomConnectResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Connect = typeof Connect;

    export const Disconnect: Endpoint<RoomDisconnectRouter, RoomDisconnectRequest, RoomDisconnectResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomDisconnectRouter,
        request: RoomDisconnectRequest,
        response: RoomDisconnectResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type Disconnect = typeof Disconnect;

    export const ConnectionsList: Endpoint<RoomConnectionsListRouter, RoomConnectionsListRequest, RoomConnectionsListResponse> = {
        method: HTTPMethod.POST,
        requestType: ContentType.json,
        responseType: ContentType.json,
        router: RoomConnectionsListRouter,
        request: RoomConnectionsListRequest,
        response: RoomConnectionsListResponse,
        options: { deviceRequired: true, accessTokenRequired: true },
    }
    export type ConnectionsList = typeof ConnectionsList;
}