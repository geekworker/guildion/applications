import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { RoomAttributes, Room } from "@guildion/core/src/adaptors/models/Room";

export class RoomShowRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/show',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomShowRequestAttributes extends RequestParameters {
    id: string,
    isPublic?: boolean,
}

export class RoomShowRequest extends Record<RoomShowRequestAttributes>({
    id: '',
    isPublic: undefined,
}) {
    constructor(args: RoomShowRequestAttributes) {
        super(args);
    }
}

export interface RoomShowResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    room: RoomAttributes,
}

export class RoomShowResponse extends Record<RoomShowResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    room: new Room().toJSON(),
}) {
    constructor(args: RoomShowResponseAttributes) {
        super(args);
    }

    public getRoom(): Room {
        const result = this.get('room');
        return new Room(result);
    }
}