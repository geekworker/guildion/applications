import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { MemberAttributes, Members } from "@guildion/core/src/adaptors/models/Member";

export class RoomConnectingMembersListRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/members/connecting/list',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomConnectingMembersListRequestAttributes extends RequestParameters {
    roomId: string,
    count?: number,
    offset?: number,
    gtAt?: string,
    ltAt?: string,
}

export class RoomConnectingMembersListRequest extends Record<RoomConnectingMembersListRequestAttributes>({
    roomId: '',
    count: undefined,
    offset: undefined,
    gtAt: undefined,
    ltAt: undefined,
}) {
    constructor(args: RoomConnectingMembersListRequestAttributes) {
        super(args);
    }

    public getGtAt(): Date | undefined {
        const result = this.get('gtAt');
        return result ? new Date(result) : undefined;
    }

    public getLtAt(): Date | undefined {
        const result = this.get('ltAt');
        return result ? new Date(result) : undefined;
    }
}

export interface RoomConnectingMembersListResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    members: MemberAttributes[],
    paginatable: boolean,
}

export class RoomConnectingMembersListResponse extends Record<RoomConnectingMembersListResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    members: [],
    paginatable: false,
}) {
    constructor(args: RoomConnectingMembersListResponseAttributes) {
        super(args);
    }

    public getMembers(): Members {
        const result = this.get('members');
        return new Members(result);
    }
}