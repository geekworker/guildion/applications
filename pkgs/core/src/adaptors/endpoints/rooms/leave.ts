import { HTTPStatusCode } from "@guildion/core/src/constants/HttpStatusCode";
import { Record } from "immutable";
import { RequestParameters, ResponseParameters } from "..";
import { Router } from "../router";
import { GET_CURRENT_API_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";

export class RoomLeaveRouter extends Router.Factory<{}> {
    constructor() {
        super(
            '/api/v1/room/leave',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_API_URL_STRING() + this.toPath$();
    }
}

export interface RoomLeaveRequestAttributes extends RequestParameters {
    id: string,
}

export class RoomLeaveRequest extends Record<RoomLeaveRequestAttributes>({
    id: '',
}) {
    constructor(args: RoomLeaveRequestAttributes) {
        super(args);
    }
}

export interface RoomLeaveResponseAttributes extends ResponseParameters {
    status: HTTPStatusCode,
    error?: Error,
    success: boolean,
}

export class RoomLeaveResponse extends Record<RoomLeaveResponseAttributes>({
    status: HTTPStatusCode.CONTINUE,
    success: false,
}) {
    constructor(args: RoomLeaveResponseAttributes) {
        super(args);
    }
}