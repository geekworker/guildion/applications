import { GET_CURRENT_APP_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class DocumentShowRouter extends Router.Factory<{ groupSlug: string, sectionSlug: string, slug: string }, {}> {
    constructor() {
        super(
            '/documents/:groupSlug/:sectionSlug/:slug',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath({ groupSlug, sectionSlug, slug }: { groupSlug: string, sectionSlug: string, slug: string }): string {
        return GET_CURRENT_APP_URL_STRING() + this.toPath$({ groupSlug, sectionSlug, slug });
    }
}

export const documentShowRouter = new DocumentShowRouter();