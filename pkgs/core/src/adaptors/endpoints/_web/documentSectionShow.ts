import { GET_CURRENT_APP_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class DocumentSectionShowRouter extends Router.Factory<{ groupSlug: string, sectionSlug: string }, {}> {
    constructor() {
        super(
            '/documents/:groupSlug/:sectionSlug',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath({ groupSlug, sectionSlug }: { groupSlug: string, sectionSlug: string }): string {
        return GET_CURRENT_APP_URL_STRING() + this.toPath$({ groupSlug, sectionSlug });
    }
}

export const documentSectionShowRouter = new DocumentSectionShowRouter();