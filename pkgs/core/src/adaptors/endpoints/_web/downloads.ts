import { GET_CURRENT_APP_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class DownloadsRouter extends Router.Factory<{ os?: string }, {}> {
    constructor() {
        super(
            '/downloads(/:os)',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath({ os }: { os?: string }): string {
        return os ? GET_CURRENT_APP_URL_STRING() + '/downloads/' + os : GET_CURRENT_APP_URL_STRING() + '/downloads';
        // return os ? GET_CURRENT_APP_URL_STRING() + this.toPath$({ os }) : GET_CURRENT_APP_URL_STRING() + '/downloads';
    }
}

export const downloadsRouter = new DownloadsRouter();