import { GET_CURRENT_APP_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class DocumentGroupShowRouter extends Router.Factory<{ groupSlug: string }, {}> {
    constructor() {
        super(
            '/documents/:groupSlug',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath({ groupSlug }: { groupSlug: string }): string {
        return GET_CURRENT_APP_URL_STRING() + this.toPath$({ groupSlug });
    }
}

export const documentGroupShowRouter = new DocumentGroupShowRouter();