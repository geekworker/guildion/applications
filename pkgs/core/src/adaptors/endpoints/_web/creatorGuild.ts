import { GET_CURRENT_APP_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { Router } from "../router";

export class CreatorGuildRouter extends Router.Factory<{}, {}> {
    constructor() {
        super(
            '/creator-guild',
            {
                isNotFound: false,
                isRoot: false,
                isDevelopment: false,
                localizable: false,
            }
        )
    }
    public toPath(): string {
        return GET_CURRENT_APP_URL_STRING() + this.toPath$({});
    }
}

export const creatorGuildRouter = new CreatorGuildRouter();