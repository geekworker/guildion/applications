import axios, { AxiosRequestConfig } from "axios"
import { HTTPMethod, toAxiosMethod } from "@guildion/core/src/constants/HttpMethod";
import { encode } from 'base64-arraybuffer'

export const fetchImageBase64 = async (url: string): Promise<string> => {
    const payload: AxiosRequestConfig = {
        url,
        responseType: 'arraybuffer',
        headers: {
        },
        method: toAxiosMethod(HTTPMethod.GET),
    }
    const result = await axios.request(payload);
    const base64Encoded = encode(result.data);
    return base64Encoded;
}