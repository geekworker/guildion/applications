import { LanguageCode } from "@guildion/core/src/constants/LanguageCode";
import { CountryCode } from "@guildion/core/src/constants/CountryCode";
import { TimeZoneID, fallbackTimeZoneID } from "@guildion/core/src/constants/TimeZone";
import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { Account, AccountAttributes } from './Account';
import { DeviceAttributes, Devices } from './Device';
import { Administrator, AdministratorAttributes } from './Administrator';
import { File, FileAttributes, Files } from './File';

export const UserStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    VERIFIED: 'VERIFIED',
    LOCKED: 'LOCKED',
    DELETED: 'DELETED',
    DELETING: 'DELETING',
} as const;

export type UserStatus = typeof UserStatus[keyof typeof UserStatus];

export interface UserRecordAttributes {
    profile?: File,
    account?: Account,
    devices?: Devices,
    administrator?: Administrator,
    files?: Files,
    blocks?: Users,
    blockers?: Users,
    mutes?: Users,
    muters?: Users,
}

export interface UserAttributes {
    id?: string,
    profileId?: string,
    status: UserStatus,
    username: string,
    displayName: string,
    description: string,
    languageCode: LanguageCode,
    countryCode: CountryCode,
    timezone: TimeZoneID,
    isConnecting?: boolean,
    isOnline?: boolean,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    profile?: FileAttributes,
    account?: AccountAttributes,
    devices?: DeviceAttributes[],
    administrator?: AdministratorAttributes,
}

export class User extends Record<UserAttributes>({
    id: undefined,
    profileId: undefined,
    status: UserStatus.NEW,
    username: '',
    displayName: '',
    description: '',
    languageCode: LanguageCode.English, // fallbackLanguageCode,
    countryCode: CountryCode.JAPAN, // fallbackCountryCode,
    timezone: fallbackTimeZoneID,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    isConnecting: undefined,
    isOnline: undefined,
    profile: Files.getSeed().generateUserProfile(),
    account: undefined,
    devices: undefined,
    administrator: undefined,
}) {
    public static usernameMaxLimit: number = 50;
    public static usernameMinLimit: number = 2;
    public static displayNameMaxLimit: number = 50;
    public static displayNameMinLimit: number = 1;
    public static descriptionMaxLimit: number = 250;
    public static descriptionMinLimit: number = 1;
    public static get defaultProfiles(): File[] {
        return Files.getSeed().userProfiles;
    };
    public readonly records: UserRecordAttributes;

    constructor(args?: Partial<UserAttributes>, records?: UserRecordAttributes) {
        super({
            ...args,
            account: records?.account?.toJSON() ?? args?.account,
            devices: records?.devices?.toJSON() ?? args?.devices,
            administrator: records?.administrator?.toJSON() ?? args?.administrator,
        });
        this.records = {};
        this.records.account = records?.account ? records.account : args?.account ? new Account(args.account) : undefined;
        this.records.devices = records?.devices ? records.devices : args?.devices ? new Devices(args.devices ?? []) : undefined;
        this.records.administrator = records?.administrator ? records.administrator : args?.administrator ? new Administrator(args.administrator) : undefined;
    }

    public setRecord(key: keyof UserRecordAttributes, record?: UserRecordAttributes[keyof UserRecordAttributes]): User {
        return new User(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<UserAttributes, keyof UserRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Users extends Records<User> {
    constructor(array: Array<UserAttributes | User>) {
        super(array, { Record: User, Records: Users })
    }
}