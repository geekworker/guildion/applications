import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { User, UserAttributes } from './User';

export const AccountStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    VERIFIED: 'VERIFIED',
    LOCKED: 'LOCKED',
    DELETING: 'DELETING',
    DELETED: 'DELETED',
} as const;

export type AccountStatus = typeof AccountStatus[keyof typeof AccountStatus];

export const AccountSessionStatus = {
    REJECTED: 'REJECTED',
    ALLOWED: 'ALLOWED'
} as const;

export type AccountSessionStatus = typeof AccountSessionStatus[keyof typeof AccountSessionStatus];

export interface AccountRecordAttributes {
    user?: User,
}

export interface AccountAttributes {
    id?: string,
    userId?: string | null,
    status: AccountStatus,
    sessionStatus: AccountSessionStatus,
    sessionAllowedAt?: Date | null,
    sessionAllowedIntervalSec?: number | null,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    user?: UserAttributes,
}

export class Account extends Record<AccountAttributes>({
    id: undefined,
    userId: undefined,
    status: AccountStatus.NEW,
    sessionStatus: AccountSessionStatus.REJECTED,
    sessionAllowedAt: undefined,
    sessionAllowedIntervalSec: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    user: undefined,
}) {
    public static passwordMaxLimit: number = 250;
    public static passwordMinLimit: number = 8;
    public readonly records: AccountRecordAttributes;

    constructor(args?: Partial<AccountAttributes>, records?: AccountRecordAttributes) {
        super({
            ...args,
            user: records?.user?.toJSON() ?? args?.user,
        });
        this.records = {};
        this.records.user = records?.user ? records.user : args?.user ? new User(args.user) : undefined;
    }

    public setRecord(key: keyof AccountRecordAttributes, record?: AccountRecordAttributes[keyof AccountRecordAttributes]): Account {
        return new Account(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<AccountAttributes, keyof AccountRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUser(): User | undefined {
        const result = this.get('user');
        return result ? (new User(result)) : undefined;
    }
}

export class Accounts extends Records<Account> {
    constructor(array: Array<AccountAttributes | Account>) {
        super(array, { Record: Account, Records: Accounts })
    }
}