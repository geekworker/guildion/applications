import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';

export const NewsLetterSubscriberStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    REJECTED: 'REJECTED',
} as const;

export type NewsLetterSubscriberStatus = typeof NewsLetterSubscriberStatus[keyof typeof NewsLetterSubscriberStatus];

export interface NewsLetterSubscriberRecordAttributes {
}

export interface NewsLetterSubscriberAttributes {
    id?: string,
    displayName: string,
    email: string,
    status: NewsLetterSubscriberStatus,
    createdAt?: string,
    updatedAt?: string,
}

export class NewsLetterSubscriber extends Record<NewsLetterSubscriberAttributes>({
    id: undefined,
    displayName: '',
    email: '',
    status: NewsLetterSubscriberStatus.NEW,
    createdAt: undefined,
    updatedAt: undefined,
}) {
    public readonly records: NewsLetterSubscriberRecordAttributes;

    constructor(args?: Partial<NewsLetterSubscriberAttributes>, records?: NewsLetterSubscriberRecordAttributes) {
        super({
            ...args,
        });
        this.records = {};
    }

    public setRecord(key: keyof NewsLetterSubscriberRecordAttributes, record?: NewsLetterSubscriberRecordAttributes[keyof NewsLetterSubscriberRecordAttributes]): NewsLetterSubscriber {
        return new NewsLetterSubscriber(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<NewsLetterSubscriberAttributes, keyof NewsLetterSubscriberRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class NewsLetterSubscribers extends Records<NewsLetterSubscriber> {
    constructor(array: Array<NewsLetterSubscriberAttributes | NewsLetterSubscriber>) {
        super(array, { Record: NewsLetterSubscriber, Records: NewsLetterSubscribers })
    }
}