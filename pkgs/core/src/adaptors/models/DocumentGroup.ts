import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { DocumentAttributes, Documents } from './Document';
import { DocumentSectionAttributes, DocumentSections } from './DocumentSection';

export interface DocumentGroupRecordAttributes {
    sections?: DocumentSections,
    documents?: Documents,
}

export interface DocumentGroupAttributes {
    id?: string,
    slug: string,
    jaName: string,
    enName: string,
    status: DocumentGroupStatus,
    type: DocumentGroupType,
    publishedAt?: string,
    deletedAt?: string,
    createdAt?: string,
    updatedAt?: string,
    sections?: DocumentSectionAttributes[],
    documents?: DocumentAttributes[],
}

export const DocumentGroupStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',

    LOADING: 'LOADING,'
} as const;

export type DocumentGroupStatus = typeof DocumentGroupStatus[keyof typeof DocumentGroupStatus];

export const DocumentGroupType = {
    DEFAULT: 'default',
} as const;

export type DocumentGroupType = typeof DocumentGroupType[keyof typeof DocumentGroupType];

export class DocumentGroup extends Record<DocumentGroupAttributes>({
    id: undefined,
    slug: '',
    enName: '',
    jaName: '',
    status: DocumentGroupStatus.NEW,
    type: DocumentGroupType.DEFAULT,
    publishedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    documents: undefined,
    sections: undefined,
}) {
    public readonly records: DocumentGroupRecordAttributes;

    constructor(args?: Partial<DocumentGroupAttributes>, records?: DocumentGroupRecordAttributes) {
        super({
            ...args,
            documents: records?.documents?.toJSON() ?? args?.documents,
            sections: records?.sections?.toJSON() ?? args?.sections,
        });
        this.records = {};
        this.records.sections = records?.sections ? records.sections : args?.sections ? new DocumentSections(args?.sections) : undefined;
    }

    public setRecord(key: keyof DocumentGroupRecordAttributes, record?: DocumentGroupRecordAttributes[keyof DocumentGroupRecordAttributes]): DocumentGroup {
        return new DocumentGroup(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<DocumentGroupAttributes, keyof DocumentGroupRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getPublishedAt(): Date | undefined {
        const result = this.get('publishedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getName(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaName;
        default:
        case LanguageCode.English: return this.enName;
        }
    }
}

export class DocumentGroups extends Records<DocumentGroup> {
    constructor(array: Array<DocumentGroupAttributes | DocumentGroup>) {
        super(array, { Record: DocumentGroup, Records: DocumentGroups })
    }
}