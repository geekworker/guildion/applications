import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { SubscriptionInterval } from '@guildion/core/src/constants/SubscriptionInterval';
import { Guild, GuildAttributes } from './Guild';

export const PremiumKeyStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',
    PENDING: 'PENDING',
    DELETED: 'DELETED',
} as const;

export type PremiumKeyStatus = typeof PremiumKeyStatus[keyof typeof PremiumKeyStatus];

export const GuildPremiumKeyStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',
    PENDING: 'PENDING',
    DELETING: 'DELETING',
    DELETED: 'DELETED',
} as const;

export type GuildPremiumKeyStatus = typeof GuildPremiumKeyStatus[keyof typeof GuildPremiumKeyStatus];

export interface PremiumKeyRecordAttributes {
    guild?: Guild,
}

export interface PremiumKeyAttributes {
    id?: string,
    guildPremiumKeyId?: string,
    guildId?: string,
    jaName: string,
    enName: string,
    jaDescription: string,
    enDescription: string,
    storageContentLength?: number,
    basePrice: number,
    webCommissionPrice: number,
    iosCommissionPrice: number,
    androidCommissionPrice: number,
    webProviderKey?: string | null,
    iosProviderKey?: string | null,
    androidProviderKey?: string | null,
    webStatus: PremiumKeyStatus,
    iosStatus: PremiumKeyStatus,
    androidStatus: PremiumKeyStatus,
    interval: SubscriptionInterval,
    publishedAt?: string | null,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    guildStatus?: GuildPremiumKeyStatus,
    commissionRate?: number,
    guild?: GuildAttributes,
}

export class PremiumKey extends Record<PremiumKeyAttributes>({
    id: undefined,
    guildPremiumKeyId: undefined,
    guildId: undefined,
    jaName: '',
    enName: '',
    jaDescription: '',
    enDescription: '',
    storageContentLength: undefined,
    basePrice: 0,
    webCommissionPrice: 0,
    iosCommissionPrice: 0,
    androidCommissionPrice: 0,
    webProviderKey: undefined,
    iosProviderKey: undefined,
    androidProviderKey: undefined,
    webStatus: PremiumKeyStatus.NEW,
    iosStatus: PremiumKeyStatus.NEW,
    androidStatus: PremiumKeyStatus.NEW,
    interval: SubscriptionInterval.Monthly,
    publishedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    guildStatus: undefined,
    commissionRate: undefined,
    guild: undefined,
}) {
    public readonly records: PremiumKeyRecordAttributes;

    constructor(args?: Partial<PremiumKeyAttributes>, records?: PremiumKeyRecordAttributes) {
        super({
            ...args,
            guild: records?.guild?.toJSON() ?? args?.guild,
        });
        this.records = {};
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args?.guild) : undefined;
    }

    public setRecord(key: keyof PremiumKeyRecordAttributes, record?: PremiumKeyRecordAttributes[keyof PremiumKeyRecordAttributes]): PremiumKey {
        return new PremiumKey(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<PremiumKeyAttributes, keyof PremiumKeyRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getPublishedAt(): Date | undefined {
        const result = this.get('publishedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class PremiumKeys extends Records<PremiumKey> {
    constructor(array: Array<PremiumKeyAttributes | PremiumKey>) {
        super(array, { Record: PremiumKey, Records: PremiumKeys })
    }
}