import { Record } from 'immutable';
import { Provider } from '@guildion/core/src/constants/Provider';
import { Records } from '@guildion/core/src/extension/Records';

export interface ConnectionAttributes {
    id?: string,
    isConnecting: boolean,
    secWebsocketKey: string,
    provider: Provider,
    connectedAt?: string | null,
    disconnectedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
}

export class Connection extends Record<ConnectionAttributes>({
    id: undefined,
    isConnecting: false,
    secWebsocketKey: '',
    provider: Provider.Connect,
    connectedAt: undefined,
    disconnectedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
}) {
    public static passwordMaxLimit: number = 250;
    public static passwordMinLimit: number = 8;
    public getConnectedAt(): Date | undefined {
        const result = this.get('connectedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDisconnectedAt(): Date | undefined {
        const result = this.get('disconnectedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Connections extends Records<Connection> {
    constructor(array: Array<ConnectionAttributes | Connection>) {
        super(array, { Record: Connection, Records: Connections })
    }
}