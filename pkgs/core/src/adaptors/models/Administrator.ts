import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { User, UserAttributes } from './User';

export const AdministratorRole = {
    NEW: 'NEW',
    OWNER: 'OWNER',
    CONNECT_SERVER: 'CONNECT_SERVER',
    ADMIN: 'ADMIN',
    MEMBER: 'MEMBER',
} as const;

export type AdministratorRole = typeof AdministratorRole[keyof typeof AdministratorRole];

export interface AdministratorRecordAttributes {
    user?: User,
}

export interface AdministratorAttributes {
    id?: string,
    userId?: string,
    role: AdministratorRole,
    createdAt?: string,
    updatedAt?: string,
    user?: UserAttributes,
}

export class Administrator extends Record<AdministratorAttributes>({
    id: undefined,
    userId: undefined,
    role: AdministratorRole.NEW,
    createdAt: undefined,
    updatedAt: undefined,
    user: undefined,
}) {
    public readonly records: AdministratorRecordAttributes;

    constructor(args?: Partial<AdministratorAttributes>, records?: AdministratorRecordAttributes) {
        super({
            ...args,
            user: records?.user?.toJSON() ?? args?.user,
        });
        this.records = {};
        this.records.user = records?.user ? records.user : args?.user ? new User(args.user) : undefined;
    }

    public setRecord(key: keyof AdministratorRecordAttributes, record?: AdministratorRecordAttributes[keyof AdministratorRecordAttributes]): Administrator {
        return new Administrator(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<AdministratorAttributes, keyof AdministratorRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Administrators extends Records<Administrator> {
    constructor(array: Array<AdministratorAttributes | Administrator>) {
        super(array, { Record: Administrator, Records: Administrators })
    }
}