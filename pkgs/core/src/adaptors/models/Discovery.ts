import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { File, FileAttributes } from './File';
import { GuildAttributes, Guilds } from './Guild';
import { RoomAttributes, Rooms } from './Room';

export interface DiscoveryRecordAttributes {
    jaThumbnail?: File,
    enThumbnail?: File,
    guilds?: Guilds,
    rooms?: Rooms,
}

export interface DiscoveryAttributes {
    id?: string,
    jaThumbnailId?: string | null,
    enThumbnailId?: string | null,
    slug: string,
    jaTitle: string,
    enTitle: string,
    jaDescription: string,
    enDescription: string,
    jaData: string,
    enData: string,
    status: DiscoveryStatus,
    type: DiscoveryType,
    dataType: DiscoveryDataType,
    score: number,
    createdAt?: string,
    updatedAt?: string,
    jaThumbnail?: FileAttributes,
    enThumbnail?: FileAttributes,
    guilds?: GuildAttributes[],
    rooms?: RoomAttributes[],
}

export const DiscoveryStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',

    LOADING: 'LOADING,'
} as const;

export type DiscoveryStatus = typeof DiscoveryStatus[keyof typeof DiscoveryStatus];

export const DiscoveryType = {
    guild: 'guild',
    guilds: 'guilds',
    room: 'room',
    rooms: 'rooms',
    OTHER: 'other',
} as const;

export type DiscoveryType = typeof DiscoveryType[keyof typeof DiscoveryType];

export const DiscoveryDataType = {
    URL: 'url',
    HTML: 'html',
    OTHER: 'other',
} as const;

export type DiscoveryDataType = typeof DiscoveryDataType[keyof typeof DiscoveryDataType];

export class Discovery extends Record<DiscoveryAttributes>({
    id: undefined,
    jaThumbnailId: undefined,
    enThumbnailId: undefined,
    slug: '',
    jaTitle: '',
    enTitle: '',
    jaDescription: '',
    enDescription: '',
    jaData: '',
    enData: '',
    status: DiscoveryStatus.NEW,
    type: DiscoveryType.OTHER,
    dataType: DiscoveryDataType.OTHER,
    score: 0,
    createdAt: undefined,
    updatedAt: undefined,
    jaThumbnail: undefined,
    enThumbnail: undefined,
    guilds: undefined,
    rooms: undefined,
}) {
    public readonly records: DiscoveryRecordAttributes;

    constructor(args?: Partial<DiscoveryAttributes>, records?: DiscoveryRecordAttributes) {
        super({
            ...args,
            jaThumbnail: records?.jaThumbnail?.toJSON() ?? args?.jaThumbnail,
            enThumbnail: records?.enThumbnail?.toJSON() ?? args?.enThumbnail,
            guilds: records?.guilds?.toJSON() ?? args?.guilds,
            rooms: records?.rooms?.toJSON() ?? args?.rooms,
        });
        this.records = {};
        this.records.jaThumbnail = records?.jaThumbnail ? records.jaThumbnail : args?.jaThumbnail ? new File(args?.jaThumbnail) : undefined;
        this.records.enThumbnail = records?.enThumbnail ? records.enThumbnail : args?.enThumbnail ? new File(args?.enThumbnail) : undefined;
        this.records.guilds = records?.guilds ? records.guilds : args?.guilds ? new Guilds(args.guilds) : undefined;
        this.records.rooms = records?.rooms ? records.rooms : args?.rooms ? new Rooms(args.rooms) : undefined;
    }

    public setRecord(key: keyof DiscoveryRecordAttributes, record?: DiscoveryRecordAttributes[keyof DiscoveryRecordAttributes]): Discovery {
        return new Discovery(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<DiscoveryAttributes, keyof DiscoveryRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getTitle(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaTitle;
        default:
        case LanguageCode.English: return this.enTitle;
        }
    }
    public getDescription(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaDescription;
        default:
        case LanguageCode.English: return this.enDescription;
        }
    }
    public getThumbnail(lang: LanguageCode): File | undefined {
        switch(lang) {
        case LanguageCode.Japanese: return this.records.jaThumbnail;
        default:
        case LanguageCode.English: return this.records.enThumbnail;
        }
    }
    public getData(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaData;
        default:
        case LanguageCode.English: return this.enData;
        }
    }
}

export class Discoveries extends Records<Discovery> {
    constructor(array: Array<DiscoveryAttributes | Discovery>) {
        super(array, { Record: Discovery, Records: Discoveries })
    }
}