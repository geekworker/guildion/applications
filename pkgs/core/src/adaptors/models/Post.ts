import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { FileAttributes, Files } from './File';
import { Member, MemberAttributes } from './Member';
import { Message } from './Message';
import { ReactionListAttributes, ReactionLists } from './ReactionList';
import { Room, RoomAttributes } from './Room';
import { TimeZoneID } from '@guildion/core/src/constants/TimeZone';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { Localizer } from '@guildion/core/src/locales';
import { format  } from 'date-fns-tz';
import { parseFromTimeZone } from 'date-fns-timezone';
import { isToday, isYesterday, formatDistanceToNow, nextDay, isAfter, isEqual } from 'date-fns';
import { OGPAttributes, OGPs } from './OGP';

export const PostStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    ARCHIVED: 'ARCHIVED',
    DELETED: 'DELETED',

    LOADING: 'LOADING',
} as const;

export type PostStatus = typeof PostStatus[keyof typeof PostStatus];

export const PostType = {
    PUBLIC: 'PUBLIC',
    MEMBERS: 'MEMBERS',
    GUESTS: 'GUESTS',
} as const;

export type PostType = typeof PostType[keyof typeof PostType];

export const PostReadStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    NOTIFIED: 'NOTIFIED',
    CHECKED: 'CHECKED',
} as const;

export type PostReadStatus = typeof PostReadStatus[keyof typeof PostReadStatus];

export interface PostRecordAttributes {
    sender?: Member,
    parent?: Post,
    room?: Room,
    files?: Files,
    reactions?: ReactionLists,
    comments?: Posts,
    ogps?: OGPs,
}

export interface PostAttributes {
    id?: string,
    senderId?: string,
    parentId?: string,
    roomId?: string,
    status: PostStatus,
    type: PostType,
    body: string,
    location: string,
    readStatus?: PostReadStatus,
    commentsCount: number,
    reactionsCount: number,
    isCommentable: boolean,
    isReactable: boolean,
    hasComment?: boolean,
    hasReaction?: boolean,
    hasYourComment?: boolean,
    hasYourReaction?: boolean,
    archivedAt?: string | null,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    parent?: PostAttributes,
    sender?: MemberAttributes,
    room?: RoomAttributes,
    files?: FileAttributes[],
    reactions?: ReactionListAttributes[],
    comments?: PostAttributes[],
    ogps?: OGPAttributes[],
}

export class Post extends Record<PostAttributes>({
    id: undefined,
    senderId: undefined,
    parentId: undefined,
    roomId: undefined,
    body: '',
    location: '',
    status: PostStatus.NEW,
    type: PostType.PUBLIC,
    readStatus: undefined,
    commentsCount: 0,
    reactionsCount: 0,
    isCommentable: false,
    isReactable: false,
    hasComment: undefined,
    hasReaction: undefined,
    hasYourComment: undefined,
    hasYourReaction: undefined,
    archivedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    parent: undefined,
    sender: undefined,
    room: undefined,
    files: undefined,
    reactions: undefined,
    comments: undefined,
    ogps: undefined,
}) {
    public readonly records: PostRecordAttributes;

    constructor(args?: Partial<PostAttributes>, records?: PostRecordAttributes) {
        super({
            ...args,
            sender: records?.sender?.toJSON() ?? args?.sender,
            room: records?.room?.toJSON() ?? args?.room,
            parent: records?.parent?.toJSON() ?? args?.parent,
            comments: records?.comments?.toJSON() ?? args?.comments,
            files: records?.files?.toJSON() ?? args?.files,
            reactions: records?.reactions?.toJSON() ?? args?.reactions,
            ogps: records?.ogps?.toJSON() ?? args?.ogps,
        });
        this.records = {};
        this.records.sender = records?.sender ? records.sender : args?.sender ? new Member(args?.sender) : undefined;
        this.records.room = records?.room ? records.room : args?.room ? new Room(args?.room) : undefined;
        this.records.parent = records?.parent ? records.parent : args?.parent ? new Post(args?.parent) : undefined;
        this.records.comments = records?.comments ? records.comments : args?.comments ? new Posts(args?.comments) : undefined;
        this.records.files = records?.files ? records.files : args?.files ? new Files(args?.files) : undefined;
        this.records.reactions = records?.reactions ? records.reactions : args?.reactions ? new ReactionLists(args?.reactions) : undefined;
        this.records.ogps = records?.ogps ? records.ogps : args?.ogps ? new OGPs(args?.ogps) : undefined;
    }

    public setRecord(key: keyof PostRecordAttributes, record?: PostRecordAttributes[keyof PostRecordAttributes]): Post {
        return new Post(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<PostAttributes, keyof PostRecordAttributes> {
        return { ...this.toJSON() };
    }
    public checkBodyValid(): boolean {
        return this.body.length > 0 && !Message.invalidRegExp.test(this.body);
    }
    public getCreatedHM(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "hh:mm", { timeZone: timezone }) : undefined;
    }
    public getCreatedYMD(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "yyyy/MM/dd", { timeZone: timezone }) : undefined;
    }
    public checkTomorrowDate(before: Message, timezone: TimeZoneID): boolean {
        const thisCreatedAt = this.getCreatedAt();
        const beforeCreatedAt = before.getCreatedAt();
        if (!thisCreatedAt || !beforeCreatedAt || !isAfter(thisCreatedAt, beforeCreatedAt)) return false;
        const thisCreatedAtTz = parseFromTimeZone(thisCreatedAt.toISOString(), { timeZone: timezone });
        const beforeCreatedAtTz = parseFromTimeZone(beforeCreatedAt.toISOString(), { timeZone: timezone });
        const targetAt = new Date(beforeCreatedAtTz.getFullYear(), beforeCreatedAtTz.getMonth(), beforeCreatedAtTz.getDate() + 1);
        const startAt = new Date(thisCreatedAtTz.getFullYear(), thisCreatedAtTz.getMonth(), thisCreatedAtTz.getDate());
        return isAfter(startAt, targetAt) || isEqual(startAt, targetAt);
    }
    public getCreatedDistanceNow(lang: LanguageCode): string | undefined {
        const result = this.get('createdAt');
        const localizer = new Localizer(lang);
        return result ? formatDistanceToNow(new Date(result), { locale: localizer.localeDateFns }) : undefined;
    }
    public getCreatedDateSection(timezone: TimeZoneID, lang: LanguageCode): string | undefined {
        const result = this.get('createdAt')
        const localizer = new Localizer(lang);
        return result ?
            isToday(new Date(result)) ?
            localizer.dictionary.g.date.today :
            isYesterday(new Date(result)) ?
            localizer.dictionary.g.date.yesterday :
            format(new Date(result), "yyyy/MM/dd", { timeZone: timezone }) :
            undefined;
    }
    public getArchivedAt(): Date | undefined {
        const result = this.get('archivedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Posts extends Records<Post> {
    constructor(array: Array<PostAttributes | Post>) {
        super(array, { Record: Post, Records: Posts })
    }
}