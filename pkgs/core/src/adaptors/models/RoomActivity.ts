import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { MemberAttributes, Members } from './Member';
import { Room, RoomAttributes } from './Room';
import { TimeZoneID } from '@guildion/core/src/constants/TimeZone';
import { format } from 'date-fns-tz'
import { RoomLog, RoomLogAttributes } from './RoomLog';
import { SyncVision, SyncVisionAttributes } from './SyncVision';

export interface RoomActivityRecordAttributes {
    room?: Room,
    connectingMembers?: Members,
    syncVision?: SyncVision,
    log?: RoomLog,
}

export interface RoomActivityAttributes {
    id?: string,
    roomId?: string,
    logId?: string | null,
    syncVisionId?: string | null,
    connectsCount: number,
    membersCount: number,
    postsCount: number,
    publicPostsCount: number,
    contentLength: number,
    createdAt?: string,
    updatedAt?: string,
    room?: RoomAttributes,
    syncVision?: SyncVisionAttributes,
    log?: RoomLogAttributes,
    connectingMembers?: MemberAttributes[],
    notificationsCount?: number,
    messageNotificationsCount?: number,
    postNotificationsCount?: number,
}

export class RoomActivity extends Record<RoomActivityAttributes>({
    id: undefined,
    roomId: undefined,
    logId: undefined,
    syncVisionId: undefined,
    connectsCount: 0,
    membersCount: 0,
    postsCount: 0,
    publicPostsCount: 0,
    contentLength: 0,
    createdAt: undefined,
    updatedAt: undefined,
    syncVision: undefined,
    log: undefined,
    connectingMembers: undefined,
    notificationsCount: undefined,
    messageNotificationsCount: undefined,
    postNotificationsCount: undefined,
}) {
    public static connectingMembersMaxLimit: number = 4;
    public readonly records: RoomActivityRecordAttributes;

    constructor(args?: Partial<RoomActivityAttributes>, records?: RoomActivityRecordAttributes) {
        super({
            ...args,
            room: records?.room?.toJSON() ?? args?.room,
            connectingMembers: records?.connectingMembers?.toJSON() ?? args?.connectingMembers,
            syncVision: records?.syncVision?.toJSON() ?? args?.syncVision,
            log: records?.log?.toJSON() ?? args?.log,
        });
        this.records = {};
        this.records.room = records?.room ? records.room : args?.room ? new Room(args.room) : undefined;
        this.records.connectingMembers = records?.connectingMembers ? records.connectingMembers : args?.connectingMembers ? new Members(args.connectingMembers) : undefined;
        this.records.syncVision = records?.syncVision ? records.syncVision : args?.syncVision ? new SyncVision(args.syncVision) : undefined;
        this.records.log = records?.log ? records.log : args?.log ? new RoomLog(args.log) : undefined;
    }

    public setRecord(key: keyof RoomActivityRecordAttributes, record?: RoomActivityRecordAttributes[keyof RoomActivityRecordAttributes]): RoomActivity {
        return new RoomActivity(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<RoomActivityAttributes, keyof RoomActivityRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedHM(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "hh:mm", { timeZone: timezone }) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class RoomActivities extends Records<RoomActivity> {
    constructor(array: Array<RoomActivityAttributes | RoomActivity>) {
        super(array, { Record: RoomActivity, Records: RoomActivities })
    }
}