import { Record } from 'immutable';
import { CountryCode } from '@guildion/core/src/constants/CountryCode';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { Records } from '@guildion/core/src/extension/Records';
import { Users, UserAttributes } from './User';
import { TimeZone, TimeZoneID } from '@guildion/core/src/constants/TimeZone';

export interface DeviceRecordAttributes {
    users?: Users,
}

export interface DeviceAttributes {
    id?: string,
    udid: string,
    os: string,
    model: string,
    status: DeviceStatus
    languageCode: LanguageCode,
    countryCode: CountryCode,
    timezone: TimeZoneID,
    onesignalNotificationId?: string | null,
    notificationToken?: string | null,
    appVersion: string,
    createdAt?: string,
    updatedAt?: string,
    users?: UserAttributes[],
}

export const DeviceStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    LOCKED: 'LOCKED',
} as const;

export type DeviceStatus = typeof DeviceStatus[keyof typeof DeviceStatus];

export const UserDeviceStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    TRUSTED: 'TRUSTED',
    LOCKED: 'LOCKED',
} as const;

export type UserDeviceStatus = typeof UserDeviceStatus[keyof typeof UserDeviceStatus];

export const UserDeviceSessionStatus = {
    LOGOUT: 'LOGOUT',
    LOGIN: 'LOGIN',
    SHOULD_LOGOUT: 'SHOULD_LOGOUT',
} as const;

export type UserDeviceSessionStatus = typeof UserDeviceSessionStatus[keyof typeof UserDeviceSessionStatus];

export class Device extends Record<DeviceAttributes>({
    id: undefined,
    udid: '',
    os: '',
    status: DeviceStatus.NEW,
    model: '',
    languageCode: LanguageCode.English,
    countryCode: CountryCode.JAPAN,
    timezone: TimeZone.Tokyo.id,
    onesignalNotificationId: undefined,
    notificationToken: undefined,
    appVersion: '0.0.0',
    createdAt: undefined,
    updatedAt: undefined,
    users: undefined,
}) {
    public static accountMaxLimit: number = 10;
    public readonly records: DeviceRecordAttributes;

    constructor(args?: Partial<DeviceAttributes>, records?: DeviceRecordAttributes) {
        super({
            ...args,
            users: records?.users?.toJSON() ?? args?.users,
        });
        this.records = {};
        this.records.users = records?.users ? records.users : args?.users ? new Users(args?.users) : undefined;
    }

    public setRecord(key: keyof DeviceRecordAttributes, record?: DeviceRecordAttributes[keyof DeviceRecordAttributes]): Device {
        return new Device(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<DeviceAttributes, keyof DeviceRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Devices extends Records<Device> {
    constructor(array: Array<DeviceAttributes | Device>) {
        super(array, { Record: Device, Records: Devices })
    }
}