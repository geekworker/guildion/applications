import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { Guild, GuildAttributes } from './Guild';
import { File, FileAttributes } from './File';
import { allCases } from '../../interfaces/CaseIterable';

export const RoleType = {
    Members: 'members',
    Trial: 'trial',
    Premium: 'premium',
    Creator: 'creator',
    Admin: 'admin',
    Owner: 'owner',
    Custom: 'custom',
} as const;

export type RoleType = typeof RoleType[keyof typeof RoleType];

export const GuildRoleTypes: RoleType[] = allCases(RoleType).filter(rt => rt != RoleType.Custom);
export const TrialRoleTypes: RoleType[] = [RoleType.Members, RoleType.Trial];
export const PremiumRoleTypes: RoleType[] = [RoleType.Members, RoleType.Premium];
export const CreatorRoleTypes: RoleType[] = [RoleType.Members, RoleType.Creator];
export const AdminRoleTypes: RoleType[] = [RoleType.Members, RoleType.Admin];
export const OwnerRoleTypes: RoleType[] = [RoleType.Members, RoleType.Admin, RoleType.Owner];

export interface RoleRecordAttributes {
    guild?: Guild,
    file?: File,
}

export interface RoleAttributes {
    id?: string,
    memberId?: string,
    guildId?: string,
    fileId?: string,
    roomId?: string,
    type: RoleType,
    createdAt?: string,
    updatedAt?: string,
    guild?: GuildAttributes,
    file?: FileAttributes,
}

export class Role extends Record<RoleAttributes>({
    id: undefined,
    memberId: undefined,
    guildId: undefined,
    fileId: undefined,
    roomId: undefined,
    type: RoleType.Custom,
    createdAt: undefined,
    updatedAt: undefined,
    guild: undefined,
    file: undefined,
}) {
    public static displayNameMaxLimit: number = 50;
    public static displayNameMinLimit: number = 1;
    public readonly records: RoleRecordAttributes;

    constructor(args?: Partial<RoleAttributes>, records?: RoleRecordAttributes) {
        super({
            ...args,
            guild: records?.guild?.toJSON() ?? args?.guild,
            file: records?.file?.toJSON() ?? args?.file,
        });
        this.records = {};
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args.guild ?? []) : undefined;
        this.records.file = records?.file ? records.file : args?.file ? new File(args.file ?? []) : undefined;
    }

    public setRecord(key: keyof RoleRecordAttributes, record?: RoleRecordAttributes[keyof RoleRecordAttributes]): Role {
        return new Role(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<RoleAttributes, keyof RoleRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Roles extends Records<Role> {
    constructor(array: Array<RoleAttributes | Role>) {
        super(array, { Record: Role, Records: Roles })
    }
}