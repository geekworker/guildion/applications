import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { BlogAttributes, Blogs } from './Blog';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';


export interface BlogCategoryRecordAttributes {
    blogs?: Blogs,
    parent?: BlogCategory,
    children?: BlogCategories,
}

export interface BlogCategoryAttributes {
    id?: string,
    parentId?: string | null,
    jaName: string,
    enName: string,
    slug: string,
    index: number,
    status: BlogCategoryStatus,
    createdAt?: string,
    updatedAt?: string,
    blogs?: BlogAttributes[],
    parent?: BlogCategoryAttributes,
    children?: BlogCategoryAttributes[],
}

export const BlogCategoryStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',

    LOADING: 'LOADING,'
} as const;

export type BlogCategoryStatus = typeof BlogCategoryStatus[keyof typeof BlogCategoryStatus];

export class BlogCategory extends Record<BlogCategoryAttributes>({
    id: undefined,
    jaName: '',
    enName: '',
    slug: '',
    index: 0,
    status: BlogCategoryStatus.NEW,
    createdAt: undefined,
    updatedAt: undefined,
    blogs: undefined,
    parent: undefined,
    children: undefined,
}) {
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public readonly records: BlogCategoryRecordAttributes;

    constructor(args?: Partial<BlogCategoryAttributes>, records?: BlogCategoryRecordAttributes) {
        super({
            ...args,
            blogs: records?.blogs?.toJSON() ?? args?.blogs,
            parent: records?.parent?.toJSON() ?? args?.parent,
            children: records?.children?.toJSON() ?? args?.children,
        });
        this.records = {};
        this.records.blogs = records?.blogs ? records.blogs : args?.blogs ? new Blogs(args.blogs ?? []) : undefined;
        this.records.parent = records?.parent ? records.parent : args?.parent ? new BlogCategory(args.parent) : undefined;
        this.records.children = records?.children ? records.children : args?.children ? new BlogCategories(args.children ?? []) : undefined;
    }

    public setRecord(key: keyof BlogCategoryRecordAttributes, record?: BlogCategoryRecordAttributes[keyof BlogCategoryRecordAttributes]): BlogCategory {
        return new BlogCategory(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<BlogCategoryAttributes, keyof BlogCategoryRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getName(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaName;
        default:
        case LanguageCode.English: return this.enName;
        }
    }
}

export class BlogCategories extends Records<BlogCategory> {
    constructor(array: Array<BlogCategoryAttributes | BlogCategory>) {
        super(array, { Record: BlogCategory, Records: BlogCategories })
    }
}