import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { Guild, GuildAttributes } from './Guild';
import { RoomAttributes, Rooms } from './Room';

export interface RoomCategoryRecordAttributes {
    rooms?: Rooms,
    guild?: Guild,
}

export interface RoomCategoryAttributes {
    id?: string,
    guildId?: string,
    name: string,
    description: string,
    index: number,
    createdAt?: string,
    updatedAt?: string,
    loading?: boolean,
    rooms?: RoomAttributes[],
    guild?: GuildAttributes,
}

export class RoomCategory extends Record<RoomCategoryAttributes>({
    id: undefined,
    guildId: undefined,
    name: '',
    description: '',
    index: 0,
    createdAt: undefined,
    updatedAt: undefined,
    loading: undefined,
    rooms: undefined,
    guild: undefined,
}) {
    public static nameMaxLimit: number = 50;
    public static nameMinLimit: number = 1;
    public readonly records: RoomCategoryRecordAttributes;

    constructor(args?: Partial<RoomCategoryAttributes>, records?: RoomCategoryRecordAttributes) {
        super({
            ...args,
            guild: records?.guild?.toJSON() ?? args?.guild,
            rooms: records?.rooms?.toJSON() ?? args?.rooms,
        });
        this.records = {};
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args?.guild) : undefined;
        this.records.rooms = records?.rooms ? records.rooms : args?.rooms ? new Rooms(args?.rooms) : undefined;
    }

    public setRecord(key: keyof RoomCategoryRecordAttributes, record?: RoomCategoryRecordAttributes[keyof RoomCategoryRecordAttributes]): RoomCategory {
        return new RoomCategory(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<RoomCategoryAttributes, keyof RoomCategoryRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class RoomCategories extends Records<RoomCategory> {
    constructor(array: Array<RoomCategoryAttributes | RoomCategory>) {
        super(array, { Record: RoomCategory, Records: RoomCategories })
    }
}