import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { safeURL } from '@guildion/core/src/extension/URL';

export const SocialLinkType = {
    TwitterProfile: 'twitter_profile',
    FacebookProfile: 'facebook_profile',
    InstagramProfile: 'instgram_profile',
    YouTubeChannel: 'youtube_channel',
    Custom: 'custom',
    Other: 'other',
} as const;

export type SocialLinkType = typeof SocialLinkType[keyof typeof SocialLinkType];

export interface SocialLinkRecordAttributes {
}

export interface SocialLinkAttributes {
    id?: string,
    url: string,
    createdAt?: string,
    updatedAt?: string,
}

export class SocialLink extends Record<SocialLinkAttributes>({
    id: undefined,
    url: '',
    createdAt: undefined,
    updatedAt: undefined,
}) {
    public readonly records: SocialLinkRecordAttributes;

    public static youtubeChannelRegExp: RegExp = /(https?:\/\/)?(www\.)?youtube\.com\/(channel|user)\/[\w-]+/;
    public static twitterProfileRegExp: RegExp = /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/;
    public static instagramProfileRegExp: RegExp = /(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am)\/([A-Za-z0-9-_]+)/im;
    public static facebookProfileRegExp: RegExp = /(?:https?:\/\/)?(?:www\.)?(?:facebook|fb|m\.facebook)\.(?:com|me)\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]+)(?:\/)?/i;

    constructor(args?: Partial<SocialLinkAttributes>, records?: SocialLinkRecordAttributes) {
        super({
            ...args,
        });
        this.records = {};
    }

    public setRecord(key: keyof SocialLinkRecordAttributes, record?: SocialLinkRecordAttributes[keyof SocialLinkRecordAttributes]): SocialLink {
        return new SocialLink(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<SocialLinkAttributes, keyof SocialLinkRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt');
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt');
        return result ? (new Date(result)) : undefined;
    }
    public getURL(): URL | undefined {
        const result = this.get('url');
        return result ? (safeURL(result)) : undefined;
    }
    public get type(): SocialLinkType {
        const url = this.get('url');
        if (!url || !this.getURL()) return SocialLinkType.Other;
        switch(true) {
        case SocialLink.youtubeChannelRegExp.test(url): return SocialLinkType.YouTubeChannel;
        case SocialLink.twitterProfileRegExp.test(url): return SocialLinkType.TwitterProfile;
        case SocialLink.facebookProfileRegExp.test(url): return SocialLinkType.FacebookProfile;
        case SocialLink.instagramProfileRegExp.test(url): return SocialLinkType.InstagramProfile;
        default: return SocialLinkType.Custom;
        }
    }
}

export class SocialLinks extends Records<SocialLink> {
    constructor(array: Array<SocialLinkAttributes | SocialLink>) {
        super(array, { Record: SocialLink, Records: SocialLinks })
    }
}