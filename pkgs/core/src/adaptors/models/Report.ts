import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { UserAttributes, User } from './User';
import { Guild, GuildAttributes } from './Guild';
import { Room, RoomAttributes } from './Room';
import { File, FileAttributes } from './File';

export const ReportType = {
    User: 'user',
    Guild: 'guild',
    Room: 'room',
    File: 'file',
    App: 'app',
    Other: 'other',
} as const;

export type ReportType = typeof ReportType[keyof typeof ReportType];

export const ReportStatus = {
    NEW: 'NEW',
    OPEN: 'OPEN',
    RESOLVED: 'RESOLVED',
    CLOSED: 'CLOSED',
    REJECTED: 'REJECTED',
} as const;

export type ReportStatus = typeof ReportStatus[keyof typeof ReportStatus];

export interface ReportRecordAttributes {
    sender?: User,
    guild?: Guild,
    room?: Room,
    user?: User,
    file?: File,
}

export interface ReportAttributes {
    id?: string,
    senderId?: string,
    status: ReportStatus,
    type: ReportType,
    title: string,
    body: string,
    createdAt?: string,
    updatedAt?: string,
    sender?: UserAttributes,
    guildId?: string,
    guild?: GuildAttributes,
    roomId?: string,
    room?: RoomAttributes,
    userId?: string,
    user?: UserAttributes,
    fileId?: string,
    file?: FileAttributes,
}

export class Report extends Record<ReportAttributes>({
    id: undefined,
    senderId: undefined,
    status: ReportStatus.NEW,
    type: ReportType.Other,
    title: '',
    body: '',
    createdAt: undefined,
    updatedAt: undefined,
    sender: undefined,
    guildId: undefined,
    guild: undefined,
    roomId: undefined,
    room: undefined,
    userId: undefined,
    user: undefined,
    fileId: undefined,
    file: undefined,
}) {
    public readonly records: ReportRecordAttributes;

    constructor(args?: Partial<ReportAttributes>, records?: ReportRecordAttributes) {
        super({
            ...args,
            sender: records?.sender?.toJSON() ?? args?.sender,
            guild: records?.guild?.toJSON() ?? args?.guild,
            room: records?.room?.toJSON() ?? args?.room,
            user: records?.user?.toJSON() ?? args?.user,
            file: records?.file?.toJSON() ?? args?.file,
        });
        this.records = {};
        this.records.sender = records?.sender ? records.sender : args?.sender ? new User(args.sender) : undefined;
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args.guild) : undefined;
        this.records.room = records?.room ? records.room : args?.room ? new Room(args.room) : undefined;
        this.records.user = records?.user ? records.user : args?.user ? new User(args.user) : undefined;
        this.records.file = records?.file ? records.file : args?.file ? new File(args.file) : undefined;
    }

    public setRecord(key: keyof ReportRecordAttributes, record?: ReportRecordAttributes[keyof ReportRecordAttributes]): Report {
        return new Report(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<ReportAttributes, keyof ReportRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Reports extends Records<Report> {
    constructor(array: Array<ReportAttributes | Report>) {
        super(array, { Record: Report, Records: Reports })
    }
}