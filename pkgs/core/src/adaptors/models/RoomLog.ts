import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { MemberAttributes, Member } from './Member';
import { File, FileAttributes } from './File';
import { Message, MessageAttributes } from './Message';
import { Room, RoomAttributes } from './Room';
import { TimeZone } from '@guildion/core/src/constants/TimeZone';
import { format } from 'date-fns-tz'
import { Post, PostAttributes } from './Post';
import { Role, RoleAttributes } from './Role';
import { SyncVision, SyncVisionAttributes } from './SyncVision';

export const RoomLogType = {
    Welcome: 'welcome',
    MessageCreate: 'message_create',
    PostCreate: 'post_create',
    SyncVisionCreate: 'sync_vision_create',
    RoomFileCreate: 'room_file_create',
    RoomRoleCreate: 'room_role_create',
    MemberCreate: 'member_create',
    MemberDestroy: 'member_destroy',
    Other: 'other',
} as const;

export type RoomLogType = typeof RoomLogType[keyof typeof RoomLogType];

export interface RoomLogRecordAttributes {
    room?: Room,
    syncVision?: SyncVision,
    message?: Message,
    post?: Post,
    role?: Role,
    file?: File,
    member?: Member,
}

export interface RoomLogAttributes {
    id?: string,
    roomId?: string,
    messageId?: string | null,
    postId?: string | null,
    syncVisionId?: string | null,
    roomRoleId?: string | null,
    roomFileId?: string | null,
    memberId?: string | null,
    type: RoomLogType,
    changedColumn?: string | null,
    beforeDiff?: string | null,
    afterDiff?: string | null,
    createdAt?: string,
    updatedAt?: string,
    room?: RoomAttributes,
    syncVision?: SyncVisionAttributes,
    message?: MessageAttributes,
    post?: PostAttributes,
    role?: RoleAttributes,
    file?: FileAttributes,
    member?: MemberAttributes,
}

export class RoomLog extends Record<RoomLogAttributes>({
    id: undefined,
    roomId: undefined,
    messageId: undefined,
    postId: undefined,
    syncVisionId: undefined,
    roomRoleId: undefined,
    roomFileId: undefined,
    type: RoomLogType.Other,
    changedColumn: undefined,
    beforeDiff: undefined,
    afterDiff: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    room: undefined,
    syncVision: undefined,
    message: undefined,
    post: undefined,
    role: undefined,
    file: undefined,
    member: undefined,
}) {
    public static connectingMembersMaxLimit: number = 4;
    public readonly records: RoomLogRecordAttributes;

    constructor(args?: Partial<RoomLogAttributes>, records?: RoomLogRecordAttributes) {
        super({
            ...args,
            room: records?.room?.toJSON() ?? args?.room,
            syncVision: records?.syncVision?.toJSON() ?? args?.syncVision,
            message: records?.message?.toJSON() ?? args?.message,
            post: records?.post?.toJSON() ?? args?.post,
            role: records?.role?.toJSON() ?? args?.role,
            file: records?.file?.toJSON() ?? args?.file,
            member: records?.member?.toJSON() ?? args?.member,
        });
        this.records = {};
        this.records.room = records?.room ? records.room : args?.room ? new Room(args.room) : undefined;
        this.records.syncVision = records?.syncVision ? records.syncVision : args?.syncVision ? new SyncVision(args.syncVision) : undefined;
        this.records.message = records?.message ? records.message : args?.message ? new Message(args.message) : undefined;
        this.records.post = records?.post ? records.post : args?.post ? new Post(args.post) : undefined;
        this.records.role = records?.role ? records.role : args?.role ? new Role(args.role) : undefined;
        this.records.file = records?.file ? records.file : args?.file ? new File(args.file) : undefined;
        this.records.member = records?.member ? records.member : args?.member ? new Member(args.member) : undefined;
    }

    public setRecord(key: keyof RoomLogRecordAttributes, record?: RoomLogRecordAttributes[keyof RoomLogRecordAttributes]): RoomLog {
        return new RoomLog(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<RoomLogAttributes, keyof RoomLogRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedHM(timezone: TimeZone): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "hh:mm", { timeZone: timezone.id }) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public toString(): string {
        switch(this.type) {
        default:
        case RoomLogType.Other: return '';
        };
    }
}

export class RoomLogs extends Records<RoomLog> {
    constructor(array: Array<RoomLogAttributes | RoomLog>) {
        super(array, { Record: RoomLog, Records: RoomLogs })
    }
}