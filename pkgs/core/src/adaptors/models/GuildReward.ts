import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { RewardMethod } from '@guildion/core/src/constants/RewardMethod';
import { Member, MemberAttributes } from './Member';
import { GuildRewardPaymentAttributes, GuildRewardPayments } from './GuildRewardPayment';

export const GuildRewardStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    CONFIRMED: 'CONFIRMED',
    ACTIVE: 'ACTIVE',
    REJECTED: 'REJECTED',
    CANCELING: 'CANCELING',
    CANCELED: 'CANCELED',
} as const;

export type GuildRewardStatus = typeof GuildRewardStatus[keyof typeof GuildRewardStatus];

export interface GuildRewardRecordAttributes {
    member?: Member,
    payments?: GuildRewardPayments,
}

export interface GuildRewardAttributes {
    id?: string,
    memberId?: string,
    status: GuildRewardStatus,
    method: RewardMethod,
    providerKey: string,
    commissionRate: number,
    confirmedAt?: string | null,
    activedAt?: string | null,
    canceledAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    member?: MemberAttributes,
    payments?: GuildRewardPaymentAttributes[],
}

export class GuildReward extends Record<GuildRewardAttributes>({
    id: undefined,
    memberId: undefined,
    status: GuildRewardStatus.NEW,
    method: RewardMethod.WISE,
    providerKey: '',
    commissionRate: 0,
    confirmedAt: undefined,
    activedAt: undefined,
    canceledAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    member: undefined,
    payments: undefined,
}) {
    public readonly records: GuildRewardRecordAttributes;

    constructor(args?: Partial<GuildRewardAttributes>, records?: GuildRewardRecordAttributes) {
        super({
            ...args,
            member: records?.member?.toJSON() ?? args?.member,
            payments: records?.payments?.toJSON() ?? args?.payments,
        });
        this.records = {};
        this.records.member = records?.member ? records.member : args?.member ? new Member(args?.member) : undefined;
        this.records.payments = records?.payments ? records.payments : args?.payments ? new GuildRewardPayments(args?.payments) : undefined;
    }

    public setRecord(key: keyof GuildRewardRecordAttributes, record?: GuildRewardRecordAttributes[keyof GuildRewardRecordAttributes]): GuildReward {
        return new GuildReward(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<GuildRewardAttributes, keyof GuildRewardRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getConfirmedAt(): Date | undefined {
        const result = this.get('confirmedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getActivedAt(): Date | undefined {
        const result = this.get('activedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCanceledAt(): Date | undefined {
        const result = this.get('canceledAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class GuildRewards extends Records<GuildReward> {
    constructor(array: Array<GuildRewardAttributes | GuildReward>) {
        super(array, { Record: GuildReward, Records: GuildRewards })
    }
}