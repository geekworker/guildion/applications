import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { User, UserAttributes } from './User';
import { Guild, GuildAttributes } from './Guild';
import { RoleAttributes, Roles } from './Role';
import { File, FileAttributes, Files } from './File';

export const MemberStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    BANNED: 'BANNED',
    LOCKED: 'LOCKED',
    DELETED: 'DELETED',
    GUEST: 'GUEST',
} as const;

export type MemberStatus = typeof MemberStatus[keyof typeof MemberStatus];

export const RoomMemberType = {
    MEMBER: 'MEMBER',
    GUEST: 'GUEST',
} as const;

export type RoomMemberType = typeof RoomMemberType[keyof typeof RoomMemberType];

export const RoomMemberStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    BANNED: 'BANNED',
    LOCKED: 'LOCKED',
    DELETED: 'DELETED',
    GUEST: 'GUEST',
} as const;

export type RoomMemberStatus = typeof RoomMemberStatus[keyof typeof RoomMemberStatus];

export const RoomMemberConnectionStatus = {
    NEW: 'NEW',
    CONNECTING: 'CONNECTING',
    VOICE: 'VOICE',
    VIDEO: 'VIDEO',
    SCREENSHARE: 'SCREENSHARE',
    VOICE_VIDEO: 'VOICE_VIDEO',
    VOICE_SCREENSHARE: 'VOICE_SCREENSHARE',
    DISCONNECTED: 'DISCONNECTED'
} as const;

export type RoomMemberConnectionStatus = typeof RoomMemberConnectionStatus[keyof typeof RoomMemberConnectionStatus];

export interface MemberRecordAttributes {
    profile?: File,
    user?: User,
    guild?: Guild,
    roles?: Roles,
}

export interface MemberAttributes {
    id?: string,
    profileId?: string,
    userId?: string,
    guildId?: string,
    roomId?: string,
    roomMemberId?: string,
    status: MemberStatus,
    roomConnectionStatus?: RoomMemberConnectionStatus,
    roomMemberType?: RoomMemberType,
    displayName: string,
    description: string,
    index: number,
    isMute?: boolean | null,
    isMuteMentions?: boolean | null,
    isConnecting?: boolean,
    createdAt?: string,
    updatedAt?: string,
    profile?: FileAttributes,
    user?: UserAttributes,
    guild?: GuildAttributes,
    roles?: RoleAttributes[],
}

export class Member extends Record<MemberAttributes>({
    id: undefined,
    userId: undefined,
    roomId: undefined,
    roomMemberId: undefined,
    guildId: undefined,
    profileId: undefined,
    displayName: '',
    description: '',
    status: MemberStatus.NEW,
    roomMemberType: undefined,
    roomConnectionStatus: RoomMemberConnectionStatus.NEW,
    index: 0,
    isMute: false,
    isMuteMentions: false,
    isConnecting: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    profile: Files.getSeed().generateUserProfile(),
    user: undefined,
    guild: undefined,
    roles: undefined,
}) {
    public static displayNameMaxLimit: number = 50;
    public static displayNameMinLimit: number = 1;
    public static descriptionMaxLimit: number = 250;
    public static descriptionMinLimit: number = 1;
    public static get defaultProfiles(): File[] {
        return Files.getSeed().userProfiles;
    };
    public readonly records: MemberRecordAttributes;

    constructor(args?: Partial<MemberAttributes>, records?: MemberRecordAttributes) {
        super({
            ...args,
            profile: records?.profile?.toJSON() ?? args?.profile,
            user: records?.user?.toJSON() ?? args?.user,
            guild: records?.guild?.toJSON() ?? args?.guild,
            roles: records?.roles?.toJSON() ?? args?.roles,
        });
        this.records = {};
        this.records.profile = records?.profile ? records.profile : args?.profile ? new File(args?.profile) : undefined;
        this.records.user = records?.user ? records.user : args?.user ? new User(args?.user) : undefined;
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args?.guild) : undefined;
        this.records.roles = records?.roles ? records.roles : args?.roles ? new Roles(args?.roles) : undefined;
    }

    public setRecord(key: keyof MemberRecordAttributes, record?: MemberRecordAttributes[keyof MemberRecordAttributes]): Member {
        return new Member(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<MemberAttributes, keyof MemberRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Members extends Records<Member> {
    constructor(array: Array<MemberAttributes | Member>) {
        super(array, { Record: Member, Records: Members })
    }
}