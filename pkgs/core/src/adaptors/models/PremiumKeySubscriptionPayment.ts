import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { PaymentMethod } from '@guildion/core/src/constants/PaymentMethod';
import { PremiumKeySubscription, PremiumKeySubscriptionAttributes } from './PremiumKeySubscription';

export const PremiumKeySubscriptionPaymentStatus = {
    NEW: 'NEW',
    NOT_YET_PAID: 'NOT_YET_PAID',
    PAID: 'PAID',
    CANCELED: 'CANCELED',
    REFUNDED: 'REFUNDED',
} as const;

export type PremiumKeySubscriptionPaymentStatus = typeof PremiumKeySubscriptionPaymentStatus[keyof typeof PremiumKeySubscriptionPaymentStatus];

export interface PremiumKeySubscriptionPaymentRecordAttributes {
    subscription?: PremiumKeySubscription,
}

export interface PremiumKeySubscriptionPaymentAttributes {
    id?: string,
    subscriptionId?: string,
    status: PremiumKeySubscriptionPaymentStatus,
    method: PaymentMethod,
    providerKey: string,
    requestedAt?: string | null,
    paidAt?: string | null,
    canceledAt?: string | null,
    refundedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    subscription?: PremiumKeySubscriptionAttributes,
}

export class PremiumKeySubscriptionPayment extends Record<PremiumKeySubscriptionPaymentAttributes>({
    id: undefined,
    subscriptionId: undefined,
    status: PremiumKeySubscriptionPaymentStatus.NEW,
    method: PaymentMethod.OTHER,
    providerKey: '',
    requestedAt: undefined,
    paidAt: undefined,
    canceledAt: undefined,
    refundedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    subscription: undefined,
}) {
    public readonly records: PremiumKeySubscriptionPaymentRecordAttributes;

    constructor(args?: Partial<PremiumKeySubscriptionPaymentAttributes>, records?: PremiumKeySubscriptionPaymentRecordAttributes) {
        super({
            ...args,
            subscription: records?.subscription?.toJSON() ?? args?.subscription,
        });
        this.records = {};
        this.records.subscription = records?.subscription ? records.subscription : args?.subscription ? new PremiumKeySubscription(args?.subscription) : undefined;
    }

    public setRecord(key: keyof PremiumKeySubscriptionPaymentRecordAttributes, record?: PremiumKeySubscriptionPaymentRecordAttributes[keyof PremiumKeySubscriptionPaymentRecordAttributes]): PremiumKeySubscriptionPayment {
        return new PremiumKeySubscriptionPayment(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<PremiumKeySubscriptionPaymentAttributes, keyof PremiumKeySubscriptionPaymentRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getRequestedAt(): Date | undefined {
        const result = this.get('requestedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getPaidAt(): Date | undefined {
        const result = this.get('paidAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCanceledAt(): Date | undefined {
        const result = this.get('canceledAt')
        return result ? (new Date(result)) : undefined;
    }
    public getRefundedAt(): Date | undefined {
        const result = this.get('refundedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class PremiumKeySubscriptionPayments extends Records<PremiumKeySubscriptionPayment> {
    constructor(array: Array<PremiumKeySubscriptionPaymentAttributes | PremiumKeySubscriptionPayment>) {
        super(array, { Record: PremiumKeySubscriptionPayment, Records: PremiumKeySubscriptionPayments })
    }
}