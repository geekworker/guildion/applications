import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { UserAttributes, User, Users } from './User';
import { Member, MemberAttributes, Members } from './Member';
import { InviteAttributes, Invites } from './Invite';
import { RoleAttributes, Roles } from './Role';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { CountryCode } from '@guildion/core/src/constants/CountryCode';
import { GuildCategories, GuildCategoryAttributes } from './GuildCategory';
import { File, FileAttributes, Files } from './File';
import { GuildActivity, GuildActivityAttributes } from './GuildActivity';
import { RoomAttributes, Rooms } from './Room';
import { SocialLinkAttributes, SocialLinks } from './SocialLink';

export const GuildStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    BANNED: 'BANNED',
    LOCKED: 'LOCKED',
    DELETING: 'DELETING',
    DELETED: 'DELETED',
} as const;

export type GuildStatus = typeof GuildStatus[keyof typeof GuildStatus];

export const GuildType = {
    PUBLIC: 'PUBLIC',
    PRIVATE: 'PRIVATE',
    INDIVIDUAL: 'INDIVIDUAL',
    OFFICIAL: 'OFFICIAL',
} as const;

export type GuildType = typeof GuildType[keyof typeof GuildType];

export interface GuildRecordAttributes {
    storage?: File,
    categories?: GuildCategories,
    profile?: File,
    owner?: User,
    ownerMember?: Member,
    currentMember?: Member,
    members?: Members,
    invites?: Invites,
    rooms?: Rooms,
    roles?: Roles,
    blocks?: Users,
    activity?: GuildActivity,
    socialLinks?: SocialLinks,
}

export interface GuildAttributes {
    id?: string,
    profileId?: string,
    ownerMemberId?: string,
    ownerId?: string,
    status: GuildStatus,
    type: GuildType,
    displayName: string,
    description: string,
    guildname: string,
    languageCode: LanguageCode,
    countryCode: CountryCode,
    index?: number,
    maxContentLength?: number,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    storage?: FileAttributes,
    categories?: GuildCategoryAttributes[],
    profile?: FileAttributes,
    owner?: UserAttributes,
    ownerMember?: MemberAttributes,
    currentMember?: MemberAttributes,
    members?: MemberAttributes[],
    invites?: InviteAttributes[],
    rooms?: RoomAttributes[],
    roles?: RoleAttributes[],
    blocks?: UserAttributes[],
    activity?: GuildActivityAttributes,
    socialLinks?: SocialLinkAttributes[],
}

export class Guild extends Record<GuildAttributes>({
    id: undefined,
    profileId: undefined,
    ownerMemberId: undefined,
    ownerId: undefined,
    displayName: '',
    description: '',
    guildname: '',
    languageCode: LanguageCode.English,
    countryCode: CountryCode.JAPAN,
    status: GuildStatus.NEW,
    type: GuildType.PRIVATE,
    index: undefined,
    maxContentLength: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    categories: undefined,
    profile: Files.getSeed().generateGuildProfile(),
    storage: undefined,
    owner: undefined,
    ownerMember: undefined,
    currentMember: undefined,
    members: undefined,
    invites: undefined,
    roles: undefined,
    rooms: undefined,
    blocks: undefined,
    activity: undefined,
    socialLinks: [],
}) {
    public static displayNameMaxLimit: number = 50;
    public static displayNameMinLimit: number = 1;
    public static activeIntervalMinHour: number = 0.5;
    public static get defaultProfiles(): File[] {
        return Files.getSeed().guildProfiles;
    };
    public readonly records: GuildRecordAttributes;

    constructor(args?: Partial<GuildAttributes>, records?: GuildRecordAttributes) {
        super({
            ...args,
            categories: records?.categories?.toJSON() ?? args?.categories,
            profile: records?.profile?.toJSON() ?? args?.profile,
            storage: records?.storage?.toJSON() ?? args?.storage,
            owner: records?.owner?.toJSON() ?? args?.owner,
            ownerMember: records?.ownerMember?.toJSON() ?? args?.ownerMember,
            currentMember: records?.currentMember?.toJSON() ?? args?.currentMember,
            members: records?.members?.toJSON() ?? args?.members,
            invites: records?.invites?.toJSON() ?? args?.invites,
            roles: records?.roles?.toJSON() ?? args?.roles,
            rooms: records?.rooms?.toJSON() ?? args?.rooms,
            blocks: records?.blocks?.toJSON() ?? args?.blocks,
            activity: records?.activity?.toJSON() ?? args?.activity,
            socialLinks: records?.socialLinks?.toJSON() ?? args?.socialLinks,
        });
        this.records = {};
        this.records.categories = records?.categories ? records.categories : args?.categories ? new GuildCategories(args?.categories) : undefined;
        this.records.profile = records?.profile ? records.profile : args?.profile ? new File(args?.profile) : undefined;
        this.records.storage = records?.storage ? records.storage : args?.storage ? new File(args?.storage) : undefined;
        this.records.owner = records?.owner ? records.owner : args?.owner ? new User(args?.owner) : undefined;
        this.records.ownerMember = records?.ownerMember ? records.ownerMember : args?.ownerMember ? new Member(args?.ownerMember) : undefined;
        this.records.currentMember = records?.currentMember ? records.currentMember : args?.currentMember ? new Member(args?.currentMember) : undefined;
        this.records.members = records?.members ? records.members : args?.members ? new Members(args?.members) : undefined;
        this.records.invites = records?.invites ? records.invites : args?.invites ? new Invites(args?.invites) : undefined;
        this.records.roles = records?.roles ? records.roles : args?.roles ? new Roles(args?.roles) : undefined;
        this.records.rooms = records?.rooms ? records.rooms : args?.rooms ? new Rooms(args?.rooms) : undefined;
        this.records.blocks = records?.blocks ? records.blocks : args?.blocks ? new Users(args?.blocks) : undefined;
        this.records.activity = records?.activity ? records.activity : args?.activity ? new GuildActivity(args?.activity) : undefined;
        this.records.socialLinks = records?.socialLinks ? records.socialLinks : args?.socialLinks ? new SocialLinks(args?.socialLinks) : undefined;
    }

    public setRecord(key: keyof GuildRecordAttributes, record?: GuildRecordAttributes[keyof GuildRecordAttributes]): Guild {
        return new Guild(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<GuildAttributes, keyof GuildRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Guilds extends Records<Guild> {
    constructor(array: Array<GuildAttributes | Guild>) {
        super(array, { Record: Guild, Records: Guilds })
    }
}