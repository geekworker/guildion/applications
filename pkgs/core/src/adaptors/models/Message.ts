import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { FileAttributes, Files } from './File';
import { Member, MemberAttributes } from './Member';
import { ReactionListAttributes, ReactionLists } from './ReactionList';
import { Room, RoomAttributes } from './Room';
import { TimeZoneID } from '@guildion/core/src/constants/TimeZone';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { Localizer } from '@guildion/core/src/locales';
import { format  } from 'date-fns-tz';
import { parseFromTimeZone } from 'date-fns-timezone';
import { isToday, isYesterday, formatDistanceToNow, nextDay, isAfter, isEqual } from 'date-fns';
import { OGPAttributes, OGPs } from './OGP';

export const MessageStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    ARCHIVED: 'ARCHIVED',
    DELETED: 'DELETED',

    LOADING: 'LOADING',
    FAILED: 'FAILED',
} as const;

export type MessageStatus = typeof MessageStatus[keyof typeof MessageStatus];

export const MessageReadStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    NOTIFIED: 'NOTIFIED',
    CHECKED: 'CHECKED',
} as const;

export type MessageReadStatus = typeof MessageReadStatus[keyof typeof MessageReadStatus];

export interface MessageRecordAttributes {
    sender?: Member,
    room?: Room,
    parent?: Message,
    children?: Messages,
    files?: Files,
    reactions?: ReactionLists,
    ogps?: OGPs,
}

export interface MessageAttributes {
    id?: string,
    senderId?: string,
    roomId?: string,
    parentId?: string,
    status: MessageStatus,
    readStatus?: MessageReadStatus,
    body: string,
    location: string,
    commentsCount: number,
    isCommentable: boolean,
    archivedAt?: string | null,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    sender?: MemberAttributes,
    room?: RoomAttributes,
    parent?: MessageAttributes,
    children?: MessageAttributes[],
    files?: FileAttributes[],
    reactions?: ReactionListAttributes[],
    ogps?: OGPAttributes[],
}

export class Message extends Record<MessageAttributes>({
    id: undefined,
    senderId: undefined,
    roomId: undefined,
    parentId: undefined,
    status: MessageStatus.NEW,
    readStatus: undefined,
    body: '',
    location: '',
    commentsCount: 0,
    isCommentable: true,
    archivedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    sender: undefined,
    room: undefined,
    parent: undefined,
    children: undefined,
    files: undefined,
    reactions: undefined,
    ogps: undefined,
}) {
    public readonly records: MessageRecordAttributes;
    public static membersMentionRegExp = /@(room)/i;
    public static memberMentionRegExp = /\[(@[^:]+):([^\]]+)\]/i;
    public static roomMentionRegExp = /\[(![^:]+):([^\]]+)\]/i;
    public static codeBlockRegExp = /`{3}(.*)`{3}/i;
    public static boldRegExp = /\*\*(.*)\*\*/i;
    public static strongRegExp = /`{1}(.*)`{1}/i;
    public static durationRegExp = /([0-9]{1,2}:){1,2}[0-9][0-9]/;
    public static invalidRegExp = /^(\n|\s)+$/;

    constructor(args?: Partial<MessageAttributes>, records?: MessageRecordAttributes) {
        super({
            ...args,
            sender: records?.sender?.toJSON() ?? args?.sender,
            room: records?.room?.toJSON() ?? args?.room,
            parent: records?.parent?.toJSON() ?? args?.parent,
            children: records?.children?.toJSON() ?? args?.children,
            files: records?.files?.toJSON() ?? args?.files,
            reactions: records?.reactions?.toJSON() ?? args?.reactions,
            ogps: records?.ogps?.toJSON() ?? args?.ogps,
        });
        this.records = {};
        this.records.sender = records?.sender ? records.sender : args?.sender ? new Member(args?.sender) : undefined;
        this.records.room = records?.room ? records.room : args?.room ? new Room(args?.room) : undefined;
        this.records.parent = records?.parent ? records.parent : args?.parent ? new Message(args?.parent) : undefined;
        this.records.children = records?.children ? records.children : args?.children ? new Messages(args?.children) : undefined;
        this.records.files = records?.files ? records.files : args?.files ? new Files(args?.files) : undefined;
        this.records.reactions = records?.reactions ? records.reactions : args?.reactions ? new ReactionLists(args?.reactions) : undefined;
        this.records.ogps = records?.ogps ? records.ogps : args?.ogps ? new OGPs(args?.ogps) : undefined;
    }

    public setRecord(key: keyof MessageRecordAttributes, record?: MessageRecordAttributes[keyof MessageRecordAttributes]): Message {
        return new Message(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<MessageAttributes, keyof MessageRecordAttributes> {
        return { ...this.toJSON() };
    }
    public checkBodyValid(): boolean {
        return this.body.length > 0 && !Message.invalidRegExp.test(this.body);
    }
    public getCreatedHM(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "hh:mm", { timeZone: timezone }) : undefined;
    }
    public getCreatedYMD(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "yyyy/MM/dd", { timeZone: timezone }) : undefined;
    }
    public checkTomorrowDate(before: Message, timezone: TimeZoneID): boolean {
        const thisCreatedAt = this.getCreatedAt();
        const beforeCreatedAt = before.getCreatedAt();
        if (!thisCreatedAt || !beforeCreatedAt || !isAfter(thisCreatedAt, beforeCreatedAt)) return false;
        const thisCreatedAtTz = parseFromTimeZone(thisCreatedAt.toISOString(), { timeZone: timezone });
        const beforeCreatedAtTz = parseFromTimeZone(beforeCreatedAt.toISOString(), { timeZone: timezone });
        const targetAt = new Date(beforeCreatedAtTz.getFullYear(), beforeCreatedAtTz.getMonth(), beforeCreatedAtTz.getDate() + 1);
        const startAt = new Date(thisCreatedAtTz.getFullYear(), thisCreatedAtTz.getMonth(), thisCreatedAtTz.getDate());
        return isAfter(startAt, targetAt) || isEqual(startAt, targetAt);
    }
    public getCreatedDistanceNow(lang: LanguageCode): string | undefined {
        const result = this.get('createdAt');
        const localizer = new Localizer(lang);
        return result ? formatDistanceToNow(new Date(result), { locale: localizer.localeDateFns }) : undefined;
    }
    public getCreatedDateSection(timezone: TimeZoneID, lang: LanguageCode): string | undefined {
        const result = this.get('createdAt')
        const localizer = new Localizer(lang);
        return result ?
            isToday(new Date(result)) ?
            localizer.dictionary.g.date.today :
            isYesterday(new Date(result)) ?
            localizer.dictionary.g.date.yesterday :
            format(new Date(result), "yyyy/MM/dd", { timeZone: timezone }) :
            undefined;
    }
    public getArchivedAt(): Date | undefined {
        const result = this.get('archivedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Messages extends Records<Message> {
    constructor(array: Array<MessageAttributes | Message>) {
        super(array, { Record: Message, Records: Messages })
    }
}