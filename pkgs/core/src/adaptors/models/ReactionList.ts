import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { File, FileAttributes } from './File';
import { MemberAttributes, Members } from './Member';

export interface ReactionListRecordAttributes {
    file?: File,
    senders?: Members,
}

export interface ReactionListAttributes {
    file?: FileAttributes,
    length: number,
    senders?: MemberAttributes[],
}

export class ReactionList extends Record<ReactionListAttributes>({
    file: new File().toJSON(),
    length: 0,
    senders: [],
}) {
    public readonly records: ReactionListRecordAttributes;

    constructor(args?: Partial<ReactionListAttributes>, records?: ReactionListRecordAttributes) {
        super({
            ...args,
            senders: records?.senders?.toJSON() ?? args?.senders,
            file: records?.file?.toJSON() ?? args?.file,
        });
        this.records = {
            senders: records?.senders ? records.senders : args?.senders ? new Members(args?.senders) : undefined,
            file: records?.file ? records.file : args?.file ? new File(args?.file) : undefined,
        };
    }

    public setRecord(key: keyof ReactionListRecordAttributes, record?: ReactionListRecordAttributes[keyof ReactionListRecordAttributes]): ReactionList {
        return new ReactionList(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<ReactionListAttributes, keyof ReactionListRecordAttributes> {
        return { ...this.toJSON() };
    }
}

export class ReactionLists extends Records<ReactionList> {
    constructor(array: Array<ReactionListAttributes | ReactionList>) {
        super(array, { Record: ReactionList, Records: ReactionLists })
    }
}