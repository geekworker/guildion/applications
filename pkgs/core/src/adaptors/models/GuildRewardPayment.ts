import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { RewardMethod } from '@guildion/core/src/constants/RewardMethod';
import { GuildReward, GuildRewardAttributes } from './GuildReward';

export const GuildRewardPaymentStatus = {
    NEW: 'NEW',
    NOT_YET_PAID: 'NOT_YET_PAID',
    PAID: 'PAID',
    CANCELED: 'CANCELED',
} as const;

export type GuildRewardPaymentStatus = typeof GuildRewardPaymentStatus[keyof typeof GuildRewardPaymentStatus];

export interface GuildRewardPaymentRecordAttributes {
    reward?: GuildReward,
}

export interface GuildRewardPaymentAttributes {
    id?: string,
    rewardId?: string,
    status: GuildRewardPaymentStatus,
    method: RewardMethod,
    providerKey: string,
    requestedAt?: string | null,
    payAt?: string | null,
    paidAt?: string | null,
    canceledAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    reward?: GuildRewardAttributes,
}

export class GuildRewardPayment extends Record<GuildRewardPaymentAttributes>({
    id: undefined,
    rewardId: undefined,
    status: GuildRewardPaymentStatus.NEW,
    method: RewardMethod.WISE,
    providerKey: '',
    requestedAt: undefined,
    payAt: undefined,
    paidAt: undefined,
    canceledAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    reward: undefined,
}) {
    public readonly records: GuildRewardPaymentRecordAttributes;

    constructor(args?: Partial<GuildRewardPaymentAttributes>, records?: GuildRewardPaymentRecordAttributes) {
        super({
            ...args,
            reward: records?.reward?.toJSON() ?? args?.reward,
        });
        this.records = {};
        this.records.reward = records?.reward ? records.reward : args?.reward ? new GuildReward(args?.reward) : undefined;
    }

    public setRecord(key: keyof GuildRewardPaymentRecordAttributes, record?: GuildRewardPaymentRecordAttributes[keyof GuildRewardPaymentRecordAttributes]): GuildRewardPayment {
        return new GuildRewardPayment(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<GuildRewardPaymentAttributes, keyof GuildRewardPaymentRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getRequestedAt(): Date | undefined {
        const result = this.get('requestedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getPayAt(): Date | undefined {
        const result = this.get('payAt')
        return result ? (new Date(result)) : undefined;
    }
    public getPaidAt(): Date | undefined {
        const result = this.get('paidAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCanceledAt(): Date | undefined {
        const result = this.get('canceledAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class GuildRewardPayments extends Records<GuildRewardPayment> {
    constructor(array: Array<GuildRewardPaymentAttributes | GuildRewardPayment>) {
        super(array, { Record: GuildRewardPayment, Records: GuildRewardPayments })
    }
}