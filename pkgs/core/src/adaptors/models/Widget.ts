import { Record } from 'immutable';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { Records } from '@guildion/core/src/extension/Records';

export interface WidgetAttributes {
    id?: string,
    widgetname: string,
    jaName: string,
    enName: string,
    jaDescription: string,
    enDescription: string,
    createdAt?: string,
    updatedAt?: string,
}

export class Widget extends Record<WidgetAttributes>({
    id: undefined,
    widgetname: '',
    jaName: '',
    enName: '',
    jaDescription: '',
    enDescription: '',
    createdAt: undefined,
    updatedAt: undefined,
}) {
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    
    public getName(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaName;
        default:
        case LanguageCode.English: return this.enName;
        }
    }

    public getDescription(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaDescription;
        default:
        case LanguageCode.English: return this.enDescription;
        }
    }
}

export class Widgets extends Records<Widget> {
    constructor(array: Array<WidgetAttributes | Widget>) {
        super(array, { Record: Widget, Records: Widgets })
    }
}