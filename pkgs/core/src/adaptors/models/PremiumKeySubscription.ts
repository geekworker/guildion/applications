import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { PaymentMethod } from '@guildion/core/src/constants/PaymentMethod';
import { Member, MemberAttributes } from './Member';
import { PremiumKey, PremiumKeyAttributes } from './PremiumKey';
import { PremiumKeySubscriptionPaymentAttributes, PremiumKeySubscriptionPayments } from './PremiumKeySubscriptionPayment';

export const PremiumKeySubscriptionStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    CONFIRMED: 'CONFIRMED',
    ACTIVE: 'ACTIVE',
    EXPIRED: 'EXPIRED',
    REJECTED: 'REJECTED',
    CANCELING: 'CANCELING',
    CANCELED: 'CANCELED',
} as const;

export type PremiumKeySubscriptionStatus = typeof PremiumKeySubscriptionStatus[keyof typeof PremiumKeySubscriptionStatus];

export interface PremiumKeySubscriptionRecordAttributes {
    member?: Member,
    premiumKey?: PremiumKey,
    payments?: PremiumKeySubscriptionPayments,
}

export interface PremiumKeySubscriptionAttributes {
    id?: string,
    memberId?: string,
    premiumKeyId?: string,
    status: PremiumKeySubscriptionStatus,
    method: PaymentMethod,
    providerKey: string,
    confirmedAt?: string | null,
    activedAt?: string | null,
    expiredAt?: string | null,
    canceledAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    member?: MemberAttributes,
    premiumKey?: PremiumKeyAttributes,
    payments?: PremiumKeySubscriptionPaymentAttributes[],
}

export class PremiumKeySubscription extends Record<PremiumKeySubscriptionAttributes>({
    id: undefined,
    memberId: undefined,
    premiumKeyId: undefined,
    status: PremiumKeySubscriptionStatus.NEW,
    method: PaymentMethod.OTHER,
    providerKey: '',
    confirmedAt: undefined,
    activedAt: undefined,
    expiredAt: undefined,
    canceledAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    member: undefined,
    premiumKey: undefined,
    payments: undefined,
}) {
    public readonly records: PremiumKeySubscriptionRecordAttributes;

    constructor(args?: Partial<PremiumKeySubscriptionAttributes>, records?: PremiumKeySubscriptionRecordAttributes) {
        super({
            ...args,
            member: records?.member?.toJSON() ?? args?.member,
            premiumKey: records?.premiumKey?.toJSON() ?? args?.premiumKey,
            payments: records?.payments?.toJSON() ?? args?.payments,
        });
        this.records = {};
        this.records.member = records?.member ? records.member : args?.member ? new Member(args?.member) : undefined;
        this.records.premiumKey = records?.premiumKey ? records.premiumKey : args?.premiumKey ? new PremiumKey(args?.premiumKey) : undefined;
        this.records.payments = records?.payments ? records.payments : args?.payments ? new PremiumKeySubscriptionPayments(args?.payments) : undefined;
    }

    public setRecord(key: keyof PremiumKeySubscriptionRecordAttributes, record?: PremiumKeySubscriptionRecordAttributes[keyof PremiumKeySubscriptionRecordAttributes]): PremiumKeySubscription {
        return new PremiumKeySubscription(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<PremiumKeySubscriptionAttributes, keyof PremiumKeySubscriptionRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getConfirmedAt(): Date | undefined {
        const result = this.get('confirmedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getActivedAt(): Date | undefined {
        const result = this.get('activedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getExpiredAt(): Date | undefined {
        const result = this.get('expiredAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCanceledAt(): Date | undefined {
        const result = this.get('canceledAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class PremiumKeySubscriptions extends Records<PremiumKeySubscription> {
    constructor(array: Array<PremiumKeySubscriptionAttributes | PremiumKeySubscription>) {
        super(array, { Record: PremiumKeySubscription, Records: PremiumKeySubscriptions })
    }
}