import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { Guild, GuildAttributes } from './Guild';
import { Member, MemberAttributes } from './Member';
import { Room, RoomAttributes } from './Room';
import { GET_CURRENT_API_URL_STRING } from '../../constants/ApplicationConfig';
import { addDays } from 'date-fns';

export const InviteStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    OPEN: 'OPEN',
    EXCEEDED: 'EXCEEDED',
    EXPIRED: 'EXPIRED',
} as const;

export type InviteStatus = typeof InviteStatus[keyof typeof InviteStatus];

export const InviteType = {
    guild: 'guild',
    room: 'room',
    other: 'other',
} as const;

export type InviteType = typeof InviteType[keyof typeof InviteType];

export interface InviteRecordAttributes {
    member?: Member,
    guild?: Guild,
    room?: Room,
}

export interface InviteAttributes {
    id?: string,
    memberId?: string,
    guildId?: string,
    roomId?: string | null,
    status: InviteStatus,
    type: InviteType,
    maxMembersCount?: number | null,
    currentMembersCount: number | null,
    expiredAt?: string,
    createdAt?: string,
    updatedAt?: string,
    member?: MemberAttributes,
    guild?: GuildAttributes,
    room?: RoomAttributes,
}

export class Invite extends Record<InviteAttributes>({
    id: undefined,
    memberId: undefined,
    guildId: undefined,
    roomId: undefined,
    maxMembersCount: undefined,
    currentMembersCount: 0,
    status: InviteStatus.NEW,
    type: InviteType.other,
    expiredAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    member: undefined,
    guild: undefined,
    room: undefined,
}) {
    public readonly records: InviteRecordAttributes;

    constructor(args?: Partial<InviteAttributes>, records?: InviteRecordAttributes) {
        super({
            ...args,
            member: records?.member?.toJSON() ?? args?.member,
            guild: records?.guild?.toJSON() ?? args?.guild,
            room: records?.room?.toJSON() ?? args?.room,
        });
        this.records = {};
        this.records.member = records?.member ? records.member : args?.member ? new Member(args?.member) : undefined;
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args?.guild) : undefined;
        this.records.room = records?.room ? records.room : args?.room ? new Room(args?.room) : undefined;
    }

    public setRecord(key: keyof InviteRecordAttributes, record?: InviteRecordAttributes[keyof InviteRecordAttributes]): Invite {
        return new Invite(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<InviteAttributes, keyof InviteRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getExpiredAt(): Date | undefined {
        const result = this.get('expiredAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public get urlstring(): string {
        return GET_CURRENT_API_URL_STRING() + '/invites/' + (this.id ?? '');
    }
    public static getExpiredByDay(day: number): Date {
        return addDays(new Date(), day);
    } 
    public setExpiredByDay(day: number): Invite {
        if (day <= 0) return this;
        return this.set('expiredAt', Invite.getExpiredByDay(day).toISOString());
    }
}

export class Invites extends Records<Invite> {
    constructor(array: Array<InviteAttributes | Invite>) {
        super(array, { Record: Invite, Records: Invites })
    }
}