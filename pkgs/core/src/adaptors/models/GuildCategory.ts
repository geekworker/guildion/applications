import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { GuildAttributes, Guilds } from './Guild';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';


export interface GuildCategoryRecordAttributes {
    guilds?: Guilds,
}

export interface GuildCategoryAttributes {
    id?: string,
    jaName: string,
    enName: string,
    jaGroupname: string,
    enGroupname: string,
    slug: string,
    index: number,
    hidden: boolean,
    createdAt?: string,
    updatedAt?: string,
    guilds?: GuildAttributes[],
}

export class GuildCategory extends Record<GuildCategoryAttributes>({
    id: undefined,
    jaName: '',
    enName: '',
    jaGroupname: '',
    enGroupname: '',
    slug: '',
    index: 0,
    hidden: false,
    createdAt: undefined,
    updatedAt: undefined,
    guilds: undefined,
}) {
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public readonly records: GuildCategoryRecordAttributes;

    constructor(args?: Partial<GuildCategoryAttributes>, records?: GuildCategoryRecordAttributes) {
        super({
            ...args,
            guilds: records?.guilds?.toJSON() ?? args?.guilds,
        });
        this.records = {};
        this.records.guilds = records?.guilds ? records.guilds : args?.guilds ? new Guilds(args.guilds ?? []) : undefined;
    }

    public setRecord(key: keyof GuildCategoryRecordAttributes, record?: GuildCategoryRecordAttributes[keyof GuildCategoryRecordAttributes]): GuildCategory {
        return new GuildCategory(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<GuildCategoryAttributes, keyof GuildCategoryRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getName(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaName;
        default:
        case LanguageCode.English: return this.enName;
        }
    }

    public getGroupname(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaGroupname;
        default:
        case LanguageCode.English: return this.enGroupname;
        }
    }
}

export class GuildCategories extends Records<GuildCategory> {
    constructor(array: Array<GuildCategoryAttributes | GuildCategory>) {
        super(array, { Record: GuildCategory, Records: GuildCategories })
    }

    static getSeed = (): GuildCategories => new GuildCategories([
        new GuildCategory({
            jaName: 'その他',
            jaGroupname: 'その他',
            enName: 'Others',
            enGroupname: 'Others',
            slug: 'others',
            index: 1,
        }),
    ]);
}