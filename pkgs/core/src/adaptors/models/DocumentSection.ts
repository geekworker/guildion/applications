import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { DocumentGroup, DocumentGroupAttributes } from './DocumentGroup';
import { DocumentAttributes, Documents } from './Document';

export interface DocumentSectionRecordAttributes {
    section?: DocumentSection,
    sections?: DocumentSections,
    group?: DocumentGroup,
    documents?: Documents,
}

export interface DocumentSectionAttributes {
    id?: string,
    groupId?: string,
    sectionId?: string | null,
    slug: string,
    jaName: string,
    enName: string,
    index: number,
    status: DocumentSectionStatus,
    type: DocumentSectionType,
    publishedAt?: string,
    deletedAt?: string,
    createdAt?: string,
    updatedAt?: string,
    section?: DocumentSectionAttributes,
    sections?: DocumentSectionAttributes[],
    group?: DocumentGroupAttributes,
    documents?: DocumentAttributes[],
}

export const DocumentSectionStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',

    LOADING: 'LOADING,'
} as const;

export type DocumentSectionStatus = typeof DocumentSectionStatus[keyof typeof DocumentSectionStatus];

export const DocumentSectionType = {
    DEFAULT: 'default',
    ALL_IN_ONE: 'all_in_one',
} as const;

export type DocumentSectionType = typeof DocumentSectionType[keyof typeof DocumentSectionType];

export class DocumentSection extends Record<DocumentSectionAttributes>({
    id: undefined,
    groupId: undefined,
    sectionId: undefined,
    slug: '',
    enName: '',
    jaName: '',
    index: 0,
    status: DocumentSectionStatus.NEW,
    type: DocumentSectionType.DEFAULT,
    publishedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    section: undefined,
    sections: undefined,
    group: undefined,
    documents: undefined,
}) {
    public readonly records: DocumentSectionRecordAttributes;

    constructor(args?: Partial<DocumentSectionAttributes>, records?: DocumentSectionRecordAttributes) {
        super({
            ...args,
            section: records?.section?.toJSON() ?? args?.section,
            sections: records?.sections?.toJSON() ?? args?.sections,
            group: records?.group?.toJSON() ?? args?.group,
            documents: records?.documents?.toJSON() ?? args?.documents,
        });
        this.records = {};
        this.records.section = records?.section ? records.section : args?.section ? new DocumentSection(args?.section) : undefined;
        this.records.sections = records?.sections ? records.sections : args?.sections ? new DocumentSections(args?.sections) : undefined;
        this.records.group = records?.group ? records.group : args?.group ? new DocumentGroup(args?.group) : undefined;
        this.records.documents = records?.documents ? records.documents : args?.documents ? new Documents(args?.documents) : undefined;
    }

    public setRecord(key: keyof DocumentSectionRecordAttributes, record?: DocumentSectionRecordAttributes[keyof DocumentSectionRecordAttributes]): DocumentSection {
        return new DocumentSection(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<DocumentSectionAttributes, keyof DocumentSectionRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getPublishedAt(): Date | undefined {
        const result = this.get('publishedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getName(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaName;
        default:
        case LanguageCode.English: return this.enName;
        }
    }
}

export class DocumentSections extends Records<DocumentSection> {
    constructor(array: Array<DocumentSectionAttributes | DocumentSection>) {
        super(array, { Record: DocumentSection, Records: DocumentSections })
    }
}