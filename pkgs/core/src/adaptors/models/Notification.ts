import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { User, UserAttributes } from './User';
import { RoomLog, RoomLogAttributes } from './RoomLog';

export const NotificationType = {
    Welcome: 'welcome',
    RoomLog: 'room_log',
    Other: 'other',
} as const;

export type NotificationType = typeof NotificationType[keyof typeof NotificationType];

export const NotificationStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    NOTIFIED: 'NOTIFIED',
    CHECKED: 'CHECKED',
} as const;

export type NotificationStatus = typeof NotificationStatus[keyof typeof NotificationStatus];

export interface NotificationRecordAttributes {
    receiver?: User,
    roomLog?: RoomLog,
}

export interface NotificationAttributes {
    id?: string,
    receiverId?: string,
    roomLogId?: string,
    type: NotificationType,
    status: NotificationStatus,
    createdAt?: string,
    updatedAt?: string,
    receiver?: UserAttributes,
    roomLog?: RoomLogAttributes,
}

export class Notification extends Record<NotificationAttributes>({
    id: undefined,
    receiverId: undefined,
    roomLogId: undefined,
    type: NotificationType.Other,
    status: NotificationStatus.NEW,
    createdAt: undefined,
    updatedAt: undefined,
    receiver: undefined,
    roomLog: undefined,
}) {
    public readonly records: NotificationRecordAttributes;

    constructor(args?: Partial<NotificationAttributes>, records?: NotificationRecordAttributes) {
        super({
            ...args,
            receiver: records?.receiver?.toJSON() ?? args?.receiver,
            roomLog: records?.roomLog?.toJSON() ?? args?.roomLog,
        });
        this.records = {};
        this.records.receiver = records?.receiver ? records.receiver : args?.receiver ? new User(args.receiver) : undefined;
        this.records.roomLog = records?.roomLog ? records.roomLog : args?.roomLog ? new RoomLog(args.roomLog) : undefined;
    }

    public setRecord(key: keyof NotificationRecordAttributes, record?: NotificationRecordAttributes[keyof NotificationRecordAttributes]): Notification {
        return new Notification(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<NotificationAttributes, keyof NotificationRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Notifications extends Records<Notification> {
    constructor(array: Array<NotificationAttributes | Notification>) {
        super(array, { Record: Notification, Records: Notifications })
    }
}