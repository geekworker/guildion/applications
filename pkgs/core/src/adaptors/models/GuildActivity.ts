import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { MemberAttributes, Members } from './Member';
import { Guild, GuildAttributes } from './Guild';
import { TimeZone } from '@guildion/core/src/constants/TimeZone';
import { format } from 'date-fns-tz'

export interface GuildActivityRecordAttributes {
    guild?: Guild,
    connectingMembers?: Members,
}

export interface GuildActivityAttributes {
    id?: string,
    guildId?: string,
    connectsCount: number,
    roomsCount: number,
    publicRoomsCount: number,
    postsCount: number,
    publicPostsCount: number,
    membersCount: number,
    contentLength: number,
    createdAt?: string,
    updatedAt?: string,
    guild?: GuildAttributes,
    connectingMembers?: MemberAttributes[],
    notificationsCount?: number,
}

export class GuildActivity extends Record<GuildActivityAttributes>({
    id: undefined,
    guildId: undefined,
    connectsCount: 0,
    roomsCount: 0,
    publicRoomsCount: 0,
    postsCount: 0,
    publicPostsCount: 0,
    membersCount: 0,
    contentLength: 0,
    createdAt: undefined,
    updatedAt: undefined,
    guild: undefined,
    connectingMembers: undefined,
    notificationsCount: undefined,
}) {
    public static connectingMembersMaxLimit: number = 4;
    public readonly records: GuildActivityRecordAttributes;

    constructor(args?: Partial<GuildActivityAttributes>, records?: GuildActivityRecordAttributes) {
        super({
            ...args,
            guild: records?.guild?.toJSON() ?? args?.guild,
            connectingMembers: records?.connectingMembers?.toJSON() ?? args?.connectingMembers,
        });
        this.records = {};
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args.guild) : undefined;
        this.records.connectingMembers = records?.connectingMembers ? records.connectingMembers : args?.connectingMembers ? new Members(args.connectingMembers) : undefined;
    }

    public setRecord(key: keyof GuildActivityRecordAttributes, record?: GuildActivityRecordAttributes[keyof GuildActivityRecordAttributes]): GuildActivity {
        return new GuildActivity(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<GuildActivityAttributes, keyof GuildActivityRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedHM(timezone: TimeZone): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "hh:mm", { timeZone: timezone.id }) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class GuildActivities extends Records<GuildActivity> {
    constructor(array: Array<GuildActivityAttributes | GuildActivity>) {
        super(array, { Record: GuildActivity, Records: GuildActivities })
    }
}