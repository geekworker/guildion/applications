import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { BlogCategory, BlogCategoryAttributes } from './BlogCategory';
import { TimeZone } from '@guildion/core/src/constants/TimeZone';
import { format } from 'date-fns-tz'

export interface BlogRecordAttributes {
    category?: BlogCategory,
    prev?: Blog,
    next?: Blog,
}

export interface BlogAttributes {
    id?: string,
    jaThumbnailURL?: string,
    enThumbnailURL?: string,
    categoryId?: string,
    slug: string,
    jaTitle: string,
    enTitle: string,
    jaDescription: string,
    enDescription: string,
    jaData: string,
    enData: string,
    status: BlogStatus,
    type: BlogType,
    dataType: BlogDataType,
    index: number,
    publishedAt?: string,
    deletedAt?: string,
    createdAt?: string,
    updatedAt?: string,
    category?: BlogCategoryAttributes,
    prev?: BlogAttributes,
    next?: BlogAttributes,
}

export const BlogStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',

    LOADING: 'LOADING,'
} as const;

export type BlogStatus = typeof BlogStatus[keyof typeof BlogStatus];

export const BlogType = {
    DEFAULT: 'default',
} as const;

export type BlogType = typeof BlogType[keyof typeof BlogType];

export const BlogDataType = {
    HTML: 'html',
    OTHER: 'other',
} as const;

export type BlogDataType = typeof BlogDataType[keyof typeof BlogDataType];

export class Blog extends Record<BlogAttributes>({
    id: undefined,
    jaThumbnailURL: undefined,
    enThumbnailURL: undefined,
    categoryId: undefined,
    slug: '',
    jaTitle: '',
    enTitle: '',
    jaDescription: '',
    enDescription: '',
    jaData: '',
    enData: '',
    status: BlogStatus.NEW,
    type: BlogType.DEFAULT,
    dataType: BlogDataType.OTHER,
    index: 0,
    publishedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    category: undefined,
    prev: undefined,
    next: undefined,
}) {
    public readonly records: BlogRecordAttributes;

    constructor(args?: Partial<BlogAttributes>, records?: BlogRecordAttributes) {
        super({
            ...args,
            category: records?.category?.toJSON() ?? args?.category,
            prev: records?.prev?.toJSON() ?? args?.prev,
            next: records?.next?.toJSON() ?? args?.next,
        });
        this.records = {};
        this.records.category = records?.category ? records.category : args?.category ? new BlogCategory(args?.category) : undefined;
        this.records.prev = records?.prev ? records.prev : args?.prev ? new Blog(args?.prev) : undefined;
        this.records.next = records?.next ? records.next : args?.next ? new Blog(args?.next) : undefined;
    }

    public setRecord(key: keyof BlogRecordAttributes, record?: BlogRecordAttributes[keyof BlogRecordAttributes]): Blog {
        return new Blog(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<BlogAttributes, keyof BlogRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getPublishedAt(): Date | undefined {
        const result = this.get('publishedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getTitle(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaTitle;
        default:
        case LanguageCode.English: return this.enTitle;
        }
    }
    public getDescription(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaDescription;
        default:
        case LanguageCode.English: return this.enDescription;
        }
    }
    public getThumbnailURL(lang: LanguageCode): string | undefined {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaThumbnailURL;
        default:
        case LanguageCode.English: return this.enThumbnailURL;
        }
    }
    public getData(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaData;
        default:
        case LanguageCode.English: return this.enData;
        }
    }
    public getCreatedFormatedAt(timezone: TimeZone): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "yyyy/MM/dd", { timeZone: timezone.id }) : undefined;
    }
    public getUpdatedFormatedAt(timezone: TimeZone): string | undefined {
        const result = this.get('updatedAt')
        return result ? format(new Date(result), "yyyy/MM/dd", { timeZone: timezone.id }) : undefined;
    }
}

export class Blogs extends Records<Blog> {
    constructor(array: Array<BlogAttributes | Blog>) {
        super(array, { Record: Blog, Records: Blogs })
    }
}