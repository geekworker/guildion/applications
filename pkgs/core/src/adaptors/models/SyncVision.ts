import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { Room, RoomAttributes } from './Room';
import { File, FileAttributes } from './File';

export const SyncVisionStatus = {
    NEW: 'NEW',
    STARTED: 'STARTED',
    PLAYING: 'PLAYING',
    PAUSED: 'PAUSED',
    FINISHED: 'FINISHED',
} as const;

export type SyncVisionStatus = typeof SyncVisionStatus[keyof typeof SyncVisionStatus];

export interface SyncVisionRecordAttributes {
    file?: File,
    room?: Room,
}

export interface SyncVisionAttributes {
    id?: string,
    fileId?: string,
    roomId?: string,
    status: SyncVisionStatus,
    offsetDurationMs: number,
    connectedAt?: string | null,
    disconnectedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    file?: FileAttributes,
    room?: RoomAttributes,
}

export class SyncVision extends Record<SyncVisionAttributes>({
    id: undefined,
    fileId: undefined,
    roomId: undefined,
    status: SyncVisionStatus.NEW,
    offsetDurationMs: 0,
    connectedAt: undefined,
    disconnectedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    file: undefined,
    room: undefined,
}) {
    public readonly records: SyncVisionRecordAttributes;

    constructor(args?: Partial<SyncVisionAttributes>, records?: SyncVisionRecordAttributes) {
        super({
            ...args,
            file: records?.file?.toJSON() ?? args?.file,
            room: records?.room?.toJSON() ?? args?.room,
        });
        this.records = {};
        this.records.file = records?.file ? records.file : args?.file ? new File(args?.file) : undefined;
        this.records.room = records?.room ? records.room : args?.room ? new Room(args?.room) : undefined;
    }

    public setRecord(key: keyof SyncVisionRecordAttributes, record?: SyncVisionRecordAttributes[keyof SyncVisionRecordAttributes]): SyncVision {
        return new SyncVision(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<SyncVisionAttributes, keyof SyncVisionRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getConnectedAt(): Date | undefined {
        const result = this.get('connectedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDisconnectedAt(): Date | undefined {
        const result = this.get('disconnectedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class SyncVisions extends Records<SyncVision> {
    constructor(array: Array<SyncVisionAttributes | SyncVision>) {
        super(array, { Record: SyncVision, Records: SyncVisions })
    }
}