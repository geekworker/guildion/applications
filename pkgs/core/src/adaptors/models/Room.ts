import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { Member, MemberAttributes, Members } from './Member';
import { WidgetAttributes, Widgets } from './Widget';
import { File, FileAttributes, Files } from './File';
import { InviteAttributes, Invites } from './Invite';
import { Role, RoleAttributes, Roles } from './Role';
import { RoomCategories, RoomCategoryAttributes } from './RoomCategory';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { CountryCode } from '@guildion/core/src/constants/CountryCode';
import { RoomActivity, RoomActivityAttributes } from './RoomActivity';
import { Guild, GuildAttributes } from './Guild';
import { SocialLinks, SocialLinkAttributes } from './SocialLink';

export const RoomStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    BANNED: 'BANNED',
    LOCKED: 'LOCKED',
    DELETING: 'DELETING',
    DELETED: 'DELETED',
} as const;

export type RoomStatus = typeof RoomStatus[keyof typeof RoomStatus];

export const RoomType = {
    PUBLIC: 'PUBLIC',
    PRIVATE: 'PRIVATE',
    DM: 'DM',
    INDIVIDUAL: 'INDIVIDUAL',
    OFFICIAL: 'OFFICIAL',
} as const;

export type RoomType = typeof RoomType[keyof typeof RoomType];

export interface RoomRecordAttributes {
    activity?: RoomActivity,
    storage?: File,
    profile?: File,
    categories?: RoomCategories,
    owner?: Member,
    guild?: Guild,
    members?: Members,
    ownerMember?: Member,
    currentMember?: Member,
    widgets?: Widgets,
    files?: Files,
    invites?: Invites,
    roles?: Roles,
    socialLinks?: SocialLinks,
}

export interface RoomAttributes {
    id?: string,
    profileId?: string,
    guildId?: string,
    ownerId?: string,
    displayName: string,
    description: string,
    roomname: string,
    languageCode: LanguageCode,
    countryCode: CountryCode,
    status: RoomStatus,
    type: RoomType,
    maxContentLength?: number,
    deletedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    activity?: RoomActivityAttributes,
    storage?: FileAttributes,
    profile?: FileAttributes,
    categories?: RoomCategoryAttributes[],
    guild?: GuildAttributes,
    owner?: MemberAttributes,
    members?: MemberAttributes[],
    ownerMember?: MemberAttributes,
    currentMember?: MemberAttributes,
    widgets?: WidgetAttributes[],
    files?: FileAttributes[],
    invites?: InviteAttributes[],
    roles?: RoleAttributes[],
    socialLinks?: SocialLinkAttributes[],
}

export class Room extends Record<RoomAttributes>({
    id: undefined,
    profileId: undefined,
    guildId: undefined,
    ownerId: undefined,
    displayName: '',
    description: '',
    roomname: '',
    languageCode: LanguageCode.English,
    countryCode: CountryCode.JAPAN,
    status: RoomStatus.NEW,
    type: RoomType.PRIVATE,
    maxContentLength: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    storage: undefined,
    activity: undefined,
    profile: Files.getSeed().generateRoomProfile(),
    categories: undefined,
    owner: undefined,
    members: undefined,
    ownerMember: undefined,
    currentMember: undefined,
    widgets: undefined,
    files: undefined,
    invites: undefined,
    roles: undefined,
    socialLinks: undefined,
}) {
    public static displayNameMaxLimit: number = 50;
    public static displayNameMinLimit: number = 1;
    public static get defaultProfiles(): File[] {
        return Files.getSeed().guildProfiles;
    };
    public readonly records: RoomRecordAttributes;

    constructor(args?: Partial<RoomAttributes>, records?: RoomRecordAttributes) {
        super({
            ...args,
            activity: records?.activity?.toJSON() ?? args?.activity,
            storage: records?.storage?.toJSON() ?? args?.storage,
            profile: records?.profile?.toJSON() ?? args?.profile,
            categories: records?.categories?.toJSON() ?? args?.categories,
            owner: records?.owner?.toJSON() ?? args?.owner,
            guild: records?.guild?.toJSON() ?? args?.guild,
            members: records?.members?.toJSON() ?? args?.members,
            ownerMember: records?.ownerMember?.toJSON() ?? args?.ownerMember,
            currentMember: records?.currentMember?.toJSON() ?? args?.currentMember,
            widgets: records?.widgets?.toJSON() ?? args?.widgets,
            files: records?.files?.toJSON() ?? args?.files,
            invites: records?.invites?.toJSON() ?? args?.invites,
            roles: records?.roles?.toJSON() ?? args?.roles,
            socialLinks: records?.socialLinks?.toJSON() ?? args?.socialLinks,
        });
        this.records = {};
        this.records.activity = records?.activity ? records.activity : args?.activity ? new RoomActivity(args?.activity) : undefined;
        this.records.storage = records?.storage ? records.storage : args?.storage ? new File(args?.storage) : undefined;
        this.records.profile = records?.profile ? records.profile : args?.profile ? new File(args?.profile) : undefined;
        this.records.categories = records?.categories ? records.categories : args?.categories ? new RoomCategories(args?.categories) : undefined;
        this.records.owner = records?.owner ? records.owner : args?.owner ? new Member(args?.owner) : undefined;
        this.records.guild = records?.guild ? records.guild : args?.guild ? new Guild(args?.guild) : undefined;
        this.records.members = records?.members ? records.members : args?.members ? new Members(args?.members) : undefined;
        this.records.ownerMember = records?.ownerMember ? records.ownerMember : args?.ownerMember ? new Member(args?.ownerMember) : undefined;
        this.records.currentMember = records?.currentMember ? records.currentMember : args?.currentMember ? new Member(args?.currentMember) : undefined;
        this.records.widgets = records?.widgets ? records.widgets : args?.widgets ? new Widgets(args?.widgets) : undefined;
        this.records.files = records?.files ? records.files : args?.files ? new Files(args?.files) : undefined;
        this.records.invites = records?.invites ? records.invites : args?.invites ? new Invites(args?.invites) : undefined;
        this.records.roles = records?.roles ? records.roles : args?.roles ? new Roles(args?.roles) : undefined;
        this.records.socialLinks = records?.socialLinks ? records.socialLinks : args?.socialLinks ? new SocialLinks(args.socialLinks) : undefined;
    }

    public setRecord(key: keyof RoomRecordAttributes, record?: RoomRecordAttributes[keyof RoomRecordAttributes]): Room {
        return new Room(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<RoomAttributes, keyof RoomRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class Rooms extends Records<Room> {
    constructor(array: Array<RoomAttributes | Room>) {
        super(array, { Record: Room, Records: Rooms })
    }
}