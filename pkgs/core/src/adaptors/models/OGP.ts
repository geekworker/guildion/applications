import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { TimeZoneID } from '@guildion/core/src/constants/TimeZone';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { Localizer } from '@guildion/core/src/locales';
import { format } from 'date-fns-tz'
import { isToday, isYesterday, formatDistanceToNow } from 'date-fns';
import { safeURL, extractHostname } from '@guildion/core/src/extension/URL';

export const OGPStatus = {
    NEW: 'NEW',
    FETCHING: 'FETCHING',
    FETCHED: 'FETCHED',
    DELETED: 'DELETED',
} as const;

export type OGPStatus = typeof OGPStatus[keyof typeof OGPStatus];

export interface OGPRecordAttributes {
}

export interface OGPAttributes {
    id?: string,
    status: OGPStatus,
    title: string,
    description: string,
    url: string,
    imageURL: string,
    createdAt?: string,
    updatedAt?: string,
}

export class OGP extends Record<OGPAttributes>({
    id: undefined,
    status: OGPStatus.NEW,
    title: '',
    description: '',
    url: '',
    imageURL: '',
    createdAt: undefined,
    updatedAt: undefined,
}) {
    public readonly records: OGPRecordAttributes;

    constructor(args?: Partial<OGPAttributes>, records?: OGPRecordAttributes) {
        super({
            ...args,
        });
        this.records = {};
    }

    public setRecord(key: keyof OGPRecordAttributes, record?: OGPRecordAttributes[keyof OGPRecordAttributes]): OGP {
        return new OGP(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<OGPAttributes, keyof OGPRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getURL(): URL | undefined {
        return safeURL(this.get('url'));
    }
    public getDomain(): string {
        return extractHostname(this.get('url'))
    }
    public getImageURL(): URL | undefined {
        return safeURL(this.get('imageURL'));
    }
    public getCreatedHM(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "hh:mm", { timeZone: timezone }) : undefined;
    }
    public getCreatedYMD(timezone: TimeZoneID): string | undefined {
        const result = this.get('createdAt')
        return result ? format(new Date(result), "yyyy/MM/dd", { timeZone: timezone }) : undefined;
    }
    public getCreatedDistanceNow(lang: LanguageCode): string | undefined {
        const result = this.get('createdAt');
        const localizer = new Localizer(lang);
        return result ? formatDistanceToNow(new Date(result), { locale: localizer.localeDateFns }) : undefined;
    }
    public getCreatedDateSection(timezone: TimeZoneID, lang: LanguageCode): string | undefined {
        const result = this.get('createdAt')
        const localizer = new Localizer(lang);
        return result ?
            isToday(new Date(result)) ?
            localizer.dictionary.g.date.today :
            isYesterday(new Date(result)) ?
            localizer.dictionary.g.date.yesterday :
            format(new Date(result), "yyyy/MM/dd", { timeZone: timezone }) :
            undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
}

export class OGPs extends Records<OGP> {
    constructor(array: Array<OGPAttributes | OGP>) {
        super(array, { Record: OGP, Records: OGPs })
    }
}