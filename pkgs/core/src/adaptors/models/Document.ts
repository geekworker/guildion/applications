import { Record } from 'immutable';
import { Records } from '@guildion/core/src/extension/Records';
import { LanguageCode } from '@guildion/core/src/constants/LanguageCode';
import { File, FileAttributes } from './File';
import { DocumentGroup, DocumentGroupAttributes } from './DocumentGroup';
import { DocumentSection, DocumentSectionAttributes } from './DocumentSection';

export interface DocumentRecordAttributes {
    jaThumbnail?: File,
    enThumbnail?: File,
    group?: DocumentGroup,
    section?: DocumentSection,
    prev?: Document,
    next?: Document,
}

export interface DocumentAttributes {
    id?: string,
    jaThumbnailId?: string | null,
    enThumbnailId?: string | null,
    groupId?: string,
    sectionId?: string,
    slug: string,
    jaTitle: string,
    enTitle: string,
    jaDescription: string,
    enDescription: string,
    jaData: string,
    enData: string,
    status: DocumentStatus,
    type: DocumentType,
    dataType: DocumentDataType,
    index: number,
    publishedAt?: string,
    deletedAt?: string,
    createdAt?: string,
    updatedAt?: string,
    jaThumbnail?: FileAttributes,
    enThumbnail?: FileAttributes,
    group?: DocumentGroupAttributes,
    section?: DocumentSectionAttributes,
    prev?: DocumentAttributes,
    next?: DocumentAttributes,
}

export const DocumentStatus = {
    NEW: 'NEW',
    DRAFT: 'DRAFT',
    PUBLISH: 'PUBLISH',

    LOADING: 'LOADING,'
} as const;

export type DocumentStatus = typeof DocumentStatus[keyof typeof DocumentStatus];

export const DocumentType = {
    DEFAULT: 'default',
} as const;

export type DocumentType = typeof DocumentType[keyof typeof DocumentType];

export const DocumentDataType = {
    HTML: 'html',
    OTHER: 'other',
} as const;

export type DocumentDataType = typeof DocumentDataType[keyof typeof DocumentDataType];

export class Document extends Record<DocumentAttributes>({
    id: undefined,
    jaThumbnailId: undefined,
    enThumbnailId: undefined,
    groupId: undefined,
    sectionId: undefined,
    slug: '',
    jaTitle: '',
    enTitle: '',
    jaDescription: '',
    enDescription: '',
    jaData: '',
    enData: '',
    status: DocumentStatus.NEW,
    type: DocumentType.DEFAULT,
    dataType: DocumentDataType.OTHER,
    index: 0,
    publishedAt: undefined,
    deletedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    jaThumbnail: undefined,
    enThumbnail: undefined,
    group: undefined,
    section: undefined,
    prev: undefined,
    next: undefined,
}) {
    public readonly records: DocumentRecordAttributes;

    constructor(args?: Partial<DocumentAttributes>, records?: DocumentRecordAttributes) {
        super({
            ...args,
            jaThumbnail: records?.jaThumbnail?.toJSON() ?? args?.jaThumbnail,
            enThumbnail: records?.enThumbnail?.toJSON() ?? args?.enThumbnail,
            section: records?.section?.toJSON() ?? args?.section,
            group: records?.group?.toJSON() ?? args?.group,
            prev: records?.prev?.toJSON() ?? args?.prev,
            next: records?.next?.toJSON() ?? args?.next,
        });
        this.records = {};
        this.records.jaThumbnail = records?.jaThumbnail ? records.jaThumbnail : args?.jaThumbnail ? new File(args?.jaThumbnail) : undefined;
        this.records.enThumbnail = records?.enThumbnail ? records.enThumbnail : args?.enThumbnail ? new File(args?.enThumbnail) : undefined;
        this.records.section = records?.section ? records.section : args?.section ? new DocumentSection(args?.section) : undefined;
        this.records.group = records?.group ? records.group : args?.group ? new DocumentGroup(args?.group) : undefined;
        this.records.prev = records?.prev ? records.prev : args?.prev ? new Document(args?.prev) : undefined;
        this.records.next = records?.next ? records.next : args?.next ? new Document(args?.next) : undefined;
    }

    public setRecord(key: keyof DocumentRecordAttributes, record?: DocumentRecordAttributes[keyof DocumentRecordAttributes]): Document {
        return new Document(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<DocumentAttributes, keyof DocumentRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getPublishedAt(): Date | undefined {
        const result = this.get('publishedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getTitle(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaTitle;
        default:
        case LanguageCode.English: return this.enTitle;
        }
    }
    public getDescription(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaDescription;
        default:
        case LanguageCode.English: return this.enDescription;
        }
    }
    public getThumbnail(lang: LanguageCode): File | undefined {
        switch(lang) {
        case LanguageCode.Japanese: return this.records.jaThumbnail;
        default:
        case LanguageCode.English: return this.records.enThumbnail;
        }
    }
    public getData(lang: LanguageCode): string {
        switch(lang) {
        case LanguageCode.Japanese: return this.jaData;
        default:
        case LanguageCode.English: return this.enData;
        }
    }
}

export class Documents extends Records<Document> {
    constructor(array: Array<DocumentAttributes | Document>) {
        super(array, { Record: Document, Records: Documents })
    }
}