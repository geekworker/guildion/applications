import { Record } from 'immutable';
import { FileType } from '@guildion/core/src/constants/FileType';
import { Provider } from '@guildion/core/src/constants/Provider';
import { Records } from '@guildion/core/src/extension/Records';
import { safeURL } from '@guildion/core/src/extension/URL';
import { ContentType, FileExtension } from '@guildion/core/src/constants/ContentType';
import type { User, UserAttributes } from './User';
import type { SyncVision, SyncVisionAttributes } from './SyncVision';
import type { Guild, GuildAttributes } from './Guild';
import type { Room, RoomAttributes } from './Room';
import { GET_CURRENT_ABS_PROXY_URL_STRING } from "@guildion/core/src/constants/ApplicationConfig";
import { IS_PROD, NODE_ENV } from '../../constants/NODE_ENV';

export const FileStatus = {
    NEW: 'NEW',
    UPLOADING: 'UPLOADING',
    UPLOADED: 'UPLOADED',
    FAILED: 'FAILED',
    LOCKED: 'LOCKED',
    ARCHIVED: 'ARCHIVED',
    DELETING: 'DELETING',
    DELETED: 'DELETED',

    LOADING: 'LOADING',
} as const;

export type FileStatus = typeof FileStatus[keyof typeof FileStatus];

export const FileAccessType = {
    PUBLIC: 'PUBLIC',
    PRIVATE: 'PRIVATE',
} as const;

export type FileAccessType = typeof FileAccessType[keyof typeof FileAccessType];

export interface FileRecordAttributes {
    storage?: File,
    folder?: File,
    thumbnail?: File,
    holder?: User,
    syncVision?: SyncVision,
    guild?: Guild,
    room?: Room,
}

export interface FileAttributes {
    id?: string,
    holderId?: string | null,
    thumbnailId?: string | null,
    filingId?: string,
    folderId?: string,
    roomId?: string,
    roomFileId?: string,
    guildId?: string,
    guildFileId?: string,
    guildReactionId?: string,
    syncVisionId?: string,
    signedURL?: string,
    presignedURL?: string,
    displayName: string,    
    description: string,
    url: string,
    status: FileStatus,
    type: FileType,
    accessType: FileAccessType,
    extension: FileExtension,
    contentType: ContentType,
    contentLength: number,
    provider: Provider,
    providerKey: string,
    isSystem: boolean,
    showInGuild?: boolean,
    content?: string | null,
    widthPx?: number | null,
    heightPx?: number | null,
    durationMs?: number | null,
    filesCount?: number | null,
    index?: number | null,
    randomIndex?: number | null,
    deletedAt?: string | null,
    archivedAt?: string | null,
    createdAt?: string,
    updatedAt?: string,
    holder?: UserAttributes,
    folder?: FileAttributes,
    thumbnail?: FileAttributes,
    syncVision?: SyncVisionAttributes,
    guild?: GuildAttributes,
    room?: RoomAttributes,
}

export class File extends Record<FileAttributes>({
    id: undefined,
    holderId: undefined,
    thumbnailId: undefined,
    filingId: undefined,
    roomId: undefined,
    roomFileId: undefined,
    guildId: undefined,
    guildFileId: undefined,
    guildReactionId: undefined,
    syncVisionId: undefined,
    signedURL: undefined,
    presignedURL: undefined,
    displayName: '',    
    description: '',
    url: '',
    extension: 'other',
    status: FileStatus.NEW,
    type: FileType.Other,
    accessType: FileAccessType.PUBLIC,
    contentType: ContentType.other,
    contentLength: 0,
    provider: Provider.S3,
    providerKey: '',
    isSystem: false,
    deletedAt: undefined,
    archivedAt: undefined,
    createdAt: undefined,
    updatedAt: undefined,
    showInGuild: undefined,
    content: undefined,
    widthPx: undefined,
    heightPx: undefined,
    durationMs: undefined,
    filesCount: undefined,
    index: undefined,
    randomIndex: undefined,
    folder: undefined,
    thumbnail: undefined,
    holder: undefined,
    syncVision: undefined,
    guild: undefined,
    room: undefined,
}) {
    public static displayNameMaxLimit: number = 250;
    public static displayNameMinLimit: number = 1;
    public static providerKeyMaxLimit: number = 500;
    public static providerKeyMinLimit: number = 1;
    public static urlMaxLimit: number = 500;
    public static urlMinLimit: number = 1;
    public readonly records: FileRecordAttributes;

    constructor(args?: Partial<FileAttributes>, records?: FileRecordAttributes) {
        super({
            ...args,
            folder: records?.folder?.toJSON() ?? args?.folder,
            thumbnail: records?.thumbnail?.toJSON() ?? args?.thumbnail,
            holder: records?.holder?.toJSON() ?? args?.holder,
            syncVision: records?.syncVision?.toJSON() ?? args?.syncVision,
            guild: records?.guild?.toJSON() ?? args?.guild,
            room: records?.room?.toJSON() ?? args?.room,
        });
        this.records = {};
        this.records.folder = records?.folder ? records.folder : args?.folder ? new File(args?.folder) : undefined;
        this.records.thumbnail = records?.thumbnail ? records.thumbnail : args?.thumbnail ? new File(args?.thumbnail) : undefined;
        this.records.holder = records?.holder ? records.holder : args?.holder ? new (require('./User').User)(args?.holder) : undefined;
        this.records.syncVision = records?.syncVision ? records.syncVision : args?.holder ? new (require('./SyncVision').SyncVision)(args?.syncVision) : undefined;
        this.records.guild = records?.guild ? records.guild : args?.guild ? new (require('./Guild').Guild)(args?.guild) : undefined;
        this.records.room = records?.room ? records.room : args?.room ? new (require('./Room').Room)(args?.room) : undefined;
    }

    public setRecord(key: keyof FileRecordAttributes, record?: FileRecordAttributes[keyof FileRecordAttributes]): File {
        return new File(this.toJSON(), {
            ...this.records,
            [key]: record,
        });
    }
    public get attributes(): Omit<FileAttributes, keyof FileRecordAttributes> {
        return { ...this.toJSON() };
    }
    public getSignedURL(): URL | undefined {
        return safeURL(this.get('signedURL'));
    }
    public getURL(): URL | undefined {
        return safeURL(this.get('url'));
    }
    public getArchivedAt(): Date | undefined {
        const result = this.get('archivedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getDeletedAt(): Date | undefined {
        const result = this.get('deletedAt')
        return result ? (new Date(result)) : undefined;
    }
    public getCreatedAt(): Date | undefined {
        const result = this.get('createdAt')
        return result ? (new Date(result)) : undefined;
    }
    public getUpdatedAt(): Date | undefined {
        const result = this.get('updatedAt')
        return result ? (new Date(result)) : undefined;
    }
    get thumbnailUrl(): string {
        switch(this.type) {
        case FileType.Image: return this.url;
        case FileType.Video: return this.thumbnail?.url ?? '';
        default: return '';
        }
    }

    public static generateEnvPrefix = () => IS_PROD() ? NODE_ENV.PRODUCTION : NODE_ENV.STAGING;
    public static generateEmojisKey = () => `emojis`;
    public static generateEmojiKey = (slug: string) => `${File.generateEmojisKey()}/${slug}`;
    public static generateProfilesKey = () => `${File.generateEnvPrefix()}/profiles`;
    public static generateProfileKey = (file: { id: string, extension: FileExtension }) => `${File.generateProfilesKey()}/${file.id}.${file.extension}`;
    public static generateThumbnailsKey = () => `${File.generateEnvPrefix()}/thumbnail`;
    public static generateThumbnailKey = (providerKey: string) => `${providerKey}/${File.generateThumbnailsKey()}`;
    public static generateGuildStorageKey = (id: string) => `${File.generateEnvPrefix()}/guilds/${id}`;
    public static generateGuildFileKey = (id: string, file: { id: string, extension: FileExtension }) => `${File.generateGuildStorageKey(id)}/${file.id}.${file.extension}`;
    public static generateRoomStorageKey = (id: string) => `${File.generateEnvPrefix()}/rooms/${id}`;
    public static generateRoomFileKey = (id: string, file: { id: string, extension: FileExtension }) => `${File.generateRoomStorageKey(id)}/${file.id}.${file.extension}`;
}

export class Files extends Records<File> {
    constructor(array: Array<FileAttributes | File>) {
        super(array, { Record: File, Records: Files })
    }
    public get logos(): Files {
        return new Files(this.toArray().filter((file) => file.providerKey.startsWith('logos/')));
    }
    public get brandLogos(): Files {
        return new Files(this.toArray().filter((file) => file.providerKey.startsWith('brand_logos/')));
    }
    public get userProfiles(): Files {
        return new Files(this.toArray().filter((file) => file.providerKey.startsWith('user_profiles/')));
    }
    public get guildProfiles(): Files {
        return new Files(this.toArray().filter((file) => file.providerKey.startsWith('guild_profiles/')));
    }
    public get roomProfiles(): Files {
        return new Files(this.toArray().filter((file) => file.providerKey.startsWith('room_profiles/')));
    }
    public generateUserProfile(): File {
        return this.userProfiles.get(
            Math.floor(
                Math.random() * Math.floor(this.userProfiles.length)
            )
        )!;
    };
    public generateGuildProfile(): File {
        return this.guildProfiles.get(
            Math.floor(
                Math.random() * Math.floor(this.guildProfiles.length)
            )
        )!;
    };
    public generateRoomProfile(): File {
        return this.roomProfiles.get(
            Math.floor(
                Math.random() * Math.floor(this.roomProfiles.length)
            )
        )!;
    };
    static getSeed = (): Files => new Files([
        new File({
            // brand_logos
            contentLength: 35300,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/brand_logos/blue.${FileExtension(ContentType.png)}`,
            displayName: `blue.${FileExtension(ContentType.png)}`,
            description: '',
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            status: FileStatus.UPLOADED,
            provider: Provider.S3,
            contentType: ContentType.png,
            isSystem: true,
            providerKey: `brand_logos/blue.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 35300,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/brand_logos/original.${FileExtension(ContentType.png)}`,
            displayName: `original.${FileExtension(ContentType.png)}`,
            description: '',
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            status: FileStatus.UPLOADED,
            provider: Provider.S3,
            contentType: ContentType.png,
            isSystem: true,
            providerKey: `brand_logos/original.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 35300,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/brand_logos/premium.${FileExtension(ContentType.png)}`,
            displayName: `premium.${FileExtension(ContentType.png)}`,
            description: '',
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            status: FileStatus.UPLOADED,
            provider: Provider.S3,
            contentType: ContentType.png,
            isSystem: true,
            providerKey: `brand_logos/premium.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 35300,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/brand_logos/red.${FileExtension(ContentType.png)}`,
            displayName: `red.${FileExtension(ContentType.png)}`,
            description: '',
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            status: FileStatus.UPLOADED,
            provider: Provider.S3,
            contentType: ContentType.png,
            isSystem: true,
            providerKey: `brand_logos/red.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 35300,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/brand_logos/white.${FileExtension(ContentType.png)}`,
            displayName: `white.${FileExtension(ContentType.png)}`,
            description: '',
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            status: FileStatus.UPLOADED,
            provider: Provider.S3,
            contentType: ContentType.png,
            isSystem: true,
            providerKey: `brand_logos/white.${FileExtension(ContentType.png)}`,
        }),
        // guild_profiles
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/guild_profiles/blue.${FileExtension(ContentType.png)}`,
            displayName: `blue.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey:  `guild_profiles/blue.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/guild_profiles/green.${FileExtension(ContentType.png)}`,
            displayName: `green.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey:  `guild_profiles/green.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/guild_profiles/pink.${FileExtension(ContentType.png)}`,
            displayName: `pink.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey:  `guild_profiles/pink.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/guild_profiles/purple.${FileExtension(ContentType.png)}`,
            displayName: `purple.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey:  `guild_profiles/purple.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/guild_profiles/yellow.${FileExtension(ContentType.png)}`,
            displayName: `yellow.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey:  `guild_profiles/yellow.${FileExtension(ContentType.png)}`,
        }),
        // logos
        new File({
            contentLength: 50400,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/logos/original.${FileExtension(ContentType.png)}`,
            displayName: `original.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `logos/original.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 50400,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/logos/premium.${FileExtension(ContentType.png)}`,
            displayName: `premium.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `logos/premium.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 50400,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/logos/white.${FileExtension(ContentType.png)}`,
            displayName: `white.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `logos/white.${FileExtension(ContentType.png)}`,
        }),
        // room_profiles
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/room_profiles/blue.${FileExtension(ContentType.png)}`,
            displayName: `blue.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `room_profiles/blue.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/room_profiles/green.${FileExtension(ContentType.png)}`,
            displayName: `green.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `room_profiles/green.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/room_profiles/pink.${FileExtension(ContentType.png)}`,
            displayName: `pink.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `room_profiles/pink.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/room_profiles/purple.${FileExtension(ContentType.png)}`,
            displayName: `purple.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `room_profiles/purple.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/room_profiles/yellow.${FileExtension(ContentType.png)}`,
            displayName: `yellow.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `room_profiles/yellow.${FileExtension(ContentType.png)}`,
        }),
        // user_profiles
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/user_profiles/blue.${FileExtension(ContentType.png)}`,
            displayName: `blue.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `user_profiles/blue.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/user_profiles/green.${FileExtension(ContentType.png)}`,
            displayName: `green.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `user_profiles/green.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/user_profiles/pink.${FileExtension(ContentType.png)}`,
            displayName: `pink.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `user_profiles/pink.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/user_profiles/purple.${FileExtension(ContentType.png)}`,
            displayName: `purple.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `user_profiles/purple.${FileExtension(ContentType.png)}`,
        }),
        new File({
            contentLength: 25800,
            widthPx: 512,
            heightPx: 512,
            extension: FileExtension(ContentType.png),
            url: `${GET_CURRENT_ABS_PROXY_URL_STRING()}/user_profiles/yellow.${FileExtension(ContentType.png)}`,
            displayName: `yellow.${FileExtension(ContentType.png)}`,
            description: '',
            status: FileStatus.UPLOADED,
            type: FileType.Image,
            accessType: FileAccessType.PUBLIC,
            contentType: ContentType.png,
            provider: Provider.S3,
            isSystem: true,
            providerKey: `user_profiles/yellow.${FileExtension(ContentType.png)}`,
        }),
    ]);
}