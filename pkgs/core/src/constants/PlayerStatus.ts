export const PlayerStatus = {
    PLAY: 'PLAY',
    PAUSE: 'PAUSE',
    LOADING: 'LOADING',
} as const;

export type PlayerStatus = typeof PlayerStatus[keyof typeof PlayerStatus];