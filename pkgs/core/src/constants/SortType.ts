export const SortType = {
    CREATED_AT: 'CREATED_AT',
    UPDATED_AT: 'UPDATED_AT',
    POPULAR: 'POPULAR',
    RECOMMEND: 'RECOMMEND',
} as const;

export type SortType = typeof SortType[keyof typeof SortType];