export const BreakPoint = {
    xs: {
        number: 365,
        px: '365px',
    },
    sm: {
        number: 400,
        px: '400px',
    },
    fm: {
        number: 560,
        px: '560px',
    },
    md: {
        number: 768,
        px: '768px',
    },
    lg: {
        number: 1000,
        px: '1000px',
    },
    xl: {
        number: 1200,
        px: '1200px',
    },
    xxl: {
        number: 1300,
        px: '1300px',
    },
} as const;

export type BreakPoint = typeof BreakPoint[keyof typeof BreakPoint];