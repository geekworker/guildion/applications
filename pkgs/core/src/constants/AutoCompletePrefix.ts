export const AutoCompletePrefix = {
    Member: '@',
    Room: '!',
    Other: 'other',
} as const;

export type AutoCompletePrefix = typeof AutoCompletePrefix[keyof typeof AutoCompletePrefix];