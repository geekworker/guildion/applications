import { Endpoint } from "../adaptors/endpoints";

export const maxRetryLimit: number = Number.MAX_SAFE_INTEGER;
export const retryDelay: number = 100;

export const fetchData_limit = (endpoint: Endpoint<any, any, any>) => {
    switch(endpoint) {
        default: 10;
    }
}