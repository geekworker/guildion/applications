import { Widget } from "../../adaptors/models/Widget";

export const SettingWidgetName: string = 'setting' 
export const SettingWidget: Widget = new Widget({
    widgetname: SettingWidgetName,
    jaName: '設定',
    enName: 'Setting',
    jaDescription: 'このルームやメンバーに関する情報の設定をすることができます',
    enDescription: 'we can edit the data of this room and members',
});