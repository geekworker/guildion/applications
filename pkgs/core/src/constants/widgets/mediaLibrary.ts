import { Widget } from "../../adaptors/models/Widget";

export const MediaLibraryWidgetName: string = 'media_library' 
export const MediaLibraryWidget: Widget = new Widget({
    widgetname: MediaLibraryWidgetName,
    jaName: 'メディアライブラリー',
    enName: 'Media Library',
    jaDescription: 'ルーム内の画像・動画などのメディアをアップロード・管理することができます',
    enDescription: 'we can upload and store the media like image/video in this room',
});