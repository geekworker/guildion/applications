import { Widget } from "../../adaptors/models/Widget";

export const TextChatWidgetName: string = 'text_chat' 
export const TextChatWidget: Widget = new Widget({
    widgetname: TextChatWidgetName,
    jaName: 'チャット',
    enName: 'Text chat',
    jaDescription: '同期再生しながらメンバーとのチャットを楽しめます',
    enDescription: 'While sync vision playing, we can chat with members',
});