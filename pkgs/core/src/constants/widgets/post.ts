import { Widget } from "../../adaptors/models/Widget";

export const PostWidgetName: string = 'post' 
export const PostWidget: Widget = new Widget({
    widgetname: PostWidgetName,
    jaName: '投稿',
    enName: 'Post',
    jaDescription: '投稿することでルーム内外のメンバーにこのルームに関する情報を伝えることができます',
    enDescription: 'we can inform the members inside and outside here about this room',
});