import { Widget } from "../../adaptors/models/Widget";

export const SyncVisionWidgetName: string = 'sync_vision' 
export const SyncVisionWidget: Widget = new Widget({
    widgetname: SyncVisionWidgetName,
    jaName: '同期再生',
    enName: 'Sync vision',
    jaDescription: 'メンバーと一緒にリアルタイムで動画・画像を楽しむことができます。',
    enDescription: 'we can watch video/image with the members in realtime',
});