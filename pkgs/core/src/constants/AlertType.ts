export const AlertType = {
    Error: 'error',
    Success: 'success',
    Warning: 'warning',
};

export type AlertType = typeof AlertType[keyof typeof AlertType];