export const MentionType = {
    Members: '@room',
} as const;

export type MentionType = typeof MentionType[keyof typeof MentionType];