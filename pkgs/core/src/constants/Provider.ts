export const Provider = {
    Guildion: 'guildion',
    S3: 's3',
    Connect: 'connect',
    Other: 'other',
} as const;

export type Provider = typeof Provider[keyof typeof Provider];

export const fallbackProvider: Provider = "other";