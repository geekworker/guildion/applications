export const PaymentMethod = {
    STRIPE: 'STRIPE',
    IOS: 'IOS',
    ANDROID: 'ANDROID',
    OTHER: 'OTHER',
} as const;

export type PaymentMethod = typeof PaymentMethod[keyof typeof PaymentMethod];