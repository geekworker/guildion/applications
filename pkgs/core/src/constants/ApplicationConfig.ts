import { CURRENT_NODE_ENV, IS_MOBILE, LOCAL_IP_ADDRESS, GUILDION_API_SERVICE_HOST, GUILDION_CONNECT_SERVICE_HOST, GUILDION_CDN_SERVICE_HOST, GUILDION_PCDN_SERVICE_HOST } from "./Configure";
import { NODE_ENV } from "./NODE_ENV";

export const APP_NAME: string = 'Guildion';
export const APP_NAME_LATIN: string = 'Guildion';
export const APP_NAME_UPPERCASE: string = 'GUILDION';
export const BLOG_NAME: string = 'Geeklize';
export const BLOG_SEO_NAME: string = 'Geeklize Blog';
export const BLOG_NAME_LATIN: string = 'Geeklize';
export const BLOG_NAME_UPPERCASE: string = 'GEEKLIZE';
export const APP_ICON: string = 'Guildion';
export const HOST_NAME: string = 'Guildion';
export const LOCAL_APP_PORT: number = 7000;
export const LOCAL_API_PORT: number = 7001;
export const LOCAL_CONNECT_PORT: number = 7002;
export const LOCAL_BLOG_PORT: number = 7003;

export const ABS_PROXY_URL_STRING: string = CURRENT_NODE_ENV() == NODE_ENV.PRODUCTION ? `https://cdn.guildion.co/abs` : `https://cdn.guildion.co/abs`;
export const ABS_PROXY_URL: URL = new URL(`${ABS_PROXY_URL_STRING}`);
export const PCDN_PROXY_URL_STRING: string = CURRENT_NODE_ENV() == NODE_ENV.PRODUCTION ? `https://pcdn.guildion.co` : `https://pcdn.guildion.co`;
export const PCDN_PROXY_URL: URL = new URL(`${PCDN_PROXY_URL_STRING}`);
export const CDN_PROXY_URL_STRING: string = CURRENT_NODE_ENV() == NODE_ENV.PRODUCTION ? `https://cdn.guildion.co` : `https://cdn.guildion.co`;
export const CDN_PROXY_URL: URL = new URL(`${CDN_PROXY_URL_STRING}`);

export const IOS_SCHEME: string = 'guildion://';
export const IOS_DOWNLOAD_URL: URL = new URL('https://apps.apple.com/jp/app/nowful-realtime-community/id1515505548');
export const IOS_TESTFLIGHT_URL = 'https://testflight.apple.com/join/t5YOhCWK';

export const GET_APP_DOMAIN = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        case NODE_ENV.DEVELOPMENT:
            return IS_MOBILE() ? `${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}` : `localhost:${LOCAL_APP_PORT}`;
        case NODE_ENV.STAGING:
            return 'staging.guildion.co';
        default:
        case NODE_ENV.PRODUCTION:
            return 'www.guildion.co';
    }
};

export const GET_APP_URL = (nodeEnv: NODE_ENV): URL => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? new URL(`http://${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}`) : new URL(`http://localhost:${LOCAL_APP_PORT}`);
    case NODE_ENV.STAGING:
        return new URL('https://staging.guildion.co');
    default:
    case NODE_ENV.PRODUCTION:
        return new URL('https://www.guildion.co');
    }
};

export const GET_APP_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? `http://${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}` : `http://localhost:${LOCAL_APP_PORT}`;
    case NODE_ENV.STAGING:
        return 'https://staging.guildion.co';
    default:
    case NODE_ENV.PRODUCTION:
        return 'https://www.guildion.co';
    }
};

export const GET_BLOG_DOMAIN = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        case NODE_ENV.DEVELOPMENT:
            return IS_MOBILE() ? `${LOCAL_IP_ADDRESS()}:${LOCAL_BLOG_PORT}` : `localhost:${LOCAL_BLOG_PORT}`;
        case NODE_ENV.STAGING:
            return 'staging.geeklize.com';
        default:
        case NODE_ENV.PRODUCTION:
            return 'www.geeklize.com';
    }
};

export const GET_BLOG_URL = (nodeEnv: NODE_ENV): URL => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? new URL(`http://${LOCAL_IP_ADDRESS()}:${LOCAL_BLOG_PORT}`) : new URL(`http://localhost:${LOCAL_BLOG_PORT}`);
    case NODE_ENV.STAGING:
        return new URL('https://staging.geeklize.com');
    default:
    case NODE_ENV.PRODUCTION:
        return new URL('https://www.geeklize.com');
    }
};

export const GET_BLOG_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? `http://${LOCAL_IP_ADDRESS()}:${LOCAL_BLOG_PORT}` : `http://localhost:${LOCAL_BLOG_PORT}`;
    case NODE_ENV.STAGING:
        return 'https://staging.geeklize.com';
    default:
    case NODE_ENV.PRODUCTION:
        return 'https://www.geeklize.com';
    }
};

export const GET_API_DOMAIN = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        case NODE_ENV.DEVELOPMENT:
            return IS_MOBILE() ? `${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}` : `localhost:${LOCAL_APP_PORT}`;
        case NODE_ENV.STAGING:
            return 'staging.guildion.co';
        default:
        case NODE_ENV.PRODUCTION:
            return 'www.guildion.co';
    }
};

export const GET_API_URL = (nodeEnv: NODE_ENV): URL => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? new URL(`http://${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}/api/proxy`) : new URL(`localhost:${LOCAL_APP_PORT}/api/proxy`);
    case NODE_ENV.STAGING:
        return !!GUILDION_API_SERVICE_HOST() ? new URL(`http://${GUILDION_API_SERVICE_HOST()}`) : new URL('https://staging.guildion.co/api/proxy');
    default:
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_API_SERVICE_HOST() ? new URL(`http://${GUILDION_API_SERVICE_HOST()}`) : new URL('https://www.guildion.co/api/proxy');
    }
};

export const GET_API_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? `http://${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}/api/proxy` : `http://localhost:${LOCAL_APP_PORT}/api/proxy`;
    case NODE_ENV.STAGING:
        return !!GUILDION_API_SERVICE_HOST() ? `http://${GUILDION_API_SERVICE_HOST()}` : 'https://staging.guildion.co/api/proxy';
    default:
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_API_SERVICE_HOST() ? `http://${GUILDION_API_SERVICE_HOST()}` : 'https://www.guildion.co/api/proxy';
    }
};

export const GET_CONNECT_API_URL = (nodeEnv: NODE_ENV): URL => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? new URL(`http://${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}/api/proxy/connect/v1`) : new URL(`http://localhost:${LOCAL_APP_PORT}/api/proxy/connect/v1`);
    case NODE_ENV.STAGING:
        return !!GUILDION_CONNECT_SERVICE_HOST() ? new URL(`http://${GUILDION_CONNECT_SERVICE_HOST()}`) : new URL('https://staging.guildion.co/api/proxy/connect/v1');
    default:
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_CONNECT_SERVICE_HOST() ? new URL(`http://${GUILDION_CONNECT_SERVICE_HOST()}`) : new URL('https://www.guildion.co/api/proxy/connect/v1');
    }
};

export const GET_CONNECT_API_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? `http://${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}/api/proxy/connect/v1` : `http://localhost:${LOCAL_APP_PORT}/api/proxy/connect/v1`;
    case NODE_ENV.STAGING:
        return !!GUILDION_CONNECT_SERVICE_HOST() ? `http://${GUILDION_CONNECT_SERVICE_HOST()}` : 'https://staging.guildion.co/api/proxy/connect/v1';
    default:
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_CONNECT_SERVICE_HOST() ? `http://${GUILDION_CONNECT_SERVICE_HOST()}` : 'https://www.guildion.co/api/proxy/connect/v1';
    }
};

export const GET_CONNECT_API_DOMAIN = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        case NODE_ENV.DEVELOPMENT:
            return IS_MOBILE() ? `${LOCAL_IP_ADDRESS()}:${LOCAL_APP_PORT}` : `localhost:${LOCAL_APP_PORT}`;
        case NODE_ENV.STAGING:
            return !!GUILDION_CONNECT_SERVICE_HOST() ? `${GUILDION_CONNECT_SERVICE_HOST()}` : 'staging.guildion.co';
        default:
        case NODE_ENV.PRODUCTION:
            return !!GUILDION_CONNECT_SERVICE_HOST() ? `${GUILDION_CONNECT_SERVICE_HOST()}` : 'www.guildion.co';
    }
};

export const GET_ABS_PROXY_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        default:
    case NODE_ENV.STAGING:
        return !!GUILDION_CDN_SERVICE_HOST() ? `http://${GUILDION_CDN_SERVICE_HOST()}` : 'https://cdn.guildion.co/abs';
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_CDN_SERVICE_HOST() ? `http://${GUILDION_CDN_SERVICE_HOST()}` : 'https://cdn.guildion.co/abs';
    }
};

export const GET_PCDN_PROXY_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        default:
    case NODE_ENV.STAGING:
        return !!GUILDION_PCDN_SERVICE_HOST() ? `http://${GUILDION_PCDN_SERVICE_HOST()}` : 'https://pcdn.guildion.co';
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_PCDN_SERVICE_HOST() ? `http://${GUILDION_PCDN_SERVICE_HOST()}` : 'https://pcdn.guildion.co';
    }
};

export const GET_CDN_PROXY_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
        default:
    case NODE_ENV.STAGING:
        return !!GUILDION_CDN_SERVICE_HOST() ? `http://${GUILDION_CDN_SERVICE_HOST()}` : 'https://cdn.guildion.co';
    case NODE_ENV.PRODUCTION:
        return !!GUILDION_CDN_SERVICE_HOST() ? `http://${GUILDION_CDN_SERVICE_HOST()}` : 'https://cdn.guildion.co';
    }
};

export const GET_WEBSOCKET_URL = (nodeEnv: NODE_ENV): URL => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? new URL(`ws://${LOCAL_IP_ADDRESS()}:${LOCAL_CONNECT_PORT}`) : new URL(`ws://localhost:${LOCAL_CONNECT_PORT}`);
    case NODE_ENV.STAGING:
        return new URL('wss://staging.guildion.co/connect');
    default:
    case NODE_ENV.PRODUCTION:
        return new URL('wss://www.guildion.co/connect');
    }
};

export const GET_WEBSOCKET_URL_STRING = (nodeEnv: NODE_ENV): string => { 
    switch(nodeEnv) {
    case NODE_ENV.DEVELOPMENT:
        return IS_MOBILE() ? `ws://${LOCAL_IP_ADDRESS()}:${LOCAL_CONNECT_PORT}` : `ws://localhost:${LOCAL_CONNECT_PORT}`;
    case NODE_ENV.STAGING:
        return 'wss://staging.guildion.co/connect';
    default:
    case NODE_ENV.PRODUCTION:
        return 'wss://www.guildion.co/connect';
    }
};

export const GET_CURRENT_APP_DOMAIN = () => GET_APP_DOMAIN(CURRENT_NODE_ENV());
export const GET_CURRENT_APP_URL = () => GET_APP_URL(CURRENT_NODE_ENV());
export const GET_CURRENT_APP_URL_STRING = () => GET_APP_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_BLOG_DOMAIN = () => GET_BLOG_DOMAIN(CURRENT_NODE_ENV());
export const GET_CURRENT_BLOG_URL = () => GET_BLOG_URL(CURRENT_NODE_ENV());
export const GET_CURRENT_BLOG_URL_STRING = () => GET_BLOG_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_API_DOMAIN = () => GET_API_DOMAIN(CURRENT_NODE_ENV());
export const GET_CURRENT_API_URL = () => GET_API_URL(CURRENT_NODE_ENV());
export const GET_CURRENT_API_URL_STRING = () => GET_API_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_ABS_PROXY_URL_STRING = () => GET_ABS_PROXY_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_PCDN_PROXY_URL_STRING = () => GET_PCDN_PROXY_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_CDN_PROXY_URL_STRING = () => GET_CDN_PROXY_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_CONNECT_API_DOMAIN = () => GET_CONNECT_API_DOMAIN(CURRENT_NODE_ENV());
export const GET_CURRENT_CONNECT_API_URL = () => GET_CONNECT_API_URL(CURRENT_NODE_ENV());
export const GET_CURRENT_CONNECT_API_URL_STRING = () => GET_CONNECT_API_URL_STRING(CURRENT_NODE_ENV());
export const GET_CURRENT_WEBSOCKET_URL = () => GET_WEBSOCKET_URL(CURRENT_NODE_ENV());
export const GET_CURRENT_WEBSOCKET_URL_STRING = () => GET_WEBSOCKET_URL_STRING(CURRENT_NODE_ENV());