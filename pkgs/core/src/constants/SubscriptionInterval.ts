export const SubscriptionInterval = {
    Daily: 'daily',
    Weekly: 'weekly',
    Monthly: 'monthly',
    Yearly: 'yearly',
    Forever: 'forever',
} as const;

export type SubscriptionInterval = typeof SubscriptionInterval[keyof typeof SubscriptionInterval];