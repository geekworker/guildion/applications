export const SuccessType = {
    network: 'network',
};

export type SuccessType = typeof SuccessType[keyof typeof SuccessType];