export const OS = {
    Web: 'web',
    iOS: 'ios',
    Android: 'android',
} as const;

export type OS = typeof OS[keyof typeof OS];