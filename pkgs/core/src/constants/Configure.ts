import { NODE_ENV } from "./NODE_ENV";

let LOCAL_IP_ADDRESS$: string = '';
let IS_MOBILE$: boolean = false;
let CURRENT_NODE_ENV$: NODE_ENV = NODE_ENV.DEVELOPMENT;
let GUILDION_API_SERVICE_HOST$: string = '';
let GUILDION_CONNECT_SERVICE_HOST$: string = '';
let GUILDION_CDN_SERVICE_HOST$: string = '';
let GUILDION_PCDN_SERVICE_HOST$: string = '';

export function LOCAL_IP_ADDRESS(): string { return LOCAL_IP_ADDRESS$ };
export function IS_MOBILE(): boolean { return IS_MOBILE$ };
export function CURRENT_NODE_ENV(): NODE_ENV { return CURRENT_NODE_ENV$ };
export function GUILDION_API_SERVICE_HOST(): string | undefined { return GUILDION_API_SERVICE_HOST$ };
export function GUILDION_CONNECT_SERVICE_HOST(): string | undefined { return GUILDION_CONNECT_SERVICE_HOST$ };
export function GUILDION_CDN_SERVICE_HOST(): string | undefined { return GUILDION_CDN_SERVICE_HOST$ };
export function GUILDION_PCDN_SERVICE_HOST(): string | undefined { return GUILDION_PCDN_SERVICE_HOST$ };

export function configure(values: { LOCAL_IP_ADDRESS?: string, IS_MOBILE?: boolean, CURRENT_NODE_ENV?: NODE_ENV, GUILDION_API_SERVICE_HOST?: string, GUILDION_CONNECT_SERVICE_HOST?: string, GUILDION_CDN_SERVICE_HOST?: string, GUILDION_PCDN_SERVICE_HOST?: string }) {
    if (values.LOCAL_IP_ADDRESS !== undefined) { LOCAL_IP_ADDRESS$ = values.LOCAL_IP_ADDRESS }
    if (values.IS_MOBILE !== undefined) { IS_MOBILE$ = values.IS_MOBILE }
    if (values.CURRENT_NODE_ENV !== undefined) { CURRENT_NODE_ENV$ = values.CURRENT_NODE_ENV }
    if (values.GUILDION_API_SERVICE_HOST !== undefined) { GUILDION_API_SERVICE_HOST$ = values.GUILDION_API_SERVICE_HOST }
    if (values.GUILDION_CONNECT_SERVICE_HOST !== undefined) { GUILDION_CONNECT_SERVICE_HOST$ = values.GUILDION_CONNECT_SERVICE_HOST }
    if (values.GUILDION_CDN_SERVICE_HOST !== undefined) { GUILDION_CDN_SERVICE_HOST$ = values.GUILDION_CDN_SERVICE_HOST }
    if (values.GUILDION_PCDN_SERVICE_HOST !== undefined) { GUILDION_PCDN_SERVICE_HOST$ = values.GUILDION_PCDN_SERVICE_HOST }
}