export const SortDirection = {
    DESC: 'DESC',
    ASC: 'ASC',
} as const;

export type SortDirection = typeof SortDirection[keyof typeof SortDirection];