export const HTMLEmbed = {
    Link: {
        value: 'LINK',
        stringify: (args:{ href: string, className?: string }) => `${HTMLEmbedSeparator}` + 'LINK' + JSON.stringify(args) + `${HTMLEmbedSeparator}`,
        parse: (str: string) => JSON.parse(str) as { href: string, className?: string },
    },
    Image: {
        value: 'IMAGE',
        stringify: (args:{ src: string, className?: string, width: number, height: number, layout: 'fill' | 'responsive' }) => `${HTMLEmbedSeparator}` + 'IMAGE' + JSON.stringify(args) + `${HTMLEmbedSeparator}`,
        parse: (str: string) => JSON.parse(str) as { src: string, className?: string, width: number, height: number, layout: 'fill' | 'responsive' },
    },
    ReferenceImage: {
        value: 'REFERENCE_IMAGE',
        stringify: (args:{ href:string, name: string, src: string, className?: string, width?: number, height?: number, layout: 'fill' | 'responsive' }) => `${HTMLEmbedSeparator}` + 'REFERENCE_IMAGE' + JSON.stringify(args) + `${HTMLEmbedSeparator}`,
        parse: (str: string) => JSON.parse(str) as { href:string, name: string, src: string, className?: string, width?: number, height?: number, layout: 'fill' | 'responsive' },
    },
    AppDownloadButton: {
        value: 'APP_DOWNLOAD_BUTTON',
        stringify: (args:{ iosHref?: string, androidHref?: string }) => `${HTMLEmbedSeparator}` + 'APP_DOWNLOAD_BUTTON' + JSON.stringify(args) + `${HTMLEmbedSeparator}`,
        parse: (str: string) => JSON.parse(str) as { iosHref?: string, androidHref?: string },
    },
    Custom: {
        value: 'CUSTOM',
        stringify: (args: { [key: string]: any }) => `${HTMLEmbedSeparator}` + 'CUSTOM' + JSON.stringify(args) + `${HTMLEmbedSeparator}`,
        parse: (str: string) => JSON.parse(str) as { [key: string]: any },
    },
} as const;

export type HTMLEmbed = typeof HTMLEmbed[keyof typeof HTMLEmbed];

export const HTMLEmbedSeparator = '!!!!!!!!!!';