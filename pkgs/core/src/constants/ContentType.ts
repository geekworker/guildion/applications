export const ContentType = {
    other: "other",

    folder: "guildion/folder",
    playlist: "guildion/playlist",
    album: "guildion/album",

    txt: "text/plain",
    csv: "text/csv",
    html: "text/html",
    css: "text/css",
    javascript: "text/javascript",
    exe: "application/octet-stream",
    json: "application/json",
    pdf: "application/pdf",
    xls: "application/vnd.ms-excel",
    xlsx: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    ppt: "application/vnd.ms-powerpoint",
    pptx: "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    doc: "application/msword",
    docx: "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    
    bitmap: "image/bmp",
    svg: "image/svg+xml",
    zip: "application/zip",
    lzh: "application/x-lzh",
    tar: "application/x-tar",
    
    ai: "application/postscript",
    bmp: "image/x-bmp",
    rle: "image/x-bmp",
    dib: "image/x-bmp",
    cgm: "image/cgm",
    dwf: "drawing/x-dwf",
    epsf: "appilcation/postscript",
    eps: "appilcation/postscript",
    ps: "appilcation/postscript",
    fif: "image/fif",
    fpx: "image/fpx", // "image/x-fpx"
    gif: "image/gif",
    jpg: "image/jpeg",
    jpeg: "image/jpeg",
    jpe: "image/jpeg",
    jfif: "image/jpeg",
    jfi: "image/jpeg",
    pcd: "image/pcd", // "image/x-photo-cd"
    pict: "image/pict",
    pct: "image/pict",
    png: "image/png", // "image/x-png"
    tga: "image/x-targa",
    tpic: "image/x-targa",
    vda: "image/x-targa",
    vst: "image/x-targa",
    tiff: "image/tiff", // "image/x-tiff"
    tif: "image/tiff", // "image/x-tiff"
    wrl: "model/vrml", // "x-world/x-vrml"
    xbm: "image/x-bitmap",
    xpm: "image/x-xpixmap",
    

    aiff: "audio/aiff", // "audio/x-aiff"
    aif: "audio/aiff", // "audio/x-aiff"
    au: "audio/basic",
    snd: "audio/basic",
    kar: "audio/midi", // "audio/x-midi"
    midi: "audio/midi", // "audio/x-midi"
    mid: "audio/midi", // "audio/x-midi"
    smf: "audio/midi", // "audio/x-midi"
    m1a: "audio/mpeg", // "audio/x-mpeg"
    m2a: "audio/mpeg", // "audio/x-mpeg"
    mp2: "audio/mpeg", // "audio/x-mpeg"
    mp3: "audio/mpeg", // "audio/x-mpeg"
    mpa: "audio/mpeg", // "audio/x-mpeg"
    mpega: "audio/mpeg", // "audio/x-mpeg"
    rpm: "audio/x-pn-realaudio-plugin",
    // MARK: only swa is sound
    swa: "application/x-director",
    dcr: "application/x-director",
    dir: "application/x-director",
    dxr: "application/x-director",
    vqf: "audio/x-twinvq",
    wav: "audio/wav", // "audio/x-wav"
    
    
    aab: "application/x-authorware-bin",
    aam: "application/x-authorware-map",
    aas: "application/x-authorware-seg",
    asf: "video/x-ms-asf",
    avi: "vide/x-msvideo",
    flc: "video/flc",
    fli: "video/flc",
    moov: "video/quicktime",
    mov: "video/quicktime",
    qt: "video/quicktime",
    mp4: "video/mp4",
    // MARK: _2s is equal 2s
    mpeg: "vide/mpeg", // "video/x-mpeg"
    mpg: "vide/mpeg", // "video/x-mpeg"
    mpe: "vide/mpeg", // "video/x-mpeg"
    mpv: "vide/mpeg", // "video/x-mpeg"
    mng: "vide/mpeg", // "video/x-mpeg"
    m1s: "vide/mpeg", // "video/x-mpeg"
    m1v: "vide/mpeg", // "video/x-mpeg"
    _2s: "vide/mpeg", // "video/x-mpeg"
    m2v: "vide/mpeg", // "video/x-mpeg"
    
    rm: "audio/x-pn-realaudio",
    spl: "application/futuresplash",
    swf: "application/x-shockwave-flash",
    vdo: "video/vdo",
    viv: "video/vnd.vivo",
    vivo: "video/vnd.vivo",
    xdm: "application/x-xdma",
    xdma: "application/x-xdma",
} as const;

export type ContentType = typeof ContentType[keyof typeof ContentType];

export type FileExtension = "txt" | "csv" | "html" | "css" | "javascript" | "exe" | "json" | "pdf" | "xls" | "xlsx" | "ppt" | "pptx" | "doc" | "docx" | "bitmap" | "svg" | "zip" | "lzh" | "tar" | "ai" | "bmp" | "rle" | "dib" | "cgm" | "dwf" | "epsf" | "eps" | "ps" | "fif" | "fpx" | "gif" | "jpg" | "jpeg" | "jpe" | "jfif" | "jfi" | "pcd" | "pict" | "pct" | "png" | "tga" | "tpic" | "vda" | "vst" | "tiff" | "tif" | "wrl" | "xbm" | "xpm" | "aiff" | "aif" | "au" | "snd" | "kar" | "midi" | "mid" | "smf" | "m1a" | "m2a" | "mp2" | "mp3" | "mpa" | "mpega" | "rpm" | "swa" | "dcr" | "dir" | "dxr" | "vqf" | "wav" | "aab" | "aam" | "aas" | "asf" | "avi" | "flc" | "fli" | "moov" | "mov" | "qt" | "mpeg" | "mpg" | "mpe" | "mpv" | "mng" | "m1s" | "m1v" | "2s" | "m2v" | "mp4" | "rm" | "spl" | "swf" | "vdo" | "viv" | "vivo" | "xdm" | "xdma" | "folder" | "playlist" | "album" | "other";

export const FileExtension = (contentType: ContentType): FileExtension => {
    switch (contentType) {
        case ContentType.txt: return "txt"
        case ContentType.csv: return "csv"
        case ContentType.html: return "html"
        case ContentType.css: return "css"
        case ContentType.javascript: return "javascript"
        case ContentType.exe: return "exe"
        case ContentType.json: return "json"
        case ContentType.pdf: return "pdf"
        case ContentType.xls: return "xls"
        case ContentType.xlsx: return "xlsx"
        case ContentType.ppt: return "ppt"
        case ContentType.pptx: return "pptx"
        case ContentType.doc: return "doc"
        case ContentType.docx: return "docx"
        
        case ContentType.bitmap: return "bitmap"
        case ContentType.svg: return "svg"
        case ContentType.zip: return "zip"
        case ContentType.lzh: return "lzh"
        case ContentType.tar: return "tar"
        
        case ContentType.ai: return "ai"
        case ContentType.bmp: return "bmp"
        case ContentType.rle: return "rle"
        case ContentType.dib: return "dib"
        case ContentType.cgm: return "cgm"
        case ContentType.dwf: return "dwf"
        case ContentType.epsf: return "epsf"
        case ContentType.eps: return "eps"
        case ContentType.ps: return "ps"
        case ContentType.fif: return "fif"
        case ContentType.fpx: return "fpx"
        case ContentType.gif: return "gif"
        case ContentType.jpg: return "jpg"
        case ContentType.jpeg: return "jpeg"
        case ContentType.jpe: return "jpe"
        case ContentType.jfif: return "jfif"
        case ContentType.jfi: return "jfi"
        case ContentType.pcd: return "pcd"
        case ContentType.pict: return "pict"
        case ContentType.pct: return "pct"
        case ContentType.png: return "png"
        case ContentType.tga: return "tga"
        case ContentType.tpic: return "tpic"
        case ContentType.vda: return "vda"
        case ContentType.vst: return "vst"
        case ContentType.tiff: return "tiff"
        case ContentType.tif: return "tif"
        case ContentType.wrl: return "wrl"
        case ContentType.xbm: return "xbm"
        case ContentType.xpm: return "xpm"
        
        case ContentType.aiff: return "aiff"
        case ContentType.aif: return "aif"
        case ContentType.au: return "au"
        case ContentType.snd: return "snd"
        case ContentType.kar: return "kar"
        case ContentType.midi: return "midi"
        case ContentType.mid: return "mid"
        case ContentType.smf: return "smf"
        case ContentType.m1a: return "m1a"
        case ContentType.m2a: return "m2a"
        case ContentType.mp2: return "mp2"
        case ContentType.mp3: return "mp3"
        case ContentType.mpa: return "mpa"
        case ContentType.mpega: return "mpega"
        case ContentType.rpm: return "rpm"
        
        case ContentType.swa: return "swa"
        case ContentType.dcr: return "dcr"
        case ContentType.dir: return "dir"
        case ContentType.dxr: return "dxr"
        case ContentType.vqf: return "vqf"
        case ContentType.wav: return "wav"
        
        
        case ContentType.aab: return "aab"
        case ContentType.aam: return "aam"
        case ContentType.aas: return "aas"
        case ContentType.asf: return "asf"
        case ContentType.avi: return "avi"
        case ContentType.flc: return "flc"
        case ContentType.fli: return "fli"
        case ContentType.moov: return "moov"
        case ContentType.mov: return "mov"
        case ContentType.qt: return "qt"
        
        case ContentType.mpeg: return "mpeg"
        case ContentType.mpg: return "mpg"
        case ContentType.mpe: return "mpe"
        case ContentType.mpv: return "mpv"
        case ContentType.mng: return "mng"
        case ContentType.m1s: return "m1s"
        case ContentType.m1v: return "m1v"
        // MARK: _2s is equal 2s
        case ContentType._2s: return "2s"
        case ContentType.m2v: return "m2v"
        case ContentType.mp4: return "mp4"
        
        case ContentType.rm: return "rm"
        case ContentType.spl: return "spl"
        case ContentType.swf: return "swf"
        case ContentType.vdo: return "vdo"
        case ContentType.viv: return "viv"
        case ContentType.vivo: return "vivo"
        case ContentType.xdm: return "xdm"
        case ContentType.xdma: return "xdma"
        
        case ContentType.folder: return "folder"
        case ContentType.playlist: return "playlist"
        case ContentType.album: return "album"
        default: return "other"
    }
}

export const transformFromExtension = (extension: string): ContentType => {
    switch (extension) {
        case "txt": return ContentType.txt;
        case "csv": return ContentType.csv;
        case "html": return ContentType.html;
        case "css": return ContentType.css;
        case "javascript": return ContentType.javascript;
        case "exe": return ContentType.exe;
        case "json": return ContentType.json;
        case "pdf": return ContentType.pdf;
        case "xls": return ContentType.xls;
        case "xlsx": return ContentType.xlsx;
        case "ppt": return ContentType.ppt;
        case "pptx": return ContentType.pptx;
        case "doc": return ContentType.doc;
        case "docx": return ContentType.docx;
        
        case "bitmap": return ContentType.bitmap;
        case "svg": return ContentType.svg;
        case "zip": return ContentType.zip;
        case "lzh": return ContentType.lzh;
        case "tar": return ContentType.tar;
        
        case "ai": return ContentType.ai;
        case "bmp": return ContentType.bmp;
        case "rle": return ContentType.rle;
        case "dib": return ContentType.dib;
        case "cgm": return ContentType.cgm;
        case "dwf": return ContentType.dwf;
        case "epsf": return ContentType.epsf;
        case "eps": return ContentType.eps;
        case "ps": return ContentType.ps;
        case "fif": return ContentType.fif;
        case "fpx": return ContentType.fpx;
        case "gif": return ContentType.gif;
        case "jpg": return ContentType.jpg;
        case "jpeg": return ContentType.jpeg;
        case "jpe": return ContentType.jpe;
        case "jfif": return ContentType.jfif;
        case "jfi": return ContentType.jfi;
        case "pcd": return ContentType.pcd;
        case "pict": return ContentType.pict;
        case "pct": return ContentType.pct;
        case "png": return ContentType.png;
        case "tga": return ContentType.tga;
        case "tpic": return ContentType.tpic;
        case "vda": return ContentType.vda;
        case "vst": return ContentType.vst;
        case "tiff": return ContentType.tiff;
        case "tif": return ContentType.tif;
        case "wrl": return ContentType.wrl;
        case "xbm": return ContentType.xbm;
        case "xpm": return ContentType.xpm;
        
        case "aiff": return ContentType.aiff;
        case "aif": return ContentType.aif;
        case "au": return ContentType.au;
        case "snd": return ContentType.snd;
        case "kar": return ContentType.kar;
        case "midi": return ContentType.midi;
        case "mid": return ContentType.mid;
        case "smf": return ContentType.smf;
        case "m1a": return ContentType.m1a;
        case "m2a": return ContentType.m2a;
        case "mp2": return ContentType.mp2;
        case "mp3": return ContentType.mp3;
        case "mpa": return ContentType.mpa;
        case "mpega": return ContentType.mpega;
        case "rpm": return ContentType.rpm;
        
        case "swa": return ContentType.swa;
        case "dcr": return ContentType.dcr;
        case "dir": return ContentType.dir;
        case "dxr": return ContentType.dxr;
        case "vqf": return ContentType.vqf;
        case "wav": return ContentType.wav;
        
        
        case "aab": return ContentType.aab;
        case "aam": return ContentType.aam;
        case "aas": return ContentType.aas;
        case "asf": return ContentType.asf;
        case "avi": return ContentType.avi;
        case "flc": return ContentType.flc;
        case "fli": return ContentType.fli;
        case "moov": return ContentType.moov;
        case "mov": return ContentType.mov;
        case "qt": return ContentType.qt;
        
        case "mpeg": return ContentType.mpeg;
        case "mpg": return ContentType.mpg;
        case "mpe": return ContentType.mpe;
        case "mpv": return ContentType.mpv;
        case "mng": return ContentType.mng;
        case "m1s": return ContentType.m1s;
        case "m1v": return ContentType.m1v;
        // MARK: _2s is equal 2s
        case "2s": return ContentType._2s;
        case "m2v": return ContentType.m2v;
        case "mp4": return ContentType.mp4;
        
        case "rm": return ContentType.rm;
        case "spl": return ContentType.spl;
        case "swf": return ContentType.swf;
        case "vdo": return ContentType.vdo;
        case "viv": return ContentType.viv;
        case "vivo": return ContentType.vivo;
        case "xdm": return ContentType.xdm;
        case "xdma": return ContentType.xdma;

        case "folder": return ContentType.folder;
        case "album": return ContentType.album;
        case "playlist": return ContentType.playlist;
        
        default: return ContentType.other
    }
}

export const isVideo = (contentType: ContentType): boolean => {
    switch (contentType) {
        case ContentType.aab: return true
        case ContentType.aam: return true
        case ContentType.aas: return true
        case ContentType.asf: return true
        case ContentType.avi: return true
        case ContentType.flc: return true
        case ContentType.fli: return true
        case ContentType.moov: return true
        case ContentType.mov: return true
        case ContentType.qt: return true
        case ContentType.mpeg: return true
        case ContentType.mpg: return true
        case ContentType.mpe: return true
        case ContentType.mpv: return true
        case ContentType.mng: return true
        case ContentType.m1s: return true
        case ContentType.m1v: return true
        case ContentType._2s: return true
        case ContentType.m2v: return true
        case ContentType.rm: return true
        case ContentType.spl: return true
        case ContentType.swf: return true
        case ContentType.vdo: return true
        case ContentType.viv: return true
        case ContentType.vivo: return true
        case ContentType.xdm: return true
        case ContentType.xdma: return true
        case ContentType.mp4: return true
        default: return false
    }
}

export const isSound = (contentType: ContentType): boolean => {
    switch (contentType) {
        case ContentType.aiff: return true
        case ContentType.aif: return true
        case ContentType.au: return true
        case ContentType.snd: return true
        case ContentType.kar: return true
        case ContentType.midi: return true
        case ContentType.mid: return true
        case ContentType.smf: return true
        case ContentType.m1a: return true
        case ContentType.m2a: return true
        case ContentType.mp2: return true
        case ContentType.mp3: return true
        case ContentType.mpa: return true
        case ContentType.mpega: return true
        case ContentType.rpm: return true
        case ContentType.swa: return true
        case ContentType.dcr: return true
        case ContentType.dir: return true
        case ContentType.dxr: return true
        case ContentType.vqf: return true
        case ContentType.wav: return true
        default: return false
    }
}

export const isImage = (contentType: ContentType): boolean => {
    switch (contentType) {
        case ContentType.ai: return true
        case ContentType.bmp: return true
        case ContentType.rle: return true
        case ContentType.dib: return true
        case ContentType.cgm: return true
        case ContentType.dwf: return true
        case ContentType.epsf: return true
        case ContentType.eps: return true
        case ContentType.ps: return true
        case ContentType.fif: return true
        case ContentType.fpx: return true
        case ContentType.gif: return true
        case ContentType.jpg: return true
        case ContentType.jpeg: return true
        case ContentType.jpe: return true
        case ContentType.jfif: return true
        case ContentType.jfi: return true
        case ContentType.pcd: return true
        case ContentType.pict: return true
        case ContentType.pct: return true
        case ContentType.png: return true
        case ContentType.tga: return true
        case ContentType.tpic: return true
        case ContentType.vda: return true
        case ContentType.vst: return true
        case ContentType.tiff: return true
        case ContentType.tif: return true
        case ContentType.wrl: return true
        case ContentType.xbm: return true
        case ContentType.xpm: return true
        default: return false
    }
}

export const fallbackContentType: ContentType = ContentType.other;