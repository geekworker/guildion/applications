import { ContentType, isImage, isSound, isVideo, FileExtension, transformFromExtension } from "./ContentType";

export const FileType = {
    Video: 'video',
    Music: 'music',
    Image: 'image',
    YouTube: 'youtube',
    Emoji: 'emoji',
    Folder: 'folder',
    Album: 'album',
    Playlist: 'playlist',
    Other: 'other',
} as const;

export type FileType = typeof FileType[keyof typeof FileType];

export const contentTypeToFileType = (contentType: ContentType): FileType => {
    return isImage(contentType) ? FileType.Image : isVideo(contentType) ? FileType.Video : isSound(contentType) ? FileType.Music : contentType === ContentType.folder ? FileType.Folder : contentType === ContentType.playlist ? FileType.Playlist : contentType === ContentType.album ? FileType.Album : FileType.Other; 
};

export const extensionToFileType = (extension: FileExtension): FileType => {
    return contentTypeToFileType(transformFromExtension(extension)); 
};