export const AccessTokenStatus = {
    NEW: 'NEW',
    ACTIVE: 'ACTIVE',
    ONETIME: 'ONETIME',
    EXPIRED: 'EXPIRED',
} as const;

export type AccessTokenStatus = typeof AccessTokenStatus[keyof typeof AccessTokenStatus];