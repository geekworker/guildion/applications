export type TimeZoneSet = {
    id: string,
    option: string,
}

export const TimeZone = {
    InternationalDateLineWest: {
        id: 'Etc/GMT+12',
        option: 'UTC-12:00',
    },
    CoordinatedUniversalTime11: {
        id: 'Etc/GMT+11',
        option: 'UTC-11:00',
    },
    Hawaii: {
        id: 'Pacific/Honolulu',
        option: 'UTC-10:00',
    },
    Alaska: {
        id: 'America/Anchorage',
        option: 'UTC-09:00',
    },
    BajaCalifornia: {
        id: 'America/Santa_Isabel',
        option: 'UTC-08:00',
    },
    PacificTime: {
        id: 'America/Los_Angeles',
        option: 'UTC-08:00',
    },
    Chihuahua: {
        id: 'America/Chihuahua',
        option: 'UTC-07:00',
    },
    Mazatlan: {
        id: 'America/Chihuahua',
        option: 'UTC-07:00',
    },
    Arizona: {
        id: 'America/Phoenix',
        option: 'UTC-07:00',
    },
    MountainTime: {
        id: 'America/Denver',
        option: 'UTC-07:00',
    },
    CentralAmerica: {
        id: 'America/Guatemala',
        option: 'UTC-06:00',
    },
    CentralTime: {
        id: 'America/Chicago',
        option: 'UTC-06:00',
    },
    CentralTimeUS: {
        id: 'America/Chicago',
        option: 'UTC-06:00',
    },
    CentralTimeCanada: {
        id: 'America/Chicago',
        option: 'UTC-06:00',
    },
    Saskatchewan: {
        id: 'America/Regina',
        option: 'UTC-06:00',
    },
    Guadalajara: {
        id: 'America/Mexico_City',
        option: 'UTC-06:00',
    },
    MexicoCity: {
        id: 'America/Mexico_City',
        option: 'UTC-06:00',
    },
    Monterey: {
        id: 'America/Mexico_City',
        option: 'UTC-06:00',
    },
    Bogota: {
        id: 'America/Bogota',
        option: 'UTC-05:00',
    },
    Lima: {
        id: 'America/Bogota',
        option: 'UTC-05:00',
    },
    Quito: {
        id: 'America/Bogota',
        option: 'UTC-05:00',
    },
    IndianaEast: {
        id: 'America/Indiana/Indianapolis',
        option: 'UTC-05:00',
    },
    EasternTime: {
        id: 'America/New_York',
        option: 'UTC-05:00',
    },
    Caracas: {
        id: 'America/Caracas',
        option: 'UTC-04:30',
    },
    AtlanticTime: {
        id: 'America/Halifax',
        option: 'UTC-04:00',
    },
    Asuncion: {
        id: 'America/Asuncion',
        option: 'UTC-04:00',
    },
    Georgetown: {
        id: 'America/La_Paz',
        option: 'UTC-04:00',
    },
    LaPaz: {
        id: 'America/La_Paz',
        option: 'UTC-04:00',
    },
    Manaus: {
        id: 'America/La_Paz',
        option: 'UTC-04:00',
    },
    SanJuan: {
        id: 'America/La_Paz',
        option: 'UTC-04:00',
    },
    Cuiaba: {
        id: 'America/Cuiaba',
        option: 'UTC-04:00',
    },
    Santiago: {
        id: 'America/Santiago',
        option: 'UTC-04:00',
    },
    Newfoundland: {
        id: 'America/St_Johns',
        option: 'UTC-03:30',
    },
    Brasilia: {
        id: 'America/Sao_Paulo',
        option: 'UTC-03:00',
    },
    Greenland: {
        id: 'America/Godthab',
        option: 'UTC-03:00',
    },
    Cayenne: {
        id: 'America/Cayenne',
        option: 'UTC-03:00',
    },
    Fortaleza: {
        id: 'America/Cayenne',
        option: 'UTC-03:00',
    },
    BuenosAires: {
        id: 'America/Argentina/Buenos_Aires',
        option: 'UTC-03:00',
    },
    Montevideo: {
        id: 'America/Montevideo',
        option: 'UTC-03:00',
    },
    CoordinatedUniversalTime2: {
        id: 'Etc/GMT+2',
        option: 'UTC-02:00',
    },
    CapeVerde: {
        id: 'Atlantic/Cape_Verde',
        option: 'UTC-01:00',
    },
    Azores: {
        id: 'Atlantic/Azores',
        option: 'UTC-01:00',
    },
    Casablanca: {
        id: 'Africa/Casablanca',
        option: 'UTC+00:00',
    },
    Monrovia: {
        id: 'Atlantic/Reykjavik',
        option: 'UTC+00:00',
    },
    Reykjavik: {
        id: 'Atlantic/Reykjavik',
        option: 'UTC+00:00',
    },
    Dublin: {
        id: 'Europe/London',
        option: 'UTC+00:00',
    },
    Edinburgh: {
        id: 'Europe/London',
        option: 'UTC+00:00',
    },
    Lisbon: {
        id: 'Europe/London',
        option: 'UTC+00:00',
    },
    London: {
        id: 'Europe/London',
        option: 'UTC+00:00',
    },
    CoordinatedUniversalTime: {
        id: 'Etc/GMT',
        option: 'UTC+00:00',
    },
    Amsterdam: {
        id: 'Europe/Berlin',
        option: 'UTC+01:00',
    },
    Berlin: {
        id: 'Europe/Berlin',
        option: 'UTC+01:00',
    },
    Bern: {
        id: 'Europe/Berlin',
        option: 'UTC+01:00',
    },
    Rome: {
        id: 'Europe/Berlin',
        option: 'UTC+01:00',
    },
    Stockholm: {
        id: 'Europe/Berlin',
        option: 'UTC+01:00',
    },
    Vienna: {
        id: 'Europe/Berlin',
        option: 'UTC+01:00',
    },
    Brussels: {
        id: 'Europe/Paris',
        option: 'UTC+01:00',
    },
    Copenhagen: {
        id: 'Europe/Paris',
        option: 'UTC+01:00',
    },
    Madrid: {
        id: 'Europe/Paris',
        option: 'UTC+01:00',
    },
    Paris: {
        id: 'Europe/Paris',
        option: 'UTC+01:00',
    },
    WestCentralAfrica: {
        id: 'Africa/Lagos',
        option: 'UTC+01:00',
    },
    Belgrade: {
        id: 'Europe/Budapest',
        option: 'UTC+01:00',
    },
    Bratislava: {
        id: 'Europe/Budapest',
        option: 'UTC+01:00',
    },
    Budapest: {
        id: 'Europe/Budapest',
        option: 'UTC+01:00',
    },
    Ljubljana: {
        id: 'Europe/Budapest',
        option: 'UTC+01:00',
    },
    Prague: {
        id: 'Europe/Budapest',
        option: 'UTC+01:00',
    },
    Sarajevo: {
        id: 'Europe/Warsaw',
        option: 'UTC+01:00',
    },
    Skopje: {
        id: 'Europe/Warsaw',
        option: 'UTC+01:00',
    },
    Warsaw: {
        id: 'Europe/Warsaw',
        option: 'UTC+01:00',
    },
    Zagreb: {
        id: 'Europe/Warsaw',
        option: 'UTC+01:00',
    },
    Windhoek: {
        id: 'Africa/Windhoek',
        option: 'UTC+01:00',
    },
    Athens: {
        id: 'Europe/Istanbul',
        option: 'UTC+02:00',
    },
    Bucharest: {
        id: 'Europe/Istanbul',
        option: 'UTC+02:00',
    },
    Istanbul: {
        id: 'Europe/Istanbul',
        option: 'UTC+02:00',
    },
    Helsinki: {
        id: 'Europe/Kiev',
        option: 'UTC+02:00',
    },
    Kiev: {
        id: 'Europe/Kiev',
        option: 'UTC+02:00',
    },
    Riga: {
        id: 'Europe/Kiev',
        option: 'UTC+02:00',
    },
    Sofia: {
        id: 'Europe/Kiev',
        option: 'UTC+02:00',
    },
    Tallinn: {
        id: 'Europe/Kiev',
        option: 'UTC+02:00',
    },
    Vilnius: {
        id: 'Europe/Kiev',
        option: 'UTC+02:00',
    },
    Cairo: {
        id: 'Africa/Cairo',
        option: 'UTC+02:00',
    },
    Damascus: {
        id: 'Asia/Damascus',
        option: 'UTC+02:00',
    },
    Amman: {
        id: 'Asia/Amman',
        option: 'UTC+02:00',
    },
    Harare: {
        id: 'Africa/Johannesburg',
        option: 'UTC+02:00',
    },
    Pretoria: {
        id: 'Africa/Johannesburg',
        option: 'UTC+02:00',
    },
    Jerusalem: {
        id: 'Asia/Jerusalem',
        option: 'UTC+02:00',
    },
    Beirut: {
        id: 'Asia/Beirut',
        option: 'UTC+02:00',
    },
    Baghdad: {
        id: 'Asia/Baghdad',
        option: 'UTC+03:00',
    },
    Minsk: {
        id: 'Europe/Minsk',
        option: 'UTC+03:00',
    },
    Kuwait: {
        id: 'Asia/Riyadh',
        option: 'UTC+03:00',
    },
    Riyadh: {
        id: 'Asia/Riyadh',
        option: 'UTC+03:00',
    },
    Nairobi: {
        id: 'Africa/Nairobi',
        option: 'UTC+03:00',
    },
    Tehran: {
        id: 'Asia/Tehran',
        option: 'UTC+03:30',
    },
    Moscow: {
        id: 'Europe/Moscow',
        option: 'UTC+04:00',
    },
    St_Petersburg: {
        id: 'Europe/Moscow',
        option: 'UTC+04:00',
    },
    Volgograd: {
        id: 'Europe/Moscow',
        option: 'UTC+04:00',
    },
    Tbilisi: {
        id: 'Asia/Tbilisi',
        option: 'UTC+04:00',
    },
    Yerevan: {
        id: 'Asia/Yerevan',
        option: 'UTC+04:00',
    },
    AbuDhabi: {
        id: 'Asia/Dubai',
        option: 'UTC+04:00',
    },
    Muscat: {
        id: 'Asia/Dubai',
        option: 'UTC+04:00',
    },
    Baku: {
        id: 'Asia/Baku',
        option: 'UTC+04:00',
    },
    PortLouis: {
        id: 'Indian/Mauritius',
        option: 'UTC+04:00',
    },
    Kabul: {
        id: 'Asia/Kabul',
        option: 'UTC+04:30',
    },
    Tashkent: {
        id: 'Asia/Tashkent',
        option: 'UTC+05:00',
    },
    Islamabad: {
        id: 'Asia/Karachi',
        option: 'UTC+05:00',
    },
    Karachi: {
        id: 'Asia/Karachi',
        option: 'UTC+05:00',
    },
    SriJayewardenepuraKotte	: {
        id: 'Asia/Colombo',
        option: 'UTC+05:30',
    },
    Chennai: {
        id: 'Asia/Kolkata',
        option: 'UTC+05:30',
    },
    Kolkata: {
        id: 'Asia/Kolkata',
        option: 'UTC+05:30',
    },
    Mumbai: {
        id: 'Asia/Kolkata',
        option: 'UTC+05:30',
    },
    NewDelhi: {
        id: 'Asia/Kolkata',
        option: 'UTC+05:30',
    },
    Kathmandu: {
        id: 'Asia/Kathmandu',
        option: 'UTC+05:45',
    },
    Astana: {
        id: 'Asia/Almaty',
        option: 'UTC+06:00',
    },
    Dhaka: {
        id: 'Asia/Dhaka',
        option: 'UTC+06:00',
    },
    Yekaterinburg: {
        id: 'Asia/Yekaterinburg',
        option: 'UTC+06:00',
    },
    Yangon: {
        id: 'Asia/Yangon',
        option: 'UTC+06:30',
    },
    Bangkok: {
        id: 'Asia/Bangkok',
        option: 'UTC+07:00',
    },
    Hanoi: {
        id: 'Asia/Bangkok',
        option: 'UTC+07:00',
    },
    Jakarta: {
        id: 'Asia/Bangkok',
        option: 'UTC+07:00',
    },
    Novosibirsk: {
        id: 'Asia/Novosibirsk',
        option: 'UTC+07:00',
    },
    Krasnoyarsk: {
        id: 'Asia/Krasnoyarsk',
        option: 'UTC+08:00',
    },
    Ulaanbaatar: {
        id: 'Asia/Ulaanbaatar',
        option: 'UTC+08:00',
    },
    Beijing: {
        id: 'Asia/Shanghai',
        option: 'UTC+08:00',
    },
    Chongqing: {
        id: 'Asia/Shanghai',
        option: 'UTC+08:00',
    },
    HongKong: {
        id: 'Asia/Shanghai',
        option: 'UTC+08:00',
    },
    Urumqi: {
        id: 'Asia/Shanghai',
        option: 'UTC+08:00',
    },
    Perth: {
        id: 'Australia/Perth',
        option: 'UTC+08:00',
    },
    KualaLumpur: {
        id: 'Asia/Singapore',
        option: 'UTC+08:00',
    },
    Singapore: {
        id: 'Asia/Singapore',
        option: 'UTC+08:00',
    },
    Taipei: {
        id: 'Asia/Taipei',
        option: 'UTC+08:00',
    },
    Irkutsk: {
        id: 'Asia/Irkutsk',
        option: 'UTC+09:00',
    },
    Seoul: {
        id: 'Asia/Seoul',
        option: 'UTC+09:00',
    },
    Osaka: {
        id: 'Asia/Tokyo',
        option: 'UTC+09:00',
    },
    Sapporo: {
        id: 'Asia/Tokyo',
        option: 'UTC+09:00',
    },
    Tokyo: {
        id: 'Asia/Tokyo',
        option: 'UTC+09:00',
    },
    Darwin: {
        id: 'Australia/Darwin',
        option: 'UTC+09:30',
    },
    Adelaide: {
        id: 'Australia/Adelaide',
        option: 'UTC+09:30',
    },
    Hobart: {
        id: 'Australia/Hobart',
        option: 'UTC+10:00',
    },
    Yakutsk: {
        id: 'Asia/Yakutsk',
        option: 'UTC+10:00',
    },
    Brisbane: {
        id: 'Australia/Brisbane',
        option: 'UTC+10:00',
    },
    Guam: {
        id: 'Pacific/Port_Moresby',
        option: 'UTC+10:00',
    },
    PortMoresby: {
        id: 'Pacific/Port_Moresby',
        option: 'UTC+10:00',
    },
    Canberra: {
        id: 'Australia/Sydney',
        option: 'UTC+10:00',
    },
    Melbourne: {
        id: 'Australia/Sydney',
        option: 'UTC+10:00',
    },
    Sydney: {
        id: 'Australia/Sydney',
        option: 'UTC+10:00',
    },
    Vladivostok: {
        id: 'Asia/Vladivostok',
        option: 'UTC+11:00',
    },
    SolomonIslands: {
        id: 'Pacific/Guadalcanal',
        option: 'UTC+11:00',
    },
    NewCaledonia: {
        id: 'Pacific/Guadalcanal',
        option: 'UTC+11:00',
    },
    CoordinatedUniversalTime12: {
        id: 'Etc/GMT-12',
        option: 'UTC+12:00',
    },
    Fiji: {
        id: 'Pacific/Fiji',
        option: 'UTC+12:00',
    },
    MarshallIslands: {
        id: 'Pacific/Fiji',
        option: 'UTC+12:00',
    },
    Magadan: {
        id: 'Asia/Magadan',
        option: 'UTC+12:00',
    },
    Auckland: {
        id: 'Pacific/Auckland',
        option: 'UTC+12:00',
    },
    Wellington: {
        id: 'Pacific/Auckland',
        option: 'UTC+12:00',
    },
    Nukualofa	: {
        id: 'Pacific/Tongatapu',
        option: 'UTC+13:00',
    },
    Samoa: {
        id: 'Pacific/Apia',
        option: 'UTC+13:00',
    },
} as const;

export type TimeZone = typeof TimeZone[keyof typeof TimeZone];

export const TimeZoneID = Object.keys(TimeZone).map((key) => { return { [key as keyof typeof TimeZone]: TimeZone[key as keyof typeof TimeZone].id } });
export type TimeZoneID = TimeZone['id'];

export const fallbackTimeZone: TimeZone = TimeZone.Tokyo;
export const fallbackTimeZoneID: TimeZoneID = fallbackTimeZone.id;