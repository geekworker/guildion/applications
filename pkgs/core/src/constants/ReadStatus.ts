export const ReadStatus = {
    NEW: 'NEW',
    CREATED: 'CREATED',
    NOTIFIED: 'NOTIFIED',
    CHECKED: 'CHECKED',
};

export type ReadStatus = typeof ReadStatus[keyof typeof ReadStatus];