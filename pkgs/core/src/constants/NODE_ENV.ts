import { CURRENT_NODE_ENV } from "./Configure";

export const NODE_ENV = {
    DEVELOPMENT: 'development',
    STAGING: 'staging',
    PRODUCTION: 'production',
    TEST: 'test',
    STORYBOOK: 'storybook',
} as const

export type NODE_ENV = typeof NODE_ENV[keyof typeof NODE_ENV];

export const GET_NODE_ENV = (nodeEnv: string): NODE_ENV => {
    const results = Object.values(NODE_ENV).filter((v: string) => v == nodeEnv)
    return results.length > 0 ? results[0] : NODE_ENV.DEVELOPMENT
}

export const IS_DEV = (): boolean => CURRENT_NODE_ENV() == NODE_ENV.DEVELOPMENT;
export const IS_STAGING = (): boolean => CURRENT_NODE_ENV() == NODE_ENV.STAGING;
export const IS_PROD = (): boolean => CURRENT_NODE_ENV() == NODE_ENV.PRODUCTION;
export const IS_TEST = (): boolean => CURRENT_NODE_ENV() == NODE_ENV.TEST;
export const IS_STORYBOOK = (): boolean => CURRENT_NODE_ENV() == NODE_ENV.STORYBOOK;