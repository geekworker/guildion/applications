export const ErrorType = {
    global: 'global',
    accountUsername: 'account_username',
};

export type ErrorType = typeof ErrorType[keyof typeof ErrorType];