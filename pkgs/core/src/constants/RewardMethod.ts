export const RewardMethod = {
    WISE: 'WISE',
} as const;

export type RewardMethod = typeof RewardMethod[keyof typeof RewardMethod];