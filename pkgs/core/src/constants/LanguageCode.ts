export const LanguageCode = {
    Abkhazian: 'ab',
    Afar: 'aa',
    Afrikaans: 'af',
    Albanian: 'sq',
    Amharic: 'am',
    Arabic: 'ar',
    Armenian: 'hy',
    Assamese: 'as',
    Aymara: 'ay',
    Azerbaijani: 'az',
    Bashkir: 'ba',
    Basque: 'eu',
    Bangla: 'bn',
    Bengali: 'bn',
    Bhutani: 'dz',
    Bihari: 'bh',
    Bislama: 'bi',
    Breton: 'br',
    Bulgarian: 'bg',
    Burmese: 'my',
    Byelorussian: 'be',
    Cambodian: 'km',
    Catalan: 'ca',
    Chinese: 'zh',
    Corsican: 'co',
    Croatian: 'hr',
    Czech: 'cs',
    Danish: 'da',
    Dutch: 'nl',
    English: 'en',
    Esperanto: 'eo',
    Estonian: 'et',
    Faeroese: 'fo',
    Farsi: 'fa',
    Fiji: 'fj',
    Finnish: 'fi',
    French: 'fr',
    Frisian: 'fy',
    Galician: 'gl',
    GaelicScottish: 'gd',
    GaelicManx: 'gv',
    Georgian: 'ka',
    German: 'de',
    Greek: 'el',
    Greenlandic: 'kl',
    Guarani: 'gn',
    Gujarati: 'gu',
    Hausa: 'ha',
    Hebrew: 'he',
    Hindi: 'hi',
    Hungarian: 'hu',
    Icelandic: 'is',
    Indonesian: 'id',
    Interlingua: 'ia',
    Interlingue: 'ie',
    Inuktitut: 'iu',
    Inupiak: 'ik',
    Irish: 'ga',
    Italian: 'it',
    Japanese: 'ja',
    Javanese: 'ja',
    Kannada: 'kn',
    Kashmiri: 'ks',
    Kazakh: 'kk',
    Kinyarwanda: 'rw',
    Ruanda: 'rw',
    Kirghiz: 'ky',
    Kirundi: 'rn',
    Rundi: 'rn',
    Korean: 'ko',
    Kurdish: 'ku',
    Laothian: 'lo',
    Latin: 'la',
    Latvian: 'lv',
    Lettish: 'lv',
    Limburgish : 'li',
    Limburger: 'li',
    Lingala: 'ln',
    Lithuanian: 'lt',
    Macedonian: 'mk',
    Malagasy: 'mg',
    Malay: 'ms',
    Malayalam: 'ml',
    Maltese: 'mt',
    Maori: 'mi',
    Marathi: 'mr',
    Moldavian: 'mo',
    Mongolian: 'mn',
    Nauru: 'na',
    Nepali: 'ne',
    Norwegian: 'no',
    Occitan: 'oc',
    Oriya: 'or',
    Oromo: 'om',
    Afan: 'om',
    Galla: 'om',
    Pashto: 'ps',
    Pushto: 'ps',
    Polish: 'pl',
    Portuguese: 'pt',
    Punjabi: 'pa',
    Quechua: 'qu',
    RhaetoRomance: 'rm',
    Romanian: 'ro',
    Russian: 'ru',
    Samoan: 'sm',
    Sangro: 'sg',
    Sanskrit: 'sa',
    Serbian: 'sr',
    SerboCroatian: 'sh',
    Sesotho: 'st',
    Setswana: 'tn',
    Shona: 'sn',
    Sindhi: 'sd',
    Sinhalese: 'si',
    Siswati: 'ss',
    Slovak: 'sk',
    Slovenian: 'sl',
    Somali: 'so',
    Spanish: 'es',
    Sundanese: 'su',
    Swahili: 'sw',
    Kiswahili: 'sw',
    Swedish: 'sv',
    Tagalog: 'tl',
    Tajik: 'tg',
    Tamil: 'ta',
    Tatar: 'tt',
    Telugu: 'te',
    Thai: 'th',
    Tibetan: 'bo',
    Tigrinya: 'ti',
    Tonga: 'to',
    Tsonga: 'ts',
    Turkish: 'tr',
    Turkmen: 'tk',
    Twi: 'tw',
    Uighur: 'ug',
    Ukrainian: 'uk',
    Urdu: 'ur',
    Uzbek: 'uz',
    Vietnamese: 'vi',
    Volapük: 'vo',
    Welsh: 'cy',
    Wolof: 'wo',
    Xhosa: 'xh',
    Yiddish: 'yi',
    Yoruba: 'yo',
    Zulu: 'zu',
} as const;

export type LanguageCode = typeof LanguageCode[keyof typeof LanguageCode];

export const fallbackLanguageCode: LanguageCode = LanguageCode.English;