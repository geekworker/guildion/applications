// https://github.com/cubicdaiya/ngx_small_light#parameters-for-small_light-function

import { CDN_PROXY_URL_STRING, PCDN_PROXY_URL_STRING } from "./ApplicationConfig";

export interface ImgTransformation {
    p?: string,
    e?: string,
    q?: number,
    of?: 'png' | 'jpg' | 'webp' | 'gif',
    jpeghint?: string,
    dw?: string,
    dh?: string,
    dx?: string,
    dy?: string,
    da?: string,
    ds?: string,
    cw?: number,
    ch?: number,
    cc?: string,
    bw?: number,
    bh?: number,
    bc?: string,
    sw?: string,
    sh?: string,
    sx?: string,
    sy?: string,
    pt?: string,
    sharpen?: string,
    unsharp?: string,
    blur?: string,
    embedicon?: string,
    ix?: number,
    iy?: number,
    angle?: number,
    progressive?: string,
    cmyk2rgb?: string,
    rmprof?: string,
    autoorient?: string,
};

export const generateTransformPath = (transformations: ImgTransformation) => {
    return `small_light(${
        Object.keys(transformations)
            .filter(k => !!transformations[k as keyof ImgTransformation])
            .map(k => `${k}=${transformations[k as keyof ImgTransformation]}`)
            .join(',')
    })`;
};

export const smallLightRegExp = /\/small_light[^/]*\//;

export const makeTransformSrc = (src: string, transformations: ImgTransformation) => {
    const transformedPath = generateTransformPath(transformations);
    if (src.startsWith(CDN_PROXY_URL_STRING)) {
        const baseSrc = src
            .replace(`${CDN_PROXY_URL_STRING}/`, '')
            .replace(smallLightRegExp, '');
        return `${CDN_PROXY_URL_STRING}/${transformedPath}/${baseSrc}`;
    } else if (src.startsWith(PCDN_PROXY_URL_STRING)) {
        const baseSrc = src
            .replace(`${PCDN_PROXY_URL_STRING}/`, '')
            .replace(smallLightRegExp, '');
        return `${PCDN_PROXY_URL_STRING}/${transformedPath}/${baseSrc}`;
    } else {
        return src;
    }
}