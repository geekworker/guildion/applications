export const safeJSONParse = <T>(json: string | string[]): T | undefined => {
    try {
        const result = JSON.parse(String(json));
        return result as T;
    } catch {
        return undefined;
    }
};