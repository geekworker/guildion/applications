export type PromiseResolve<T> = (value: T | PromiseLike<T>) => void;
export type PromiseReject<T> = (reason?: any) => void;


export const hasErrorPromise = async (promise: Promise<any>): Promise<boolean> => {
    try {
        await promise.catch();
        return false;
    } catch {
        return true;
    }
}