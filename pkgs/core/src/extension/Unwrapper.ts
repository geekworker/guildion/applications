export namespace Unwrapper {
    export const isString = (value: any): value is string => typeof value == 'string';
    export const isBoolean = (value: any): value is boolean => value === true || value === false;
    export const isTrue = (value: any): value is boolean => value === true && value !== false;
    export const isFalse = (value: any): value is boolean => value !== true && value === false;
}