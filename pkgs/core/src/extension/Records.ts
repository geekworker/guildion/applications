import { RecordClass, RecordsClass } from '@guildion/core/src/types/RecordClass';
import { isRecord, List, Record } from 'immutable';

export class Records<T extends Record<ReturnType<T['toJSON']>>> extends Array<T> {
    private value: List<T>;
    private Record: RecordClass<T>;
    private Records: RecordsClass<Records<T>>;

    constructor(array: Array<ReturnType<T['toJSON']> | T>, options: { Record: RecordClass<T>, Records: RecordsClass<Records<T>> }) {
        const elements = array
            .map(ele => isRecord(ele) ? ele : new options.Record(ele))
            .filter((ele): ele is T => isRecord(ele));
        super(...elements);
        this.value = List(elements);
        this.Record = options.Record;
        this.Records = options.Records;
        Object.freeze(this);
    }


    get size(): number {
        return this.value.size;
    }

    set(index: number, element: T): Records<T> {
        const result = this.value.set(index, element)
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }

    get(index: number): T | undefined {
        return this.value.get(index);
    }

    has(index: number): boolean {
        return this.value.has(index);
    }

    includes(element: T): boolean {
        return this.value.includes(element);
    }

    get first(): T | undefined {
        return this.value.first<T>();
    }
    
    get last(): T | undefined {
        return this.value.last<T>();
    }

    append(element: T): Records<T> {
        return new this.Records(this.value.push(element).toArray(), { Record: this.Record, Records: this.Records })
    }

    insert(index: number, element: T): Records<T> {
        return new this.Records(this.value.insert(index, element).toArray(), { Record: this.Record, Records: this.Records });
    }

    remove(index: number): Records<T> {
        return new this.Records(this.value.remove(index).toArray(), { Record: this.Record, Records: this.Records });
    }

    delete(index: number): Records<T> {
        const result = this.value.delete(index)
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }

    removeLast(): Records<T> {
        const result = this.value.pop();
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }

    removeFirst(): Records<T> {
        const result = this.value.shift();
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }

    insertsFirst(...elements: T[]): Records<T> {
        const result = this.value.unshift(...elements)
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }

    updateBy(index: number, updater: (value: T | undefined) => T): Records<T> {
        const result = this.value.update(index, updater);
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }

    update(index: number, updater: (value: T | undefined) => T): Records<T> {
        const result = this.value.update(index, updater);
        return new this.Records(result.toArray(), { Record: this.Record, Records: this.Records });
    }
    
    setSize(size: number): Records<T> {
        const result = this.value.setSize(size)
        return new this.Records(result.toArray());
    }

    setIn(keyPath: Iterable<any>, value: any): Records<T> {
        const result = this.value.setIn(keyPath, value);
        return new this.Records(result.toArray());
    }

    deleteIn(keyPath: Iterable<any>): Records<T> {
        const result = this.value.deleteIn(keyPath);
        return new this.Records(result.toArray());
    }

    getIn(keyPath: Iterable<any>, notSetValue?: any): any {
        return this.value.getIn(keyPath, notSetValue);
    }

    hasIn(keyPath: Iterable<any>): boolean {
        return this.value.hasIn(keyPath);
    }

    updateIn(
        keyPath: Iterable<any>,
        updater: (value: any) => any
    ): Records<T> {
        const result = this.value.updateIn(keyPath, updater);
        return new this.Records(result.toArray());
    }

    mergeIn(keyPath: Iterable<any>, ...collections: Array<any>): Records<T> {
        const result = this.value.mergeIn(keyPath, ...collections);
        return new this.Records(result.toArray());
    }

    mergeDeepIn(keyPath: Iterable<any>, ...collections: Array<any>): Records<T> {
        const result = this.value.mergeDeepIn(keyPath, ...collections);
        return new this.Records(result.toArray());
    }

    toArray(): T[] {
        return this.value.toArray();
    }

    toList(): List<T> {
        return this.value;
    }

    toJSON(): Array<ReturnType<T['toJSON']>> {
        return this.value.toArray().map(v => v.toJSON());
    }

    toObject(): { [key: string]: T } {
        return this.value.toObject();
    }

    toJS(): Array<T> {
        return this.value.toArray();
    }

    indexOf(searchValue: T): number {
        return this.value.indexOf(searchValue);
    }

    lastIndexOf(searchValue: T): number {
        return this.value.lastIndexOf(searchValue);
    }

    findIndex(predicate: (value: T, index: number, iter: any) => boolean, context?: unknown): number {
        return this.value.findIndex(predicate, context)
    }

    findLastIndex(predicate: (value: T, index: number, iter: List<T>) => boolean, context?: unknown): number {
        return this.value.findLastIndex(predicate, context)
    }

    select(predicate: (value: T, index: number, iter: List<T>) => boolean, context?: unknown): T | undefined {
        const result = this.value.find(predicate, context);
        return result ? result : undefined;
    }

    equalTo(target: Records<T>): boolean{
        return this.value == target.value;
    }

    concat(records: Records<T>): Records<T> {
        return new Records(this.value.concat(records.toList()).toArray(), { Record: this.Record, Records: this.Records })
    };
}

export const isRecords = <T>(val: any): val is Records<T | any> => val instanceof Records;
export type ObjectOfRecord<T extends Record<any>> = T extends Record<infer V> ? V : any;
export type ElementOfRecords<T extends Records<any>> = T extends Records<infer V> ? V : any;
export type ElementObjectOfRecords<T extends Records<any>> = T extends Records<infer V> ? ObjectOfRecord<V> : any;
export type ArrayOfRecord<T extends Records<any>> = T extends Records<infer V> ? Array<V> : any;
export type ArrayObjectOfRecord<T extends Records<any>> = T extends Records<infer V> ? Array<ObjectOfRecord<V>> : any;