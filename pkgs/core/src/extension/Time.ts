export function formatMs(ms: number) {
    const minutes = Math.floor(ms / 60000);
    const seconds = ((ms % 60000) / 1000).toFixed(0);
    const hours = minutes % 60;
    return minutes > 60 ?
    `${(hours < 10 ? "0" : "")}${hours}:${minutes % 60}:${(Number(seconds) < 10 ? "0" : "")}${seconds}` :
    `${minutes}:${(Number(seconds) < 10 ? "0" : "")}${seconds}`;
}