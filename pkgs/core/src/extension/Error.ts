import { ErrorType } from "../constants/ErrorType";

export class AppError extends Error {
    public isAppError: boolean = true;
    public type: ErrorType = ErrorType.global

    constructor(message: string, options?: { type?: ErrorType }) {
        super(message);
        Object.setPrototypeOf(this, AppError.prototype);
        if (options?.type) this.type = options.type;
    }
}

export const isAppError = (payload: any): payload is AppError => (typeof payload === 'object') && (payload.isAppError === true);

export class NetworkError extends Error {
    public isNetworkError: boolean = true;

    constructor(message: string = 'Network Error') {
        super(message);
        Object.setPrototypeOf(this, AppError.prototype);
    }
}

export const isNetworkError = (payload: any): payload is NetworkError => (typeof payload === 'object') && (payload.isNetworkError === true);