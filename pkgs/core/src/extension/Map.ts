import Immutable from 'immutable'

export interface ImmutableMap<T> extends Immutable.Map<string, any> {
    get<K extends keyof T> (name: K): T[K];
}