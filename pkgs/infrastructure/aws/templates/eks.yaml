AWSTemplateFormatVersion: '2010-09-09'
Description: aws-eks

Metadata: 
  "AWS::CloudFormation::Interface":
    ParameterGroups: 
      - Label: 
          default: "Root Stack Name"
        Parameters: 
          - RootStackName

Parameters:
  RootStackName:
    Description: Enter the root stack name for importing them outputs
    Type: String
    MinLength: 1
    MaxLength: 41
    AllowedPattern: ^[a-zA-Z0-9-_]*$
  ClusterName:
    Description: Cluster name of EKS
    Type: String
    Default: eks-cluster
  KeyName:
    Description: The EC2 Key Pair to allow SSH access to the instances
    Type: AWS::EC2::KeyPair::KeyName
  NodeImageId:
    Type: AWS::EC2::Image::Id
    Description: |
      AMI id for the node instances.
      ami-02d2e0c36bbdb4bc3: Asia Pacific (Tokyo) ap-northeast-1.
      ami-0a54c984b9f908c81: US East (Ohio) us-east-2.
      ami-0440e4f6b9713faf6: US East (N. Virginia)	us-east-1.
      ami-0c7a4976cb6fafd3a: EU (Ireland) eu-west-1.
      ami-03d79d440297083e3: Asia Pacific (Tokyo) ap-northeast-1. AmazonLinux2 Kernel5.10 (64-bit x86)
      ami-0feb52c8d51bafff6: Asia Pacific (Tokyo) ap-northeast-1. AmazonLinux2 Kernel5.10 (64-bit Arm)
      ami-0e3861265a3a335cc: Asia Pacific (Tokyo) ap-northeast-1. AmazonLinux2 Kernel4.14 (64-bit x86)
      ami-0b49509d917c6649b: Asia Pacific (Tokyo) ap-northeast-1. AmazonLinux2 Kernel4.14 (64-bit x86)
      ami-0059a1d25f9f5ac82: Asia Pacific (Tokyo) ap-northeast-1. AmazonLinux2 Kernel4.14 (64-bit x86 accelerated)
      ami-0c194b5324b20e5b4: Asia Pacific (Tokyo) ap-northeast-1. AmazonLinux2 Kernel5.10 (64-bit Arm)
    Default: ami-0059a1d25f9f5ac82
    AllowedValues:
      - ami-02d2e0c36bbdb4bc3
      - ami-0a54c984b9f908c81
      - ami-0440e4f6b9713faf6
      - ami-0c7a4976cb6fafd3a
      - ami-03d79d440297083e3
      - ami-0feb52c8d51bafff6
      - ami-0e3861265a3a335cc
      - ami-01835da93183e373a
      - ami-0b49509d917c6649b
      - ami-0059a1d25f9f5ac82
      - ami-0c194b5324b20e5b4
  NodeGroupName:
    Description: Unique identifier for the Node Group.
    Type: String
    Default: node-group
  NodeInstanceType:
    Description: EC2 instance type for the node instances
    Type: String
    Default: t3.small
    AllowedValues:
    - t4g.nano
    - t4g.micro
    - t4g.small
    - t4g.medium
    - t4g.large
    - t4g.xlarge
    - t4g.2xlarge
    - t3.nano
    - t3.micro
    - t3.small
    - t3.medium
    - t3.large
    - t3.xlarge
    - t3.2xlarge
    - t3a.nano
    - t3a.micro
    - t3a.small
    - t3a.medium
    - t3a.large
    - t3a.xlarge
    - t3a.2xlarge
    - t2.small
    - t2.medium
    - t2.large
    - t2.xlarge
    - t2.2xlarge
    - m3.medium
    - m3.large
    - m3.xlarge
    - m3.2xlarge
    - m4.large
    - m4.xlarge
    - m4.2xlarge
    - m4.4xlarge
    - m4.10xlarge
    - m5.large
    - m5.xlarge
    - m5.2xlarge
    - m5.4xlarge
    - m5.12xlarge
    - m5.24xlarge
    - c4.large
    - c4.xlarge
    - c4.2xlarge
    - c4.4xlarge
    - c4.8xlarge
    - c5.large
    - c5.xlarge
    - c5.2xlarge
    - c5.4xlarge
    - c5.9xlarge
    - c5.18xlarge
    - i3.large
    - i3.xlarge
    - i3.2xlarge
    - i3.4xlarge
    - i3.8xlarge
    - i3.16xlarge
    - r3.xlarge
    - r3.2xlarge
    - r3.4xlarge
    - r3.8xlarge
    - r4.large
    - r4.xlarge
    - r4.2xlarge
    - r4.4xlarge
    - r4.8xlarge
    - r4.16xlarge
    - x1.16xlarge
    - x1.32xlarge
    - p2.xlarge
    - p2.8xlarge
    - p2.16xlarge
    - p3.2xlarge
    - p3.8xlarge
    - p3.16xlarge
    ConstraintDescription: Must be a valid EC2 instance type
  SpotNodeInstanceType:
    Description: EC2 spot instance type for the node instances
    Type: String
    Default: t3.large
    AllowedValues:
    - t4g.nano
    - t4g.micro
    - t4g.small
    - t4g.medium
    - t4g.large
    - t4g.xlarge
    - t4g.2xlarge
    - t3.nano
    - t3.micro
    - t3.small
    - t3.medium
    - t3.large
    - t3.xlarge
    - t3.2xlarge
    - t3a.nano
    - t3a.micro
    - t3a.small
    - t3a.medium
    - t3a.large
    - t3a.xlarge
    - t3a.2xlarge
    - t2.small
    - t2.medium
    - t2.large
    - t2.xlarge
    - t2.2xlarge
    - m3.medium
    - m3.large
    - m3.xlarge
    - m3.2xlarge
    - m4.large
    - m4.xlarge
    - m4.2xlarge
    - m4.4xlarge
    - m4.10xlarge
    - m5.large
    - m5.xlarge
    - m5.2xlarge
    - m5.4xlarge
    - m5.12xlarge
    - m5.24xlarge
    - c4.large
    - c4.xlarge
    - c4.2xlarge
    - c4.4xlarge
    - c4.8xlarge
    - c5.large
    - c5.xlarge
    - c5.2xlarge
    - c5.4xlarge
    - c5.9xlarge
    - c5.18xlarge
    - i3.large
    - i3.xlarge
    - i3.2xlarge
    - i3.4xlarge
    - i3.8xlarge
    - i3.16xlarge
    - r3.xlarge
    - r3.2xlarge
    - r3.4xlarge
    - r3.8xlarge
    - r4.large
    - r4.xlarge
    - r4.2xlarge
    - r4.4xlarge
    - r4.8xlarge
    - r4.16xlarge
    - r5.large
    - r5.xlarge
    - r5.2xlarge
    - r5.4xlarge
    - r5.9xlarge
    - r5.18xlarge
    - r5a.large
    - r5a.xlarge
    - r5a.2xlarge
    - r5a.4xlarge
    - r5a.9xlarge
    - r5a.18xlarge
    - r5b.large
    - r5b.xlarge
    - r5b.2xlarge
    - r5b.4xlarge
    - r5b.9xlarge
    - r5b.18xlarge
    - x1.16xlarge
    - x1.32xlarge
    - p2.xlarge
    - p2.8xlarge
    - p2.16xlarge
    - p3.2xlarge
    - p3.8xlarge
    - p3.16xlarge
    ConstraintDescription: Must be a valid EC2 spot instance type
  NodeAutoScalingGroupMinSize:
    Type: Number
    Description: Minimum size of Node Group ASG.
    Default: 1
  NodeAutoScalingGroupMaxSize:
    Type: Number
    Description: Maximum size of Node Group ASG.
    Default: 3
  SpotNodeAutoScalingGroupMinSize:
    Type: Number
    Description: Minimum size of Spot Node Group ASG.
    Default: 2
  SpotNodeAutoScalingGroupMaxSize:
    Type: Number
    Description: Maximum size of Spot Node Group ASG.
    Default: 5
  NodeVolumeSize:
    Type: Number
    Description: Node volume size
    Default: 30
  SpotNodeVolumeSize:
    Type: Number
    Description: Spot Node volume size
    Default: 60
  BootstrapArguments:
    Description: Arguments to pass to the bootstrap script. See files/bootstrap.sh in https://github.com/awslabs/amazon-eks-ami
    Default: ""
    Type: String

Conditions:
  EnableSpot: !Not [!Equals [ !Ref SpotNodeInstanceType, false ]]

Resources:      
  EKSRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service: eks.amazonaws.com
            Action: "sts:AssumeRole"
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonEKSClusterPolicy
        - arn:aws:iam::aws:policy/AmazonEKSServicePolicy
        - arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy
        - arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy
        - arn:aws:iam::aws:policy/CloudWatchLogsFullAccess
      RoleName: eks-cluster-role

  ControlPlaneSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Cluster communication with worker nodes
      VpcId:
        Fn::ImportValue:
          !Sub "${RootStackName}-vpc"

  EKSCluster:
    Type: AWS::EKS::Cluster
    Properties:
      Name: !Sub ${ClusterName}
      RoleArn:
        Fn::GetAtt:
          - EKSRole
          - Arn
      ResourcesVpcConfig:
        SecurityGroupIds:
          - Fn::GetAtt:
            - ControlPlaneSecurityGroup
            - GroupId
        SubnetIds:
          - 
            Fn::ImportValue:
              !Sub "${RootStackName}-public-subnet-a"
          - 
            Fn::ImportValue:
              !Sub "${RootStackName}-public-subnet-c"

  NodeInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: "/"
      Roles:
        - !Ref NodeInstanceRole

  NodeInstanceRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - ec2.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy
        - arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy
        - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly
        - arn:aws:iam::aws:policy/CloudWatchAgentServerPolicy
        - arn:aws:iam::aws:policy/CloudWatchLogsFullAccess

  NodeSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group for all nodes in the cluster
      VpcId:
        Fn::ImportValue:
          !Sub "${RootStackName}-vpc"
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: 0.0.0.0/0
      Tags:
      - Key: !Sub "kubernetes.io/cluster/${ClusterName}"
        Value: 'owned'

  AppSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: SecurityGroup eks pod applications
      VpcId:
        Fn::ImportValue:
          !Sub "${RootStackName}-vpc"
      SecurityGroupIngress:
        - IpProtocol: tcp
          CidrIp: 0.0.0.0/0
          FromPort: 80
          ToPort: 80
        - IpProtocol: tcp
          CidrIp: 0.0.0.0/0
          FromPort: 443
          ToPort: 443
        - IpProtocol: tcp
          FromPort: 22
          ToPort: 22
          CidrIp: 0.0.0.0/0
      Tags: 
        - Key: Name
          Value: !Sub "${RootStackName}-app-security-group"

  NodeSecurityGroupIngress:
    Type: AWS::EC2::SecurityGroupIngress
    DependsOn: NodeSecurityGroup
    Properties:
      Description: Allow node to communicate with each other
      GroupId: !Ref NodeSecurityGroup
      SourceSecurityGroupId: !Ref NodeSecurityGroup
      IpProtocol: '-1'
      FromPort: 0
      ToPort: 65535

  NodeSecurityGroupFromControlPlaneIngress:
    Type: AWS::EC2::SecurityGroupIngress
    DependsOn: NodeSecurityGroup
    Properties:
      Description: Allow worker Kubelets and pods to receive communication from the cluster control plane
      GroupId: !Ref NodeSecurityGroup
      SourceSecurityGroupId: !Ref ControlPlaneSecurityGroup
      IpProtocol: tcp
      FromPort: 1025
      ToPort: 65535

  ControlPlaneEgressToNodeSecurityGroup:
    Type: AWS::EC2::SecurityGroupEgress
    DependsOn: NodeSecurityGroup
    Properties:
      Description: Allow the cluster control plane to communicate with worker Kubelet and pods
      GroupId: !Ref ControlPlaneSecurityGroup
      DestinationSecurityGroupId: !Ref NodeSecurityGroup
      IpProtocol: tcp
      FromPort: 1025
      ToPort: 65535

  NodeSecurityGroupFromControlPlaneOn443Ingress:
    Type: AWS::EC2::SecurityGroupIngress
    DependsOn: NodeSecurityGroup
    Properties:
      Description: Allow pods running extension API servers on port 443 to receive communication from cluster control plane
      GroupId: !Ref NodeSecurityGroup
      SourceSecurityGroupId: !Ref ControlPlaneSecurityGroup
      IpProtocol: tcp
      FromPort: 443
      ToPort: 443

  ControlPlaneEgressToNodeSecurityGroupOn443:
    Type: AWS::EC2::SecurityGroupEgress
    DependsOn: NodeSecurityGroup
    Properties:
      Description: Allow the cluster control plane to communicate with pods running extension API servers on port 443
      GroupId: !Ref ControlPlaneSecurityGroup
      DestinationSecurityGroupId: !Ref NodeSecurityGroup
      IpProtocol: tcp
      FromPort: 443
      ToPort: 443

  ClusterControlPlaneSecurityGroupIngress:
    Type: AWS::EC2::SecurityGroupIngress
    DependsOn: NodeSecurityGroup
    Properties:
      Description: Allow pods to communicate with the cluster API ServerA
      GroupId: !Ref ControlPlaneSecurityGroup
      SourceSecurityGroupId: !Ref NodeSecurityGroup
      IpProtocol: tcp
      ToPort: 443
      FromPort: 443

  SpotNodeLaunchTemplate:
    Type: AWS::EC2::LaunchTemplate
    Properties:
      LaunchTemplateName: !Sub "${ClusterName}-spot-instance-template"
      LaunchTemplateData:
        NetworkInterfaces:
          - DeviceIndex: 0
            AssociatePublicIpAddress: true
            Groups:
            - !GetAtt NodeSecurityGroup.GroupId
        IamInstanceProfile:
          Arn: !GetAtt
            - NodeInstanceProfile
            - Arn
        ImageId: !Ref NodeImageId
        InstanceType: !Ref SpotNodeInstanceType
        KeyName: !Ref KeyName
        BlockDeviceMappings:
          - DeviceName: /dev/xvda
            Ebs:
              VolumeSize: !Ref SpotNodeVolumeSize
              VolumeType: gp2
              DeleteOnTermination: true
        # CreditSpecification:
        #   CpuCredits: unlimited
        # InstanceMarketOptions:
        #   MarketType: spot
        #   SpotOptions:
        #     InstanceInterruptionBehavior: stop
        #     MaxPrice: '0.10' # !Ref 'AWS::NoValue'
        #     SpotInstanceType: persistent
        UserData:
          Fn::Base64:
            !Sub |
              #!/bin/bash
              set -o xtrace
              /etc/eks/bootstrap.sh ${ClusterName} ${BootstrapArguments} --kubelet-extra-args '--node-labels=spot=true'
              /opt/aws/bin/cfn-signal --exit-code $? \
                      --stack  ${AWS::StackName} \
                      --resource NodeGroup  \
                      --region ${AWS::Region}

  NodeGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      DesiredCapacity: !Ref NodeAutoScalingGroupMinSize
      LaunchConfigurationName: !Ref NodeLaunchConfig
      MinSize: !Ref NodeAutoScalingGroupMinSize
      MaxSize: !Ref NodeAutoScalingGroupMaxSize
      VPCZoneIdentifier:
        - 
          Fn::ImportValue:
            !Sub "${RootStackName}-public-subnet-a"
        - 
          Fn::ImportValue:
            !Sub "${RootStackName}-public-subnet-c"
      Tags:
      - Key: Name
        Value: !Sub "${ClusterName}-${NodeGroupName}-Node"
        PropagateAtLaunch: 'true'
      - Key: !Sub 'kubernetes.io/cluster/${ClusterName}'
        Value: 'owned'
        PropagateAtLaunch: 'true'
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: '1'
        MaxBatchSize: '1'

  SpotNodeGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      DesiredCapacity: !Ref SpotNodeAutoScalingGroupMinSize
      MinSize: !Ref SpotNodeAutoScalingGroupMinSize
      MaxSize: !Ref SpotNodeAutoScalingGroupMaxSize
      MixedInstancesPolicy:
        InstancesDistribution:
          OnDemandBaseCapacity: 0
          OnDemandPercentageAboveBaseCapacity: 0
          SpotAllocationStrategy: lowest-price
          SpotInstancePools: 2
        LaunchTemplate:
          LaunchTemplateSpecification:
            LaunchTemplateId: !Ref 'SpotNodeLaunchTemplate'
            Version: !GetAtt 'SpotNodeLaunchTemplate.LatestVersionNumber'
          Overrides:
            - InstanceType: t3.large
            - InstanceType: t2.large
      VPCZoneIdentifier:
        - 
          Fn::ImportValue:
            !Sub "${RootStackName}-public-subnet-a"
        - 
          Fn::ImportValue:
            !Sub "${RootStackName}-public-subnet-c"
      Tags:
      - Key: Name
        Value: !Sub "${ClusterName}-${NodeGroupName}-Node"
        PropagateAtLaunch: 'true'
      - Key: !Sub 'kubernetes.io/cluster/${ClusterName}'
        Value: 'owned'
        PropagateAtLaunch: 'true'
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: '1'
        MaxBatchSize: '1'

  NodeLaunchConfig:
    Type: AWS::AutoScaling::LaunchConfiguration
    Properties:
      AssociatePublicIpAddress: 'true'
      IamInstanceProfile: !Ref NodeInstanceProfile
      ImageId: !Ref NodeImageId
      InstanceType: !Ref NodeInstanceType
      KeyName: !Ref KeyName
      SecurityGroups:
      - !Ref NodeSecurityGroup
      BlockDeviceMappings:
        - DeviceName: /dev/xvda
          Ebs:
            VolumeSize: !Ref NodeVolumeSize
            VolumeType: gp2
            DeleteOnTermination: true
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash
            set -o xtrace
            /etc/eks/bootstrap.sh ${ClusterName} ${BootstrapArguments} --kubelet-extra-args '--node-labels=spot=false'
            /opt/aws/bin/cfn-signal --exit-code $? \
                     --stack  ${AWS::StackName} \
                     --resource NodeGroup  \
                     --region ${AWS::Region}

  AWSLoadBalancerControllerIAMPolicy:
    Type: AWS::IAM::Policy
    Properties:
      Roles:
        - !Ref EKSRole
        - !Ref NodeInstanceRole
      PolicyName: AWSLoadBalancerControllerIAMPolicy
      PolicyDocument:
        {
          "Version": "2012-10-17",
          "Statement": [
            {
              "Effect": "Allow",
              "Action": [
                "acm:DescribeCertificate",
                "acm:ListCertificates",
                "acm:GetCertificate"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:CreateSecurityGroup",
                "ec2:CreateTags",
                "ec2:DeleteTags",
                "ec2:DeleteSecurityGroup",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeAddresses",
                "ec2:DescribeInstances",
                "ec2:DescribeInstanceStatus",
                "ec2:DescribeInternetGateways",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeSubnets",
                "ec2:DescribeTags",
                "ec2:DescribeVpcs",
                "ec2:ModifyInstanceAttribute",
                "ec2:ModifyNetworkInterfaceAttribute",
                "ec2:RevokeSecurityGroupIngress"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "elasticloadbalancing:AddListenerCertificates",
                "elasticloadbalancing:AddTags",
                "elasticloadbalancing:CreateListener",
                "elasticloadbalancing:CreateLoadBalancer",
                "elasticloadbalancing:CreateRule",
                "elasticloadbalancing:CreateTargetGroup",
                "elasticloadbalancing:DeleteListener",
                "elasticloadbalancing:DeleteLoadBalancer",
                "elasticloadbalancing:DeleteRule",
                "elasticloadbalancing:DeleteTargetGroup",
                "elasticloadbalancing:DeregisterTargets",
                "elasticloadbalancing:DescribeListenerCertificates",
                "elasticloadbalancing:DescribeListeners",
                "elasticloadbalancing:DescribeLoadBalancers",
                "elasticloadbalancing:DescribeLoadBalancerAttributes",
                "elasticloadbalancing:DescribeRules",
                "elasticloadbalancing:DescribeSSLPolicies",
                "elasticloadbalancing:DescribeTags",
                "elasticloadbalancing:DescribeTargetGroups",
                "elasticloadbalancing:DescribeTargetGroupAttributes",
                "elasticloadbalancing:DescribeTargetHealth",
                "elasticloadbalancing:ModifyListener",
                "elasticloadbalancing:ModifyLoadBalancerAttributes",
                "elasticloadbalancing:ModifyRule",
                "elasticloadbalancing:ModifyTargetGroup",
                "elasticloadbalancing:ModifyTargetGroupAttributes",
                "elasticloadbalancing:RegisterTargets",
                "elasticloadbalancing:RemoveListenerCertificates",
                "elasticloadbalancing:RemoveTags",
                "elasticloadbalancing:SetIpAddressType",
                "elasticloadbalancing:SetSecurityGroups",
                "elasticloadbalancing:SetSubnets",
                "elasticloadbalancing:SetWebAcl"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "iam:CreateServiceLinkedRole",
                "iam:GetServerCertificate",
                "iam:ListServerCertificates"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "cognito-idp:DescribeUserPoolClient"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "waf-regional:GetWebACLForResource",
                "waf-regional:GetWebACL",
                "waf-regional:AssociateWebACL",
                "waf-regional:DisassociateWebACL"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "tag:GetResources",
                "tag:TagResources"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "waf:GetWebACL"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "wafv2:GetWebACL",
                "wafv2:GetWebACLForResource",
                "wafv2:AssociateWebACL",
                "wafv2:DisassociateWebACL"
              ],
              "Resource": "*"
            },
            {
              "Effect": "Allow",
              "Action": [
                "shield:DescribeProtection",
                "shield:GetSubscriptionState",
                "shield:DeleteProtection",
                "shield:CreateProtection",
                "shield:DescribeSubscription",
                "shield:ListProtections"
              ],
              "Resource": "*"
            }
          ]
        }

Outputs:
  SecurityGroups:
    Description: Security group for the cluster control plane communication with worker nodes
    Value: !Join [ ",", [ !Ref ControlPlaneSecurityGroup ] ]
    Export:
      Name: !Sub "${RootStackName}-eks-security-group"
  AppSecurityGroup:
    Value: !Ref AppSecurityGroup
    Export:
      Name: !Sub "${RootStackName}-app-security-group"
  NodeInstanceRole:
    Description: The node instance role
    Value: !GetAtt NodeInstanceRole.Arn
    Export:
      Name: !Sub "${RootStackName}-node-instance-role-arn"