# Guildion connect Server

https://connect.guildion.co

The websocket connect server for Guildion that community building service for video watchers ( https://www.guildion.co )

## Installation
#### Build Docker

````
git clone https://gitlab.com/zack.s/guildion-connect
cd guildion-connect
docker build . -t guildion:guldion-connect:[tagname]
````

## Useage

#### To run Guildion connect in development mode

```
PORT=7002 go run cmd/connect/main.go
```