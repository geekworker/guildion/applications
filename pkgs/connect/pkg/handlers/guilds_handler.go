package handlers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/endpoints/guilds_connect_api"
	"gitlab.com/guildion-connect/pkg/models"
	"gitlab.com/guildion-connect/pkg/usecases"
)

type GuildsHandler struct {
}

func (handler *GuildsHandler) MsgsCreate(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	vars := mux.Vars(req)
	device := &models.DeviceModel{}
	err := json.Unmarshal([]byte(req.Header.Get(constants.API_HEADER_APPLICATION_DEVICE)), device)
	if err != nil {
		log.Println("ServeHTTP:", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	request := &guilds_connect_api.GuildConnectAPIMsgsCreateRequest{
		ID:           vars["id"],
		AcessToken:   vars["accessToken"],
		CSRFSecret:   vars["csrfSecret"],
		Device:       device,
		LanguageCode: req.Header.Get(constants.HEADER_ACCEPT_LANGUAGE),
		Message:      vars["message"],
	}
	u := &usecases.GuildsUsecase{}
	err = u.ConnectionsList(request, 20, 0)
	if err != nil {
		log.Println("ServeHTTP:", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	ResponseWriter := &guilds_connect_api.GuildConnectAPIMsgsCreateResponse{
		Success: true,
	}
	json.NewEncoder(w).Encode(ResponseWriter)
}
