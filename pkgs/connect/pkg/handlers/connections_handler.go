package handlers

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/endpoints/connections_connect_api"
	"gitlab.com/guildion-connect/pkg/models"
	"gitlab.com/guildion-connect/pkg/usecases"
)

type ConnectionsHandler struct {
}

func (handler *ConnectionsHandler) Create(w http.ResponseWriter, req *http.Request) {
	Socket, err := constants.Upgrader.Upgrade(w, req, nil)
	vars := mux.Vars(req)
	if err != nil {
		log.Println("ServeHTTP:", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	connection := models.GenerateNewConnection(w, req, Socket)
	log.Println("debug: [CONNECTED] <--", connection.SecWebsocketKey)

	u := &usecases.ConnectionsUsecase{}
	connection, err = u.Create(connection, vars["id"])
	if err != nil {
		log.Println("ServeHTTP:", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	res := &connections_connect_api.ConnectionConnectAPICreateResponse{
		Type:       "connected",
		Success:    true,
		Connection: connection,
	}
	msg := models.MsgModel{
		Connection: connection,
		Message:    res.Jsonify(),
	}
	// FIXME: ConnectionResponse Send not working
	u.Send(connection.ID, msg)
	defer func() {
		handler.Destroy(connection)
	}()
	go handler.Write(connection)
	handler.Read(connection)
}

func (handler *ConnectionsHandler) Read(connection *models.ConnectionModel) {
	defer func() {
		connection.Socket.Close()
	}()
	for {
		_, message, err := connection.Socket.ReadMessage()
		if err != nil {
			return
		}
		msg := models.MsgModel{
			Connection: connection,
			Message:    message,
		}
		handler.Forward(connection, msg)
	}
}

func (handler *ConnectionsHandler) Write(connection *models.ConnectionModel) {
	defer func() {
		connection.Socket.Close()
	}()
	for msg := range connection.Send {
		err := connection.Socket.WriteMessage(websocket.TextMessage, msg)
		if err != nil {
			return
		}
	}
}

func (handler *ConnectionsHandler) Forward(connection *models.ConnectionModel, cmsg models.MsgModel) {
}

func (handler *ConnectionsHandler) Destroy(connection *models.ConnectionModel) {
	log.Println("debug: [DISCONNECTED] <--", connection.SecWebsocketKey)
	u := &usecases.ConnectionsUsecase{}
	u.Destroy(connection)
}
