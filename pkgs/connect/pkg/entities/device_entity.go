package entities

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/modules/db"
)

type DeviceEntity struct {
	ID           string `gorm:"col:id"`
	UDID         string `gorm:"col:udid"`
	OS           string `gorm:"col:os"`
	MODEL        string `gorm:"col:model"`
	LanguageCode string `gorm:"col:language_code"`
	CountryCode  string `gorm:"col:country_code"`
	AppVersion   string `gorm:"col:app_version"`
	Status       string `gorm:"col:status"`
}

func (device DeviceEntity) TableName() string {
	return "devices"
}

func (device DeviceEntity) Validate(db *gorm.DB) {
}

var ConnectDevice = &DeviceEntity{
	ID:           "",
	UDID:         constants.GetENV().CONNECT_DEVICE_UDID,
	OS:           "Linux OS",
	MODEL:        "Amazon Linux 2",
	LanguageCode: "en",
	CountryCode:  "JP",
	AppVersion:   "0.0.0",
	Status:       "CREATED",
}

func SyncConnectDevice() error {
	if err := db.DB.Where(DeviceEntity{
		OS:    "Linux OS",
		MODEL: "Amazon Linux 2",
	}).Find(ConnectDevice).Error; err != nil {
		return err
	}
	return nil
}
