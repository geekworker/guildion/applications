package entities

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/guildion-connect/pkg/modules/db"
)

type CSRFEntity struct {
	ID       string `gorm:"col:id"`
	DeviceID string `gorm:"col:device_id"`
	Secret   string `gorm:"col:secret"`
	Hash     string `gorm:"col:hash"`
	Salt     string `gorm:"col:salt"`
	Iv       string `gorm:"col:iv"`
	IsActive bool   `gorm:"col:is_active"`
}

func (device CSRFEntity) TableName() string {
	return "csrfs"
}

func (device CSRFEntity) Validate(db *gorm.DB) {
}

var ConnectCSRF = &CSRFEntity{}

func SyncConnectCSRF() error {
	if err := db.DB.Where(CSRFEntity{
		DeviceID: ConnectDevice.ID,
	}).Find(ConnectCSRF).Error; err != nil {
		return err
	}
	return nil
}
