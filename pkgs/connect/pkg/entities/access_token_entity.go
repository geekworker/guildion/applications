package entities

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/guildion-connect/pkg/modules/db"
)

type AccessTokenEntity struct {
	ID        string `gorm:"col:id"`
	AccountID string `gorm:"col:account_id"`
	DeviceID  string `gorm:"col:device_id"`
	Secret    string `gorm:"col:secret"`
	IsOneTime bool   `gorm:"col:is_one_time"`
	IsActive  bool   `gorm:"col:is_active"`
}

func (device AccessTokenEntity) TableName() string {
	return "access_tokens"
}

func (device AccessTokenEntity) Validate(db *gorm.DB) {
}

var ConnectAccessToken = &AccessTokenEntity{}

func SyncConnectAccessToken() error {
	if err := db.DB.Where(AccessTokenEntity{
		DeviceID: ConnectDevice.ID,
	}).Find(ConnectAccessToken).Error; err != nil {
		return err
	}
	return nil
}
