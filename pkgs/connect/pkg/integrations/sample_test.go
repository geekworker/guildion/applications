package integrations__test

import (
	"testing"
)

func TestSample(t *testing.T) {
	expected := 2
	actual := 1 + 1
	if actual != expected {
		t.Errorf("got: %v\nwant: %v", actual, expected)
	}
}
