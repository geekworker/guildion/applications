package usecases

import (
	"github.com/thoas/go-funk"
	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/endpoints/guilds_api"
	"gitlab.com/guildion-connect/pkg/endpoints/guilds_connect_api"
	"gitlab.com/guildion-connect/pkg/models"
	"gitlab.com/guildion-connect/pkg/repositories"
)

type GuildsUsecase struct {
}

func (u *GuildsUsecase) ConnectionsList(r *guilds_connect_api.GuildConnectAPIMsgsCreateRequest, count int, offset int) error {
	repo := &repositories.GuildsRepository{}
	res, err := repo.ConnectionsList(&guilds_api.GuildAPIConnectionsListRequest{
		ID:     r.ID,
		Count:  count,
		Offset: offset,
	}, &endpoints.APIRequestOpts{
		Device:       r.Device,
		AcessToken:   r.AcessToken,
		CSRFSecret:   r.CSRFSecret,
		LanguageCode: r.LanguageCode,
	})
	if err != nil {
		return err
	}
	uc := &ConnectionsUsecase{}
	msg := models.MsgModel{
		Message: []byte(r.Message),
	}
	ids := funk.Map(res.Connections, func(connection *models.ConnectionModel) string {
		return connection.ID
	}).([]string)
	err = uc.SendFromIDs(ids, msg)
	if err != nil {
		return err
	}
	if res.Paginatable {
		return u.ConnectionsList(r, count, offset+count)
	} else {
		return nil
	}
}
