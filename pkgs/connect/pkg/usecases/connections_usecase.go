package usecases

import (
	"log"

	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/endpoints/connections_api"
	"gitlab.com/guildion-connect/pkg/entities"
	"gitlab.com/guildion-connect/pkg/models"
	"gitlab.com/guildion-connect/pkg/repositories"
)

type ConnectionsUsecase struct {
}

func (u *ConnectionsUsecase) Create(connection *models.ConnectionModel, DeviceID string) (*models.ConnectionModel, error) {
	r := &repositories.ConnectionsRepository{}
	req := &connections_api.ConnectionAPICreateRequest{
		Connection: connection,
		DeviceID:   DeviceID,
	}
	entities.SyncConnectDevice()
	entities.SyncConnectAccessToken()
	entities.SyncConnectCSRF()
	res, err := r.Create(req, &endpoints.APIRequestOpts{
		Device: &models.DeviceModel{
			ID:           entities.ConnectDevice.ID,
			UDID:         entities.ConnectDevice.UDID,
			OS:           entities.ConnectDevice.OS,
			MODEL:        entities.ConnectDevice.MODEL,
			LanguageCode: entities.ConnectDevice.LanguageCode,
			CountryCode:  entities.ConnectDevice.CountryCode,
			AppVersion:   entities.ConnectDevice.AppVersion,
			Status:       entities.ConnectDevice.Status,
		},
		AcessToken: entities.ConnectAccessToken.Secret,
		CSRFSecret: entities.ConnectCSRF.Secret,
	})
	if err != nil {
		return nil, err
	}
	connection.ID = res.ID
	connection.IsConnecting = true
	models.AppendConnection(connection)
	go r.Subscribe(connection)
	return connection, err
}

func (u *ConnectionsUsecase) Destroy(connection *models.ConnectionModel) error {
	r := &repositories.ConnectionsRepository{}
	req := &connections_api.ConnectionAPIDestroyRequest{
		ID: &connection.ID,
	}
	entities.SyncConnectDevice()
	entities.SyncConnectAccessToken()
	entities.SyncConnectCSRF()
	res, err := r.Destroy(req, &endpoints.APIRequestOpts{
		Device: &models.DeviceModel{
			ID:           entities.ConnectDevice.ID,
			UDID:         entities.ConnectDevice.UDID,
			OS:           entities.ConnectDevice.OS,
			MODEL:        entities.ConnectDevice.MODEL,
			LanguageCode: entities.ConnectDevice.LanguageCode,
			CountryCode:  entities.ConnectDevice.CountryCode,
			AppVersion:   entities.ConnectDevice.AppVersion,
			Status:       entities.ConnectDevice.Status,
		},
		AcessToken: entities.ConnectAccessToken.Secret,
		CSRFSecret: entities.ConnectCSRF.Secret,
	})
	if err != nil || !res {
		return err
	}
	connection.IsConnecting = false
	models.RemoveConnections(connection)
	return nil
}

func (u *ConnectionsUsecase) Send(id string, msg models.MsgModel) error {
	r := &repositories.ConnectionsRepository{}
	r.PublishMessage(id, msg.Message)
	log.Println("debug: SENT MESSAGE TO -->", id, "CONNECTION")
	return nil
}

func (u *ConnectionsUsecase) SendFromIDs(ids []string, msg models.MsgModel) error {
	for _, id := range ids {
		u.Send(id, msg)
	}
	return nil
}
