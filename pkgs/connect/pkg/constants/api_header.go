package constants

const API_HEADER_APPLICATION_ACCESS_TOKEN string = "application-accesstoken"
const API_HEADER_APPLICATION_DEVICE string = "application-device"
const API_HEADER_APPLICATION_CSRF string = "application-csrf"
const HEADER_ACCEPT_LANGUAGE = "Accept-Language"
