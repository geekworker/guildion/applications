package constants

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

type ENV struct {
	Singleton
	GO_ENV                 GO_ENV `json:"GO_ENV"`
	PORT                   string `json:"PORT"`
	CONNECT_DEVICE_UDID    string `json:"CONNECT_DEVICE_UDID"`
	CONNECT_REDIS_HOST_URL string `json:"CONNECT_REDIS_HOST_URL"`
	API_DATABASE_USERNAME  string `json:"API_DATABASE_USERNAME"`
	API_DATABASE_PASSWORD  string `json:"API_DATABASE_PASSWORD"`
	API_DATABASE_NAME      string `json:"API_DATABASE_NAME"`
	API_DATABASE_HOST      string `json:"API_DATABASE_HOST"`
	API_DATABASE_PORT      string `json:"API_DATABASE_PORT"`
}

var instance *ENV

func GetENV() *ENV {
	if instance == nil {
		if os.Getenv("GO_ENV") == "" {
			os.Setenv("GO_ENV", "development")
		}
		mode := os.Getenv("GO_ENV")
		err := godotenv.Load()
		if err != nil {
			log.Fatal("FileNotFoundError: not found .env file")
		}
		switch mode {
		case "staging":
			instance = &ENV{
				GO_ENV:                 STAGING,
				PORT:                   os.Getenv("PORT"),
				CONNECT_DEVICE_UDID:    os.Getenv("CONNECT_DEVICE_UDID"),
				CONNECT_REDIS_HOST_URL: os.Getenv("CONNECT_REDIS_HOST_URL"),
				API_DATABASE_USERNAME:  os.Getenv("API_DATABASE_USERNAME"),
				API_DATABASE_PASSWORD:  os.Getenv("API_DATABASE_PASSWORD"),
				API_DATABASE_NAME:      os.Getenv("API_DATABASE_NAME"),
				API_DATABASE_PORT:      os.Getenv("API_DATABASE_PORT"),
				API_DATABASE_HOST:      os.Getenv("API_DATABASE_HOST"),
			}
		case "production":
			instance = &ENV{
				GO_ENV:                 PRODUCTION,
				PORT:                   os.Getenv("PORT"),
				CONNECT_DEVICE_UDID:    os.Getenv("CONNECT_DEVICE_UDID"),
				CONNECT_REDIS_HOST_URL: os.Getenv("CONNECT_REDIS_HOST_URL"),
				API_DATABASE_USERNAME:  os.Getenv("API_DATABASE_USERNAME"),
				API_DATABASE_PASSWORD:  os.Getenv("API_DATABASE_PASSWORD"),
				API_DATABASE_NAME:      os.Getenv("API_DATABASE_NAME"),
				API_DATABASE_PORT:      os.Getenv("API_DATABASE_PORT"),
				API_DATABASE_HOST:      os.Getenv("API_DATABASE_HOST"),
			}
		default:
			instance = &ENV{
				GO_ENV:                 DEVELOPMENT,
				PORT:                   os.Getenv("PORT"),
				CONNECT_DEVICE_UDID:    os.Getenv("CONNECT_DEVICE_UDID"),
				CONNECT_REDIS_HOST_URL: os.Getenv("CONNECT_REDIS_HOST_URL"),
				API_DATABASE_USERNAME:  os.Getenv("API_DATABASE_USERNAME"),
				API_DATABASE_PASSWORD:  os.Getenv("API_DATABASE_PASSWORD"),
				API_DATABASE_NAME:      os.Getenv("API_DATABASE_NAME"),
				API_DATABASE_PORT:      os.Getenv("API_DATABASE_PORT"),
				API_DATABASE_HOST:      os.Getenv("API_DATABASE_HOST"),
			}
		}
	}
	return instance
}
