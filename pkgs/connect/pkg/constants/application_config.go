package constants

const APP_NAME string = "Guildion"
const APP_NAME_LATIN string = "Guildion"
const APP_NAME_UPPERCASE string = "GUILDION"
const APP_ICON string = "Guildion"
const HOST_NAME string = "Guildion"
const LOCAL_APP_URL string = "http://localhost:7001"

func GET_APP_DOMAIN(go_env GO_ENV) string {
	switch go_env {
	default:
		return "localhost:7000"
	case DEVELOPMENT:
		return "localhost:7000"
	case STAGING:
		return "staging.guildion.co"
	case PRODUCTION:
		return "www.guildion.co"
	}
}

func GET_APP_URL(go_env GO_ENV) string {
	switch go_env {
	default:
		return "http://localhost:7000"
	case DEVELOPMENT:
		return "http://localhost:7000"
	case STAGING:
		return "https://staging.guildion.co"
	case PRODUCTION:
		return "https://www.guildion.co"
	}
}

func GET_API_DOMAIN(go_env GO_ENV) string {
	switch go_env {
	default:
		return "localhost:7000/api/proxy"
	case DEVELOPMENT:
		return "localhost:7000/api/proxy"
	case STAGING:
		return "staging.guildion.co/api/proxy"
	case PRODUCTION:
		return "www.guildion.co/api/proxy"
	}
}

func GET_API_URL(go_env GO_ENV) string {
	switch go_env {
	default:
		return "http://localhost:7000/api/proxy"
	case DEVELOPMENT:
		return "http://localhost:7000/api/proxy"
	case STAGING:
		return "https://staging.guildion.co/api/proxy"
	case PRODUCTION:
		return "https://www.guildion.co/api/proxy"
	}
}

func GET_CONNECT_API_DOMAIN(go_env GO_ENV) string {
	switch go_env {
	default:
		return "localhost:7000/api/proxy/connect/v1"
	case DEVELOPMENT:
		return "localhost:7000/api/proxy/connect/v1"
	case STAGING:
		return "staging.guildion.co/api/proxy/connect/v1"
	case PRODUCTION:
		return "www.guildion.co/api/proxy/connect/v1"
	}
}

func GET_CONNECT_API_URL(go_env GO_ENV) string {
	switch go_env {
	default:
		return "http://localhost:7000/api/proxy/connect/v1"
	case DEVELOPMENT:
		return "http://localhost:7000/api/proxy/connect/v1"
	case STAGING:
		return "https://staging.guildion.co/api/proxy/connect/v1"
	case PRODUCTION:
		return "https://www.guildion.co/api/proxy/connect/v1"
	}
}

func GET_CURRENT_APP_DOMAIN() string {
	return GET_APP_DOMAIN(GetENV().GO_ENV)
}
func GET_CURRENT_APP_URL() string {
	return GET_APP_URL(GetENV().GO_ENV)
}
func GET_CURRENT_API_DOMAIN() string {
	return GET_API_DOMAIN(GetENV().GO_ENV)
}
func GET_CURRENT_API_URL() string {
	return GET_API_URL(GetENV().GO_ENV)
}
func GET_CURRENT_CONNECT_API_DOMAIN() string {
	return GET_CONNECT_API_DOMAIN(GetENV().GO_ENV)
}
func GET_CURRENT_CONNECT_API_URL() string {
	return GET_CONNECT_API_URL(GetENV().GO_ENV)
}
