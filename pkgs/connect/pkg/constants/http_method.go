package constants

import (
	"fmt"
	"strings"
)

type HttpMethod string

const (
	HTTP_GET  HttpMethod = "GET"
	HTTP_POST HttpMethod = "POST"
)

var ErrInvalidHttpMethodType = fmt.Errorf("invalid HttpMethod type")

func (v HttpMethod) String() string {
	return string(v)
}

func (v HttpMethod) Valid() error {
	switch v {
	case HTTP_GET, HTTP_POST:
		return nil
	default:
		return ErrInvalidHttpMethodType
	}
}

func (v *HttpMethod) UnmarshalJSON(b []byte) error {
	*v = HttpMethod(strings.Trim(string(b), `"`))
	return v.Valid()
}

func ParseHttpMethod(s string) (v HttpMethod, err error) {
	v = HttpMethod(s)
	err = v.Valid()
	return
}
