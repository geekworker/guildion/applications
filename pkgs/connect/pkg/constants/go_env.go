package constants

import (
	"fmt"
	"strings"
)

type GO_ENV string

const (
	DEVELOPMENT GO_ENV = "development"
	STAGING     GO_ENV = "staging"
	PRODUCTION  GO_ENV = "production"
)

var ErrInvalidGoEnvType = fmt.Errorf("invalid GO_ENV type")

func (v GO_ENV) String() string {
	return string(v)
}

func (v GO_ENV) Valid() error {
	switch v {
	case DEVELOPMENT, STAGING, PRODUCTION:
		return nil
	default:
		return ErrInvalidGoEnvType
	}
}

func (v *GO_ENV) UnmarshalJSON(b []byte) error {
	*v = GO_ENV(strings.Trim(string(b), `"`))
	return v.Valid()
}

func ParseGoEnv(s string) (v GO_ENV, err error) {
	v = GO_ENV(s)
	err = v.Valid()
	return v, err
}
