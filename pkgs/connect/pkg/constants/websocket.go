package constants

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

const (
	SocketBufferSize  = 1024
	MessageBufferSize = 256
	HandshakeTimeout  = 24 * time.Hour
)

var Upgrader = &websocket.Upgrader{
	ReadBufferSize:   SocketBufferSize,
	WriteBufferSize:  SocketBufferSize,
	HandshakeTimeout: HandshakeTimeout,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}
