package redis

import (
	"github.com/go-redis/redis/v8"
	"gitlab.com/guildion-connect/pkg/constants"
)

var Redis *redis.Client

func CreateRedisClient() {
	opt, err := redis.ParseURL(constants.GetENV().CONNECT_REDIS_HOST_URL)
	if err != nil {
		panic(err)
	}
	redis := redis.NewClient(opt)
	Redis = redis
}

func CloseRedisConn() {
	Redis.Close()
}
