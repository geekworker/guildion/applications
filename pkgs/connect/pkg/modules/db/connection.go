package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"github.com/qor/validations"
	"gitlab.com/guildion-connect/pkg/constants"
)

type DBConnector struct {
	Dialect  string `json:"dialect"`
	Username string `json:"username"`
	Password string `json:"password"`
	Host     string `json:"host"`
	Port     string `json:"port"`
	Database string `json:"database"`
}

var DB *gorm.DB
var connectTemplate = "host=%s port=%s user=%s password=%s dbname=%s sslmode=disable"

func getConnector() DBConnector {
	var connector = DBConnector{
		Dialect:  "postgres",
		Username: constants.GetENV().API_DATABASE_USERNAME,
		Password: constants.GetENV().API_DATABASE_PASSWORD,
		Host:     constants.GetENV().API_DATABASE_HOST,
		Port:     constants.GetENV().API_DATABASE_PORT,
		Database: constants.GetENV().API_DATABASE_NAME,
	}
	return connector
}

func Connect() *gorm.DB {
	connector := getConnector()
	connect := fmt.Sprintf(connectTemplate, connector.Host, connector.Port, connector.Username, connector.Password, connector.Database)
	db, err := gorm.Open(connector.Dialect, connect)
	DB = db
	validations.RegisterCallbacks(DB)
	if err != nil {
		panic(err)
	}
	return DB
}

func CloseConn() {
	DB.Close()
}
