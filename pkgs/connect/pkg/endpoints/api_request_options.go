package endpoints

import "gitlab.com/guildion-connect/pkg/models"

type APIRequestOpts struct {
	AcessToken   string
	CSRFSecret   string
	LanguageCode string
	Device       *models.DeviceModel
}
