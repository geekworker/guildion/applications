package rooms_connect_api

import (
	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/models"
)

type RoomConnectAPIMsgsCreateRouter struct {
}

func (r *RoomConnectAPIMsgsCreateRouter) Path() string {
	return "/api/v1/room/msgs/create"
}

func (r *RoomConnectAPIMsgsCreateRouter) URL() string {
	return constants.GET_CURRENT_CONNECT_API_URL() + r.Path()
}

func (r *RoomConnectAPIMsgsCreateRouter) Method() constants.HttpMethod {
	return constants.HTTP_POST
}

type RoomConnectAPIMsgsCreateRequest struct {
	ID           string              `json:"id"`
	AcessToken   string              `json:"accessToken"`
	CSRFSecret   string              `json:"csrfSecret"`
	Device       *models.DeviceModel `json:"device"`
	LanguageCode string              `json:"languageCode"`
	Message      string              `json:"message"`
}

func (r *RoomConnectAPIMsgsCreateRequest) ByteMessage() []byte {
	return []byte(r.Message)
}

type RoomConnectAPIMsgsCreateResponse struct {
	Success bool `json:"success,string"`
}
