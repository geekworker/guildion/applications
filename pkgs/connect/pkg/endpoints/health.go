package endpoints

type HealthResponse struct {
	Success      bool   `json:"success,string"`
	HealthStatus string `json:"health_status"`
}
