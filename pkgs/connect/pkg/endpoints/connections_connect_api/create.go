package connections_connect_api

import (
	"encoding/json"

	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/models"
)

type ConnectionConnectAPICreateRouter struct {
}

func (r *ConnectionConnectAPICreateRouter) Path() string {
	return "/api/v1/guild/msgs/create"
}

func (r *ConnectionConnectAPICreateRouter) URL() string {
	return constants.GET_CURRENT_CONNECT_API_URL() + r.Path()
}

func (r *ConnectionConnectAPICreateRouter) Method() constants.HttpMethod {
	return constants.HTTP_POST
}

type ConnectionConnectAPICreateRequest struct {
}

type ConnectionConnectAPICreateResponse struct {
	Type       string                  `json:"type"`
	Success    bool                    `json:"success,string"`
	Connection *models.ConnectionModel `json:"connection"`
}

func (r *ConnectionConnectAPICreateResponse) Jsonify() []byte {
	json, _ := json.Marshal(r)
	return json
}
