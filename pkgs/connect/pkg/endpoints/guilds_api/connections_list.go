package guilds_api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/models"
)

type GuildAPIConnectionsListRouter struct {
}

func (r *GuildAPIConnectionsListRouter) Path() string {
	return "/api/v1/guild/connections/list"
}

func (r *GuildAPIConnectionsListRouter) URL() string {
	return constants.GET_CURRENT_API_URL() + r.Path()
}

func (r *GuildAPIConnectionsListRouter) Method() constants.HttpMethod {
	return constants.HTTP_POST
}

type GuildAPIConnectionsListRequest struct {
	ID     string `json:"id"`
	Offset int    `json:"offset,string"`
	Count  int    `json:"count,string"`
}

func (r *GuildAPIConnectionsListRequest) Jsonify() []byte {
	json, _ := json.Marshal(r)
	return json
}

type GuildAPIConnectionsListResponse struct {
	Connections []*models.ConnectionModel `json:"connections"`
	Paginatable bool                      `json:"paginatable,string"`
}

func (r *GuildAPIConnectionsListRequest) Call(opts *endpoints.APIRequestOpts) (*GuildAPIConnectionsListResponse, error) {
	router := &GuildAPIConnectionsListRouter{}
	req, _ := http.NewRequest(router.Method().String(), router.URL(), bytes.NewBuffer(r.Jsonify()))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept-Language", opts.LanguageCode)
	req.Header.Set(constants.API_HEADER_APPLICATION_ACCESS_TOKEN, opts.AcessToken)
	req.Header.Set(constants.API_HEADER_APPLICATION_CSRF, opts.CSRFSecret)
	req.Header.Set(constants.API_HEADER_APPLICATION_DEVICE, opts.Device.JsonStringify())
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var target = &GuildAPIConnectionsListResponse{}
	err = json.Unmarshal(body, &target)
	if err != nil && !strings.HasPrefix(err.Error(), "json: invalid use of ,string struct tag") {
		return nil, err
	}
	if res == nil || target == nil {
		return nil, fmt.Errorf("invalid request error")
	}
	return target, nil
}
