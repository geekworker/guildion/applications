package rooms_api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/models"
)

type RoomAPIConnectionsListRouter struct {
}

func (r *RoomAPIConnectionsListRouter) Path() string {
	return "/api/v1/room/connections/list"
}

func (r *RoomAPIConnectionsListRouter) URL() string {
	return constants.GET_CURRENT_API_URL() + r.Path()
}

func (r *RoomAPIConnectionsListRouter) Method() constants.HttpMethod {
	return constants.HTTP_POST
}

type RoomAPIConnectionsListRequest struct {
	ID     string `json:"id"`
	Offset int    `json:"offset,string"`
	Count  int    `json:"count,string"`
}

func (r *RoomAPIConnectionsListRequest) Jsonify() []byte {
	json, _ := json.Marshal(r)
	return json
}

type RoomAPIConnectionsListResponse struct {
	Connections []*models.ConnectionModel `json:"connection"`
	Paginatable bool                      `json:"paginatable,string"`
}

func (r *RoomAPIConnectionsListRequest) Call(opts *endpoints.APIRequestOpts) (*RoomAPIConnectionsListResponse, error) {
	router := &RoomAPIConnectionsListRouter{}
	req, _ := http.NewRequest(router.Method().String(), router.URL(), bytes.NewBuffer(r.Jsonify()))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept-Language", opts.LanguageCode)
	req.Header.Set(constants.API_HEADER_APPLICATION_ACCESS_TOKEN, opts.AcessToken)
	req.Header.Set(constants.API_HEADER_APPLICATION_CSRF, opts.CSRFSecret)
	req.Header.Set(constants.API_HEADER_APPLICATION_DEVICE, opts.Device.JsonStringify())
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var target = &RoomAPIConnectionsListResponse{}
	err = json.Unmarshal(body, &target)
	if err != nil && !strings.HasPrefix(err.Error(), "json: invalid use of ,string struct tag") {
		return nil, err
	}
	if res == nil || target == nil {
		return nil, fmt.Errorf("invalid request error")
	}
	return target, nil
}
