package guilds_connect_api

import (
	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/models"
)

type GuildConnectAPIMsgsCreateRouter struct {
}

func (r *GuildConnectAPIMsgsCreateRouter) Path() string {
	return "/api/v1/guild/msgs/create"
}

func (r *GuildConnectAPIMsgsCreateRouter) URL() string {
	return constants.GET_CURRENT_CONNECT_API_URL() + r.Path()
}

func (r *GuildConnectAPIMsgsCreateRouter) Method() constants.HttpMethod {
	return constants.HTTP_POST
}

type GuildConnectAPIMsgsCreateRequest struct {
	ID           string              `json:"id"`
	AcessToken   string              `json:"accessToken"`
	CSRFSecret   string              `json:"csrfSecret"`
	Device       *models.DeviceModel `json:"device"`
	LanguageCode string              `json:"lang"`
	Message      string              `json:"message"`
}

func (r *GuildConnectAPIMsgsCreateRequest) ByteMessage() []byte {
	return []byte(r.Message)
}

type GuildConnectAPIMsgsCreateResponse struct {
	Success bool `json:"success,string"`
}
