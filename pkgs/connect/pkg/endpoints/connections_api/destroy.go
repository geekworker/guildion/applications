package connections_api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/endpoints"
)

type ConnectionAPIDestroyRouter struct {
}

func (r *ConnectionAPIDestroyRouter) Path() string {
	return "/api/v1/connection/destroy"
}

func (r *ConnectionAPIDestroyRouter) URL() string {
	return constants.GET_CURRENT_API_URL() + r.Path()
}

func (r *ConnectionAPIDestroyRouter) Method() constants.HttpMethod {
	return constants.HTTP_POST
}

type ConnectionAPIDestroyRequest struct {
	ID *string `json:"id"`
}

func (r *ConnectionAPIDestroyRequest) Jsonify() []byte {
	json, _ := json.Marshal(r)
	return json
}

type ConnectionAPIDestroyResponse struct {
	Success bool `json:"success,string"`
}

func (r *ConnectionAPIDestroyRequest) Call(opts *endpoints.APIRequestOpts) (*ConnectionAPIDestroyResponse, error) {
	router := &ConnectionAPIDestroyRouter{}
	req, _ := http.NewRequest(router.Method().String(), router.URL(), bytes.NewBuffer(r.Jsonify()))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept-Language", opts.LanguageCode)
	req.Header.Set(constants.API_HEADER_APPLICATION_ACCESS_TOKEN, opts.AcessToken)
	req.Header.Set(constants.API_HEADER_APPLICATION_CSRF, opts.CSRFSecret)
	req.Header.Set(constants.API_HEADER_APPLICATION_DEVICE, opts.Device.JsonStringify())
	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	var target = &ConnectionAPIDestroyResponse{}
	err = json.Unmarshal(body, &target)
	if err != nil && !strings.HasPrefix(err.Error(), "json: invalid use of ,string struct tag") {
		return nil, err
	}
	if res == nil || target == nil {
		return nil, fmt.Errorf("invalid request error")
	}
	return target, nil
}
