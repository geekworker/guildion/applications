package infrastructure

import (
	"flag"
	"log"
	"net/http"

	"github.com/comail/colog"
	"github.com/gorilla/mux"
	"gitlab.com/guildion-connect/pkg/constants"
	"gitlab.com/guildion-connect/pkg/middlewares"
)

func RouterRun() {
	colog.SetMinLevel(colog.LTrace)
	colog.SetDefaultLevel(colog.LDebug)
	colog.ParseFields(true)
	colog.Register()
	port := constants.GetENV().PORT

	var addr = flag.String("addr", ":"+port, "The listen port of the application.")
	flag.Parse()

	router := mux.NewRouter()
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Welcome to Guildion Connect API\n"))
	})
	router.HandleFunc("/health", middlewares.HealthMiddleware)
	router.HandleFunc("/api/v1/connection/create/{id}", middlewares.APIV1ConnectionCreateMiddleware)
	router.HandleFunc("/api/v1/guild/msgs/create", middlewares.APIV1GuildMsgsCreate).Methods("POST")
	router.HandleFunc("/api/v1/room/msgs/create", middlewares.APIV1RoomMsgsCreate).Methods("POST")

	http.Handle("/", router)
	log.Println("info: * Environment:", constants.GetENV().GO_ENV)
	log.Println("info: * Starting connect server on", *addr)

	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}

	defer func() {
	}()
}
