package models

type MsgModel struct {
	Connection *ConnectionModel `json:"connection"`
	Message    []byte           `json:"-"`
}
