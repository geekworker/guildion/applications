package models

import (
	"encoding/json"
)

type DeviceModel struct {
	ID           string `json:"id"`
	UDID         string `json:"udid"`
	OS           string `json:"os"`
	MODEL        string `json:"model"`
	LanguageCode string `json:"language_code"`
	CountryCode  string `json:"country_code"`
	AppVersion   string `json:"app_version"`
	Status       string `json:"status"`
}

func (d *DeviceModel) Jsonify() []byte {
	json, _ := json.Marshal(d)
	return json
}

func (d *DeviceModel) JsonStringify() string {
	json := d.Jsonify()
	return string(json)
}
