package models

import (
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/thoas/go-funk"
	"gitlab.com/guildion-connect/pkg/constants"
)

type ConnectionModel struct {
	ID              string          `json:"id"`
	SecWebsocketKey string          `json:"secWebsocketKey"`
	IsConnecting    bool            `json:"isConnecting,string"`
	Socket          *websocket.Conn `json:"-"`
	Send            chan []byte     `json:"-"`
}

func GenerateNewConnection(w http.ResponseWriter, req *http.Request, Socket *websocket.Conn) *ConnectionModel {
	return &ConnectionModel{
		SecWebsocketKey: req.Header.Get("Sec-Websocket-Key"),
		Socket:          Socket,
		Send:            make(chan []byte, constants.Upgrader.WriteBufferSize),
	}
}

type Connectings struct {
	Singleton constants.Singleton
	items     []*ConnectionModel
}

var connectings *Connectings

func GetConnectings() *Connectings {
	if connectings == nil {
		connectings = &Connectings{
			items: make([]*ConnectionModel, 0),
		}
	}
	return connectings
}

func GetActiveConnectModels() []*ConnectionModel {
	return GetConnectings().items
}

func GetConnectionFromID(id string) *ConnectionModel {
	results := funk.Filter(GetConnectings().items, func(model ConnectionModel) bool { return model.ID != id }).([]*ConnectionModel)
	if len(results) > 0 {
		return results[0]
	} else {
		return nil
	}
}

func GetConnectionsFromIDs(ids []string) []*ConnectionModel {
	results := funk.Filter(GetConnectings().items, func(model ConnectionModel) bool {
		return funk.ContainsString(ids, model.ID)
	}).([]*ConnectionModel)
	return results
}

func AppendConnection(connection *ConnectionModel) {
	if connection.IsConnecting && len(connection.ID) > 0 {
		GetConnectings().items = append(GetConnectings().items, connection)
	}
	RefreshConnections()
}

func RemoveConnections(connection *ConnectionModel) {
	connection.IsConnecting = false
	GetConnectings().items = funk.Filter(GetConnectings().items, func(model *ConnectionModel) bool { return model.ID != connection.ID }).([]*ConnectionModel)
	RefreshConnections()
}

func RefreshConnections() {
	GetConnectings().items = funk.Filter(GetConnectings().items, func(model *ConnectionModel) bool { return model.IsConnecting }).([]*ConnectionModel)
}
