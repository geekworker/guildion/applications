package middlewares

import (
	"net/http"

	"gitlab.com/guildion-connect/pkg/handlers"
	"gitlab.com/guildion-connect/pkg/modules/loggers"
)

/*
* PATH: POST(/api/v1/room/msgs/create)
 */
func APIV1RoomMsgsCreate(w http.ResponseWriter, req *http.Request) {
	logger := loggers.RequestLogger{
		Action: req.Method,
		Path:   req.URL.Path,
	}
	logger.Start()
	roomsHandler := &handlers.RoomsHandler{}
	roomsHandler.MsgsCreate(w, req)
	logger.End()
}
