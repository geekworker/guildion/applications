package middlewares

import (
	"encoding/json"
	"net/http"

	"gitlab.com/guildion-connect/pkg/endpoints"
)

/*
* PATH: ROOT(/api/v1/connection/create)
 */
func HealthMiddleware(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(&endpoints.HealthResponse{
		Success:      true,
		HealthStatus: "healthy",
	})
}
