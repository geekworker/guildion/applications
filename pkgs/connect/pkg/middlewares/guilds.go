package middlewares

import (
	"net/http"

	"gitlab.com/guildion-connect/pkg/handlers"
	"gitlab.com/guildion-connect/pkg/modules/loggers"
)

/*
* PATH: POST(/api/v1/guild/msgs/create)
 */
func APIV1GuildMsgsCreate(w http.ResponseWriter, req *http.Request) {
	logger := loggers.RequestLogger{
		Action: req.Method,
		Path:   req.URL.Path,
	}
	logger.Start()
	guildsHandler := &handlers.GuildsHandler{}
	guildsHandler.MsgsCreate(w, req)
	logger.End()
}
