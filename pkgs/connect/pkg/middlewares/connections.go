package middlewares

import (
	"net/http"

	"gitlab.com/guildion-connect/pkg/handlers"
)

/*
* PATH: ROOT(/api/v1/connection/create)
 */
func APIV1ConnectionCreateMiddleware(w http.ResponseWriter, req *http.Request) {
	connectionsHandler := &handlers.ConnectionsHandler{}
	connectionsHandler.Create(w, req)
}
