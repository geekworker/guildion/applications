package repositories

import (
	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/endpoints/guilds_api"
)

type GuildsRepository struct {
}

func (repository *GuildsRepository) ConnectionsList(req *guilds_api.GuildAPIConnectionsListRequest, opts *endpoints.APIRequestOpts) (*guilds_api.GuildAPIConnectionsListResponse, error) {
	res, err := req.Call(opts)
	return res, err
}
