package repositories

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/endpoints/connections_api"
	"gitlab.com/guildion-connect/pkg/models"
	"gitlab.com/guildion-connect/pkg/modules/redis"
)

type ConnectionsRepository struct {
}

func (r *ConnectionsRepository) Create(req *connections_api.ConnectionAPICreateRequest, opts *endpoints.APIRequestOpts) (*models.ConnectionModel, error) {
	res, err := req.Call(opts)
	if res == nil || res.Connection == nil {
		return nil, fmt.Errorf("invalid request error")
	}
	return res.Connection, err
}

func (r *ConnectionsRepository) Destroy(req *connections_api.ConnectionAPIDestroyRequest, opts *endpoints.APIRequestOpts) (bool, error) {
	res, err := req.Call(opts)
	if res == nil {
		return false, nil
	}
	return res.Success, err
}

var ctx = context.Background()

func (r *ConnectionsRepository) PublishMessage(id string, message []byte) {
	err := redis.Redis.Publish(ctx, id, message).Err()
	if err != nil {
		log.Println(err)
	}
}

func (r *ConnectionsRepository) Subscribe(connection *models.ConnectionModel) {
	pubsub := redis.Redis.Subscribe(ctx, connection.ID)
	ch := pubsub.Channel()
	for message := range ch {
		msg := &models.MsgModel{
			Message:    []byte(message.Payload),
			Connection: connection,
		}
		connection.Send <- msg.Message
	}
}
