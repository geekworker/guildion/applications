package repositories

import (
	"gitlab.com/guildion-connect/pkg/endpoints"
	"gitlab.com/guildion-connect/pkg/endpoints/rooms_api"
)

type RoomsRepository struct {
}

func (repository *RoomsRepository) ConnectionsList(req *rooms_api.RoomAPIConnectionsListRequest, opts *endpoints.APIRequestOpts) (*rooms_api.RoomAPIConnectionsListResponse, error) {
	res, err := req.Call(opts)
	return res, err
}
