package main

import (
	"gitlab.com/guildion-connect/pkg/infrastructure"
	"gitlab.com/guildion-connect/pkg/modules/db"
	"gitlab.com/guildion-connect/pkg/modules/redis"
)

func main() {
	defer func() {
		redis.CloseRedisConn()
		db.CloseConn()
	}()
	redis.CreateRedisClient()
	db.Connect()
	infrastructure.RouterRun()
}
