module gitlab.com/guildion-connect

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/comail/colog v0.0.0-20160416085026-fba8e7b1f46c
	github.com/go-redis/redis/v8 v8.11.3
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/guildion/webrtc-sfu v0.0.0
	github.com/jinzhu/gorm v1.9.16
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.8.0
	github.com/qor/qor v1.2.0 // indirect
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46
	github.com/thoas/go-funk v0.9.1
)

replace github.com/guildion/webrtc-sfu => ../sfu
