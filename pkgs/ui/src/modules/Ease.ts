export namespace Ease {
    export const easeOutCubic = (x: number): number => {
        return 1 - Math.pow(1 - x, 3);
    };

    export const easeOutCirc = (x: number): number => {
        return Math.sqrt(1 - Math.pow(x - 1, 2));
    };

    export const easeOutSine = (x: number): number => {
        return Math.sin((x * Math.PI) / 2);
    }

    export const easeInOutQuint = (x: number): number => {
        return x < 0.5 ? 16 * x * x * x * x * x : 1 - Math.pow(-2 * x + 2, 5) / 2;
    }

    export const easeInQuart = (x: number): number => {
        return x * x * x * x;
    }

    export const easeOutQuint = (x: number): number => {
        return 1 - Math.pow(1 - x, 5);
    }
}