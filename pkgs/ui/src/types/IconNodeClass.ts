import React from "react";

export interface IconNodeProps {
    size: number,
    color: string,
}

export type IconNodeClass = React.FC<IconNodeProps>;