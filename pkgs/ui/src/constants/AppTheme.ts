import { Record } from 'immutable';

export const CommonTheme = {
    transparent: 'transparent',
    innerWhite: 'white',
    innerSubWhite: '#DBDBDB',
    overlayBackgroundColor: 'rgba(38, 38, 38, 0.5)',
    overlayBackgroundColorStrong: 'rgba(38, 38, 38, 0.8)',
    splashBackground: "#1F1F1F",
} as const;

export const DarkAppThemeAttributes = {
    common: CommonTheme,
    background: {
        player: "#070707",
        most: "#070707",
        subm1: '#151515',
        subm05: "#1A1A1A",
        root: '#1F1F1F',
        subp1: '#262626',
        subp2: '#30333B',
        subp3: '#41444C',
        complementary: '#FFFFFF',
    },
    shadow: {
        defaultShadowColor: "#000000",
        defaultShadowOpacity: 0.25,
        thickShadowOpacity: 0.8,
    },
    tab: {
        background: "#151515",
        shadow: "#30333B",
        active: '#FFFFFF',
        inactive: '#8e8e93',
    },
    welcome: {
        balloon: '#47B4A5',
    },
    element: {
        default: '#FFFFFF',
        subp1: '#DBDBDB',
        subp1h: '#D1D0D0',
        subp15: '#8e8e93',
        subp2: '#8FA5B5',
        main: '#22B4A5',
        input: '#91959F',
        link: '#52F2D5',
        error: '#FF190C',
        connecting: '#52F2D5',
        url: '#3C77E5',
        highlight: 'rgba(244, 183, 65, 0.3)',
    },
    button: {
        default: '#22B4A5',
        active: '#52F2D5',
        mono: '#30333B',
        disabled: '#676C7B',
    },
    checkbox: {
        default: '#91959F',
        active: '#22B4A5',
        icon: '#FFFFFF',
    },
    textField: {
        default: '#91959F',
        value: '#FFFFFF',
        label: '#FFFFFF',
        placeholder: '#B4B4B4',
    },
    indicator: {
        default: '#B9B9B9',
        active: '#FFFFFF',
    },
    skeleton: {
        boneColor: "#121212",
        highlightColor: "#333333",
    },
    navigation: {
        defaultHeader: "#1F1F1F",
        defaultText: "#FFFFFF",
        defaultIcon: "#FFFFFF",
    },
    guild: {
        tabBarBackground: "#151515",
        tabBorder: '#D1D0D0',
        tabBarShadow: "#30333B",
        tabBackground: "#1A1A1A",
        tabBackgroundActive: "#1F1F1F",
        tabShadowColor: "#000000",
    },
    room: {
        balloonColor: '#333333',
        cellBackground: "#1A1A1A",
        cellShadowColor: "#000000",
        cellShadowOpacity: 0.25,
        syncVisionBackground: '#000000',
    },
    explore: {
        cellBackground: "#1A1A1A",
        cellShadowColor: "#000000",
        cellShadowOpacity: 0.25,
    },
    member: {
        cellFilterColor: '#151515',
        cellFilterOpacity: 0.5,
    },
    mention: {
        memberBackgroundColor: 'rgba(34, 180, 165, 0.4)',
        memberColor: '#52F2D5',
        yourBackgroundColor: 'rgba(244, 183, 65, 0.4)',
        yourColor: '#F4B741',
        roomBackgroundColor: 'rgba(60, 119, 229, 0.4)',
        roomColor: '#56A9D8',
    },
    searchBarTheme: {
        backgroundColor: 'transparent',
        placeholderColor: '#8e8e93',
        textInputBackground: '#30333B',
        textColor: '#ffffff',
        selectionColor: '#2979ff',
        clearIconColor: '#c7c7cc',
        searchIconColor: '#8e8e93',
        textButtonColor: '#ffffff'
    },
    slider: {
        playerBackgroundColor: '#30333B',
        playerBufferColor: '#41444C',
        playerSeekColor: '#52F2D5',
    },
    player: {
        overlayBackgroundColor: 'rgba(38, 38, 38, 0.5)',
        button: 'rgb(255, 255, 255)',
        activeIconColor: 'rgba(255, 255, 255, 0.8)',
        disableIconColor: 'rgba(200, 200, 200, 0.8)',
    },
}

export type AppThemeAttributes = typeof DarkAppThemeAttributes;

export const LightAppThemeAttributes: AppThemeAttributes = {
    common: CommonTheme,
    background: {
        player: "#070707",
        most: "#070707",
        subm1: '#151515',
        subm05: "#1A1A1A",
        root: '#FFFFFF',
        subp1: '#262626',
        subp2: '#30333B',
        subp3: '#41444C',
        complementary: '#151515',
    },
    shadow: {
        defaultShadowColor: "#000000",
        defaultShadowOpacity: 0.25,
        thickShadowOpacity: 0.8,
    },
    tab: {
        background: "#151515",
        shadow: "#30333B",
        active: '#FFFFFF',
        inactive: '#DBDBDB',
    },
    welcome: {
        balloon: '#47B4A5',
    },
    element: {
        default: '#FFFFFF',
        subp1: '#DBDBDB',
        subp1h: '#D1D0D0',
        subp15: '#8e8e93',
        subp2: '#8FA5B5',
        main: '#22B4A5',
        input: '#91959F',
        link: '#52F2D5',
        error: '#FF190C',
        connecting: '#52F2D5',
        url: '#3C77E5',
        highlight: 'rgba(244, 183, 65, 0.1)',
    },
    button: {
        default: '#22B4A5',
        active: '#52F2D5',
        mono: '#30333B',
        disabled: '#676C7B',
    },
    checkbox: {
        default: '#91959F',
        active: '#22B4A5',
        icon: '#FFFFFF',
    },
    textField: {
        default: '#91959F',
        value: '#FFFFFF',
        label: '#FFFFFF',
        placeholder: '#B4B4B4',
    },
    indicator: {
        default: '#FFFFFF',
        active: '#B9B9B9',
    },
    skeleton: {
        boneColor: "#121212",
        highlightColor: "#333333",
    },
    navigation: {
        defaultHeader: "#1F1F1F",
        defaultText: "#FFFFFF",
        defaultIcon: "#FFFFFF",
    },
    guild: {
        tabBarBackground: "#1F1F1F",
        tabBarShadow: "#30333B",
        tabBorder: '#D1D0D0',
        tabBackground: "#262626",
        tabBackgroundActive: "#30333B",
        tabShadowColor: "#000000",
    },
    room: {
        balloonColor: '#151515',
        cellBackground: "#1A1A1A",
        cellShadowColor: "#000000",
        cellShadowOpacity: 0.25,
        syncVisionBackground: '#000000',
    },
    explore: {
        cellBackground: "#1A1A1A",
        cellShadowColor: "#000000",
        cellShadowOpacity: 0.25,
    },
    member: {
        cellFilterColor: '#151515',
        cellFilterOpacity: 0.5,
    },
    mention: {
        memberBackgroundColor: 'rgba(34, 180, 165, 0.7)',
        memberColor: '#52F2D5',
        yourBackgroundColor: 'rgba(244, 183, 65, 0.4)',
        yourColor: '#F4B741',
        roomBackgroundColor: 'rgba(60, 119, 229, 0.7)',
        roomColor: '#56A9D8',
    },
    searchBarTheme: {
        backgroundColor: 'transparent',
        placeholderColor: '#636366',
        textInputBackground: 'rgba(44,44,46,0.8)',
        textColor: 'white',
        selectionColor: '#2979ff',
        clearIconColor: '#c7c7cc',
        searchIconColor: '#b0b0b2',
        textButtonColor: '#2979ff'
    },
    slider: {
        playerBackgroundColor: '#30333B',
        playerBufferColor: '#41444C',
        playerSeekColor: '#52F2D5',
    },
    player: {
        overlayBackgroundColor: 'rgba(38, 38, 38, 0.5)',
        button: 'rgb(255, 255, 255)',
        activeIconColor: 'rgba(255, 255, 255, 0.8)',
        disableIconColor: 'rgba(200, 200, 200, 0.8)',
    },
}

export class AppTheme extends Record<AppThemeAttributes>({
    ...DarkAppThemeAttributes,
}) {
}

export const DarkAppTheme = new AppTheme({ ...DarkAppThemeAttributes });
export const LightAppTheme = new AppTheme({ ...LightAppThemeAttributes });

export const AppThemes = {
    Light: LightAppTheme,
    Dark: DarkAppTheme,
} as const;

export const AppThemeString = {
    Light: 'light',
    Dark: 'dark',
} as const;

export type AppThemeString = typeof AppThemeString[keyof typeof AppThemeString];

export const guardAppTheme = (code: AppThemeString | undefined): AppTheme => {
    switch(code) {
        case AppThemeString.Light: return AppThemes.Light;
        case AppThemeString.Dark: return AppThemes.Dark;
        default: return fallbackAppTheme;
    }
}

export const appThemeToString = (theme: AppTheme): AppThemeString => {
    switch(theme) {
        case AppThemes.Light: return AppThemeString.Light;
        case AppThemes.Dark: return AppThemeString.Dark;
        default: return fallbackAppThemeString;
    }
}

export const fallbackAppTheme: AppTheme = DarkAppTheme;
export const fallbackAppThemeString: AppThemeString = appThemeToString(DarkAppTheme);