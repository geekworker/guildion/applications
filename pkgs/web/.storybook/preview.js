import '@storybook/addon-console';
import '@/presentation/styles/globals.scss';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport';

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    layout: 'centered',
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
    viewport: {
        viewports: INITIAL_VIEWPORTS,
    },
    backgrounds: {
        default: 'dark',
        values: [
          {
            name: 'dark',
            value: '#151515',
          },
          {
            name: 'light',
            value: '#ffffff',
          },
        ],
    },
};