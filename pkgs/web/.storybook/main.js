module.exports = {
    stories: [
        "../src/**/*.stories.mdx",
        "../src/**/*.stories.@(js|jsx|ts|tsx)",
        "../src/**/stories.@(js|jsx|ts|tsx)"
    ],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        '@storybook/addon-controls',
        '@storybook/addon-storysource',
        '@storybook/addon-viewport',
        '@storybook/addon-backgrounds',
    ],
    framework: "@storybook/react",
    core: {
        builder: 'webpack5',
    },
    typescript: {
        reactDocgen: false,
    },
};