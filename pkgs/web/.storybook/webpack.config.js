/* eslint-disable
    @typescript-eslint/no-var-requires,
    @typescript-eslint/explicit-function-return-type
*/

const { resolve } = require('path');

module.exports = ({ config }) => {
    config.module.rules.push({
        test: /\.(ts|tsx)$/,
        loader: require.resolve('babel-loader'),
        options: {
            presets: [require.resolve('babel-preset-react-app')]
        }
    });
    config.module.rules.push({
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
    });
    config.resolve.extensions.push('.ts', '.tsx');
    config.resolve.alias['@'] = resolve(__dirname, '../src');
    config.resolve.alias['/fonts'] = resolve(__dirname, '..', 'public', 'fonts');
    config.resolve.modules = [
        resolve(__dirname, ".."),
        "node_modules",
        resolve(__dirname, "..", "..", "..", "node_modules"),
    ];
    return config
}