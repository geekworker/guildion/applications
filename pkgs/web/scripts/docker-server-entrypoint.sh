#!/bin/sh

set -e

raise(){
  echo "raise exception"
  exit 1
}

echo "* build mode:" $NODE_ENV

echo "bootstraping server"

if [ $NODE_ENV == "staging" ]
then
  yarn workspace @guildion/web staging
else
  yarn workspace @guildion/web production
fi
