import React from "react";
import RootRedux from "./redux";

namespace RootHooks {
    export const useReducer = () => {
        const [state, dispatch] = React.useReducer(
            RootRedux.reducer,
            RootRedux.initialState,
        );
        return {
            state,
            dispatch
        }
    }

    export const useContext = () => {
        const context = React.useContext(RootRedux.Context);
        return context;
    }

    export const useAppTheme = () => {
        return useContext().state.appTheme;
    }
}

export default RootHooks