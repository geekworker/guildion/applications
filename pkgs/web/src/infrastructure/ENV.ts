import { UPDATE_CURRENT_ENV } from "@/shared/constants/ENV";

export default function configureENV() {
    UPDATE_CURRENT_ENV();
}
