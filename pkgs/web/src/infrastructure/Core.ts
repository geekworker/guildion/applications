import { getAppConfig } from '@/shared/constants/AppConfig';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import { configure } from '@guildion/core';

export default function configureCore() {
    configure({
        CURRENT_NODE_ENV: CURRENT_ENV.NODE_ENV,
        IS_MOBILE: false,
        GUILDION_API_SERVICE_HOST: getAppConfig().GUILDION_API_SERVICE_HOST,
        GUILDION_CONNECT_SERVICE_HOST: getAppConfig().GUILDION_CONNECT_SERVICE_HOST,
        GUILDION_CDN_SERVICE_HOST: getAppConfig().GUILDION_CDN_SERVICE_HOST,
        GUILDION_PCDN_SERVICE_HOST: getAppConfig().GUILDION_PCDN_SERVICE_HOST,
    });
}