import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { Document, DocumentsEndpoint, DocumentShowRequest, DocumentShowResponse } from '@guildion/core';
import { call } from 'redux-saga/effects';

export default class DocumentsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *get({ groupSlug, sectionSlug, slug }: { groupSlug: string, sectionSlug: string, slug: string }): SagaIterator<Document> {
        const result: DocumentShowResponse = yield call(BaseRepository.callAPI, 
            DocumentsEndpoint.Show,
            {
                data: new DocumentShowRequest({ 
                    groupSlug,
                    sectionSlug,
                    slug,
                }),
            }
        );
        return result.getDocument();
    }
}