import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { DocumentSection, Document, DocumentSectionsEndpoint, DocumentSectionShowRequest, DocumentSectionShowResponse } from '@guildion/core';
import { call } from 'redux-saga/effects';

export default class DocumentSectionsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *get({ groupSlug, sectionSlug }: { groupSlug: string, sectionSlug: string }): SagaIterator<{ section: DocumentSection, document: Document }> {
        const result: DocumentSectionShowResponse = yield call(BaseRepository.callAPI, 
            DocumentSectionsEndpoint.Show,
            {
                data: new DocumentSectionShowRequest({ 
                    slug: sectionSlug,
                    groupSlug,
                }),
            }
        );
        return {
            section: result.getSection(),
            document: result.getDocument().setRecord('section', result.getSection()),
        };
    }
}