import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { Device, DeviceRegisterRequest, DeviceRegisterResponse, DevicesEndpoint } from '@guildion/core';
import { call } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";

export default class DevicesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

   *register({ device }: { device: Device }): SagaIterator<{ device: Device, csrfSecret: string }> {
        const result: DeviceRegisterResponse = yield call(BaseRepository.callAPI, 
            DevicesEndpoint.Register,
            {
                data: new DeviceRegisterRequest({ 
                    device: device.toJSON(),
                }),
                // lang: CURRENT_CONFIG.DEFAULT_LANG,
            }
        );
        return {
            device: result.getDevice()!,
            csrfSecret: result.csrfSecret,
        };
    }
}