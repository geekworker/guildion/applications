import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { DocumentGroup, DocumentGroupsEndpoint, DocumentGroupShowRequest, DocumentGroupShowResponse } from '@guildion/core';
import { call } from 'redux-saga/effects';

export default class DocumentGroupsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *get({ groupSlug }: { groupSlug: string }): SagaIterator<DocumentGroup> {
        const result: DocumentGroupShowResponse = yield call(BaseRepository.callAPI, 
            DocumentGroupsEndpoint.Show,
            {
                data: new DocumentGroupShowRequest({ 
                    slug: groupSlug,
                }),
            }
        );
        return result.getGroup();
    }
}