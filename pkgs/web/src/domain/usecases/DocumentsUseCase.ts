import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import DocumentsRepository from '../repositories/DocumentsRepository';
import { SagaIterator } from 'redux-saga';
import { call } from 'redux-saga/effects';
import { Document } from '@guildion/core';

export default class DocumentsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchShow({ groupSlug, sectionSlug, slug }: { groupSlug: string, sectionSlug: string, slug: string }): SagaIterator<{ document: Document }> {
        const document: Document = yield call(new DocumentsRepository().get, { groupSlug, sectionSlug, slug });
        return {
            document,
        };
    }
}