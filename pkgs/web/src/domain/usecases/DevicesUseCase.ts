import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import { Device } from '@guildion/core';
import DevicesRepository from '../repositories/DevicesRepository';
import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";

export default class DevicesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *register({}: {}): SagaIterator<{ device: Device, csrfSecret: string }> {
        const device = new Device({
            // os: Platform.OS,
            // model: DeviceInfo.getModel(),
            // udid: DeviceInfo.getUniqueId(),
            // appVersion: DeviceInfo.getVersion(),
            // countryCode: CURRENT_CONFIG.DEFAULT_COUNTRY,
            // languageCode: CURRENT_CONFIG.DEFAULT_LANG,
            // timezone: CURRENT_CONFIG.DEFAULT_TIMEZONE,
        });
        const result: { device: Device, csrfSecret: string } = yield call(new DevicesRepository().register, { device: device });
        return { device: result.device, csrfSecret: result.csrfSecret };
    }
}