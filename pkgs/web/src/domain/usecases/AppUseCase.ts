import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";

export default class AppUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }
}