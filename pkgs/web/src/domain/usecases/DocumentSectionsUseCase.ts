import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import DocumentSectionsRepository from '../repositories/DocumentSectionsRepository';
import { SagaIterator } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import { DocumentSection, Document } from '@guildion/core';
import { DocumentAction } from '@/presentation/redux/Document/DocumentReducer';

export default class DocumentSectionsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchShow({ slug, groupSlug }: { slug: string, groupSlug: string }): SagaIterator<{ section: DocumentSection }> {
        const {
            section,
            document,
        }: {
            section: DocumentSection,
            document: Document,
        } = yield call(new DocumentSectionsRepository().get, { groupSlug, sectionSlug: slug });
        yield put(DocumentAction.setShow({ document, groupSlug, sectionSlug: slug }));
        return {
            section,
        };
    }
}