import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import DocumentGroupsRepository from '../repositories/DocumentGroupsRepository';
import { SagaIterator } from 'redux-saga';
import { call } from 'redux-saga/effects';
import { DocumentGroup } from '@guildion/core';

export default class DocumentGroupsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchShow({ slug }: { slug: string }): SagaIterator<{ group: DocumentGroup }> {
        const group: DocumentGroup = yield call(new DocumentGroupsRepository().get, { groupSlug: slug });
        return {
            group,
        };
    }
}