import RootHooks from "@/infrastructure/Root/hooks"

export const useAppTheme = RootHooks.useAppTheme;