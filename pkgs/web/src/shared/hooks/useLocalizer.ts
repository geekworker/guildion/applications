import { useRouter } from 'next/router';
import { fallbackLanguageCode, guardCase, LanguageCode, Localizer } from '@guildion/core';

export const useLocalizer = () => {
    const { locale } = useRouter();
    const lang = guardCase(locale, { fallback: fallbackLanguageCode, cases: LanguageCode });
    return new Localizer(lang);
};