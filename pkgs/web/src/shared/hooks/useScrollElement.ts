import React from 'react';

export const useScrollElement = <E extends HTMLElement>() => {
    const ref = React.useRef<E>(null);
    const [scrolling, setScrolling] = React.useState(false);
    const [scrollTop, setScrollTop] = React.useState(0);
    const [scrollHeight, setScrollHeight] = React.useState(0);
    React.useEffect(() => {
        if (!ref.current) return;
        ref.current.onscroll = (e: Event) => {
            if (!ref.current) return;
            setScrollTop(ref.current.scrollHeight);
            setScrollHeight(ref.current.scrollHeight);
            setScrolling(ref.current.scrollTop > scrollTop);
        }
    }, [ref.current]);
    return {
        scrolling,
        scrollTop,
        scrollHeight,
        ref,
    }
};