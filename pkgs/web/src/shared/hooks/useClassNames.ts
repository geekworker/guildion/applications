import { useState, useEffect } from 'react';
import cx, { Argument } from 'classnames';

export const useClassNames = (...args: Argument[]) => {
	const [className, setClassName] = useState(cx(args));
	useEffect(() => {
		setClassName(cx(args));
	}, [args]);
	return className;
};