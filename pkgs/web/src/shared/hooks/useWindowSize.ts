import React from "react";

export const useWindowSize = () => {
    const [windowSize, setWindowSize] = React.useState({
        width: 0,
        height: 0,
        widthDiff: 0,
        heightDiff: 0,
    });
    const [cacheWindowSize, setCacheWindowSize] = React.useState({
        width: 0,
        height: 0,
    });

    React.useEffect(() => {
        setCacheWindowSize({ ...windowSize })
    }, [windowSize.width, windowSize.height])

    const handleResize = React.useCallback(() => process.browser && setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
        widthDiff: Math.abs(cacheWindowSize.width - window.innerWidth),
        heightDiff: Math.abs(cacheWindowSize.height - window.innerHeight),
    }), [setWindowSize, cacheWindowSize, process.browser]);
  
    React.useEffect(() => {
        if (typeof window !== 'undefined') {
            window.addEventListener("resize", handleResize);
            handleResize();
            return () => window.removeEventListener("resize", handleResize);
        }
        return;
    }, [handleResize]);
    return windowSize;
}