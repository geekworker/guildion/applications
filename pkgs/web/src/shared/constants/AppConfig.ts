import getConfig from 'next/config';

export interface AppConfig {
    SSR: boolean,
    CSR: boolean,
    BUILD: boolean,
    GUILDION_API_SERVICE_HOST?: string,
    GUILDION_CONNECT_SERVICE_HOST?: string,
    GUILDION_CDN_SERVICE_HOST?: string,
    GUILDION_PCDN_SERVICE_HOST?: string,
    CLOUDFRONT_SECRET_HEADER?: string,
    CLOUDFRONT_SECRET_VALUE?: string,
};

export const getAppConfig = (): AppConfig => {
    const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();
    const CSR = !serverRuntimeConfig.SSR && !!publicRuntimeConfig.RUNTIME;
    const NONE_BUILD: boolean = !!process.env.NEXT_START || CSR;
    return {
        SSR: !!serverRuntimeConfig.SSR,
        CSR,
        BUILD: !NONE_BUILD,
        GUILDION_API_SERVICE_HOST: process.env.GUILDION_API_SERVICE_HOST,
        GUILDION_CONNECT_SERVICE_HOST: process.env.GUILDION_CONNECT_SERVICE_HOST,
        GUILDION_CDN_SERVICE_HOST: process.env.GUILDION_CDN_SERVICE_HOST,
        GUILDION_PCDN_SERVICE_HOST: process.env.GUILDION_PCDN_SERVICE_HOST,
        CLOUDFRONT_SECRET_HEADER: process.env.CLOUDFRONT_SECRET_HEADER,
        CLOUDFRONT_SECRET_VALUE: process.env.CLOUDFRONT_SECRET_VALUE,
    }
}