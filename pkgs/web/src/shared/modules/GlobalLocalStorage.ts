import { Record } from 'immutable';
import { guardAppTheme, AppThemeString, AppTheme } from '@guildion/ui';

export interface GlobalLocalStorageRecordAttributes {
    appTheme: AppThemeString,
    accessToken?: string,
}

export class GlobalLocalStorageRecord extends Record<GlobalLocalStorageRecordAttributes>({
    appTheme: AppThemeString.Dark,
    accessToken: undefined,
}) {
    public getTheme(): AppTheme | undefined {
        const result = this.get('appTheme');
        return result ? guardAppTheme(result) : undefined;
    }
}

export default class GlobalLocalStorage {
    public static PERSISTENCE_KEY: string = 'GlobalLocalStorage';
    private static $instance: GlobalLocalStorage;
    private $current: GlobalLocalStorageRecord = new GlobalLocalStorageRecord();
    private isSynced: boolean = false;

    public static instance(): GlobalLocalStorage {
        if (!this.$instance) {
            this.$instance = new GlobalLocalStorage(GlobalLocalStorage.instance);
        }
        return this.$instance;
    }
    
    constructor(caller: Function) {
        if (caller != GlobalLocalStorage.instance && GlobalLocalStorage.$instance) throw new Error('Singleton instance already exists');
        if (caller != GlobalLocalStorage.instance && !GlobalLocalStorage.$instance) throw new Error('Caller argument is incorrect');
    }

    public save(values: Partial<GlobalLocalStorageRecordAttributes>): boolean {
        try{
            const newRecord = new GlobalLocalStorageRecord({
                ...this.$current.toJSON(),
                ...values,
            });
            localStorage.setItem(GlobalLocalStorage.PERSISTENCE_KEY, JSON.stringify(newRecord.toJSON()));
            this.$current = newRecord;
            return true
        } catch(error) {
            return false;
        }
    }

    public read(): boolean {
        if (this.isSynced) return true;
        try{
            const result = localStorage.getItem(GlobalLocalStorage.PERSISTENCE_KEY);
            if (!!result) {
                this.$current = new GlobalLocalStorageRecord({ ...JSON.parse(result) })
                this.isSynced = true;
                return true   
            } else {
                this.isSynced = false;
                return false
            }
        } catch(error) {
            return false;
        }
    }

    public get current(): GlobalLocalStorageRecord {
        return this.$current;
    }
}

export const globalAsyncStorage: GlobalLocalStorage = GlobalLocalStorage.instance();