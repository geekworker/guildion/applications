export function scrollTo<E extends HTMLElement>(element: E, to: number, duration: number) {
    let start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

    const easeInOutQuad = function (t: number, b: number, c: number, d: number) {
        t /= d/2;
        if (t < 1) return c/2*t*t + b;
        t--;
        return -c/2 * (t*(t-2) - 1) + b;
    };    

    const animateScroll = function(){
        currentTime += increment;
        var val = easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if (currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}