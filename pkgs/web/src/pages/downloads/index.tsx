import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import Head from 'next/head';
import React from 'react';

type Props = {};

const DownloadsContainer: React.FC<Props> = ({}) => {
    const localizer = useLocalizer();
    return (
        <>
            <Head>
            </Head>
        </>
    );
};

export const getStaticProps = wrapper.getStaticProps(store => async _ => {
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
        },
    };
});

export default DownloadsContainer;