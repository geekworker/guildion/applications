import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import Head from 'next/head';
import React from 'react';
import { allCases, LanguageCode, OS } from '@guildion/core';

type Props = {
    os: OS,
};

const DownloadsContainer: React.FC<Props> = ({ os }) => {
    const localizer = useLocalizer();
    return (
        <>
            <Head>
            </Head>
        </>
    );
};

export const getStaticPaths = async () => {
    return {
        paths: [
            ...allCases(OS).map(os => { return { params: { os }, locale: LanguageCode.Japanese } }),
            ...allCases(OS).map(os => { return { params: { os }, locale: LanguageCode.English } }),
        ],
        fallback: false
    };
}

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (context: { params: { os: OS } }) => {
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
            os: context.params.os,
        },
    };
});

export default DownloadsContainer;