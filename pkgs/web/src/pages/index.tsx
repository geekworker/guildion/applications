import { wrapper } from '@/presentation/redux/MakeStore';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import React from 'react';
import Home from '@/presentation/components/pages/Home';
import OGP from './OGP';

type Props = {};

const HomeContainer: React.FC<Props> = ({}) => {
    const localizer = useLocalizer();
    return (
        <>
            <OGP
                description={localizer.dic.home.seo.description}
            />
            <Home/>
        </>
    );
};

export const getStaticProps = wrapper.getStaticProps(store => async _ => {
    return {
        props: {
        },
    };
});

export default HomeContainer;
