import React from 'react';
import { compare } from '@/shared/modules/ObjectCompare';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import classNames from 'classnames';
import { appThemeToString } from '@guildion/ui';
import Head from 'next/head';
import Header from '@/presentation/components/molecules/Header';
import Footer from '@/presentation/components/molecules/Footer';
import { useLocalizer } from '../../shared/hooks/useLocalizer';

type Props = {
    children?: React.ReactNode,
};

const Layout: React.FC<Props> = ({ children }) => {
    const appTheme = useAppTheme();
    const appThemeString = React.useMemo(() => appThemeToString(appTheme), [appTheme]);
    const localizer = useLocalizer();
    return (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1, shrink-to-fit=no"
                />
                <style type="text/css">
                    {`html { background-color: ${appTheme.background.most}; }`}
                </style>
            </Head>
            <div className={classNames('theme', 'lang')} data-theme={appThemeString} data-lang={localizer.lang}>
                <Header/>
                {children}
                <Footer/>
            </div>
        </>
    );
};

export default React.memo(Layout, compare);