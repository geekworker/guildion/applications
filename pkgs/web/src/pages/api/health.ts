// import { healthCheckAPI, healthCheckConnect, HTTPStatusCode } from "@guildion/core";
// import type { NextApiRequest, NextApiResponse } from 'next';
// import configureCore from '@/infrastructure/Core';
// import configureENV from '@/infrastructure/ENV';

// export default async function handler(req: NextApiRequest, res: NextApiResponse) {
//     configureENV();
//     configureCore();
//     const apiHealthy = await healthCheckAPI();
//     const connectHealthy = await healthCheckConnect();
//     res.status(
//         apiHealthy && connectHealthy ? HTTPStatusCode.OK : HTTPStatusCode.SERVICE_UNAVAILABLE
//     ).json({
//         healthStatus: apiHealthy && connectHealthy ? 'healthy' : 'unhealthy',
//         healthStatusAPI: apiHealthy ? 'healthy' : 'unhealthy',
//         healthStatusWeb: 'healthy',
//         healthStatusConnect: connectHealthy ? 'healthy' : 'unhealthy',
//     });
// };

import { HTTPStatusCode } from "@guildion/core";
import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    res.status(
        HTTPStatusCode.OK
    ).json({
        healthStatus: 'healthy',
    });
};