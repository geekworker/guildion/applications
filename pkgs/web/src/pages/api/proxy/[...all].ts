import { NextApiRequest, NextApiResponse } from "next";
import httpProxyMiddleware from 'next-http-proxy-middleware';
import configureCore from '@/infrastructure/Core';
import configureENV from '@/infrastructure/ENV';
import { LOCAL_API_PORT, LOCAL_CONNECT_PORT } from "@guildion/core";
import { getAppConfig } from "@/shared/constants/AppConfig";

export const config = {
    api: {
        bodyParser: false,
        externalResolver: true,
    },
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyAPI = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    const proxy = httpProxyMiddleware(req, res, {
        target: getAppConfig().GUILDION_API_SERVICE_HOST ? `http://${getAppConfig().GUILDION_API_SERVICE_HOST}` : `http://127.0.0.1:${LOCAL_API_PORT}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyAPIHealth = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    const proxy = httpProxyMiddleware(req, res, {
        target: getAppConfig().GUILDION_API_SERVICE_HOST ? `http://${getAppConfig().GUILDION_API_SERVICE_HOST}` : `http://127.0.0.1:${LOCAL_API_PORT}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/api/v1',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyWebsocket = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    const proxy = httpProxyMiddleware(req, res, {
        target: getAppConfig().GUILDION_CONNECT_SERVICE_HOST ? `http://${getAppConfig().GUILDION_CONNECT_SERVICE_HOST}` : `http://127.0.0.1:${LOCAL_CONNECT_PORT}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/connect/v1',
            replaceStr: ''
        }],
        ws: true,
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyConnect = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    const proxy = httpProxyMiddleware(req, res, {
        target: getAppConfig().GUILDION_CONNECT_SERVICE_HOST ? `http://${getAppConfig().GUILDION_CONNECT_SERVICE_HOST}` : `http://127.0.0.1:${LOCAL_CONNECT_PORT}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/connect/v1',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyCDN = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    const { CLOUDFRONT_SECRET_HEADER, CLOUDFRONT_SECRET_VALUE } = getAppConfig();
    if (!CLOUDFRONT_SECRET_HEADER || !CLOUDFRONT_SECRET_VALUE || !getAppConfig().GUILDION_CDN_SERVICE_HOST) {
        res.status(404).send(null);
        return;
    }
    const secret = req.headers[CLOUDFRONT_SECRET_HEADER.toLowerCase()];
    if (secret !== CLOUDFRONT_SECRET_VALUE) {
        res.status(404).send(null);
        return;
    }
    const proxy = httpProxyMiddleware(req, res, {
        target: `http://${getAppConfig().GUILDION_CDN_SERVICE_HOST}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/cdn',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyCDNStatus = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    if (!getAppConfig().GUILDION_CDN_SERVICE_HOST) {
        res.status(404).send(null);
        return;
    }
    const proxy = httpProxyMiddleware(req, res, {
        target: `http://${getAppConfig().GUILDION_CDN_SERVICE_HOST}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/pcdn',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyPCDN = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    const { CLOUDFRONT_SECRET_HEADER, CLOUDFRONT_SECRET_VALUE } = getAppConfig();
    if (!CLOUDFRONT_SECRET_HEADER || !CLOUDFRONT_SECRET_VALUE || !getAppConfig().GUILDION_PCDN_SERVICE_HOST) {
        res.status(404).send(null);
        return;
    }
    const secret = req.headers[CLOUDFRONT_SECRET_HEADER.toLowerCase()];
    if (secret !== CLOUDFRONT_SECRET_VALUE) {
        res.status(404).send(null);
        return;
    }
    const proxy = httpProxyMiddleware(req, res, {
        target: `http://${getAppConfig().GUILDION_PCDN_SERVICE_HOST}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/pcdn',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxyPCDNStatus = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    if (!getAppConfig().GUILDION_PCDN_SERVICE_HOST) {
        res.status(404).send(null);
        return;
    }
    const proxy = httpProxyMiddleware(req, res, {
        target: `http://${getAppConfig().GUILDION_PCDN_SERVICE_HOST}`,
        changeOrigin: true,
        pathRewrite: [{
            patternStr: '^/api/proxy/pcdn',
            replaceStr: ''
        }],
    });
    return proxy;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getProxy = async (req: NextApiRequest, res: NextApiResponse) : Promise<any> => {
    if (req.url?.startsWith('/api/proxy/connect/v1/connection/create')) {
        return getProxyWebsocket(req, res);
    } else if (req.url?.startsWith('/api/proxy/connect/v1')) {
        return getProxyConnect(req, res);
    } else if (req.url?.startsWith('/api/proxy/api/v1/health')) {
        return getProxyAPIHealth(req, res);
    } else if (req.url?.startsWith('/api/proxy/api/v1')) {
        return getProxyAPI(req, res);
    } else if (req.url && (req.url == '/api/proxy/cdn/status' || req.url == '/api/proxy/cdn/status/')) {
        return getProxyCDNStatus(req, res);
    } else if (req.url && (req.url == '/api/proxy/pcdn/status' || req.url == '/api/proxy/pcdn/status/')) {
        return getProxyPCDNStatus(req, res);
    } else if (req.url?.startsWith('/api/proxy/cdn')) {
        return getProxyCDN(req, res);
    } else if (req.url?.startsWith('/api/proxy/pcdn')) {
        return getProxyPCDN(req, res);
    } else {
        res.status(404).send(null);
        return;
    }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    configureENV();
    configureCore();
    console.log(`[WEB PROXY] ${req.method} <-- ${req.url} ${JSON.stringify(req.headers, null, 2)} ${req.body}`);
    const proxy = await getProxy(req, res);
    return proxy;
}