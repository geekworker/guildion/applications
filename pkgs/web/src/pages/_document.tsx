import { CDN_PROXY_URL_STRING, IS_PROD, PCDN_PROXY_URL_STRING } from '@guildion/core';
import NextDocument, { Html, Head, Main, NextScript } from 'next/document';
import configureCore from '@/infrastructure/Core';
import configureENV from '@/infrastructure/ENV';
import { CURRENT_ENV } from '../shared/constants/ENV';

configureENV();
configureCore();

type Props = {};

class Document extends NextDocument<Props> {
    render() {
        return (
            <Html>
                <Head>
                    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
                    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
                    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
                    <link rel="manifest" href="/site.webmanifest"/>
                    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#1f1f1f"/>
                    <link rel="shortcut icon" href="/favicon.ico" />
                    <meta name="msapplication-TileColor" content="#1f1f1f"/>
                    <meta name="theme-color" content="#1f1f1f"/>
                    <link rel="preconnect" href="https://www.google-analytics.com" />
                    <link rel="preconnect" href="https://www.googletagmanager.com" />
                    <link rel="preconnect" href={CDN_PROXY_URL_STRING} />
                    <link rel="preconnect" href={PCDN_PROXY_URL_STRING} />
                    <link
                        href="/fonts/Montserrat-Medium.woff2"
                        rel="preconnect"
                        as="font"
                        type="font/woff2"
                        crossOrigin="anonymous"
                    />
                    <link
                        href="/fonts/Montserrat-Bold.woff2"
                        rel="preconnect"
                        as="font"
                        type="font/woff2"
                        crossOrigin="anonymous"
                    />
                    <link
                        href="/fonts/ZenKakuGothicAntique-Regular.woff2"
                        rel="preconnect"
                        as="font"
                        type="font/woff2"
                        crossOrigin="anonymous"
                    />
                    <link
                        href="/fonts/ZenKakuGothicAntique-Bold.woff2"
                        rel="preconnect"
                        as="font"
                        type="font/woff2"
                        crossOrigin="anonymous"
                    />
                    {IS_PROD() && CURRENT_ENV.GOOGLE_ANALYTICS_ID && (
                        <script async src={`https://www.googletagmanager.com/gtag/js?id=${CURRENT_ENV.GOOGLE_ANALYTICS_ID}`}></script>
                    )}
                    {IS_PROD() && CURRENT_ENV.GOOGLE_ANALYTICS_ID && (
                        <script
                            async
                            dangerouslySetInnerHTML={{
                                __html: `
                                    window.dataLayer = window.dataLayer || [];
                                    function gtag(){dataLayer.push(arguments);}
                                    gtag('js', new Date());
                                
                                    gtag('config', '${CURRENT_ENV.GOOGLE_ANALYTICS_ID}');
                                `,
                            }}
                        ></script>
                    )}
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
};

export default Document;