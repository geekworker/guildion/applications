import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '../../shared/hooks/useClassNames';
import { useAppTheme } from '../../shared/hooks/useAppTheme';
import Custom404Icon from '../../presentation/components/icons/Custom404Icon';
import Custom404Text from '@/presentation/components/icons/Custom404Text';

type Props = {
} & LayoutProps;

const Custom404: React.FC<Props> = ({
    style,
    className,
}) => {
    const containerClassName = useClassNames(styles['container'], className);
    const appTheme = useAppTheme();
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['text']}>
                <Custom404Text/>
            </div>
            <div className={styles['icon']}>
                <Custom404Icon width={300} height={200} color={appTheme.element.subp1} />
            </div>
        </div>
    );
};

export default React.memo(Custom404, compare);