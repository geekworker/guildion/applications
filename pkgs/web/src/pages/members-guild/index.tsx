import { wrapper } from '@/presentation/redux/MakeStore';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import Head from 'next/head';
import React from 'react';

type Props = {};

const MembersGuildContainer: React.FC<Props> = ({}) => {
    const localizer = useLocalizer();
    return (
        <>
            <Head>
            </Head>
        </>
    );
};

export const getStaticProps = wrapper.getStaticProps(store => async _ => {
    return {
        props: {
        },
    };
});

export default MembersGuildContainer;