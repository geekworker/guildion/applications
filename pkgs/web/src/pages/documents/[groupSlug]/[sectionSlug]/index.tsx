import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import React from 'react';
import { DocumentSectionsEndpoint, DocumentSectionsShowPathsRequest, healthCheckAPI, requestAPI } from '@guildion/core';
import { DocumentSectionAction } from '@/presentation/redux/DocumentSection/DocumentSectionReducer';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import DocumentSectionShow from '@/presentation/components/pages/DocumentSectionShow';
import { DocumentGroupAction } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';
import HeaderHooks from '@/presentation/components/molecules/Header/hooks';
import FooterHooks from '@/presentation/components/molecules/Footer/hooks';
import { getAppConfig } from '@/shared/constants/AppConfig';

type Props = {
    rebuild?: boolean,
    groupSlug: string,
    sectionSlug: string,
};

const DocumentSectionShowContainer: React.FC<Props> = ({ rebuild, groupSlug, sectionSlug }) => {
    if (rebuild && process.browser && !!window) window.location.reload();
    HeaderHooks.useFixed();
    FooterHooks.useDocument();
    return (
        <>
            <DocumentSectionShow groupSlug={groupSlug} sectionSlug={sectionSlug} />
        </>
    );
};

export const getStaticPaths = async () => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return { paths: [], fallback: true };
    const result = await requestAPI(DocumentSectionsEndpoint.ShowPaths, {
        data: new DocumentSectionsShowPathsRequest({}),
    })
    return { paths: result.paths, fallback: false };
}

const revalidate =
    CURRENT_ENV.NODE_ENV === "staging" ||
    CURRENT_ENV.NODE_ENV === "production"
        ? 86400 * 1
        : 600;

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (context: { params: { groupSlug: string, sectionSlug: string } }) => {
    const { groupSlug, sectionSlug } = context.params;
    if (getAppConfig().BUILD || !await healthCheckAPI()) return {
        props: {
            rebuild: true,
            groupSlug,
            sectionSlug,
        },
        revalidate: 1,
    };
    store.dispatch(DocumentSectionAction.fetchShow.started({ slug: sectionSlug, groupSlug }));
    store.dispatch(DocumentGroupAction.fetchShow.started({ slug: groupSlug }));
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
            groupSlug,
            sectionSlug,
        },
        revalidate,
    };
});

export default DocumentSectionShowContainer;