import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import React from 'react';
import { DocumentsEndpoint, DocumentsShowPathsRequest, healthCheckAPI, requestAPI } from '@guildion/core';
import { DocumentAction } from '@/presentation/redux/Document/DocumentReducer';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import DocumentShow from '@/presentation/components/pages/DocumentShow';
import { DocumentGroupAction } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';
import FooterHooks from '@/presentation/components/molecules/Footer/hooks';
import HeaderHooks from '@/presentation/components/molecules/Header/hooks';
import { getAppConfig } from '@/shared/constants/AppConfig';

type Props = {
    rebuild?: boolean,
    groupSlug: string,
    sectionSlug: string,
    slug: string,
};

const DocumentShowContainer: React.FC<Props> = ({ rebuild, groupSlug, sectionSlug, slug }) => {
    if (rebuild && process.browser && !!window) window.location.reload();
    HeaderHooks.useFixed();
    FooterHooks.useDocument();
    return (
        <>
            <DocumentShow groupSlug={groupSlug} sectionSlug={sectionSlug} slug={slug} />
        </>
    );
};

export const getStaticPaths = async () => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return { paths: [], fallback: true };
    const result = await requestAPI(DocumentsEndpoint.ShowPaths, {
        data: new DocumentsShowPathsRequest({}),
    })
    return { paths: result.paths, fallback: false };
}

const revalidate =
    CURRENT_ENV.NODE_ENV === "staging" ||
    CURRENT_ENV.NODE_ENV === "production"
        ? 86400 * 1
        : 600;

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (context: { params: { groupSlug: string, sectionSlug: string, slug: string } }) => {
    const { groupSlug, sectionSlug, slug } = context.params;
    if (getAppConfig().BUILD || !await healthCheckAPI()) return {
        props: {
            rebuild: true,
            groupSlug,
            sectionSlug,
            slug,
        },
        revalidate: 1,
    };
    store.dispatch(DocumentAction.fetchShow.started({ slug, sectionSlug, groupSlug }));
    store.dispatch(DocumentGroupAction.fetchShow.started({ slug: groupSlug }));
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
            groupSlug,
            sectionSlug,
            slug,
        },
        revalidate,
    };
});

export default DocumentShowContainer;