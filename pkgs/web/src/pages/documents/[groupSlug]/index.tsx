import { wrapper } from '@/presentation/redux/MakeStore';
import { END } from 'redux-saga';
import React from 'react';
import { DocumentGroupsEndpoint, DocumentGroupsShowPathsRequest, healthCheckAPI, requestAPI } from '@guildion/core';
import { DocumentGroupAction } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';
import { CURRENT_ENV } from '@/shared/constants/ENV';
import DocumentGroupShow from '@/presentation/components/pages/DocumentGroupShow';
import { getAppConfig } from '@/shared/constants/AppConfig';

type Props = {
    rebuild?: boolean,
    groupSlug: string,
};

const DocumentGroupShowContainer: React.FC<Props> = ({ rebuild, groupSlug }) => {
    if (rebuild && process.browser && !!window) window.location.reload();
    return (
        <>
            <DocumentGroupShow groupSlug={groupSlug} />
        </>
    );
};

export const getStaticPaths = async () => {
    if (getAppConfig().BUILD || !await healthCheckAPI()) return { paths: [], fallback: true };
    const result = await requestAPI(DocumentGroupsEndpoint.ShowPaths, {
        data: new DocumentGroupsShowPathsRequest({}),
    })
    return { paths: result.paths, fallback: false };
}

const revalidate =
    CURRENT_ENV.NODE_ENV === "staging" ||
    CURRENT_ENV.NODE_ENV === "production"
        ? 86400 * 1
        : 600;

export const getStaticProps = wrapper.getStaticProps<Props>(store => async (context: { params: { groupSlug: string } }) => {
    const { groupSlug } = context.params;
    if (getAppConfig().BUILD || !await healthCheckAPI()) return {
        props: {
            rebuild: true,
            groupSlug,
        },
        revalidate: 1,
    };
    store.dispatch(DocumentGroupAction.fetchShow.started({ slug: groupSlug }));
    store.dispatch(END);
    await store.sagaTask.toPromise();
    return {
        props: {
            groupSlug,
        },
        revalidate,
    };
});

export default DocumentGroupShowContainer;