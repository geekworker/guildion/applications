import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';
import { DocumentGroupPageStates } from './DocumentGroupReducer';

export const documentGroupShowPageStatesSelector = (state: RootState) => state.documentGroup.pages.show;
export const documentGroupShowPageSelector = createSelector(
    documentGroupShowPageStatesSelector,
    (_: RootState, slug: string) => slug,
    (show: DocumentGroupPageStates, slug: string) => show.find(p => p.get('group').slug == slug)
);