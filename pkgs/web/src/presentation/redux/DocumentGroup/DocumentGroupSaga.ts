import DocumentGroupsUseCase from '@/domain/usecases/DocumentGroupsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { DocumentGroupAction } from './DocumentGroupReducer';

export default function* DocumentGroupSaga() {
    yield takeEvery(DocumentGroupAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(DocumentGroupAction.fetchShow, { skipStartedAction: true })(new DocumentGroupsUseCase().fetchShow), payload)
    });
}