import documentGroupReducer from './DocumentGroupReducer';
import documentGroupSaga from './DocumentGroupSaga';

export { documentGroupReducer, documentGroupSaga };