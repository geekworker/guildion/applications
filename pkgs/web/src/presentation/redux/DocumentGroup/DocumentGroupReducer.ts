import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record, List, Map } from 'immutable';
import { DocumentGroup, ImmutableMap, safeJSONParse } from '@guildion/core';
import { GlobalAction } from '../GlobalAction';

const actionCreator = actionCreatorFactory('DocumentGroup');

export type DocumentGroupPageStateAttributes = { group: DocumentGroup, loading?: boolean, paginatable?: boolean, success?: boolean };
export type DocumentGroupPageState = ImmutableMap<DocumentGroupPageStateAttributes>;
export type DocumentGroupPageStates = List<DocumentGroupPageState>;

export const DocumentGroupAction = {
    fetchShow: actionCreator.async<{ slug: string }, { group: DocumentGroup }>('DOCUMENT_FETCH_SHOW'),
};

export type DocumentGroupAction = typeof DocumentGroupAction[keyof typeof DocumentGroupAction];

export class DocumentGroupState extends Record<{
    pages: {
        show: DocumentGroupPageStates,
    },
}>({
    pages: {
        show: List([]),
    },
}) {
    static deserializeState(state: any) {
        const parsed = safeJSONParse<ReturnType<DocumentGroupState['toJSON']>>(state);
        return new DocumentGroupState({
            ...parsed,
            pages: {
                show: parsed?.pages.show ? List(
                    parsed.pages.show.map(
                        (s: any) => Map({ ...s, group: new DocumentGroup(s.group) })
                    )
                ) : List([]),
            },
        });
    }
    public serializeState(): string {
        return JSON.stringify({
            ...this.toJSON(),
            show: this.pages.show.toArray().map(s => s.toJSON()).map(s => { return { ...s, group: s.group.toJSON() } }),
        });
    }
}

const DocumentGroupReducer = reducerWithInitialState(new DocumentGroupState())
    .case(GlobalAction.HYDRATE, (state, payload) => {
        return payload.documentGroup;
    })
    .case(DocumentGroupAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('group').slug == payload.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ group: new DocumentGroup({ slug: payload.slug }), loading: true, success: false }));
        } else {
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentGroupAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('group').slug == payload.params.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ document: payload.result.group, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('group', payload.result.group);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentGroupAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('group').slug == payload.params.slug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ group: new DocumentGroup({ slug: payload.params.slug }), loading: false, success: false }));
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })

export default DocumentGroupReducer;