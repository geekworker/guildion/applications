import type { NextPageContext } from 'next/types';
import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware, { Task } from 'redux-saga';
import { createWrapper } from 'next-redux-wrapper';
import { rootReducer, RootState } from './RootReducer';
import { rootSaga } from './RootSaga';
import { Middleware } from 'redux';
import { StoreEnhancer } from 'redux';
import { Store } from 'redux';
import { AppState } from './App/AppReducer';
import { DeviceState } from './Device/DeviceReducer';
import { IS_DEV } from '@guildion/core';
import { DocumentState } from './Document/DocumentReducer';
import { DocumentGroupState } from './DocumentGroup/DocumentGroupReducer';
import { DocumentSectionState } from './DocumentSection/DocumentSectionReducer';

export type SagaStore = Store<RootState> & { sagaTask: Task }

const bindMiddleware = (middleware: Middleware<any, any, any>[]): StoreEnhancer => {
    if (IS_DEV()) {
        const { composeWithDevTools } = require('redux-devtools-extension')
        return composeWithDevTools(applyMiddleware(...middleware))
    }
    return applyMiddleware(...middleware)
}

export const makeStore = (context: NextPageContext<any>): SagaStore => {
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore(rootReducer(), bindMiddleware([sagaMiddleware])) as SagaStore;
    store.sagaTask = sagaMiddleware.run(rootSaga);
    return store
}

export const makeStoreWithoutContext = (): SagaStore => {
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore(rootReducer(), bindMiddleware([sagaMiddleware])) as SagaStore;
    store.sagaTask = sagaMiddleware.run(rootSaga);
    return store
}

export const wrapper = createWrapper(makeStore, {
    debug: IS_DEV(),
    serializeState: state => {
        return {
            app: state.app.serializeState(),
            device: state.device.serializeState(),
            document: state.document.serializeState(),
            documentGroup: state.documentGroup.serializeState(),
            documentSection: state.documentSection.serializeState(),
        };
    },
    deserializeState: state => {
        return {
            app: AppState.deserializeState(state.app),
            device: DeviceState.deserializeState(state.device),
            document: DocumentState.deserializeState(state.document),
            documentGroup: DocumentGroupState.deserializeState(state.documentGroup),
            documentSection: DocumentSectionState.deserializeState(state.documentSection),
        };
    }
});