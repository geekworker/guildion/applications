import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { ImmutableMap, safeJSONParse, Document } from '@guildion/core';
import { GlobalAction } from '../GlobalAction';

const actionCreator = actionCreatorFactory('Document');

export type DocumentPageStateAttributes = { document: Document, groupSlug: string, sectionSlug: string, loading?: boolean, paginatable?: boolean, success?: boolean };
export type DocumentPageState = ImmutableMap<DocumentPageStateAttributes>;
export type DocumentPageStates = List<DocumentPageState>;

export const DocumentAction = {
    fetchShow: actionCreator.async<{ groupSlug: string, sectionSlug: string, slug: string }, { document: Document }>('DOCUMENT_FETCH_SHOW'),
    setShow: actionCreator<{ document: Document, groupSlug: string, sectionSlug: string }>('DOCUMENT_SET_SHOW'),
};

export type DocumentAction = typeof DocumentAction[keyof typeof DocumentAction];

export class DocumentState extends Record<{
    pages: {
        show: DocumentPageStates,
    },
}>({
    pages: {
        show: List([]),
    },
}) {
    static deserializeState(state: any) {
        const parsed = safeJSONParse<ReturnType<DocumentState['toJSON']>>(state);
        return new DocumentState({
            ...parsed,
            pages: {
                show: parsed?.pages.show ? List(
                    parsed.pages.show.map(
                        (s: any) => Map({ ...s, document: new Document(s.document) })
                    )
                ) : List([]),
            },
        });
    }
    public serializeState(): string {
        return JSON.stringify({
            ...this.toJSON(),
            pages: {
                show: this.pages.show.toArray().map(s => s.toJSON()).map(s => { return { ...s, document: s.document.toJSON() } }),
            },
        });
    }
}

const DocumentReducer = reducerWithInitialState(new DocumentState())
    .case(GlobalAction.HYDRATE, (state, payload) => {
        return payload.document;
    })
    .case(DocumentAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('document').slug == payload.slug && p.get('sectionSlug') == payload.sectionSlug && p.get('groupSlug') == payload.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ document: new Document({ slug: payload.slug }), groupSlug: payload.groupSlug, sectionSlug: payload.sectionSlug, loading: true, success: false }));
        } else {
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('document').slug == payload.params.slug && p.get('sectionSlug') == payload.params.sectionSlug && p.get('groupSlug') == payload.params.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ document: payload.result.document, groupSlug: payload.params.groupSlug, sectionSlug: payload.params.sectionSlug, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('document', payload.result.document);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('document').slug == payload.params.slug && p.get('sectionSlug') == payload.params.sectionSlug && p.get('groupSlug') == payload.params.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ document: new Document({ slug: payload.params.slug }), groupSlug: payload.params.groupSlug, sectionSlug: payload.params.sectionSlug, loading: false, success: false }));
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentAction.setShow, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('document').slug == payload.document.slug && p.get('sectionSlug') == payload.sectionSlug && p.get('groupSlug') == payload.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ document: payload.document, groupSlug: payload.groupSlug, sectionSlug: payload.sectionSlug, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('document', payload.document);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })

export default DocumentReducer;