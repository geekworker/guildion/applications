import documentReducer from './DocumentReducer';
import documentSaga from './DocumentSaga';

export { documentReducer, documentSaga };