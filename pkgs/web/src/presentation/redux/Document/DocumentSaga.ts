import DocumentsUseCase from '@/domain/usecases/DocumentsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { DocumentAction } from './DocumentReducer';

export default function* DocumentSaga() {
    yield takeEvery(DocumentAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(DocumentAction.fetchShow, { skipStartedAction: true })(new DocumentsUseCase().fetchShow), payload)
    });
}