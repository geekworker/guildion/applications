import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';
import { DocumentPageStates } from './DocumentReducer';

export const documentShowPageStatesSelector = (state: RootState) => state.document.pages.show;
export const documentShowPageSelector = createSelector(
    documentShowPageStatesSelector,
    (_: RootState, { slug, groupSlug, sectionSlug }: { slug: string, groupSlug: string, sectionSlug: string }) => { return { slug, groupSlug, sectionSlug } },
    (show: DocumentPageStates, { slug, groupSlug, sectionSlug }: { slug: string, groupSlug: string, sectionSlug: string }) => show.find(p => p.get('document').slug == slug && p.get('groupSlug') == groupSlug && p.get('sectionSlug') == sectionSlug)
);