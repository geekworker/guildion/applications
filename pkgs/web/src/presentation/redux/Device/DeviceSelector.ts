import { RootState } from '../RootReducer';

export const csrfSecretSelector = (state: RootState) => state.device.csrfSecret;
export const currentDeviceSelector = (state: RootState) => state.device.currentDevice;