import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Device, safeJSONParse } from '@guildion/core';
import { GlobalAction } from '../GlobalAction';

const actionCreator = actionCreatorFactory('Device');

export const DeviceAction = {
    register: actionCreator.async<{}, { device: Device, csrfSecret: string }>('REGISTER'),
    setDevice: actionCreator<{ device: Device, csrfSecret: string }>('SET_DEVICE'),
};

export type DeviceAction = typeof DeviceAction[keyof typeof DeviceAction];

export class DeviceState extends Record<{
    currentDevice?: Device,
    csrfSecret?: string,
    deviceChecked: boolean,
}>({
    currentDevice: undefined,
    csrfSecret: undefined,
    deviceChecked: false,
}) {
    static deserializeState(state: any) {
        const parsed = safeJSONParse<ReturnType<DeviceState['toJSON']>>(state);
        return new DeviceState({
            ...parsed,
            currentDevice: parsed?.currentDevice ? new Device(parsed.currentDevice) : undefined,
        });
    }
    public serializeState(): string {
        return JSON.stringify({
            ...this.toJSON(),
            currentDevice: this.currentDevice?.toJSON(),
        });
    }
}

const DeviceReducer = reducerWithInitialState(new DeviceState())
    .case(GlobalAction.HYDRATE, (state, payload) => {
        return payload.device;
    })
    .case(DeviceAction.register.done, (state, payload) => {
        state = state.set('currentDevice', payload.result.device);
        state = state.set('csrfSecret', payload.result.csrfSecret);
        state = state.set('deviceChecked', true);
        return state;
    })
    .case(DeviceAction.register.failed, (state, payload) => {
        state = state.set('deviceChecked', true);
        return state;
    })
    .case(DeviceAction.setDevice, (state, payload) => {
        state = state.set('currentDevice', payload.device);
        state = state.set('csrfSecret', payload.csrfSecret);
        state = state.set('deviceChecked', true);
        return state;
    })

export default DeviceReducer;