import DevicesUseCase from '@/domain/usecases/DevicesUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { DeviceAction } from './DeviceReducer';

export default function* DeviceSaga() {
    yield takeEvery(DeviceAction.register.started, function*({ payload }) {
        yield call(bindAsyncAction(DeviceAction.register, { skipStartedAction: true })(new DevicesUseCase().register), payload)
    })
}