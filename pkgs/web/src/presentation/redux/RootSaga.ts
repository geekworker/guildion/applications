import { all, fork } from 'redux-saga/effects';
import { appSaga } from '@/presentation/redux/App';
import { deviceSaga } from '@/presentation/redux/Device';
import { documentSaga } from './Document';
import { documentGroupSaga } from './DocumentGroup';
import { documentSectionSaga } from './DocumentSection';

export const rootSaga = function* root() {
    yield all([
        fork(appSaga),
        fork(deviceSaga),
        fork(documentSaga),
        fork(documentGroupSaga),
        fork(documentSectionSaga),
    ]);
};
