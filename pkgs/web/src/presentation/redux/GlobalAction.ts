import { ActionCreator } from 'typescript-fsa';
import { HYDRATE } from 'next-redux-wrapper';
import { RootState } from './RootReducer';

export const GlobalAction = {
    HYDRATE: { type: HYDRATE } as ActionCreator<RootState>,
};

export type GlobalAction = typeof GlobalAction;