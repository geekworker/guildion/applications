import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Record } from 'immutable';
import { AppError, ErrorType, NetworkStatus, safeJSONParse } from '@guildion/core';
import { GlobalAction } from '../GlobalAction';

const actionCreator = actionCreatorFactory('App');

export const AppAction = {
    displayErrors: actionCreator<{ errors: List<AppError> }>('DISPLAY_ERRORS'),
    resetErrors: actionCreator('RESET_ERRORS'),
    resetGlobalErrors: actionCreator('RESET_GLOBAL_ERRORS'),
    resetAppErrors: actionCreator('RESET_APP_ERRORS'),
};

export type AppAction = typeof AppAction[keyof typeof AppAction];

export class AppState extends Record<{
    errors: List<AppError>,
    isLaunched: boolean,
    networkStatus: NetworkStatus,
    csr: boolean,
}>({
    errors: List([]),
    isLaunched: false,
    networkStatus: NetworkStatus.Stable,
    csr: false,
}) {
    static deserializeState(state: any) {
        // const parsed = safeJSONParse<ReturnType<AppState['toJSON']>>(state);
        return new AppState({
            csr: true,
        });
    }
    public serializeState(): string {
        return JSON.stringify({
            ...this.toJSON(),
            errors: this.errors.toJSON(),
        });
    }
};

const AppReducer = reducerWithInitialState(new AppState())
    .case(GlobalAction.HYDRATE, (state, payload) => {
        return payload.app;
    })
    .case(AppAction.displayErrors, (state, payload) => {
        state = state.set('errors', payload.errors);
        return state;
    })
    .case(AppAction.resetErrors, (state) => {
        state = state.set('errors', List([]));
        return state;
    })
    .case(AppAction.resetGlobalErrors, (state) => {
        state = state.set('errors', state.errors.filter(e => e.type != ErrorType.global));
        return state;
    })
    .case(AppAction.resetAppErrors, (state) => {
        state = state.set('errors', state.errors.filter(e => e.type == ErrorType.global));
        return state;
    })

export default AppReducer;