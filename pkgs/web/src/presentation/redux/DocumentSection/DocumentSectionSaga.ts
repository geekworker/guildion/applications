import DocumentSectionsUseCase from '@/domain/usecases/DocumentSectionsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { DocumentSectionAction } from './DocumentSectionReducer';

export default function* DocumentSectionSaga() {
    yield takeEvery(DocumentSectionAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(DocumentSectionAction.fetchShow, { skipStartedAction: true })(new DocumentSectionsUseCase().fetchShow), payload)
    });
}