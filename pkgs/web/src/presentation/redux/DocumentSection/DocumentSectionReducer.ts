import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record, List, Map } from 'immutable';
import { DocumentSection, ImmutableMap, safeJSONParse } from '@guildion/core';
import { GlobalAction } from '../GlobalAction';

const actionCreator = actionCreatorFactory('DocumentSection');

export type DocumentSectionPageStateAttributes = { section: DocumentSection, groupSlug: string, loading?: boolean, paginatable?: boolean, success?: boolean };
export type DocumentSectionPageState = ImmutableMap<DocumentSectionPageStateAttributes>;
export type DocumentSectionPageStates = List<DocumentSectionPageState>;

export const DocumentSectionAction = {
    fetchShow: actionCreator.async<{ slug: string, groupSlug: string }, { section: DocumentSection }>('DOCUMENT_SECTION_FETCH_SHOW'),
};

export type DocumentSectionAction = typeof DocumentSectionAction[keyof typeof DocumentSectionAction];

export class DocumentSectionState extends Record<{
    pages: {
        show: DocumentSectionPageStates,
    },
}>({
    pages: {
        show: List([]),
    },
}) {
    static deserializeState(state: any) {
        const parsed = safeJSONParse<ReturnType<DocumentSectionState['toJSON']>>(state);
        return new DocumentSectionState({
            ...parsed,
            pages: {
                show: parsed?.pages.show ? List(
                    parsed.pages.show.map(
                        (s: any) => Map({ ...s, section: new DocumentSection(s.section) })
                    )
                ) : List([]),
            },
        });
    }
    public serializeState(): string {
        return JSON.stringify({
            ...this.toJSON(),
            show: this.pages.show.toArray().map(s => s.toJSON()).map(s => { return { ...s, section: s.section.toJSON() } }),
        });
    }
}

const DocumentSectionReducer = reducerWithInitialState(new DocumentSectionState())
    .case(GlobalAction.HYDRATE, (state, payload) => {
        return payload.documentSection;
    })
    .case(DocumentSectionAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('section').slug == payload.slug && p.get('groupSlug') == payload.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ section: new DocumentSection({ slug: payload.slug }), groupSlug: payload.groupSlug, loading: true, success: false }));
        } else {
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentSectionAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('section').slug == payload.params.slug && p.get('groupSlug') == payload.params.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ document: payload.result.section, groupSlug: payload.params.groupSlug, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('section', payload.result.section);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(DocumentSectionAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('section').slug == payload.params.slug && p.get('groupSlug') == payload.params.groupSlug);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ section: new DocumentSection({ slug: payload.params.slug }), groupSlug: payload.params.groupSlug, loading: false, success: false }));
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })

export default DocumentSectionReducer;