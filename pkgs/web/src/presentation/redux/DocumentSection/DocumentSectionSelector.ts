import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';
import { DocumentSectionPageStates } from './DocumentSectionReducer';

export const documentSectionShowPageStatesSelector = (state: RootState) => state.documentSection.pages.show;
export const documentSectionShowPageSelector = createSelector(
    documentSectionShowPageStatesSelector,
    (_: RootState, { groupSlug, sectionSlug }: { groupSlug: string, sectionSlug: string }) => { return { groupSlug, sectionSlug } },
    (show: DocumentSectionPageStates, { groupSlug, sectionSlug }: { groupSlug: string, sectionSlug: string }) => show.find(p => p.get('section').slug == sectionSlug && p.get('groupSlug') == groupSlug)
);