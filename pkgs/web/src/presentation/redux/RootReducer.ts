import { combineReducers, Reducer } from 'redux';
import appReducer, { AppAction, AppState } from '@/presentation/redux/App/AppReducer';
import deviceReducer, { DeviceAction, DeviceState } from '@/presentation/redux/Device/DeviceReducer';
import { GlobalAction } from './GlobalAction';
import { DocumentAction, DocumentState } from './Document/DocumentReducer';
import { DocumentGroupAction, DocumentGroupState } from './DocumentGroup/DocumentGroupReducer';
import { DocumentSectionAction, DocumentSectionState } from './DocumentSection/DocumentSectionReducer';
import { documentReducer } from './Document';
import { documentGroupReducer } from './DocumentGroup';
import { documentSectionReducer } from './DocumentSection';

export interface RootState {
    app: AppState,
    device: DeviceState,
    document: DocumentState,
    documentGroup: DocumentGroupState,
    documentSection: DocumentSectionState,
}

export type RootAction = GlobalAction | AppAction | DeviceAction | DocumentAction | DocumentGroupAction | DocumentSectionAction;

export const rootReducer = (): Reducer => {
    return combineReducers({
        app: appReducer,
        device: deviceReducer,
        document: documentReducer,
        documentGroup: documentGroupReducer,
        documentSection: documentSectionReducer,
    });
}

export const initRootState = (): Partial<RootState> => {
    return {
        app: new AppState(),
        device: new DeviceState(),
        document: new DocumentState(),
        documentGroup: new DocumentGroupState(),
        documentSection: new DocumentSectionState(),
    };
};