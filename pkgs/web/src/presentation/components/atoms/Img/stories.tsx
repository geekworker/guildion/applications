import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import Img from '.';
import { Files } from '@guildion/core';

export default {
    component: Img,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof Img>;

export const Default: ComponentStoryObj<typeof Img> = {
    args: {
        src: Files.getSeed().generateGuildProfile().url,
        transformations: {
            dw: '220',
            dh: '220',
            of: 'webp',
        },
        width: 120,
        height: 120,
    },
}