import React from 'react';
import ImgComponent from './component';
import ImgPresenter from './presenter';

const Img = (props: ImgPresenter.Input) => {
    const output = ImgPresenter.usePresenter(props);
    return <ImgComponent {...output} />;
};

export default React.memo(Img, ImgPresenter.inputAreEqual);