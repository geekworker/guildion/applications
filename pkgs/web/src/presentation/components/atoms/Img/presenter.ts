import ImgHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { ImageProps } from "next/image";
import { ImgTransformation } from './types';

namespace ImgPresenter {
    export type Input = {
        src: string,
        transformations?: Partial<ImgTransformation>,
        loading?: boolean,
    } & Omit<ImageProps, 'loading'>;
    
    export type Output = {
        loading: boolean,
        onLoadStart: () => void,
        onLoadingComplete: () => void,
    } & Omit<Input, 'loading'>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {
            src,
            loading,
            onLoadStart,
            onLoadingComplete,
        } = ImgHooks.useState(props);
        return {
            ...props,
            src,
            loading,
            onLoadStart,
            onLoadingComplete,
        }
    }
}

export default ImgPresenter;