import React from 'react';
import useInterval from 'use-interval';
import type WelcomeBalloonPresenter from './presenter';

namespace WelcomeBalloonHooks {

    export const useAnimated = (initialAnimated?: boolean) => {
        const [animated, setAnimated] = React.useState<boolean>(!!initialAnimated);
        React.useEffect(() => {
            !!initialAnimated != animated && setAnimated(!!initialAnimated);
        }, [initialAnimated]);
        return animated;
    }

    export const useAnimation = ({ diameter, animated, interval, reverse, rotateRate, rotatePx, offsetTime, frameRate }: WelcomeBalloonPresenter.Input): {
        translationX: number,
        translationY: number,
    } => {
        const [translationY, setTranslationY] = React.useState(0);
        const [translationX, setTranslationX] = React.useState(0);
        const center = React.useRef(0).current;
        const radius = React.useRef(diameter / 2).current;
        useInterval(() => {
            if (!animated) return;
            const t = new Date().getTime() / 500 + (offsetTime ?? 0);
            const x = center + Math.cos(t * (frameRate  ?? 1)) * (rotatePx || (radius * (rotateRate ?? 1)));
            const y = center + Math.sin(t * (frameRate  ?? 1)) * (rotatePx || (radius * (rotateRate ?? 1)));
            setTranslationY(x);
            setTranslationX(y);
        }, !!animated ? (interval || 10) : false);
        return {
            translationX,
            translationY,
        }
    }
}

export default WelcomeBalloonHooks;