import { compare } from "@/shared/modules/ObjectCompare";
import { LayoutProps } from "@guildion/next";
import WelcomeBalloonHooks from "./hooks";

namespace WelcomeBalloonPresenter {
    export type Input = {
        animated?: boolean,
        reverse?: boolean,
        interval?: number,
        diameter: number,
        rotateRate?: number,
        rotatePx?: number,
        offsetTime?: number,
        frameRate?: number,
    } & LayoutProps;
    
    export type Output = {
        diameter: number,
        translationX: number,
        translationY: number,
    } & LayoutProps;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const animated = WelcomeBalloonHooks.useAnimated(props.animated);
        const translations = WelcomeBalloonHooks.useAnimation({ ...props, animated });
        return {
            ...props,
            ...translations,
        }
    }
}

export default WelcomeBalloonPresenter;