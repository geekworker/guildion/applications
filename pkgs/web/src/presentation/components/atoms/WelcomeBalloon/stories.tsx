import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import WelcomeBalloon from '.';
import { fallbackAppTheme } from '@guildion/ui';

export default {
    component: WelcomeBalloon,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof WelcomeBalloon>;

export const Default: ComponentStoryObj<typeof WelcomeBalloon> = {
    args: {
        animated: false,
        diameter: 100,
        interval: 10,
        frameRate: 0.2,
        offsetTime: 20,
        style: {
            background: fallbackAppTheme.welcome.balloon,
            opacity: 0.5,
        },
    },
}

export const Fill: ComponentStoryObj<typeof WelcomeBalloon> = {
    args: {
        animated: true,
        diameter: 100,
        interval: 10,
        frameRate: 0.2,
        offsetTime: 30,
        style: {
            background: fallbackAppTheme.welcome.balloon,
            opacity: 0.5,
        },
    },
}

export const Border: ComponentStoryObj<typeof WelcomeBalloon> = {
    args: {
        animated: true,
        diameter: 100,
        interval: 10,
        frameRate: 0.2,
        offsetTime: 40,
        style: {
            borderColor: fallbackAppTheme.welcome.balloon,
            borderWidth: 5,
            opacity: 0.5,
        },
    },
}