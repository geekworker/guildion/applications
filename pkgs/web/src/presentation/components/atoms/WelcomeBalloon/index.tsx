import React from 'react';
import WelcomeBalloonComponent from './component';
import WelcomeBalloonPresenter from './presenter';

const WelcomeBalloon = (props: WelcomeBalloonPresenter.Input) => {
    const output = WelcomeBalloonPresenter.usePresenter(props);
    return <WelcomeBalloonComponent {...output} />;
};

export default React.memo(WelcomeBalloon, WelcomeBalloonPresenter.inputAreEqual);