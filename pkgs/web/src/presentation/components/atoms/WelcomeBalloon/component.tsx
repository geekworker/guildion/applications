import React from 'react';
import WelcomeBalloonPresenter from './presenter';

const WelcomeBalloon: React.FC<WelcomeBalloonPresenter.Output> = ({ className, style, diameter, translationX, translationY }) => {
    return (
        <div
            className={className}
            style={{
                ...style,
                width: diameter,
                height: diameter,
                borderRadius: diameter / 2,
                transform: `translate3d(${translationX}px, ${translationY}px, 0)`,
            }}
        />
    ); 
};

export default React.memo(WelcomeBalloon, WelcomeBalloonPresenter.outputAreEqual);
