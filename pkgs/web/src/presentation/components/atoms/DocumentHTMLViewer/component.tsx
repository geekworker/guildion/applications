import { useClassNames } from '@/shared/hooks/useClassNames';
import React from 'react';
import DocumentHTMLViewerPresenter from './presenter';

const DocumentHTMLViewerComponent = ({ style, className, document, localizer, appTheme }: DocumentHTMLViewerPresenter.Output) => {
    const containerClassName = useClassNames('document-html-viewer', className);
    return (
        <div className={containerClassName} style={style}>
            <div
                className="document-html-viewer__inner"
                dangerouslySetInnerHTML={{
                    __html: document.getData(localizer.lang),
                }}
            />
        </div>
    );
};

export default React.memo(DocumentHTMLViewerComponent, DocumentHTMLViewerPresenter.outputAreEqual);