import React from 'react';
import DocumentHTMLViewerComponent from './component';
import DocumentHTMLViewerPresenter from './presenter';

const DocumentHTMLViewer = (props: DocumentHTMLViewerPresenter.Input) => {
    const output = DocumentHTMLViewerPresenter.usePresenter(props);
    return <DocumentHTMLViewerComponent {...output} />;
};

export default React.memo(DocumentHTMLViewer, DocumentHTMLViewerPresenter.inputAreEqual);