import React from 'react';

type Props = {
    children?: React.ReactNode,
};

const CenterView: React.FC<Props> = ({
    children,
}) => {
    return (
       <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flex: 1 }}>
           {children}
       </div> 
    );
};

export default CenterView;