import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';

type Props = {
} & LayoutProps;

const AndroidStatusBar: React.FC<Props> = ({
    style,
    className,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
       <div className={containerClassName} style={style}>
           <div className={styles['rect']}/>
           <div className={styles['circle']}/>
           <div className={styles['triangle']}/>
       </div>
    );
};

export default React.memo(AndroidStatusBar, compare);