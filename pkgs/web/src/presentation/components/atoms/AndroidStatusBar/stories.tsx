import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import AndroidStatusBar from '.';

export default {
    component: AndroidStatusBar,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof AndroidStatusBar>;

export const Default: ComponentStoryObj<typeof AndroidStatusBar> = {
    args: {
    },
}