import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import Link from 'next/link';

type Props = {
    href: string,
    title: string,
} & LayoutProps;

export type BreadCrumbProps = Props;

const BreadCrumb: React.FC<Props> = ({
    style,
    className,
    href,
    title,
}) => {
    const containerClassName = useClassNames(styles['container'], className);
    return (
        <div className={containerClassName} style={style}>
            <Link href={href}>
                <a className={styles['container-title']}>
                    {title}
                </a>
            </Link>
        </div>
    );
};

export default React.memo(BreadCrumb, compare);