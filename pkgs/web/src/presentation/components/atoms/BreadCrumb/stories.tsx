import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import BreadCrumb from '.';

export default {
    component: BreadCrumb,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof BreadCrumb>;

export const Default: ComponentStoryObj<typeof BreadCrumb> = {
    args: {
    },
}