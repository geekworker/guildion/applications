import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import BreadCrumbs from '.';

export default {
    component: BreadCrumbs,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof BreadCrumbs>;

export const Default: ComponentStoryObj<typeof BreadCrumbs> = {
    args: {
    },
}