import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import BreadCrumb, { BreadCrumbProps } from '../../atoms/BreadCrumb';
import { HiChevronRight } from 'react-icons/hi';
import { useAppTheme } from '@/shared/hooks/useAppTheme';

export type BreadCrumbsData = BreadCrumbProps[];

type Props = {
    data: BreadCrumbsData,
} & LayoutProps;

const BreadCrumbs: React.FC<Props> = ({
    style,
    className,
    data,
}) => {
    const containerClassName = useClassNames(styles['container'], className);
    const appTheme = useAppTheme();
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['container-inner']}>
                <ul className={styles['items']}>
                    {data.map((d, i) => (
                        <li key={i} style={{ display: 'flex' }} >
                            <BreadCrumb {...d} />
                            {i < data.length - 1 && (
                                <div style={{ paddingTop: '2px' }}>
                                    <HiChevronRight size={16} color={appTheme.element.subp1} />
                                </div>
                            )}
                        </li>
                    ))}
                </ul>
            </div>
        </div>
    );
};

export default React.memo(BreadCrumbs, compare);