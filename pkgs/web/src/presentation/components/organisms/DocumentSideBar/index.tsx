import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { APP_NAME, DocumentGroup, Documents, DocumentSection, DocumentSections, WebEndpoints } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import Link from 'next/link';
import BrandIcon from '../../icons/BrandIcon';

type Props = {
    group: DocumentGroup,
    currentSlug?: string,
    currentSectionSlug?: string,
} & LayoutProps;

const DocumentSideBar: React.FC<Props> = ({
    style,
    className,
    group,
    currentSlug,
    currentSectionSlug,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    const sections = React.useMemo(() => group.records.sections, [group.records.sections]);
    const localizer = useLocalizer();
    const appTheme = useAppTheme();

    if (!sections) return <></>;

    const renderDocuments = (documents: Documents, section: DocumentSection) => documents.toArray().map((d, i) => (
        <li className={styles['document']} key={i}>
            <Link href={WebEndpoints.documentShowRouter.toPath({ groupSlug: group.slug, sectionSlug: section.slug, slug: d.slug })} >
                <a className={styles['document-link']} id={currentSlug === d.slug && section.slug === currentSectionSlug ? styles['active'] : ''}>
                    {d.getTitle(localizer.lang)}
                </a>
            </Link>
        </li>
    ));

    const renderSections = (sections: DocumentSections) => sections.toArray().map((s, i) => (
        <ul className={styles['sections']} key={i}>
            <li className={styles['section']}>
                {s.getName(localizer.lang)}
            </li>
            <ul className={styles['documents']}>
                {s.records.documents && renderDocuments(s.records.documents, s)}
            </ul>
        </ul>
    ));

    return (
        <div className={containerClassName} style={style}>
            {renderSections(sections)}
            <Link href={WebEndpoints.homeRouter.path}>
                <a className={styles['logo']}>
                    <BrandIcon width={20} height={20} color={appTheme.background.subp3} />
                    <span className={styles['logo_name']}>
                        {APP_NAME}
                    </span>
                </a>
            </Link>
        </div>
    );
};

export default React.memo(DocumentSideBar, compare);