import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import DocumentSideBar from '.';

export default {
    component: DocumentSideBar,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof DocumentSideBar>;

export const Default: ComponentStoryObj<typeof DocumentSideBar> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}