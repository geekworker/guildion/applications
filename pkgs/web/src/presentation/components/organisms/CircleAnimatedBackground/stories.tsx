import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import CircleAnimatedBackground from '.';

export default {
    component: CircleAnimatedBackground,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof CircleAnimatedBackground>;

export const Default: ComponentStoryObj<typeof CircleAnimatedBackground> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}