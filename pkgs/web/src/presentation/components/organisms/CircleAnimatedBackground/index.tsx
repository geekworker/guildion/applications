import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useClassNames } from '@/shared/hooks/useClassNames';

type Props = {
    count?: number;
    color?: 'gold',
} & Partial<LayoutProps>;

const CircleAnimatedBackground: React.FC<Props> = ({
    style,
    className,
    count,
    color,
}) => {
    count ??= 10;
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            <div className={styles["area"]} >
                <ul className={styles["circles"]} id={color ? styles[color] : undefined}>
                    {Array.from(new Array(count)).map((v,i) => i).map((i) => (
                        <li key={i} />
                    ))}
                </ul>
            </div >
        </div>
    );
};

export default React.memo(CircleAnimatedBackground, compare);