import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import { LayoutProps } from "@guildion/next";
import useInterval from "use-interval";

const Custom404Icon: React.FC<IconProps & LayoutProps & { clipColor?: string }> = ({ width, height, color, className, style, clipColor }) => {
    const [brightCount, setBrightCount] = React.useState(0);
    const [bright, setBright] = React.useState(false);
    useInterval(() => {
        const newCount = (brightCount + 1) % 20;
        setBrightCount(newCount);
        setBright(newCount == 1 || newCount == 5 || newCount == 7);
        return () => {
            setBrightCount(0);
            setBright(false);
        }
    }, 100);
    return (
        <svg
            viewBox="0 0 306 194"
            width={width}
            height={height}
            className={className}
            style={style}
        >
            <path
                d="M393,177.48a5.53,5.53,0,0,1,5.53,5.52V357a5.53,5.53,0,0,1-5.53,5.52H107a5.53,5.53,0,0,1-5.53-5.52V183a5.53,5.53,0,0,1,5.53-5.52H393m0-4.48H107a10,10,0,0,0-10,10V357a10,10,0,0,0,10,10H393a10,10,0,0,0,10-10V183a10,10,0,0,0-10-10Z"
                transform="translate(-97 -173)"
                style={{ fill: color }}
            />
            <rect style={{ fill: color }} x="13" y="12" width="280" height="169.42" rx="8.94"/>
            {bright ? (
                <rect x="212" y="77" width="2.88" height="18.88" transform="translate(120 288) rotate(-90)"/>
            ) : (
                <circle cx="212" cy="77" r="9.44"/>
            )}
            {bright ? (
                <rect x="98" y="77" width="2.88" height="18.88" transform="translate(20 175) rotate(-90)"/>
            ) : (
                <circle cx="98" cy="77" r="9.44"/>
            )}
            <rect x="162.56" y="234.56" width="18.88" height="4.88" transform="translate(-214.21 18.04) rotate(-45)"/>
            <path d="M177,229.39l2.66,2.66-12.56,12.56L164.39,242,177,229.39m0-1.58L162.81,242l4.24,4.24,14.14-14.14L177,227.81Z" transform="translate(-97 -173)"/>
            <rect x="325.61" y="227.99" width="4.88" height="18.88" transform="translate(-168.81 128.51) rotate(-45)"/>
            <path d="M323.1,229.82l12.56,12.56L333,245.05l-12.56-12.56,2.66-2.67m0-1.58-4.24,4.25L333,246.63l4.24-4.25L323.1,228.24Z" transform="translate(-97 -173)"/>
            <path d="M250.12,294.32c-8.51,0-15.44-3.8-15.44-8.47s6.93-8.48,15.44-8.48,15.44,3.8,15.44,8.48S258.63,294.32,250.12,294.32Z" transform="translate(-97 -173)"/>
            <path d="M250.12,278.05c8,0,14.76,3.57,14.76,7.8s-6.76,7.79-14.76,7.79-14.76-3.57-14.76-7.79,6.76-7.8,14.76-7.8m0-1.36c-8.9,0-16.12,4.1-16.12,9.16s7.22,9.15,16.12,9.15,16.12-4.1,16.12-9.15-7.22-9.16-16.12-9.16Z" transform="translate(-97 -173)"/>
            <path d="M127,333.56l-.1,11.82a1.16,1.16,0,0,0,1.73,1l10.38-6a1.15,1.15,0,0,0,0-2l-10.29-5.83A1.15,1.15,0,0,0,127,333.56Z" transform="translate(-97 -173)"/>
            <path d="M146.5,339.77a.77.77,0,1,1,0-1.54h225a.77.77,0,0,1,0,1.54Z" transform="translate(-97 -173)"/>
            <path d="M371.5,336.5h-225a2.5,2.5,0,0,0,0,5h225a2.5,2.5,0,0,0,0-5Z" transform="translate(-97 -173)"/>
            <path d="M189.2,344.68a5.38,5.38,0,1,1,5.48-5.38A5.44,5.44,0,0,1,189.2,344.68Z" transform="translate(-97 -173)"/>
            <path d="M189.2,334.24a5.06,5.06,0,1,1-5.15,5.06,5.12,5.12,0,0,1,5.15-5.06m0-.64a5.7,5.7,0,1,0,5.8,5.7,5.75,5.75,0,0,0-5.8-5.7Z" transform="translate(-97 -173)"/>
        </svg>
    );
};

export default React.memo(Custom404Icon, compare);
