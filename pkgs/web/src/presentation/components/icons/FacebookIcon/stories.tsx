import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import FacebookIcon from '.';

export default {
    component: FacebookIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof FacebookIcon>;

export const Default: ComponentStoryObj<typeof FacebookIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof FacebookIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}