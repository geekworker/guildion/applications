import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import InstagramIcon from '.';

export default {
    component: InstagramIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof InstagramIcon>;

export const Default: ComponentStoryObj<typeof InstagramIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof InstagramIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}