import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import StrokeUnderline from '.';

export default {
    component: StrokeUnderline,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof StrokeUnderline>;

export const Default: ComponentStoryObj<typeof StrokeUnderline> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof StrokeUnderline> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}