import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import { LayoutProps } from "@guildion/next";

const StrokeUnderlineIcon: React.FC<IconProps & LayoutProps & { strokeWidth?: number }> = ({ width, height, color, className, style, strokeWidth }) => {
    return (
        <svg 
            viewBox="0 0 274 8"
            width={width}
            height={height}
            className={className}
            style={style}
        >
            <path
                fill="none"
                stroke={color}
                strokeWidth={`${strokeWidth ?? 1}`}
                d="M174,456.290698 C307.466899,452.763566 398.466899,451 447,451"
                transform="translate(-173 -450)"
            />
        </svg>
    );
};

export default React.memo(StrokeUnderlineIcon, compare);