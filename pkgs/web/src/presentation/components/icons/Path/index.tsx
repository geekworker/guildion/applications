import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import { motion } from "framer-motion";
import React from "react";
import useInterval from "use-interval";
import { LayoutProps } from "@guildion/next";

type Props = {
    animationType?: 'draw' | 'repeat',
    sx?: number,
    sy?: number,
    ex?: number,
    ey?: number,
    interval?: number,
} & IconProps & LayoutProps;

const BrandIcon: React.FC<Props> = ({ width, height, color, animationType, sx, sy, ex, ey, interval, className, style }) => {
    const variants = React.useMemo(() => {
        return {
            hidden: {
              pathLength: 0,
              fill: "rgba(0, 0, 0, 0)"
            },
            visible: {
              pathLength: 2,
              fill: color,
            }
        };
    }, [color]);
    const [animated, setAnimated] = React.useState(false);
    useInterval(() => {
        setAnimated(!animated);
    }, animationType === 'repeat' ? (interval || 500) : false);
    
    return (
        <motion.svg
            x="0px"
            y="0px"
            xmlns="http://www.w3.org/2000/svg"
            viewBox={`0 0 100 100`}
            width={width}
            height={height}
            className={className}
            style={style}
        >
            <motion.path
                stroke={color}
                d={animated ? `M ${ex ?? 0} ${ey ?? 0} L ${sx ?? 0} ${sy ?? 0}` : `M ${sx ?? 0} ${sy ?? 0} L ${ex ?? 0} ${ey ?? 0}`}
                initial={!!animationType && !animated ? 'hidden' : undefined}
                animate={!!animationType && !animated ? 'visible' : undefined}
                variants={variants}
                transition={{
                    default: { duration: 0.3, ease: "easeInOut" },
                    fill: { duration: 0.3, ease: [1, 0, 0.8, 1] }
                }}
            />
        </motion.svg>
    );
};

export default React.memo(BrandIcon, compare);