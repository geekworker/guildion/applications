import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import Path from '.';

export default {
    component: Path,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof Path>;

export const Default: ComponentStoryObj<typeof Path> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
        sx: 0,
        sy: 50,
        ex: 100,
        ey: 50,
    },
}

export const DrawAnimation: ComponentStoryObj<typeof Path> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
        animationType: 'draw',
        sx: 0,
        sy: 50,
        ex: 100,
        ey: 50,
    },
}

export const RepeatAnimation: ComponentStoryObj<typeof Path> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
        animationType: 'repeat',
        sx: 0,
        sy: 50,
        ex: 100,
        ey: 50,
    },
}