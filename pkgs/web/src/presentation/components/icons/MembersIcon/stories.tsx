import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import MembersIcon from '.';

export default {
    component: MembersIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof MembersIcon>;

export const Default: ComponentStoryObj<typeof MembersIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof MembersIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}