import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import { LayoutProps } from "@guildion/next";
import { motion, Variants, useAnimation } from 'framer-motion';
import { useAppTheme } from "@/shared/hooks/useAppTheme";
import useInterval from "use-interval";
import { useWindowSize } from "@/shared/hooks/useWindowSize";

const Custom404Text: React.FC<IconProps & LayoutProps & { clipColor?: string }> = ({ width, height, color, className, style, clipColor }) => {
    const appTheme = useAppTheme();
    const animator = useAnimation();
    const window = useWindowSize();
    const drawVariants: Variants = {
        hidden: { pathLength: 0, opacity: 0 },
        visible: (i) => {
            const delay = 1 + i * 0.5;
            return {
                pathLength: delay,
                opacity: 1,
                transition: {
                    pathLength: { delay, type: "spring", duration: 1.5, bounce: 0 },
                    opacity: { delay, duration: 0.01 }
                }
            };
        }
    };
    React.useEffect(() => {
        animator.set('hidden');
        animator.start('visible');
    }, [])
    useInterval(() => {
        animator.set('hidden');
        animator.start('visible');
    }, 5000);
    return (
        <div className={className} style={style}>
            <motion.svg
                width={window.width < 500 ? "300" : "800"}
                height={window.width < 500 ? "150" : "400"}
                viewBox="0 0 600 200"
                animate={animator}
            >
                <motion.line
                    x1="160"
                    y1="120"
                    x2="20"
                    y2="120"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={1}
                    animate={animator}
                />
                <motion.line
                    x1="20"
                    y1="120"
                    x2="120"
                    y2="20"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={1.5}
                    animate={animator}
                />
                <motion.line
                    x1="120"
                    y1="20"
                    x2="120"
                    y2="180"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={2}
                />
                <motion.ellipse
                    cx="260"
                    cy="100"
                    rx="60"
                    ry="80"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={2.5}
                />
                <motion.line
                    x1="500"
                    y1="120"
                    x2="360"
                    y2="120"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={3}
                />
                <motion.line
                    x1="360"
                    y1="120"
                    x2="460"
                    y2="20"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={3.5}
                />
                <motion.line
                    x1="460"
                    y1="20"
                    x2="460"
                    y2="180"
                    stroke={appTheme.element.subp1}
                    variants={drawVariants}
                    custom={4}
                />
            </motion.svg>
        </div>
    );
};

export default React.memo(Custom404Text, compare);
