import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import SyncVisionIcon from '.';

export default {
    component: SyncVisionIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof SyncVisionIcon>;

export const Default: ComponentStoryObj<typeof SyncVisionIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof SyncVisionIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}