import AppProvider from '@/infrastructure/AppProvider';
import StoreProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import BrandIcon from '.';

export default {
    component: BrandIcon,
    decorators: [
        (storyFn) => (
            <StoreProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StoreProvider>
        )
    ]
} as ComponentMeta<typeof BrandIcon>;

export const Default: ComponentStoryObj<typeof BrandIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
    },
}

export const FadeInAnimation: ComponentStoryObj<typeof BrandIcon> = {
    args: {
        width: 320,
        height: 320,
        color: 'white',
        animationType: 'fadeIn',
    },
}