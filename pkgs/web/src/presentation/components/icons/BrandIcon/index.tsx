import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import { motion } from "framer-motion";
import React from "react";
import { LayoutProps } from "@guildion/next";

const BrandIcon: React.FC<IconProps & { animationType?: 'fadeIn' } & LayoutProps> = ({ width, height, color, animationType, className, style }) => {
    const variants = React.useMemo(() => {
        return {
            hidden: {
              opacity: 0,
              pathLength: 0,
              fill: "transparent"
            },
            visible: {
              opacity: 1,
              pathLength: 1,
              fill: color,
            }
        };
    }, [color]);
    
    return (
        <motion.svg
            x="0px"
            y="0px"
            viewBox="0 0 421.04 422.05"
            width={width}
            height={height}
            initial={animationType === 'fadeIn' ? 'hidden' : undefined}
            animate={animationType === 'fadeIn' ? 'visible' : undefined}
            variants={variants}
            className={className}
            style={style}
            transition={{
                default: { duration: 0.3, ease: "easeInOut" },
                fill: { duration: 0.3, ease: [1, 0, 0.8, 1] }
            }}
        >
            <motion.polygon
                fill={color}
                points="421.04 210.01 254.84 111.19 254.52 156.64 382.64 232.82 421.04 210.01"
            />
            <motion.polygon
                fill={color}
                points="421.04 210.01 254.84 308.82 254.52 263.37 382.64 187.19 421.04 210.01"
            />
            <motion.polygon
                fill={color}
                points="51.78 0 49.3 193.35 88.83 215.8 90.73 21.85 51.78 0"
            />
            <motion.polygon
                fill={color}
                points="51.78 0 220.46 94.53 220.16 139.99 51.22 44.66 51.78 0"
            />
            <motion.polygon
                fill={color}
                points="52.66 420 221.35 325.47 221.05 280.02 52.11 375.34 52.66 420"
            />
            <motion.polygon
                fill={color}
                points="52.66 420 50.19 226.65 89.71 249.1 91.62 398.15 52.66 420"
            />
        </motion.svg>
    );
};

export default React.memo(BrandIcon, compare);