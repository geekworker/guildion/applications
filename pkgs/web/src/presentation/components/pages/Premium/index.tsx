import React from 'react';
import PremiumPresenter from './presenter';
import PremiumComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: PremiumPresenter.MergedProps) => {
    const output = PremiumPresenter.usePresenter(props);
    return <PremiumComponent {...output} />;
};

const Premium = connect<PremiumPresenter.StateProps, PremiumPresenter.DispatchProps, PremiumPresenter.Input, PremiumPresenter.MergedProps, RootState>(
    PremiumPresenter.mapStateToProps,
    PremiumPresenter.mapDispatchToProps,
    PremiumPresenter.mergeProps,
    PremiumPresenter.connectOptions,
)(Container);

export default Premium;