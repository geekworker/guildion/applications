import React from 'react';
import styles from './styles.module.scss'
import PremiumPresenter from './presenter';
import { useClassNames } from '@/shared/hooks/useClassNames';

const PremiumComponent = ({ style, className }: PremiumPresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
        </div>
    );
};

export default React.memo(PremiumComponent, PremiumPresenter.outputAreEqual);