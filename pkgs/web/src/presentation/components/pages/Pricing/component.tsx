import React from 'react';
import styles from './styles.module.scss'
import PricingPresenter from './presenter';
import { useClassNames } from '@/shared/hooks/useClassNames';

const PricingComponent = ({ style, className }: PricingPresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
        </div>
    );
};

export default React.memo(PricingComponent, PricingPresenter.outputAreEqual);