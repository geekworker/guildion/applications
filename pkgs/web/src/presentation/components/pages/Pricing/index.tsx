import React from 'react';
import PricingPresenter from './presenter';
import PricingComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: PricingPresenter.MergedProps) => {
    const output = PricingPresenter.usePresenter(props);
    return <PricingComponent {...output} />;
};

const Pricing = connect<PricingPresenter.StateProps, PricingPresenter.DispatchProps, PricingPresenter.Input, PricingPresenter.MergedProps, RootState>(
    PricingPresenter.mapStateToProps,
    PricingPresenter.mapDispatchToProps,
    PricingPresenter.mergeProps,
    PricingPresenter.connectOptions,
)(Container);

export default Pricing;