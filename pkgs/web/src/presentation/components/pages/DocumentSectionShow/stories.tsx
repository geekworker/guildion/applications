import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import DocumentSectionShow from '.';

export default {
    component: DocumentSectionShow,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof DocumentSectionShow>;

export const Default: ComponentStoryObj<typeof DocumentSectionShow> = {
    args: {
    },
}