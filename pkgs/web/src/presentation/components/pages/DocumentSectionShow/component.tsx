import React from 'react';
import styles from './styles.module.scss'
import DocumentSectionShowPresenter from './presenter';
import Head from 'next/head';
import { APP_NAME } from '@guildion/core';
import DocumentShow from '../DocumentShow';

const DocumentSectionShowComponent = ({ style, className, localizer, page, groupPage, appTheme, groupSlug, sectionSlug, document }: DocumentSectionShowPresenter.Output) => {
    if (!page) return <></>;
    return (
        <>
            <Head>
                <title>
                    {page.get('section').getName(localizer.lang)} | {APP_NAME}
                </title>
                <meta
                    name="description"
                    content={page.get('section').getName(localizer.lang)}
                />
            </Head>
            {document && (
                <DocumentShow
                    groupSlug={groupSlug}
                    sectionSlug={sectionSlug}
                    slug={document.slug}
                    className={className}
                    style={style}
                    disableSEO
                />
            )}
        </>
    );
};

export default React.memo(DocumentSectionShowComponent, DocumentSectionShowPresenter.outputAreEqual);