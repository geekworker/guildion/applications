import React from 'react';
import type DocumentSectionShowPresenter from './presenter';

namespace DocumentSectionShowHooks {
    export const useState = (props: DocumentSectionShowPresenter.MergedProps) => {
        const section = React.useMemo(() => {
            return props.page?.get('section');
        }, [props.page?.get('section')]);
        const document = React.useMemo(() => {
            return section?.records.documents?.first;
        }, [section]);
        return {
            document,
        };
    }
}

export default DocumentSectionShowHooks;