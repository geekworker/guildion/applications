import DocumentSectionShowHooks from './hooks';
import { Action, bindActionCreators } from 'redux';
import { RootState } from '@/presentation/redux/RootReducer';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { DocumentSectionPageState } from '@/presentation/redux/DocumentSection/DocumentSectionReducer';
import { documentSectionShowPageSelector } from '@/presentation/redux/DocumentSection/DocumentSectionSelector';
import { Localizer, Document } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { documentGroupShowPageSelector } from '@/presentation/redux/DocumentGroup/DocumentGroupSelector';
import { DocumentGroupPageState } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';
import { DocumentAction } from '@/presentation/redux/Document/DocumentReducer';

namespace DocumentSectionShowPresenter {
    export type StateProps = {
        page?: DocumentSectionPageState,
        groupPage?: DocumentGroupPageState,
    }
    
    export type DispatchProps = {
        fetchDocument: ({ slug, sectionSlug, groupSlug }: { slug: string, sectionSlug: string, groupSlug: string, }) => Action,
    }
    
    export type Input = {
        groupSlug: string,
        sectionSlug: string,
    } & LayoutProps;
    
    export type Output = {
        localizer: Localizer,
        appTheme: AppTheme,
        document?: Document,
    } & Omit<MergedProps, ''>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            page: documentSectionShowPageSelector(state, { ...ownProps }),
            groupPage: documentGroupShowPageSelector(state, ownProps.groupSlug),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchDocument: ({ slug, sectionSlug, groupSlug }: { slug: string, sectionSlug: string, groupSlug: string, }) => DocumentAction.fetchShow.started({ slug, sectionSlug, groupSlug })
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.app.csr == next.app.csr &&
            prev.documentSection.pages.show == next.documentSection.pages.show &&
            prev.documentGroup.pages.show == next.documentGroup.pages.show
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const {
            document,
        } = DocumentSectionShowHooks.useState(props);
        return {
            ...props,
            document,
            localizer: useLocalizer(),
            appTheme: useAppTheme(),
        }
    }
}

export default DocumentSectionShowPresenter;