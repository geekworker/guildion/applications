import React from 'react';
import DocumentSectionShowPresenter from './presenter';
import DocumentSectionShowComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: DocumentSectionShowPresenter.MergedProps) => {
    const output = DocumentSectionShowPresenter.usePresenter(props);
    return <DocumentSectionShowComponent {...output} />;
};

const DocumentSectionShow = connect<DocumentSectionShowPresenter.StateProps, DocumentSectionShowPresenter.DispatchProps, DocumentSectionShowPresenter.Input, DocumentSectionShowPresenter.MergedProps, RootState>(
    DocumentSectionShowPresenter.mapStateToProps,
    DocumentSectionShowPresenter.mapDispatchToProps,
    DocumentSectionShowPresenter.mergeProps,
    DocumentSectionShowPresenter.connectOptions,
)(Container);

export default DocumentSectionShow;