import React from 'react';
import styles from './styles.module.scss'
import DownloadsPresenter from './presenter';
import { useClassNames } from '@/shared/hooks/useClassNames';

const DownloadsComponent = ({ style, className }: DownloadsPresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
        </div>
    );
};

export default React.memo(DownloadsComponent, DownloadsPresenter.outputAreEqual);