import React from 'react';
import DownloadsPresenter from './presenter';
import DownloadsComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: DownloadsPresenter.MergedProps) => {
    const output = DownloadsPresenter.usePresenter(props);
    return <DownloadsComponent {...output} />;
};

const Downloads = connect<DownloadsPresenter.StateProps, DownloadsPresenter.DispatchProps, DownloadsPresenter.Input, DownloadsPresenter.MergedProps, RootState>(
    DownloadsPresenter.mapStateToProps,
    DownloadsPresenter.mapDispatchToProps,
    DownloadsPresenter.mergeProps,
    DownloadsPresenter.connectOptions,
)(Container);

export default Downloads;