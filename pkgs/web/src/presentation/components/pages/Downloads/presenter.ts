import DownloadsHooks from './hooks';
import { Action, bindActionCreators } from 'redux';
import { RootState } from '@/presentation/redux/RootReducer';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';

namespace DownloadsPresenter {
    export type StateProps = {
    }
    
    export type DispatchProps = {
    }
    
    export type Input = {
    } & LayoutProps;
    
    export type Output = {
    } & Omit<Input, ''>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const {} = DownloadsHooks.useState(props);
        return {
            ...props,
        }
    }
}

export default DownloadsPresenter;