import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import DocumentGroupShow from '.';

export default {
    component: DocumentGroupShow,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ],
} as ComponentMeta<typeof DocumentGroupShow>;

export const Default: ComponentStoryObj<typeof DocumentGroupShow> = {
    args: {
    },
}