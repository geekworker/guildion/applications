import React from 'react';
import styles from './styles.module.scss'
import DocumentGroupShowPresenter from './presenter';
import Head from 'next/head';
import { APP_NAME } from '@guildion/core';
import DocumentGroupDefaultTemplate from '../../templates/DocumentGroupDefaultTemplate';
import { useClassNames } from '@/shared/hooks/useClassNames';

const DocumentGroupShowComponent = ({ style, className, localizer, page, appTheme }: DocumentGroupShowPresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    if (!page) return <></>;
    return (
        <>
            <Head>
                <title>
                    {page.get('group').getName(localizer.lang)} | {APP_NAME}
                </title>
                <meta
                    name="description"
                    content={page.get('group').getName(localizer.lang)}
                />
            </Head>
            <div className={containerClassName} style={style}>
                <DocumentGroupDefaultTemplate
                    page={page}
                />
            </div>
        </>
    );
};

export default React.memo(DocumentGroupShowComponent, DocumentGroupShowPresenter.outputAreEqual);