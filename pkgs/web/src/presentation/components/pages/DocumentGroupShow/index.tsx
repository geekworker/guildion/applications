import React from 'react';
import DocumentGroupShowPresenter from './presenter';
import DocumentGroupShowComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: DocumentGroupShowPresenter.MergedProps) => {
    const output = DocumentGroupShowPresenter.usePresenter(props);
    return <DocumentGroupShowComponent {...output} />;
};

const DocumentGroupShow = connect<DocumentGroupShowPresenter.StateProps, DocumentGroupShowPresenter.DispatchProps, DocumentGroupShowPresenter.Input, DocumentGroupShowPresenter.MergedProps, RootState>(
    DocumentGroupShowPresenter.mapStateToProps,
    DocumentGroupShowPresenter.mapDispatchToProps,
    DocumentGroupShowPresenter.mergeProps,
    DocumentGroupShowPresenter.connectOptions,
)(Container);

export default DocumentGroupShow;