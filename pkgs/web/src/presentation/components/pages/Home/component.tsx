import styles from './styles.module.scss';
import React from 'react';
import HomePresenter from './presenter';
import LaunchSection from './components/LaunchSection';
import GuildSection from './components/GuildSection';
import RoomSection from './components/RoomSection';
import RoomHeadSection from './components/RoomHeadSection';
import PremiumSection from './components/PremiumSection';
import LastSection from './components/LastSection';
import RoomHeadSectionProvider from './components/RoomHeadSection/provider';
import GuildSectionProvider from './components/GuildSection/provider';
import LaunchSectionProvider from './components/LaunchSection/provider';

const HomeComponent = ({}: HomePresenter.Output) => {
    return (
        <div className={styles['container']}>
            <LaunchSectionProvider>
                <RoomHeadSectionProvider>
                    <GuildSectionProvider>
                        <LaunchSection/>
                        <GuildSection/>
                        <RoomHeadSection/>
                        <div className={styles['container-sticky_room']}>
                            <RoomSection/>
                        </div>
                        <PremiumSection/>
                        <LastSection/>
                    </GuildSectionProvider>
                </RoomHeadSectionProvider>
            </LaunchSectionProvider>
        </div>
    );
};

export default React.memo(HomeComponent, HomePresenter.outputAreEqual);