import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import PremiumSection from '.';

export default {
    component: PremiumSection,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof PremiumSection>;

export const Default: ComponentStoryObj<typeof PremiumSection> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}