import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { usePositionElement } from '@/shared/hooks/usePotisionElement';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { Pixel } from 'react-device-mockups';
import Img from '@/presentation/components/atoms/Img';
import { CDN_PROXY_URL_STRING, WebEndpoints } from '@guildion/core';
import GuildSectionRedux from './redux';
import Link from 'next/link';
import { FaChevronRight } from 'react-icons/fa';
import { useWindowSize } from '@/shared/hooks/useWindowSize';

export const useHomeGuildSectionHeight = (): number => {
    const context = React.useContext(GuildSectionRedux.Context);
    return context.state.sectionHeight;
};

type Props = {
} & Partial<LayoutProps>;

const GuildSection: React.FC<Props> = ({
    style,
    className,
}) => {
    const { width } = useWindowSize();
    const localizer = useLocalizer();
    const splitGuildTitle = React.useMemo(() => localizer.dic.home.page.guild.title.split(localizer.dic.g.db.guild), [localizer]);
    const splitRoomTitle = React.useMemo(() => localizer.dic.home.page.room.title.split(localizer.dic.g.db.room), [localizer]);
    const {
        ref: containerRef,
        ...containerInfo
    } = usePositionElement<HTMLDivElement>();
    const context = React.useContext(GuildSectionRedux.Context);
    React.useEffect(() => context.dispatch(GuildSectionRedux.Action.setSectionHeight(containerInfo.height)), [containerInfo.height]);
    const containerClassName = useClassNames(className, styles['container']);
    const roomsMockType = React.useMemo<'grid' | 'horizontal'>(() => width > 765 ? 'horizontal' : 'grid', [width]);

    return (
        <div className={containerClassName} style={{ ...style }} ref={containerRef}>
            <div className={styles['container-inner']}>
                <div className={styles['title']}>
                    {splitGuildTitle[0]}
                    <strong>{localizer.dic.g.db.guild}</strong>
                    {splitGuildTitle[1] && splitGuildTitle[1]}
                </div>
                <div
                    className={styles['stories']}
                >
                    <div
                        className={styles['story-landscape']}
                        id={styles['story1']}
                    >
                        <div
                            className={styles['story-landscape-flex']}
                        >
                            <div
                                className={styles['story-landscape-texts']}
                            >
                                <p className={styles['story-landscape-label']}>
                                    {localizer.dic.home.page.guild.story1.label}
                                </p>
                                <h3 className={styles['story-landscape-title']}>
                                    {localizer.dic.home.page.guild.story1.title}
                                </h3>
                            </div>
                            <div className={styles['story-landscape-devices']}>
                                <div className={styles['story-landscape-device']}>
                                    <Pixel width={400}>
                                        <div className={styles['story-landscape-device_inner']}>
                                            <Img
                                                src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                transformations={{
                                                    dw: '600',
                                                    of: 'webp',
                                                }}
                                            />
                                        </div>
                                    </Pixel>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div
                        className={styles['story-portrait']}
                        id={styles['story2']}
                    >
                        <p className={styles['story-portrait-label']}>
                            {localizer.dic.home.page.guild.story2.label}
                        </p>
                        <h3 className={styles['story-portrait-title']}>
                            {localizer.dic.home.page.guild.story2.title}
                        </h3>
                        <h4 className={styles['story-portrait-description']}>
                            {localizer.dic.home.page.guild.story2.description}
                        </h4>
                        {/* <Link href={WebEndpoints.creatorGuildRouter.path}>
                            <a className={styles['story-portrait-link']}>
                                <span className={styles['story-portrait-link_text']}>
                                    {localizer.dic.home.attr.seeMore}
                                </span>
                                <FaChevronRight color={'white'} size={20} />
                            </a>
                        </Link> */}
                    </div>
                    <div
                        className={styles['story-portrait']}
                        id={styles['story3']}
                    >
                        <p className={styles['story-portrait-label']}>
                            {localizer.dic.home.page.guild.story3.label}
                        </p>
                        <h3 className={styles['story-portrait-title']}>
                            {localizer.dic.home.page.guild.story3.title}
                        </h3>
                        <h4 className={styles['story-portrait-description']}>
                            {localizer.dic.home.page.guild.story3.description}
                        </h4>
                        {/* <Link href={WebEndpoints.membersGuildRouter.path}>
                            <a className={styles['story-portrait-link']}>
                                <span className={styles['story-portrait-link_text']}>
                                    {localizer.dic.home.attr.seeMore}
                                </span>
                                <FaChevronRight color={'white'} size={20} />
                            </a>
                        </Link> */}
                    </div>
                    <div
                        className={styles['story-landscape']}
                        id={styles['story4']}
                    >
                        <p className={styles['story-landscape-label']}>
                            {localizer.dic.home.page.guild.story4.label}
                        </p>
                        <h3 className={styles['story-landscape-title']}>
                            {localizer.dic.home.page.guild.story4.title}
                        </h3>
                        <div className={styles['story-landscape-rooms']}>
                            <Img
                                src={`${CDN_PROXY_URL_STRING}/abs/vi/overviews/${localizer.lang}/rooms-${roomsMockType}-cells.png`}
                                transformations={{
                                    dh: roomsMockType == 'horizontal' ? '600' : '800',
                                    of: 'webp',
                                }}
                                height={roomsMockType == 'horizontal' ? 240 : 400}
                                width={roomsMockType == 'horizontal' ? 1100 : 540}
                                layout={'responsive'}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(GuildSection, compare);