import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import RoomSection from '.';

export default {
    component: RoomSection,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof RoomSection>;

export const Default: ComponentStoryObj<typeof RoomSection> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}