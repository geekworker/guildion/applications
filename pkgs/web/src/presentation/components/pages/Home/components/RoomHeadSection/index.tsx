import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useWindowSize } from '@/shared/hooks/useWindowSize';
import { useInView } from "react-intersection-observer";
import { motion, useAnimation, Variants } from 'framer-motion';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { LanguageCode } from '@guildion/core';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { usePositionElement } from '@/shared/hooks/usePotisionElement';
import RoomHeadSectionRedux from './redux';

type Props = {
} & LayoutProps;

export const useHomeRoomHeadSectionHeight = (): number => {
    const context = React.useContext(RoomHeadSectionRedux.Context);
    return context.state.sectionHeight;
};

const titleVariants = {
    visible: { opacity: 1, x: 0, transition: { ease: "easeOut", duration: 0.4 }, delay: 0.2 },
    hidden: { opacity: 0, x: -100 },
} as Variants;

const linkVariants = {
    visible: { opacity: 1, x: 0, transition: { ease: "easeOut", duration: 0.4 }, delay: 0.4 },
    hidden: { opacity: 0, x: 100 },
} as Variants;

const RoomHeadSection: React.FC<Props> = ({
    style,
    className
}) => {
    const { width, height } = useWindowSize();
    const [titleRef, titleInView] = useInView();
    const titleAnimator = useAnimation();
    React.useEffect(() => {
        titleAnimator.start(titleInView ? "visible" : "hidden");
    }, [titleAnimator, titleInView]);
    const [linkRef, linkInView] = useInView();
    const linkAnimator = useAnimation();
    React.useEffect(() => {
        linkAnimator.start(linkInView ? "visible" : "hidden");
    }, [linkAnimator, linkInView]);
    const localizer = useLocalizer();

    const {
        ref: containerRef,
        ...containerInfo
    } = usePositionElement<HTMLDivElement>();
    const context = React.useContext(RoomHeadSectionRedux.Context);
    React.useEffect(() => context.dispatch(RoomHeadSectionRedux.Action.setSectionHeight(containerInfo.height)), [containerInfo.height]);

    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style} ref={containerRef}>
            <div className={styles['container-inner']}>
                <motion.h3
                    className={styles['container-title']}
                    ref={titleRef}
                    variants={titleVariants}
                    initial={"hidden"}
                    animate={titleAnimator}
                >
                    {localizer.lang === LanguageCode.Japanese ? (
                        <>
                            コンテンツを
                            <strong>みんなで楽しめる機能</strong>
                            全て詰め込みました
                        </>
                    ) : (
                        <>
                            Packed with all for
                            <strong>enjoy creator contents</strong>
                            with members
                        </>
                    )}
                </motion.h3>
                <motion.h4
                    className={styles['container-link']}
                    ref={linkRef}
                    variants={linkVariants}
                    initial={"hidden"}
                    animate={linkAnimator}
                >
                    {localizer.dic.home.page.room.introduce}
                </motion.h4>
            </div>
        </div>
    );
};

export default React.memo(RoomHeadSection, compare);