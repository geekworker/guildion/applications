import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import RoomHeadSection from '.';

export default {
    component: RoomHeadSection,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof RoomHeadSection>;

export const Default: ComponentStoryObj<typeof RoomHeadSection> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}