import React from 'react';
import GuildSectionRedux from './redux';

interface Props {
    children?: React.ReactNode,
}

const GuildSectionProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        GuildSectionRedux.reducer,
        GuildSectionRedux.initialState,
    );
    return (
        <GuildSectionRedux.Context.Provider value={{ state, dispatch }} >
            {children}
        </GuildSectionRedux.Context.Provider>
    );
};

export default GuildSectionProvider;