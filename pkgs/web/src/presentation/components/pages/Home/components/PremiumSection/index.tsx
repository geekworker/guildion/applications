import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { CDN_PROXY_URL_STRING, Localizer, WebEndpoints } from '@guildion/core';
import Link from 'next/link';
import { FaChevronRight } from "react-icons/fa";
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { Pixel } from 'react-device-mockups';
import Img from '@/presentation/components/atoms/Img';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { useWindowSize } from '@/shared/hooks/useWindowSize';

type Props = {
} & LayoutProps;

const PremiumSection: React.FC<Props> = ({
    style,
    className,
}) => {
    const appTheme = useAppTheme();
    const containerClassName = useClassNames(className, styles['container']);
    const localizer = useLocalizer();
    const { width, height } = useWindowSize();
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['container-inner']}>
                <div className={styles['premium']}>
                    <div className={styles['premium-left']}>
                        <h2 className={styles['premium-left_title']}>
                            Guildion Premium
                        </h2>
                        <span className={styles['premium-left_description']}>
                            Discover new engraving options for Airpods. Mix emoji, text, and numbers.
                        </span>
                        <Link href={WebEndpoints.premiumRouter.path}>
                            <a className={styles['premium-left_link']}>
                                <span className={styles['premium-left_link-text']}>
                                    Check more Guildion Premium
                                </span>
                                <FaChevronRight color={appTheme.element.link} size={20} />
                            </a>
                        </Link>
                    </div>
                    <div className={styles['premium-right']}>
                        <div className={styles['premium-right_device2']}>
                            <Pixel width={width < 540 ? 160 : 240}>
                                <div style={{ width: '100%', height: '100%', position: 'relative' }}>
                                    <Img
                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                        transformations={{
                                            dw: '600',
                                            of: 'webp',
                                        }}
                                    />
                                </div>
                            </Pixel>
                        </div>
                        <div className={styles['premium-right_device1']}>
                            <Pixel width={width < 540 ? 160 : 240}>
                                <div style={{ width: '100%', height: '100%', position: 'relative' }}>
                                    <Img
                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                        transformations={{
                                            dw: '600',
                                            of: 'webp',
                                        }}
                                    />
                                </div>
                            </Pixel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(PremiumSection, compare);