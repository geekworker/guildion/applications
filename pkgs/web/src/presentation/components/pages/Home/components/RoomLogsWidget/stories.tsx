import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import RoomLogsWidget from '.';

export default {
    component: RoomLogsWidget,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof RoomLogsWidget>;

export const Default: ComponentStoryObj<typeof RoomLogsWidget> = {
    args: {
    },
}