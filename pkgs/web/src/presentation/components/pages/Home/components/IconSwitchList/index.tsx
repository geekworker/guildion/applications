import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { FaHashtag } from 'react-icons/fa';
import { MdArticle, MdOutlineMoreHoriz } from 'react-icons/md';
import { AiFillFolder } from 'react-icons/ai';
import SyncVisionIcon from '@/presentation/components/icons/SyncVisionIcon';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import MembersIcon from '@/presentation/components/icons/MembersIcon';
import { useClassNames } from '@/shared/hooks/useClassNames';

type Props = {
    enableSyncVision?: boolean,
    enableMembers?: boolean,
    enableChat?: boolean,
    enableLibrary?: boolean,
    enablePost?: boolean,
    enableSetting?: boolean,
} & LayoutProps;

const IconSwitchList: React.FC<Props> = ({
    style,
    className,
    enableSyncVision,
    enableMembers,
    enableChat,
    enableLibrary,
    enablePost,
    enableSetting,
}) => {
    const appTheme = useAppTheme();
    const containerClassName = useClassNames(styles['container'], className);
    return (
        <div className={containerClassName} style={{ ...style }}>
            <div className={styles['container-inner']}>
                <div className={styles['button']}>
                    <MembersIcon
                        color={enableMembers ? appTheme.element.link : appTheme.element.default}
                        width={18}
                        height={18}
                    />
                    <div className={styles['button-title']} style={{ color: enableMembers ? appTheme.element.link : appTheme.element.default }}>
                        MEMBERS
                    </div>
                </div>
                <div className={styles['button']}>
                    <SyncVisionIcon
                        color={enableSyncVision ? appTheme.element.link : appTheme.element.default}
                        width={18}
                        height={18}
                    />
                    <div className={styles['button-title']} style={{ color: enableSyncVision ? appTheme.element.link : appTheme.element.default }}>
                        SHARE
                    </div>
                </div>
                <div className={styles['border']}/>
                <div className={styles['button']}>
                    <FaHashtag
                        color={enableChat ? appTheme.element.link : appTheme.element.default}
                        size={18}
                    />
                    <div className={styles['button-title']} style={{ color: enableChat ? appTheme.element.link : appTheme.element.default }}>
                        CHAT
                    </div>
                </div>
                <div className={styles['button']}>
                    <AiFillFolder
                        color={enableLibrary ? appTheme.element.link : appTheme.element.default}
                        size={18}
                    />
                    <div className={styles['button-title']} style={{ color: enableLibrary ? appTheme.element.link : appTheme.element.default }}>
                        LIBRARY
                    </div>
                </div>
                <div className={styles['button']}>
                    <MdArticle
                        color={enablePost ? appTheme.element.link : appTheme.element.default}
                        size={18}
                    />
                    <div className={styles['button-title']} style={{ color: enablePost ? appTheme.element.link : appTheme.element.default }}>
                        POST
                    </div>
                </div>
                <div className={styles['button']}>
                    <MdOutlineMoreHoriz
                        color={enableSetting ? appTheme.element.link : appTheme.element.default}
                        size={18}
                    />
                    <div className={styles['button-title']} style={{ color: enableSetting ? appTheme.element.link : appTheme.element.default }}>
                        SETTING
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(IconSwitchList, compare);