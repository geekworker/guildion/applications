import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { useWindowSize } from '@/shared/hooks/useWindowSize';
import { Pixel } from 'react-device-mockups';
import { usePositionElement } from '@/shared/hooks/usePotisionElement';
import { useScroll } from '@/shared/hooks/useScroll';
import { Ease } from '@guildion/ui';
import { CDN_PROXY_URL_STRING } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import AndroidStatusBar from '@/presentation/components/atoms/AndroidStatusBar';
import Img from '@/presentation/components/atoms/Img';
import IconSwitchList from '../IconSwitchList';
import RoomLogsWidget from '../RoomLogsWidget';
import { useClassNames } from '@/shared/hooks/useClassNames';
import MessagesWidget from '../MessagesWidget';
import { motion, Variants, useAnimation } from 'framer-motion';
import { useHomeLaunchSectionHeight } from '../LaunchSection';
import { useHomeGuildSectionHeight } from '../GuildSection';
import { useHomeRoomHeadSectionHeight } from '../RoomHeadSection';
import LaunchSectionRedux from '../LaunchSection/redux';

type Props = {
} & LayoutProps;

const RoomSection: React.FC<Props> = ({
    style,
    className
}) => {
    const { width, height } = useWindowSize();
    const [isPlay, setIsPlay] = React.useState(false);
    const videoRef = React.useRef<HTMLVideoElement>(null);
    const { scrollTop } = useScroll();
    const {
        ref: containerRef,
        ...containerInfo
    } = usePositionElement<HTMLDivElement>();
    const localizer = useLocalizer();

    const messageAnimator = useAnimation();
    const messageVariants: Variants = React.useMemo(() => {
        return {
            hidden: {
                top: '100%',
                transition: { ease: "easeInOut", duration: 0.1 },
            },
            visible: {
                top: 0,
                transition: { ease: "easeInOut", duration: 0.1 },
            },
        }
    }, []);
    const [currentMessageVariant, setCurrentMessageVariant] = React.useState("hidden");
    const launchSectionHeight = useHomeLaunchSectionHeight();
    const guildSectionHeight = useHomeGuildSectionHeight();
    const roomHeadSectionHeight = useHomeRoomHeadSectionHeight();

    const [translate, setTranslate] = React.useState(0);
    const [translate1, setTranslate1] = React.useState(0);
    const [translateOver1, setTranslateOver1] = React.useState(0);
    const [translate2, setTranslate2] = React.useState(0);
    const [translateOver2, setTranslateOver2] = React.useState(0);
    const [translate3, setTranslate3] = React.useState(0);
    const [translateOver3, setTranslateOver3] = React.useState(0);
    const [translate4, setTranslate4] = React.useState(0);
    const [translateOver4, setTranslateOver4] = React.useState(0);
    const [translate5, setTranslate5] = React.useState(0);
    const [translateOver5, setTranslateOver5] = React.useState(0);
    const [translate6, setTranslate6] = React.useState(0);
    const [translateOver6, setTranslateOver6] = React.useState(0);

    React.useEffect(() => {
        const offsetHeight = launchSectionHeight + guildSectionHeight + roomHeadSectionHeight;
        const max = height * 12;
        const max1 = height * 1;
        const max2 = height * 1;
        const max3 = height * 1;
        const max4 = height * 1;
        const max5 = height * 1;
        const max6 = height * 1;
        const offsetY = Math.max(0, scrollTop - (height * 1) - offsetHeight);
        const offsetY1 = Math.max(0, scrollTop - (height * 0) - offsetHeight);
        const offsetY2 = Math.max(0, scrollTop - (height * 2) - offsetHeight);
        const offsetY3 = Math.max(0, scrollTop - (height * 4) - offsetHeight);
        const offsetY4 = Math.max(0, scrollTop - (height * 6) - offsetHeight);
        const offsetY5 = Math.max(0, scrollTop - (height * 8) - offsetHeight);
        const offsetY6 = Math.max(0, scrollTop - (height * 10) - offsetHeight);
        const current = Math.max(0, Math.min(1, offsetY / max));
        const current1 = Math.max(0, Math.min(1, offsetY1 / max1));
        const currentOver1 = Math.max(0, Math.min(2, offsetY1 / max1));
        const current2 = Math.max(0, Math.min(1, offsetY2 / max2));
        const currentOver2 = Math.max(0, Math.min(2, offsetY2 / max2));
        const current3 = Math.max(0, Math.min(1, offsetY3 / max3));
        const currentOver3 = Math.max(0, Math.min(2, offsetY3 / max3));
        const current4 = Math.max(0, Math.min(1, offsetY4 / max4));
        const currentOver4 = Math.max(0, Math.min(2, offsetY4 / max4));
        const current5 = Math.max(0, Math.min(1, offsetY5 / max5));
        const currentOver5 = Math.max(0, Math.min(2, offsetY5 / max5));
        const current6 = Math.max(0, Math.min(1, offsetY6 / max6));
        const currentOver6 = Math.max(0, Math.min(2, offsetY6 / max6));
        setTranslate(Ease.easeOutCubic(current || 0));
        setTranslate1(Ease.easeOutCubic(current1 || 0));
        setTranslateOver1(Ease.easeOutCubic(currentOver1 || 0));
        setTranslate2(Ease.easeOutCubic(current2 || 0));
        setTranslateOver2(Ease.easeOutCubic(currentOver2 || 0));
        setTranslate3(Ease.easeOutCubic(current3 || 0));
        setTranslateOver3(Ease.easeOutCubic(currentOver3 || 0));
        setTranslate4(Ease.easeOutCubic(current4 || 0));
        setTranslateOver4(Ease.easeOutCubic(currentOver4 || 0));
        setTranslate5(Ease.easeOutCubic(current5 || 0));
        setTranslateOver5(Ease.easeOutCubic(currentOver5 || 0));
        setTranslate6(Ease.easeOutCubic(current6 || 0));
        setTranslateOver6(Ease.easeOutCubic(currentOver6 || 0));

        const newMessageVariant = translate2 > 0 ? "visible" : "hidden";
        currentMessageVariant !== newMessageVariant && messageAnimator.start(newMessageVariant);
        setCurrentMessageVariant(newMessageVariant);

    }, [scrollTop, currentMessageVariant, launchSectionHeight, guildSectionHeight, roomHeadSectionHeight]);
    const containerClassName = useClassNames(className, styles['container']);

    return (
        <div className={containerClassName} ref={containerRef} style={style}>
            <div className={styles['container-inner']}>
                <div className={styles['text']}>
                    {translateOver1 > 0 && translateOver1 < 2 && (
                        <>
                            <div className={styles['description']} style={{ opacity: translateOver1 > 1 ? (2 - translateOver1) : translateOver1, transform: `translate(0px, ${(1 - translateOver1) * 100}px)` }}>
                                <div className={styles['description-inner']}>
                                {localizer.dic.home.page.room.features[0].description}
                                </div>
                            </div>
                            <div className={styles['title']} style={{ opacity: translateOver1 > 1 ? (2 - translateOver1) : translateOver1, transform: `translate(0px, ${(1 - translateOver1) * 100}px)` }}>
                                <div className={styles['title-inner']}>
                                    {localizer.dic.home.page.room.features[0].title}
                                </div>
                            </div>
                        </>
                    )}
                    {translateOver2 > 0 && translateOver2 < 2 && (
                        <>
                            <div className={styles['description']} style={{ opacity: translateOver2 > 1 ? (2 - translateOver2) : translateOver2, transform: `translate(0px, ${(1 - translateOver2) * 100}px)` }}>
                                <div className={styles['description-inner']}>
                                    {localizer.dic.home.page.room.features[1].description}
                                </div>
                            </div>
                            <div className={styles['title']} style={{ opacity: translateOver2 > 1 ? (2 - translateOver2) : translateOver2, transform: `translate(0px, ${(1 - translateOver2) * 100}px)` }}>
                                <div className={styles['title-inner']}>
                                    {localizer.dic.home.page.room.features[1].title}
                                </div>
                            </div>
                        </>
                    )}
                    {translateOver3 > 0 && translateOver3 < 2 && (
                        <>
                            <div className={styles['description']} style={{ opacity: translateOver3 > 1 ? (2 - translateOver3) : translateOver3, transform: `translate(0px, ${(1 - translateOver3) * 100}px)` }}>
                                <div className={styles['description-inner']}>
                                    {localizer.dic.home.page.room.features[2].description}
                                </div>
                            </div>
                            <div className={styles['title']} style={{ opacity: translateOver3 > 1 ? (2 - translateOver3) : translateOver3, transform: `translate(0px, ${(1 - translateOver3) * 100}px)` }}>
                                <div className={styles['title-inner']}>
                                    {localizer.dic.home.page.room.features[2].title}
                                </div>
                            </div>
                        </>
                    )}
                    {translateOver4 > 0 && translateOver4 < 2 && (
                        <>
                            <div className={styles['description']} style={{ opacity: translateOver4 > 1 ? (2 - translateOver4) : translateOver4, transform: `translate(0px, ${(1 - translateOver4) * 100}px)` }}>
                                <div className={styles['description-inner']}>
                                    {localizer.dic.home.page.room.features[3].description}
                                </div>
                            </div>
                            <div className={styles['title']} style={{ opacity: translateOver4 > 1 ? (2 - translateOver4) : translateOver4, transform: `translate(0px, ${(1 - translateOver4) * 100}px)` }}>
                                <div className={styles['title-inner']}>
                                    {localizer.dic.home.page.room.features[3].title}
                                </div>
                            </div>
                        </>
                    )}
                    {translateOver5 > 0 && (
                        <>
                            <div className={styles['description']} style={{ opacity: translateOver5 > 1 ? 1 : translateOver5, transform: translateOver5 > 1 ? `translate(0px, 0px)` : `translate(0px, ${(1 - translateOver5) * 100}px)` }}>
                                <div className={styles['description-inner']}>
                                    {localizer.dic.home.page.room.features[4].description}
                                </div>
                            </div>
                            <div className={styles['title']} style={{ opacity: translateOver5 > 1 ? 1 : translateOver5, transform: translateOver5 > 1 ? `translate(0px, 0px)` : `translate(0px, ${(1 - translateOver5) * 100}px)` }}>
                                <div className={styles['title-inner']}>
                                    {localizer.dic.home.page.room.features[4].title}
                                </div>
                            </div>
                        </>
                    )}
                    {/* {translateOver6 > 0 && translateOver6 < 2 && (
                        <>
                            <div className={styles['description']} style={{ opacity: translateOver6 > 1 ? (2 - translateOver6) : translateOver6, transform: `translate(0px, ${(1 - translateOver6) * 100}px)` }}>
                                <div className={styles['description-inner']}>
                                    {localizer.dic.home.page.room.features[5].description}
                                </div>
                            </div>
                            <div className={styles['title']} style={{ opacity: translateOver6 > 1 ? (2 - translateOver6) : translateOver6, transform: `translate(0px, ${(1 - translateOver6) * 100}px)` }}>
                                <div className={styles['title-inner']}>
                                    {localizer.dic.home.page.room.features[5].title}
                                </div>
                            </div>
                        </>
                    )} */}
                </div>
                {translate1 > 0 && (
                    <div className={styles['device']} style={{ opacity: translate1, transform: `translate(0px, ${(1 - translate1) * 100}px)` }}>
                        <div className={styles['device-scale']}>
                            <Pixel width={400}>
                                <div className={styles['device-inner']}>
                                    <AndroidStatusBar/>
                                    <div className={styles['player']}>
                                        <Img
                                            src={`${CDN_PROXY_URL_STRING}/abs/vi/overviews/aurora.png`}
                                            transformations={{
                                                dh: '204',
                                            }}
                                        />
                                        <video muted playsInline autoPlay loop className={styles['video']}>
                                            <source
                                                src={`${CDN_PROXY_URL_STRING}/abs/vi/overviews/aurora.mp4`}
                                                type="video/mp4"
                                            />
                                        </video>
                                    </div>
                                    <IconSwitchList className={styles['bar']} enableSyncVision enableChat={translateOver2 > 0 && translateOver2 < 2} enablePost={translateOver3 > 0 && translateOver3 < 2} />
                                    <div className={styles['widget']}>
                                        <RoomLogsWidget/>
                                        <motion.div
                                            className={styles['messages']}
                                            animate={messageAnimator}
                                            variants={messageVariants}
                                        >
                                            <MessagesWidget/>
                                        </motion.div>
                                    </div>
                                </div>
                            </Pixel>
                        </div>
                    </div>
                )}
            </div>
        </div>
    );
};

export default React.memo(RoomSection, compare);