import React from 'react';
import RoomHeadSectionRedux from './redux';

interface Props {
    children?: React.ReactNode,
}

const RoomHeadSectionProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        RoomHeadSectionRedux.reducer,
        RoomHeadSectionRedux.initialState,
    );
    return (
        <RoomHeadSectionRedux.Context.Provider value={{ state, dispatch }} >
            {children}
        </RoomHeadSectionRedux.Context.Provider>
    );
};

export default RoomHeadSectionProvider;