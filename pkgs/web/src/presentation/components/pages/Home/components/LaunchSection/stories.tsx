import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import LaunchSection from '.';

export default {
    component: LaunchSection,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof LaunchSection>;

export const Default: ComponentStoryObj<typeof LaunchSection> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}