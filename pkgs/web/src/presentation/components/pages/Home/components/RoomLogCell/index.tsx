import { useClassNames } from '@/shared/hooks/useClassNames';
import { compare } from '@/shared/modules/ObjectCompare';
import { RoomLog } from '@guildion/core';
import { LayoutProps } from '@guildion/next';
import React from 'react';
import Img from '../../../../atoms/Img';
import styles from './styles.module.scss';

type Props = {
    log: RoomLog,
} & LayoutProps;

const RoomLogCell: React.FC<Props> = ({
    style,
    className,
    log,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            {log.records.syncVision?.records.file && (
                <div className={styles['top']}>
                    <Img
                        src={log.records.syncVision.records.file.thumbnailUrl}
                        transformations={{
                            dh: '200'
                        }}
                    />
                </div>
            )}
            <div className={styles['bottom']}>
                <div className={styles['profile']}>
                    {log.records.member?.profile && (
                        <Img
                            src={log.records.member.profile.url}
                            transformations={{
                                dh: '40'
                            }}
                        />
                    )}
                </div>
                <div className={styles['center']}>
                    <div className={styles['center-name']}>
                        {log.records.member?.displayName}
                    </div>
                    <div className={styles['center-message']}>
                        {log.records.message?.body}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(RoomLogCell, compare);