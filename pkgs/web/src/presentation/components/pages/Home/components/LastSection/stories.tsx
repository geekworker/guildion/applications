import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import LastSection from '.';

export default {
    component: LastSection,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof LastSection>;

export const Default: ComponentStoryObj<typeof LastSection> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}