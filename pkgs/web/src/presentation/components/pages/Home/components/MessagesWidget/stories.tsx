import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import MessagesWidget from '.';

export default {
    component: MessagesWidget,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof MessagesWidget>;

export const Default: ComponentStoryObj<typeof MessagesWidget> = {
    args: {
    },
}