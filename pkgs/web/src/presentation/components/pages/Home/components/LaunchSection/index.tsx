import 'html5-device-mockups/dist/device-mockups.min.css';
import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { Pixel } from 'react-device-mockups';
import Img from '@/presentation/components/atoms/Img';
import { APP_NAME, CDN_PROXY_URL_STRING, Files } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { usePositionElement } from '@/shared/hooks/usePotisionElement';
import { useWindowSize } from '@/shared/hooks/useWindowSize';
import { useScroll } from '@/shared/hooks/useScroll';
import Path from '@/presentation/components/icons/Path';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import YouTubeIcon from '@/presentation/components/icons/YouTubeIcon';
import TwitterIcon from '@/presentation/components/icons/TwitterIcon';
import TikTokIcon from '@/presentation/components/icons/TikTokIcon';
import { ImEarth } from "react-icons/im";
import InstagramIcon from '@/presentation/components/icons/InstagramIcon';
import FacebookIcon from '@/presentation/components/icons/FacebookIcon';
import BrandIcon from '@/presentation/components/icons/BrandIcon';
import { Ease } from '@guildion/ui';
import { useClassNames } from '@/shared/hooks/useClassNames';
import dynamic from 'next/dynamic'
import StrokeUnderline from '@/presentation/components/icons/StrokeUnderline';
import Button from '../../../../atoms/Button';
import { downloadsRouter } from '@guildion/core/src/adaptors/endpoints/_web';
import LaunchSectionRedux from './redux';

export const useHomeLaunchSectionHeight = (): number => {
    const context = React.useContext(LaunchSectionRedux.Context);
    return context.state.sectionHeight;
};

const WelcomeBalloon = dynamic(() => import('@/presentation/components/atoms/WelcomeBalloon'), {
  ssr: false
});

type Props = {
} & LayoutProps;

const LaunchScreen: React.FC<Props> = ({
    style,
    className,
}) => {
    const window = useWindowSize();
    const interval = React.useMemo(() => 10, []);
    const pathInterval = React.useMemo(() => 1000, []);
    const frameRate = React.useMemo(() => 0.4, []);
    const { scrollTop } = useScroll();
    const positionCondition = React.useCallback(() => scrollTop < window.height * 2, [scrollTop, window.height]);
    const {
        ref: deviceInnerRef,
        ...deviceInnerInfo
    } = usePositionElement<HTMLDivElement>(positionCondition);
    const {
        ref: containerRef,
        ...containerInfo
    } = usePositionElement<HTMLDivElement>();
    const context = React.useContext(LaunchSectionRedux.Context);
    React.useEffect(() => context.dispatch(LaunchSectionRedux.Action.setSectionHeight(containerInfo.clientHeight)), [containerInfo.clientHeight]);
    const localizer = useLocalizer();
    const appTheme = useAppTheme();
    const [translate, setTranslate] = React.useState(0);
    const [translate1, setTranslate1] = React.useState(0);
    const [translateSplash1, setTranslateSplash1] = React.useState(0);
    const [translateOverSplash1, setTranslateOverSplash1] = React.useState(0);
    const [translateSplash2, setTranslateSplash2] = React.useState(0);
    const [translateOverSplash2, setTranslateOverSplash2] = React.useState(0);
    const [translateSplash3, setTranslateSplash3] = React.useState(0);
    const [translateOverSplash3, setTranslateOverSplash3] = React.useState(0);
    const [translateSplash4, setTranslateSplash4] = React.useState(0);
    const [translateOverSplash4, setTranslateOverSplash4] = React.useState(0);
    React.useEffect(() => {
        const max = window.height;
        const max1 = window.height / 2;
        const current = Math.min(1, scrollTop / max);
        const current1 = Math.min(1, scrollTop / max1);
        setTranslate(Ease.easeInOutQuint(current || 0));
        setTranslate1(Ease.easeOutCubic(current1 || 0));

        const maxSplash1 = window.height * 1;
        const maxSplash2 = window.height * 1;
        const maxSplash3 = window.height * 1;
        const maxSplash4 = window.height * 1;
        const offsetSplash1 = Math.max(0, scrollTop - max * 1);
        const offsetSplash2 = Math.max(0, scrollTop - max * 3);
        const offsetSplash3 = Math.max(0, scrollTop - max * 5);
        const offsetSplash4 = Math.max(0, scrollTop - max * 7);
        const currentSplash1 = Math.max(0, Math.min(1, offsetSplash1 / maxSplash1));
        const currentSplashOver1 = Math.max(0, Math.min(2, offsetSplash1 / maxSplash1));
        const currentSplash2 = Math.max(0, Math.min(1, offsetSplash2 / maxSplash2));
        const currentSplashOver2 = Math.max(0, Math.min(2, offsetSplash2 / maxSplash2));
        const currentSplash3 = Math.max(0, Math.min(1, offsetSplash3 / maxSplash3));
        const currentSplashOver3 = Math.max(0, Math.min(2, offsetSplash3 / maxSplash3));
        const currentSplash4 = Math.max(0, Math.min(1, offsetSplash4 / maxSplash4));
        const currentSplashOver4 = Math.max(0, Math.min(2, offsetSplash4 / maxSplash4));
        setTranslateSplash1(Ease.easeOutCubic(currentSplash1 || 0));
        setTranslateOverSplash1(Ease.easeOutCubic(currentSplashOver1 || 0));
        setTranslateSplash2(Ease.easeOutCubic(currentSplash2 || 0));
        setTranslateOverSplash2(Ease.easeOutCubic(currentSplashOver2 || 0));
        setTranslateSplash3(Ease.easeOutCubic(currentSplash3 || 0));
        setTranslateOverSplash3(Ease.easeOutCubic(currentSplashOver3 || 0));
        setTranslateSplash4(Ease.easeOutCubic(currentSplash4 || 0));
        setTranslateOverSplash4(Ease.easeOutCubic(currentSplashOver4 || 0));
    }, [scrollTop, window.height]);

    const splashScreenWidth = React.useMemo(() => {
        const to = window.width;
        const from = deviceInnerInfo.width;
        return from + ((to - from) * translate) 
    }, [window.width, deviceInnerInfo.width, translate]);
    const splashScreenHeight = React.useMemo(() => {
        const to = window.height;
        const from = deviceInnerInfo.height;
        return from + ((to - from) * translate) 
    }, [window.height, deviceInnerInfo.height, translate]);
    const splashScreenTop = React.useMemo(() => {
        const to = 0;
        const from = deviceInnerInfo.y - 64;
        return from + ((to - from) * translate) 
    }, [window, deviceInnerInfo.y, translate]);
    const splashScreenLeft = React.useMemo(() => {
        const to = 0;
        const from = deviceInnerInfo.x;
        return from + ((to - from) * translate) 
    }, [window, deviceInnerInfo.x, translate]);
    const splashScreenBorderRadius = React.useMemo(() => {
        const to = 0;
        const from = 800 / 2;
        return 0; // from + ((to - from) * translate) 
    }, [translate]);
    const story1Bottom = React.useMemo(() => {
        const from = window.height < 560 ? -580 : -740;
        const to = (window.height + from) / 2;
        return from + ((to - from) * translateSplash1) 
    }, [window, translateSplash1]);
    const story2Bottom = React.useMemo(() => {
        const from = window.height < 560 ? -580 : -740;
        const to = (window.height + from) / 2;
        return from + ((to - from) * translateSplash2) 
    }, [window, translateSplash2]);
    const story3Bottom = React.useMemo(() => {
        const from = window.height < 560 ? -580 : -740;
        const to = (window.height + from) / 2;
        return from + ((to - from) * translateSplash3) 
    }, [window, translateSplash3]);
    const story4Bottom = React.useMemo(() => {
        const from = window.height < 560 ? -580 : -740;
        const to = (window.height + from) / 2;
        return from + ((to - from) * translateSplash4) 
    }, [window, translateSplash4]);
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style} ref={containerRef}>
            <div className={styles['container-inner']}>
                <div className={styles['balloons']}>
                    <div className={styles['balloons-background']} style={{ opacity: 1 - translate1 }}>
                        <WelcomeBalloon
                            className={styles['balloons-background_balloon1']}
                            diameter={window.width > 1400 ? 420 : window.width > 765 ? 300 : 200}
                            interval={interval}
                            frameRate={frameRate}
                            rotatePx={30}
                            animated
                        />
                        <WelcomeBalloon
                            className={styles['balloons-background_balloon2']}
                            diameter={window.width > 1400 ? 520 : window.width > 765 ? 420 : 280}
                            interval={interval}
                            frameRate={frameRate}
                            offsetTime={10}
                            rotatePx={30}
                            animated
                        />
                        <WelcomeBalloon
                            className={styles['balloons-background_balloon3']}
                            diameter={window.width > 1400 ? 512 : window.width > 765 ? 412 : 260}
                            interval={interval}
                            frameRate={frameRate}
                            offsetTime={20}
                            rotatePx={30}
                            animated
                        />
                        <WelcomeBalloon
                            className={styles['balloons-background_balloon4']}
                            diameter={window.width > 1400 ? 512 : window.width > 765 ? 412 : 260}
                            interval={interval}
                            frameRate={frameRate}
                            offsetTime={30}
                            rotatePx={30}
                            animated
                        />
                    </div>
                    <div className={styles['icons']} style={{ opacity: 1 - translate1 }}>
                        {window.width > 765 && window.height > 600 && (
                            <div className={styles['icons-background']}>
                                <Path
                                    className={styles['icons-background_path1']}
                                    animationType={'repeat'}
                                    interval={pathInterval}
                                    color={appTheme.element.subp15}
                                    sx={10}
                                    sy={10}
                                    ex={100}
                                    ey={20}
                                    width={300}
                                    height={300}
                                />
                                <YouTubeIcon
                                    className={styles['icons-background_icon1']}
                                    color={appTheme.element.subp15}
                                    width={44}
                                    height={44}
                                />
                                <Path
                                    className={styles['icons-background_path2']}
                                    animationType={'repeat'}
                                    interval={pathInterval}
                                    color={appTheme.element.subp15}
                                    sx={10}
                                    sy={30}
                                    ex={100}
                                    ey={20}
                                    width={300}
                                    height={300}
                                />
                                <TwitterIcon
                                    className={styles['icons-background_icon2']}
                                    color={appTheme.element.subp15}
                                    width={44}
                                    height={44}
                                />
                                <Path
                                    className={styles['icons-background_path3']}
                                    animationType={'repeat'}
                                    interval={pathInterval}
                                    color={appTheme.element.subp15}
                                    sx={10}
                                    sy={50}
                                    ex={100}
                                    ey={40}
                                    width={300}
                                    height={300}
                                />
                                <TikTokIcon
                                    className={styles['icons-background_icon3']}
                                    color={appTheme.element.subp15}
                                    width={44}
                                    height={44}
                                />
                                <Path
                                    className={styles['icons-background_path4']}
                                    animationType={'repeat'}
                                    interval={pathInterval}
                                    color={appTheme.element.subp15}
                                    ex={10}
                                    ey={20}
                                    sx={100}
                                    sy={10}
                                    width={300}
                                    height={300}
                                />
                                <div className={styles['icons-background_icon4']}>
                                    <ImEarth
                                        color={appTheme.element.subp15}
                                        size={44}
                                    />
                                </div>
                                <Path
                                    className={styles['icons-background_path5']}
                                    animationType={'repeat'}
                                    interval={pathInterval}
                                    color={appTheme.element.subp15}
                                    ex={10}
                                    ey={20}
                                    sx={100}
                                    sy={30}
                                    width={300}
                                    height={300}
                                />
                                <InstagramIcon
                                    className={styles['icons-background_icon5']}
                                    color={appTheme.element.subp15}
                                    width={44}
                                    height={44}
                                />
                                <Path
                                    className={styles['icons-background_path6']}
                                    animationType={'repeat'}
                                    interval={pathInterval}
                                    color={appTheme.element.subp15}
                                    ex={10}
                                    ey={50}
                                    sx={100}
                                    sy={40}
                                    width={300}
                                    height={300}
                                />
                                <FacebookIcon
                                    className={styles['icons-background_icon6']}
                                    color={appTheme.element.subp15}
                                    width={44}
                                    height={44}
                                />
                            </div>
                        )}
                    </div>
                    <div className={styles['balloons-foreground']}>
                        <div className={styles['content']}>
                            <h1 className={styles['content-title']} style={{ opacity: 1 - translate1, transform: `translate(0px, ${(1 - translate1) * 40}px)` }} >
                                {localizer.dic.home.page.title}
                                <strong style={{ display: 'block' }}>{localizer.dic.g.db.creatorCommunity}</strong>
                            </h1>
                            <h2 className={styles['content-comment']} style={{ opacity: 1 - translate1, transform: `translate(0px, ${(1 - translate1) * 40}px)` }}>
                                <strong>
                                    {localizer.dic.home.page.comment}
                                    <StrokeUnderline color={appTheme.element.default}/>
                                </strong>
                            </h2>
                            <Button
                                className={styles['content-button']}
                                text={localizer.dic.home.attr.donwloads}
                                href={downloadsRouter.toPath({})}
                                style={translate1 > 0 ? { opacity: 1 - translate1 } : {}}
                            />
                            <div
                                className={styles['content-device']}
                            >
                                {window.width > 0 && (
                                    <div
                                        className={styles['content-device_inner']}
                                    >
                                        <Pixel width={window.width > 765 ? 300 : 200}>
                                            <div className={styles['content-device_screen']} style={{ width: '100%', height: '100%' }} ref={deviceInnerRef} />
                                        </Pixel>
                                    </div>
                                )}
                            </div>
                            <div
                                className={styles['splash-screen']}
                                style={{
                                    width: splashScreenWidth,
                                    height: splashScreenHeight,
                                    top: splashScreenTop,
                                    left: splashScreenLeft,
                                    borderRadius: `${splashScreenBorderRadius}px`,
                                    transition: translate > 0 && translate < 1 && window.width < 765 ? 'all 0.1s ease-in-out' : '',
                                }}
                            >
                                <div className={styles['splash-screen_logo']} style={{ opacity: 1 - translate1, transform: `translate(0px, ${(1 - translate1) * 40}px)` }}>
                                    <Img
                                        src={Files.getSeed().find(f => f.url.includes('/logos/original.png'))!.url}
                                        alt={APP_NAME}
                                        objectFit={'contain'}
                                        transformations={{
                                            dw: '250',
                                            of: 'webp',
                                        }}
                                    />
                                </div>
                                <div className={styles['splash-screen_name']} style={{ opacity: 1 - translate1, transform: `translate(0px, ${(1 - translate1) * 40}px)` }}>
                                    {APP_NAME}
                                </div>
                                <div className={styles['splash-screen-foreground']} style={{ opacity: translate1, zoom: `${(translateSplash1 + translateSplash2 + translateSplash3 + translateSplash4) * -4 + 100}%` }}>
                                    <div className={styles['splash-screen-foreground_inner']}>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-1']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_2-1']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-2']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_2-2']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-3']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-4']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_2-4']}
                                        >
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-5']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-6']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_2-6']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_1-7']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_device-vertical_2-7']}
                                        >
                                            <Pixel width={240}>
                                                <div className={styles['splash-screen-foreground_device-vertical_inner']}>
                                                    <Img
                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/rooms-screenshot.png`}
                                                        transformations={{
                                                            dw: '600',
                                                            of: 'webp',
                                                        }}
                                                    />
                                                </div>
                                            </Pixel>
                                        </div>
                                        <div
                                            className={styles['splash-screen-foreground_content']}
                                            style={{ opacity: translate1 }}
                                        >
                                            <div
                                                className={styles['splash-screen-foreground_content-icon']}
                                            >
                                                <BrandIcon color={appTheme.element.default} width={120} height={120} />
                                            </div>
                                            <div
                                                className={styles['splash-screen-foreground_content-title']}
                                            >
                                                {localizer.dic.home.page.headline.title}
                                            </div>
                                            <div
                                                className={styles['splash-screen-foreground_content-description']}
                                            >
                                                {localizer.dic.home.page.headline.description}
                                            </div>
                                        </div>
                                    </div>
                                    {translateSplash1 > 0 && (
                                        <div className={styles['splash-screen-overlay']} style={{ background: `rgba(0, 0, 0, ${Math.min(0.9, translateSplash1)})` }}>
                                            <div className={styles['pagination']}>
                                                {translateOverSplash1 > 0 && translateOverSplash1 < 2 && (
                                                    <div
                                                        className={styles['story']}
                                                        id={styles['story1']}
                                                        style={{
                                                            transform: translateOverSplash1 < 1 ? 'scale(1)' : `scale(${1 - (translateOverSplash1 - 1) * 0.5})`,
                                                            opacity: translateOverSplash1 < 1 ? 0.99 : 2 - translateOverSplash1,
                                                            bottom: `${story1Bottom}px`,
                                                        }}
                                                    >
                                                        <p className={styles['story-label']}>
                                                            {localizer.dic.home.page.headline.story1.label}
                                                        </p>
                                                        <h3 className={styles['story-title']}>
                                                            {localizer.dic.home.page.headline.story1.title}
                                                        </h3>
                                                        <h4 className={styles['story-comment']} >
                                                            <strong>
                                                                {localizer.dic.home.page.headline.story1.comment}
                                                                <StrokeUnderline color={appTheme.element.default}/>
                                                            </strong>
                                                        </h4>
                                                        <div className={styles['story-device']}>
                                                            <Pixel width={400}>
                                                                <div className={styles['story-device_inner']}>
                                                                    <Img
                                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/qr-turtorial.png`}
                                                                        transformations={{
                                                                            dw: '600',
                                                                            of: 'webp',
                                                                        }}
                                                                    />
                                                                </div>
                                                            </Pixel>
                                                        </div>
                                                    </div>
                                                )}
                                                {translateOverSplash2 > 0 && translateOverSplash2 < 2 && (
                                                    <div
                                                        className={styles['story']}
                                                        id={styles['story2']}
                                                        style={{
                                                            transform: translateOverSplash2 < 1 ? 'scale(1)' : `scale(${1 - (translateOverSplash2 - 1) * 0.5})`,
                                                            opacity: translateOverSplash2 < 1 ? 0.99 : 2 - translateOverSplash2,
                                                            bottom: `${story2Bottom}px`,
                                                        }}
                                                    >
                                                        <p className={styles['story-label']}>
                                                            {localizer.dic.home.page.headline.story2.label}
                                                        </p>
                                                        <h3 className={styles['story-title']}>
                                                            {localizer.dic.home.page.headline.story2.title}
                                                        </h3>
                                                        <h4 className={styles['story-comment']} >
                                                            <strong>
                                                                {localizer.dic.home.page.headline.story2.comment}
                                                                <StrokeUnderline color={appTheme.element.default}/>
                                                            </strong>
                                                        </h4>
                                                        <div className={styles['story-device']}>
                                                            <Pixel width={400}>
                                                                <div className={styles['story-device_inner']}>
                                                                    <Img
                                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/room-sample1.png`}
                                                                        transformations={{
                                                                            dw: '600',
                                                                            of: 'webp',
                                                                        }}
                                                                    />
                                                                </div>
                                                            </Pixel>
                                                        </div>
                                                    </div>
                                                )}
                                                {translateOverSplash3 > 0 && translateOverSplash3 < 2 && (
                                                    <div
                                                        className={styles['story']}
                                                        id={styles['story3']}
                                                        style={{
                                                            transform: translateOverSplash3 < 1 ? 'scale(1)' : `scale(${1 - (translateOverSplash3 - 1) * 0.5})`,
                                                            opacity: translateOverSplash3 < 1 ? 0.99 : 2 - translateOverSplash3,
                                                            bottom: `${story3Bottom}px`,
                                                        }}
                                                    >
                                                        <p className={styles['story-label']}>
                                                            {localizer.dic.home.page.headline.story3.label}
                                                        </p>
                                                        <h3 className={styles['story-title']}>
                                                            {localizer.dic.home.page.headline.story3.title}
                                                        </h3>
                                                        <h4 className={styles['story-comment']} >
                                                            <strong>
                                                                {localizer.dic.home.page.headline.story3.comment}
                                                                <StrokeUnderline color={appTheme.element.default}/>
                                                            </strong>
                                                        </h4>
                                                        <div className={styles['story-device']}>
                                                            <Pixel width={400}>
                                                                <div className={styles['story-device_inner']}>
                                                                    <Img
                                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/posts-screenshot.png`}
                                                                        transformations={{
                                                                            dw: '600',
                                                                            of: 'webp',
                                                                        }}
                                                                    />
                                                                </div>
                                                            </Pixel>
                                                        </div>
                                                    </div>
                                                )}
                                                {translateOverSplash4 > 0 && translateOverSplash4 < 2 && (
                                                    <div
                                                        className={styles['story']}
                                                        id={styles['story4']}
                                                        style={{
                                                            transform: translateOverSplash4 < 1 ? 'scale(1)' : `scale(${1 - (translateOverSplash4 - 1) * 0.5})`,
                                                            opacity: translateOverSplash4 < 1 ? 0.99 : 2 - translateOverSplash4,
                                                            bottom: `${story4Bottom}px`,
                                                        }}
                                                    >
                                                        <p className={styles['story-label']}>
                                                            {localizer.dic.home.page.headline.story4.label}
                                                        </p>
                                                        <h3 className={styles['story-title']}>
                                                            {localizer.dic.home.page.headline.story4.title}
                                                        </h3>
                                                        <h4 className={styles['story-comment']} >
                                                            <strong>
                                                                {localizer.dic.home.page.headline.story4.comment}
                                                                <StrokeUnderline color={appTheme.element.default}/>
                                                            </strong>
                                                        </h4>
                                                        <div className={styles['story-device']}>
                                                            <Pixel width={400}>
                                                                <div className={styles['story-device_inner']}>
                                                                    <Img
                                                                        src={`${CDN_PROXY_URL_STRING}/abs/vi/android/${localizer.lang}/guildion-premium-screenshot.png`}
                                                                        transformations={{
                                                                            dw: '600',
                                                                            of: 'webp',
                                                                        }}
                                                                    />
                                                                </div>
                                                            </Pixel>
                                                        </div>
                                                    </div>
                                                )}
                                            </div>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(LaunchScreen, compare);