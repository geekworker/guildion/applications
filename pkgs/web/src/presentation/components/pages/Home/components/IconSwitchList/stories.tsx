import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import IconSwitchList from '.';

export default {
    component: IconSwitchList,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof IconSwitchList>;

export const Default: ComponentStoryObj<typeof IconSwitchList> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}