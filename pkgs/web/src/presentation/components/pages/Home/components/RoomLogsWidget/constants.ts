import { CDN_PROXY_URL_STRING, File, Files, FileType, Member, Message, RoomLog, RoomLogs, SyncVision } from "@guildion/core";

export const jaMockRoomLogs = new RoomLogs([
    new RoomLog({}, {
        member: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
        message: new Message({ body: '視聴を開始しました' })
    }),
    new RoomLog({}, {
        member: new Member({ displayName: 'ユーザー2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
        message: new Message({ body: '動画を再生しました' }),
        syncVision: new SyncVision({}, { file: new File({ url: `${CDN_PROXY_URL_STRING}/abs/vi/overviews/aurora.png`, type: FileType.Image }) }),
    }),
]);

export const enMockRoomLogs = new RoomLogs([
    new RoomLog({}, {
        member: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
        message: new Message({ body: 'Started to watch' })
    }),
    new RoomLog({}, {
        member: new Member({ displayName: 'user2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
        message: new Message({ body: 'Started to play' }),
        syncVision: new SyncVision({}, { file: new File({ url: `${CDN_PROXY_URL_STRING}/abs/vi/overviews/aurora.png`, type: FileType.Image }) }),
    }),
]);