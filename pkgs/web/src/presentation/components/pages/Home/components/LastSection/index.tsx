import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import Img from '@/presentation/components/atoms/Img';
import { APP_NAME, Files } from '@guildion/core';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { useLocalizer } from '../../../../../../shared/hooks/useLocalizer';
import Button from '../../../../atoms/Button';
import { downloadsRouter } from '@guildion/core/src/adaptors/endpoints/_web';

type Props = {
} & LayoutProps;

const LastSection: React.FC<Props> = ({
    style,
    className,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    const localizer = useLocalizer();
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['container-inner']}>
                <div className={styles['logo']}>
                    <Img
                        src={Files.getSeed().brandLogos.find(b => b.providerKey.includes('original'))?.url ?? ''}
                        transformations={{
                            dh: '120',
                            of: 'webp',
                        }}
                    />
                </div>
                <div className={styles['name']}>
                    {APP_NAME}
                </div>
                <div className={styles['title']}>
                    {localizer.dic.home.page.footer.title}
                </div>
                <div className={styles['description']}>
                    {localizer.dic.home.page.footer.description}
                </div>
                <Button
                    className={styles['button']}
                    text={localizer.dic.home.attr.donwloads}
                    href={downloadsRouter.toPath({})}
                />
            </div>
        </div>
    );
};

export default React.memo(LastSection, compare);