import Img from '@/presentation/components/atoms/Img';
import { useClassNames } from '@/shared/hooks/useClassNames';
import { compare } from '@/shared/modules/ObjectCompare';
import { Message } from '@guildion/core';
import { LayoutProps } from '@guildion/next';
import React from 'react';
import styles from './styles.module.scss';

type Props = {
    message: Message,
} & LayoutProps;

const MessageCell: React.FC<Props> = ({
    style,
    className,
    message,
}) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            <div className={styles['container-inner']}>
                <div className={styles['left']}>
                    <div className={styles['profile']}>
                        {message.sender?.profile && (
                            <Img
                                src={message.sender.profile.url}
                                transformations={{
                                    dh: '40'
                                }}
                            />
                        )}
                    </div>
                </div>
                <div className={styles['right']}>
                    <div className={styles['right-name']}>
                        {message.sender?.displayName}
                    </div>
                    <div className={styles['right-message']}>
                        {message.body}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(MessageCell, compare);