import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import GuildSection from '.';

export default {
    component: GuildSection,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof GuildSection>;

export const Default: ComponentStoryObj<typeof GuildSection> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}