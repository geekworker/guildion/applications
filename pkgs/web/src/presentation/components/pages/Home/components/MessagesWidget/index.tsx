import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import classNames from 'classnames';
import { LayoutProps } from '@guildion/next';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { LanguageCode, Messages } from '@guildion/core';
import { enMockMessages, jaMockMessages } from './constants';
import MessageCell from '../MessageCell';
import useInterval from 'use-interval';
import { scrollTo } from '@/shared/modules/ScrollTo';

type Props = {
} & Partial<LayoutProps>;

const MessagesWidget: React.FC<Props> = ({
    style,
    className,
}) => {
    const localizer = useLocalizer();
    const _messages = React.useMemo(() => localizer.lang === LanguageCode.Japanese ? jaMockMessages : enMockMessages, [localizer.lang]);
    const [messages, setMessages] = React.useState(new Messages([_messages.get(0)!]));
    const scrollRef = React.useRef<HTMLDivElement>(null);

    useInterval(() => {
        if (messages.size < _messages.size && _messages.get(messages.size) && scrollRef.current) {
            setMessages(messages.append(_messages.get(messages.size)!));
            scrollTo(scrollRef.current, scrollRef.current.scrollHeight, 1000);
        }
    }, 3000);

    return (
        <div className={classNames(className, styles['container'])} style={style} ref={scrollRef}>
            <div className={styles['container-inner']}>
                <div className={styles['items']}>
                    {messages.toArray().map((message, i) => (
                        <div className={styles['item']} key={i}>
                            <MessageCell message={message} />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default React.memo(MessagesWidget, compare);