import React from 'react';
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { AnyAction } from 'redux';

namespace RoomHeadSectionRedux {
    export class State extends Record<{
        sectionHeight: number,
    }>({
        sectionHeight: 0,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        setSectionHeight: actionCreator<number>('ROOM_HEAD_SECTION_SET_SECTION_HEIGHT'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.setSectionHeight, (state, payload) => {
            state = state.set('sectionHeight', payload);
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: RoomHeadSectionRedux.State,
        dispatch: React.Dispatch<AnyAction>,
    });
}

export default RoomHeadSectionRedux