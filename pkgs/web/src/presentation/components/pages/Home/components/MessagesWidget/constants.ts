import { Files, Member, Message, Messages } from "@guildion/core";

export const jaMockMessages = new Messages([
    new Message({
        body: "今日のライブ映像みんなで見よう！",
    }, {
        sender: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "OK!",
    }, {
        sender: new Member({ displayName: 'ユーザー2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "ここのボーカルマジで最高！",
    }, {
        sender: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "たしかに！このバンドドラムもなかなか凄いよ。",
    }, {
        sender: new Member({ displayName: 'ユーザー2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "なんか噂で聞いたことある。確かにめっちゃ上手かった。",
    }, {
        sender: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "これってどこに保存してあるっけ？",
    }, {
        sender: new Member({ displayName: 'ユーザー2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "今日の日付のプレイリストに保存してあるよー",
    }, {
        sender: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "ナイス！",
    }, {
        sender: new Member({ displayName: 'ユーザー2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "ドラムソロめっちゃかっけーやん！",
    }, {
        sender: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "さすがすぎるwww",
    }, {
        sender: new Member({ displayName: 'ユーザー1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
]);

export const enMockMessages = new Messages([
    new Message({
        body: "Let's check today's live movie!",
    }, {
        sender: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "OK!",
    }, {
        sender: new Member({ displayName: 'user2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "His voice is very addiction！ ",
    }, {
        sender: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "Sure！The drumer in this band is also very cool.",
    }, {
        sender: new Member({ displayName: 'user2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "I've heard rumors about it. Certainly it was really good.",
    }, {
        sender: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "Where is this stored？",
    }, {
        sender: new Member({ displayName: 'user2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "I've saved it in a playlist dated today.",
    }, {
        sender: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "Nice！",
    }, {
        sender: new Member({ displayName: 'user2' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('blue')) }),
    }),
    new Message({
        body: "Drum solo is really cool！",
    }, {
        sender: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
    new Message({
        body: "As expected."
    }, {
        sender: new Member({ displayName: 'user1' }, { profile: Files.getSeed().userProfiles.find(p => p.displayName.includes('green')) }),
    }),
]);
