import React from 'react';
import styles from './styles.module.scss';
import { compare } from '@/shared/modules/ObjectCompare';
import classNames from 'classnames';
import { LayoutProps } from '@guildion/next';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { LanguageCode, RoomLogs } from '@guildion/core';
import { enMockRoomLogs, jaMockRoomLogs } from './constants';
import RoomLogCell from '../RoomLogCell';

type Props = {
} & Partial<LayoutProps>;

const RoomLogsWidget: React.FC<Props> = ({
    style,
    className,
}) => {
    const localizer = useLocalizer();
    const logs = React.useMemo(() => localizer.lang === LanguageCode.Japanese ? jaMockRoomLogs : enMockRoomLogs, [localizer.lang]);
    return (
        <div className={classNames(className, styles['container'])} style={style}>
            <div className={styles['container-inner']}>
                <div className={styles['title']}>
                    {localizer.dic.room.show.logs.title}
                </div>
                <div className={styles['items']}>
                    {logs.toArray().map((log, i) => (
                        <div className={styles['item']} key={i}>
                            <RoomLogCell log={log} />
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default React.memo(RoomLogsWidget, compare);