import React from 'react';
import LaunchSectionRedux from './redux';

interface Props {
    children?: React.ReactNode,
}

const LaunchSectionProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        LaunchSectionRedux.reducer,
        LaunchSectionRedux.initialState,
    );
    return (
        <LaunchSectionRedux.Context.Provider value={{ state, dispatch }} >
            {children}
        </LaunchSectionRedux.Context.Provider>
    );
};

export default LaunchSectionProvider;