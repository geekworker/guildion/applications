import React from 'react';
import HomePresenter from './presenter';
import HomeComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: HomePresenter.MergedProps) => {
    const output = HomePresenter.usePresenter(props);
    return <HomeComponent {...output} />;
};

const Home = connect<HomePresenter.StateProps, HomePresenter.DispatchProps, HomePresenter.Input, HomePresenter.MergedProps, RootState>(
    HomePresenter.mapStateToProps,
    HomePresenter.mapDispatchToProps,
    HomePresenter.mergeProps,
    HomePresenter.connectOptions,
)(Container);

export default Home;