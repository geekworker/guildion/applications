import React from 'react';
import DocumentShowPresenter from './presenter';
import DocumentShowComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: DocumentShowPresenter.MergedProps) => {
    const output = DocumentShowPresenter.usePresenter(props);
    return <DocumentShowComponent {...output} />;
};

const DocumentShow = connect<DocumentShowPresenter.StateProps, DocumentShowPresenter.DispatchProps, DocumentShowPresenter.Input, DocumentShowPresenter.MergedProps, RootState>(
    DocumentShowPresenter.mapStateToProps,
    DocumentShowPresenter.mapDispatchToProps,
    DocumentShowPresenter.mergeProps,
    DocumentShowPresenter.connectOptions,
)(Container);

export default DocumentShow;