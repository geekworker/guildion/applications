import DocumentShowHooks from './hooks';
import { Action, bindActionCreators } from 'redux';
import { RootState } from '@/presentation/redux/RootReducer';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { documentShowPageSelector } from '@/presentation/redux/Document/DocumentSelector';
import { documentGroupShowPageSelector } from '@/presentation/redux/DocumentGroup/DocumentGroupSelector';
import { DocumentPageState } from '@/presentation/redux/Document/DocumentReducer';
import { DocumentGroupPageState } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';
import { Localizer } from '@guildion/core';
import { AppTheme } from '@guildion/ui';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { useAppTheme } from '@/shared/hooks/useAppTheme';

namespace DocumentShowPresenter {
    export type StateProps = {
        page?: DocumentPageState,
        groupPage?: DocumentGroupPageState,
    }
    
    export type DispatchProps = {
    }
    
    export type Input = {
        groupSlug: string,
        sectionSlug: string,
        slug: string,
        disableSEO?: boolean,
    } & LayoutProps;
    
    export type Output = {
        localizer: Localizer,
        appTheme: AppTheme,
    } & Omit<MergedProps, ''>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            page: documentShowPageSelector(state, { ...ownProps }),
            groupPage: documentGroupShowPageSelector(state, ownProps.groupSlug),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.app.csr == next.app.csr &&
            prev.documentGroup.pages.show == next.documentGroup.pages.show &&
            prev.document.pages.show == next.document.pages.show
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const {} = DocumentShowHooks.useState(props);
        return {
            ...props,
            localizer: useLocalizer(),
            appTheme: useAppTheme(),
        }
    }
}

export default DocumentShowPresenter;