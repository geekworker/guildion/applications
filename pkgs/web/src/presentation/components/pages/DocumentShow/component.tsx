import React from 'react';
import styles from './styles.module.scss'
import DocumentShowPresenter from './presenter';
import { useClassNames } from '@/shared/hooks/useClassNames';
import Head from 'next/head';
import { APP_NAME } from '@guildion/core';
import DocumentHTMLTemplate from '../../templates/DocumentHTMLTemplate';

const DocumentShowComponent = ({ style, className, groupSlug, slug, sectionSlug, page, groupPage, localizer, disableSEO }: DocumentShowPresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    if (!page || !groupPage) return <></>;
    return (
        <>
            {!disableSEO && (
                <Head>
                    <title>
                        {page.get('document').getTitle(localizer.lang)} | {APP_NAME}
                    </title>
                    <meta
                        name="description"
                        content={page.get('document').getDescription(localizer.lang)}
                    />
                </Head>
            )}
            <div className={containerClassName} style={style}>
                <DocumentHTMLTemplate
                    page={page}
                    groupPage={groupPage}
                />
            </div>
        </>
    );
};

export default React.memo(DocumentShowComponent, DocumentShowPresenter.outputAreEqual);