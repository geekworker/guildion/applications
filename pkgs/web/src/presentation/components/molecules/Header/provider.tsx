import React from 'react';
import HeaderRedux from './redux';

interface Props {
    children?: React.ReactNode,
}

const HeaderProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        HeaderRedux.reducer,
        HeaderRedux.initialState,
    );
    return (
        <HeaderRedux.Context.Provider value={{ state, dispatch }} >
            {children}
        </HeaderRedux.Context.Provider>
    );
};

export default HeaderProvider;