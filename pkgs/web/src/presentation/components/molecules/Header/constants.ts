export const HeaderType = {
    Default: "Default",
    Fixed: "Fixed",
} as const;

export type HeaderType = typeof HeaderType[keyof typeof HeaderType];