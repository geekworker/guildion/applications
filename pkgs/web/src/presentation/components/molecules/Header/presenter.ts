import HeaderHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { HeaderType } from './constants';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { AppTheme } from '@guildion/ui';
import { LayoutProps } from '@guildion/next';

namespace HeaderPresenter {
    export type Input = {
    } & LayoutProps;
    
    export type Output = {
        type: HeaderType,
        appTheme: AppTheme,
        height: number,
        scrolling: boolean,
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {
            type,
            scrolling,
        } = HeaderHooks.useState(props);
        const appTheme = useAppTheme();
        const height = HeaderHooks.useHeight(props);
        return {
            ...props,
            type,
            appTheme,
            height,
            scrolling,
        }
    }
}

export default HeaderPresenter;