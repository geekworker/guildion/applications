import React from 'react';
import { HeaderType } from './constants';
import HeaderPresenter from './presenter';
import styles from './styles.module.scss';
import Link from 'next/link';
import BrandIcon from '../../icons/BrandIcon';
import { APP_NAME, WebEndpoints } from '@guildion/core';
import { useClassNames } from '@/shared/hooks/useClassNames';

const HeaderComponent = ({ type, appTheme, height, scrolling, className, style }: HeaderPresenter.Output) => {
    const defaultClassName = useClassNames(className, styles['default'], { [styles['default-hidden']]: scrolling && type !== HeaderType.Fixed });
    const defaultHeader = (
        <div style={{ ...style, paddingTop: height }} id={styles['header']}>
            <div className={defaultClassName}>
                <div className={styles['default-inner']}>
                    <Link href={WebEndpoints.homeRouter.path}>
                        <a className={styles['default-logo']}>
                            <BrandIcon width={20} height={20} color={appTheme.element.default} />
                            <span className={styles['default-logo_name']}>
                                {APP_NAME}
                            </span>
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    );

    switch(type) {
    case HeaderType.Fixed:
    default:
    case HeaderType.Default:
        return defaultHeader;
    };
};

export default React.memo(HeaderComponent, HeaderPresenter.outputAreEqual);