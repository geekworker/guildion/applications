import { useScroll } from '@/shared/hooks/useScroll';
import React from 'react';
import { HeaderType } from './constants';
import type HeaderPresenter from './presenter';
import HeaderRedux from './redux';

namespace HeaderHooks {
    export const useState = (props: HeaderPresenter.Input) => {
        const context = React.useContext(HeaderRedux.Context);
        const {
            scrolling,
        } = useScroll();
        return {
            type: context.state.type,
            scrolling,
        };
    }

    export const useHeight = (props: HeaderPresenter.Input): number => {
        const context = React.useContext(HeaderRedux.Context);
        switch(context.state.type) {
        default:
        case HeaderType.Default: return 64;
        }
    }

    export const useFixed = () => {
        const context = React.useContext(HeaderRedux.Context);
        React.useEffect(() => {
            context.dispatch(HeaderRedux.Action.setType(HeaderType.Fixed));
            return () => context.dispatch(HeaderRedux.Action.setType(HeaderType.Default));
        }, []);
    }
}

export default HeaderHooks;