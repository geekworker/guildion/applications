import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Action as ReduxAction } from "redux";
import { HeaderType } from "./constants";

namespace HeaderRedux {
    export class State extends Record<{
        type: HeaderType,
    }>({
        type: HeaderType.Default,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        setType: actionCreator<HeaderType>("SET_HEADER_TYPE"),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.setType, (state, payload) => {
            state = state.set('type', payload);
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: HeaderRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });

    export type Context = {
        state: HeaderRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    };
}

export default HeaderRedux