import FooterHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { FooterType } from './constants';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { Localizer } from '@guildion/core';

namespace FooterPresenter {
    export type Input = {
    } & LayoutProps;
    
    export type Output = {
        type: FooterType,
        appTheme: AppTheme,
        localizer: Localizer,
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {
            type,
        } = FooterHooks.useState(props);
        const appTheme = useAppTheme(); 
        const localizer = useLocalizer();
        return {
            ...props,
            type,
            appTheme,
            localizer,
        }
    }
}

export default FooterPresenter;