import { Localizer, OS } from "@guildion/core";
import { WebEndpoints } from "@guildion/core";

export const FooterType = {
    Default: "Default",
    Document: "Document",
} as const;
  
export type FooterType = typeof FooterType[keyof typeof FooterType];

export interface FooterLink {
    name: () => string,
    link: string,
}

export interface FooterLinks {
    name: () => string,
    links: FooterLink[],
}

export const getFooterLinks = (localizer: Localizer): FooterLinks[] => [
    {
        name: () => localizer.dic.home.footer.service.name,
        links: [
            {
                name: () => localizer.dic.home.footer.service.site,
                link: WebEndpoints.homeRouter.path,
            },
            {
                name: () => localizer.dic.home.attr.donwloads,
                link: WebEndpoints.downloadsRouter.toPath({}),
            },
            {
                name: () => localizer.dic.home.footer.service.premium,
                link: WebEndpoints.premiumRouter.path,
            },
            {
                name: () => localizer.dic.home.footer.service.pricing,
                link: WebEndpoints.pricingRouter.path,
            },
        ],
    },
    {
        name: () => localizer.dic.home.footer.platform.name,
        links: [
            {
                name: () => localizer.dic.home.footer.platform.ios,
                link: WebEndpoints.downloadsRouter.toPath({ os: OS.iOS }),
            },
            {
                name: () => localizer.dic.home.footer.platform.android,
                link: WebEndpoints.downloadsRouter.toPath({ os: OS.Android }),
            },
        ],
    },
    // {
    //     name: () => localizer.dic.home.footer.forUsers.name,
    //     links: [
    //     ],
    // },
    {
        name: () => localizer.dic.home.footer.forCreators.name,
        links: [
            {
                name: () => localizer.dic.home.footer.forCreators.premium,
                link: WebEndpoints.documentGroupShowRouter.toPath({ groupSlug: 'premium-guild' }),
            },
        ],
    },
    {
        name: () => localizer.dic.home.footer.support.name,
        links: [
            {
                name: () => localizer.dic.home.footer.support.contact,
                link: WebEndpoints.contactRouter.path,
            },
        ],
    },
];