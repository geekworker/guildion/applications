import React from 'react';
import FooterComponent from './component';
import FooterPresenter from './presenter';

const Footer = (props: FooterPresenter.Input) => {
    const output = FooterPresenter.usePresenter(props);
    return <FooterComponent {...output} />;
};

export default React.memo(Footer, FooterPresenter.inputAreEqual);