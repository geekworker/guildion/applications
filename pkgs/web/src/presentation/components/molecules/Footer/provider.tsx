import React from 'react';
import FooterRedux from './redux';

interface Props {
    children?: React.ReactNode,
}

const FooterProvider: React.FC<Props> = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        FooterRedux.reducer,
        FooterRedux.initialState,
    );
    return (
        <FooterRedux.Context.Provider value={{ state, dispatch }} >
            {children}
        </FooterRedux.Context.Provider>
    );
};

export default FooterProvider;