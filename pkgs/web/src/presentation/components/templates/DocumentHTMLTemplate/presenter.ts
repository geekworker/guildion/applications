import DocumentHTMLTemplateHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { DocumentGroup, DocumentSection, Document, Localizer } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { DocumentPageState } from '@/presentation/redux/Document/DocumentReducer';
import { DocumentGroupPageState } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';

namespace DocumentHTMLTemplatePresenter {
    export type Input = {
        page: DocumentPageState,
        groupPage: DocumentGroupPageState,
    } & LayoutProps;
    
    export type Output = {
        appTheme: AppTheme,
        localizer: Localizer,
        document: Document,
        group: DocumentGroup,
        section: DocumentSection,
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {
            document,
            section,
            group,
        } = DocumentHTMLTemplateHooks.useState(props);
        return {
            ...props,
            document,
            section,
            group,
            localizer: useLocalizer(),
            appTheme: useAppTheme(),
        }
    }
}

export default DocumentHTMLTemplatePresenter;