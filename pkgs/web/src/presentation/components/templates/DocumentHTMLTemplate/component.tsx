import { useClassNames } from '@/shared/hooks/useClassNames';
import React from 'react';
import styles from './styles.module.scss';
import DocumentHTMLTemplatePresenter from './presenter';
import BreadCrumbs from '../../organisms/BreadCrumbs';
import { WebEndpoints } from '@guildion/core';
import DocumentSideBar from '../../organisms/DocumentSideBar';
import DocumentHTMLViewer from '../../atoms/DocumentHTMLViewer';

const DocumentHTMLTemplateComponent = ({ style, appTheme, className, localizer, document, group, section }: DocumentHTMLTemplatePresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);
    return (
        <div className={containerClassName} style={style}>
            <BreadCrumbs
                data={[
                    {
                        title: group.getName(localizer.lang),
                        href: WebEndpoints.documentGroupShowRouter.toPath({ groupSlug: group.slug })
                    },
                    {
                        title: section.getName(localizer.lang),
                        href: WebEndpoints.documentSectionShowRouter.toPath({ groupSlug: group.slug, sectionSlug: section.slug })
                    },
                ]}
                className={styles['nav']}
            />
            <div className={styles['bar']}>
                <DocumentSideBar group={group} currentSlug={document.slug} currentSectionSlug={section.slug} />
            </div>
            <div className={styles['container-inner']}>
                <div className={styles['side']}>
                </div>
                <div className={styles['content']}>
                    <div className={styles['content-inner']}>
                        <div className={styles['content-header']}>
                            <div className={styles['content-header_title']}>
                                {document.getTitle(localizer.lang)}
                            </div>
                            <div className={styles['content-header_border']}/>
                        </div>
                        <div className={styles['content-body']}>
                            <DocumentHTMLViewer document={document} />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(DocumentHTMLTemplateComponent, DocumentHTMLTemplatePresenter.outputAreEqual);