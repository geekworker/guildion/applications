import React from 'react';
import DocumentHTMLTemplateComponent from './component';
import DocumentHTMLTemplatePresenter from './presenter';

const DocumentHTMLTemplate = (props: DocumentHTMLTemplatePresenter.Input) => {
    const output = DocumentHTMLTemplatePresenter.usePresenter(props);
    return <DocumentHTMLTemplateComponent {...output} />;
};

export default React.memo(DocumentHTMLTemplate, DocumentHTMLTemplatePresenter.inputAreEqual);