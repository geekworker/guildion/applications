import React from 'react';
import { DocumentSection } from '@guildion/core';
import type DocumentHTMLTemplatePresenter from './presenter';

namespace DocumentHTMLTemplateHooks {
    export const useState = (props: DocumentHTMLTemplatePresenter.Input) => {
        const document = React.useMemo(() => props.page.get('document'), [props.page.get('document')]);
        const section = React.useMemo(() => document.records.section ?? new DocumentSection(), [document]);
        const group = React.useMemo(() => props.groupPage.get('group'), [props.groupPage.get('group')]);
        return {
            document,
            section,
            group,
        };
    }
}

export default DocumentHTMLTemplateHooks;