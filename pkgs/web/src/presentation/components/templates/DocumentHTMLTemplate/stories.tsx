import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import DocumentHTMLTemplate from '.';

export default {
    component: DocumentHTMLTemplate,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof DocumentHTMLTemplate>;

export const Default: ComponentStoryObj<typeof DocumentHTMLTemplate> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}