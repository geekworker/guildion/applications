import DocumentGroupDefaultTemplateHooks from './hooks';
import { compare } from '@/shared/modules/ObjectCompare';
import { LayoutProps } from '@guildion/next';
import { AppTheme } from '@guildion/ui';
import { useAppTheme } from '@/shared/hooks/useAppTheme';
import { DocumentGroup, Localizer } from '@guildion/core';
import { useLocalizer } from '@/shared/hooks/useLocalizer';
import { DocumentGroupPageState } from '@/presentation/redux/DocumentGroup/DocumentGroupReducer';

namespace DocumentGroupDefaultTemplatePresenter {
    export type Input = {
        page: DocumentGroupPageState,
    } & LayoutProps;
    
    export type Output = {
        appTheme: AppTheme,
        localizer: Localizer,
        group: DocumentGroup,
    } & Omit<Input, ''>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const {
            group,
        } = DocumentGroupDefaultTemplateHooks.useState(props);
        return {
            ...props,
            appTheme: useAppTheme(),
            localizer: useLocalizer(),
            group,
        }
    }
}

export default DocumentGroupDefaultTemplatePresenter;