import React from 'react';
import type DocumentGroupDefaultTemplatePresenter from './presenter';

namespace DocumentGroupDefaultTemplateHooks {
    export const useState = (props: DocumentGroupDefaultTemplatePresenter.Input) => {
        const group = React.useMemo(() => props.page.get('group'), [props.page.get('group')])
        return {
            group,
        };
    }
}

export default DocumentGroupDefaultTemplateHooks;