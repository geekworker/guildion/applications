import React from 'react';
import DocumentGroupDefaultTemplateComponent from './component';
import DocumentGroupDefaultTemplatePresenter from './presenter';

const DocumentGroupDefaultTemplate = (props: DocumentGroupDefaultTemplatePresenter.Input) => {
    const output = DocumentGroupDefaultTemplatePresenter.usePresenter(props);
    return <DocumentGroupDefaultTemplateComponent {...output} />;
};

export default React.memo(DocumentGroupDefaultTemplate, DocumentGroupDefaultTemplatePresenter.inputAreEqual);