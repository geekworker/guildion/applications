import AppProvider from '@/infrastructure/AppProvider';
import StorybookProvider from '@/infrastructure/StorybookProvider';
import { ComponentMeta, ComponentStoryObj } from '@storybook/react';
import DocumentGroupDefaultTemplate from '.';

export default {
    component: DocumentGroupDefaultTemplate,
    decorators: [
        (storyFn) => (
            <StorybookProvider>
                <AppProvider>
                    {storyFn()}
                </AppProvider>
            </StorybookProvider>
        )
    ]
} as ComponentMeta<typeof DocumentGroupDefaultTemplate>;

export const Default: ComponentStoryObj<typeof DocumentGroupDefaultTemplate> = {
    args: {
        style: {
            width: '100vw',
            height: '100vh',
        },
    },
}