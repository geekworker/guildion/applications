import React from 'react';
import styles from './styles.module.scss';
import DocumentGroupDefaultTemplatePresenter from './presenter';
import BrandIcon from '../../icons/BrandIcon';
import { useClassNames } from '@/shared/hooks/useClassNames';
import Link from 'next/link';
import { DocumentSection, DocumentSections, DocumentSectionType, WebEndpoints } from '@guildion/core';
import { HiArrowNarrowRight } from 'react-icons/hi';

const DocumentGroupDefaultTemplateComponent = ({ style, appTheme, className, localizer, page, group }: DocumentGroupDefaultTemplatePresenter.Output) => {
    const containerClassName = useClassNames(className, styles['container']);

    const renderDefaultSection = (s: DocumentSection, i: number | string) => (
        <li className={styles['contents-section_inner']} key={i} id={`${s.slug}`}>
            <div className={styles['contents-section']} >
                <Link href={WebEndpoints.documentSectionShowRouter.toPath({ groupSlug: group.slug, sectionSlug: s.slug })}>
                    <a className={styles['contents-section_title']}>
                        <h2 className={styles['contents-section_title-inner']}>
                            {s.getName(localizer.lang)}
                        </h2>
                    </a>
                </Link>
            </div>
            <ul className={styles['contents-documents']}>
                {s.records.documents && s.records.documents.toArray().map((d, i) => (
                    <li className={styles['contents-document']} key={i}>
                        <Link href={WebEndpoints.documentShowRouter.toPath({ groupSlug: group.slug, sectionSlug: s.slug, slug: d.slug })}>
                            <a className={styles['contents-document_link']}>
                                <h3 className={styles['contents-document_title']}>
                                    {d.getTitle(localizer.lang)}
                                </h3>
                                <h4 className={styles['contents-document_description']}>
                                    {d.getDescription(localizer.lang)}
                                </h4>
                                <div className={styles['contents-document_button']}>
                                    <span className={styles['contents-document_button-title']}>
                                        {localizer.dic.g.seeMore}
                                    </span>
                                    <HiArrowNarrowRight size={20} color={appTheme.element.link} />
                                </div>
                            </a>
                        </Link>
                    </li>
                ))}
            </ul>
        </li>
    );

    const renderAllInOneSection = (s: DocumentSection, i: number | string) => (
        <li className={styles['contents-section_inner']} key={i}>
            <div className={styles['contents-section']}>
                <h2 className={styles['contents-section_title']}>
                    {s.getName(localizer.lang)}
                </h2>
            </div>
        </li>
    );

    const renderSections = (sections: DocumentSections) => (
        <ul className={styles['contents-sections']}>
            {sections.toArray().map((s, i) =>
                // s.type === DocumentSectionType.ALL_IN_ONE ?
                //     renderAllInOneSection(s, i) :
                    renderDefaultSection(s, i)
            )}
        </ul>
    );

    return (
        <div className={containerClassName} style={style}>
            <div className={styles['header']}>
                <BrandIcon width={120} height={120} color={appTheme.element.default} />
                <h1 className={styles['header-text']}>
                    {group.getName(localizer.lang)}
                </h1>
            </div>
            <div className={styles['aside']}>
                <div className={styles['aside-inner']}>
                    <div className={styles['side']}>
                        <div className={styles['side-inner']}>
                            <ul className={styles['side-sections']}>
                                {group.records.sections && group.records.sections?.toArray().map((s, k) => (
                                    <li className={styles['side-section']} key={k}>
                                        <Link href={`#${s.slug}`}>
                                            <a>
                                                {s.getName(localizer.lang)}
                                            </a>
                                        </Link>
                                    </li>
                                ))}
                            </ul>
                        </div>
                    </div>
                    <div className={styles['contents']}>
                        <div className={styles['contents-inner']}>
                            <div className={styles['contents-header']}>
                                <h1 className={styles['contents-header_title']}>
                                    {group.getName(localizer.lang)}
                                </h1>
                            </div>
                            {group.records.sections && renderSections(group.records.sections)}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default React.memo(DocumentGroupDefaultTemplateComponent, DocumentGroupDefaultTemplatePresenter.outputAreEqual);