import { UPDATE_STORYBOOK_CURRENT_CONFIG } from "@/shared/constants/Config";
import AsyncStorage from "@react-native-async-storage/async-storage";

const getStorybookUIRoot = () => {
    UPDATE_STORYBOOK_CURRENT_CONFIG();
    const { AppRegistry } = require('react-native');
    const { getStorybookUI, configure, addDecorator } = require('@storybook/react-native');
    const { withKnobs } = require('@storybook/addon-knobs');
    const { loadStories } = require('./storyLoader');
    require('./rn-addons');
    
    addDecorator(withKnobs);

    configure(() => {
        loadStories();
    }, module);

    const StorybookUIRoot = getStorybookUI({
        host: '0.0.0.0',
        asyncStorage: AsyncStorage,
    });

    AppRegistry.registerComponent('%APP_NAME%', () => StorybookUIRoot);

    return StorybookUIRoot;
}

export default getStorybookUIRoot;
