import React from "react";

export interface IconNodeProps {
    size: number,
    color: string,
}

type IconNodeClass = React.FC<IconNodeProps>;

export default IconNodeClass;