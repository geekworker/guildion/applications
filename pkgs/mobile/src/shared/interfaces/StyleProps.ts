import { AppTheme } from "../constants/AppTheme";
import AnimatedStyle from "./AnimatedStyle";
import { Style } from "./Style";

interface StyleProps {
    appTheme?: AppTheme,
    style?: Style,
    animatedStyle?: AnimatedStyle,
}

export default StyleProps;