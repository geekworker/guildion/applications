import { StyleSheet } from "react-native";
import { Style } from "./Style";

export type StylesKey<S extends (val: any) => StyleSheet.NamedStyles<any>> = keyof ReturnType<S>;

export interface StylesImpl<S extends (val: any) => StyleSheet.NamedStyles<any>> {
    getStyle: (key: StylesKey<S>) => Style
}