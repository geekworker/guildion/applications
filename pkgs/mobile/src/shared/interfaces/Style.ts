import { Record } from "immutable";
import { ImageStyle, TextStyle, ViewStyle } from "react-native";

export class Style extends Record<ViewStyle & TextStyle & ImageStyle>({
    backfaceVisibility: undefined,
    backgroundColor: undefined,
    borderBottomColor: undefined,
    borderBottomEndRadius: undefined,
    borderBottomLeftRadius: undefined,
    borderBottomRightRadius: undefined,
    borderBottomStartRadius: undefined,
    borderBottomWidth: undefined,
    borderColor: undefined,
    borderEndColor: undefined,
    borderLeftColor: undefined,
    borderLeftWidth: undefined,
    borderRadius: undefined,
    borderRightColor: undefined,
    borderRightWidth: undefined,
    borderStartColor: undefined,
    borderStyle: undefined,
    borderTopColor: undefined,
    borderTopEndRadius: undefined,
    borderTopLeftRadius: undefined,
    borderTopRightRadius: undefined,
    borderTopStartRadius: undefined,
    borderTopWidth: undefined,
    borderWidth: undefined,
    opacity: undefined,
    elevation: undefined,
    color: undefined,
    fontFamily: undefined,
    fontSize: undefined,
    fontStyle: undefined,
    fontWeight: undefined,
    letterSpacing: undefined,
    lineHeight: undefined,
    textAlign: undefined,
    textDecorationLine: undefined,
    textDecorationStyle: undefined,
    textDecorationColor: undefined,
    textShadowColor: undefined,
    textShadowOffset: undefined,
    textShadowRadius: undefined,
    textTransform: undefined,
    resizeMode: undefined,
    overlayColor: undefined,
    tintColor: undefined,
    
    alignContent: undefined,
    alignItems: undefined,
    alignSelf: undefined,
    aspectRatio: undefined,
    borderEndWidth: undefined,
    borderStartWidth: undefined,
    bottom: undefined,
    display: undefined,
    end: undefined,
    flex: undefined,
    flexBasis: undefined,
    flexDirection: undefined,
    flexGrow: undefined,
    flexShrink: undefined,
    flexWrap: undefined,
    height: undefined,
    justifyContent: undefined,
    left: undefined,
    margin: 0,
    marginBottom: undefined,
    marginEnd: undefined,
    marginHorizontal: undefined,
    marginLeft: undefined,
    marginRight: undefined,
    marginStart: undefined,
    marginTop: undefined,
    marginVertical: undefined,
    maxHeight: undefined,
    maxWidth: undefined,
    minHeight: undefined,
    minWidth: undefined,
    overflow: undefined,
    padding: 0,
    paddingBottom: undefined,
    paddingEnd: undefined,
    paddingHorizontal: undefined,
    paddingLeft: undefined,
    paddingRight: undefined,
    paddingStart: undefined,
    paddingTop: undefined,
    paddingVertical: undefined,
    position: undefined,
    right: undefined,
    start: undefined,
    top: undefined,
    width: undefined,
    zIndex: undefined,
    direction: undefined,
    shadowColor: undefined,
    shadowOffset: undefined,
    shadowOpacity: undefined,
    shadowRadius: undefined,

    transform: undefined,
    transformMatrix: undefined,
    rotation: undefined,
    scaleX: undefined,
    scaleY: undefined,
    translateX: undefined,
    translateY: undefined,

    fontVariant: undefined,
    writingDirection: undefined,
    textAlignVertical: undefined,
    includeFontPadding: undefined,
}) {
    public getAsNumber(key: keyof (ViewStyle & TextStyle & ImageStyle)): number {
        const result = Number(this.get(key) ?? 0);
        return !!result ? result : 0;
    }

    public get innerWidth(): number {
        const width = this.getAsNumber('width') || 0;
        const marginLeft = this.getAsNumber('marginLeft') || this.getAsNumber('margin') || 0;
        const marginRight = this.getAsNumber('marginRight') || this.getAsNumber('margin') || 0;
        const paddingLeft = this.getAsNumber('paddingLeft') || this.getAsNumber('padding') || 0;
        const paddingRight = this.getAsNumber('paddingRight') || this.getAsNumber('padding') || 0;
        return width - marginLeft - marginRight - paddingLeft - paddingRight;
    }
    
    public get innerHeight(): number {
        const height = this.getAsNumber('height') || 0;
        const marginTop = this.getAsNumber('marginTop') || this.getAsNumber('margin') || 0;
        const marginBottom = this.getAsNumber('marginBottom') || this.getAsNumber('margin') || 0;
        const paddingTop = this.getAsNumber('paddingTop') || this.getAsNumber('padding') || 0;
        const paddingBottom = this.getAsNumber('paddingBottom') || this.getAsNumber('padding') || 0;
        return height - marginTop - marginBottom - paddingTop - paddingBottom;
    }

    public toStyleObject(): ViewStyle & TextStyle & ImageStyle {
        const json = this.toJSON();
        Object.keys(json).forEach((key) => json[key as keyof typeof json] === undefined ? delete json[key as keyof typeof json] : {});
        return json;
    }
}