export type Route = {
    name: string,
    key: string,
    params: { [key: string]: any }
}

export const isRoute = (obj: any): obj is Route => {
    return typeof obj === 'object' && "name" in obj && "key" in obj && "params" in obj;
}