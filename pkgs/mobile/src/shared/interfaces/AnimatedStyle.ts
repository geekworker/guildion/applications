import { Record } from "immutable";
import { Animated, StyleProp, ViewStyle, TransformsStyle } from "react-native";

export type AnimatedStyleAttributes = Animated.AnimatedProps<StyleProp<ViewStyle>>;

export default class AnimatedStyle extends Record<Animated.AnimatedProps<ViewStyle>>({
    backfaceVisibility: undefined,
    backgroundColor: undefined,
    borderBottomColor: undefined,
    borderBottomEndRadius: undefined,
    borderBottomLeftRadius: undefined,
    borderBottomRightRadius: undefined,
    borderBottomStartRadius: undefined,
    borderBottomWidth: undefined,
    borderColor: undefined,
    borderEndColor: undefined,
    borderLeftColor: undefined,
    borderLeftWidth: undefined,
    borderRadius: undefined,
    borderRightColor: undefined,
    borderRightWidth: undefined,
    borderStartColor: undefined,
    borderStyle: undefined,
    borderTopColor: undefined,
    borderTopEndRadius: undefined,
    borderTopLeftRadius: undefined,
    borderTopRightRadius: undefined,
    borderTopStartRadius: undefined,
    borderTopWidth: undefined,
    borderWidth: undefined,
    opacity: undefined,
    elevation: undefined,
    
    transform: undefined,
    transformMatrix: undefined,
    rotation: undefined,
    scaleX: undefined,
    scaleY: undefined,
    translateX: undefined,
    translateY: undefined,

    alignContent: undefined,
    alignItems: undefined,
    alignSelf: undefined,
    aspectRatio: undefined,
    borderEndWidth: undefined,
    borderStartWidth: undefined,
    bottom: undefined,
    display: undefined,
    end: undefined,
    flex: undefined,
    flexBasis: undefined,
    flexDirection: undefined,
    flexGrow: undefined,
    flexShrink: undefined,
    flexWrap: undefined,
    height: undefined,
    justifyContent: undefined,
    left: undefined,
    margin: undefined,
    marginBottom: undefined,
    marginEnd: undefined,
    marginHorizontal: undefined,
    marginLeft: undefined,
    marginRight: undefined,
    marginStart: undefined,
    marginTop: undefined,
    marginVertical: undefined,
    maxHeight: undefined,
    maxWidth: undefined,
    minHeight: undefined,
    minWidth: undefined,
    overflow: undefined,
    padding: undefined,
    paddingBottom: undefined,
    paddingEnd: undefined,
    paddingHorizontal: undefined,
    paddingLeft: undefined,
    paddingRight: undefined,
    paddingStart: undefined,
    paddingTop: undefined,
    paddingVertical: undefined,
    position: undefined,
    right: undefined,
    start: undefined,
    top: undefined,
    width: undefined,
    zIndex: undefined,
    direction: undefined,
    shadowColor: undefined,
    shadowOffset: undefined,
    shadowOpacity: undefined,
    shadowRadius: undefined,
}) {
    public toStyleObject(): Animated.AnimatedProps<ViewStyle> {
        const json = this.toJSON();
        Object.keys(json).forEach((key) => json[key as keyof typeof json] === undefined ? delete json[key as keyof typeof json] : {});
        return json;
    }
}