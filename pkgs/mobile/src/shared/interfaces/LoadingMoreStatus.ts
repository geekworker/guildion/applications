export const LoadingMoreStatus = {
    INCREMENT: 'INCREMENT',
    DECREMENT: 'DECREMENT',
} as const;

export type LoadingMoreStatus = typeof LoadingMoreStatus[keyof typeof LoadingMoreStatus];