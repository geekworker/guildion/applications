import { Change } from "diff";

export const DiffStatus = {
    NEW: 'new',
    REMOVED: 'removed',
    KEEP: 'keep',
    REPLACED: 'replaced',
    OTHER: 'other',
} as const;

export type DiffStatus = typeof DiffStatus[keyof typeof DiffStatus];

export const convertToDiffStatus = (part: Change): DiffStatus => {
    const status: DiffStatus = part.added ? DiffStatus.NEW : part.removed ? DiffStatus.REMOVED : DiffStatus.KEEP;
    return status;
}