import { AutoCompletePrefix } from "@guildion/core";
import { Change } from "diff";
import { DiffChunk } from "./diffChunk";
import { DiffStatus } from "./diffStatus";

export const diffForward = ({
    diff,
    oldChunk,
}: {
    diff: Change[],
    oldChunk?: DiffChunk,
}): { newChunk?: DiffChunk, mentionQuery?: string } => {
    const newChunk = DiffChunk.buildFrom(diff);
    // console.log(oldChunk, newChunk);
    if (!newChunk) return {};
    switch(newChunk.status) {
    case DiffStatus.NEW:
        // MEMO: when new @ char added
        if (
            !oldChunk &&
            newChunk &&
            newChunk.changedValue == AutoCompletePrefix.Member
        ) {
            return { newChunk, mentionQuery: newChunk.changedValue };
        // MEMO: when continued to add new char with @
        } else if (
            !!oldChunk &&
            oldChunk.changedValue.startsWith(AutoCompletePrefix.Member)
        ) {
            const mergedChunk = oldChunk.concat(newChunk);
            if (!mergedChunk) return { newChunk };
            return {
                mentionQuery: mergedChunk.changedValue,
                newChunk: mergedChunk,
            }
        } else {
            return { newChunk };
        }
    case DiffStatus.REMOVED:
        if (
            !!oldChunk &&
            oldChunk.changedValue.startsWith(AutoCompletePrefix.Member)
        ) {
            const mergedChunk = oldChunk.concat(newChunk);
            if (!mergedChunk) return { newChunk };
            return {
                mentionQuery: mergedChunk.changedValue,
                newChunk: mergedChunk,
            }
        }
    case DiffStatus.REPLACED:
        const {
            newDiff,
            removedDiff,
        } = DiffChunk.splitReplacedChunk(diff);
        const removedResult = diffForward({ diff: removedDiff, oldChunk });
        return diffForward({ diff: newDiff, oldChunk: removedResult.newChunk });
    default: return {};
    }
}