import uuid from "react-native-uuid";

export const autoCompleteAddedSpliter: string = `\t${uuid.v4()}\t`.replace('-', '');
export const autoCompleteRemovedSpliter: string = `\t${uuid.v4()}\t`.replace('-', '');