import { Record } from "immutable";
import { convertToDiffStatus, DiffStatus } from "./diffStatus";
import { Change } from "diff";
import { autoCompleteAddedSpliter, autoCompleteRemovedSpliter } from "./constants";

export type DiffChunkAttributes = {
    value: string,
    start: number,
    end: number,
    changedValue: string,
    status: DiffStatus,
};

export class DiffChunk extends Record<DiffChunkAttributes>({
    value: '',
    start: 0,
    end: 0,
    changedValue: '',
    status: DiffStatus.OTHER,
}) {
    static detectDiffStatus(diff: Change[]): DiffStatus {
        if (diff.filter(p => !!p.added).length == 1 && diff.filter(p => !!p.removed).length == 1) {
            return DiffStatus.REPLACED;
        } else if (diff.filter(p => !!p.added).length == 1) {
            return DiffStatus.NEW;
        } else if (diff.filter(p => !!p.removed).length == 1) {
            return DiffStatus.REMOVED;
        } else {
            return DiffStatus.OTHER;
        }
    }

    static splitReplacedChunk(diff: Change[]): { newDiff: Change[], removedDiff: Change[] } {
        if (DiffChunk.detectDiffStatus(diff) != DiffStatus.REPLACED) return { newDiff: [], removedDiff: [] };
        const removedDiff = diff.filter(d => !d.added);
        const newDiff = diff.filter(d => !d.removed);
        return {
            newDiff,
            removedDiff,
        }
    }

    public static buildFrom(diff: Change[]): DiffChunk | undefined {
        const status = DiffChunk.detectDiffStatus(diff);
        switch(status) {
        case DiffStatus.NEW: return DiffChunk.buildFromNew(diff)
        case DiffStatus.REMOVED: return DiffChunk.buildFromRemoved(diff);
        case DiffStatus.REPLACED: return new DiffChunk({ status: DiffStatus.REPLACED });
        default: return undefined;
        }
    }

    private static buildFromNew(diff: Change[]): DiffChunk | undefined {
        const status = DiffChunk.detectDiffStatus(diff);
        if (status != DiffStatus.NEW) return;
        let changedValue: string = '';
        const results = diff.map(p => {
            const status = convertToDiffStatus(p);
            const str = status == DiffStatus.NEW && p.value.length > 0 ?
                `${autoCompleteAddedSpliter}${p.value}${autoCompleteAddedSpliter}` :
                status == DiffStatus.REMOVED ?
                '' :
                p.value;
            if(status == DiffStatus.NEW) changedValue = p.value;
            return str;
        });
        const value = results.join('');
        const tmps = value.split(autoCompleteAddedSpliter);
        if (tmps.length != 3) return undefined;
        return new DiffChunk({ value, start: tmps[0].length, end: tmps[0].length + changedValue.length, changedValue, status })
    }

    private static buildFromRemoved(diff: Change[]): DiffChunk | undefined {
        const status = DiffChunk.detectDiffStatus(diff);
        if (status != DiffStatus.REMOVED) return;
        let changedValue: string = '';
        const results = diff.map(p => {
            const status = convertToDiffStatus(p);
            const str = status == DiffStatus.REMOVED && p.value.length > 0 ?
                `${autoCompleteRemovedSpliter}${p.value}${autoCompleteRemovedSpliter}` :
                status == DiffStatus.NEW ?
                '' :
                p.value;
            if(status == DiffStatus.REMOVED) changedValue = p.value;
            return str;
        });
        const value = results.join('');
        const tmps = value.split(autoCompleteRemovedSpliter);
        if (tmps.length != 3) return undefined;
        return new DiffChunk({ value, start: tmps[0].length, end: tmps[0].length + changedValue.length, changedValue, status })
    };

    public concat(newChunk: DiffChunk): DiffChunk | undefined {
        switch(true) {
        case this.status == DiffStatus.NEW && newChunk.status == DiffStatus.NEW: return this.concatWithEachNews(newChunk);
        case this.status == DiffStatus.NEW && newChunk.status == DiffStatus.REMOVED: return this.concatWithRemoved(newChunk)
        default: return undefined;
        }
    };

    private concatWithEachNews = (newChunk: DiffChunk): DiffChunk | undefined => {
        if (this.status != DiffStatus.NEW || newChunk.status != DiffStatus.NEW) return undefined;
        if (!this.checkContinueToAdd(newChunk)) return newChunk;
        const tmps = newChunk.value.split(autoCompleteAddedSpliter);
        return new DiffChunk({
            changedValue: this.changedValue + newChunk.changedValue,
            value: `${tmps[0]}${autoCompleteAddedSpliter}${this.changedValue}${newChunk.changedValue}${autoCompleteAddedSpliter}${tmps[2]}`,
            start: this.start,
            end: newChunk.end,
            status: DiffStatus.NEW,
        })
    };

    private concatWithRemoved = (newChunk: DiffChunk): DiffChunk | undefined => {
        if (this.status != DiffStatus.NEW || newChunk.status != DiffStatus.REMOVED) return undefined;
        if (!this.checkEraseToRemove(newChunk)) return newChunk;
        const newTmps = this.value.split(autoCompleteAddedSpliter);
        const removedTmps = newChunk.value.split(autoCompleteRemovedSpliter);
        const changedValueSplits = this.changedValue.slice(0, newChunk.start - this.start) + this.changedValue.slice(newChunk.end - this.start, this.changedValue.length)
        return new DiffChunk({
            changedValue: changedValueSplits,
            value: `${newTmps[0]}${autoCompleteAddedSpliter}${changedValueSplits}${autoCompleteAddedSpliter}${removedTmps[2]}`,
            start: this.start,
            end: `${newTmps[0]}${changedValueSplits}`.length,
            status: DiffStatus.NEW,
        })
    };

    public checkContinueToAdd(newChunk: DiffChunk): boolean {
        return (
            this.status == DiffStatus.NEW &&
            newChunk.status == DiffStatus.NEW &&
            this.start === newChunk.start - this.changedValue.length &&
            this.end === newChunk.end - newChunk.changedValue.length &&
            newChunk.value.split(autoCompleteAddedSpliter).length == 3 &&
            this.value.split(autoCompleteAddedSpliter).length == 3 &&
            newChunk.value.split(autoCompleteAddedSpliter)[0].endsWith(this.changedValue)
        );
    };

    public checkEraseToRemove(newChunk: DiffChunk): boolean {
        return (
            this.status == DiffStatus.NEW &&
            newChunk.status == DiffStatus.REMOVED &&
            this.start < newChunk.start &&
            this.end >= newChunk.end &&
            this.changedValue.length > newChunk.changedValue.length &&
            newChunk.value.split(autoCompleteRemovedSpliter).length == 3 &&
            this.value.split(autoCompleteAddedSpliter).length == 3 &&
            this.changedValue.includes(newChunk.changedValue)
        );
    };
}