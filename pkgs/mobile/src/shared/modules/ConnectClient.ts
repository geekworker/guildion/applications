import { Store } from "@/infrastructure/Store";
import { ConnectAction } from "@/presentation/redux/Connect/ConnectReducer";
import { ConnectAPI } from "@guildion/core";

export default class ConnectClient {
    private static $instance: ConnectClient;
    private websocket?: WebSocket;
    private connectResolver?: (success: boolean) => void;
    private deviceId?: string;

    public static instance(): ConnectClient {
        if (!this.$instance) {
            this.$instance = new ConnectClient(ConnectClient.instance);
        }
        return this.$instance;
    }

    constructor(caller: Function) {
        if (caller != ConnectClient.instance && ConnectClient.$instance) throw new Error('Singleton instance already exists');
        if (caller != ConnectClient.instance && !ConnectClient.$instance) throw new Error('Caller argument is incorrect');
    }

    async connect(deviceId: string) {
        ConnectClient.instance().deviceId = deviceId;
        return new Promise((resolve, reject) => {
            ConnectClient.instance().connectResolver = resolve;
            ConnectClient.instance().websocket = new WebSocket(new ConnectAPI.ConnectionCreateRouter().toPath(deviceId));
            ConnectClient.instance().websocket!.onopen = ConnectClient.instance().onopen;
            ConnectClient.instance().websocket!.onclose = ConnectClient.instance().onclose;
            ConnectClient.instance().websocket!.onerror = ConnectClient.instance().onerror;
            ConnectClient.instance().websocket!.onmessage = ConnectClient.instance().onmessage;
        });
    }

    disconnect() {
        if (!ConnectClient.instance().websocket) return;
        if (ConnectClient.instance().connectResolver) ConnectClient.instance().connectResolver!(false);
        ConnectClient.instance().websocket?.close();
        ConnectClient.instance().websocket = undefined;
        ConnectClient.instance().connectResolver = undefined;
    }

    private onmessage(event: WebSocketMessageEvent) {
        console.log("onmessage", event.data);
    }

    private onopen() {
        if (ConnectClient.instance().connectResolver) ConnectClient.instance().connectResolver!(true);
        ConnectClient.instance().connectResolver = undefined;
        Store.dispatch(ConnectAction.connect());
    }

    private onclose(event: WebSocketCloseEvent) {
        // Store.dispatch(AppAction.displayErrors({
        //     errors: [new NetworkError(localizer.dictionary.error.common.networkError)]
        // }));
        Store.dispatch(ConnectAction.disconnect());
        const timer = setInterval(() => {
            ConnectClient.instance().connect(ConnectClient.instance().deviceId!);
            clearInterval(timer);
        }, 3000);
    }

    private onerror(event: WebSocketErrorEvent) {
        ConnectClient.instance().disconnect();
    }
}

export const connectClient: ConnectClient = ConnectClient.instance();