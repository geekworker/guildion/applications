import { Device, DeviceAttributes } from '@guildion/core';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { InitialState } from '@react-navigation/routers';
import { Record } from 'immutable';
import { guardAppTheme, AppThemeString, AppTheme } from '../constants/AppTheme';

export interface GlobalStorageRecordAttributes {
    appTheme: AppThemeString,
    navigationState: InitialState,
    roomSplitSceneNavigationState: InitialState,
    accessToken?: string,
}

export class GlobalStorageRecord extends Record<GlobalStorageRecordAttributes>({
    appTheme: AppThemeString.Dark,
    navigationState: { routes: [] },
    roomSplitSceneNavigationState: { routes: [] },
    accessToken: undefined,
}) {
    public getTheme(): AppTheme | undefined {
        const result = this.get('appTheme');
        return result ? guardAppTheme(result) : undefined;
    }
}

export default class GlobalAsyncStorage {
    public static PERSISTENCE_KEY: string = 'GlobalAsyncStorage';
    private static $instance: GlobalAsyncStorage;
    private $current: GlobalStorageRecord = new GlobalStorageRecord();
    private isSynced: boolean = false;

    public static instance(): GlobalAsyncStorage {
        if (!this.$instance) {
            this.$instance = new GlobalAsyncStorage(GlobalAsyncStorage.instance);
        }
        return this.$instance;
    }
    
    constructor(caller: Function) {
        if (caller != GlobalAsyncStorage.instance && GlobalAsyncStorage.$instance) throw new Error('Singleton instance already exists');
        if (caller != GlobalAsyncStorage.instance && !GlobalAsyncStorage.$instance) throw new Error('Caller argument is incorrect');
    }

    public async save(values: Partial<GlobalStorageRecordAttributes>): Promise<boolean> {
        try{
            const newRecord = new GlobalStorageRecord({
                ...this.$current.toJSON(),
                ...values,
            });
            await AsyncStorage.setItem(GlobalAsyncStorage.PERSISTENCE_KEY, JSON.stringify(newRecord.toJSON()));
            this.$current = newRecord;
            return true
        } catch(error) {
            return false;
        }
    }

    public async read(): Promise<boolean> {
        if (this.isSynced) return true;
        try{
            const result = await AsyncStorage.getItem(GlobalAsyncStorage.PERSISTENCE_KEY);
            if (!!result) {
                this.$current = new GlobalStorageRecord({ ...JSON.parse(result) })
                this.isSynced = true;
                return true   
            } else {
                this.isSynced = false;
                return false
            }
        } catch(error) {
            return false;
        }
    }

    public get current(): GlobalStorageRecord {
        return this.$current;
    }
}

export const globalAsyncStorage: GlobalAsyncStorage = GlobalAsyncStorage.instance();