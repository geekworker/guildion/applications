import { isRecords } from "@guildion/core";
import { isRecord } from "immutable";
import { isRoute } from "../interfaces/Route";
import isEqual from "react-fast-compare";

export type ObjectComparableOperator<T> = {
    [key in keyof T]: (prev: T[key], next: T[key]) => boolean
}

export type ObjectCompareResult = {
    success: boolean,
    name?: string,
    prevValue?: any,
    nextValue?: any,
}

export const compare = <T = { [key: string]: any }>(
    prev: T,
    next: T,
    options?: {
        omits?: (keyof T)[],
        operators?: Partial<ObjectComparableOperator<T>>,
        strict?: boolean,
        debug?: boolean,
    }
): boolean => {
    const failed: ObjectCompareResult[] = Object.keys(prev)
        .map((key: string): ObjectCompareResult => {
            if (options?.omits && options?.omits?.includes(key as keyof T)) return { success: true };
            const operator = options?.operators && options.operators[key as keyof T];
            const prevValue = prev[key as keyof T];
            const nextValue = next[key as keyof T];
            const result = !!operator ?
                operator(prevValue, nextValue) :
                isRecord(prevValue) && isRecord(nextValue) && prevValue != nextValue ?
                isEqual(prevValue.toJSON(), nextValue.toJSON()) :
                isRecords(prevValue) && isRecords(nextValue) && prevValue != nextValue ?
                isEqual(prevValue.toJSON(), nextValue.toJSON()) :
                key == "route" && isRoute(prevValue) && isRoute(nextValue) ?
                prevValue.name == nextValue.name && prevValue.key == nextValue.key && isEqual(prevValue.params, nextValue.params) :
                !!options?.strict ?
                prevValue === nextValue :
                isEqual(prevValue, nextValue);
            return { success: result, name: key, prevValue, nextValue }
        })
        .filter(val => !val.success);
    if (failed.length > 0 && !!options?.debug) failed.map(f => console.log("changed", f.name, ":", f.prevValue, "to", f.nextValue))
    return failed.length == 0;
};