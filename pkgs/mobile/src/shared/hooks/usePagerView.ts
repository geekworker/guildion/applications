import React from "react";
import { Animated } from "react-native";
import PagerView, { PagerViewOnPageScrollEventData, PagerViewOnPageSelectedEventData } from "react-native-pager-view";

export interface UsePagerViewInput {
    selectedIndex: number,
}

export const usePagerView = (input: UsePagerViewInput) => {
    const pagerViewRef = React.useRef<PagerView>(null);
    const setPage = React.useCallback((page: number) => pagerViewRef.current?.setPage(page), []);
    const [selectedIndex, setSelectedIndex] = React.useState(0);
    const onPageScrollOffset = React.useRef(new Animated.Value(0)).current;
    const onPageScrollPosition = React.useRef(new Animated.Value(0)).current;
    const onPageSelectedPosition = React.useRef(new Animated.Value(0)).current;
    // onPageScrollOffset.addListener(({ value }) => console.log("onPageScrollOffset", value))
    // onPageScrollPosition.addListener(({ value }) => console.log("onPageScrollPosition", value))
    // onPageSelectedPosition.addListener(({ value }) => console.log("onPageSelectedPosition", value))
    const onPageScroll = React.useMemo(() => Animated.event<PagerViewOnPageScrollEventData>([{
        nativeEvent: {
            offset: onPageScrollOffset,
            position: onPageScrollPosition,
        },
    }], { useNativeDriver: true }), []);
    const onPageSelected = React.useMemo(() => Animated.event<PagerViewOnPageSelectedEventData>([{
        nativeEvent: { position: onPageSelectedPosition }
    }], {
        listener: ({ nativeEvent: { position } }) => {
            setSelectedIndex(position);
        },
        useNativeDriver: true,
    }), []);
    React.useEffect(() => {
        if (input.selectedIndex != selectedIndex) setPage(input.selectedIndex);
    }, [input.selectedIndex])
    return {
        pagerViewRef,
        setPage,
        selectedIndex,
        onPageScroll,
        onPageSelected,
        onPageScrollOffset,
        onPageScrollPosition,
        onPageSelectedPosition,
    };
};

export type usePagerView = typeof usePagerView;
export type UsePagerViewOutput = ReturnType<usePagerView>;