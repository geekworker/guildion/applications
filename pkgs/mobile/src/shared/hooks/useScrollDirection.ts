import React, { Dispatch, SetStateAction } from 'react';
import { NativeScrollPoint } from 'react-native';
import { ScrollDirection } from '../constants/ScrollDirection';

export const useScrollDirection = (): [ScrollDirection, Dispatch<SetStateAction<NativeScrollPoint>>] => {
    const [offset, setOffset] = React.useState<NativeScrollPoint>({ x: 0, y: 0 });
    const [old, setOld] = React.useState<NativeScrollPoint>({ x: 0, y: 0 });
    const [direction, setDirection] = React.useState<ScrollDirection>(ScrollDirection.None);
    React.useEffect(() => {
        if (old.y > offset.y) {
            setDirection(ScrollDirection.Down);
        } else if (old.y < offset.y) {
            setDirection(ScrollDirection.Up);
        } else if (old.x > offset.x) {
            setDirection(ScrollDirection.Left);
        } else if (old.x < offset.x) {
            setDirection(ScrollDirection.Right);
        }
        setOld(old);
    }, [offset]);
    return [direction, setOffset];
}

export const useWaitScrollDirection = (interval: number): [ScrollDirection, Dispatch<SetStateAction<NativeScrollPoint>>] => {
    const [offset, setOffset] = React.useState<NativeScrollPoint>({ x: 0, y: 0 });
    const [old, setOld] = React.useState<NativeScrollPoint>({ x: 0, y: 0 });
    const [current, setCurrent] = React.useState<NativeScrollPoint>({ x: 0, y: 0 });
    const [direction, setDirection] = React.useState<ScrollDirection>(ScrollDirection.None);
    const [currentDirection, setCurrentDirection] = React.useState<ScrollDirection>(ScrollDirection.None);
    React.useEffect(() => {
        if (old.y > offset.y) {
            if (direction == ScrollDirection.Down) {
                current.y > offset.y - interval && setCurrentDirection(ScrollDirection.Down);
            } else {
                setDirection(ScrollDirection.Down);
                setCurrent(offset);
            }
        } else if (old.y < offset.y) {
            if (direction == ScrollDirection.Up) {
                current.y < offset.y - interval && setCurrentDirection(ScrollDirection.Up);
            } else {
                setDirection(ScrollDirection.Up);
                setCurrent(offset);
            }
        } else if (old.x > offset.x) {
            if (direction == ScrollDirection.Left) {
                current.x > offset.x + interval && setCurrentDirection(ScrollDirection.Left);
            } else {
                setDirection(ScrollDirection.Left);
                setCurrent(offset);
            }
        } else if (old.x < offset.x) {
            if (direction == ScrollDirection.Right) {
                current.x < offset.x - interval && setCurrentDirection(ScrollDirection.Right);
            } else {
                setDirection(ScrollDirection.Right);
                setCurrent(offset);
            }
        }
        setOld(old);
    }, [offset]);
    return [currentDirection, setOffset];
}