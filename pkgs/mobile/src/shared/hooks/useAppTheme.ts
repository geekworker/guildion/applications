import React from "react";
import { AppTheme, fallbackAppTheme } from "../constants/AppTheme";
import RootRedux from "@/infrastructure/Root/redux";

export const useAppTheme = (): AppTheme => {
    const context = React.useContext(RootRedux.Context)
    return context && context.state ? context.state.appTheme : fallbackAppTheme;
};