import React from "react";
import { Animated } from "react-native";

export const useAnimatedValue = (animatedValue: Animated.Value, options?: { throttle?: number }) => {
    const [value, setValue] = React.useState<number>(Number(JSON.stringify(animatedValue)));
    const [cache, setCache] = React.useState<number>(Number(JSON.stringify(animatedValue)));
    animatedValue.addListener((v) => {
        if (!!options?.throttle) {
            if (Math.abs(cache - v.value) > options.throttle) setValue(v.value);
        } else {
            setValue(v.value)
        }
        setCache(v.value);
    });
    return value;
}