import { useWindowDimensions } from "react-native";
import { useSafeAreaInsets } from "react-native-safe-area-context";

export interface InsetsOutput {
    innerWidth: number,
    innerHeight: number,
}

export const useInsets = (options?: { topExclude?: boolean, leftExclude?: boolean, rightExclude?: boolean, bottomExclude?: boolean, height?: number, width?: number }): InsetsOutput => {
    const window = useWindowDimensions();
    const insets = useSafeAreaInsets();
    return {
        innerWidth: (options?.width ?? window.width) - (!!options?.leftExclude ? 0 : insets.left) - (!!options?.rightExclude ? 0 : insets.right),
        innerHeight: (options?.height ?? window.height) - (!!options?.topExclude ? 0 : insets.top) - (!!options?.bottomExclude ? 0 : insets.bottom),
    };
}