import React from "react";
import { Room } from "@guildion/core";
import { useSplitView } from "./useSplitView";
import RootRedux from "@/infrastructure/Root/redux";
import { RootNavigatorRoutes } from "@/presentation/components/navigators/RootNavigator/constants";
import { RoomSplitScreenNavigatorRoutes } from "@/presentation/components/navigators/RoomSplitScreenNavigator/constants";

export const usePushRoom = (): (room: Room) => void => {
    const shouldSplit = useSplitView();
    const context = React.useContext(RootRedux.Context);
    const pushRoom = React.useCallback((room: Room) => {
        if (room.id && room.guildId) {
            shouldSplit ?
                context.state.currentRoomSplitSceneNavigation?.push(RoomSplitScreenNavigatorRoutes.RoomShow, { id: room.id!, guildId: room.guildId }) :
                context.state.currentHomeTabsNavigation?.push(RootNavigatorRoutes.RoomShow, { id: room.id!, guildId: room.guildId })
        }
    }, [context.state.currentHomeTabsNavigation, context.state.currentRoomSplitSceneNavigation, shouldSplit]);
    return pushRoom;
};