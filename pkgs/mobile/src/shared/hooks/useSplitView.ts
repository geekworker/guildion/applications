import { useWindowDimensions } from "react-native"
import React from "react";
import { getShoultSplit } from "../constants/SplitConfig";

export const useSplitView = (): boolean => {
    const window = useWindowDimensions();
    const [shouldSplit, setShouldSplit] = React.useState<boolean>(getShoultSplit(window));
    React.useEffect(() => {
        shouldSplit != getShoultSplit(window) && setShouldSplit(getShoultSplit(window));
    }, [window]);
    return shouldSplit;
};