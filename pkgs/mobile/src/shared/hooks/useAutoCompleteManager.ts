import { diffChars } from "diff";
import React from "react";
import { DiffChunk } from "../modules/AutoComplete/diffChunk";
import { diffForward } from "../modules/AutoComplete/diffForward";

export const useAutoCompleteManager = () => {
    const [oldChunk, setOldChunk] = React.useState<DiffChunk | undefined>(undefined);
    const [mentionQuery, setMentionQuery] = React.useState<string | undefined>(undefined);
    const register = React.useCallback(
        (oldStr: string, newStr: string) => {
            const diff = diffChars(oldStr, newStr);
            const result = diffForward({ oldChunk, diff });
            setMentionQuery(result.mentionQuery);
            setOldChunk(result.newChunk);
        },
        [oldChunk, mentionQuery]
    );
    return {
        register,
        mentionQuery,
    };
}