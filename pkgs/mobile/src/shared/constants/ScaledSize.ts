import { ScaledSize } from "react-native";

export const ScaledSizeZero: ScaledSize = { width: 0, height: 0, scale: 0, fontScale: 0 };