import { LanguageCode, Localizer } from "@guildion/core";

export let localizer: Localizer;

export const setLocalizer = (lang: LanguageCode) => {
    localizer = new Localizer(lang);
    return localizer;
}