import { MediaLibraryWidget, PostWidget, SettingWidget, SyncVisionWidget, TextChatWidget, Widget, Widgets } from "@guildion/core";

export const staticRoomWidgets = new Widgets([
    new Widget({
        ...SyncVisionWidget.toJSON(),
        id: '1',
    }),
    new Widget({
        ...TextChatWidget.toJSON(),
        id: '2',
    }),
    new Widget({
        ...MediaLibraryWidget.toJSON(),
        id: '3',
    }),
    new Widget({
        ...PostWidget.toJSON(),
        id: '4',
    }),
    new Widget({
        ...SettingWidget.toJSON(),
        id: '5',
    }),
]);