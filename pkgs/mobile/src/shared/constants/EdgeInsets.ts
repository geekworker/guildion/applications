import { EdgeInsets } from "react-native-safe-area-context";

export const EdgeInsetsZero: EdgeInsets = { top: 0, left: 0, right: 0, bottom: 0 };