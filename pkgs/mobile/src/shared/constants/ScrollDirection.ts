export const ScrollDirection = {
    None: 'None',
    Up: 'Up',
    Down: 'Down',
    Left: 'Left',
    Right: 'Right',
}

export type ScrollDirection = typeof ScrollDirection[keyof typeof ScrollDirection];