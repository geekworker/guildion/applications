import { ScaledSize } from "react-native";

export const minSplitWindowWidth: number = 800;
export const minSplitWindowHeight: number = 600;
export const leftTabStackWidth: number = 64;
export const leftStackWidth: number = 320;
export const getRightStackWidth = (width: number): number => Math.max(width, minSplitWindowWidth) - leftTabStackWidth - leftStackWidth;
export const getShoultSplit = (window: ScaledSize): boolean => {
    return window.width >= minSplitWindowWidth && window.height >= minSplitWindowHeight;
};