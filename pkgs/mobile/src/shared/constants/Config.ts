import { NODE_ENV, GET_NODE_ENV, guardCase, LanguageCode, CountryCode, TimeZoneID, fallbackLanguageCode, fallbackCountryCode, fallbackTimeZoneID, configure } from "@guildion/core";
import ReactNativeConfig from 'react-native-config';
import RNLocalize from "react-native-localize";
import { setLocalizer } from "./Localizer";

export type Config = {
    NODE_ENV: NODE_ENV,
    LOCAL_IP_ADDRESS: string,
    DEFAULT_LANG: LanguageCode,
    DEFAULT_COUNTRY: CountryCode,
    DEFAULT_TIMEZONE: TimeZoneID,
}

export let CURRENT_CONFIG: Config;

export const UPDATE_CURRENT_CONFIG = () => {
    const locales = RNLocalize.getLocales();
    CURRENT_CONFIG = {
        NODE_ENV: GET_NODE_ENV(ReactNativeConfig.NODE_ENV),
        LOCAL_IP_ADDRESS: ReactNativeConfig.LOCAL_IP_ADDRESS,
        DEFAULT_LANG: guardCase(locales.length > 0 ? locales[0].languageCode : undefined, { fallback: fallbackLanguageCode, cases: LanguageCode }),
        DEFAULT_COUNTRY: guardCase(RNLocalize.getCountry(), { fallback: fallbackCountryCode, cases: CountryCode }),
        DEFAULT_TIMEZONE: guardCase(RNLocalize.getTimeZone(), { fallback: fallbackTimeZoneID, cases: TimeZoneID })
    };
    setLocalizer(CURRENT_CONFIG.DEFAULT_LANG);
};

export const UPDATE_STORYBOOK_CURRENT_CONFIG = () => {
    CURRENT_CONFIG = {
        ...CURRENT_CONFIG,
        NODE_ENV: NODE_ENV.STORYBOOK,
    };
    configure({ 
        CURRENT_NODE_ENV: NODE_ENV.STORYBOOK,
    });
};