import {
    StatusBarStyle,
    TouchableHighlight,
    TouchableOpacity,
} from 'react-native';
import React from 'react';
import { Record } from 'immutable';
import { DefaultTheme } from '@react-navigation/native';
import * as GuildionAppTheme from '@guildion/ui';

export const CommonTheme = {
    ...GuildionAppTheme.CommonTheme,
} as const;

export const DarkAppThemeAttributes = {
    ...GuildionAppTheme.DarkAppThemeAttributes,
    barStyle: 'light-content' as StatusBarStyle,
    touchableComponent: TouchableHighlight as typeof React.Component,
    common: CommonTheme,
}

export type AppThemeAttributes = typeof DarkAppThemeAttributes;

export const LightAppThemeAttributes: AppThemeAttributes = {
    ...GuildionAppTheme.LightAppThemeAttributes,
    barStyle: 'dark-content' as StatusBarStyle,
    touchableComponent: TouchableOpacity,
    common: CommonTheme,
}

export class AppTheme extends Record<AppThemeAttributes>({
    ...DarkAppThemeAttributes,
}) {
}

export const DarkAppTheme = new AppTheme({ ...DarkAppThemeAttributes });
export const LightAppTheme = new AppTheme({ ...LightAppThemeAttributes });

export const AppThemes = {
    Light: LightAppTheme,
    Dark: DarkAppTheme,
} as const;

export const AppThemeString = {
    ...GuildionAppTheme.AppThemeString,
} as const;

export type AppThemeString = typeof AppThemeString[keyof typeof AppThemeString];

export const guardAppTheme = (code: AppThemeString | undefined): AppTheme => {
    switch(code) {
        case AppThemeString.Light: return AppThemes.Light;
        case AppThemeString.Dark: return AppThemes.Dark;
        default: return fallbackAppTheme;
    }
}

export const appThemeToString = (theme: AppTheme): AppThemeString => {
    switch(theme) {
        case AppThemes.Light: return AppThemeString.Light;
        case AppThemes.Dark: return AppThemeString.Dark;
        default: return fallbackAppThemeString;
    }
}

export const fallbackAppTheme: AppTheme = DarkAppTheme;
export const fallbackAppThemeString: AppThemeString = appThemeToString(DarkAppTheme);

export const navigationTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        background: 'transparent',
    },
};