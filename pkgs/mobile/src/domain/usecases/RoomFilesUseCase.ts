import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import RoomFilesRepository from '../repositories/RoomFilesRepository';
import GuildFilesRepository from '../repositories/GuildFilesRepository';
import { SagaIterator } from '@redux-saga/types';
import { Files, GuildFilesRecentListResponse, RoomFilesListResponse } from '@guildion/core';
import { call, select } from 'redux-saga/effects';
import { roomFileAlbumsPageStateSelector, roomFilePlaylistsPageStateSelector, roomFilesPageStateSelector, roomFilesRecentPageStateSelector } from '@/presentation/redux/RoomFile/RoomFileSelector';
import { RoomFilesPageState, RoomRecentFilesPageState } from '@/presentation/redux/RoomFile/RoomFileReducer';

export default class RoomFilesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ roomId }: { roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (roomId.length == 0) return { files: new Files([]), paginatable: false };
        const result: RoomFilesListResponse = yield call(new RoomFilesRepository().gets, roomId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreIndex({ roomId }: { roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: RoomFilesPageState | undefined = yield select(state => roomFilesPageStateSelector(state, roomId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: RoomFilesListResponse = yield call(new RoomFilesRepository().gets, roomId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchRecentFiles({ guildId, roomId }: { guildId: string, roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (roomId.length == 0 || guildId.length == 0) return { files: new Files([]), paginatable: false };
        const result: GuildFilesRecentListResponse = yield call(new GuildFilesRepository().getRecents, guildId, { count: 10, offset: 0, roomId });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreRecentFiles({ guildId, roomId }: { guildId: string, roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: RoomRecentFilesPageState | undefined = yield select(state => roomFilesRecentPageStateSelector(state, roomId, guildId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: GuildFilesRecentListResponse = yield call(new GuildFilesRepository().getRecents, guildId, { count: 10, offset: size, roomId });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchAlbums({ roomId }: { roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (roomId.length == 0) return { files: new Files([]), paginatable: false };
        const result: RoomFilesListResponse = yield call(new RoomFilesRepository().getAlbums, roomId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreAlbums({ roomId }: { roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: RoomFilesPageState | undefined = yield select(state => roomFileAlbumsPageStateSelector(state, roomId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: RoomFilesListResponse = yield call(new RoomFilesRepository().getAlbums, roomId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchPlaylists({ roomId }: { roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (roomId.length == 0) return { files: new Files([]), paginatable: false };
        const result: RoomFilesListResponse = yield call(new RoomFilesRepository().getPlaylists, roomId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMorePlaylists({ roomId }: { roomId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: RoomFilesPageState | undefined = yield select(state => roomFilePlaylistsPageStateSelector(state, roomId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: RoomFilesListResponse = yield call(new RoomFilesRepository().getPlaylists, roomId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }
}