import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import RoomsRepository from '../repositories/RoomsRepository';
import { Room, Rooms, RoomsListResponse, RoomsSearchResponse } from "@guildion/core";
import { SagaIterator } from "@redux-saga/types";
import { call, select } from "@redux-saga/core/effects";
import { roomsSearchPageStateSelector } from "@/presentation/redux/Room/RoomSelector";
import { SearchRoomsPageState } from "@/presentation/redux/Room/RoomReducer";

export default class RoomsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ guildId, isPublic }: { guildId: string, isPublic?: boolean }): SagaIterator<{ rooms: Rooms, paginatable: boolean }> {
        if (guildId.length == 0) return { rooms: new Rooms([]), paginatable: false };
        const result: RoomsListResponse = yield call(new RoomsRepository().gets, guildId, { count: 10, offset: 0, isPublic });
        return {
            rooms: result.getRooms(),
            paginatable: result.paginatable,
        };
    }

    *fetchShow({ id, isPublic }: { id: string, isPublic?: boolean }): SagaIterator<{ room: Room }> {
        yield call(new RoomsUseCase().connect, { id });
        const room: Room = yield call(new RoomsRepository().get, id, { isPublic });
        return {
            room,
        };
    }

    *connect({ id }: { id: string }): SagaIterator<{ result: boolean }> {
        const result: boolean = yield call(new RoomsRepository().connect, id);
        return {
            result,
        };
    }

    *disconnect(): SagaIterator<{ result: boolean }> {
        const result: boolean = yield call(new RoomsRepository().disconnect);
        return {
            result,
        };
    }

    *search({ query }: { query: string }): SagaIterator<{ rooms: Rooms, paginatable: boolean }> {
        if (query.length === 0) return { rooms: new Rooms([]), paginatable: false };
        const result: RoomsSearchResponse = yield call(new RoomsRepository().search, query, { count: 10, offset: 0 });
        return {
            rooms: result.getRooms(),
            paginatable: result.paginatable,
        };
    }

    *searchMore({ query }: { query: string }): SagaIterator<{ rooms: Rooms, paginatable: boolean }> {
        const pageState: SearchRoomsPageState | undefined = yield select(state => roomsSearchPageStateSelector(state, query));
        const size = pageState?.get('rooms').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { rooms: new Rooms([]), paginatable: false };
        const result: RoomsSearchResponse = yield call(new RoomsRepository().search, query, { count: 10, offset: size });
        return {
            rooms: result.getRooms(),
            paginatable: result.paginatable,
        };
    }
}