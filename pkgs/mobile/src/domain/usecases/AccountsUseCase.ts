import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import { Account, AccountAuthenticateResponse, AccountRegisterResponse, AppError, ErrorType, User } from '@guildion/core';
import AccountsRepository from '../repositories/AccountsRepository';
import { globalAsyncStorage } from "@/shared/modules/GlobalAsyncStorage";
import { localizer } from "@/shared/constants/Localizer";
import { AppAction } from "@/presentation/redux/App/AppReducer";
import { SagaIterator } from "@redux-saga/types";
import { call, put } from "@redux-saga/core/effects";
import { List } from "immutable";

export default class AccountsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *check({}: {}): SagaIterator<{ account?: Account }> {
        try {
            const accessToken = globalAsyncStorage.current.accessToken;
            if (!accessToken) return {};
            const result: Account = yield call(new AccountsRepository().show, accessToken);
            return { account: result };
        } catch {
            globalAsyncStorage.save({ accessToken: undefined });
            return {}
        }
    }

    *register({ user }: { user: User }): SagaIterator<{ account: Account, accessToken?: string }> {
        const result: AccountRegisterResponse = yield call(new AccountsRepository().register, user);
        globalAsyncStorage.save({ accessToken: result.accessToken });
        return { account: result.getAccount()!, accessToken: result.accessToken };
    }

    *authenticate({ username }: { username: string }): SagaIterator<{ account?: Account, shouldTwoFactorAuth: boolean, accessToken?: string }> {
        const result: AccountAuthenticateResponse = yield call(new AccountsRepository().authenticate, { username });
        if (result.accessToken) globalAsyncStorage.save({ accessToken: result.accessToken });
        return { account: result.getAccount()!, shouldTwoFactorAuth: result.shouldTwoFactorAuth, accessToken: result.accessToken };
    }

    *verify({ username }: { username: string }): SagaIterator<{ account?: Account, accessToken?: string }> {
        const result: AccountAuthenticateResponse = yield call(new AccountsRepository().authenticate, { username });
        if (!!result.shouldTwoFactorAuth) {
            const error = new AppError(localizer.dictionary.error.device.trustedError, { type: ErrorType.accountUsername });
            yield put(AppAction.displayErrors({ errors: List([error]) }))
            throw error;
        }
        if (result.accessToken) globalAsyncStorage.save({ accessToken: result.accessToken });
        return { account: result.getAccount()!, accessToken: result.accessToken };
    }
}