import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import DiscoveriesRepository from '../repositories/DiscoveriesRepository';
import { SagaIterator } from 'redux-saga';
import { Discoveries, DiscoveriesRecommendResponse } from '@guildion/core';
import { call, select } from 'redux-saga/effects';
import { DiscoveriesRecommendPageState } from '@/presentation/redux/Discovery/DiscoveryReducer';
import { discoveryRecommendPageStateSelector } from '@/presentation/redux/Discovery/DiscoverySelector';

export default class DiscoveriesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchRecommend(): SagaIterator<{ discoveries: Discoveries, paginatable: boolean }> {
        const result: DiscoveriesRecommendResponse = yield call(new DiscoveriesRepository().recommend, { count: 10, offset: 0 });
        return {
            discoveries: result.getDiscoveries(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreRecommend(): SagaIterator<{ discoveries: Discoveries, paginatable: boolean }> {
        const pageState: DiscoveriesRecommendPageState | undefined = yield select(state => discoveryRecommendPageStateSelector(state));
        const size = pageState?.get('discoveries').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { discoveries: new Discoveries([]), paginatable: false };
        const result: DiscoveriesRecommendResponse = yield call(new DiscoveriesRepository().recommend, { count: 10, offset: size });
        return {
            discoveries: result.getDiscoveries(),
            paginatable: result.paginatable,
        };
    }
}