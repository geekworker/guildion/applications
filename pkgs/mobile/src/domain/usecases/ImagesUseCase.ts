import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import ImagesRepository from '../repositories/ImagesRepository';
import { imageCacheSelector } from "@/presentation/redux/Image/ImageSelector";
import { SagaIterator } from "@redux-saga/types";
import { select, call } from "@redux-saga/core/effects";

export default class ImagesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetch({ url }: { url: string }): SagaIterator<{ base64: string }> {
        const cache = yield select(state => imageCacheSelector(state, url))
        if (cache) return { base64: cache }
        const base64: string = yield call(new ImagesRepository().fetchBase64, url);
        return { base64 };
    }
}