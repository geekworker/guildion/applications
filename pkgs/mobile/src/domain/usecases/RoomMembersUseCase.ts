import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import RoomMembersRepository from '../repositories/RoomMembersRepository';
import { Members, RoomConnectingMembersListResponse } from "@guildion/core";
import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";
import RoomsUseCase from "./RoomsUseCase";

export default class RoomMembersUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchConnectingIndex({ roomId }: { roomId: string }): SagaIterator<{ members: Members, paginatable: boolean }> {
        if (roomId.length == 0) return { members: new Members([]), paginatable: false };
        yield call(new RoomsUseCase().connect, { id: roomId });
        const result: RoomConnectingMembersListResponse = yield call(new RoomMembersRepository().getConnectings, roomId, { count: 10, offset: 0 });
        return {
            members: result.getMembers(),
            paginatable: result.paginatable,
        };
    }
}