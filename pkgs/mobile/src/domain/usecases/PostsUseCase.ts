import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import PostsRepository from '../repositories/PostsRepository';
import { Post, Posts, PostsListResponse, PostsSearchResponse } from '@guildion/core';
import { call, select } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { RoomPostsPageState } from '@/presentation/redux/Post/PostReducer';
import { postsIndexPageStateSelector, postsSearchPageStateSelector } from '@/presentation/redux/Post/PostSelector';

export default class PostsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ postId, roomId, isPublic }: { postId?: string, roomId: string, isPublic?: boolean }): SagaIterator<{ posts: Posts, paginatable: boolean }> {
        if (roomId.length === 0 || (postId && postId.length === 0)) return { posts: new Posts([]), paginatable: false };
        const result: PostsListResponse = yield call(new PostsRepository().gets, roomId, { count: 10, offset: 0, postId, isPublic });
        return {
            posts: result.getPosts(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreIndex({ postId, roomId, isPublic }: { postId?: string, roomId: string, isPublic?: boolean }): SagaIterator<{ posts: Posts, paginatable: boolean }> {
        const pageState: RoomPostsPageState | undefined = yield select(state => postsIndexPageStateSelector(state, roomId, postId));
        const size = pageState?.get('posts').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { posts: new Posts([]), paginatable: false };
        const result: PostsListResponse = yield call(new PostsRepository().gets, roomId, { count: 10, offset: size, postId, isPublic });
        return {
            posts: result.getPosts(),
            paginatable: result.paginatable,
        };
    }

    *fetchShow({ id }: { id: string }): SagaIterator<{ post: Post }> {
        const post: Post = yield call(new PostsRepository().get, id);
        return {
            post,
        };
    }

    *search({ query }: { query: string }): SagaIterator<{ posts: Posts, paginatable: boolean }> {
        if (query.length === 0) return { posts: new Posts([]), paginatable: false };
        const result: PostsSearchResponse = yield call(new PostsRepository().search, query, { count: 10, offset: 0 });
        return {
            posts: result.getPosts(),
            paginatable: result.paginatable,
        };
    }

    *searchMore({ query }: { query: string }): SagaIterator<{ posts: Posts, paginatable: boolean }> {
        const pageState: RoomPostsPageState | undefined = yield select(state => postsSearchPageStateSelector(state, query));
        const size = pageState?.get('posts').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { posts: new Posts([]), paginatable: false };
        const result: PostsSearchResponse = yield call(new PostsRepository().search, query, { count: 10, offset: size });
        return {
            posts: result.getPosts(),
            paginatable: result.paginatable,
        };
    }
}