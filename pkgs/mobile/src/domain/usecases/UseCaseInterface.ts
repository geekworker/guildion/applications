import BaseUseCase from "./BaseUseCase";

export default interface UseCaseInterface {
}

export type UseCase = UseCaseInterface & BaseUseCase;