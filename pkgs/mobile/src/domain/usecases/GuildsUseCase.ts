import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import GuildsRepository from '../repositories/GuildsRepository';
import { Guild, Guilds, GuildsListResponse, GuildsSearchResponse } from "@guildion/core";
import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";
import { select } from "redux-saga/effects";
import { guildsSearchPageStateSelector } from "@/presentation/redux/Guild/GuildSelector";
import { SearchGuildsPageState } from "@/presentation/redux/Guild/GuildReducer";

export default class GuildsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({}: {}): SagaIterator<{ guilds: Guilds, paginatable: boolean }> {
        const result: GuildsListResponse = yield call(new GuildsRepository().gets, { count: 10, offset: 0 });
        return {
            guilds: result.getGuilds(),
            paginatable: result.paginatable,
        };
    }

    *fetchShow({ id, isPublic }: { id: string, isPublic?: boolean }): SagaIterator<{ guild: Guild }> {
        yield call(new GuildsUseCase().connect, { id });
        const guild: Guild = yield call(new GuildsRepository().get, id, { isPublic });
        return {
            guild,
        };
    }

    *connect({ id }: { id: string }): SagaIterator<{ result: boolean }> {
        const result: boolean = yield call(new GuildsRepository().connect, id);
        return {
            result,
        };
    }

    *disconnect(): SagaIterator<{ result: boolean }> {
        const result: boolean = yield call(new GuildsRepository().disconnect);
        return {
            result,
        };
    }

    *search({ query }: { query: string }): SagaIterator<{ guilds: Guilds, paginatable: boolean }> {
        if (query.length === 0) return { guilds: new Guilds([]), paginatable: false };
        const result: GuildsSearchResponse = yield call(new GuildsRepository().search, query, { count: 10, offset: 0 });
        return {
            guilds: result.getGuilds(),
            paginatable: result.paginatable,
        };
    }

    *searchMore({ query }: { query: string }): SagaIterator<{ guilds: Guilds, paginatable: boolean }> {
        const pageState: SearchGuildsPageState | undefined = yield select(state => guildsSearchPageStateSelector(state, query));
        const size = pageState?.get('guilds').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { guilds: new Guilds([]), paginatable: false };
        const result: GuildsSearchResponse = yield call(new GuildsRepository().search, query, { count: 10, offset: size });
        return {
            guilds: result.getGuilds(),
            paginatable: result.paginatable,
        };
    }
}