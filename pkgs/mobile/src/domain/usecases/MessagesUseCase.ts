import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import MessagesRepository from '../repositories/MessagesRepository';
import { SagaIterator } from 'redux-saga';
import { Message, Messages, MessagesListResponse } from '@guildion/core';
import { call, select } from 'redux-saga/effects';
import { roomMessagesPageStateSelector } from '@/presentation/redux/Message/MessageSelector';
import { MessagesPageState } from '@/presentation/redux/Message/MessageReducer';
import { isRecord } from 'immutable';

export default class MessagesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ roomId }: { roomId: string }): SagaIterator<{ messages: Messages, paginatable: boolean, basedAt: Date }> {
        const basedAt = new Date();
        if (roomId.length == 0) return { messages: new Messages([]), paginatable: false, basedAt };
        const result: MessagesListResponse = yield call(new MessagesRepository().gets, roomId, { count: 10, offset: 0, ltAt: basedAt });
        return {
            messages: result.getMessages(),
            paginatable: result.paginatable,
            basedAt,
        };
    }

    *incrementIndex({ roomId }: { roomId: string }): SagaIterator<{ messages: Messages, paginatable: boolean }> {
        const pageState: MessagesPageState | undefined = yield select(state => roomMessagesPageStateSelector(state, roomId));
        const last = pageState?.get('messages').last;
        if (!pageState || !last || !pageState.get('incrementable')) return { messages: new Messages([]), paginatable: false };
        const result: MessagesListResponse = yield call(new MessagesRepository().gets, roomId, { count: 10, offset: 0, gtAt: last.getCreatedAt() });
        return {
            messages: result.getMessages(),
            paginatable: result.paginatable,
        };
    }

    *decrementIndex({ roomId }: { roomId: string }): SagaIterator<{ messages: Messages, paginatable: boolean }> {
        const pageState: MessagesPageState | undefined = yield select(state => roomMessagesPageStateSelector(state, roomId));
        const first = pageState?.get('messages').first;
        if (!pageState || !first || !pageState?.get('decrementable')) return { messages: new Messages([]), paginatable: false };
        const result: MessagesListResponse = yield call(new MessagesRepository().gets, roomId, { count: 10, offset: 0, ltAt: first.getCreatedAt() });
        return {
            messages: result.getMessages(),
            paginatable: result.paginatable,
        };
    }

    *create({ message }: { message: Message }): SagaIterator<{ message: Message | undefined }> {
        if (message.body.length == 0 && (!message.files || message.files?.length == 0)) return { message: undefined };
        const result: Message = yield call(new MessagesRepository().create, message);
        return {
            message: result,
        };
    }

    *update({ message }: { message: Message }): SagaIterator<{ message: Message | undefined }> {
        if (message.body.length == 0 && (!message.files || message.files?.length == 0)) return { message: undefined };
        const result: Message = yield call(new MessagesRepository().update, message);
        return {
            message: result,
        };
    }

    *destroy({ id }: { id: string }): SagaIterator<{ success: boolean }> {
        if (id.length == 0) return { success: false };
        const success: boolean = yield call(new MessagesRepository().destroy, id);
        return {
            success,
        };
    }
}