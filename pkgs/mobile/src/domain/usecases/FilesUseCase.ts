import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import FilesRepository from '../repositories/FilesRepository';
import { Files } from "@guildion/core";
import { SagaIterator } from "@redux-saga/types";
import { call, select } from "@redux-saga/core/effects";
import { systemFilesSelector } from "@/presentation/redux/File/FileSelector";

export default class FilesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchSystemFiles({}: {}): SagaIterator<{ files: Files }> {
        const cache: Files | undefined = yield select(systemFilesSelector);
        if (!!cache) return { files: cache };
        const files: Files = yield call(new FilesRepository().getSystemFiles);
        return { files };
    }
}