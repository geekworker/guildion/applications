import UseCaseInterface from './UseCaseInterface';
import BaseUseCase from './BaseUseCase';
import GuildFilesRepository from '../repositories/GuildFilesRepository';
import { SagaIterator } from '@redux-saga/types';
import { Files, GuildFilesRecentListResponse, GuildFilesListResponse } from '@guildion/core';
import { call, select } from 'redux-saga/effects';
import { guildFileAlbumsPageStateSelector, guildFilePlaylistsPageStateSelector, guildFilesPageStateSelector, guildFilesRecentPageStateSelector } from '@/presentation/redux/GuildFile/GuildFileSelector';
import { GuildFilesPageState, GuildRecentFilesPageState } from '@/presentation/redux/GuildFile/GuildFileReducer';

export default class GuildFilesUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ guildId }: { guildId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (guildId.length == 0) return { files: new Files([]), paginatable: false };
        const result: GuildFilesListResponse = yield call(new GuildFilesRepository().gets, guildId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreIndex({ guildId }: { guildId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: GuildFilesPageState | undefined = yield select(state => guildFilesPageStateSelector(state, guildId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: GuildFilesListResponse = yield call(new GuildFilesRepository().gets, guildId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchRecentFiles({ guildId }: { guildId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (guildId.length == 0) return { files: new Files([]), paginatable: false };
        const result: GuildFilesRecentListResponse = yield call(new GuildFilesRepository().getRecents, guildId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreRecentFiles({ guildId }: { guildId: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: GuildRecentFilesPageState | undefined = yield select(state => guildFilesRecentPageStateSelector(state, guildId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: GuildFilesRecentListResponse = yield call(new GuildFilesRepository().getRecents, guildId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchAlbums({ guildId, roomId }: { guildId: string, roomId?: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (guildId.length == 0) return { files: new Files([]), paginatable: false };
        const result: GuildFilesListResponse = roomId ? yield call(new GuildFilesRepository().getRecentAlbums, guildId, { count: 10, offset: 0, roomId }) : yield call(new GuildFilesRepository().getAlbums, guildId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMoreAlbums({ guildId, roomId }: { guildId: string, roomId?: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: GuildFilesPageState | undefined = yield select(state => guildFileAlbumsPageStateSelector(state, guildId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: GuildFilesListResponse = roomId ? yield call(new GuildFilesRepository().getRecentAlbums, guildId, { count: 10, offset: size, roomId }) : yield call(new GuildFilesRepository().getAlbums, guildId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchPlaylists({ guildId, roomId }: { guildId: string, roomId?: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        if (guildId.length == 0) return { files: new Files([]), paginatable: false };
        const result: GuildFilesListResponse = roomId ? yield call(new GuildFilesRepository().getRecentPlaylists, guildId, { count: 10, offset: 0, roomId }) : yield call(new GuildFilesRepository().getPlaylists, guildId, { count: 10, offset: 0 });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }

    *fetchMorePlaylists({ guildId, roomId }: { guildId: string, roomId?: string }): SagaIterator<{ files: Files, paginatable: boolean }> {
        const pageState: GuildFilesPageState | undefined = yield select(state => guildFilePlaylistsPageStateSelector(state, guildId));
        const size = pageState?.get('files').size;
        if (!pageState || !size || size === 0 || !pageState.get('paginatable')) return { files: new Files([]), paginatable: false };
        const result: GuildFilesListResponse = roomId ? yield call(new GuildFilesRepository().getRecentPlaylists, guildId, { count: 10, offset: size, roomId }) : yield call(new GuildFilesRepository().getPlaylists, guildId, { count: 10, offset: size });
        return {
            files: result.getFiles(),
            paginatable: result.paginatable,
        };
    }
}