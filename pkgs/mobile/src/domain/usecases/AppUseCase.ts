import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import AccountsRepository from '../repositories/AccountsRepository';
import DevicesRepository from '../repositories/DevicesRepository';
import { Account, Device, Files } from "@guildion/core";
import { Platform } from "react-native";
import DeviceInfo from 'react-native-device-info';
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { DeviceAction } from "@/presentation/redux/Device/DeviceReducer";
import { globalAsyncStorage } from "@/shared/modules/GlobalAsyncStorage";
import { AccountAction } from "@/presentation/redux/Account/AccountReducer";
import { AppAction } from "@/presentation/redux/App/AppReducer";
import FilesRepository from "../repositories/FilesRepository";
import { FileAction } from "@/presentation/redux/File/FileReducer";
import ConnectClient from "@/shared/modules/ConnectClient";
import { all, call, put } from "@redux-saga/core/effects";
import { SagaIterator } from "@redux-saga/types";
import { NavigationState } from "@react-navigation/routers";

export default class AppUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *launch({}: {}): SagaIterator {
        yield put(AppAction.readInitialNavigationState());
        yield put(AppAction.readInitialRoomSplitScreenNavigationState());
        const device = new Device({
            os: Platform.OS,
            model: DeviceInfo.getModel(),
            udid: DeviceInfo.getUniqueId(),
            appVersion: DeviceInfo.getVersion(),
            countryCode: CURRENT_CONFIG.DEFAULT_COUNTRY,
            languageCode: CURRENT_CONFIG.DEFAULT_LANG,
        });
        const deviceRegisterResponse: { device: Device, csrfSecret: string } = yield call(new DevicesRepository().register, { device: device });
        const [
            _,
            systemFiles,
        ] = yield all([
            put(DeviceAction.setDevice(deviceRegisterResponse)),
            call(new FilesRepository().getSystemFiles),
            call(ConnectClient.instance().connect, deviceRegisterResponse.device.id!),
        ])
        yield put(FileAction.setSystemFiles(systemFiles));
        try {
            const accessToken = globalAsyncStorage.current.accessToken;
            if (!accessToken || accessToken.length == 0) throw new Error('');
            const accountShowResult = yield call(new AccountsRepository().show, accessToken, { ignoreError: true });
            yield all([
                put(AccountAction.setAccount({ account: accountShowResult })),
                put(AccountAction.setAccessToken({ accessToken })),
            ]);
        } catch {
            globalAsyncStorage.save({ accessToken: undefined });
            yield all([
                put(AccountAction.setAccount({})),
            ]);
        }
        return {};
    }

    *saveInitialNavigationState(state: NavigationState): SagaIterator {
        if (globalAsyncStorage.current.accessToken) globalAsyncStorage.save({ navigationState: state });
        return {};
    }

    *saveInitialRoomSplitScreenNavigationState(state: NavigationState): SagaIterator {
        if (globalAsyncStorage.current.accessToken) globalAsyncStorage.save({ roomSplitSceneNavigationState: state });
        return {};
    }
}