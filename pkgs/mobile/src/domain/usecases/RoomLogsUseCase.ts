import UseCaseInterface from "./UseCaseInterface";
import BaseUseCase from "./BaseUseCase";
import RoomLogsRepository from '../repositories/RoomLogsRepository';
import { RoomLogs, RoomLogsListResponse } from "@guildion/core";
import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";

export default class RoomLogsUseCase extends BaseUseCase implements UseCaseInterface {
    constructor() {
        super();
    }

    *fetchIndex({ roomId }: { roomId: string }): SagaIterator<{ logs: RoomLogs, paginatable: boolean }> {
        if (roomId.length == 0) return { logs: new RoomLogs([]), paginatable: false };
        const result: RoomLogsListResponse = yield call(new RoomLogsRepository().gets, roomId, { count: 10, offset: 0 });
        return {
            logs: result.getLogs(),
            paginatable: result.paginatable,
        };
    }
}