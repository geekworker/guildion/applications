import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { Post, PostsEndpoint, PostShowRequest, PostShowResponse, PostsListRequest, PostsListResponse, PostsSearchRequest, PostsSearchResponse } from '@guildion/core';
import { SagaIterator } from 'redux-saga';
import { call, select } from '@redux-saga/core/effects';
import { csrfSecretSelector, currentDeviceSelector } from '@/presentation/redux/Device/DeviceSelector';
import { accessTokenSelector } from '@/presentation/redux/Account/AccountSelector';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

export default class PostsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets(roomId: string, { count, offset, postId, isPublic }: { count?: number, offset?: number, postId?: string, isPublic?: boolean }): SagaIterator<PostsListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: PostsListResponse = yield call(BaseRepository.callAPI, 
            PostsEndpoint.List,
            {
                data: new PostsListRequest({
                    roomId,
                    postId,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    isPublic,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *search(query: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<PostsSearchResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: PostsSearchResponse = yield call(BaseRepository.callAPI, 
            PostsEndpoint.Search,
            {
                data: new PostsSearchRequest({
                    query,
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *get(id: string): SagaIterator<Post> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: PostShowResponse = yield call(BaseRepository.callAPI, 
            PostsEndpoint.Show,
            {
                data: new PostShowRequest({
                    id,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.getPost();
    }
}