import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { call, select } from '@redux-saga/core/effects';
import { FileType, RoomFilesEndpoint, RoomFilesListRequest, RoomFilesListResponse } from '@guildion/core';
import { csrfSecretSelector, currentDeviceSelector } from '@/presentation/redux/Device/DeviceSelector';
import { accessTokenSelector } from '@/presentation/redux/Account/AccountSelector';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

export default class RoomFilesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<RoomFilesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomFilesListResponse = yield call(BaseRepository.callAPI, 
            RoomFilesEndpoint.List,
            {
                data: new RoomFilesListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getAlbums(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<RoomFilesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomFilesListResponse = yield call(BaseRepository.callAPI, 
            RoomFilesEndpoint.List,
            {
                data: new RoomFilesListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    types: [FileType.Image, FileType.Album],
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getPlaylists(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<RoomFilesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomFilesListResponse = yield call(BaseRepository.callAPI, 
            RoomFilesEndpoint.List,
            {
                data: new RoomFilesListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    types: [FileType.Video, FileType.Playlist],
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }
}