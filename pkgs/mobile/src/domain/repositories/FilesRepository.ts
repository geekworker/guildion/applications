import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { FilesEndpoint, FilesListRequest, Files, File, FileShowResponse, FileShowRequest, FileFilingsListRequest, FileFilingsListResponse } from '@guildion/core';
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { SagaIterator } from "@redux-saga/types";
import { call, select } from "@redux-saga/core/effects";
import { csrfSecretSelector, currentDeviceSelector } from "@/presentation/redux/Device/DeviceSelector";
import { accessTokenSelector } from "@/presentation/redux/Account/AccountSelector";

export default class FilesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *getSystemFiles(): SagaIterator<Files> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const result = yield call(BaseRepository.callAPI, 
            FilesEndpoint.List,
            {
                data: new FilesListRequest({
                    isJSONString: true
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                logger: false,
            }
        );
        return result.getFilesFromJSON()!;
    }

    *show(id: string): SagaIterator<File> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: FileShowResponse = yield call(BaseRepository.callAPI, 
            FilesEndpoint.Show,
            {
                data: new FileShowRequest({
                    id,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.getFile();
    }

    *getFilings(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<FileFilingsListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: FileFilingsListResponse = yield call(BaseRepository.callAPI, 
            FilesEndpoint.FilingsList,
            {
                data: new FileFilingsListRequest({
                    id,
                    count,
                    offset,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getRandomFilings(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<FileFilingsListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: FileFilingsListResponse = yield call(BaseRepository.callAPI, 
            FilesEndpoint.FilingsList,
            {
                data: new FileFilingsListRequest({
                    id,
                    count,
                    offset,
                    isRandom: true,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }
}