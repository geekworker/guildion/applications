import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { fetchImageBase64 } from '@guildion/core';
import { SagaIterator } from "@redux-saga/types";
import { call } from "@redux-saga/core/effects";

export default class ImagesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *fetchBase64(url: string): SagaIterator<string> {
        const result = yield call(fetchImageBase64, url);
        return result;
    }
}