import BaseRepository from "./BaseRepository";

export default interface RepositoryInterface {
}

export type Repository = RepositoryInterface & BaseRepository;