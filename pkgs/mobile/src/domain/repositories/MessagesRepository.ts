import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { Message, MessageCreateRequest, MessageCreateResponse, MessageDestroyRequest, MessageDestroyResponse, MessagesEndpoint, MessagesListRequest, MessagesListResponse, MessageUpdateRequest, MessageUpdateResponse } from '@guildion/core';
import { call, select } from 'redux-saga/effects';
import { csrfSecretSelector, currentDeviceSelector } from '@/presentation/redux/Device/DeviceSelector';
import { accessTokenSelector } from '@/presentation/redux/Account/AccountSelector';
import { SagaIterator } from 'redux-saga';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

export default class MessagesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets(roomId: string, { count, offset, gtAt, ltAt }: { count?: number, offset?: number, gtAt?: Date, ltAt?: Date, }): SagaIterator<MessagesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: MessagesListResponse = yield call(BaseRepository.callAPI, 
            MessagesEndpoint.List,
            {
                data: new MessagesListRequest({
                    roomId,
                    count: count ?? 10,
                    offset: offset ?? 0,
                    gtAt: gtAt?.toISOString(),
                    ltAt: ltAt?.toISOString(),
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *create(newMessage: Message): SagaIterator<Message> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: MessageCreateResponse = yield call(BaseRepository.callAPI, 
            MessagesEndpoint.Create,
            {
                data: new MessageCreateRequest({
                    message: newMessage.toJSON(),
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.getMessage()!;
    }

    *update(newMessage: Message): SagaIterator<Message> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: MessageUpdateResponse = yield call(BaseRepository.callAPI, 
            MessagesEndpoint.Update,
            {
                data: new MessageUpdateRequest({
                    message: { ...newMessage.toJSON(), id: newMessage.id! },
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.getMessage()!;
    }

    *destroy(id: string): SagaIterator<boolean> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: MessageDestroyResponse = yield call(BaseRepository.callAPI, 
            MessagesEndpoint.Destroy,
            {
                data: new MessageDestroyRequest({
                    id,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.success;
    }
}