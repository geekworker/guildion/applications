import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { RoomLogsEndpoint, RoomLogsListRequest, RoomLogsListResponse } from "@guildion/core";
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { SagaIterator } from "@redux-saga/types";
import { accessTokenSelector } from "@/presentation/redux/Account/AccountSelector";
import { csrfSecretSelector, currentDeviceSelector } from "@/presentation/redux/Device/DeviceSelector";
import { call, select } from "@redux-saga/core/effects";

export default class RoomLogsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets(roomId: string, { count, offset, gtAt, ltAt }: { count?: number, offset?: number, gtAt?: Date, ltAt?: Date, }): SagaIterator<RoomLogsListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomLogsListResponse = yield call(BaseRepository.callAPI, 
            RoomLogsEndpoint.List,
            {
                data: new RoomLogsListRequest({
                    roomId,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    gtAt: gtAt?.toISOString(),
                    ltAt: ltAt?.toISOString(),
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }
}