import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { DiscoveriesRecommendRequest, DiscoveriesRecommendResponse, DiscoveriesEndpoint } from '@guildion/core';
import { call, select } from '@redux-saga/core/effects';
import { csrfSecretSelector, currentDeviceSelector } from '@/presentation/redux/Device/DeviceSelector';
import { accessTokenSelector } from '@/presentation/redux/Account/AccountSelector';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

export default class DiscoveriesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *recommend({ count, offset }: { count?: number, offset?: number }): SagaIterator<DiscoveriesRecommendResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: DiscoveriesRecommendResponse = yield call(BaseRepository.callAPI, 
            DiscoveriesEndpoint.Recommend,
            {
                data: new DiscoveriesRecommendRequest({
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }
}