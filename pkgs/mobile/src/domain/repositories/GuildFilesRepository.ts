import RepositoryInterface from './RepositoryInterface';
import BaseRepository from './BaseRepository';
import { SagaIterator } from 'redux-saga';
import { call, select } from '@redux-saga/core/effects';
import { FileType, GuildFilesEndpoint, GuildFilesListRequest, GuildFilesListResponse, GuildFilesRecentListRequest, GuildFilesRecentListResponse } from '@guildion/core';
import { csrfSecretSelector, currentDeviceSelector } from '@/presentation/redux/Device/DeviceSelector';
import { accessTokenSelector } from '@/presentation/redux/Account/AccountSelector';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

export default class GuildFilesRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<GuildFilesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildFilesListResponse = yield call(BaseRepository.callAPI, 
            GuildFilesEndpoint.List,
            {
                data: new GuildFilesListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getRecents(id: string, { count, offset, roomId }: { count?: number, offset?: number, roomId?: string }): SagaIterator<GuildFilesRecentListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildFilesRecentListResponse = yield call(BaseRepository.callAPI, 
            GuildFilesEndpoint.RecentList,
            {
                data: new GuildFilesRecentListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    roomId,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getRecentAlbums(id: string, { count, offset, roomId }: { count?: number, offset?: number, roomId?: string }): SagaIterator<GuildFilesRecentListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildFilesRecentListResponse = yield call(BaseRepository.callAPI, 
            GuildFilesEndpoint.RecentList,
            {
                data: new GuildFilesRecentListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    roomId,
                    types: [FileType.Image, FileType.Album],
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getRecentPlaylists(id: string, { count, offset, roomId }: { count?: number, offset?: number, roomId?: string }): SagaIterator<GuildFilesRecentListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildFilesRecentListResponse = yield call(BaseRepository.callAPI, 
            GuildFilesEndpoint.RecentList,
            {
                data: new GuildFilesRecentListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    roomId,
                    types: [FileType.Video, FileType.Playlist],
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getAlbums(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<GuildFilesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildFilesListResponse = yield call(BaseRepository.callAPI, 
            GuildFilesEndpoint.List,
            {
                data: new GuildFilesListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    types: [FileType.Image, FileType.Album],
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *getPlaylists(id: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<GuildFilesListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildFilesListResponse = yield call(BaseRepository.callAPI, 
            GuildFilesEndpoint.List,
            {
                data: new GuildFilesListRequest({
                    id,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    types: [FileType.Video, FileType.Playlist],
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }
}