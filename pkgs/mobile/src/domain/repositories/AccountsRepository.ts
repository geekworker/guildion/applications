import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { Account, AccountShowRequest, AccountsEndpoint, AccountRegisterResponse, AccountRegisterRequest, User, AccountAuthenticateResponse, AccountAuthenticateRequest, AccountShowResponse } from '@guildion/core';
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { SagaIterator } from "@redux-saga/types";
import { call, select } from "@redux-saga/core/effects";
import { csrfSecretSelector, currentDeviceSelector } from "@/presentation/redux/Device/DeviceSelector";

export default class AccountsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *register(user: User): SagaIterator<AccountRegisterResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const result: AccountRegisterResponse = yield call(BaseRepository.callAPI, 
            AccountsEndpoint.Register,
            {
                data: new AccountRegisterRequest({
                    user: user.toJSON(),
                }),
                csrfSecret,
                device,
                lang: CURRENT_CONFIG.DEFAULT_LANG,
            }
        );
        return result;
    }

    *authenticate({ username }: { username: string }): SagaIterator<AccountAuthenticateResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const result: AccountAuthenticateResponse = yield call(BaseRepository.callAPI, 
            AccountsEndpoint.Authenticate,
            {
                data: new AccountAuthenticateRequest({
                    username,
                }),
                csrfSecret,
                device,
                lang: CURRENT_CONFIG.DEFAULT_LANG,
            }
        );
        return result;
    }

    *show(accessToken: string, options?: { ignoreError?: boolean }): SagaIterator<Account> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const result: AccountShowResponse = yield call(BaseRepository.callAPI, 
            AccountsEndpoint.Show,
            {
                data: new AccountShowRequest({}),
                csrfSecret,
                device,
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                accessToken,
            },
            options
        );
        return result.getAccount()!;
    }
}