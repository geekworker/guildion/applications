import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { RoomsListRequest, RoomsListResponse, RoomsEndpoint, Room, RoomShowRequest, RoomConnectRequest, RoomDisconnectRequest, RoomShowResponse, RoomConnectResponse, RoomDisconnectResponse, RoomsSearchResponse, RoomsSearchRequest } from "@guildion/core";
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { SagaIterator } from "@redux-saga/types";
import { accessTokenSelector } from "@/presentation/redux/Account/AccountSelector";
import { csrfSecretSelector, currentDeviceSelector } from "@/presentation/redux/Device/DeviceSelector";
import { call, select } from "@redux-saga/core/effects";

export default class RoomsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets(guildId: string, { count, offset, gtAt, ltAt, isPublic }: { count?: number, offset?: number, gtAt?: Date, ltAt?: Date, isPublic?: boolean }): SagaIterator<RoomsListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomsListResponse = yield call(BaseRepository.callAPI, 
            RoomsEndpoint.List,
            {
                data: new RoomsListRequest({
                    guildId,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    gtAt: gtAt?.toISOString(),
                    ltAt: ltAt?.toISOString(),
                    isPublic,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *search(query: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<RoomsSearchResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomsSearchResponse = yield call(BaseRepository.callAPI, 
            RoomsEndpoint.Search,
            {
                data: new RoomsSearchRequest({
                    query,
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *get(id: string, { isPublic }: { isPublic?: boolean }): SagaIterator<Room> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomShowResponse = yield call(BaseRepository.callAPI, 
            RoomsEndpoint.Show,
            {
                data: new RoomShowRequest({
                    id,
                    isPublic,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.getRoom();
    }

    *connect(id: string): SagaIterator<boolean> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomConnectResponse = yield call(BaseRepository.callAPI, 
            RoomsEndpoint.Connect,
            {
                data: new RoomConnectRequest({
                    id,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.success;
    }

    *disconnect(): SagaIterator<boolean> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomDisconnectResponse = yield call(BaseRepository.callAPI, 
            RoomsEndpoint.Disconnect,
            {
                data: new RoomDisconnectRequest({}),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.success;
    }
}