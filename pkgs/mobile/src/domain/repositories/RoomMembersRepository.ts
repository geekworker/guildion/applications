import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { RoomsEndpoint, RoomConnectingMembersListRequest, RoomConnectingMembersListResponse } from "@guildion/core";
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { SagaIterator } from "@redux-saga/types";
import { accessTokenSelector } from "@/presentation/redux/Account/AccountSelector";
import { csrfSecretSelector, currentDeviceSelector } from "@/presentation/redux/Device/DeviceSelector";
import { call, select } from "@redux-saga/core/effects";

export default class RoomMembersRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *getConnectings(roomId: string, {
        count, offset, gtAt, ltAt,
    }: {
        count?: number, offset?: number, gtAt?: Date, ltAt?: Date,
    }): SagaIterator<RoomConnectingMembersListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: RoomConnectingMembersListResponse = yield call(BaseRepository.callAPI, 
            RoomsEndpoint.ConnectingMembersList,
            {
                data: new RoomConnectingMembersListRequest({
                    roomId,
                    count: count ?? 20,
                    offset: offset ?? 0,
                    gtAt: gtAt?.toISOString(),
                    ltAt: ltAt?.toISOString(),
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }
}