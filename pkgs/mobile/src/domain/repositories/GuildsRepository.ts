import RepositoryInterface from "./RepositoryInterface";
import BaseRepository from "./BaseRepository";
import { GuildsListResponse, GuildsEndpoint, GuildsListRequest, Guild, GuildShowRequest, GuildConnectRequest, GuildDisconnectRequest, GuildShowResponse, GuildConnectResponse, GuildDisconnectResponse, GuildsSearchResponse, GuildsSearchRequest } from "@guildion/core";
import { CURRENT_CONFIG } from "@/shared/constants/Config";
import { SagaIterator } from "@redux-saga/types";
import { accessTokenSelector } from "@/presentation/redux/Account/AccountSelector";
import { csrfSecretSelector, currentDeviceSelector } from "@/presentation/redux/Device/DeviceSelector";
import { call, select } from "@redux-saga/core/effects";

export default class GuildsRepository extends BaseRepository implements RepositoryInterface {
    constructor() {
        super();
    }

    *gets({ count, offset }: { count?: number, offset?: number }): SagaIterator<GuildsListResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildsListResponse = yield call(BaseRepository.callAPI, 
            GuildsEndpoint.List,
            {
                data: new GuildsListRequest({
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *get(id: string, { isPublic }: { isPublic?: boolean }): SagaIterator<Guild> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildShowResponse = yield call(BaseRepository.callAPI, 
            GuildsEndpoint.Show,
            {
                data: new GuildShowRequest({
                    id,
                    isPublic,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.getGuild();
    }

    *search(query: string, { count, offset }: { count?: number, offset?: number }): SagaIterator<GuildsSearchResponse> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildsSearchResponse = yield call(BaseRepository.callAPI, 
            GuildsEndpoint.Search,
            {
                data: new GuildsSearchRequest({
                    query,
                    count: count ?? 20,
                    offset: offset ?? 0,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result;
    }

    *connect(id: string): SagaIterator<boolean> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildConnectResponse = yield call(BaseRepository.callAPI, 
            GuildsEndpoint.Connect,
            {
                data: new GuildConnectRequest({
                    id,
                }),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.success;
    }

    *disconnect(): SagaIterator<boolean> {
        const csrfSecret = yield select(csrfSecretSelector);
        const device = yield select(currentDeviceSelector);
        const accessToken = yield select(accessTokenSelector);
        const result: GuildDisconnectResponse = yield call(BaseRepository.callAPI, 
            GuildsEndpoint.Disconnect,
            {
                data: new GuildDisconnectRequest({}),
                lang: CURRENT_CONFIG.DEFAULT_LANG,
                csrfSecret,
                device,
                accessToken,
            }
        );
        return result.success;
    }
}