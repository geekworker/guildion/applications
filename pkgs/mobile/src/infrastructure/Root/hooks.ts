import React from "react";
import { AppState, useWindowDimensions } from "react-native";
import RootRedux from "./redux";
import RootStyles from './styles';
import { globalAsyncStorage } from "@/shared/modules/GlobalAsyncStorage";
import DeviceInfo from 'react-native-device-info';
import Orientation from 'react-native-orientation';
import { showMessage } from "react-native-flash-message";
import { alertDuration } from "@/shared/constants/AlertConfig";
import { useSplitView } from "@/shared/hooks/useSplitView";
import { HomeTabsNavigatorRoutes } from "@/presentation/components/navigators/HomeTabsNavigator/constants";
import type RootPresenter from "./presenter";
import { localizer } from "@/shared/constants/Localizer";

namespace RootHooks {
    export const useDidLoad = (props: RootPresenter.MergedProps & RootPresenter.Payload) => {
        React.useEffect(() => {
            (async () => {
                await globalAsyncStorage.read();
                props.launch();
            })();
        }, []);
    }

    export const useLoaded = (props: RootPresenter.MergedProps & RootPresenter.Payload): boolean => {
        const isLoaded = props.isLaunched;
        const isLoaded$ = React.useMemo(() => isLoaded, [isLoaded]);
        return isLoaded$
    }

    export const useReducer = () => {
        const [state, dispatch] = React.useReducer(
            RootRedux.reducer,
            RootRedux.initialState,
        );
        return {
            state,
            dispatch
        }
    }

    export const useContext = () => {
        const context = React.useContext(RootRedux.Context);
        return context;
    }

    export const useStyles = (): RootStyles => {
        const context = React.useContext(RootRedux.Context);
        const window = useWindowDimensions();
        const appTheme = context.state.get('appTheme');
        const styles = React.useMemo(() => new RootStyles({ window, appTheme }), [window, appTheme]);
        return styles;
    }

    export const useOrientation = () => {
        if (DeviceInfo.isTablet()) {
            Orientation.unlockAllOrientations();
        } else {
            Orientation.lockToPortrait();
        }
    }

    export const useVisible = () => {
        const appState = React.useRef(AppState.currentState);
        const context = React.useContext(RootRedux.Context);
        React.useEffect(() => {
            const subscription: any = AppState.addEventListener("change", nextAppState => {
                if (
                    appState.current.match(/inactive|background/) &&
                    nextAppState === "active"
                ) {
                    // console.log("App has come to the foreground");
                    context.dispatch(RootRedux.Action.enterForground);
                } else if (
                    appState.current === "active" &&
                    nextAppState.match(/inactive|background/)
                ) {
                    // console.log("App has been to the background");
                    context.dispatch(RootRedux.Action.enterBackground)
                }
                appState.current = nextAppState;
            });
            return () => {
                subscription && subscription.remove();
            };
        }, []);
    }
    
    export const useErrorMessage = (props: RootPresenter.MergedProps & RootPresenter.Payload) => {
        const [errorMessage, setErrorMessage] = React.useState(props.errorMessage);
        React.useEffect(() => {
            if (props.errorMessage && props.errorMessage != errorMessage && props.errorMessage.length > 0) {
                setErrorMessage(props.errorMessage)
                props.resetErrors();
                showMessage({
                    message: localizer.dictionary.error.title,
                    description: props.errorMessage,
                    icon: { icon: "auto", position: "left" },
                    type: 'danger',
                    floating: true,
                    autoHide: true,
                    duration: alertDuration,
                });
                const timer = setInterval(() => {
                    clearInterval(timer);
                    setErrorMessage(undefined);
                }, alertDuration);
            }
            return () => {
                props.errorMessage = undefined;
            }
        }, [props.errorMessage])
    }

    export const useRoomSplitScene = (): boolean => {
        const shouldSplit = useSplitView();
        const context = React.useContext(RootRedux.Context);
        const currentHomeRoute = context.state.currentHomeRoute;
        return React.useMemo(() => currentHomeRoute === HomeTabsNavigatorRoutes.GuildsNavigator && shouldSplit, [currentHomeRoute, shouldSplit]);
    }
}

export default RootHooks