import React from 'react';
import RootRedux from './redux';
import RootPresenter from './presenter';

interface Props {
    children?: React.ReactNode,
}

const RootProvider: React.FC<Props> = ({ children }) => {
    const payload = RootPresenter.usePayload({});
    return (
        <RootRedux.Context.Provider value={{ state: payload.state, dispatch: payload.dispatch }} >
            {children}
        </RootRedux.Context.Provider>
    );
};

export default RootProvider;
