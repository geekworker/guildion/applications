import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { AppTheme, appThemeToString, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { globalAsyncStorage } from "@/shared/modules/GlobalAsyncStorage";
import { Action as ReduxAction } from "redux";
import HomeTabsNavigatorPresenter from "@/presentation/components/navigators/HomeTabsNavigator/presenter";
import { HomeTabsNavigatorRoutes } from "@/presentation/components/navigators/HomeTabsNavigator/constants";
import RoomShowEmptyPresenter from "@/presentation/components/pages/RoomShowEmpty/presenter";
import RoomShowPresenter from "@/presentation/components/pages/RoomShow/presenter";

namespace RootRedux {
    export class State extends Record<{
        appTheme: AppTheme,
        isVisible: boolean,
        currentHomeTabsNavigation?: HomeTabsNavigatorPresenter.Input["navigation"],
        currentHomeRoute?: HomeTabsNavigatorRoutes,
        currentRoomSplitSceneNavigation?: RoomShowEmptyPresenter.Input["navigation"] | RoomShowPresenter.SplitScreenProps["navigation"],
    }>({
        appTheme: fallbackAppTheme,
        isVisible: true,
        currentHomeTabsNavigation: undefined,
        currentHomeRoute: undefined,
        currentRoomSplitSceneNavigation: undefined,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        setAppTheme: actionCreator<AppTheme>('SET_APP_THEME'),
        enterForground: actionCreator('ENTER_FORGROUND'),
        enterBackground: actionCreator('ENTER_BACKGROUND'),
        setCurrentHomeTabsNavigator: actionCreator<HomeTabsNavigatorPresenter.Input["navigation"] | undefined>('SET_CURRENT_HOME_TABS_NAVIGATOR'),
        setCurrentHomeRoute: actionCreator<HomeTabsNavigatorRoutes | undefined>('SET_CURRENT_HOME_ROUTE'),
        setCurrentRoomSplitSceneNavigation: actionCreator<RoomShowEmptyPresenter.Input["navigation"] | RoomShowPresenter.SplitScreenProps["navigation"] | undefined>('SET_CURRENT_ROOM_SPLIT_SCENE_NAVIGATOR'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.setAppTheme, (state, payload) => {
            state = state.set('appTheme', payload);
            globalAsyncStorage.save({
                appTheme: appThemeToString(payload),
            });
            return state;
        })
        .case(Action.enterForground, (state) => {
            state = state.set('isVisible', true);
            return state;
        })
        .case(Action.enterBackground, (state) => {
            state = state.set('isVisible', false);
            return state;
        })
        .case(Action.setCurrentHomeTabsNavigator, (state, payload) => {
            if (payload?.isFocused()) {
                state = state.set('currentHomeTabsNavigation', payload);
            } else {
                state = state.set('currentHomeTabsNavigation', undefined);
                state = state.set('currentHomeRoute', undefined);
            }
            return state;
        })
        .case(Action.setCurrentHomeRoute, (state, payload) => {
            state = state.set('currentHomeRoute', payload);
            return state;
        })
        .case(Action.setCurrentRoomSplitSceneNavigation, (state, payload) => {
            if (payload?.isFocused()) {
                state = state.set('currentRoomSplitSceneNavigation', payload);
            } else {
                state = state.set('currentRoomSplitSceneNavigation', undefined);
            }
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: RootRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });

    export type Context = {
        state: RootRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    };
}

export default RootRedux