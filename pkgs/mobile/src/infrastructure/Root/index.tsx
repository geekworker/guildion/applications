import React from 'react';
import RootRedux from './redux';
import RootPresenter from './presenter';
import RootContainer from './container';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<RootPresenter.MergedProps> = (props) => {
    const payload = RootPresenter.usePayload(props);
    return (
        <RootRedux.Context.Provider value={{ state: payload.state, dispatch: payload.dispatch }} >
            <RootContainer { ...payload } { ...props } />
        </RootRedux.Context.Provider>
    );
};

const Root = connect<RootPresenter.StateProps, RootPresenter.DispatchProps, RootPresenter.Input, RootPresenter.MergedProps, RootState>(
    RootPresenter.mapStateToProps,
    RootPresenter.mapDispatchToProps,
    RootPresenter.mergeProps,
    RootPresenter.connectOptions,
)(Container);

export default Root;
