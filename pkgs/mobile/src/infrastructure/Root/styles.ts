import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { leftTabStackWidth } from "@/shared/constants/SplitConfig";
import { Style } from "@/shared/interfaces/Style";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet, ScaledSize } from 'react-native';

const styles = ({ window, appTheme }: { window: ScaledSize, appTheme: AppTheme }) => StyleSheet.create({
    wrapper: {
        width: window.width,
        height: window.height,
        position: 'relative',
    },
    background: {
        width: window.width,
        height: window.height,
        position: 'relative',
    },
    foreground: {
        position: 'absolute',
        left: 0,
        right: 0,
        width: window.width,
        height: window.height,
    },
    dummy: {
        backgroundColor: appTheme.background.root,
        width: window.width,
        height: window.height,
    },
    splitTabContainer: {
        paddingLeft: leftTabStackWidth,
    },
    splitTab: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        width: leftTabStackWidth,
        height: window.height,
        backgroundColor: appTheme.background.subm1,
        borderRightColor: appTheme.tab.shadow,
        borderRightWidth: 1,
    },
    screen: {
        width: window.width,
        height: window.height,
    },
});

type Styles = typeof styles;

export default class RootStyles extends Record<ReturnType<Styles>>({
    ...styles({ appTheme: fallbackAppTheme, window: { width: 0, height: 0, fontScale: 0, scale: 0 } })
}) implements StylesImpl<Styles> {
    constructor({ window, appTheme }: { window: ScaledSize, appTheme: AppTheme }) {
        super(styles({ window, appTheme }));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};