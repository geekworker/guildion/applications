import React from 'react';
import {
    StatusBar,
    Platform,
    View,
    StatusBarStyle,
} from 'react-native';
import RootBackground from '@/presentation/components/molecules/RootBackground';
import RootNavigationContainer from '@/presentation/components/navigators/RootNavigationContainer';
import RootPresenter from './presenter';
import RoomSplitScreenNavigationContainer from '@/presentation/components/navigators/RoomSplitScreenNavigationContainer';
import IconSideTabBar from '@/presentation/components/organisms/IconSideTabBar';

const AnimatedSplash = require("react-native-animated-splash-screen").default;

const RootComponent: React.FC<RootPresenter.Output> = ({ appTheme, isLoaded, styles, showRoomSplitScene, shouldSplit, currentAccount }) => {
    return (
        <AnimatedSplash
            translucent={true}
            logoWidht={150}
            logoHeight={150}
            isLoaded={isLoaded}
            backgroundColor={appTheme.common.splashBackground}
            logoImage={require("@/assets/images/brands/logo.png")}
        >
            <RootBackground foregrondStyle={!!isLoaded && shouldSplit && !!currentAccount ? styles.getStyle('splitTabContainer') : undefined} >
                {Platform.OS === 'ios' && (
                    <StatusBar barStyle={appTheme.barStyle as StatusBarStyle} />
                )}
                {!!isLoaded && shouldSplit && !!currentAccount && <IconSideTabBar style={styles.getStyle('splitTab')} />}
                {!!isLoaded && <RootNavigationContainer style={styles.getStyle('screen')} appTheme={appTheme} />}
                {!!isLoaded && showRoomSplitScene && <RoomSplitScreenNavigationContainer style={styles.getStyle('screen')} appTheme={appTheme}/>}
            </RootBackground>
        </AnimatedSplash>
    );
};

export default React.memo(RootComponent, RootPresenter.outputAreEqual);