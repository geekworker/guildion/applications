import React from 'react';
import RootComponent from './component';
import RootPresenter from './presenter';

const RootContainer = (props: RootPresenter.MergedProps & RootPresenter.Payload) => {
    const output = RootPresenter.usePresenter(props);
    return <RootComponent {...output} />;
};

export default RootContainer;