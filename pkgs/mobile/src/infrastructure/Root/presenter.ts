import { AppTheme } from "@/shared/constants/AppTheme";
import React from "react";
import { AnyAction } from "redux";
import RootHooks from "./hooks";
import RootRedux from "./redux";
import { bindActionCreators } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { Action } from "typescript-fsa";
import { AppAction } from "@/presentation/redux/App/AppReducer";
import { Account } from "@guildion/core";
import { useSplitView } from "@/shared/hooks/useSplitView";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { globalErrorSelector } from "@/presentation/redux/App/AppSelector";
import RootStyles from "./styles";

namespace RootPresenter {
    export type StateProps = {
        isLaunched: boolean,
        errorMessage?: string,
        currentAccount?: Account,
    }
    
    export type DispatchProps = {
        launch: () => Action<{}>,
        resetErrors: () => Action<void>,
    }
    
    export type Input = {
    }
    
    export type Output = {
        styles: RootStyles,
        appTheme: AppTheme,
        isLoaded: boolean,
        showRoomSplitScene: boolean,
        shouldSplit: boolean,
        currentAccount?: Account,
    }

    export type MergedProps = StateProps & DispatchProps & Input;
    
    export type Payload = {
        state: RootRedux.State,
        dispatch: React.Dispatch<AnyAction>,
    }
    
    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            isLaunched: state.app.isLaunched,
            errorMessage: globalErrorSelector(state),
            currentAccount: state.account.currentAccount,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            launch: () => AppAction.launch.started({}),
            resetErrors: () => AppAction.resetGlobalErrors(),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.app.isLaunched == next.app.isLaunched &&
            prev.account.currentAccount == next.account.currentAccount
        ) || (
            !prev.app.isLaunched
        ),
        areOwnPropsEqual: () => true,
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePayload(props: Input): Payload {
        const { state, dispatch } = RootHooks.useReducer();
        return {
            state,
            dispatch
        }
    }
    
    export function usePresenter(props: MergedProps & Payload): Output {
        const styles = RootHooks.useStyles();
        const context = RootHooks.useContext();
        RootHooks.useDidLoad(props);
        RootHooks.useOrientation();
        RootHooks.useVisible();
        RootHooks.useErrorMessage(props);
        const isLoaded = RootHooks.useLoaded(props);
        const showRoomSplitScene = RootHooks.useRoomSplitScene();
        const shouldSplit = useSplitView();
        return {
            ...props,
            ...context.state.toJSON(),
            styles,
            isLoaded,
            showRoomSplitScene,
            shouldSplit,
        }
    }
}

export default RootPresenter;