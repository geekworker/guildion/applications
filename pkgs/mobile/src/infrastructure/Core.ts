import { CURRENT_CONFIG, UPDATE_CURRENT_CONFIG } from '@/shared/constants/Config';
import { configure } from '@guildion/core';
import { Platform } from 'react-native';

UPDATE_CURRENT_CONFIG();

configure({ 
    CURRENT_NODE_ENV: CURRENT_CONFIG.NODE_ENV,
    IS_MOBILE: true,
    LOCAL_IP_ADDRESS: '153.124.180.147', // Platform.OS === 'android' ? '10.0.2.2' : 'localhost',
});