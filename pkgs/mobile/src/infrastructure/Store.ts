import { RootState } from '@/presentation/redux/RootReducer';
import Redux from 'redux';

export let Store: Redux.Store<RootState>;
export const setStore = (store: Redux.Store<RootState>) => {
    Store = store;
}