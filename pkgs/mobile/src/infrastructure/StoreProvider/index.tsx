import { Store } from '../Store';
import { Provider } from 'react-redux';
import React, { ReactNode } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import FlashMessage from "react-native-flash-message";
import { alertDuration } from '@/shared/constants/AlertConfig';
import ModalsProvider from '@/presentation/components/molecules/Modals/provider';
import Modals from '@/presentation/components/molecules/Modals';
import TopMostsProvider from '@/presentation/components/molecules/TopMosts/provider';

interface Props {
    children?: ReactNode,
}

const StoreProvider = ({ children }: Props) => {
    return (
        <Provider store={Store}>
            <SafeAreaProvider>
                <ModalsProvider>
                    <TopMostsProvider>
                        <FlashMessage duration={alertDuration} />
                        {children}
                        <Modals/>
                    </TopMostsProvider>
                </ModalsProvider>
            </SafeAreaProvider>
        </Provider>
    );
}

export default StoreProvider;