import React from 'react'
import Root from '../Root';
import StoreProvider from '../StoreProvider';

interface Props {
}

const App: React.FC<Props> = ({}) => {
    return (
        <StoreProvider>
            <Root/>
        </StoreProvider>
    );
}

export default App;