import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        input: {
            width: style?.width,
            maxHeight: style?.maxHeight,
            minHeight: style?.minHeight,
            fontSize: 14,
            fontFamily: MontserratFont.Regular,
        },
        placeholder: {
            color: appTheme.element.subp1,
            fontSize: 14,
            fontFamily: MontserratFont.Regular,
        },
        text: {
            width: style?.width,
            maxHeight: style?.maxHeight,
            minHeight: style?.minHeight,
        },
    })
};

type Styles = typeof styles;

export default class MessageTextInputStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};