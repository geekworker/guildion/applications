import React from 'react';
import MessageTextInputComponent from './component';
import MessageTextInputPresenter from './presenter';

const MessageTextInput = (props: MessageTextInputPresenter.Input) => {
    const output = MessageTextInputPresenter.usePresenter(props);
    return <MessageTextInputComponent {...output} />;
};

export default React.memo(MessageTextInput, MessageTextInputPresenter.inputAreEqual);