import React from 'react';
import { TextInput, View } from 'react-native';
import RoomParsedText from '../../atoms/RoomParsedText';
import MessageTextInputPresenter from './presenter';

const MessageTextInputComponent = ({ value, onChangeValue, styles }: MessageTextInputPresenter.Output) => {
    return (
        <View style={styles.container} >
            <TextInput
                onChangeText={onChangeValue}
                style={{
                    ...styles.input,
                }}
                placeholder={'Aa'}
                placeholderTextColor={styles.placeholder.color}
                multiline
            >
                <RoomParsedText
                    text={value}
                    style={styles.getStyle('text')}
                />
            </TextInput>
        </View>
    );
};

export default React.memo(MessageTextInputComponent, MessageTextInputPresenter.outputAreEqual);