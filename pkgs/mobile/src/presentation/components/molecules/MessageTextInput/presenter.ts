import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MessageTextInputHooks from "./hooks";
import MessageTextInputStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { Member, Message, Room } from "@guildion/core";

namespace MessageTextInputPresenter {
    export type Input = {
        message: Message,
        onInputMemberMention?: (newValue?: string) => Promise<Member | undefined>,
        onInputRoomMention?: (newValue?: string) => Promise<Room | undefined>,
        onChangeMessage?: (message: Message) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MessageTextInputStyles,
        appTheme: AppTheme,
        message: Message,
        value?: string,
        onChangeValue: (newValue?: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MessageTextInputHooks.useStyles({ ...props });
        const {
            value,
            onChangeValue,
        } = MessageTextInputHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            value,
            onChangeValue,
        }
    }
}

export default MessageTextInputPresenter;