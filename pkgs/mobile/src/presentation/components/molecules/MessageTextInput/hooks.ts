import React from "react";
import MessageTextInputStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type MessageTextInputPresenter from "./presenter";

namespace MessageTextInputHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new MessageTextInputStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useState = (props: MessageTextInputPresenter.Input) => {
        const [value, setValue] = React.useState<string | undefined>(props.message.body);
        React.useEffect(() => {
            value != props.message.body && setValue(props.message.body);
        }, [props.message.body]);
        const onChangeValue = React.useCallback((newValue?: string) => {
            setValue(newValue);
            props.onChangeMessage && props.onChangeMessage(props.message.set('body', newValue ?? ''))
        }, [props.message, props.onChangeMessage]);
        return {
            value,
            setValue,
            onChangeValue,
        };
    }
}

export default MessageTextInputHooks;