import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MessageTextInput from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Message } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('MessageTextInput', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MessageTextInput
            style={new Style({
                width: number('width', 375),
                maxHeight: number('maxHeight', 120),
                minHeight: number('minHeight', 44),
            })}
            message={new Message({ body: 'test' })}
        />
    ))
    .add('Edit', () => (
        <MessageTextInput
            style={new Style({
                width: number('width', 375),
                maxHeight: number('maxHeight', 120),
                minHeight: number('minHeight', 44),
            })}
            message={
                new Message({
                    body: `
@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`
                })
            }
        />
    ))