import React from 'react';
import SyncVisionEmptyBackgroundComponent from './component';
import SyncVisionEmptyBackgroundPresenter from './presenter';

const SyncVisionEmptyBackground = (props: SyncVisionEmptyBackgroundPresenter.Input) => {
    const output = SyncVisionEmptyBackgroundPresenter.usePresenter(props);
    return <SyncVisionEmptyBackgroundComponent {...output} />;
};

export default React.memo(SyncVisionEmptyBackground, SyncVisionEmptyBackgroundPresenter.inputAreEqual);