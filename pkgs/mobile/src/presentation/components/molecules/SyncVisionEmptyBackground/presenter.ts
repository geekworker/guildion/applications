import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import SyncVisionEmptyBackgroundHooks from "./hooks";
import SyncVisionEmptyBackgroundStyles from './styles';
import React from 'react';
import { Animated } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";

namespace SyncVisionEmptyBackgroundPresenter {
    export type Input = {
        children?: React.ReactNode,
        isAppeared?: boolean,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SyncVisionEmptyBackgroundStyles,
        appTheme: AppTheme,
        animated: boolean,
        opacity: Animated.Value,
        isAppeared: boolean,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }

    export function usePresenter(props: Input): Output {
        const styles = SyncVisionEmptyBackgroundHooks.useStyles({ ...props });
        const { animated, opacity } = SyncVisionEmptyBackgroundHooks.useAnimation(props.loading !== undefined ? !props.loading : props.isAppeared);
        return {
            ...props,
            isAppeared: !!props.isAppeared,
            styles,
            animated,
            opacity,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default SyncVisionEmptyBackgroundPresenter;