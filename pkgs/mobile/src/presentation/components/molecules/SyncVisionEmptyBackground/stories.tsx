import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SyncVisionEmptyBackground from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { boolean, number } from '@storybook/addon-knobs';
import { Style } from '@/shared/interfaces/Style';

storiesOf('SyncVisionEmptyBackground', module)
    .addDecorator((getStory) => <StoreProvider><CenterView style={{ backgroundColor: 'black' }} >{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <SyncVisionEmptyBackground
            style={new Style({
                width: number('width', 385),
                height: number('height', 240),
            })}
            isAppeared={boolean('isAppeared', true)}
        />
    ))
    .add('Loading', () => (
        <SyncVisionEmptyBackground
            style={new Style({
                width: number('width', 385),
                height: number('height', 240),
            })}
            isAppeared={boolean('isAppeared', true)}
            loading={true}
        />
    ))
