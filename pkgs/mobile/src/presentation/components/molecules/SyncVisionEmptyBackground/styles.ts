import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const ANIMATED_OFFSET: number = 0

const styles = ({
    style,
    appTheme,
    isAppeared,
}: StyleProps & { isAppeared?: boolean }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            backgroundColor: style?.backgroundColor ?? appTheme.common.transparent,
            position: style?.position ?? 'relative',
            overflow: 'hidden',
        },
        wrapperSkeleton: {
            width: style?.width,
            height: style?.height,
            borderRadius: 0,
        },
        balloon1: {
            width: 80,
            height: 80,
            borderRadius: 40,
            top: !!isAppeared ? 10 + ANIMATED_OFFSET : 10,
            left: !!isAppeared ? 40 + ANIMATED_OFFSET : 40,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon2: {
            width: 60,
            height: 60,
            borderRadius: 30,
            top: !!isAppeared ? -10 + ANIMATED_OFFSET : -10,
            right: !!isAppeared ? 80 + ANIMATED_OFFSET : 80,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon3: {
            width: 160,
            height: 160,
            borderRadius: 100,
            bottom: !!isAppeared ? -80 + ANIMATED_OFFSET : -80,
            left: !!isAppeared ? -90 + ANIMATED_OFFSET : -90,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon4: {
            width: 120,
            height: 120,
            borderRadius: 60,
            bottom: !!isAppeared ? -30 + ANIMATED_OFFSET : -30,
            right: !!isAppeared ? -20 + ANIMATED_OFFSET : -20,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon5: {
            width: 100,
            height: 100,
            borderRadius: 50,
            bottom: !!isAppeared ? 220 + ANIMATED_OFFSET : 220,
            right: !!isAppeared ? 150 + ANIMATED_OFFSET : 150,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon6: {
            width: 40,
            height: 40,
            borderRadius: 20,
            top: !!isAppeared ? 220 + ANIMATED_OFFSET : 220,
            left: !!isAppeared ? 150 + ANIMATED_OFFSET : 150,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon7: {
            width: 60,
            height: 60,
            borderRadius: 30,
            bottom: !!isAppeared ? 20 + ANIMATED_OFFSET : 20,
            right: !!isAppeared ? 280 + ANIMATED_OFFSET : 280,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
        balloon8: {
            width: 70,
            height: 70,
            borderRadius: 35,
            bottom: !!isAppeared ? 80 + ANIMATED_OFFSET : 80,
            left: !!isAppeared ? 300 + ANIMATED_OFFSET : 300,
            backgroundColor: appTheme.room.balloonColor,
            borderColor: appTheme.room.balloonColor,
        },
    });
}

type Styles = typeof styles;

export default class SyncVisionEmptyBackgroundStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};