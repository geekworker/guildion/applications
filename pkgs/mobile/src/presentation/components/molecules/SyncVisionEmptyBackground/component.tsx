import React from 'react';
import { Animated } from 'react-native';
import WelcomeBalloon from '@/presentation/components/atoms/WelcomeBalloon';
import SyncVisionEmptyBackgroundPresenter from './presenter';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const SyncVisionEmptyBackgroundComponent: React.FC<SyncVisionEmptyBackgroundPresenter.Output> = ({ appTheme, loading, styles, opacity }) => {
    return loading ? (
        <SkeletonContent
            isLoading={loading}
            boneColor={appTheme.skeleton.boneColor}
            highlightColor={appTheme.skeleton.highlightColor}
            animationType="pulse"
            layout={[styles.wrapperSkeleton]}
        />
    ) : (
        <Animated.View style={{ ...styles.wrapper, opacity }} >
            <WelcomeBalloon
                style={styles.getStyle('balloon1')}
                type={'fill'}
                animated={false/*!animated || !!isAppeared*/}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon2')}
                type={'fill'}
                animated={false/*!animated || !!isAppeared*/}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon3')}
                type={'fill'}
                animated={false/*!animated || !!isAppeared*/}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon4')}
                type={'fill'}
                animated={false/*!animated || !!isAppeared*/}
            />
            {(styles.wrapper.width ?? 0) > 500 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon5')}
                    type={'fill'}
                    animated={false/*!animated || !!isAppeared*/}
                />
            )}
            {(styles.wrapper.width ?? 0) > 500 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon6')}
                    type={'fill'}
                    animated={false/*!animated || !!isAppeared*/}
                />
            )}
            {(styles.wrapper.width ?? 0) > 500 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon7')}
                    type={'fill'}
                    animated={false/*!animated || !!isAppeared*/}
                />
            )}
            {(styles.wrapper.width ?? 0) > 700 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon8')}
                    type={'fill'}
                    animated={false/*!animated || !!isAppeared*/}
                />
            )}
        </Animated.View>
    );
};

export default React.memo(SyncVisionEmptyBackgroundComponent, SyncVisionEmptyBackgroundPresenter.outputAreEqual);