import React from "react";
import SyncVisionEmptyBackgroundStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Animated } from "react-native";

namespace SyncVisionEmptyBackgroundHooks {
    export const useStyles = (props: StyleProps & { isAppeared?: boolean }) => {
        const styles = React.useMemo(() => new SyncVisionEmptyBackgroundStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useAnimation = (isAppeared?: boolean): {
        animated: boolean,
        opacity: Animated.Value,
    } => {
        const opacity = React.useRef(new Animated.Value(0)).current;
        const [animated, setAnimated] = React.useState(false);
        React.useEffect(() => {
            Animated.timing(opacity, {
                toValue: !!isAppeared ? 1 : 0,
                duration: 500,
                useNativeDriver: true,
            }).start(() => {
                if (!!isAppeared) {
                    const timer = setInterval(() => {
                        if (!!isAppeared) {
                            setAnimated(true)
                            clearInterval(timer);
                        }
                    }, 500)
                }
            });
            return () => {
                isAppeared = false;
            }
        }, [isAppeared]);
        return {
            animated,
            opacity,
        }
    }
}

export default SyncVisionEmptyBackgroundHooks