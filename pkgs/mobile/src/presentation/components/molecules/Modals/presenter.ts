import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import ModalsHooks from './hooks';
import ModalsStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import ModalsRedux from './redux';
import { AnyAction } from 'redux';
import React from 'react';

namespace ModalsPresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: ModalsStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        onDismissRoomControlPanel: () => void,
    } & Payload;

    export type Payload = {
        state: ModalsRedux.State,
        dispatch: React.Dispatch<AnyAction>,
    }

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };

    export function usePayload(): Payload {
        const { state, dispatch } = ModalsHooks.useReducer();
        return {
            state,
            dispatch
        }
    };
    
    export function usePresenter(props: Input): Output {
        const styles = ModalsHooks.useStyles({ ...props });
        const context = React.useContext(ModalsRedux.Context);
        const {
            onDismissRoomControlPanel,
        } = ModalsHooks.useState(props);
        return {
            ...props,
            ...context,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onDismissRoomControlPanel,
        }
    }
}

export default ModalsPresenter;