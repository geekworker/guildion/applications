import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Action as ReduxAction } from "redux";
import type RoomControlPanelTemplatePresenter from "../../templates/RoomControlPanelTemplate/presenter";

namespace ModalsRedux {
    export class State extends Record<{
        propsRoomControlPanel?: RoomControlPanelTemplatePresenter.Input,
    }>({
        propsRoomControlPanel: undefined,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        showRoomControlPanel: actionCreator<RoomControlPanelTemplatePresenter.Input>('SHOW_ROOM_CONTROL_PANEL'),
        hideRoomControlPanel: actionCreator('HIDE_ROOM_CONTROL_PANEL'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.showRoomControlPanel, (state, payload) => {
            state = state.set('propsRoomControlPanel', payload);
            return state;
        })
        .case(Action.hideRoomControlPanel, (state) => {
            state = state.set('propsRoomControlPanel', undefined);
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: ModalsRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });

    export type Context = {
        state: ModalsRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    };
}

export default ModalsRedux