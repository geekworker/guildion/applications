import React from 'react';
import ModalsComponent from './component';
import ModalsPresenter from './presenter';

const Modals = (props: ModalsPresenter.Input) => {
    const output = ModalsPresenter.usePresenter(props);
    return <ModalsComponent {...output} />;
};

export default React.memo(Modals, ModalsPresenter.inputAreEqual);