import React from 'react';
import ModalsPresenter from './presenter';
import Modal from 'react-native-modal';
import RoomControlPanelTemplate from '../../templates/RoomControlPanelTemplate';
import { TouchableWithoutFeedback } from 'react-native';

const ModalsComponent = ({ state, onDismissRoomControlPanel }: ModalsPresenter.Output) => {
    return (
        <>
            <TouchableWithoutFeedback onPress={onDismissRoomControlPanel}>
                <Modal
                    isVisible={!!state.get('propsRoomControlPanel')}
                    animationIn="slideInRight"
                    animationOut="slideOutRight"
                    onBackdropPress={onDismissRoomControlPanel}
                    onDismiss={onDismissRoomControlPanel}
                    hideModalContentWhileAnimating
                    swipeDirection="left"
                >
                    <RoomControlPanelTemplate
                        {...state.get('propsRoomControlPanel')}
                        onDismiss={onDismissRoomControlPanel}
                    />
                </Modal>
            </TouchableWithoutFeedback>
        </>
    );
};

export default React.memo(ModalsComponent, ModalsPresenter.outputAreEqual);