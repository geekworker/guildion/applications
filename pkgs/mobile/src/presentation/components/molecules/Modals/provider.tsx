import React from 'react';
import ModalsRedux from './redux';
import ModalsPresenter from './presenter';

interface Props {
    children?: React.ReactNode,
}

const ModalsProvider: React.FC<Props> = ({ children }) => {
    const payload = ModalsPresenter.usePayload();
    return (
        <ModalsRedux.Context.Provider value={{ state: payload.state, dispatch: payload.dispatch }} >
            {children}
        </ModalsRedux.Context.Provider>
    );
};

export default ModalsProvider;
