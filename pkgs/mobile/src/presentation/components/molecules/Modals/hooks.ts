import React from 'react';
import ModalsStyles from './styles';
import type ModalsPresenter from './presenter';
import ModalsRedux from './redux';

namespace ModalsHooks {
    export const useStyles = (props: ModalsPresenter.Input) => {
        const styles = React.useMemo(() => new ModalsStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useReducer = () => {
        const [state, dispatch] = React.useReducer(
            ModalsRedux.reducer,
            ModalsRedux.initialState,
        );
        return {
            state,
            dispatch
        }
    }

    export const useState = (props: ModalsPresenter.Input) => {
        const context = React.useContext(ModalsRedux.Context);
        const onDismissRoomControlPanel = React.useCallback(() => {
            context.dispatch(ModalsRedux.Action.hideRoomControlPanel());
        }, []);
        return {
            onDismissRoomControlPanel,
        };
    }
}

export default ModalsHooks;