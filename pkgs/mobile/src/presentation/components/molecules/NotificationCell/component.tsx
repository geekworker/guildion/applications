import React from 'react';
import { Animated, View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import NotificationCellPresenter from './presenter';

const NotificationCellComponent = ({ styles, appTheme, animatedStyle }: NotificationCellPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableHighlight style={styles.touchable}>
                <View style={styles.inner}>
                </View>
            </TouchableHighlight>
        </Animated.View>
    );
};

export default React.memo(NotificationCellComponent, NotificationCellPresenter.outputAreEqual);