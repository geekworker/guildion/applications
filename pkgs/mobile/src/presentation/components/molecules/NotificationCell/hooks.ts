import React from 'react';
import NotificationCellStyles from './styles';
import type NotificationCellPresenter from './presenter';

namespace NotificationCellHooks {
    export const useStyles = (props: NotificationCellPresenter.Input) => {
        const styles = React.useMemo(() => new NotificationCellStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: NotificationCellPresenter.Input) => {
        return {};
    }
}

export default NotificationCellHooks;