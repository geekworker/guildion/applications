import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const paddingTop = style?.getAsNumber('paddingTop') ?? 20;
    const paddingLeft = style?.getAsNumber('paddingLeft') ?? 20;
    const paddingRight = style?.getAsNumber('paddingRight') ?? 20;
    const paddingBottom = style?.getAsNumber('paddingBottom') ?? 20;
    const borderRadius = style?.getAsNumber('borderRadius') ?? 10;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            borderRadius,
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            shadowColor: style?.shadowColor ?? appTheme.shadow.defaultShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.shadow.defaultShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            elevation: style?.elevation ?? 4,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        touchable: {
            width,
            height,
            paddingTop,
            paddingLeft,
            paddingRight,
            paddingBottom,
        },
        inner: {
            width,
            height,
            paddingTop,
            paddingLeft,
            paddingRight,
            paddingBottom,
        },
    });
};

type Styles = typeof styles;

export default class NotificationCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};