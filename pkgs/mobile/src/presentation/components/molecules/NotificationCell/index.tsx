import React from 'react';
import NotificationCellComponent from './component';
import NotificationCellPresenter from './presenter';

const NotificationCell = (props: NotificationCellPresenter.Input) => {
    const output = NotificationCellPresenter.usePresenter(props);
    return <NotificationCellComponent {...output} />;
};

export default React.memo(NotificationCell, NotificationCellPresenter.inputAreEqual);