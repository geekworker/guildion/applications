import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { Animated, View, Image, Text } from 'react-native';
import WelcomeBalloon from '../../atoms/WelcomeBalloon';
import ExploreHeaderPresenter from './presenter';

const ExploreHeaderComponent = ({ styles, appTheme, animatedStyle,  }: ExploreHeaderPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <WelcomeBalloon
                    style={styles.getStyle('balloon1')}
                    type={'border'}
                    animated={false}
                />
                <WelcomeBalloon
                    style={styles.getStyle('balloon2')}
                    type={'border'}
                    animated={false}
                />
                <Image
                    style={styles.logo}
                    source={require('@/assets/images/brands/logo.png')}
                />
                <Text style={styles.title}>
                    {localizer.dictionary.explore.title}
                </Text>
                <Text style={styles.description}>
                    {localizer.dictionary.explore.description}
                </Text>
            </View>
        </Animated.View>
    );
};

export default React.memo(ExploreHeaderComponent, ExploreHeaderPresenter.outputAreEqual);