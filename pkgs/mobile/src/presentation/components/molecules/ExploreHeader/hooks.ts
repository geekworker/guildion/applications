import React from 'react';
import ExploreHeaderStyles from './styles';
import type ExploreHeaderPresenter from './presenter';

namespace ExploreHeaderHooks {
    export const useStyles = (props: ExploreHeaderPresenter.Input) => {
        const styles = React.useMemo(() => new ExploreHeaderStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: ExploreHeaderPresenter.Input) => {
        return {};
    }
}

export default ExploreHeaderHooks;