import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
        },
        inner: {
            width,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        balloon1: {
            position: 'absolute',
            top: 12,
            left: -80,
            width: 160,
            height: 160,
            borderRadius: 80,
        },
        balloon2: {
            position: 'absolute',
            top: -40,
            right: -40,
            width: 120,
            height: 120,
            borderRadius: 60,
        },
        logo: {
            width: 90,
            height: 90,
            marginTop: 100,
        },
        title: {
            fontFamily: MontserratFont.Bold,
            fontSize: 24,
            marginTop: 32,
            lineHeight: 32,
            color: appTheme.element.default,
            textAlign: 'center',
        },
        description: {
            fontFamily: MontserratFont.Bold,
            fontSize: 16,
            lineHeight: 24,
            marginTop: 12,
            color: appTheme.element.default,
            textAlign: 'center',
        },
    });
};

type Styles = typeof styles;

export default class ExploreHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};