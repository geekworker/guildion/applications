import React from 'react';
import ExploreHeaderComponent from './component';
import ExploreHeaderPresenter from './presenter';

const ExploreHeader = (props: ExploreHeaderPresenter.Input) => {
    const output = ExploreHeaderPresenter.usePresenter(props);
    return <ExploreHeaderComponent {...output} />;
};

export default React.memo(ExploreHeader, ExploreHeaderPresenter.inputAreEqual);