import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SocialLinkCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { SocialLink } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('SocialLinkCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SocialLinkCell
            style={new Style({
                width: number('width', 24),
                height: number('height', 24),
            })}
            socialLink={new SocialLink({
                url: 'https://www.guildion.co',
            })}
        />
    ))
    .add('YouTube', () => (
        <SocialLinkCell
            style={new Style({
                width: number('width', 24),
                height: number('height', 24),
            })}
            socialLink={new SocialLink({
                url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
            })}
        />
    ))
    .add('Twitter', () => (
        <SocialLinkCell
            style={new Style({
                width: number('width', 24),
                height: number('height', 24),
            })}
            socialLink={new SocialLink({
                url: 'https://twitter.com/__I_OXO_I__',
            })}
        />
    ))