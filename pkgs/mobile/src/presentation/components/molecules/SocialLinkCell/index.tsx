import React from 'react';
import SocialLinkCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { SocialLink } from '@guildion/core';
import TouchableCircleRipple from '../../atoms/TouchableCircleRipple';
import SocialLinkConditionalIcon from '../../Icons/SocialLinkConditionalIcon';

type Props = {
    socialLink: SocialLink,
    onPress?: (socialLink: SocialLink) => void,
    disabled?: boolean,
} & Partial<StyleProps>;

const SocialLinkCell: React.FC<Props> = ({
    style,
    appTheme,
    socialLink,
    onPress: _onPress,
    disabled,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new SocialLinkCellStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    const onPress = React.useCallback(() => {
        if (_onPress) _onPress(socialLink);
    }, [_onPress, socialLink]);
    return (
        <TouchableCircleRipple onPress={onPress} disabled={disabled} style={styles.getStyle('container')}>
            <SocialLinkConditionalIcon
                socialLink={socialLink}
                style={styles.getStyle('icon')}
            />
        </TouchableCircleRipple>
    );
};

export default React.memo(SocialLinkCell, compare);