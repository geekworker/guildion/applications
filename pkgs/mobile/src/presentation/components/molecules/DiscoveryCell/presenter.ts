import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import DiscoveryCellHooks from './hooks';
import DiscoveryCellStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Discovery, DiscoveryStatus } from '@guildion/core';

namespace DiscoveryCellPresenter {
    export type Input = {
        discovery: Discovery,
        onPress?: (discovery: Discovery) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: DiscoveryCellStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        discovery: Discovery,
        onPress?: () => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = DiscoveryCellHooks.useStyles({ ...props });
        const {
            onPress,
        } = DiscoveryCellHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress,
            loading: props.discovery.status == DiscoveryStatus.LOADING || props.loading,
        }
    }
}

export default DiscoveryCellPresenter;