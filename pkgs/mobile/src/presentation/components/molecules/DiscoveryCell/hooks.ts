import React from 'react';
import DiscoveryCellStyles from './styles';
import type DiscoveryCellPresenter from './presenter';

namespace DiscoveryCellHooks {
    export const useStyles = (props: DiscoveryCellPresenter.Input) => {
        const styles = React.useMemo(() => new DiscoveryCellStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: DiscoveryCellPresenter.Input) => {
        const onPress = React.useCallback(() => {
            if (props.onPress) props.onPress(props.discovery);
        }, [props.onPress, props.discovery]);
        return {
            onPress
        };
    }
}

export default DiscoveryCellHooks;