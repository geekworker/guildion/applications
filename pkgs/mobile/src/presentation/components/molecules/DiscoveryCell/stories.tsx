import { storiesOf } from '@storybook/react-native';
import React from 'react';
import DiscoveryCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Discovery, Files } from '@guildion/core';

storiesOf('DiscoveryCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <DiscoveryCell
            style={new Style({
                width: 360,
                height: 200,
            })}
            discovery={new Discovery({
                enTitle: 'test',
                enDescription: 'description'
            }, {
                enThumbnail: Files.getSeed().generateGuildProfile(),
            })}
        />
    ))
    .add('Loading', () => (
        <DiscoveryCell
            style={new Style({
                width: 360,
                height: 200,
            })}
            discovery={new Discovery({
                enTitle: 'test',
                enDescription: 'description'
            }, {
                enThumbnail: Files.getSeed().generateGuildProfile(),
            })}
            loading
        />
    ))