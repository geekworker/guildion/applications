import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const footerHeight: number = 64;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const borderRadius = style?.getAsNumber('borderRadius') || 5;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            borderRadius,
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            shadowColor: style?.shadowColor ?? appTheme.explore.cellShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.explore.cellShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            elevation: style?.elevation ?? 4,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        touchable: {
            width,
            height,
            borderRadius,
        },
        inner: {
            width,
            height,
            borderRadius,
            position: 'relative',
            overflow: 'hidden',
        },
        header: {
            width,
            height: height - footerHeight,
            position: 'relative',
        },
        thumbnail: {
            width,
            height: height - footerHeight,
        },
        footer: {
            width,
            height: footerHeight,
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 20,
            paddingRight: 20,
        },
        title: {
            textAlign: 'left',
            fontFamily: MontserratFont.Bold,
            fontSize: 18,
            color: appTheme.element.default,
            marginBottom: 4,
        },
        description: {
            textAlign: 'left',
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            color: appTheme.element.default,
        },
    });
};

type Styles = typeof styles;

export default class DiscoveryCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};