import { CURRENT_CONFIG } from '@/shared/constants/Config';
import React from 'react';
import { Animated, Text, TouchableHighlight, View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import DiscoveryCellPresenter from './presenter';

const DiscoveryCellComponent = ({ styles, appTheme, animatedStyle, onPress, discovery, loading }: DiscoveryCellPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableHighlight style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}>
                    <View style={styles.header}>
                        <AsyncImage
                            style={styles.getStyle('thumbnail')}
                            url={discovery.getThumbnail(CURRENT_CONFIG.DEFAULT_LANG)?.url ?? ''}
                            loading={loading}
                        />
                    </View>
                    <View style={styles.footer}>
                        {!loading && (
                            <Text style={styles.title} numberOfLines={1}>
                                {discovery.getTitle(CURRENT_CONFIG.DEFAULT_LANG)}
                            </Text>
                        )}
                        {!loading && (
                            <Text style={styles.description} numberOfLines={1}>
                                {discovery.getDescription(CURRENT_CONFIG.DEFAULT_LANG)}
                            </Text>
                        )}
                    </View>
                </View>
            </TouchableHighlight>
        </Animated.View>
    );
};

export default React.memo(DiscoveryCellComponent, DiscoveryCellPresenter.outputAreEqual);