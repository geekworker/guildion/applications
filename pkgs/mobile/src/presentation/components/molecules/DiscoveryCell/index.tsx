import React from 'react';
import DiscoveryCellComponent from './component';
import DiscoveryCellPresenter from './presenter';

const DiscoveryCell = (props: DiscoveryCellPresenter.Input) => {
    const output = DiscoveryCellPresenter.usePresenter(props);
    return <DiscoveryCellComponent {...output} />;
};

export default React.memo(DiscoveryCell, DiscoveryCellPresenter.inputAreEqual);