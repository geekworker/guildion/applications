import React from 'react';
import BottomCardComponent from './component';
import BottomCardPresenter from './presenter';

const BottomCard = (props: BottomCardPresenter.Input) => {
    const output = BottomCardPresenter.usePresenter(props);
    return <BottomCardComponent {...output} />;
};

export default React.memo(BottomCard, BottomCardPresenter.inputAreEqual);