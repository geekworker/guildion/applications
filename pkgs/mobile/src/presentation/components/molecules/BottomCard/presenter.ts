import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import BottomCardHooks from './hooks';
import BottomCardStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

namespace BottomCardPresenter {
    export type Input = {
        headerText?: string,
        title?: string,
        footerText?: string,
        disabled?: boolean,
        hideButton?: boolean,
        buttonTitle?: string,
        onPress?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: BottomCardStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        headerText?: string,
        title?: string,
        footerText?: string,
        disabled?: boolean,
        hideButton?: boolean,
        buttonTitle?: string,
        onPress?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = BottomCardHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default BottomCardPresenter;