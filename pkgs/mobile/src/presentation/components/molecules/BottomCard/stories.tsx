import { storiesOf } from '@storybook/react-native';
import React from 'react';
import BottomCard from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

storiesOf('BottomCard', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView style={{ backgroundColor: fallbackAppTheme.background.root }}>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <BottomCard
            style={new Style({
                width: 380,
            })}
            title='test'
            buttonTitle='next'
        />
    ))
    .add('HeaderText', () => (
        <BottomCard
            style={new Style({
                width: 380,
            })}
            headerText='id: test'
            title='test'
            buttonTitle='next'
        />
    ))
    .add('FooterText', () => (
        <BottomCard
            style={new Style({
                width: 380,
            })}
            footerText='welcome'
            title='test'
            buttonTitle='next'
        />
    ))
    .add('HideButton', () => (
        <BottomCard
            style={new Style({
                width: 380,
            })}
            footerText='welcome'
            title='test'
            hideButton
        />
    ))