import React from 'react';
import BottomCardStyles from './styles';
import type BottomCardPresenter from './presenter';

namespace BottomCardHooks {
    export const useStyles = (props: BottomCardPresenter.Input) => {
        const styles = React.useMemo(() => new BottomCardStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: BottomCardPresenter.Input) => {
        return {};
    }
}

export default BottomCardHooks;