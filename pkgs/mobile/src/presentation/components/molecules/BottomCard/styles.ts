import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
            borderTopWidth: style?.borderTopWidth ?? 1,
            borderTopColor: style?.borderTopColor ?? appTheme.background.subp2,
        },
        inner: {
            width,
            padding: 20,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        headerText: {
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            textAlign: 'center',
            fontSize: 14,
            lineHeight: 24,
            marginBottom: 8,
        },
        title: {
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'center',
            fontSize: 24,
            marginBottom: 8,
        },
        footerText: {
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            textAlign: 'center',
            fontSize: 14,
            lineHeight: 24,
            marginBottom: 8,
        },
        button: {
            width: 140,
            height: 44,
            marginTop: 12,
        },
    });
};

type Styles = typeof styles;

export default class BottomCardStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};