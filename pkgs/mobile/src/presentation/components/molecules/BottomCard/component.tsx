import React from 'react';
import { Animated, Text, View } from 'react-native';
import Button, { ButtonStyleType } from '../../atoms/Button';
import BottomCardPresenter from './presenter';

const BottomCardComponent = ({ styles, appTheme, animatedStyle, headerText, title, footerText, disabled, buttonTitle, onPress, hideButton }: BottomCardPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                {!!headerText && (
                    <Text style={styles.headerText}>
                        {headerText}
                    </Text>
                )}
                {!!title && (
                    <Text style={styles.title}>
                        {title}
                    </Text>
                )}
                {!!footerText && (
                    <Text style={styles.footerText}>
                        {footerText}
                    </Text>
                )}
                {!hideButton && (
                    <Button
                        style={styles.getStyle('button')}
                        styleType={ButtonStyleType.Fill}
                        title={buttonTitle}
                        onPress={onPress}
                    />
                )}
            </View>
        </Animated.View>
    );
};

export default React.memo(BottomCardComponent, BottomCardPresenter.outputAreEqual);