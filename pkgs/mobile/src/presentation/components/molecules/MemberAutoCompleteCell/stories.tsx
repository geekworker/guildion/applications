import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MemberAutoCompleteCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Member } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('MemberAutoCompleteCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MemberAutoCompleteCell
            style={new Style({
                width: number('width', 370),
                height: 40,
            })}
            member={new Member({
                displayName: 'test'
            })}
        />
    ))