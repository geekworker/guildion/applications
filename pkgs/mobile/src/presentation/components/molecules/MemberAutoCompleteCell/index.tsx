import React from 'react';
import MemberAutoCompleteCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Member } from '@guildion/core';
import { Text, View, TouchableHighlight } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';

type Props = {
    member: Member,
    onPress?: (member: Member) => void,
} & Partial<StyleProps>;

const MemberAutoCompleteCell: React.FC<Props> = ({
    style,
    appTheme,
    member,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new MemberAutoCompleteCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={React.useCallback(() => onPress && onPress(member), [onPress, member])}>
                <View style={styles.inner}>
                    <AsyncImage style={styles.getStyle('profile')} url={member.profile?.url ?? ''}  />
                    <Text style={styles.name} numberOfLines={1}>
                        {member.displayName}
                    </Text>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(MemberAutoCompleteCell, compare);