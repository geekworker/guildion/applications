import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: style?.paddingLeft ?? 12,
            paddingRight: style?.paddingRight ?? 12,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
        },
        title: {
            color: appTheme.navigation.defaultText,
            fontFamily: MontserratFont.Bold,
            fontSize: 28,
            textAlign: 'left',
            width: Number(style?.width ?? 0) - 158 - 36 - 24,
        },
        rightButtons: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'center',
            height: 32,
            paddingRight: 20,
        },
        rightNewButton: {
            color: appTheme.element.default,
            backgroundColor: appTheme.button.mono,
            width: 150,
            height: 36,
            borderRadius: 18,
            fontSize: 15,
        },
        rightNewIcon: {
            color: appTheme.element.default,
            width: 20,
            height: 20,
        },
        rightEditButton: {
            color: appTheme.element.default,
            backgroundColor: appTheme.button.mono,
            width: 36,
            height: 36,
            borderRadius: 18,
            marginRight: 8,
        },
        rightEditIcon: {
            color: appTheme.element.default,
            width: 26,
            height: 26,
        },
    })
};

type Styles = typeof styles;

export default class GuildsHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};