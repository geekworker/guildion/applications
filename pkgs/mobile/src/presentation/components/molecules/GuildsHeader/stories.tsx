import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildsHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('GuildsHeader', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <GuildsHeader
            style={new Style({
                width: 380,
                height: 64,
                backgroundColor: fallbackAppTheme.background.subp1,
                paddingTop: 20,
            })}
        />
    ))
