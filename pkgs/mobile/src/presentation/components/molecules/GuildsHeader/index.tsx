import React from 'react';
import GuildsHeaderStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Animated, Text } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IconButton from '@/presentation/components/atoms/IconButton';
import { localizer } from '@/shared/constants/Localizer';
import Button from '@/presentation/components/atoms/Button';
import { compare } from '@/shared/modules/ObjectCompare';
import { TouchableHighlight } from '@gorhom/bottom-sheet';

type Props = {
    onPressGuildNew?: () => void,
    onPressGuildsEdit?: () => void,
} & Partial<StyleProps>;

const GuildsHeader: React.FC<Props> = ({
    style,
    animatedStyle,
    appTheme,
    onPressGuildNew,
    onPressGuildsEdit,
}) => {
    const styles = React.useMemo(() => new GuildsHeaderStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toObject() }}>
            <Text style={styles.title}>
                Guilds
            </Text>
            <Animated.View style={styles.rightButtons}>
                <IconButton
                    icon={<EntypoIcon size={styles.rightEditIcon.width} name={'list'} color={styles.rightEditIcon.color} />}
                    style={styles.getStyle('rightEditButton')}
                    onPress={onPressGuildsEdit}
                />
                <Button
                    style={styles.getStyle('rightNewButton')}
                    title={localizer.dictionary.guild.home.create}
                    styleType={'fill'}
                    icon={<EntypoIcon size={styles.rightNewIcon.width} name={'plus'} color={styles.rightNewIcon.color} />}
                    onPress={onPressGuildNew}
                    TouchableComponent={TouchableHighlight}
                />
            </Animated.View>
        </Animated.View>
    )
};

export default React.memo(GuildsHeader, compare);