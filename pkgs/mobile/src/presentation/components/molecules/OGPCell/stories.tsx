import { storiesOf } from '@storybook/react-native';
import React from 'react';
import OGPCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { OGP, Files } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('OGPCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <OGPCell
            style={new Style({
                width: number('width', 300),
                height: number('height', 44),
            })}
            ogp={new OGP({
                title: 'guildion - video community service -',
                url: 'https://www.guildion.co',
                imageURL: Files.getSeed().generateGuildProfile().url,
            })}
        />
    ))