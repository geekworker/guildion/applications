import React from 'react';
import OGPCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { OGP } from '@guildion/core';
import AsyncImage from '../../atoms/AsyncImage';

type Props = {
    ogp: OGP,
    onPressOGP?: (ogp: OGP) => void,
} & Partial<StyleProps>;

const OGPCell: React.FC<Props> = ({
    style,
    appTheme,
    ogp,
    onPressOGP,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new OGPCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const onPress = React.useCallback(() => {
        onPressOGP && onPressOGP(ogp);
    }, [onPressOGP, ogp]);
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}>
                    <View style={styles.border}/>
                    <View style={styles.right}>
                        <Text style={styles.domain} numberOfLines={1}>
                            {ogp.getDomain()}
                        </Text>
                        <Text style={styles.title} numberOfLines={1}>
                            {ogp.title}
                        </Text>
                    </View>
                    <AsyncImage style={styles.getStyle('image')} url={ogp.imageURL} />
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default React.memo(OGPCell, compare);