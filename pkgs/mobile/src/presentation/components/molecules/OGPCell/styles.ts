import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const rightWidth = (style?.getAsNumber('width') ?? 0) - 13 - 54;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        touchable: {
            width: style?.width,
            height: style?.height,
        },
        inner: {
            width: style?.width,
            height: style?.height,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
        },
        border: {
            width: 3,
            borderRadius: 1.5,
            height: style?.height,
            marginRight: 10,
            backgroundColor: appTheme.element.url,
        },
        right: {
            width: rightWidth,
            height: style?.height,
        },
        domain: {
            width: rightWidth,
            fontSize: 12,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
        },
        title: {
            width: rightWidth,
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.url,
            marginTop: 8,
        },
        image: {
            width: 44,
            height: 44,
            borderRadius: 5,
            marginLeft: 10,
        },
    })
};

type Styles = typeof styles;

export default class OGPCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};