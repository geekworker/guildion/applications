import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildPublicProfileHooks from './hooks';
import GuildPublicProfileStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Guild, SocialLink } from '@guildion/core';

namespace GuildPublicProfilePresenter {
    export type Input = {
        guild: Guild,
        loading?: boolean,
        onPressSocialLink?: (socialLink: SocialLink) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildPublicProfileStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        guild: Guild,
        loading?: boolean,
        onPressSocialLink?: (socialLink: SocialLink) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildPublicProfileHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default GuildPublicProfilePresenter;