import React from 'react';
import { Animated, Text, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import AsyncImage from '../../atoms/AsyncImage';
import ConnectingLabel from '../../atoms/ConnectingLabel';
import MembersCountLabel from '../../atoms/MembersCountLabel';
import SocialLinksList from '../../organisms/SocialLinksList';
import GuildPublicProfilePresenter from './presenter';

const GuildPublicProfileComponent = ({ styles, appTheme, animatedStyle, guild, loading, onPressSocialLink }: GuildPublicProfilePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <Animated.View style={{ ...styles.profileContainer }}>
                <AsyncImage
                    style={styles.getStyle('backgroundImage')}
                    url={guild.profile?.url ?? ''}
                    loading={loading}
                />
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={[
                        'rgba(0, 0, 0, 0)',
                        String(styles.container.backgroundColor),
                    ]}
                    style={styles.filterBottom}
                />
                {!loading && (
                    <View style={styles.textContainer}>
                        <Text style={styles.displayName}>
                            {guild.displayName}
                        </Text>
                        {!!guild.description && (
                            <Text style={styles.description}>
                                {guild.description}
                            </Text>
                        )}
                    </View>
                )}
            </Animated.View>
            {!loading && guild.records.socialLinks && guild.records.socialLinks.size > 0 && (
                <View style={styles.socialLinksContainer}>
                    <SocialLinksList
                        socialLinks={guild.records.socialLinks}
                        style={styles.getStyle('socialLinks')}
                        onPressSocialLink={onPressSocialLink}
                    />
                </View>
            )}
            {!loading && guild.records.activity && (
                <View style={styles.countersContainer}>
                    <ConnectingLabel
                        count={guild.records.activity.connectsCount}
                        style={styles.getStyle('connectingLabel')}
                    />
                    <MembersCountLabel
                        count={guild.records.activity.membersCount}
                        style={styles.getStyle('membersCountLabel')}
                    />
                </View>
            )}
        </Animated.View>
    );
};

export default React.memo(GuildPublicProfileComponent, GuildPublicProfilePresenter.outputAreEqual);