import React from 'react';
import GuildPublicProfileStyles from './styles';
import type GuildPublicProfilePresenter from './presenter';

namespace GuildPublicProfileHooks {
    export const useStyles = (props: GuildPublicProfilePresenter.Input) => {
        const styles = React.useMemo(() => new GuildPublicProfileStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: GuildPublicProfilePresenter.Input) => {
        return {};
    }
}

export default GuildPublicProfileHooks;