import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildPublicProfile from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, Guild, GuildActivity, SocialLink, SocialLinks } from '@guildion/core';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

storiesOf('GuildPublicProfile', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView
                        style={{
                            backgroundColor: fallbackAppTheme.background.root,
                        }}
                    >
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildPublicProfile
            style={new Style({
                width: 380,
                height: 430,
                backgroundColor: fallbackAppTheme.background.subm1,
            })}
            guild={new Guild(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new GuildActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))
    .add('Loading', () => (
        <GuildPublicProfile
            style={new Style({
                width: 380,
                height: 430,
                backgroundColor: fallbackAppTheme.background.subm1,
            })}
            loading
            guild={new Guild(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new GuildActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))