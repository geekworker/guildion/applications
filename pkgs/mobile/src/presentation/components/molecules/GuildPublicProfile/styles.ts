import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const bottomHeight = 100;
    const profileHeight = Math.max(height - bottomHeight, 0);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height: undefined,
            maxHeight: height,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        profileContainer: {
            width,
            height: profileHeight,
            position: 'relative',
            marginBottom: 20,
        },
        backgroundImage: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            width,
            height: profileHeight,
        },
        filterTop: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: 88,
        },
        filterBottom: {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 0,
            height: profileHeight / 3,
        },
        textContainer: {
            position: 'absolute',
            left: 0,
            right: 0,
            bottom: 20,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        displayName: {
            fontFamily: MontserratFont.Bold,
            fontSize: 24,
            color: appTheme.element.default,
            textAlign: 'center',
        },
        description: {
            marginTop: 12,
            fontFamily: MontserratFont.Regular,
            fontSize: 16,
            color: appTheme.element.default,
            textAlign: 'center',
        },
        socialLinksContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 24,
            width,
            marginBottom: 20,
        },
        socialLinks: {
            height: 24,
        },
        countersContainer: {
            marginBottom: 20,
            width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        connectingLabel: {
            marginRight: 12,
        },
        membersCountLabel: {
        },
    });
};

type Styles = typeof styles;

export default class GuildPublicProfileStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};