import React from 'react';
import GuildPublicProfileComponent from './component';
import GuildPublicProfilePresenter from './presenter';

const GuildPublicProfile = (props: GuildPublicProfilePresenter.Input) => {
    const output = GuildPublicProfilePresenter.usePresenter(props);
    return <GuildPublicProfileComponent {...output} />;
};

export default React.memo(GuildPublicProfile, GuildPublicProfilePresenter.inputAreEqual);