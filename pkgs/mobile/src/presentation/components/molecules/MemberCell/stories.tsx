import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MemberCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Member, Files } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('MemberCell', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <MemberCell
            style={new Style({
                width: 44,
                height: 44,
                borderRadius: 22,
            })}
            member={new Member({ profile: Files.getSeed().generateUserProfile().toJSON() })}
        />
    ))
    .add('Disconnected', () => (
        <MemberCell
            style={new Style({
                width: 44,
                height: 44,
                borderRadius: 22,
            })}
            member={new Member({ profile: Files.getSeed().generateUserProfile().toJSON(), isConnecting: false })}
            connectingLayoutable={true}
        />
    ))
