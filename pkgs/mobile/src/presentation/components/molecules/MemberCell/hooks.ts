import React from "react";
import MemberCellStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Member } from "@guildion/core";

namespace MemberCellHooks {
    export const useStyles = (props: StyleProps & { connectingLayoutable?: boolean, member: Member }) => {
        const styles = React.useMemo(() => new MemberCellStyles({ ...props, isConnecting: props.member.isConnecting }), [
            props,
        ]);
        return styles;
    }
}

export default MemberCellHooks