import React from 'react';
import MemberCellComponent from './component';
import MemberCellPresenter from './presenter';

const MemberCell = (props: MemberCellPresenter.Input) => {
    const output = MemberCellPresenter.usePresenter(props);
    return <MemberCellComponent {...output} />;
};

export default React.memo(MemberCell, MemberCellPresenter.inputAreEqual);