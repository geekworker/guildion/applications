import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MemberCellHooks from "./hooks";
import MemberCellStyles from './styles';
import React from 'react';
import { Member } from "@guildion/core";
import { compare } from "@/shared/modules/ObjectCompare";

namespace MemberCellPresenter {
    export type Input = {
        children?: React.ReactNode,
        member: Member,
        connectingLayoutable?: boolean,
        onPress?: (member: Member) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MemberCellStyles,
        appTheme: AppTheme,
        member: Member,
        connectingLayoutable?: boolean,
        onPress?: () => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = MemberCellHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress: () => props.onPress && props.onPress(props.member),
        }
    }
}

export default MemberCellPresenter;