import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import MemberCellPresenter from './presenter';

const MemberCellComponent = ({ styles, member, onPress, loading }: MemberCellPresenter.Output) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress} disabled={loading}>
                <AsyncImage loading={loading} style={styles.getStyle('image')} url={member.profile?.url ?? ''}/>
                <View style={styles.filter}/>
            </TouchableOpacity>
        </View>
    );
};

export default React.memo(MemberCellComponent, MemberCellPresenter.outputAreEqual);