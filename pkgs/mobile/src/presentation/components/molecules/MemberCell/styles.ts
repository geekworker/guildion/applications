import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({
    style,
    appTheme,
    connectingLayoutable,
    isConnecting,
}: StyleProps & { connectingLayoutable?: boolean, isConnecting?: boolean }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            overflow: 'hidden',
            position: 'relative',
            borderWidth: style?.borderWidth ?? 1,
            borderColor: style?.backgroundColor ?? appTheme.room.cellBackground,
        },
        filter: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: connectingLayoutable && !isConnecting ? appTheme.member.cellFilterColor : appTheme.common.transparent,
            opacity: appTheme.member.cellFilterOpacity,
        },
        image: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
    });
}

type Styles = typeof styles;

export default class MemberCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { connectingLayoutable?: boolean, isConnecting?: boolean }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};