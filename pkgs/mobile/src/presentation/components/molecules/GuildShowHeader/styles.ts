import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingRight: 2,
            paddingLeft: 2,
        },
        highlight: {
            borderRadius: 10,
        },
        inner: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 12,
            paddingBottom: 12,
            paddingRight: 18,
            paddingLeft: 18,
        },
        profile: {
            marginRight: 10,
            width: 44,
            height: 44,
            borderRadius: 5,
        },
        nameContainer: {
            width: Number(style?.width ?? 0) - 74 - 52,
        },
        nameSkeleton: {
            width: Number(style?.width ?? 0) - 74 - 52,
            height: 16,
            marginBottom: 4,
        },
        name: {
            width: Number(style?.width ?? 0) - 74 - 52,
            color: appTheme.element.default,
            textAlign: 'left',
            fontFamily: MontserratFont.Bold,
            fontSize: 16,
            marginBottom: 4,
        },
        uuidSkeleton: {
            width: Number(style?.width ?? 0) - 74 - 52,
            height: 12,
        },
        uuid: {
            width: Number(style?.width ?? 0) - 74 - 52,
            color: appTheme.element.subp1,
            textAlign: 'left',
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
        },
        moreIcon: {
            marginLeft: 10,
            marginRight: 20,
            width: 22,
            height: 22,
            color: appTheme.element.subp1,
        },
    })
};

type Styles = typeof styles;

export default class GuildShowHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};