import React from 'react';
import GuildShowHeaderComponent from './component';
import GuildShowHeaderPresenter from './presenter';

const GuildShowHeader = (props: GuildShowHeaderPresenter.Input) => {
    const output = GuildShowHeaderPresenter.usePresenter(props);
    return <GuildShowHeaderComponent {...output} />;
};

export default React.memo(GuildShowHeader, GuildShowHeaderPresenter.inputAreEqual);