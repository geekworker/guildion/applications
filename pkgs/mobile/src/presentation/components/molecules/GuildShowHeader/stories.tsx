import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildShowHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Guild } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';
import { boolean } from '@storybook/addon-knobs';

storiesOf('GuildShowHeader', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <GuildShowHeader
            guild={new Guild({ displayName: 'test guild', id: '@test_guild' })}
            style={new Style({
                width: 385,
                height: 72,
            })}
            loading={boolean('loading', false)}
        />
    ))
    .add('Loading', () => (
        <GuildShowHeader
            guild={new Guild({ displayName: 'test guild', id: '@test_guild' })}
            style={new Style({
                width: 385,
                height: 72,
            })}
            loading={boolean('loading', true)}
        />
    ))
