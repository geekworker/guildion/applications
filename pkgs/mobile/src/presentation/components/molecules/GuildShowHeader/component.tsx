import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import GuildShowHeaderPresenter from './presenter';
import FeatherIcon from 'react-native-vector-icons/Feather';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const GuildShowHeaderComponent: React.FC<GuildShowHeaderPresenter.Output> = ({ styles, onPress, guild, loading, appTheme }) => {
    return (
        <View style={styles.container}>
            <TouchableHighlight onPress={onPress} style={styles.highlight} disabled={loading} >
                <View style={styles.inner} >
                    <AsyncImage loading={loading} url={guild.profile?.url!} style={styles.getStyle('profile')} />
                    <View style={styles.nameContainer}>
                        <SkeletonContent
                            isLoading={loading ?? false}
                            boneColor={appTheme.skeleton.boneColor}
                            highlightColor={appTheme.skeleton.highlightColor}
                            animationType="pulse"
                            layout={[styles.nameSkeleton]}
                        >
                            <Text style={styles.name} numberOfLines={1} adjustsFontSizeToFit>
                                {guild.displayName}
                            </Text>
                        </SkeletonContent>
                        <SkeletonContent
                            isLoading={loading ?? false}
                            boneColor={appTheme.skeleton.boneColor}
                            highlightColor={appTheme.skeleton.highlightColor}
                            animationType="pulse"
                            layout={[styles.uuidSkeleton]}
                        >
                            <Text style={styles.uuid} numberOfLines={1} adjustsFontSizeToFit>
                                {guild.guildname}
                            </Text>
                        </SkeletonContent>
                    </View>
                    {!loading && (
                        <FeatherIcon name={'more-horizontal'} size={styles.moreIcon.width} color={styles.moreIcon.color} />
                    )}
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(GuildShowHeaderComponent, GuildShowHeaderPresenter.outputAreEqual);