import React from "react";
import GuildShowHeaderStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace GuildShowHeaderHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new GuildShowHeaderStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default GuildShowHeaderHooks;