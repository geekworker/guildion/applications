import React from 'react';
import MentionAutoCompleteCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Text, View, TouchableHighlight } from 'react-native';
import { MentionType } from '@guildion/core';

type Props = {
    onPress?: (type: MentionType) => void,
    type: MentionType,
} & Partial<StyleProps>;

const MentionAutoCompleteCell: React.FC<Props> = ({
    style,
    appTheme,
    onPress,
    type,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new MentionAutoCompleteCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const textStyle = React.useMemo(() => {
        switch(type) {
        case MentionType.Members:
        default: return styles.members;
        }
    }, [type]);
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={React.useCallback(() => onPress && onPress(type), [onPress, type])}>
                <View style={styles.inner}>
                    <Text style={textStyle} numberOfLines={1}>
                        {type}
                    </Text>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(MentionAutoCompleteCell, compare);