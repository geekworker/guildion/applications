import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MentionAutoCompleteCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { MentionType } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('MentionAutoCompleteCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MentionAutoCompleteCell
            style={new Style({
                width: number('width', 370),
                height: 44,
            })}
            type={MentionType.Members}
        />
    ))