import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        touchable: {
            width: style?.width,
            height: style?.height,
        },
        inner: {
            width: style?.width,
            height: style?.height,
            paddingTop: 4,
            paddingBottom: 4,
            paddingLeft: 20,
            paddingRight: 20,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            borderBottomColor: appTheme.background.subp1,
            borderBottomWidth: 1,
        },
        members: {
            fontFamily: MontserratFont.Regular,
            color: appTheme.mention.yourColor,
            fontSize: 16,
            textAlign: 'left',
        },
    })
};

type Styles = typeof styles;

export default class MentionAutoCompleteCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};