import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildPublicHeaderHooks from './hooks';
import GuildPublicHeaderStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Guild } from '@guildion/core';
import { Animated } from 'react-native';

namespace GuildPublicHeaderPresenter {
    export type Input = {
        guild: Guild,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
        onPressClose?: (guild: Guild) => void,
        onPressEdit?: (guild: Guild) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildPublicHeaderStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        guild: Guild,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
        onPressClose: () => void,
        onPressEdit: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildPublicHeaderHooks.useStyles({ ...props });
        const {
            onPressClose,
            onPressEdit,
        } = GuildPublicHeaderHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPressClose,
            onPressEdit,
        }
    }
}

export default GuildPublicHeaderPresenter;