import React from 'react';
import GuildPublicHeaderStyles from './styles';
import type GuildPublicHeaderPresenter from './presenter';

namespace GuildPublicHeaderHooks {
    export const useStyles = (props: GuildPublicHeaderPresenter.Input) => {
        const styles = React.useMemo(() => new GuildPublicHeaderStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: GuildPublicHeaderPresenter.Input) => {
        const onPressClose = React.useCallback(() => {
            props.onPressClose && props.onPressClose(props.guild);
        }, [props.guild, props.onPressClose]);
        const onPressEdit = React.useCallback(() => {
            props.onPressEdit && props.onPressEdit(props.guild);
        }, [props.guild, props.onPressEdit]);
        return {
            onPressClose,
            onPressEdit,
        };
    }
}

export default GuildPublicHeaderHooks;