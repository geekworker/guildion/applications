import React from 'react';
import GuildPublicHeaderComponent from './component';
import GuildPublicHeaderPresenter from './presenter';

const GuildPublicHeader = (props: GuildPublicHeaderPresenter.Input) => {
    const output = GuildPublicHeaderPresenter.usePresenter(props);
    return <GuildPublicHeaderComponent {...output} />;
};

export default React.memo(GuildPublicHeader, GuildPublicHeaderPresenter.inputAreEqual);