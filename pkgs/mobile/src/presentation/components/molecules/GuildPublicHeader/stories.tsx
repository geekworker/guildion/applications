import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildPublicHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Guild, Files } from '@guildion/core';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Animated } from 'react-native';
import { number } from '@storybook/addon-knobs';

storiesOf('GuildPublicHeader', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildPublicHeader
            style={new Style({
                width: 380,
                height: 64,
                backgroundColor: fallbackAppTheme.background.subp1,
                paddingTop: 20,
            })}
            guild={new Guild(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                }
            )}
            transition={new Animated.Value(number('transition', 0) || 0)}
        />
    ))