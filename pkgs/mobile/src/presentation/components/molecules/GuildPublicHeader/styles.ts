import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const paddingTop = style?.getAsNumber('paddingTop') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            backgroundColor: appTheme.common.transparent,
            paddingTop: 0,
        },
        animatedContainer: {
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        inner: {
            paddingLeft: style?.paddingLeft ?? 12,
            paddingRight: style?.paddingRight ?? 12,
            paddingTop: Math.min(40, paddingTop),
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
        },
        leftButtons: {
        },
        titleContainer: {
        },
        rightButtons: {
        },
        leftButtonsInner: {
        },
        titleContainerInner: {
        },
        rightButtonsInner: {
            height,
        },
        animatedLeftButtonsInner: {
            height,
        },
        animatedTitleContainerInner: {
            height,
        },
        animatedRightButtonsInner: {
            height,
        },
        rightEditButton: {
            width: 28,
            height: 28,
            borderRadius: 14,
            marginTop: (height - 28) / 2,
        },
        rightEditIcon: {
            color: appTheme.element.default,
            width: 16,
            height: 16,
        },
        animatedRightEditButton: {
            width: 20,
            height: 20,
            marginRight: 8,
            marginTop: -(height - 20) + (-(height - 20) / 3 * 2) + 4,
        },
        animatedRightEditIcon: {
            color: appTheme.element.default,
            width: 20,
            height: 20,
        },
        leftCloseButton: {
            width: 28,
            height: 28,
            borderRadius: 14,
            marginTop: (height - 28) / 2,
        },
        leftCloseIcon: {
            color: appTheme.element.default,
            width: 16,
            height: 16,
        },
        animatedLeftCloseButton: {
            width: 24,
            height: 24,
            marginTop: -(height - 24) + (-(height - 24) / 3 * 2) + 2,
        },
        animatedLeftCloseIcon: {
            color: appTheme.element.default,
            width: 24,
            height: 24,
        },
        animatedTitle: {
            color: appTheme.element.default,
            fontFamily: MontserratFont.Bold,
            fontSize: 18,
            textAlign: 'center',        
        },
    });
};

type Styles = typeof styles;

export default class GuildPublicHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};