import React from 'react';
import { Animated, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import GuildPublicHeaderPresenter from './presenter';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import IconButton from '../../atoms/IconButton';

const GuildPublicHeaderComponent = ({ styles, appTheme, animatedStyle, guild, transition, onPressClose, onPressEdit }: GuildPublicHeaderPresenter.Output) => {
    return (
        <Animated.View
            style={{
                ...styles.container,
                ...animatedStyle?.toStyleObject(),
                backgroundColor: !transition ?
                    styles.container.backgroundColor :
                    transition.interpolate({
                        inputRange: [0, 1],
                        outputRange: [styles.container.backgroundColor, String(styles.animatedContainer.backgroundColor)],
                    }),
            }}
        >
            <Animated.View
                style={{
                    ...styles.inner,
                }}
            >
                <Animated.View
                    style={styles.leftButtons}
                >
                    <Animated.View
                        style={{
                            ...styles.leftButtonsInner,
                            opacity: transition ? transition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [1, 0],
                            }) : 1,
                        }}
                    >
                        <IconButton
                            icon={<AntDesignIcon size={styles.leftCloseIcon.width} name={'close'} color={styles.leftCloseIcon.color} />}
                            style={styles.getStyle('leftCloseButton')}
                            onPress={onPressClose}
                            isBlur
                        />
                    </Animated.View>
                    <Animated.View
                        style={{
                            ...styles.animatedLeftButtonsInner,
                            opacity: transition ? transition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, 1],
                            }) : 0,
                        }}
                    >
                        <TouchableOpacity onPress={onPressClose} style={styles.animatedLeftCloseButton}>
                            <AntDesignIcon
                                name={"close"}
                                color={styles.animatedLeftCloseIcon.color}
                                size={styles.animatedLeftCloseIcon.width}
                            />
                        </TouchableOpacity>
                    </Animated.View>
                </Animated.View>
                <Animated.View
                    style={styles.titleContainer}
                >
                    <Animated.View
                        style={{
                            ...styles.titleContainerInner,
                            opacity: transition ? transition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [1, 0],
                            }) : 1,
                        }}
                    >
                    </Animated.View>
                    <Animated.View
                        style={{
                            ...styles.animatedTitleContainerInner,
                            opacity: transition ? transition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, 1],
                            }) : 0,
                            transform: transition ? [
                                {
                                    translateY: transition.interpolate({
                                        inputRange: [0, 1],
                                        outputRange: [styles.animatedTitle.fontSize, -14],
                                    }),
                                }
                            ] : [],
                        }}
                    >
                        <Text style={styles.animatedTitle}>
                            {guild.displayName}
                        </Text>
                    </Animated.View>
                </Animated.View>
                <Animated.View
                    style={styles.rightButtons}
                >
                    <Animated.View
                        style={{
                            ...styles.rightButtonsInner,
                            opacity: transition ? transition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [1, 0],
                            }) : 1,
                        }}
                    >
                        <IconButton
                            icon={<EntypoIcon size={styles.rightEditIcon.width} name={'edit'} color={styles.rightEditIcon.color} />}
                            style={styles.getStyle('rightEditButton')}
                            onPress={onPressEdit}
                            isBlur
                        />
                    </Animated.View>
                    <Animated.View
                        style={{
                            ...styles.animatedRightButtonsInner,
                            opacity: transition ? transition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, 1],
                            }) : 0,
                        }}
                    >
                        <TouchableOpacity onPress={onPressEdit} style={styles.animatedRightEditButton}>
                            <EntypoIcon
                                name={"edit"}
                                color={styles.animatedRightEditIcon.color}
                                size={styles.animatedRightEditIcon.width}
                            />
                        </TouchableOpacity>
                    </Animated.View>
                </Animated.View>
            </Animated.View>
        </Animated.View>
    );
};

export default React.memo(GuildPublicHeaderComponent, GuildPublicHeaderPresenter.outputAreEqual);