import React from 'react';
import PostShowItemComponent from './component';
import PostShowItemPresenter from './presenter';

const PostShowItem = (props: PostShowItemPresenter.Input) => {
    const output = PostShowItemPresenter.usePresenter(props);
    return <PostShowItemComponent {...output} />;
};

export default React.memo(PostShowItem, PostShowItemPresenter.inputAreEqual);