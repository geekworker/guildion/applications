import React from "react";
import PostShowItemStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace PostShowItemHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new PostShowItemStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default PostShowItemHooks;