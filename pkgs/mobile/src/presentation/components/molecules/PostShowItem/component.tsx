import React from 'react';
import { View } from 'react-native';
import PostAttachment from '../../atoms/PostAttachment';
import RoomParsedText from '../../atoms/RoomParsedText';
import PostShowItemPresenter from './presenter';

const PostShowItemComponent = ({ styles, appTheme, post, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom }: PostShowItemPresenter.Output) => {
    return (
        <View style={styles.container}>
            {post.records.files && post.records.files.size > 0 && (
                <PostAttachment
                    style={styles.getStyle('attachment')}
                    file={post.records.files.get(0)!}
                    onPress={onPressFile}
                    appTheme={appTheme}
                />
            )}
            <RoomParsedText
                style={styles.getStyle('post')}
                text={post.body}
                onPressURL={onPressURL}
                onPressEmail={onPressEmail}
                onPressPhoneNumber={onPressPhoneNumber}
                onPressMentionMember={onPressMentionMember}
                onPressMentionRoom={onPressMentionRoom}
                appTheme={appTheme}
            />
        </View>
    );
};

export default React.memo(PostShowItemComponent, PostShowItemPresenter.outputAreEqual);