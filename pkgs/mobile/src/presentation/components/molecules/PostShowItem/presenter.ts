import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import PostShowItemHooks from "./hooks";
import PostShowItemStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Post } from "@guildion/core";

namespace PostShowItemPresenter {
    export type Input = {
        post: Post,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PostShowItemStyles,
        appTheme: AppTheme,
        post: Post,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PostShowItemHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default PostShowItemPresenter;