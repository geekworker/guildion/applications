import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PostShowHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Member, Post } from '@guildion/core';

storiesOf('PostShowHeader', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PostShowHeader
            style={new Style({
                width: number('width', 375),
            })}
            post={new Post({
                body: 'hello',
                createdAt: new Date().toISOString(),
                sender: new Member({
                    displayName: 'user1',
                }),
            })}
        />
    ))