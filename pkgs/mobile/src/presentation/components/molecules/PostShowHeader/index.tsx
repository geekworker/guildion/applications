import React from 'react';
import PostShowHeaderStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { Member, Post } from '@guildion/core';
import { Text, TouchableOpacity, View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import { CURRENT_CONFIG } from '@/shared/constants/Config';
import TouchableText from '../../atoms/TouchableText';
import IconButton from '../../atoms/IconButton';

type Props = {
    post: Post,
    onPressBack?: () => void,
    onPressSender?: (member: Member) => void,
} & Partial<StyleProps>;

const PostShowHeader: React.FC<Props> = ({
    style,
    appTheme,
    post,
    onPressBack,
    onPressSender,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new PostShowHeaderStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const _onPressSender = React.useCallback(() => {
        onPressSender && post.records.sender && onPressSender(post.records.sender)
    }, []);
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.backTouchable} onPress={onPressBack}>
                <FeatherIcon name={"chevron-left"} size={styles.back.width} color={styles.back.color} />
            </TouchableOpacity>
            <TouchableOpacity onPress={_onPressSender} style={styles.profileTouchable}>
                <AsyncImage
                    url={post.sender?.profile?.url ?? ''}
                    style={styles.getStyle('profile')}
                />
            </TouchableOpacity>
            <TouchableText style={styles.getStyle('displayName')} textStyle={styles.getStyle('displayName')} onPress={_onPressSender} numberOfLines={1}>
                {post.sender?.displayName}
            </TouchableText>
            <Text style={styles.dateDistanceLabel} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                {post.getCreatedDistanceNow(CURRENT_CONFIG.DEFAULT_LANG)}
            </Text>
            <Text style={styles.dateLabel} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                {post.getCreatedHM(CURRENT_CONFIG.DEFAULT_TIMEZONE)}
            </Text>
            <IconButton
                style={styles.getStyle('moreIconButton')}
                icon={<FeatherIcon color={styles.moreIcon.color} size={styles.moreIcon.width} name={'more-vertical'} />}
            />
        </View>
    );
};

export default React.memo(PostShowHeader, compare);