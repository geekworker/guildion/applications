import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width: number = (style?.getAsNumber('width') ?? 0) - 8 - 48;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: style?.paddingLeft ?? 4,
            paddingRight: style?.paddingRight ?? 4,
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
            borderBottomWidth: style?.borderBottomWidth ?? 1,
            borderBottomColor: style?.borderBottomColor ?? appTheme.background.root,
        },
        backTouchable: {
            width: 36,
            height: 36,
        },
        back: {
            width: 36,
            height: 36,
            color: appTheme.element.default,
            marginRight: 12,
        },
        profileTouchable: {
            width: 32,
            height: 32,
            borderRadius: 16,
        },
        profile: {
            width: 32,
            height: 32,
            borderRadius: 16,
        },
        displayName: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            color: appTheme.element.default,
            textAlign: 'left',
            maxWidth: width - 150 - 44,
            marginLeft: 4,
        },
        displayNameSkeleton: {
            height: 12,
            width: 50,
            maxWidth: width - 150 - 46,
            marginLeft: -width + 50 + 46,
        },
        dateDistanceLabel: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginLeft: 8,
            maxWidth: 100,
        },
        dateDistanceLabelSkeleton: {
            height: 12,
            marginLeft: 8,
            width: 34,
            maxWidth: 100,
        },
        dateLabel: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginLeft: 8,
            maxWidth: 34,
        },
        dateLabelSkeleton: {
            height: 12,
            marginLeft: 8,
            width: 34,
            maxWidth: 34,
        },
        moreIconButton: {
            width: 24,
            height: 24,
            position: 'absolute',
            right: 0,
            top: 10,
            borderRadius: 12,
        },
        moreIcon: {
            width: 14,
            height: 14,
            color: appTheme.element.subp1,
        },
    })
};

type Styles = typeof styles;

export default class PostShowHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};