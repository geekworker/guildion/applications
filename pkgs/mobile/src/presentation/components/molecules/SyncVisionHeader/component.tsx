import React from 'react';
import SyncVisionHeaderPresenter from './presenter';
import { Animated, Text, TouchableOpacity, View } from 'react-native';
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import ConnectingLabel from '../../atoms/ConnectingLabel';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import AsyncImage from '../../atoms/AsyncImage';
import MembersList from '../../organisms/MembersList';

const SyncVisionHeaderComponent = ({ appTheme, styles, loading, room, file, onPressFile, onPressRoom, members, opacityAnimated }: SyncVisionHeaderPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, opacity: opacityAnimated ? opacityAnimated : 1 }}>
            <Animated.View style={{ ...styles.headerContainer }}>
                <TouchableOpacity disabled={loading} style={styles.fileButton} onPress={() => onPressFile && onPressFile(file)}>
                    <AntDesignIcon
                        name={"close"}
                        color={styles.close.color}
                        size={styles.close.width}
                    />
                </TouchableOpacity>
                {room && (
                    <View style={styles.centerContainer}>
                        <TouchableOpacity disabled={loading} style={styles.roomContainer} onPress={() => onPressRoom && onPressRoom(room)}>
                            <AsyncImage
                                style={styles.getStyle('roomProfile')}
                                url={room.profile?.url ?? ""}
                                loading={loading}
                            />
                            {loading ? (
                                <SkeletonContent
                                    isLoading={loading}
                                    boneColor={appTheme.skeleton.boneColor}
                                    highlightColor={appTheme.skeleton.highlightColor}
                                    animationType="pulse"
                                    layout={[styles.roomNameSkeleton]}
                                />
                            ) : (
                                <Text style={styles.roomName} numberOfLines={1} minimumFontScale={0.5} adjustsFontSizeToFit>
                                    {room.displayName}
                                </Text>
                            )}
                        </TouchableOpacity>
                        <ConnectingLabel loading={loading} count={room.activity?.connectsCount ?? 0} style={styles.getStyle('connectingLabel')}/>
                    </View>
                )}
            </Animated.View>
            {!!members && !loading && (
                <MembersList
                    style={styles.getStyle('membersList')}
                    members={members}
                    loading={loading}
                />
            )}
        </Animated.View>
    );
};

export default React.memo(SyncVisionHeaderComponent, SyncVisionHeaderPresenter.outputAreEqual);