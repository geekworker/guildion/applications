import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SyncVisionHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { File, Member, Members, Room } from '@guildion/core';

storiesOf('SyncVisionHeader', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SyncVisionHeader
            style={new Style({
                width: 380,
                height: 84,
            })}
            room={new Room()}
            file={new File()}
        />
    ))
    .add('Members', () => (
        <SyncVisionHeader
            style={new Style({
                width: 380,
                height: 84,
            })}
            room={new Room()}
            file={new File()}
            members={new Members([
                new Member(),
                new Member(),
                new Member(),
            ])}
        />
    ))