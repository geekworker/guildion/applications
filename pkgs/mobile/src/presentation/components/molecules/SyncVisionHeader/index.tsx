import React from 'react';
import SyncVisionHeaderComponent from './component';
import SyncVisionHeaderPresenter from './presenter';

const SyncVisionHeader = (props: SyncVisionHeaderPresenter.Input) => {
    const output = SyncVisionHeaderPresenter.usePresenter(props);
    return <SyncVisionHeaderComponent {...output} />;
};

export default React.memo(SyncVisionHeader, SyncVisionHeaderPresenter.inputAreEqual);