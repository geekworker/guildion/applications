import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import SyncVisionHeaderHooks from "./hooks";
import SyncVisionHeaderStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Members, Room } from "@guildion/core";
import { Animated } from "react-native";

namespace SyncVisionHeaderPresenter {
    export type Input = {
        loading?: boolean,
        room?: Room,
        file: File,
        members?: Members,
        onPressFile?: (file: File) => void,
        onPressRoom?: (room: Room) => void,
        opacityAnimated?: Animated.Value | Animated.AnimatedInterpolation,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SyncVisionHeaderStyles,
        appTheme: AppTheme,
        loading?: boolean,
        room?: Room,
        file: File,
        members?: Members,
        onPressFile?: (file: File) => void,
        onPressRoom?: (room: Room) => void,
        opacityAnimated?: Animated.Value | Animated.AnimatedInterpolation,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SyncVisionHeaderHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            opacityAnimated: props.opacityAnimated?.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 1],
            }),
        }
    }
}

export default SyncVisionHeaderPresenter;