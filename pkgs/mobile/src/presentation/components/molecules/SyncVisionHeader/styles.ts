import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const MEMBERS_LIST_HEIGHT: number = 44;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const containerWidth = (Number(style?.width ?? 0) - 36 - 36 - (Number(style?.paddingRight ?? 12) + Number(style?.paddingLeft ?? 12))) - 40;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: style?.paddingLeft ?? 12,
            paddingRight: style?.paddingRight ?? 12,
            backgroundColor: style?.backgroundColor ?? appTheme.common.transparent,
        },
        fileButton: {
            position: 'relative',
            width: 28,
            height: 28,
            marginTop: 0,
        },
        close: {
            width: 28,
            height: 28,
            color: appTheme.element.default,
        },
        headerContainer: {
            width: style?.width,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
        },
        centerContainer: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            marginLeft: 4,
            marginRight: 4,
            width: containerWidth,
        },
        roomContainer: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: 4,
            paddingLeft: 30,
            paddingRight: 0,
        },
        roomProfile: {
            width: 24,
            height: 24,
            borderRadius: 5,
            marginRight: 8,
        },
        roomName: {
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'left',
            fontSize: 16,
            maxWidth: containerWidth - 60,
        },
        roomNameSkeleton: {
            width: containerWidth - 60,
            height: 16,
        },
        connectingLabel: {
        },
        membersList: {
            width: style?.width,
            height: MEMBERS_LIST_HEIGHT + 8,
            paddingTop: 4,
        },
    })
};

type Styles = typeof styles;

export default class SyncVisionHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};