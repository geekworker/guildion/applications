import React from "react";
import SyncVisionHeaderStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace SyncVisionHeaderHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new SyncVisionHeaderStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default SyncVisionHeaderHooks;