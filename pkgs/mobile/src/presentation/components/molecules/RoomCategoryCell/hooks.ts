import React from 'react';
import RoomCategoryCellStyles from './styles';
import type RoomCategoryCellPresenter from './presenter';
import { Room, Rooms } from '@guildion/core';

namespace RoomCategoryCellHooks {
    export const useStyles = (props: RoomCategoryCellPresenter.Input) => {
        const styles = React.useMemo(() => new RoomCategoryCellStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomCategoryCellPresenter.Input) => {
        return {};
    }

    export const useSkeletonMock = (): Rooms => {
        const rooms = React.useMemo(() => new Rooms([
            new Room({ id: '`skeleton1`' }),
            new Room({ id: '`skeleton2`' }),
            new Room({ id: '`skeleton3`' }),
            new Room({ id: '`skeleton4`' }),
            new Room({ id: '`skeleton5`' }),
            new Room({ id: '`skeleton6`' }),
        ]), []);
        return rooms;
    }
}

export default RoomCategoryCellHooks;