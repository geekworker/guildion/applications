import React from 'react';
import RoomCategoryCellComponent from './component';
import RoomCategoryCellPresenter from './presenter';

const RoomCategoryCell = (props: RoomCategoryCellPresenter.Input) => {
    const output = RoomCategoryCellPresenter.usePresenter(props);
    return <RoomCategoryCellComponent {...output} />;
};

export default React.memo(RoomCategoryCell, RoomCategoryCellPresenter.inputAreEqual);