import React from 'react';
import { Animated, Text, View } from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import RoomPublicHorizontalList from '../../organisms/RoomPublicHorizontalList';
import RoomCategoryCellPresenter from './presenter';

const RoomCategoryCellComponent = ({ styles, appTheme, animatedStyle, category, rooms, onPress, loading }: RoomCategoryCellPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                {loading ? (
                    <SkeletonContent
                        isLoading={loading ?? false}
                        boneColor={appTheme.skeleton.boneColor}
                        highlightColor={appTheme.skeleton.highlightColor}
                        animationType="pulse"
                        layout={[
                            { ...styles.titleNameSkeleton },
                        ]}
                    />
                ) : (
                    <Text style={styles.title}>
                        {category.name}
                    </Text>
                )}
                <RoomPublicHorizontalList
                    style={styles.getStyle('rooms')}
                    rooms={rooms}
                    onPress={onPress}
                    loading={loading}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(RoomCategoryCellComponent, RoomCategoryCellPresenter.outputAreEqual);