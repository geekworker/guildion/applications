import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomCategoryCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Room, RoomActivity, RoomCategory, Rooms, Files } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('RoomCategoryCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomCategoryCell
            style={new Style({
                width: number('width', 385),
                height: number('height', 88),
            })}
            category={
                new RoomCategory({
                    name: 'test',
                }, {
                    rooms: new Rooms([
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: 'Test Room1',
                        }, {
                            activity: new RoomActivity({
                                postsCount: 12,
                                createdAt: new Date().toString(),
                            }),
                            profile: Files.getSeed().generateGuildProfile(),
                        }),
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: 'Test Room1',
                        }, {
                            activity: new RoomActivity({
                                postsCount: 12,
                                createdAt: new Date().toString(),
                            }),
                            profile: Files.getSeed().generateGuildProfile(),
                        }),
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: 'Test Room1',
                        }, {
                            activity: new RoomActivity({
                                postsCount: 12,
                                createdAt: new Date().toString(),
                            }),
                            profile: Files.getSeed().generateGuildProfile(),
                        }),
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: 'Test Room1',
                        }, {
                            activity: new RoomActivity({
                                postsCount: 12,
                                createdAt: new Date().toString(),
                            }),
                            profile: Files.getSeed().generateGuildProfile(),
                        }),
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: 'Test Room1',
                        }, {
                            activity: new RoomActivity({
                                postsCount: 12,
                                createdAt: new Date().toString(),
                            }),
                            profile: Files.getSeed().generateGuildProfile(),
                        }),
                    ]),
                })
            }
        />
    ))
    .add('Loading', () => (
        <RoomCategoryCell
            style={new Style({
                width: number('width', 385),
                height: number('height', 88),
            })}
            category={
                new RoomCategory({
                    name: 'test',
                }, {
                })
            }
            loading
        />
    ))