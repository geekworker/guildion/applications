import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomCategoryCellHooks from './hooks';
import RoomCategoryCellStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Room, RoomCategory, Rooms } from '@guildion/core';

namespace RoomCategoryCellPresenter {
    export type Input = {
        category: RoomCategory,
        onPress?: (room: Room) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomCategoryCellStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        category: RoomCategory,
        rooms: Rooms,
        onPress?: (room: Room) => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomCategoryCellHooks.useStyles({ ...props });
        const mockRooms = RoomCategoryCellHooks.useSkeletonMock();
        const rooms = props.loading ? mockRooms : (props.category.records.rooms ?? new Rooms([]));
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            rooms,
        }
    }
}

export default RoomCategoryCellPresenter;