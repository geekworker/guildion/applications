import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
        },
        inner: {
            width,
        },
        title: {
            paddingLeft: style?.paddingLeft ?? 20,
            paddingRight: style?.paddingRight ?? 20,
            fontFamily: MontserratFont.Bold,
            fontSize: 20,
            marginBottom: 8,
            textAlign: 'left',
            color: appTheme.element.default,
        },
        titleNameSkeleton: {
            height: 20,
            width: 120,
            marginTop: -32,
            marginLeft: -width / 2 - 40,
        },
        rooms: {
            width,
            height: 88,
            paddingLeft: 20,
            paddingRight: 32,
        },
    });
};

type Styles = typeof styles;

export default class RoomCategoryCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};