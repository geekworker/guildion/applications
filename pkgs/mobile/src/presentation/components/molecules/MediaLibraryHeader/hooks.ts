import React from 'react';
import MediaLibraryHeaderStyles from './styles';
import type MediaLibraryHeaderPresenter from './presenter';

namespace MediaLibraryHeaderHooks {
    export const useStyles = (props: MediaLibraryHeaderPresenter.Input) => {
        const styles = React.useMemo(() => new MediaLibraryHeaderStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: MediaLibraryHeaderPresenter.Input) => {
        return {};
    }
}

export default MediaLibraryHeaderHooks;