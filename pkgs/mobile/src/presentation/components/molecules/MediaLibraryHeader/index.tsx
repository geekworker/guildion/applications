import React from 'react';
import MediaLibraryHeaderComponent from './component';
import MediaLibraryHeaderPresenter from './presenter';

const MediaLibraryHeader = (props: MediaLibraryHeaderPresenter.Input) => {
    const output = MediaLibraryHeaderPresenter.usePresenter(props);
    return <MediaLibraryHeaderComponent {...output} />;
};

export default React.memo(MediaLibraryHeader, MediaLibraryHeaderPresenter.inputAreEqual);