import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import MediaLibraryHeaderHooks from './hooks';
import MediaLibraryHeaderStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

namespace MediaLibraryHeaderPresenter {
    export type Input = {
        title?: string,
        onPressClose?: () => void,
        onPressBack?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MediaLibraryHeaderStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        title?: string,
        onPressClose?: () => void,
        onPressBack?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MediaLibraryHeaderHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default MediaLibraryHeaderPresenter;