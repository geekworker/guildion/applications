import React from 'react';
import { Animated, View, TouchableOpacity, Text } from 'react-native';
import MediaLibraryHeaderPresenter from './presenter';
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import BackNavigationButton from '../../atoms/BackNavigationButton';

const MediaLibraryHeaderComponent = ({ styles, appTheme, animatedStyle, title, onPressBack, onPressClose }: MediaLibraryHeaderPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject(), }}>
            <View style={styles.leftButton}>
                {onPressClose ? (
                    <TouchableOpacity style={styles.closeButton} onPress={onPressClose}>
                        <AntDesignIcon
                            name={"close"}
                            color={styles.close.color}
                            size={styles.close.width}
                        />
                    </TouchableOpacity>
                ) : onPressBack ? (
                    <BackNavigationButton onPress={onPressBack} />
                ) : null}
            </View>
            <Text style={styles.title}>
                {title}
            </Text>
            <View style={styles.rightButton}>
            </View>
        </Animated.View>
    );
};

export default React.memo(MediaLibraryHeaderComponent, MediaLibraryHeaderPresenter.outputAreEqual);