import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: 0,
            paddingRight: 0,
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        title: {
            color: appTheme.navigation.defaultText,
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            textAlign: 'center',
            maxWidth: Number(style?.width ?? 0) - 48 - 24,
        },
        leftButton: {
            position: 'absolute',
            left: 0,
            top: 0,
            bottom: 0,
            height: style?.height,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        closeButton: {
            width: 28,
            height: 28,
        },
        close: {
            width: 28,
            height: 28,
            color: appTheme.element.default,
        },
        rightButton: {
            position: 'absolute',
            right: 0,
            top: 0,
            bottom: 0,
            height: style?.height,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
        },
    })
};

type Styles = typeof styles;

export default class MediaLibraryHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};