import { CURRENT_CONFIG } from '@/shared/constants/Config';
import React from 'react';
import { Text, View, TouchableOpacity, TouchableHighlight } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import RoomParsedText from '../../atoms/RoomParsedText';
import MessageCellPresenter from './presenter';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import TouchableText from '../../atoms/TouchableText';
import OGPsList from '../../organisms/OGPsList';
import AttachmentsList from '../../organisms/AttachmentsList';
import MessageDateSection from '../../atoms/MessageDateSection';

const MessageCellComponent = ({ styles, appTheme, message, loading, isFirstInDate, isEditing, onPressSender, onLongPressMessage, onPressOGP, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom }: MessageCellPresenter.Output) => {
    return (
        <View style={styles.container}>
            {!loading && isFirstInDate && (
                <MessageDateSection message={message} style={styles.getStyle('dateSection')} />
            )}
            <TouchableHighlight style={styles.touchable} onLongPress={onLongPressMessage} delayPressIn={300} delayLongPress={0} activeOpacity={0.9}>
                <View style={isEditing ? styles.editingInner : styles.inner}>
                    <TouchableOpacity style={styles.left} onPress={onPressSender}>
                        <AsyncImage
                            loading={loading}
                            style={styles.getStyle('profile')}
                            url={message.sender?.profile?.url ?? ''}
                        />
                    </TouchableOpacity>
                    <View style={styles.right}>
                        <View style={styles.textHeadContainer}>
                            {loading ? (
                                <SkeletonContent
                                    isLoading={loading ?? false}
                                    boneColor={appTheme.skeleton.boneColor}
                                    highlightColor={appTheme.skeleton.highlightColor}
                                    animationType="pulse"
                                    layout={[
                                        { ...styles.displayNameSkeleton },
                                    ]}
                                />
                            ) : (
                                <TouchableText style={styles.getStyle('displayName')} textStyle={styles.getStyle('displayName')} onPress={onPressSender} numberOfLines={1}>
                                    {message.sender?.displayName}
                                </TouchableText>
                            )}
                            {!loading && (
                                <Text style={styles.dateDistanceLabel} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                                    {message.getCreatedDistanceNow(CURRENT_CONFIG.DEFAULT_LANG)}
                                </Text>
                            )}
                            {!loading && (
                                <Text style={styles.dateLabel} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                                    {message.getCreatedHM(CURRENT_CONFIG.DEFAULT_TIMEZONE)}
                                </Text>
                            )}
                        </View>
                        {loading && (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                                animationType="pulse"
                                layout={[
                                    { ...styles.messageSkeleton1 },
                                ]}
                            />
                        )}
                        {loading && (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                                animationType="pulse"
                                layout={[
                                    { ...styles.messageSkeleton2 },
                                ]}
                            />
                        )}
                        {!loading && (
                            <RoomParsedText
                                style={styles.getStyle('message')}
                                text={message.body}
                                onPressURL={onPressURL}
                                onPressEmail={onPressEmail}
                                onPressPhoneNumber={onPressPhoneNumber}
                                onPressMentionMember={onPressMentionMember}
                                onPressMentionRoom={onPressMentionRoom}
                            />
                        )}
                        {!loading && message.records.files && message.records.files.length > 0 && (
                            <AttachmentsList
                                style={styles.getStyle('attachments')}
                                files={message.records.files}
                                onPressFile={onPressFile}
                            />
                        )}
                        {!loading && message.records.ogps && message.records.ogps.length > 0 && (
                            <OGPsList
                                style={styles.getStyle('ogps')}
                                ogps={message.records.ogps}
                                onPressOGP={onPressOGP}
                            />
                        )}
                    </View>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(MessageCellComponent, MessageCellPresenter.outputAreEqual);