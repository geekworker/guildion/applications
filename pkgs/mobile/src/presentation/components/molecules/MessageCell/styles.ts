import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') ?? 0;
    const rightWidth = width - (32 + 12) - 20 - 20;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            height: undefined,
        },
        touchable: {
            width: style?.width,
            borderRadius: style?.borderRadius ?? 5,
        },
        inner: {
            width: style?.width,
            height: undefined,
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 20,
            paddingRight: 20,
            minHeight: 64,
        },
        editingInner: {
            width: style?.width,
            height: undefined,
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            paddingTop: 8,
            paddingBottom: 8,
            paddingLeft: 20,
            paddingRight: 20,
            minHeight: 64,
            backgroundColor: appTheme.element.highlight,
        },
        dateSection: {
            width: style?.width,
            height: 44,
        },
        left: {
            paddingRight: 12,
            width: 32 + 12,
        },
        right: {
            width: rightWidth,
        },
        profile: {
            width: 32,
            height: 32,
            borderRadius: 16,
        },
        textHeadContainer: {
            width: rightWidth,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
        },
        displayName: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            color: appTheme.element.default,
            textAlign: 'left',
            maxWidth: rightWidth - 150,
        },
        displayNameSkeleton: {
            height: 12,
            width: 50,
            maxWidth: rightWidth - 150,
            marginLeft: -rightWidth + 50,
        },
        dateDistanceLabel: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginLeft: 8,
            maxWidth: 100,
        },
        dateDistanceLabelSkeleton: {
            height: 12,
            marginLeft: 8,
            width: 34,
            maxWidth: 100,
        },
        dateLabel: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginLeft: 8,
            maxWidth: 34,
        },
        dateLabelSkeleton: {
            height: 12,
            marginLeft: 8,
            width: 34,
            maxWidth: 34,
        },
        message: {
            marginTop: 4,
            width: rightWidth,
        },
        messageSkeleton1: {
            marginTop: 12,
            height: 16,
            width: rightWidth,
        },
        messageSkeleton2: {
            marginTop: 12,
            height: 16,
            width: rightWidth / 3 * 2,
            marginLeft: -rightWidth / 3
        },
        ogps: {
            paddingTop: 4,
            paddingBottom: 4,
            width: rightWidth,
        },
        attachments: {
            paddingTop: 4,
            paddingBottom: 4,
            width: rightWidth,
        },
    })
};

type Styles = typeof styles;

export default class MessageCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};