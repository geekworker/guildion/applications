import React from 'react';
import MessageCellComponent from './component';
import MessageCellPresenter from './presenter';

const MessageCell = (props: MessageCellPresenter.Input) => {
    const output = MessageCellPresenter.usePresenter(props);
    return <MessageCellComponent {...output} />;
};

export default React.memo(MessageCell, MessageCellPresenter.inputAreEqual);