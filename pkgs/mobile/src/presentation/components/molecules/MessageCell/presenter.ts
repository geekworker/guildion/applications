import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MessageCellHooks from "./hooks";
import MessageCellStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Message, OGP } from "@guildion/core";

namespace MessageCellPresenter {
    export type Input = {
        message: Message,
        loading?: boolean,
        isFirstInDate?: boolean,
        isEditing?: boolean,
        onPressSender?: (sender: Member) => void,
        onLongPressMessage?: (message: Message) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MessageCellStyles,
        appTheme: AppTheme,
        message: Message,
        loading?: boolean,
        isFirstInDate?: boolean,
        isEditing?: boolean,
        onPressSender?: () => void,
        onLongPressMessage?: () => void,
        onPressFile?: (file: File) => void,
        onPressOGP?: (ogp: OGP) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MessageCellHooks.useStyles({ ...props });
        const onPressSender = MessageCellHooks.useOnPressSender(props);
        const onLongPressMessage = MessageCellHooks.useOnLongPressMessage(props);
        const onPressOGP = MessageCellHooks.useOnPressOGP(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPressSender,
            onLongPressMessage,
            onPressOGP,
        }
    }
}

export default MessageCellPresenter;