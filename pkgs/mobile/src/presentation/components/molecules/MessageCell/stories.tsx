import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MessageCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, Member, Message, OGP, OGPs } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('MessageCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: 'hello',
                createdAt: new Date().toISOString(),
                sender: new Member({
                    displayName: 'user1',
                }),
            })}
        />
    ))
    .add('Loading', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: 'hello',
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
            })}
            loading={true}
        />
    ))
    .add('isFirstInDate', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: 'hello',
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
            })}
            isFirstInDate={true}
        />
    ))
    .add('ParsedMessage', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
            })}
        />
    ))
    .add('OGPs', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
                ogps: new OGPs([
                    new OGP({
                        id: '1',
                        title: 'guildion - video community service -',
                        url: 'https://www.guildion.co',
                        imageURL: Files.getSeed().generateGuildProfile().url,
                    }),
                    new OGP({
                        id: '2',
                        title: 'guildion - video community service -',
                        url: 'https://www.guildion.co',
                        imageURL: Files.getSeed().generateGuildProfile().url,
                    }),
                    new OGP({
                        id: '3',
                        title: 'guildion - video community service -',
                        url: 'https://www.guildion.co',
                        imageURL: Files.getSeed().generateGuildProfile().url,
                    }),
                ])
            })}
        />
    ))
    .add('Attachments', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
                files: new Files([
                    Files.getSeed().generateGuildProfile(),
                    Files.getSeed().generateGuildProfile(),
                    Files.getSeed().generateGuildProfile(),
                    Files.getSeed().generateGuildProfile(),
                ])
            })}
        />
    ))
    .add('Full', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
                ogps: new OGPs([
                    new OGP({
                        id: '1',
                        title: 'guildion - video community service -',
                        url: 'https://www.guildion.co',
                        imageURL: Files.getSeed().generateGuildProfile().url,
                    }),
                ]),
                files: new Files([
                    Files.getSeed().generateGuildProfile(),
                ])
            })}
        />
    ))
    .add('isEditing', () => (
        <MessageCell
            style={new Style({
                width: number('width', 375),
            })}
            message={new Message({
                body: 'hello',
                createdAt: new Date().toISOString(),
            }, {
                sender: new Member({
                    displayName: 'user1',
                }),
            })}
            isEditing={true}
        />
    ))