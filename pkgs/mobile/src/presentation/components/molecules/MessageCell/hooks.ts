import React from "react";
import MessageCellStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type MessageCellPresenter from "./presenter";
import { OGP } from "@guildion/core";

namespace MessageCellHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new MessageCellStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useOnPressSender = (props: MessageCellPresenter.Input) => {
        return React.useCallback(() => {
            props.onPressSender && props.message.records.sender && props.onPressSender(props.message.records.sender)
        }, [props.onPressSender, props.message]);
    }

    export const useOnLongPressMessage = (props: MessageCellPresenter.Input) => {
        return React.useCallback(() => {
            props.onLongPressMessage && props.message.records.sender && props.onLongPressMessage(props.message)
        }, [props.onLongPressMessage, props.message]);
    }

    export const useOnPressOGP = (props: MessageCellPresenter.Input) => {
        return React.useCallback((ogp: OGP) => {
            props.onPressURL && props.onPressURL(ogp.url)
        }, [props.onPressURL]);
    }
}

export default MessageCellHooks;