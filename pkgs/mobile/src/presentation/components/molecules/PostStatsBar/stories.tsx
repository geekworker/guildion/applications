import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PostStatsBar from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Post } from '@guildion/core';

storiesOf('PostStatsBar', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PostStatsBar
            style={new Style({
                width: 380,
            })}
            post={new Post({
                commentsCount: 12,
                reactionsCount: 24,
            })}
        />
    ))
    .add('Active', () => (
        <PostStatsBar
            style={new Style({
                width: 380,
            })}
            post={new Post({
                commentsCount: 12,
                reactionsCount: 24,
                hasComment: true,
                hasReaction: true,
            })}
        />
    ))