import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: style?.paddingTop ?? 8,
            paddingBottom: style?.paddingBottom ?? 8,
            paddingRight: style?.paddingRight ?? 20,
            paddingLeft: style?.paddingLeft ?? 20,
            borderTopWidth: style?.borderTopWidth ?? 1,
            borderTopColor: style?.borderTopColor ?? appTheme.background.subp1,
            borderBottomWidth: style?.borderBottomWidth ?? 1,
            borderBottomColor: style?.borderBottomColor ?? appTheme.background.subp1,
        },
        reactionIconButton: {
            width: 24,
            height: 24,
            borderRadius: 12,
        },
        reactionIcon: {
            width: 18,
            height: 18,
            color: appTheme.element.subp1,
        },
        reactionCount: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.element.subp1,
            marginLeft: 1,
        },
        activeReactionIcon: {
            width: 18,
            height: 18,
            color: appTheme.element.link,
        },
        activeReactionCount: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.element.link,
            marginLeft: 1,
        },
        commentIconButton: {
            width: 24,
            height: 24,
            borderRadius: 12,
            marginLeft: 8,
        },
        commentIcon: {
            width: 20,
            height: 20,
            color: appTheme.element.subp1,
        },
        commentCount: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.element.subp1,
            marginLeft: 1,
        },
        activeCommentIcon: {
            width: 20,
            height: 20,
            color: appTheme.element.link,
        },
        activeCommentCount: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.element.link,
            marginLeft: 1,
        },
    })
};

type Styles = typeof styles;

export default class PostStatsBarStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};