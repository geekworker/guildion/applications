import React from 'react';
import PostStatsBarStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { View } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import IconButton from '../../atoms/IconButton';
import TouchableText from '../../atoms/TouchableText';
import { Post } from '@guildion/core';

type Props = {
    post: Post,
    onPressReaction?: (post: Post) => void,
    onPressComment?: (post: Post) => void,
} & Partial<StyleProps>;

const PostStatsBar: React.FC<Props> = ({
    style,
    appTheme,
    post,
    onPressReaction,
    onPressComment,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new PostStatsBarStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const _onPressReaction = React.useCallback(() => {
        onPressReaction && onPressReaction(post);
    }, [post, onPressReaction]);
    const _onPressComment = React.useCallback(() => {
        onPressComment && onPressComment(post);
    }, [post, onPressComment]);
    return (
        <View style={styles.container}>
            <IconButton
                style={styles.getStyle('reactionIconButton')}
                icon={<EntypoIcon size={styles.reactionIcon.width} color={post.hasReaction ? styles.activeReactionIcon.color : styles.reactionIcon.color} name={'emoji-happy'} />}
                onPress={_onPressReaction}
            />
            <TouchableText style={styles.getStyle('reactionCount')} textStyle={styles.getStyle(post.hasReaction ? 'activeReactionCount' : 'reactionCount')} onPress={_onPressReaction}>
                {`${post.reactionsCount}`}
            </TouchableText>
            <IconButton
                style={styles.getStyle('commentIconButton')}
                icon={<FoundationIcon size={styles.commentIcon.width} color={post.hasComment ? styles.activeCommentIcon.color : styles.commentIcon.color} name={'comment'} />}
                onPress={_onPressComment}
            />
            <TouchableText style={styles.getStyle('commentCount')} textStyle={styles.getStyle(post.hasComment ? 'activeCommentCount' : 'commentCount')} onPress={_onPressComment}>
                {`${post.commentsCount}`}
            </TouchableText>
        </View>
    );
};

export default React.memo(PostStatsBar, compare);