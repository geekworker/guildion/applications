import React from 'react';
import VideoPlayerOverlayComponent from './component';
import VideoPlayerOverlayPresenter from './presenter';

const VideoPlayerOverlay = (props: VideoPlayerOverlayPresenter.Input) => {
    const output = VideoPlayerOverlayPresenter.usePresenter(props);
    return <VideoPlayerOverlayComponent {...output} />;
};

export default React.memo(VideoPlayerOverlay, VideoPlayerOverlayPresenter.inputAreEqual);