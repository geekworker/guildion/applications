import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') ?? 0;
    const innerWidth = 240;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        touchable: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
        inner: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
            position: "relative",
            backgroundColor: appTheme.player.overlayBackgroundColor,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        playerButton: {
            width: 50,
            height: 50,
            color: appTheme.player.activeIconColor,
        },
        backSkipIconTouchable: {
            width: 32,
            height: 32,
            marginLeft: (width - innerWidth) / 2,
        },
        nextSkipIconTouchable: {
            width: 44,
            height: 44,
            marginRight: (width - innerWidth) / 2,
        },
        backSkipIcon: {
            width: 32,
            height: 32,
            color: appTheme.player.disableIconColor,
        },
        nextSkipIcon: {
            width: 32,
            height: 32,
            color: appTheme.player.disableIconColor,
        },
    })
};

type Styles = typeof styles;

export default class VideoPlayerOverlayStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};