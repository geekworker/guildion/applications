import { storiesOf } from '@storybook/react-native';
import React from 'react';
import VideoPlayerOverlay from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { PlayerStatus } from '@guildion/core';

storiesOf('VideoPlayerOverlay', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <VideoPlayerOverlay
            style={new Style({
                width: number('width', 360),
                height: number('height', 240),
                borderRadius: 5,
            })}
            status={PlayerStatus.PAUSE}
        />
    ))
    .add('Loading', () => (
        <VideoPlayerOverlay
            style={new Style({
                width: number('width', 360),
                height: number('height', 240),
                borderRadius: 5,
            })}
            status={PlayerStatus.LOADING}
        />
    ))
    .add('CanSkip', () => (
        <VideoPlayerOverlay
            style={new Style({
                width: number('width', 360),
                height: number('height', 240),
                borderRadius: 5,
            })}
            canBackSkip
            canNextSkip
            status={PlayerStatus.PAUSE}
        />
    ))