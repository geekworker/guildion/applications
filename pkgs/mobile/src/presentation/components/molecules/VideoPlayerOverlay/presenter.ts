import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import VideoPlayerOverlayHooks from './hooks';
import VideoPlayerOverlayStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { PlayerStatus } from '@guildion/core';
import { Animated } from 'react-native';

namespace VideoPlayerOverlayPresenter {
    export type Input = {
        canNextSkip?: boolean,
        canBackSkip?: boolean,
        status: PlayerStatus,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
        onPress?: () => void,
        onPressPlayerButton?: (status: PlayerStatus) => void,
        onPressNextSkip?: () => void,
        onPressBackSkip?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: VideoPlayerOverlayStyles,
        appTheme: AppTheme,
        canNextSkip?: boolean,
        canBackSkip?: boolean,
        status: PlayerStatus,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
        onPress?: () => void,
        onPressPlayerButton: (status: PlayerStatus) => void,
        onPressNextSkip?: () => void,
        onPressBackSkip?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = VideoPlayerOverlayHooks.useStyles({ ...props });
        const {
            status,
            onPressPlayerButton,
        } = VideoPlayerOverlayHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            status,
            onPressPlayerButton,
        }
    }
}

export default VideoPlayerOverlayPresenter;