import React from 'react';
import VideoPlayerOverlayStyles from './styles';
import type VideoPlayerOverlayPresenter from './presenter';
import { PlayerStatus } from '@guildion/core';

namespace VideoPlayerOverlayHooks {
    export const useStyles = (props: VideoPlayerOverlayPresenter.Input) => {
        const styles = React.useMemo(() => new VideoPlayerOverlayStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: VideoPlayerOverlayPresenter.Input) => {
        const [status, setStatus] = React.useState(props.status);
        React.useEffect(() => {
            status != props.status && setStatus(props.status);
        }, [props.status]);

        const onPressPlayerButton = React.useCallback((newStatus: PlayerStatus) => {
            setStatus(newStatus);
        }, [status, props.onPressPlayerButton]);

        return {
            status,
            onPressPlayerButton,
        };
    }
}

export default VideoPlayerOverlayHooks;