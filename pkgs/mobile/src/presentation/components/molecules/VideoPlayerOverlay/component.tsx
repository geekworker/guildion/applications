import { PlayerStatus } from '@guildion/core';
import React from 'react';
import { Animated, TouchableWithoutFeedback, View } from 'react-native';
import TouchableCircleRipple from '../../atoms/TouchableCircleRipple';
import VideoPlayerOverlayPresenter from './presenter';
import IonIcon from 'react-native-vector-icons/Ionicons';
import PlayerButton from '../../atoms/PlayerButton';

const VideoPlayerOverlayComponent = ({ styles, appTheme, animatedStyle, transition, status, onPress, onPressPlayerButton, onPressNextSkip, onPressBackSkip, canBackSkip, canNextSkip }: VideoPlayerOverlayPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, opacity: transition ,...animatedStyle?.toStyleObject() }}>
            <TouchableWithoutFeedback style={styles.inner} onPress={onPress}>
                <Animated.View style={styles.inner}>
                    {status !== PlayerStatus.LOADING ? (
                        <TouchableCircleRipple
                            onPress={canBackSkip ? onPressBackSkip : () => {}}
                            style={styles.getStyle('backSkipIconTouchable')}
                        >
                            <IonIcon
                                name={'ios-play-skip-back-sharp'}
                                color={canBackSkip ? appTheme.player.activeIconColor : appTheme.player.disableIconColor}
                                size={styles.backSkipIcon.width}
                            />
                        </TouchableCircleRipple>
                    ) : (
                        <View/>
                    )}
                    <PlayerButton
                        status={status}
                        style={styles.getStyle('playerButton')}
                        onPress={onPressPlayerButton}
                    />
                    {status !== PlayerStatus.LOADING ? (
                        <TouchableCircleRipple
                            onPress={canNextSkip ? onPressNextSkip : () => {}}
                            style={styles.getStyle('nextSkipIconTouchable')}
                        >
                            <IonIcon
                                name={'ios-play-skip-forward-sharp'}
                                color={canNextSkip ? appTheme.player.activeIconColor : appTheme.player.disableIconColor}
                                size={styles.nextSkipIcon.width}
                            />
                        </TouchableCircleRipple>
                    ) : (
                        <View/>
                    )}
                </Animated.View>
            </TouchableWithoutFeedback>
        </Animated.View>
    );
};

export default React.memo(VideoPlayerOverlayComponent, VideoPlayerOverlayPresenter.outputAreEqual);