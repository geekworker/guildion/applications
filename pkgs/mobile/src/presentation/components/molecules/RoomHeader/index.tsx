import React from 'react';
import RoomHeaderStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, TouchableOpacity, View } from 'react-native';
import { Guild, Room } from '@guildion/core';
import AsyncImage from '../../atoms/AsyncImage';
import { Badge } from 'react-native-elements';
import AntDesignIcon from "react-native-vector-icons/AntDesign";
import ConnectingLabel from '../../atoms/ConnectingLabel';
import { compare } from '@/shared/modules/ObjectCompare';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

type Props = {
    room: Room,
    guild: Guild,
    onPressGuild?: (guild: Guild) => void,
    onPressRoom?: (room: Room) => void,
    loading?: boolean,
} & Partial<StyleProps>;

const RoomHeader: React.FC<Props> = ({
    style,
    appTheme,
    guild,
    room,
    onPressGuild,
    onPressRoom,
    loading,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new RoomHeaderStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <TouchableOpacity disabled={loading} style={styles.guildButton} onPress={() => onPressGuild && onPressGuild(guild)}>
                <AntDesignIcon
                    name={"close"}
                    color={styles.close.color}
                    size={styles.close.width}
                />
                {/* {!loading && !!guild.activity?.notificationsCount && guild.activity?.notificationsCount > 0 && (
                    <Badge
                        containerStyle={styles.guildButtonBadgeContainerStyle}
                        badgeStyle={styles.guildButtonBadgeStyle}
                        textStyle={styles.guildButtonBadgeTextStyle}
                        status={'error'}
                        value={guild.activity.notificationsCount}
                    />
                )} */}
            </TouchableOpacity>
            <View style={styles.centerContainer}>
                <TouchableOpacity disabled={loading} style={styles.roomContainer} onPress={() => onPressRoom && onPressRoom(room)}>
                    <AsyncImage
                        style={styles.getStyle('roomProfile')}
                        url={room.profile?.url ?? ""}
                        loading={loading}
                    />
                    {loading ? (
                        <SkeletonContent
                            isLoading={loading}
                            boneColor={appTheme.skeleton.boneColor}
                            highlightColor={appTheme.skeleton.highlightColor}
                            animationType="pulse"
                            layout={[styles.roomNameSkeleton]}
                        />
                    ) : (
                        <Text style={styles.roomName} numberOfLines={1} minimumFontScale={0.5} adjustsFontSizeToFit>
                            {room.displayName}
                        </Text>
                    )}
                </TouchableOpacity>
                <ConnectingLabel loading={loading} count={room.activity?.connectsCount ?? 0} style={styles.getStyle('connectingLabel')}/>
            </View>
        </View>
    )
};

export default React.memo(RoomHeader, compare);