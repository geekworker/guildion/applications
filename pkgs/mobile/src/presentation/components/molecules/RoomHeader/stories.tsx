import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Guild, Member, Members, Message, Room, RoomActivity, RoomLog } from '@guildion/core';
import { number, text } from '@storybook/addon-knobs';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('RoomHeader', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <RoomHeader
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    connectingMembers: new Members([
                        new Member({
                            id: 'test1',
                            displayName: 'test1',
                        }).toJSON(),
                        new Member({
                            id: 'test2',
                            displayName: 'test2',
                        }).toJSON(),
                        new Member({
                            id: 'test3',
                            displayName: 'test3',
                        }).toJSON(),
                        new Member({
                            id: 'test4',
                            displayName: 'test4',
                        }).toJSON(),
                    ]).toJSON(),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            guild={new Guild({
                displayName: 'sample guilddddd',
            })}
            style={new Style({
                width: 380,
                height: 64,
                paddingTop: 20,
            })}
        />
    ))
    .add('Loading', () => (
        <RoomHeader
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    connectingMembers: new Members([
                        new Member({
                            id: 'test1',
                            displayName: 'test1',
                        }).toJSON(),
                        new Member({
                            id: 'test2',
                            displayName: 'test2',
                        }).toJSON(),
                        new Member({
                            id: 'test3',
                            displayName: 'test3',
                        }).toJSON(),
                        new Member({
                            id: 'test4',
                            displayName: 'test4',
                        }).toJSON(),
                    ]).toJSON(),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            guild={new Guild({
                displayName: 'sample guilddddd',
            })}
            style={new Style({
                width: 380,
                height: 64,
                paddingTop: 20,
            })}
            loading={true}
        />
    ))
