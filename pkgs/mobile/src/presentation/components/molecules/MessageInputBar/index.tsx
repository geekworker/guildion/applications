import React from 'react';
import MessageInputBarComponent from './component';
import MessageInputBarPresenter from './presenter';

const MessageInputBar = (props: MessageInputBarPresenter.Input) => {
    const output = MessageInputBarPresenter.usePresenter(props);
    return <MessageInputBarComponent {...output} />;
};

export default React.memo(MessageInputBar, MessageInputBarPresenter.inputAreEqual);