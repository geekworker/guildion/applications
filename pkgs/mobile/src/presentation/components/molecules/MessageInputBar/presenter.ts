import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MessageInputBarHooks from "./hooks";
import MessageInputBarStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { Message } from "@guildion/core";

namespace MessageInputBarPresenter {
    export type Input = {
        message: Message,
        submitting?: boolean,
        onSubmit?: (message: Message) => void,
        onPressMedia?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MessageInputBarStyles,
        appTheme: AppTheme,
        message: Message,
        submitting?: boolean,
        onSubmit: () => void,
        onPressMedia?: () => void,
        onChangeMessage?: (message: Message) => void,
        mentionQuery?: string,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MessageInputBarHooks.useStyles({ ...props });
        const {
            message,
            onChangeMessage,
            submitting,
            onSubmit,
            mentionQuery,
        } = MessageInputBarHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            message,
            onChangeMessage,
            submitting,
            onSubmit,
            mentionQuery,
        }
    }
}

export default MessageInputBarPresenter;