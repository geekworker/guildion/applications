import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MessageInputBar from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Message } from '@guildion/core';
import { number } from '@storybook/addon-knobs';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

storiesOf('MessageInputBar', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MessageInputBar
            style={new Style({
                width: number('width', 388),
                minHeight: number('minHeight', 44),
                backgroundColor: fallbackAppTheme.background.subp1,
            })}
            message={new Message()}
        />
    ))