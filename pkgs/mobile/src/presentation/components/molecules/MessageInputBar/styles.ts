import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            height: undefined,
            minHeight: style?.minHeight ?? 44,
        },
        inner: {
            width: style?.width,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'flex-start',
            height: undefined,
            backgroundColor: style?.backgroundColor,
        },
        left: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            paddingLeft: 12,
            paddingRight: 8,
            height: 44,
        },
        right: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-end',
            paddingRight: 12,
            paddingLeft: 8,
            height: 44,
        },
        media: {
            width: 24,
            height: 24,
            color: appTheme.element.subp1,
        },
        submit: {
            width: 32,
            height: 32,
            borderRadius: 18,
            borderColor: appTheme.common.transparent,
            borderWidth: 0,
            backgroundColor: appTheme.button.disabled,
            color: appTheme.common.innerSubWhite,
        },
        submitIcon: {
            width: 16,
            height: 16,
            backgroundColor: appTheme.button.disabled,
            color: appTheme.common.innerSubWhite,
        },
        activeSubmit: {
            width: 32,
            height: 32,
            borderRadius: 18,
            borderColor: appTheme.common.transparent,
            borderWidth: 0,
            backgroundColor: appTheme.button.default,
            color: appTheme.common.innerWhite,
        },
        activeSubmitIcon: {
            width: 16,
            height: 16,
            backgroundColor: appTheme.button.active,
            color: appTheme.common.innerWhite,
        },
        textInput: {
            width: (style?.getAsNumber('width') ?? 0) - (12 + 8 + 32) - (12 + 8 + 32),
            height: undefined,
            minHeight: 30,
            maxHeight: 140,
            marginBottom: 8,
        },
    })
};

type Styles = typeof styles;

export default class MessageInputBarStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};