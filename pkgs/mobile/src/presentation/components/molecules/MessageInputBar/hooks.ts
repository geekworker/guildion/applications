import React from "react";
import MessageInputBarStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type MessageInputBarPresenter from "./presenter";
import { Message } from "@guildion/core";
import { useAutoCompleteManager } from "@/shared/hooks/useAutoCompleteManager";

namespace MessageInputBarHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new MessageInputBarStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useState = (props: MessageInputBarPresenter.Input) => {
        const [message, setMessage] = React.useState(props.message);
        const [submitting, setSubmitting] = React.useState(props.submitting);
        const {
            register,
            mentionQuery,
        } = useAutoCompleteManager();
        React.useEffect(() => {
            props.message != message && setMessage(props.message);
        }, [props.message]);
        React.useEffect(() => {
            props.submitting != submitting && setSubmitting(props.submitting);
        }, [props.submitting]);
        React.useEffect(() => {
        }, [mentionQuery]);
        const onChangeMessage = React.useCallback((newMessage: Message) => {
            register(message.body, newMessage.body);
            setMessage(newMessage);
        }, [setMessage, message]);
        const onSubmit = React.useCallback(() => {
            // setSubmitting(true);
            props.onSubmit && props.onSubmit(message)
        }, [message, props.onSubmit]);
        return {
            message,
            onChangeMessage,
            submitting,
            onSubmit,
            mentionQuery,
        }
    }
}

export default MessageInputBarHooks;