import React from 'react';
import { KeyboardAvoidingView, TouchableHighlight, View } from 'react-native';
import MessageTextInput from '../MessageTextInput';
import MessageInputBarPresenter from './presenter';
import IonIcon from 'react-native-vector-icons/Ionicons';
import IconButton from '../../atoms/IconButton';

const MessageInputBarComponent = ({ styles, appTheme, message, onPressMedia, onChangeMessage, onSubmit, submitting }: MessageInputBarPresenter.Output) => {
    return (
        <View style={styles.container}>
            <KeyboardAvoidingView behavior='position' keyboardVerticalOffset={764}>
                <View style={styles.inner}>
                    <View style={styles.left}>
                        <TouchableHighlight style={styles.media} onPress={onPressMedia}>
                            <IonIcon size={styles.media.width} color={styles.media.color} name={'image'} />
                        </TouchableHighlight>
                    </View>
                    <MessageTextInput
                        message={message}
                        style={styles.getStyle('textInput')}
                        onChangeMessage={onChangeMessage}
                    />
                    <View style={styles.right}>
                        <IconButton
                            style={styles.getStyle(message.checkBodyValid() && !submitting ? 'activeSubmit' : 'submit')}
                            icon={(
                                <IonIcon
                                    size={styles.submitIcon.width}
                                    color={message.checkBodyValid() && !submitting ? styles.activeSubmitIcon.color : styles.submitIcon.color}
                                    name={'send'}
                                />
                            )}
                            disabled={!message.checkBodyValid()}
                            loading={submitting}
                            onPress={onSubmit}
                        />
                    </View>
                </View>
            </KeyboardAvoidingView>
        </View>
    );
};

export default React.memo(MessageInputBarComponent, MessageInputBarPresenter.outputAreEqual);