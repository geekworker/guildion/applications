import React from 'react';
import SyncVisionEmptyViewStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, View } from 'react-native';
import Button from '../../atoms/Button';
import SyncVisionEmptyBackground from '../SyncVisionEmptyBackground';
import SyncVisionIcon from '../../Icons/SyncVisionIcon';
import { localizer } from '@/shared/constants/Localizer';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    onPressPickMedia?: () => void,
    isAppeared?: boolean,
    loading?: boolean,
} & Partial<StyleProps>;

const SyncVisionEmptyView: React.FC<Props> = ({
    style,
    appTheme,
    onPressPickMedia,
    isAppeared,
    loading,
}) => {
    const styles = React.useMemo(() => new SyncVisionEmptyViewStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <SyncVisionEmptyBackground loading={loading} isAppeared={isAppeared} style={styles.getStyle('wallpaper')} />
            {!loading && (
                <View style={styles.headContainer}>
                    <View style={styles.mediaIcon}>
                        <SyncVisionIcon
                            width={styles.mediaIcon.width}
                            height={styles.mediaIcon.height}
                            color={styles.mediaIcon.color}
                        />
                    </View>
                    <Text style={styles.noMediaText} >
                        {localizer.dictionary.room.show.syncVision.emptyTitle}
                    </Text>
                </View>
            )}
            {!loading && (
                <Button
                    style={styles.getStyle('mediaPickButton')}
                    styleType='fill'
                    onPress={onPressPickMedia}
                    title={localizer.dictionary.room.show.syncVision.selectTitle}
                />
            )}
        </View>
    )
};

export default React.memo(SyncVisionEmptyView, compare);