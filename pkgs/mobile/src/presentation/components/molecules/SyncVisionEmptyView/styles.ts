import { MontserratFont } from '@/shared/constants/Font';
import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: style?.backgroundColor ?? appTheme.room.syncVisionBackground,
        },
        wallpaper: {
            position: 'absolute',
            width: style?.width,
            height: style?.height,
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        mediaPickButton: {
            width: 148,
            height: 40,
            marginTop: 8,
            borderRadius: 22,
            fontSize: 16,
            backgroundColor: appTheme.background.subp2,
            color: appTheme.element.default,
        },
        headContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 8,
        },
        mediaIcon: {
            width: 20,
            height: 20,
            marginRight: 8,
            color: appTheme.element.subp1,
        },
        noMediaText: {
            textAlign: 'left',
            fontFamily: MontserratFont.Bold,
            fontSize: 16,
            color: appTheme.element.subp1,
        },
    })
};

type Styles = typeof styles;

export default class SyncVisionEmptyViewStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};