import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SyncVisionEmptyView from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { boolean } from '@storybook/addon-knobs';
import { Style } from '@/shared/interfaces/Style';

storiesOf('SyncVisionEmptyView', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <SyncVisionEmptyView
            style={new Style({
                width: 380,
                height: 240,
            })}
            isAppeared={boolean('isAppeared', true)}
        />
    ))
    .add('Loading', () => (
        <SyncVisionEmptyView
            style={new Style({
                width: 380,
                height: 240,
            })}
            loading={true}
        />
    ))
