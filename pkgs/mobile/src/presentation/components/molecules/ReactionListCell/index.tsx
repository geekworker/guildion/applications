import React from 'react';
import ReactionListCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

type Props = {
} & Partial<StyleProps>;

const ReactionListCell: React.FC<Props> = ({
    style,
    appTheme,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new ReactionListCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <></>
    );
};

export default React.memo(ReactionListCell, compare);