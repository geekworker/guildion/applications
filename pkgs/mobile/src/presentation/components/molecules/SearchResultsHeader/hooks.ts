import React from 'react';
import SearchResultsHeaderStyles from './styles';
import type SearchResultsHeaderPresenter from './presenter';

namespace SearchResultsHeaderHooks {
    export const useStyles = (props: SearchResultsHeaderPresenter.Input) => {
        const styles = React.useMemo(() => new SearchResultsHeaderStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SearchResultsHeaderPresenter.Input) => {
        return {};
    }
}

export default SearchResultsHeaderHooks;