import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { Animated, Text, View } from 'react-native';
import BackNavigationButton from '../../atoms/BackNavigationButton';
import SearchInput from '../../atoms/SearchInput';
import SearchResultsHeaderPresenter from './presenter';

const SearchResultsHeaderComponent = ({ styles, appTheme, animatedStyle, onPressBack, query, onBlur, onChangeText, onFocus, onSubmitEditing }: SearchResultsHeaderPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <View style={styles.leftButtons}>
                    <BackNavigationButton
                        onPress={onPressBack}
                        label={''}
                        size={styles.backButton.height}
                    />
                </View>
                <SearchInput
                    style={styles.getStyle('search')}
                    value={query}
                    onBlur={onBlur}
                    onChangeText={onChangeText}
                    onFocus={onFocus}
                    onSubmitEditing={onSubmitEditing}
                    placeholder={localizer.dictionary.explore.placeholder}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(SearchResultsHeaderComponent, SearchResultsHeaderPresenter.outputAreEqual);