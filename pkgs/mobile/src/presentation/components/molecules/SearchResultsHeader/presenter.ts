import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import SearchResultsHeaderHooks from './hooks';
import SearchResultsHeaderStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { SearchInputProps } from '../../atoms/SearchInput';

namespace SearchResultsHeaderPresenter {
    export type Input = {
        onPressBack?: () => void,
        query?: string,
        onFocus?: SearchInputProps['onFocus'],
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SearchResultsHeaderStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        onPressBack?: () => void,
        query?: string,
        onFocus?: SearchInputProps['onFocus'],
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SearchResultsHeaderHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default SearchResultsHeaderPresenter;