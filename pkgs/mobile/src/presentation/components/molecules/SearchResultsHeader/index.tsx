import React from 'react';
import SearchResultsHeaderComponent from './component';
import SearchResultsHeaderPresenter from './presenter';

const SearchResultsHeader = (props: SearchResultsHeaderPresenter.Input) => {
    const output = SearchResultsHeaderPresenter.usePresenter(props);
    return <SearchResultsHeaderComponent {...output} />;
};

export default React.memo(SearchResultsHeader, SearchResultsHeaderPresenter.inputAreEqual);