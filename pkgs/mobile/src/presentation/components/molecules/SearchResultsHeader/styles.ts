import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const paddingTop = style?.getAsNumber('paddingTop') || 0;
    const paddingBottom = style?.getAsNumber('paddingBottom') || 0;
    const paddingLeft = style?.getAsNumber('paddingLeft') || 12;
    const paddingRight = style?.getAsNumber('paddingRight') || 12;
    const innerHeight = height - paddingTop - paddingBottom;
    const innerWidth = width - paddingLeft - paddingRight;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            paddingTop,
            paddingBottom,
            paddingLeft,
            paddingRight,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
            shadowColor: style?.shadowColor ?? appTheme.shadow.defaultShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.shadow.defaultShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
            borderBottomWidth: 1,
            borderBottomColor: style?.borderBottomColor ?? appTheme.background.subp1,
        },
        inner: {
            width: innerWidth,
            height: innerHeight,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        leftButtons: {
            height: 44,
            marginLeft: -paddingLeft,
            marginRight: 6,
            marginTop: -12,
        },
        backButton: {
            height: 32,
        },
        search: {
            width: innerWidth - paddingLeft - paddingLeft - paddingRight,
            height: 44,
            marginLeft: -paddingLeft,
        },
    });
};

type Styles = typeof styles;

export default class SearchResultsHeaderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};