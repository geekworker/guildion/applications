import React from 'react';
import PostCellComponent from './component';
import PostCellPresenter from './presenter';

const PostCell = (props: PostCellPresenter.Input) => {
    const output = PostCellPresenter.usePresenter(props);
    return <PostCellComponent {...output} />;
};

export default React.memo(PostCell, PostCellPresenter.inputAreEqual);