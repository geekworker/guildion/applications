import { CURRENT_CONFIG } from '@/shared/constants/Config';
import React from 'react';
import { View, Text } from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import AsyncImage from '../../atoms/AsyncImage';
import IconButton from '../../atoms/IconButton';
import RoomParsedText from '../../atoms/RoomParsedText';
import TouchableText from '../../atoms/TouchableText';
import PostCellPresenter from './presenter';
import FeatherIcon from 'react-native-vector-icons/Feather';
import PostStatsBar from '../PostStatsBar';
import { localizer } from '@/shared/constants/Localizer';
import PostAttachment from '../../atoms/PostAttachment';

const PostCellComponent = ({ styles, appTheme, post, loading, onPressSender, onPressSeeMore, onPressComment, onPressReaction, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom }: PostCellPresenter.Output) => {
    return (
        <View style={styles.container}>
            <View style={styles.inner}>
                <View style={styles.headContainer}>
                    <AsyncImage
                        url={post.sender?.profile?.url ?? ''}
                        style={styles.getStyle('profile')}
                        loading={loading}
                    />
                    {loading ? (
                        <SkeletonContent
                            isLoading={loading ?? false}
                            boneColor={appTheme.skeleton.boneColor}
                            highlightColor={appTheme.skeleton.highlightColor}
                            animationType="pulse"
                            layout={[
                                { ...styles.displayNameSkeleton },
                            ]}
                        />
                    ) : (
                        <TouchableText style={styles.getStyle('displayName')} textStyle={styles.getStyle('displayName')} onPress={onPressSender} numberOfLines={1}>
                            {post.sender?.displayName}
                        </TouchableText>
                    )}
                    {!loading && (
                        <Text style={styles.dateDistanceLabel} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                            {post.getCreatedDistanceNow(CURRENT_CONFIG.DEFAULT_LANG)}
                        </Text>
                    )}
                    {!loading && (
                        <Text style={styles.dateLabel} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                            {post.getCreatedHM(CURRENT_CONFIG.DEFAULT_TIMEZONE)}
                        </Text>
                    )}
                    {!loading && (
                        <IconButton
                            style={styles.getStyle('moreIconButton')}
                            icon={<FeatherIcon color={styles.moreIcon.color} size={styles.moreIcon.width} name={'more-vertical'} />}
                        />
                    )}
                </View>
                {loading && (
                    <SkeletonContent
                        isLoading={loading ?? false}
                        boneColor={appTheme.skeleton.boneColor}
                        highlightColor={appTheme.skeleton.highlightColor}
                        animationType="pulse"
                        layout={[
                            { ...styles.postSkeleton1 },
                        ]}
                    />
                )}
                {loading && (
                    <SkeletonContent
                        isLoading={loading ?? false}
                        boneColor={appTheme.skeleton.boneColor}
                        highlightColor={appTheme.skeleton.highlightColor}
                        animationType="pulse"
                        layout={[
                            { ...styles.postSkeleton2 },
                        ]}
                    />
                )}
                {!loading && post.records.files && post.records.files.size > 0 && (
                    <PostAttachment
                        style={styles.getStyle('attachment')}
                        file={post.records.files.get(0)!}
                        onPress={onPressFile}
                    />
                )}
                {!loading && (
                    <RoomParsedText
                        style={styles.getStyle('post')}
                        text={post.body}
                        onPressURL={onPressURL}
                        onPressEmail={onPressEmail}
                        onPressPhoneNumber={onPressPhoneNumber}
                        onPressMentionMember={onPressMentionMember}
                        onPressMentionRoom={onPressMentionRoom}
                        numberOfLines={3}
                    />
                )}
                {!loading && (
                    <TouchableText onPress={onPressSeeMore} style={styles.getStyle('seeMore')} textStyle={styles.getStyle('seeMoreText')} numberOfLines={1}>
                        {localizer.dictionary.g.seeMore}
                    </TouchableText>
                )}
                {!loading && (
                    <PostStatsBar
                        style={styles.getStyle('bar')}
                        post={post}
                        onPressComment={onPressComment}
                        onPressReaction={onPressReaction}
                    />
                )}
            </View>
        </View>
    );
};

export default React.memo(PostCellComponent, PostCellPresenter.outputAreEqual);