import React from "react";
import PostCellStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type PostCellPresenter from "./presenter";
import { OGP } from "@guildion/core";

namespace PostCellHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new PostCellStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useOnPressSender = (props: PostCellPresenter.Input) => {
        return React.useCallback(() => {
            props.onPressSender && props.post.records.sender && props.onPressSender(props.post.records.sender)
        }, [props.onPressSender, props.post]);
    }

    export const useOnPressSeeMore = (props: PostCellPresenter.Input) => {
        return React.useCallback(() => {
            props.onPressSeeMore && props.post.records.sender && props.onPressSeeMore(props.post)
        }, [props.onPressSeeMore, props.post]);
    }

    export const useOnPressOGP = (props: PostCellPresenter.Input) => {
        return React.useCallback((ogp: OGP) => {
            props.onPressURL && props.onPressURL(ogp.url)
        }, [props.onPressURL]);
    }
}

export default PostCellHooks;