import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    const width: number = (style?.getAsNumber('width') ?? 0) - 40;
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            height: undefined,
            borderTopWidth: style?.borderTopWidth ?? 1,
            borderTopColor: style?.borderTopColor ?? appTheme.background.subp1,
        },
        touchable: {
            width: style?.width,
        },
        inner: {
            width: style?.width,
            height: undefined,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 12,
            paddingLeft: 20,
            paddingRight: 20,
        },
        headContainer: {
            width,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            position: 'relative',
        },
        profile: {
            width: 32,
            height: 32,
            borderRadius: 16,
        },
        displayName: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            color: appTheme.element.default,
            textAlign: 'left',
            maxWidth: width - 150,
            marginLeft: 4,
        },
        displayNameSkeleton: {
            height: 12,
            width: 50,
            maxWidth: width - 150 - 46,
            marginLeft: -width + 50 + 46,
        },
        dateDistanceLabel: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginLeft: 8,
            maxWidth: 100,
        },
        dateDistanceLabelSkeleton: {
            height: 12,
            marginLeft: 8,
            width: 34,
            maxWidth: 100,
        },
        dateLabel: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginLeft: 8,
            maxWidth: 34,
        },
        dateLabelSkeleton: {
            height: 12,
            marginLeft: 8,
            width: 34,
            maxWidth: 34,
        },
        moreIconButton: {
            width: 24,
            height: 24,
            position: 'absolute',
            right: -24,
            top: 4,
            borderRadius: 12,
        },
        moreIcon: {
            width: 14,
            height: 14,
            color: appTheme.element.subp1,
        },
        post: {
            marginTop: 12,
            width,
        },
        postSkeleton1: {
            marginTop: 10,
            height: 16,
            width,
        },
        postSkeleton2: {
            marginTop: 10,
            marginBottom: 20,
            height: 16,
            width: width / 3 * 2,
            marginLeft: -width / 3
        },
        seeMore: {
            width,
            marginTop: 4,
            marginBottom: 12,
        },
        seeMoreText: {
            width,
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'left',
            textDecorationLine: 'underline',
        },
        bar: {
            width: style?.width,
        },
        ogps: {
            paddingTop: 4,
            paddingBottom: 4,
            width: width,
        },
        attachments: {
            paddingTop: 4,
            paddingBottom: 4,
            width: width,
        },
        attachment: {
            marginTop: 12,
            marginBottom: 4,
            width: width,
        },
    })
};

type Styles = typeof styles;

export default class PostCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};