import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import PostCellHooks from "./hooks";
import PostCellStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Post } from "@guildion/core";

namespace PostCellPresenter {
    export type Input = {
        post: Post,
        loading?: boolean,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PostCellStyles,
        appTheme: AppTheme,
        post: Post,
        loading?: boolean,
        onPressSender?: () => void,
        onPressSeeMore?: () => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PostCellHooks.useStyles({ ...props });
        const onPressSender = PostCellHooks.useOnPressSender(props);
        const onPressSeeMore = PostCellHooks.useOnPressSeeMore(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPressSender,
            onPressSeeMore,
        }
    }
}

export default PostCellPresenter;