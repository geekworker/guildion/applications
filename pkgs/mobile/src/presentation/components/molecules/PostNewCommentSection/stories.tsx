import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PostNewCommentSection from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, Member } from '@guildion/core';

storiesOf('PostNewCommentSection', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PostNewCommentSection
            style={new Style({
                width: 380,
                height: 44,
            })}
            currentMember={new Member({ profile: Files.getSeed().generateUserProfile().toJSON() })}
        />
    ))