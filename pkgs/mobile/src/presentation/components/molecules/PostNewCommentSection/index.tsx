import React from 'react';
import PostNewCommentSectionStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Text, View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import { Member } from '@guildion/core';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { localizer } from '@/shared/constants/Localizer';

type Props = {
    currentMember: Member,
    onPress?: () => void,
} & Partial<StyleProps>;

const PostNewCommentSection: React.FC<Props> = ({
    style,
    appTheme,
    currentMember,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new PostNewCommentSectionStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <AsyncImage style={styles.getStyle('profile')} url={currentMember.profile?.url ?? ''} />
            <TouchableHighlight style={styles.getStyle('barTouchable')} onPress={onPress}>
                <View style={styles.barInner}>
                    <Text style={styles.barPlaceHolder} numberOfLines={1}>
                        {localizer.dictionary.room.posts.new.placeholder}
                    </Text>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(PostNewCommentSection, compare);