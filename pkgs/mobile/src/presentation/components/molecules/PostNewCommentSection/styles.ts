import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const height = (style?.getAsNumber('height') ?? 0);
    const innerWidth = (style?.getAsNumber('width') ?? 0) - 40;
    const innerHeight = height - 32;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 16,
            paddingBottom: 16,
            paddingLeft: 20,
            paddingRight: 20,
            // borderTopWidth: style?.borderTopWidth ?? 1,
            // borderTopColor: style?.borderTopColor ?? appTheme.background.subp1,
            // borderBottomWidth: style?.borderBottomWidth ?? 1,
            // borderBottomColor: style?.borderBottomColor ?? appTheme.background.subp1,
        },
        profile: {
            width: innerHeight,
            height: innerHeight,
            borderRadius: innerHeight / 2,
            marginRight: 8,
        },
        barTouchable: {
            width: innerWidth - (innerHeight + 8),
            height: innerHeight,
            borderRadius: 5,
        },
        barInner: {
            width: innerWidth - (innerHeight + 8),
            height: innerHeight,
            borderRadius: 5,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingLeft: 12,
            paddingRight: 12,
            backgroundColor: appTheme.background.subp1,
        },
        barPlaceHolder: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            textAlign: 'left',
            color: appTheme.element.subp1,
        },
    })
};

type Styles = typeof styles;

export default class PostNewCommentSectionStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};