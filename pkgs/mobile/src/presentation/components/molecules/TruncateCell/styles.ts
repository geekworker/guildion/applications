import { MontserratFont } from '@/shared/constants/Font';
import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: style?.borderWidth ?? 1,
            borderColor: style?.borderColor ?? appTheme.room.cellBackground,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subp2,
        },
        icon: {
            color: appTheme.element.subp1,
            width: 10,
            height: 10,
            marginRight: 1,
        },
        count: {
            color: appTheme.element.subp1,
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
        },
    })
};

type Styles = typeof styles;

export default class TruncateCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};