import React from 'react';
import TruncateCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, View } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    count: number,
} & Partial<StyleProps>;

const TruncateCell: React.FC<Props> = ({
    style,
    appTheme,
    count,
}) => {
    const styles = React.useMemo(() => new TruncateCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <EntypoIcon name={'plus'} size={styles.icon.width} color={styles.icon.color} />
            <Text style={styles.count} numberOfLines={1} minimumFontScale={0.5} adjustsFontSizeToFit>
                {count}
            </Text>
        </View>
    )
};

export default React.memo(TruncateCell, compare);