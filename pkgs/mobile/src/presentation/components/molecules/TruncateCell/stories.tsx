import { storiesOf } from '@storybook/react-native';
import React from 'react';
import TruncateCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('TruncateCell', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <TruncateCell
            count={10}
            style={new Style({
                width: 44,
                borderRadius: 22,
                height: 44,
            })}
        />
    ))
