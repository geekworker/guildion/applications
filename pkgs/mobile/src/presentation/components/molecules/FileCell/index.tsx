import React from 'react';
import FileCellComponent from './component';
import FileCellPresenter from './presenter';

const FileCell = (props: FileCellPresenter.Input) => {
    const output = FileCellPresenter.usePresenter(props);
    return <FileCellComponent {...output} />;
};

export default React.memo(FileCell, FileCellPresenter.inputAreEqual);