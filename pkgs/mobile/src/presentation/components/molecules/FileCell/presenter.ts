import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import FileCellHooks from "./hooks";
import FileCellStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File } from "@guildion/core";

namespace FileCellPresenter {
    export type Input = {
        file: File,
        onPress?: (file: File) => void,
        onPressFolder?: (file: File) => void,
        onPressMenu?: (file: File) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: FileCellStyles,
        appTheme: AppTheme,
        file: File,
        onPress: () => void,
        onPressFolder: () => void,
        onPressMenu?: () => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = FileCellHooks.useStyles({ ...props });
        const onPress = FileCellHooks.useOnPress(props);
        const onPressFolder = FileCellHooks.useOnPressFolder(props);
        const onPressMenu = FileCellHooks.useOnPressMenu(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress,
            onPressFolder,
            onPressMenu,
        }
    }
}

export default FileCellPresenter;