import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = (style?.getAsNumber('width') ?? 0) - ((style?.getAsNumber('paddingLeft') || 10) + (style?.getAsNumber('paddingRight') || 10));
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: 0,
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
        },
        touchable: {
            width: style?.width,
            position: 'relative',
        },
        inner: {
            width: style?.width,
            position: 'relative',
            paddingLeft: style?.paddingLeft ?? 10,
            paddingRight: style?.paddingRight ?? 10,
            paddingTop: style?.paddingTop ?? 10,
            paddingBottom: style?.paddingBottom ?? 10,
        },
        thumbnailContainer: {
            borderRadius: style?.borderRadius ?? 20,
            width,
            height: (width / 2) + 10,
            position: 'relative',
        },
        thumbnail: {
            borderRadius: style?.borderRadius ?? 20,
            width,
            height: (width / 2) + 10,
        },
        folderCoverContainer: {
            borderRadius: style?.borderRadius ?? 20,
            width,
            height: (width / 2) + 10,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            paddingLeft: 40,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'flex-start',
        },
        folderCounter: {
            fontFamily: MontserratFont.Bold,
            fontSize: 32,
            color: appTheme.element.default,
            textAlign: 'left',
            marginLeft: 4,
        },
        folderIcon: {
            paddingTop: 12,
            color: appTheme.element.default,
            width: 28,
            height: 28,
        },
        duration: {
            position: 'absolute',
            bottom: 10,
            right: 10,
            height: 30,
            borderRadius: 15,
        },
        bodyContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            width,
            paddingTop: 4,
            paddingBottom: 0,
        },
        leftBody: {
            width: width - 24,
        },
        rightBody: {
            width: 24,
        },
        title: {
            marginBottom: 4,
            fontFamily: MontserratFont.Bold,
            fontSize: 16,
            lineHeight: 26,
            textAlign: 'left',
            color: appTheme.element.default,
        },
        featuringContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        featuringProfile: {
            width: 24,
            height: 24,
            borderRadius: 5,
        },
        featuringName: {
            marginLeft: 8,
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.element.subp1,
        },
        moreIconButton: {
            width: 24,
            height: 24,
            borderRadius: 12,
        },
        moreIcon: {
            width: 14,
            height: 14,
            color: appTheme.element.subp1,
        },
    })
};

type Styles = typeof styles;

export default class FileCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};