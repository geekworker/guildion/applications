import { storiesOf } from '@storybook/react-native';
import React from 'react';
import FileCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, FileType } from '@guildion/core';

storiesOf('FileCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <FileCell
            style={new Style({
                width: 380,
            })}
            file={Files.getSeed().generateUserProfile().setRecord('folder', Files.getSeed().generateUserProfile())}
        />
    ))
    .add('Video', () => (
        <FileCell
            style={new Style({
                width: 380,
            })}
            file={
                Files
                    .getSeed()
                    .generateUserProfile()
                    .setRecord('folder', Files.getSeed().generateUserProfile())
                    .set('type', FileType.Video)
                    .set('durationMs', 1200000)
                    .setRecord('thumbnail', Files.getSeed().generateUserProfile())
            }
        />
    ))