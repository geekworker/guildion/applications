import React from 'react';
import { View, TouchableHighlight, Text } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import FileCellPresenter from './presenter';
import IconButton from '../../atoms/IconButton';
import FeatherIcon from 'react-native-vector-icons/Feather';
import TouchableText from '../../atoms/TouchableText';
import { FileType } from '@guildion/core';
import DurationLabel from '../../atoms/DurationLabel';
import LinearGradient from 'react-native-linear-gradient';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

const FileCellComponent = ({ styles, appTheme, file, onPress, onPressFolder, onPressMenu, loading }: FileCellPresenter.Output) => {
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}>
                    <View style={styles.thumbnailContainer}>
                        <AsyncImage url={file.thumbnailUrl} loading={loading} onPress={onPress} style={styles.getStyle('thumbnail')} />
                        {file.type === FileType.Video && file.durationMs && (
                            <DurationLabel
                                durationMs={file.durationMs}
                                style={styles.getStyle('duration')}
                            />
                        )}
                        {file.type === FileType.Album && (
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={[
                                    'rgba(0, 0, 0, 0.8)',
                                    'rgba(0, 0, 0, 0)',
                                ]}
                                style={styles.folderCoverContainer}
                            >
                                <Text style={styles.folderCounter}>
                                    {file.filesCount}
                                </Text>
                                <MaterialIcon color={styles.folderIcon.color} size={styles.folderIcon.width} name={'photo-library'} />
                            </LinearGradient>
                        )}
                        {file.type === FileType.Playlist && (
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 0 }}
                                colors={[
                                    'rgba(0, 0, 0, 0.8)',
                                    'rgba(0, 0, 0, 0)',
                                ]}
                                style={styles.folderCoverContainer}
                            >
                                <Text style={styles.folderCounter}>
                                    {file.filesCount}
                                </Text>
                                <EntypoIcon color={styles.folderIcon.color} size={styles.folderIcon.width} name={'folder-video'} />
                            </LinearGradient>
                        )}
                    </View>
                    <View style={styles.bodyContainer}>
                        <View style={styles.leftBody}>
                            {!loading && (
                                <TouchableText textStyle={styles.getStyle('title')} onPress={onPress} numberOfLines={2}>
                                    {file.displayName}
                                </TouchableText>
                            )}
                            {file.records.folder && !loading && (
                                <View style={styles.featuringContainer}>
                                    <AsyncImage url={file.records.folder.thumbnailUrl} loading={loading} onPress={onPressFolder} style={styles.getStyle('featuringProfile')} />
                                    <TouchableText numberOfLines={1} textStyle={styles.getStyle('featuringName')} onPress={onPressFolder}>
                                        {file.records.folder.displayName}
                                    </TouchableText>
                                </View>
                            )}
                        </View>
                        <View style={styles.rightBody}>
                            {!loading && (
                                <IconButton
                                    style={styles.getStyle('moreIconButton')}
                                    icon={<FeatherIcon color={styles.moreIcon.color} size={styles.moreIcon.width} name={'more-vertical'} />}
                                    onPress={onPressMenu}
                                />
                            )}
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(FileCellComponent, FileCellPresenter.outputAreEqual);