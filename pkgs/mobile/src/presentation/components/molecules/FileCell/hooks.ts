import React from "react";
import FileCellStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type FileCellPresenter from "./presenter";

namespace FileCellHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new FileCellStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useOnPress = (props: FileCellPresenter.Input) => {
        const onPress = React.useCallback(() => {
            props.onPress && props.onPress(props.file);
        }, [props.file, props.onPress]);
        return onPress;
    }

    export const useOnPressMenu = (props: FileCellPresenter.Input) => {
        const onPress = React.useCallback(() => {
            props.onPress && props.onPress(props.file);
        }, [props.file, props.onPress]);
        return onPress;
    }

    export const useOnPressFolder = (props: FileCellPresenter.Input) => {
        const onPress = React.useCallback(() => {
            props.onPress && props.file.records.folder && props.onPress(props.file.records.folder);
        }, [props.file, props.onPress]);
        return onPress;
    }
}

export default FileCellHooks;