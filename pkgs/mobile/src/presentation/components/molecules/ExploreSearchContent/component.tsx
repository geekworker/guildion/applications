import { localizer } from '@/shared/constants/Localizer';
import { BlurView } from '@react-native-community/blur';
import React from 'react';
import { Animated, Text } from 'react-native';
import SearchInput from '../../atoms/SearchInput';
import ExploreSearchContentPresenter from './presenter';

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView);

const ExploreSearchContentComponent = ({ styles, appTheme, animatedStyle, transition, onFocus, onChangeText, onSubmitEditing, onBlur, query }: ExploreSearchContentPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <Animated.View
                style={{
                    ...styles.inner,
                    shadowColor: transition?.interpolate({
                        inputRange: [0, 1],
                        outputRange: [styles.inner.shadowColor, String(styles.animatedInner.shadowColor)],
                    }) ?? styles.inner.shadowColor,
                    shadowOpacity: transition?.interpolate({
                        inputRange: [0, 1],
                        outputRange: [styles.inner.shadowOpacity, styles.animatedInner.shadowOpacity],
                    }) ?? styles.inner.shadowOpacity,
                    shadowRadius: transition?.interpolate({
                        inputRange: [0, 1],
                        outputRange: [styles.inner.shadowRadius, styles.animatedInner.shadowRadius],
                    }) ?? styles.inner.shadowRadius,
                    borderBottomWidth: transition?.interpolate({
                        inputRange: [0, 1],
                        outputRange: [styles.inner.borderBottomWidth, styles.animatedInner.borderBottomWidth],
                    }) ?? styles.inner.borderBottomWidth,
                    borderBottomColor: transition?.interpolate({
                        inputRange: [0, 1],
                        outputRange: [styles.inner.borderBottomColor, String(styles.animatedInner.borderBottomColor)],
                    }) ?? styles.inner.borderBottomColor,
                    backgroundColor: transition?.interpolate({
                        inputRange: [0, 1],
                        outputRange: [String(styles.inner.backgroundColor), String(styles.animatedInner.backgroundColor)],
                    }) ?? styles.inner.backgroundColor,
                }}
            >
                <AnimatedBlurView
                    style={{ ...styles.filter, opacity: transition }}
                    blurType='dark'
                    blurAmount={10}
                />
                <Text style={styles.title}>
                    {localizer.dictionary.explore.name}
                </Text>
                <SearchInput
                    style={styles.getStyle('search')}
                    onFocus={onFocus}
                    placeholder={localizer.dictionary.explore.placeholder}
                    onChangeText={onChangeText}
                    onBlur={onBlur}
                    onSubmitEditing={onSubmitEditing}
                    value={query}
                />
            </Animated.View>
        </Animated.View>
    );
};

export default React.memo(ExploreSearchContentComponent, ExploreSearchContentPresenter.outputAreEqual);