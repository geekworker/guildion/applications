import { storiesOf } from '@storybook/react-native';
import React from 'react';
import ExploreSearchContent from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Animated } from 'react-native';

storiesOf('ExploreSearchContent', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView style={{ backgroundColor: fallbackAppTheme.background.root }}>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <ExploreSearchContent
            style={new Style({
                width: 370,
            })}
        />
    ))
    .add('Animated', () => (
        <ExploreSearchContent
            style={new Style({
                width: 370,
            })}
            transition={new Animated.Value(1)}
        />
    ))