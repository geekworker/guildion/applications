import React from 'react';
import ExploreSearchContentStyles from './styles';
import type ExploreSearchContentPresenter from './presenter';

namespace ExploreSearchContentHooks {
    export const useStyles = (props: ExploreSearchContentPresenter.Input) => {
        const styles = React.useMemo(() => new ExploreSearchContentStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: ExploreSearchContentPresenter.Input) => {
        return {};
    }
}

export default ExploreSearchContentHooks;