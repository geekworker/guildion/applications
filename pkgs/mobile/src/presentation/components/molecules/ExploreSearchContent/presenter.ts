import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import ExploreSearchContentHooks from './hooks';
import ExploreSearchContentStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Animated } from 'react-native';
import { SearchInputProps } from '../../atoms/SearchInput';

namespace ExploreSearchContentPresenter {
    export type Input = {
        transition?: Animated.Value | Animated.AnimatedInterpolation,
        onFocus?: () => void,
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
        query?: string,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: ExploreSearchContentStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
        onFocus?: () => void,
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
        query?: string,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = ExploreSearchContentHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default ExploreSearchContentPresenter;