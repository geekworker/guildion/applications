import React from 'react';
import ExploreSearchContentComponent from './component';
import ExploreSearchContentPresenter from './presenter';

const ExploreSearchContent = (props: ExploreSearchContentPresenter.Input) => {
    const output = ExploreSearchContentPresenter.usePresenter(props);
    return <ExploreSearchContentComponent {...output} />;
};

export default React.memo(ExploreSearchContent, ExploreSearchContentPresenter.inputAreEqual);