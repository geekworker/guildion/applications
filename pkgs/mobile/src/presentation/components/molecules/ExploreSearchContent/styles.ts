import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const paddingLeft = style?.getAsNumber('paddingLeft') || 20;
    const paddingRight = style?.getAsNumber('paddingRight') || 20;
    const innerWidth = width - paddingLeft - paddingRight;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            paddingLeft: 0,
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            padding: 0,
        },
        inner: {
            width,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
            paddingLeft,
            paddingRight,
            paddingTop: style?.paddingTop ?? 8,
            paddingBottom: style?.paddingBottom ?? 8,
            shadowColor: appTheme.common.transparent,
            shadowOpacity: 0,
            shadowRadius: 0,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
            borderBottomWidth: 0,
            borderBottomColor: appTheme.common.transparent,
        },
        animatedInner: {
            width,
            backgroundColor: appTheme.common.transparent,
            paddingLeft: style?.paddingLeft ?? 20,
            paddingRight: style?.paddingRight ?? 20,
            paddingTop: style?.paddingTop ?? 8,
            paddingBottom: style?.paddingBottom ?? 8,
            shadowColor: style?.shadowColor ?? appTheme.shadow.defaultShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.shadow.defaultShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
            borderBottomWidth: 1,
            borderBottomColor: style?.borderBottomColor ?? appTheme.background.subp1,
            overflow: 'hidden',
            position: 'relative',
        },
        filter: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            width,
            backgroundColor: appTheme.common.overlayBackgroundColorStrong,
        },
        title: {
            fontFamily: MontserratFont.Bold,
            fontSize: 24,
            textAlign: 'left',
            color: appTheme.element.default,
            marginBottom: 12,
            width: innerWidth,
        },
        search: {
            width: innerWidth,
            height: 44,
            marginBottom: 8,
        },
    });
};

type Styles = typeof styles;

export default class ExploreSearchContentStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};