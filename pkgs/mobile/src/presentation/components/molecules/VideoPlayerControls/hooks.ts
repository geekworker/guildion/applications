import React from "react";
import VideoPlayerControlsStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace VideoPlayerControlsHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new VideoPlayerControlsStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default VideoPlayerControlsHooks;