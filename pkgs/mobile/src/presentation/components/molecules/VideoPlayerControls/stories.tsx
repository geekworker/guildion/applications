import { storiesOf } from '@storybook/react-native';
import React from 'react';
import VideoPlayerControls from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { File } from '@guildion/core';

storiesOf('VideoPlayerControls', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <VideoPlayerControls
            style={new Style({
                width: 360,
            })}
            file={new File({
                displayName: 'test',
                durationMs: 60000,
            })}
            durationMs={10000}
        />
    ))