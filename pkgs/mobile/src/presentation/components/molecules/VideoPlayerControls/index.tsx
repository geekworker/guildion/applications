import React from 'react';
import VideoPlayerControlsComponent from './component';
import VideoPlayerControlsPresenter from './presenter';

const VideoPlayerControls = (props: VideoPlayerControlsPresenter.Input) => {
    const output = VideoPlayerControlsPresenter.usePresenter(props);
    return <VideoPlayerControlsComponent {...output} />;
};

export default React.memo(VideoPlayerControls, VideoPlayerControlsPresenter.inputAreEqual);