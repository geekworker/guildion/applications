import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import VideoPlayerControlsHooks from "./hooks";
import VideoPlayerControlsStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File } from "@guildion/core";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";
import { Animated } from "react-native";

namespace VideoPlayerControlsPresenter {
    export type Input = {
        durationMs: number,
        file: File,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: VideoPlayerControlsStyles,
        appTheme: AppTheme,
        animatedStyle?: AnimatedStyle,
        durationMs: number,
        file: File,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = VideoPlayerControlsHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default VideoPlayerControlsPresenter;