import React from 'react';
import { Animated, Text, View } from 'react-native';
import VideoPlayerControlsPresenter from './presenter';
import { formatMs } from '@guildion/core';
import SeekSlider from '../../atoms/SeekSlider';

const VideoPlayerControlsComponent = ({ styles, appTheme, animatedStyle, file, durationMs, transition }: VideoPlayerControlsPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <Animated.View style={{ ...styles.inner }}>
                <Animated.Text style={{ ...styles.name, opacity: transition }}>
                    {file.displayName}
                </Animated.Text>
                <Animated.View style={{ ...styles.durationContainer, opacity: transition }}>
                    <Text style={styles.currentDuration}>
                        {formatMs(durationMs)}
                    </Text>
                    <Text style={styles.borderDuration}>
                        /
                    </Text>
                    <Text style={styles.maxDuration}>
                        {formatMs(file.durationMs ?? durationMs)}
                    </Text>
                </Animated.View>
                <View style={styles.sliderContainer}>
                    <SeekSlider
                        style={styles.getStyle('slider')}
                        value={durationMs}
                        minValue={0}
                        maxValue={file.durationMs ?? durationMs}
                        transition={transition}
                    />
                </View>
            </Animated.View>
        </Animated.View>
    );
};

export default React.memo(VideoPlayerControlsComponent, VideoPlayerControlsPresenter.outputAreEqual);