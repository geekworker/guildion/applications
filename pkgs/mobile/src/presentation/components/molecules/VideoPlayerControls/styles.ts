import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') ?? 0;
    const height = style?.getAsNumber('height') ?? 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        inner: {
            width,
            position: 'relative',
        },
        name: {
            width,
            fontFamily: MontserratFont.SemiBold,
            fontSize: 14,
            textAlign: 'left',
            lineHeight: 20,
            color: appTheme.element.default,
        },
        durationContainer: {
            marginTop: 8,
            width,
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        currentDuration: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            textAlign: 'left',
            color: appTheme.element.default,
        },
        borderDuration: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            textAlign: 'left',
            color: appTheme.element.subp15,
            marginLeft: 2,
            marginRight: 2,
        },
        maxDuration: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            textAlign: 'left',
            color: appTheme.element.subp15,
        },
        sliderContainer: {
            width,
            position: 'relative',
        },
        slider: {
            width,
            height: 6,
            paddingTop: 16,
            paddingBottom: 20,
        },
    })
};

type Styles = typeof styles;

export default class VideoPlayerControlsStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};