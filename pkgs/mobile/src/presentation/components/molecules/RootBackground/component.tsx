import { View } from 'react-native';
import React from 'react';
import RootBackgroundPresenter from './presenter';
import TopMosts from '../TopMosts';

const RootBackgroundComponent = ({ children, styles, isShown, foregrondStyle, backgroundStyle, containerStyle }: RootBackgroundPresenter.Output) => {
    return (
        <View style={{ ...styles.container, ...containerStyle?.toStyleObject() }}>
            {isShown && (
                <View style={{ ...styles.background, ...backgroundStyle?.toStyleObject() }}>
                </View>
            )}
            <View style={{ ...styles.foreground, ...foregrondStyle?.toStyleObject() }}>
                {children}
            </View>
            <TopMosts/>
        </View>
    )
};

export default React.memo(RootBackgroundComponent, RootBackgroundPresenter.outputAreEqual);