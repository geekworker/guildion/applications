import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet, ScaledSize } from 'react-native';
import container from './container';

const styles = ({ window, appTheme, containerStyle }: { window: ScaledSize, appTheme: AppTheme, containerStyle?: Style }) => {
    return StyleSheet.create({
        container: {
            width: containerStyle?.width ?? window.width,
            height: containerStyle?.height ?? window.height,
            position: 'relative',
            backgroundColor: appTheme.background.root,
        },
        background: {
            width: containerStyle?.width ?? window.width,
            height: containerStyle?.height ?? window.height,
            position: 'relative',
            backgroundColor: 'transparent',
        },
        foreground: {
            position: 'absolute',
            left: 0,
            right: 0,
            width: containerStyle?.width ?? window.width,
            height: containerStyle?.height ?? window.height,
            backgroundColor: 'transparent',
        },
    });
}

type Styles = typeof styles;

export default class RootBackgroundStyles extends Record<ReturnType<Styles>>({
    ...styles({ appTheme: fallbackAppTheme, window: { width: 0, height: 0, fontScale: 0, scale: 0 } })
}) implements StylesImpl<Styles> {
    constructor({ window, appTheme, containerStyle }: { window: ScaledSize, appTheme: AppTheme, containerStyle?: Style }) {
        super(styles({ window, appTheme, containerStyle }));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};