import RootRedux from '@/infrastructure/Root/redux';
import { useReducer as reactUseReducer, useContext, useMemo } from 'react';
import RootBackgroundStyles from './styles';
import RootBackgroundRedux from './redux';
import { useWindowDimensions } from 'react-native';
import type RootBackgroundPresenter from './presenter';

namespace RootBackgroundHooks {
    export const useReducer = () => {
        const [state, dispatch] = reactUseReducer(
            RootBackgroundRedux.reducer,
            RootBackgroundRedux.initialState,
        );
        return {
            state,
            dispatch
        }
    }

    export const useStyles = (props: RootBackgroundPresenter.Input): RootBackgroundStyles => {
        const rootContext = useContext(RootRedux.Context);
        const window = useWindowDimensions();
        const appTheme = rootContext.state.get('appTheme');
        const styles = useMemo(() => new RootBackgroundStyles({ window, appTheme, containerStyle: props.containerStyle }), [window, appTheme, props.containerStyle]);
        return styles;
    }
}

export default RootBackgroundHooks;