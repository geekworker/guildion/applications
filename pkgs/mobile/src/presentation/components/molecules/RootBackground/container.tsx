import React from 'react';
import RootBackgroundComponent from './component';
import RootBackgroundPresenter from './presenter';

const RootBackgroundContainer = (props: RootBackgroundPresenter.Input & RootBackgroundPresenter.Payload) => {
    const output = RootBackgroundPresenter.usePresenter(props);
    return <RootBackgroundComponent {...output} />;
};

export default React.memo(RootBackgroundContainer, RootBackgroundPresenter.inputAreEqual);