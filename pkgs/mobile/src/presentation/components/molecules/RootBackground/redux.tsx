import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Record } from 'immutable';
import { Action as ReduxAction } from "redux";

namespace RootBackgroundRedux {

    export type FixedComponent = { component?: React.ReactNode | null, id: string };
    export class State extends Record<{
        isShown: boolean,
    }>({
        isShown: false,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        show: actionCreator<{}>('SHOW'),
        hide: actionCreator<{}>('HIDE'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.show, (state) => {
            state = state.set('isShown', true);
            return state;
        })
        .case(Action.hide, (state) => {
            state = state.set('isShown', false);
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });
}

export default RootBackgroundRedux