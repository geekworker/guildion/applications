import React from 'react';
import RootBackgroundRedux from './redux';
import RootBackgroundPresenter from './presenter';
import RootBackgroundContainer from './container';

const RootBackground = (props: RootBackgroundPresenter.Input) => {
    const payload = RootBackgroundPresenter.usePayload(props);
    return (
        <RootBackgroundRedux.Context.Provider value={{ state: payload.state, dispatch: payload.dispatch }} >
            <RootBackgroundContainer { ...payload } { ...props } />
        </RootBackgroundRedux.Context.Provider>
    );
};

export default React.memo(RootBackground, RootBackgroundPresenter.inputAreEqual);
