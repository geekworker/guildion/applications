import React, { useContext } from "react";
import { AnyAction } from "redux";
import RootBackgroundHooks from "./hooks";
import RootBackgroundRedux from "./redux";
import RootBackgroundStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { Style } from "@/shared/interfaces/Style";

namespace RootBackgroundPresenter {
    
    export type Input = {
        children?: React.ReactNode,
        containerStyle?: Style,
        foregrondStyle?: Style,
        backgroundStyle?: Style,
    }
    
    export type Output = {
        styles: RootBackgroundStyles,
        isShown: boolean,
    } & Input;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export type Payload = {
        state: RootBackgroundRedux.State,
        dispatch: React.Dispatch<AnyAction>,
    }
    
    export function usePayload(props: Input): Payload {
        const { state, dispatch } = RootBackgroundHooks.useReducer();
        return {
            state,
            dispatch
        }
    }
    
    export function usePresenter(props: Input & Payload): Output {
        const styles = RootBackgroundHooks.useStyles(props);
        const context = useContext(RootBackgroundRedux.Context);
        return {
            ...props,
            ...context.state.toJSON(),
            styles,
        }
    }
}

export default RootBackgroundPresenter;