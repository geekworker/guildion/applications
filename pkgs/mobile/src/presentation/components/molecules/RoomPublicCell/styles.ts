import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const borderRadius = style?.getAsNumber('borderRadius') || 5;
    const paddingTop = style?.paddingTop ? Number(style?.paddingTop) : 20 ;
    const paddingLeft = style?.paddingLeft ? Number(style?.paddingLeft) : 20 ;
    const paddingRight = style?.paddingRight ? Number(style?.paddingRight) : 20 ;
    const paddingBottom = style?.paddingBottom ? Number(style?.paddingBottom) : 20 ;
    const bodyContainerWidth = width - paddingLeft - paddingRight - 24 - 44 - 72;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            borderRadius,
            backgroundColor: style?.backgroundColor ?? appTheme.room.cellBackground,
            shadowColor: style?.shadowColor ?? appTheme.room.cellShadowColor,
            shadowOpacity: style?.shadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            elevation: style?.elevation ?? 4,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        touchable: {
            width,
            borderRadius,
        },
        inner: {
            width,
            borderRadius,
            paddingTop,
            paddingLeft,
            paddingRight,
            paddingBottom,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        profileContainer: {
            position: 'relative',
            width: 44,
            height: 44,
        },
        profile: {
            position: 'relative',
            width: 44,
            height: 44,
            borderRadius: 5,
        },
        bodyContainer: {
            position: 'relative',
            marginLeft: 12,
            marginRight: 12,
            width: bodyContainerWidth,
        },
        displayName: {
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'left',
            fontSize: 16,
            marginBottom: 6,
        },
        displayNameSkeleton: {
            textAlign: 'left',
            height: 16,
            marginBottom: 6,
            width: 100,
            marginLeft: -(bodyContainerWidth - 100) / 2 - 32,
        },
        contentsContainer: {
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        connectingLabel: {
        },
        postsCount: {
        },
        postsCountSkeleton: {
            textAlign: 'left',
            height: 16,
            marginBottom: 6,
            width: 60,
            marginLeft: -(bodyContainerWidth - 60) / 2 - 50,
        },
        memberStatus: {
            width: 72,
        },
        join: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            textAlign: 'right',
            color: appTheme.element.main,
        },
        member: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            textAlign: 'right',
            color: appTheme.button.disabled,
        },
        buttonSkeleton: {
            height: 16,
            width: 60,
        },
    });
};

type Styles = typeof styles;

export default class RoomPublicCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};