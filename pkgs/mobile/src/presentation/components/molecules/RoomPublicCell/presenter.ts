import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomPublicCellHooks from './hooks';
import RoomPublicCellStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Room } from '@guildion/core';

namespace RoomPublicCellPresenter {
    export type Input = {
        onPress?: (room: Room) => void,
        onPressJoin?: (room: Room) => void,
        onPressLeave?: (room: Room) => void,
        room: Room,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomPublicCellStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        onPress: () => void,
        onPressJoin?: () => void,
        onPressLeave?: () => void,
        room: Room,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomPublicCellHooks.useStyles({ ...props });
        const {
            onPress,
            onPressJoin,
            onPressLeave,
        } = RoomPublicCellHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress,
            onPressJoin,
            onPressLeave,
        }
    }
}

export default RoomPublicCellPresenter;