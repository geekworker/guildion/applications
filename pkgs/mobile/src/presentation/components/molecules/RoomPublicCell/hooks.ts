import React from 'react';
import RoomPublicCellStyles from './styles';
import type RoomPublicCellPresenter from './presenter';

namespace RoomPublicCellHooks {
    export const useStyles = (props: RoomPublicCellPresenter.Input) => {
        const styles = React.useMemo(() => new RoomPublicCellStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomPublicCellPresenter.Input) => {
        const onPress = React.useCallback(() => {
            if (props.onPress) props.onPress(props.room);
        }, [props.room, props.onPress]);
        const onPressJoin = React.useCallback(() => {
            if (props.onPressJoin) props.onPressJoin(props.room);
        }, [props.room, props.onPressJoin]);
        const onPressLeave = React.useCallback(() => {
            if (props.onPressLeave) props.onPressLeave(props.room);
        }, [props.room, props.onPressLeave]);
        return {
            onPress,
            onPressJoin,
            onPressLeave,
        };
    }
}

export default RoomPublicCellHooks;