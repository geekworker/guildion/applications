import React from 'react';
import RoomPublicCellComponent from './component';
import RoomPublicCellPresenter from './presenter';

const RoomPublicCell = (props: RoomPublicCellPresenter.Input) => {
    const output = RoomPublicCellPresenter.usePresenter(props);
    return <RoomPublicCellComponent {...output} />;
};

export default React.memo(RoomPublicCell, RoomPublicCellPresenter.inputAreEqual);