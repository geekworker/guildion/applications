import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomPublicCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Member, Members, Message, Room, RoomActivity, RoomLog, Files } from '@guildion/core';
import { number, text } from '@storybook/addon-knobs';

storiesOf('RoomPublicCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomPublicCell
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                profile: Files.getSeed().generateRoomProfile().toJSON(),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    connectingMembers: new Members([
                        new Member({
                            id: 'test1',
                            displayName: 'test1',
                        }).toJSON(),
                        new Member({
                            id: 'test2',
                            displayName: 'test2',
                        }).toJSON(),
                        new Member({
                            id: 'test3',
                            displayName: 'test3',
                        }).toJSON(),
                        new Member({
                            id: 'test4',
                            displayName: 'test4',
                        }).toJSON(),
                    ]).toJSON(),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            style={new Style({
                width: 380,
            })}
        />
    ))
    .add('Loading', () => (
        <RoomPublicCell
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    connectingMembers: new Members([
                        new Member({
                            id: 'test1',
                            displayName: 'test1',
                        }).toJSON(),
                        new Member({
                            id: 'test2',
                            displayName: 'test2',
                        }).toJSON(),
                        new Member({
                            id: 'test3',
                            displayName: 'test3',
                        }).toJSON(),
                        new Member({
                            id: 'test4',
                            displayName: 'test4',
                        }).toJSON(),
                    ]).toJSON(),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            style={new Style({
                width: 380,
            })}
            loading={true}
        />
    ))