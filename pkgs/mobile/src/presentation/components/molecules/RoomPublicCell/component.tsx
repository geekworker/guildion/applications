import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { Animated, Text, View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import AsyncImage from '../../atoms/AsyncImage';
import PostsCountLabel from '../../atoms/PostsCountLabel';
import TouchableText from '../../atoms/TouchableText';
import RoomPublicCellPresenter from './presenter';

const RoomPublicCellComponent = ({ styles, appTheme, animatedStyle, onPress, room, loading, onPressJoin, onPressLeave }: RoomPublicCellPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableHighlight style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}>
                    <View style={styles.profileContainer}>
                        <AsyncImage
                            loading={loading}
                            style={styles.getStyle('profile')}
                            url={room.profile?.url ?? ''}
                        />
                    </View>
                    <View style={styles.bodyContainer}>
                        {loading ? (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                                animationType="pulse"
                                layout={[
                                    { ...styles.displayNameSkeleton },
                                ]}
                            />
                        ) : (
                            <Text style={styles.displayName} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                                {room.displayName}
                            </Text>
                        )}
                        <View style={styles.contentsContainer}>
                            {loading ? (
                                <SkeletonContent
                                    isLoading={loading ?? false}
                                    boneColor={appTheme.skeleton.boneColor}
                                    highlightColor={appTheme.skeleton.highlightColor}
                                    animationType="pulse"
                                    layout={[
                                        { ...styles.postsCountSkeleton },
                                    ]}
                                />
                            ) : (
                                <PostsCountLabel
                                    style={styles.getStyle('postsCount')}
                                    count={room.activity?.postsCount ?? 0}
                                />
                            )}
                        </View>
                    </View>
                    <View style={styles.memberStatus}>
                        {room.currentMember && !loading ? (
                            <TouchableText textStyle={styles.getStyle('member')} onPress={onPressLeave}>
                                {localizer.dictionary.room.show.member.toUpperCase()}
                            </TouchableText>
                        ) : loading ? (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                                animationType="pulse"
                                layout={[
                                    { ...styles.buttonSkeleton },
                                ]}
                            />
                        ) : (
                            <TouchableText textStyle={styles.getStyle('join')} onPress={onPressJoin}>
                                {localizer.dictionary.room.show.join.toUpperCase()}
                            </TouchableText>
                        )}
                    </View>
                </View>
            </TouchableHighlight>
        </Animated.View>
    );
};

export default React.memo(RoomPublicCellComponent, RoomPublicCellPresenter.outputAreEqual);