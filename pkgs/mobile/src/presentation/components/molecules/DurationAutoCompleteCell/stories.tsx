import { storiesOf } from '@storybook/react-native';
import React from 'react';
import DurationAutoCompleteCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';

storiesOf('DurationAutoCompleteCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <DurationAutoCompleteCell
            style={new Style({
                width: number('width', 370),
                height: 44,
            })}
            durationMs={0}
        />
    ))
    .add('Sec', () => (
        <DurationAutoCompleteCell
            style={new Style({
                width: number('width', 370),
                height: 44,
            })}
            durationMs={6000}
        />
    ))
    .add('MS', () => (
        <DurationAutoCompleteCell
            style={new Style({
                width: number('width', 370),
                height: 44,
            })}
            durationMs={61000}
        />
    ))
    .add('HMS', () => (
        <DurationAutoCompleteCell
            style={new Style({
                width: number('width', 370),
                height: 32,
            })}
            durationMs={20061000}
        />
    ))