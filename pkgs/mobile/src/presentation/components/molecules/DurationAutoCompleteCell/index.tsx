import React from 'react';
import DurationAutoCompleteCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Text, TouchableHighlight, View } from 'react-native';
import { localizer } from '@/shared/constants/Localizer';
import { formatMs } from '@guildion/core';

type Props = {
    durationMs: number,
    onPress?: (durationMs: number) => void,
} & Partial<StyleProps>;

const DurationAutoCompleteCell: React.FC<Props> = ({
    style,
    appTheme,
    durationMs,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new DurationAutoCompleteCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={React.useCallback(() => onPress && onPress(durationMs), [onPress, durationMs])}>
                <View style={styles.inner}>
                    <Text style={styles.duration} numberOfLines={1}>
                        {formatMs(durationMs)}
                    </Text>
                    <Text style={styles.description} numberOfLines={1}>
                        {localizer.dictionary.room.textChat.durationAutoComplete}
                    </Text>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(DurationAutoCompleteCell, compare);