import React from 'react';
import RoomControlButtonComponent from './component';
import RoomControlButtonPresenter from './presenter';

const RoomControlButton = (props: RoomControlButtonPresenter.Input) => {
    const output = RoomControlButtonPresenter.usePresenter(props);
    return <RoomControlButtonComponent {...output} />;
};

export default React.memo(RoomControlButton, RoomControlButtonPresenter.inputAreEqual);