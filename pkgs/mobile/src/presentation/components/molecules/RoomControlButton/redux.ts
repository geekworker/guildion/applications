import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Action as ReduxAction } from "redux";

namespace RoomControlButtonRedux {

    export class State extends Record<{
        offsetBottom: number,
    }>({
        offsetBottom: 0,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        setOffsetBottom: actionCreator<number>('SET_OFFSET_BOTTOM'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.setOffsetBottom, (state, payload) => {
            state = state.set('offsetBottom', payload);
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });
}

export default RoomControlButtonRedux