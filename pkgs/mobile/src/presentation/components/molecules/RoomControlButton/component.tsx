import React from 'react';
import { Animated, View } from 'react-native';
import ControlButton from '../../atoms/ControlButton';
import RoomControlButtonPresenter from './presenter';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

const RoomControlButtonComponent = ({ styles, appTheme, animatedStyle, bottom, right, onPress }: RoomControlButtonPresenter.Output) => {
    return (
        <Animated.View
            style={{
                ...styles.container,
                ...animatedStyle?.toStyleObject(),
                transform: [
                    {
                        translateY: bottom,
                    },
                    {
                        translateX: right,
                    },
                ],
            }}
        >
            <ControlButton
                style={styles.getStyle('inner')}
                onPress={onPress}
                Icon={(props) => (
                    <AntDesignIcon {...props} color={appTheme.element.link} name={'menuunfold'} />
                )}
                shadow
            />
        </Animated.View>
    );
};

export default React.memo(RoomControlButtonComponent, RoomControlButtonPresenter.outputAreEqual);