import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomControlButton from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import RoomControlButtonProvider from './provider';

storiesOf('RoomControlButton', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <RoomControlButtonProvider>
                        <CenterView>
                            {getStory()}
                        </CenterView>
                    </RoomControlButtonProvider>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomControlButton
            style={new Style({
                width: 64,
                height: 64,
            })}
            panelProps={{}}
        />
    ))