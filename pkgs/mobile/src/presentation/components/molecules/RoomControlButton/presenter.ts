import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomControlButtonHooks from './hooks';
import RoomControlButtonStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import RoomControlButtonRedux from './redux';
import { AnyAction } from 'redux';
import { Animated } from 'react-native';
import type RoomControlPanelTemplatePresenter from '../../templates/RoomControlPanelTemplate/presenter';

namespace RoomControlButtonPresenter {
    export type Input = {
        panelProps: RoomControlPanelTemplatePresenter.Input,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomControlButtonStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        bottom: Animated.Value | Animated.AnimatedInterpolation,
        right: Animated.Value | Animated.AnimatedInterpolation,
        rightValue: number,
        onPress: () => void,
    };

    export type Payload = {
        state: RoomControlButtonRedux.State,
        dispatch: React.Dispatch<AnyAction>,
    }

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };

    export function usePayload(): Payload {
        const { state, dispatch } = RoomControlButtonHooks.useReducer();
        return {
            state,
            dispatch
        }
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomControlButtonHooks.useStyles({ ...props });
        const {
            bottom,
            right,
            onPress,
            rightValue,
        } = RoomControlButtonHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            bottom,
            right,
            onPress,
            rightValue,
        }
    }
}

export default RoomControlButtonPresenter;