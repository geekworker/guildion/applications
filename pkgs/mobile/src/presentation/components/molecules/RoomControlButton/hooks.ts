import React from 'react';
import RoomControlButtonStyles from './styles';
import type RoomControlButtonPresenter from './presenter';
import RoomControlButtonRedux from './redux';
import { Animated } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import ModalsRedux from '../Modals/redux';

namespace RoomControlButtonHooks {
    export const useStyles = (props: RoomControlButtonPresenter.Input) => {
        const styles = React.useMemo(() => new RoomControlButtonStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useReducer = () => {
        const [state, dispatch] = React.useReducer(
            RoomControlButtonRedux.reducer,
            RoomControlButtonRedux.initialState,
        );
        return {
            state,
            dispatch
        }
    }

    export const useState = (props: RoomControlButtonPresenter.Input) => {
        const context = React.useContext(RoomControlButtonRedux.Context);
        const modalsContext = React.useContext(ModalsRedux.Context);
        const insets = useSafeAreaInsets();
        const bottomMargin = React.useMemo(() => 20, []);
        const bottom = React.useRef(new Animated.Value(-(insets.bottom + context.state.offsetBottom + bottomMargin))).current;
        const rightMargin = React.useMemo(() => 20, []);
        const [rightValue, setRightValue] = React.useState(-(insets.right + rightMargin));
        const right = React.useRef(new Animated.Value(rightValue)).current;

        React.useEffect(() => {
            Animated.spring(bottom, {
                useNativeDriver: true,
                toValue: -(insets.bottom + context.state.offsetBottom + bottomMargin),
                overshootClamping: false,
            }).start();
        }, [insets.bottom, context.state.offsetBottom]);
        React.useEffect(() => {
            const toValue = -(insets.right + rightMargin);
            setRightValue(toValue);
            Animated.spring(right, {
                useNativeDriver: true,
                toValue: toValue,
                overshootClamping: false,
            }).start();
        }, [insets.right]);

        const onPress = React.useCallback(() => {
            modalsContext.dispatch(ModalsRedux.Action.showRoomControlPanel(props.panelProps));
        }, [props.panelProps]);

        return {
            bottom,
            right,
            onPress,
            rightValue,
        };
    }
}

export default RoomControlButtonHooks;