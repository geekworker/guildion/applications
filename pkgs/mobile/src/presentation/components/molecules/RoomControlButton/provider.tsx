import React from 'react';
import RoomControlButtonRedux from './redux';
import RoomControlButtonPresenter from './presenter';

interface Props {
    children?: React.ReactNode,
}

const RoomControlButtonProvider: React.FC<Props> = ({ children }) => {
    const payload = RoomControlButtonPresenter.usePayload();
    return (
        <RoomControlButtonRedux.Context.Provider value={{ state: payload.state, dispatch: payload.dispatch }} >
            {children}
        </RoomControlButtonRedux.Context.Provider>
    );
};

export default RoomControlButtonProvider;
