import { storiesOf } from '@storybook/react-native';
import React from 'react';
import WidgetCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Widget } from '@guildion/core';
import Icon from 'react-native-vector-icons/Fontisto';
import { Style } from '@/shared/interfaces/Style';

storiesOf('WidgetCell', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <WidgetCell
            widget={new Widget({
                enName: 'TEXT CHAT',
                enDescription: 'You can chat with this room members while watching video' 
            })}
            Icon={(props) => <Icon {...props} name={'hashtag'} />}
            buttonTitle={'Try it'}
            style={new Style({
                width: 140,
            })}
        />
    ))
