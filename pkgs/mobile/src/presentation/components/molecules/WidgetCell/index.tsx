import React from 'react';
import WidgetCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, TouchableOpacity, View } from 'react-native';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';
import Button, { ButtonStyleType } from '../../atoms/Button';
import { Widget } from '@guildion/core';
import { localizer } from '@/shared/constants/Localizer';
import WidgetConditionalIcon from '../../Icons/WidgetConditionalIcon';
import { compare } from '@/shared/modules/ObjectCompare';

export type WidgetCellProps = {
    Icon?: IconNodeClass,
    widget: Widget,
    buttonTitle: string,
    onPress?: () => void,
    showButton?: boolean,
} & Partial<StyleProps>;

const WidgetCell: React.FC<WidgetCellProps> = ({
    style,
    appTheme,
    Icon,
    widget,
    buttonTitle,
    onPress,
    showButton,
}) => {
    const styles = React.useMemo(() => new WidgetCellStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return !!showButton ? (
        <View style={styles.container}>
            {!!Icon ? (
                <Icon
                    size={styles.icon.width}
                    color={styles.icon.color}
                />
            ) : (
                <WidgetConditionalIcon widget={widget} style={styles.getStyle('icon')} />
            )}
            <Text style={styles.title} numberOfLines={1} minimumFontScale={0.5} adjustsFontSizeToFit>
                {widget.getName(localizer.lang).toUpperCase()}
            </Text>
            <Text style={styles.description} numberOfLines={4} minimumFontScale={0.5} adjustsFontSizeToFit>
                {widget.getDescription(localizer.lang)}
            </Text>
            {!!showButton && (
                <Button
                    style={styles.getStyle('button')}
                    title={buttonTitle}
                    styleType={ButtonStyleType.Fill}
                    onPress={onPress}
                />
            )}
        </View>
    ) : (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.container}>
                {!!Icon ? (
                    <Icon
                        size={styles.icon.width}
                        color={styles.icon.color}
                    />
                ) : (
                    <WidgetConditionalIcon widget={widget} style={styles.getStyle('icon')} />
                )}
                <Text style={styles.title} numberOfLines={1} minimumFontScale={0.5} adjustsFontSizeToFit>
                    {widget.getName(localizer.lang).toUpperCase()}
                </Text>
                <Text style={styles.description} numberOfLines={4} minimumFontScale={0.8} adjustsFontSizeToFit>
                    {widget.getDescription(localizer.lang)}
                </Text>
                {!!showButton && (
                    <Button
                        style={styles.getStyle('button')}
                        title={buttonTitle}
                        styleType={ButtonStyleType.Fill}
                        onPress={onPress}
                    />
                )}
            </View>
        </TouchableOpacity>
    );
};

export default React.memo(WidgetCell, compare);