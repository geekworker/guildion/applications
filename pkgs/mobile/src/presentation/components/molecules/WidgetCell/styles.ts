import { MontserratFont } from '@/shared/constants/Font';
import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style, showButton }: StyleProps & { showButton?: boolean }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            padding: 0,
            paddingTop: 16,
            paddingLeft: 10,
            paddingRight: 10,
            paddingBottom: 16,
            borderRadius: 5,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            shadowColor: style?.shadowColor ?? appTheme.room.cellShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.room.cellShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        icon: {
            width: 24,
            height: 24,
            color: appTheme.element.default,
        },
        title: {
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'center',
            marginTop: 8,
        },
        description: {
            fontSize: 12,
            lineHeight: 20,
            marginTop: 4,
            marginBottom: !!showButton ? 16 : 0,
            fontFamily: MontserratFont.SemiBold,
            color: appTheme.element.subp1,
            textAlign: 'left',
        },
        button: {
            width: Number(style?.width ?? 0) - 20,
            height: 34,
            borderRadius: 17,
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
        },
    })
};

type Styles = typeof styles;

export default class WidgetCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { showButton?: boolean }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};