import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomLogCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { RoomLog } from '@guildion/core';

storiesOf('RoomLogCell', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <RoomLogCell
            log={new RoomLog()}
        >
            
        </RoomLogCell>
    ))
