import React from 'react';
import { View } from 'react-native';
import RoomLogCellPresenter from './presenter';

const RoomLogCellComponent: React.FC<RoomLogCellPresenter.Output> = ({ styles }) => {
    return (
        <View></View>
    );
};

export default React.memo(RoomLogCellComponent, RoomLogCellPresenter.outputAreEqual);