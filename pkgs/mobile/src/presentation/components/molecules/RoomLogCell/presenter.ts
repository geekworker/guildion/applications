import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomLogCellHooks from "./hooks";
import RoomLogCellStyles from './styles';
import React from 'react';
import { RoomLog } from "@guildion/core";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomLogCellPresenter {
    export type Input = {
        children?: React.ReactNode,
        log: RoomLog,
        onPressCell?: (log: RoomLog) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomLogCellStyles,
        appTheme: AppTheme,
        log: RoomLog,
        onPressCell?: (log: RoomLog) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoomLogCellHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomLogCellPresenter;