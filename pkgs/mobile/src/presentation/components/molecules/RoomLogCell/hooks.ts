import React from "react";
import RoomLogCellStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace RoomLogCellHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomLogCellStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default RoomLogCellHooks