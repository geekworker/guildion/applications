import React from 'react';
import RoomLogCellComponent from './component';
import RoomLogCellPresenter from './presenter';

const RoomLogCell = (props: RoomLogCellPresenter.Input) => {
    const output = RoomLogCellPresenter.usePresenter(props);
    return <RoomLogCellComponent {...output} />;
};

export default React.memo(RoomLogCell, RoomLogCellPresenter.inputAreEqual);