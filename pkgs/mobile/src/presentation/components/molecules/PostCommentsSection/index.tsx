import React from 'react';
import PostCommentsSectionStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Post } from '@guildion/core';
import { Text, View } from 'react-native';
import { localizer } from '@/shared/constants/Localizer';

type Props = {
    post: Post,
} & Partial<StyleProps>;

const PostCommentsSection: React.FC<Props> = ({
    style,
    appTheme,
    post,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new PostCommentsSectionStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                {localizer.dictionary.room.posts.show.commentTitle}
            </Text>
            <Text style={styles.count}>
                {`${post.commentsCount}`}
            </Text>
        </View>
    );
};

export default React.memo(PostCommentsSection, compare);