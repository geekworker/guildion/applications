import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { merge, Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const innerWidth = (style?.getAsNumber('width') ?? 0) - 40;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 12,
            paddingBottom: 12,
            paddingLeft: 20,
            paddingRight: 20,
            // borderTopWidth: style?.borderTopWidth ?? 1,
            // borderTopColor: style?.borderTopColor ?? appTheme.background.subp1,
            borderBottomWidth: style?.borderBottomWidth ?? 1,
            borderBottomColor: style?.borderBottomColor ?? appTheme.background.subp1,
        },
        title: {
            fontFamily: MontserratFont.SemiBold,
            fontSize: 16,
            textAlign: 'left',
            maxWidth: innerWidth / 2,
            color: appTheme.element.default,
        },
        count: {
            fontFamily: MontserratFont.Regular,
            fontSize: 16,
            textAlign: 'left',
            maxWidth: (innerWidth / 2) - 8,
            color: appTheme.element.subp1,
            marginLeft: 8,
        },
    })
};

type Styles = typeof styles;

export default class PostCommentsSectionStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};