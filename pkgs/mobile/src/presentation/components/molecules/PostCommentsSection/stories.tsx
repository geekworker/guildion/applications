import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PostCommentsSection from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Post } from '@guildion/core';

storiesOf('PostCommentsSection', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PostCommentsSection
            style={new Style({
                width: 380,
                height: 44,
            })}
            post={new Post({
                commentsCount: 12,
            })}
        />
    ))