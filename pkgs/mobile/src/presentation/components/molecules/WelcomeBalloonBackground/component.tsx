import React from 'react';
import { Animated } from 'react-native'
import WelcomeBalloon from '../../atoms/WelcomeBalloon';
import WelcomeBalloonBackgroundPresenter from './presenter';

const WelcomeBalloonBackgroundComponent = ({ styles, animated, opacity, window, isAppeared }: WelcomeBalloonBackgroundPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.wrapper, opacity }} >
            <WelcomeBalloon
                style={styles.getStyle('balloon1')}
                type={'border'}
                animated={!animated || !!isAppeared}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon2')}
                type={'border'}
                animated={!animated || !!isAppeared}
                reverse={false}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon3')}
                type={'fill'}
                animated={!animated || !!isAppeared}
                reverse={true}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon4')}
                type={'fill'}
                animated={!animated || !!isAppeared}
                reverse={false}
            />
            <WelcomeBalloon
                style={styles.getStyle('balloon5')}
                type={'border'}
                animated={!animated || !!isAppeared}
                reverse={true}
            />
            {window.width >= 800 && window.height > 700 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon6')}
                    type={'fill'}
                    animated={!animated || !!isAppeared}
                    reverse={true}
                />
            )}
            {window.width >= 800 && window.height > 700 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon7')}
                    type={'border'}
                    animated={!animated || !!isAppeared}
                    reverse={true}
                />
            )}
            {window.width >= 1024 && window.height > 700 && window.height < 1224 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon8')}
                    type={'border'}
                    animated={!animated || !!isAppeared}
                    reverse={true}
                />
            )}
            {window.height >= 1224 && (
                <WelcomeBalloon
                    style={styles.getStyle('balloon9')}
                    type={'fill'}
                    animated={!animated || !!isAppeared}
                    reverse={true}
                />
            )}
        </Animated.View>
    );
};

export default React.memo(WelcomeBalloonBackgroundComponent, WelcomeBalloonBackgroundPresenter.outputAreEqual);