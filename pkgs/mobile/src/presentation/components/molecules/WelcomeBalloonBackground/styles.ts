import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            backgroundColor: style?.backgroundColor ?? appTheme.common.transparent,
            position: 'relative',
        },
        balloon1: {
            width: 240,
            height: 240,
            borderRadius: 120,
            top: -350,
            left: -300,
        },
        balloon2: {
            width: 180,
            height: 180,
            borderRadius: 90,
            top: -150,
            right: 50,
        },
        balloon3: {
            width: 120,
            height: 120,
            borderRadius: 60,
            top: 30,
            right: 250,
        },
        balloon4: {
            width: 140,
            height: 140,
            borderRadius: 70,
            top: 120,
            left: -180,
        },
        balloon5: {
            width: 120,
            height: 120,
            borderRadius: 60,
            top: 170,
            right: 40,
        },
        balloon6: {
            width: 240,
            height: 240,
            borderRadius: 120,
            top: 0,
            left: 180,
        },
        balloon7: {
            width: 150,
            height: 150,
            borderRadius: 75,
            top: 380,
            left: 120,
        },
        balloon8: {
            width: 120,
            height: 120,
            borderRadius: 60,
            top: 360,
            right: 300,
        },
        balloon9: {
            width: 120,
            height: 120,
            borderRadius: 60,
            top: 600,
            right: 200,
        },
    })
};

type Styles = typeof styles;

export default class WelcomeBalloonBackgroundStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};