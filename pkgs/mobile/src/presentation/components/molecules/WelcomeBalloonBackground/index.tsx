import React from 'react';
import WelcomeBalloonBackgroundComponent from './component';
import WelcomeBalloonBackgroundPresenter from './presenter';

const WelcomeBalloonBackground = (props: WelcomeBalloonBackgroundPresenter.Input) => {
    const output = WelcomeBalloonBackgroundPresenter.usePresenter(props);
    return <WelcomeBalloonBackgroundComponent {...output} />;
};

export default React.memo(WelcomeBalloonBackground, WelcomeBalloonBackgroundPresenter.inputAreEqual);