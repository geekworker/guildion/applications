import { storiesOf } from '@storybook/react-native';
import { boolean, number } from '@storybook/addon-knobs';
import React from 'react';
import WelcomeBalloonBackground from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { AppThemes } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('WelcomeBalloonBackground', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <WelcomeBalloonBackground
            appTheme={AppThemes.Dark}
            style={new Style({
                width: number('width', 385),
                height: number('height', 600),
            })}
            isAppeared={boolean('isAppeared', true)}
        />
    ))
