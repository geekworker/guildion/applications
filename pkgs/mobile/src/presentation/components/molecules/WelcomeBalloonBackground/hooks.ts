import React from 'react';
import WelcomeBalloonBackgroundStyles from './styles';
import { Animated } from 'react-native';
import StyleProps from '@/shared/interfaces/StyleProps';

namespace WelcomeBalloonBackgroundHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new WelcomeBalloonBackgroundStyles(props), [props]);
        return styles;
    }

    export const useAnimation = (isAppeared?: boolean): {
        animated: boolean,
        opacity: Animated.Value,
    } => {
        const opacity = React.useRef(new Animated.Value(0)).current;
        const [animated, setAnimated] = React.useState(false);
        React.useEffect(() => {
            Animated.timing(opacity, {
                toValue: !!isAppeared ? 1 : 0,
                duration: 500,
                useNativeDriver: true,
            }).start(() => {
                if (!!isAppeared) {
                    const timer = setInterval(() => {
                        if (!!isAppeared) {
                            setAnimated(true)
                            clearInterval(timer);
                        }
                    }, 500)
                }
            });
            return () => {
                isAppeared = false;
            }
        }, [isAppeared]);
        return {
            animated,
            opacity,
        }
    }
}

export default WelcomeBalloonBackgroundHooks;