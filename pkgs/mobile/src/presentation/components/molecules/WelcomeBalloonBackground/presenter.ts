import StyleProps from "@/shared/interfaces/StyleProps";
import { compare } from "@/shared/modules/ObjectCompare";
import { Animated, ScaledSize, useWindowDimensions } from "react-native";
import WelcomeBalloonBackgroundHooks from "./hooks";
import WelcomeBalloonBackgroundStyles from './styles';

namespace WelcomeBalloonBackgroundPresenter {
    
    export type Input = {
        isAppeared?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: WelcomeBalloonBackgroundStyles,
        animated: boolean,
        opacity: Animated.Value,
        window: ScaledSize,
        isAppeared: boolean,
    }

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = WelcomeBalloonBackgroundHooks.useStyles({ ...props });
        const { animated, opacity } = WelcomeBalloonBackgroundHooks.useAnimation(props.isAppeared);
        const window = useWindowDimensions();
        return {
            styles,
            animated,
            opacity,
            window,
            isAppeared: !!props.isAppeared,
        }
    }
}

export default WelcomeBalloonBackgroundPresenter;