import React from 'react';
import AttachmentCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { File, FileType } from '@guildion/core';
import { View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import { TouchableHighlight } from 'react-native-gesture-handler';

type Props = {
    file: File,
    onPress?: (file: File) => void,
} & Partial<StyleProps>;

const AttachmentCell: React.FC<Props> = ({
    style,
    appTheme,
    file,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new AttachmentCellStyles({
        style,
        appTheme,
        file,
    }), [
        style,
        appTheme,
        file,
    ]);
    const _onPress = React.useCallback(() => {
        onPress && onPress(file);
    }, [onPress, file])
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={_onPress}>
                <View style={styles.inner}>
                    <AsyncImage url={file.thumbnailUrl} style={styles.getStyle('media')}/>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(AttachmentCell, compare);