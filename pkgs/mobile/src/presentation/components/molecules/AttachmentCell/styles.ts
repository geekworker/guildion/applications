import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { File } from "@guildion/core";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style, file }: StyleProps & { file: File }) => {
    appTheme ||= fallbackAppTheme;
    const _maxWidth = Math.min(500, style?.getAsNumber('maxWidth') ?? 0);
    const minWidth = 40;
    const maxWidth = Math.max(_maxWidth, minWidth);
    const heightAspect = (file.heightPx ?? 1) / (file.widthPx ?? 1);
    const attachmentRawPixelRatio = 0.4;
    const _width = Math.min(maxWidth, (file.widthPx ?? 0) * attachmentRawPixelRatio);
    const width = Math.max(_width, minWidth);
    const height = Math.min(maxWidth, width * heightAspect);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            position: 'relative',
            maxWidth,
            minWidth,
            borderRadius: 5,
            overflow: "hidden",
            backgroundColor: 'black',
        },
        touchable: {
            width,
            height,
            position: 'relative',
            maxWidth,
            minWidth,
            borderRadius: 5,
            overflow: "hidden",
            backgroundColor: 'black',
        },
        inner: {
            width,
            height,
            position: 'relative',
            maxWidth,
            minWidth,
            borderRadius: 5,
            overflow: "hidden",
            backgroundColor: 'black',
        },
        media: {
            width,
            height,
            maxWidth,
            minWidth,
            borderRadius: 5,
            overflow: "hidden",
        },
    })
};

type Styles = typeof styles;

export default class AttachmentCellStyles extends Record<ReturnType<Styles>>({
    ...styles({ file: new File() })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { file: File }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};