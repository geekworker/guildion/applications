import React from 'react';
import RoomPublicProfileComponent from './component';
import RoomPublicProfilePresenter from './presenter';

const RoomPublicProfile = (props: RoomPublicProfilePresenter.Input) => {
    const output = RoomPublicProfilePresenter.usePresenter(props);
    return <RoomPublicProfileComponent {...output} />;
};

export default React.memo(RoomPublicProfile, RoomPublicProfilePresenter.inputAreEqual);