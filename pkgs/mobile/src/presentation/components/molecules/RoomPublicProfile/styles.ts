import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
        },
        inner: {
            ...style?.toStyleObject(),
            width,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        profile: {
            width: 164,
            height: 164,
            borderRadius: 5,
        },
        displayName: {
            marginTop: 24,
            fontFamily: MontserratFont.Bold,
            fontSize: 16,
            marginBottom: 20,
            textAlign: 'center',
            color: appTheme.element.default,
        },
        description: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            textAlign: 'center',
            color: appTheme.element.default,
        },
        socialLinksContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: 24,
            width,
            marginBottom: 20,
        },
        socialLinks: {
            height: 24,
        },
        countersContainer: {
            marginBottom: 20,
            width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        connectingLabel: {
            marginRight: 12,
        },
        membersCountLabel: {
        },
    });
};

type Styles = typeof styles;

export default class RoomPublicProfileStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};