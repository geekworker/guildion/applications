import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomPublicProfileHooks from './hooks';
import RoomPublicProfileStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Room, SocialLink } from '@guildion/core';

namespace RoomPublicProfilePresenter {
    export type Input = {
        room: Room,
        loading?: boolean,
        onPressSocialLink?: (socialLink: SocialLink) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomPublicProfileStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        room: Room,
        loading?: boolean,
        onPressSocialLink?: (socialLink: SocialLink) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomPublicProfileHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomPublicProfilePresenter;