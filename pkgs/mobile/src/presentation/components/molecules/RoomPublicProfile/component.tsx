import React from 'react';
import { Animated, Text, View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import ConnectingLabel from '../../atoms/ConnectingLabel';
import MembersCountLabel from '../../atoms/MembersCountLabel';
import SocialLinksList from '../../organisms/SocialLinksList';
import RoomPublicProfilePresenter from './presenter';

const RoomPublicProfileComponent = ({ styles, appTheme, animatedStyle, room, onPressSocialLink, loading }: RoomPublicProfilePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <AsyncImage
                    url={room.records.profile?.url ?? ''}
                    style={styles.getStyle('profile')}
                    loading={loading}
                />
                {!!room.displayName && !loading && (
                    <Text style={styles.displayName}>
                        {room.displayName}
                    </Text>
                )}
                {!!room.description && !loading && (
                    <Text style={styles.description}>
                        {room.description}
                    </Text>
                )}
                {!loading && room.records.socialLinks && room.records.socialLinks.size > 0 && (
                    <View style={styles.socialLinksContainer}>
                        <SocialLinksList
                            socialLinks={room.records.socialLinks}
                            style={styles.getStyle('socialLinks')}
                            onPressSocialLink={onPressSocialLink}
                        />
                    </View>
                )}
                {!loading && room.records.activity && (
                    <View style={styles.countersContainer}>
                        <ConnectingLabel
                            count={room.records.activity.connectsCount}
                            style={styles.getStyle('connectingLabel')}
                        />
                        <MembersCountLabel
                            count={room.records.activity.membersCount}
                            style={styles.getStyle('membersCountLabel')}
                        />
                    </View>
                )}
            </View>
        </Animated.View>
    );
};

export default React.memo(RoomPublicProfileComponent, RoomPublicProfilePresenter.outputAreEqual);