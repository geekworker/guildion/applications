import React from 'react';
import RoomPublicProfileStyles from './styles';
import type RoomPublicProfilePresenter from './presenter';

namespace RoomPublicProfileHooks {
    export const useStyles = (props: RoomPublicProfilePresenter.Input) => {
        const styles = React.useMemo(() => new RoomPublicProfileStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomPublicProfilePresenter.Input) => {
        return {};
    }
}

export default RoomPublicProfileHooks;