import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomPublicProfile from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, Room, RoomActivity, SocialLink, SocialLinks } from '@guildion/core';

storiesOf('RoomPublicProfile', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomPublicProfile
            style={new Style({
                width: 380,
                height: 430,
            })}
            room={new Room(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new RoomActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))
    .add('Loading', () => (
        <RoomPublicProfile
            style={new Style({
                width: 380,
                height: 430,
            })}
            loading
            room={new Room(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new RoomActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))