import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomCellHooks from "./hooks";
import RoomCellStyles from './styles';
import { Room } from "@guildion/core";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomCellPresenter {
    export type Input = {
        onPress?: (room: Room) => void,
        room: Room,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomCellStyles,
        appTheme: AppTheme,
        onPress?: () => void,
        room: Room,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoomCellHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress: () => props.onPress && props.onPress(props.room),
        }
    }
}

export default RoomCellPresenter;