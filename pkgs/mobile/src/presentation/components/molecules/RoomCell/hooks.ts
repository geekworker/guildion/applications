import React from "react";
import RoomCellStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace RoomCellHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomCellStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default RoomCellHooks