import React from 'react';
import RoomCellComponent from './component';
import RoomCellPresenter from './presenter';

const RoomCell = (props: RoomCellPresenter.Input) => {
    const output = RoomCellPresenter.usePresenter(props);
    return <RoomCellComponent {...output} />;
};

export default React.memo(RoomCell, RoomCellPresenter.inputAreEqual);