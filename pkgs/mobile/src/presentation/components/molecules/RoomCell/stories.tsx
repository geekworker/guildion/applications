import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Member, Members, Message, Room, RoomActivity, RoomLog } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { number, text } from '@storybook/addon-knobs';
import { Style } from '@/shared/interfaces/Style';

storiesOf('RoomCell', module)
    .addDecorator((getStory) => <StoreProvider><CenterView style={{ backgroundColor: fallbackAppTheme.background.subm1 }}>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <RoomCell
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    connectingMembers: new Members([
                        new Member({
                            id: 'test1',
                            displayName: 'test1',
                        }).toJSON(),
                        new Member({
                            id: 'test2',
                            displayName: 'test2',
                        }).toJSON(),
                        new Member({
                            id: 'test3',
                            displayName: 'test3',
                        }).toJSON(),
                        new Member({
                            id: 'test4',
                            displayName: 'test4',
                        }).toJSON(),
                    ]).toJSON(),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            style={new Style({
                width: 380,
            })}
        />
    ))
    .add('Loading', () => (
        <RoomCell
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    connectingMembers: new Members([
                        new Member({
                            id: 'test1',
                            displayName: 'test1',
                        }).toJSON(),
                        new Member({
                            id: 'test2',
                            displayName: 'test2',
                        }).toJSON(),
                        new Member({
                            id: 'test3',
                            displayName: 'test3',
                        }).toJSON(),
                        new Member({
                            id: 'test4',
                            displayName: 'test4',
                        }).toJSON(),
                    ]).toJSON(),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            style={new Style({
                width: 380,
            })}
            loading={true}
        />
    ))
