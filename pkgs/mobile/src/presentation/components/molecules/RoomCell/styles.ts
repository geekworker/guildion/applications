import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { MontserratFont } from '@/shared/constants/Font';

const styles = ({
    style,
    appTheme,
}: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const paddingTop = style?.paddingTop ? Number(style?.paddingTop) : 20 ;
    const paddingLeft = style?.paddingLeft ? Number(style?.paddingLeft) : 20 ;
    const paddingRight = style?.paddingRight ? Number(style?.paddingRight) : 20 ;
    const paddingBottom = style?.paddingBottom ? Number(style?.paddingBottom) : 20 ;
    return StyleSheet.create({
        highlight: {
            ...style?.toStyleObject(),
            backgroundColor: style?.backgroundColor ?? appTheme.room.cellBackground,
            borderRadius: style?.borderRadius ?? 5,
            borderWidth: 1,
            borderColor: style?.borderColor ?? appTheme.background.subp3,
            shadowColor: style?.shadowColor ?? appTheme.room.cellShadowColor,
            shadowOpacity: style?.shadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            elevation: style?.elevation ?? 4,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
            paddingTop,
            paddingLeft,
            paddingRight,
            paddingBottom,
            overflow: 'hidden',
        },
        container: {
        },
        syncVisionContainer: {
            width: Number(style?.width ?? 0),
            height: 180,
            position: 'relative',
        },
        syncVisionImage: {
            width: Number(style?.width ?? 0),
            height: 180,
        },
        top: {
            width: Number(style?.width ?? 0) - paddingLeft - paddingRight,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'stretch',
        },
        bottom: {
            width: style?.width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'stretch',
            marginTop: 12,
        },
        profileContainer: {
            position: 'relative',
            width: 44,
            height: 44,
        },
        profile: {
            position: 'relative',
            width: 44,
            height: 44,
            borderRadius: 5,
        },
        profileBadgeContainerStyle: {
            position: 'absolute',
            right: 2,
            top: -8,
        },
        profileBadgeStyle: {
            position: 'absolute',
            width: 20,
            height: 20,
            borderRadius: 10,
            borderWidth: 2,
            borderColor: style?.backgroundColor ?? appTheme.room.cellBackground,
        },
        profileBadgeTextStyle: {
            fontSize: 10,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
        },
        textContainer: {
            marginLeft: 12,
            marginRight: 4,
            width: Number(style?.width ?? 0) - paddingLeft - paddingRight - 44 - 12 - 30 - 4,
        },
        name: {
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'left',
            fontSize: 16,
        },
        nameSkeleton: {
            width: Number(style?.width ?? 0) - paddingLeft - paddingRight - 44 - 12 - 30 - 4,
            height: 16,
        },
        message: {
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginTop: 6,
            fontSize: 14,
        },
        messageSkeleton: {
            width: (Number(style?.width ?? 0) - paddingLeft - paddingRight - 44 - 12 - 30 - 4) - 20,
            marginTop: 6,
            marginLeft: -10,
            height: 14,
        },
        datatime: {
            fontFamily: MontserratFont.Light,
            fontSize: 12,
            color: appTheme.element.subp1,
            textAlign: 'right',
            marginLeft: 4,
            maxWidth: 30,
        },
        datatimeSkeleton: {
            height: 12,
            marginLeft: 4,
            marginTop: -24,
            width: 30,
        },
        connectingLabel: {
        },
        members: {
            marginLeft: 12,
            height: 34,
        },
    });
}

type Styles = typeof styles;

export default class RoomCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};