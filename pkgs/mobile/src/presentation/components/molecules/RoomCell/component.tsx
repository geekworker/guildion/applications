import { CURRENT_CONFIG } from '@/shared/constants/Config';
import { Members, RoomActivity } from '@guildion/core';
import React from 'react';
import { Text, View, TouchableHighlight } from 'react-native';
import { Badge } from 'react-native-elements';
import AsyncImage from '../../atoms/AsyncImage';
import ConnectingLabel from '../../atoms/ConnectingLabel';
import MembersOverlapList from '../../organisms/MembersOverlapList';
import RoomCellPresenter from './presenter';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const RoomCellComponent = ({ styles, room, onPress, loading, appTheme }: RoomCellPresenter.Output) => {
    return (
        <TouchableHighlight style={{ ...styles.highlight, borderWidth: loading ? 0 : styles.highlight.borderWidth }} onPress={onPress}>
            <View style={styles.container}>
                <View style={styles.top}>
                    <View style={styles.profileContainer}>
                        <AsyncImage loading={loading} style={styles.getStyle('profile')} url={room.profile?.url ?? ''} />
                        {!loading && !!room.activity?.notificationsCount && room.activity?.notificationsCount > 0 && (
                            <Badge
                                containerStyle={styles.profileBadgeContainerStyle}
                                badgeStyle={styles.profileBadgeStyle}
                                textStyle={styles.profileBadgeTextStyle}
                                status={'error'}
                                value={room.activity?.notificationsCount}
                            />
                        )}
                    </View>
                    <View style={styles.textContainer}>
                        {(loading ?? false) ? (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                layout={[styles.nameSkeleton]}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                            />
                        ) : (
                            <Text style={styles.name} numberOfLines={1} adjustsFontSizeToFit>
                                {room.displayName}
                            </Text>
                        )}
                        {(loading ?? false) ? (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                layout={[styles.messageSkeleton]}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                            />
                        ) : (
                            <Text style={styles.message} numberOfLines={1} >
                                {room.records.activity?.records.log?.toString()}
                            </Text>
                        )}
                    </View>
                    {(loading ?? false) ? (
                        <SkeletonContent
                            isLoading={loading ?? false}
                            layout={[styles.datatimeSkeleton]}
                            boneColor={appTheme.skeleton.boneColor}
                            highlightColor={appTheme.skeleton.highlightColor}
                        />
                    ) : (
                        <Text style={styles.datatime} numberOfLines={1} minimumFontScale={0.5} adjustsFontSizeToFit>
                            {room.records.activity?.getCreatedHM(CURRENT_CONFIG.DEFAULT_TIMEZONE)}
                        </Text>
                    )}
                </View>
                {!loading && (room.activity?.connectsCount ?? 0) > 0 && (room.activity?.connectingMembers?.length ?? 0) > 0 && (
                    <View style={styles.bottom}>
                        <ConnectingLabel count={room.activity?.connectsCount ?? room.activity?.connectingMembers?.length ?? 0} style={styles.getStyle('connectingLabel')}/>
                        <MembersOverlapList
                            members={room.records.activity?.records.connectingMembers ?? new Members([])}
                            style={styles.getStyle('members')}
                            truncateCount={RoomActivity.connectingMembersMaxLimit}
                            maxCount={room.activity?.connectsCount}
                        />
                    </View>
                )}
            </View>
        </TouchableHighlight>
    );
};

export default React.memo(RoomCellComponent, RoomCellPresenter.outputAreEqual);