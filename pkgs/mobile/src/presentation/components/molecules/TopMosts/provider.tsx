import React from 'react';
import TopMostsRedux from './redux';
import TopMostsPresenter from './presenter';

interface Props {
    children?: React.ReactNode,
}

const TopMostsProvider: React.FC<Props> = ({ children }) => {
    const payload = TopMostsPresenter.usePayload();
    return (
        <TopMostsRedux.Context.Provider value={{ state: payload.state, dispatch: payload.dispatch }} >
            {children}
        </TopMostsRedux.Context.Provider>
    );
};

export default TopMostsProvider;