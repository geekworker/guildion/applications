import React from 'react';
import MessageInputBar from '../MessageInputBar';
import TopMostsPresenter from './presenter';

const TopMostsComponent = ({ state }: TopMostsPresenter.Output) => {
    const {
        propsMessageInputBar,
    } = state;
    return (
        <>
            {!!propsMessageInputBar && (
                <MessageInputBar {...propsMessageInputBar} />
            )}
        </>
    );
};

export default React.memo(TopMostsComponent, TopMostsPresenter.outputAreEqual);