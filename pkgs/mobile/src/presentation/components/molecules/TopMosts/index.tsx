import React from 'react';
import TopMostsComponent from './component';
import TopMostsPresenter from './presenter';

const TopMosts = (props: TopMostsPresenter.Input) => {
    const output = TopMostsPresenter.usePresenter(props);
    return <TopMostsComponent {...output} />;
};

export default React.memo(TopMosts, TopMostsPresenter.inputAreEqual);