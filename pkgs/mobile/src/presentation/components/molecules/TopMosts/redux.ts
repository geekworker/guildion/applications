import React from "react";
import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Action as ReduxAction } from "redux";
import type MessageInputBarPresenter from "../MessageInputBar/presenter";

namespace TopMostsRedux {
    export class State extends Record<{
        propsMessageInputBar?: MessageInputBarPresenter.Input,
    }>({
        propsMessageInputBar: undefined,
    }) {
    }

    export const initialState: State = new State();

    const actionCreator = actionCreatorFactory();

    export const Action = {
        showMessageInputBar: actionCreator<MessageInputBarPresenter.Input>('SHOW_MESSAGE_INPUT_BAR'),
        hideMessageInputBar: actionCreator('HIDE_MESSAGE_INPUT_BAR'),
    } as const;
    export type Action = typeof Action[keyof typeof Action];
    export type ActionType = Action['type'];

    export const reducer = reducerWithInitialState<State>(new State())
        .case(Action.showMessageInputBar, (state, payload) => {
            state = state.set('propsMessageInputBar', payload);
            return state;
        })
        .case(Action.hideMessageInputBar, (state) => {
            state = state.set('propsMessageInputBar', undefined);
            return state;
        })
    
    export type Reducer = typeof reducer;
    
    export const Context = React.createContext({} as {
        state: TopMostsRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    });

    export type Context = {
        state: TopMostsRedux.State,
        dispatch: React.Dispatch<ReduxAction<ActionType>>,
    };
}

export default TopMostsRedux