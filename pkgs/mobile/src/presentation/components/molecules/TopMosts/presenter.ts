import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import TopMostsHooks from './hooks';
import TopMostsStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import TopMostsRedux from './redux';
import { AnyAction } from 'redux';
import React from 'react';

namespace TopMostsPresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: TopMostsStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
    } & Omit<Input, 'style' | 'appTheme'> & Payload;

    export type Payload = {
        state: TopMostsRedux.State,
        dispatch: React.Dispatch<AnyAction>,
    }

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };

    export function usePayload(): Payload {
        const { state, dispatch } = TopMostsHooks.useReducer();
        return {
            state,
            dispatch
        }
    };
    
    export function usePresenter(props: Input): Output {
        const styles = TopMostsHooks.useStyles({ ...props });
        const context = React.useContext(TopMostsRedux.Context);
        return {
            ...props,
            ...context,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default TopMostsPresenter;