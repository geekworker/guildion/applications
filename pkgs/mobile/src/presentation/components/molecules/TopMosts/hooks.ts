import React from 'react';
import TopMostsStyles from './styles';
import type TopMostsPresenter from './presenter';
import TopMostsRedux from './redux';

namespace TopMostsHooks {
    export const useStyles = (props: TopMostsPresenter.Input) => {
        const styles = React.useMemo(() => new TopMostsStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useReducer = () => {
        const [state, dispatch] = React.useReducer(
            TopMostsRedux.reducer,
            TopMostsRedux.initialState,
        );
        return {
            state,
            dispatch
        }
    }

    export const useState = (props: TopMostsPresenter.Input) => {
        return {};
    }
}

export default TopMostsHooks;