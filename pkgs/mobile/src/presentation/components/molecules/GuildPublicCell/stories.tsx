import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildPublicCell from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Guild, GuildActivity, Files } from '@guildion/core';

export const mockGuild = new Guild({
    displayName: 'test',
}, {
    profile: Files.getSeed().generateGuildProfile(),
    activity: new GuildActivity({
        postsCount: 100,
    })
});

storiesOf('GuildPublicCell', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildPublicCell
            style={new Style({
                width: 360,
            })}
            guild={
                new Guild({
                    displayName: 'test',
                }, {
                    profile: Files.getSeed().generateGuildProfile(),
                    activity: new GuildActivity({
                        postsCount: 100,
                    })
                })
            }
        />
    ))
    .add('Loading', () => (
        <GuildPublicCell
            style={new Style({
                width: 360,
            })}
            loading
            guild={mockGuild}
        />
    ))