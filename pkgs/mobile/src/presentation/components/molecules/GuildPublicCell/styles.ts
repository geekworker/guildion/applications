import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const borderRadius = style?.getAsNumber('borderRadius') || 5;
    const paddingTop = style?.paddingTop ? Number(style?.paddingTop) : 20 ;
    const paddingLeft = style?.paddingLeft ? Number(style?.paddingLeft) : 20 ;
    const paddingRight = style?.paddingRight ? Number(style?.paddingRight) : 20 ;
    const paddingBottom = style?.paddingBottom ? Number(style?.paddingBottom) : 20 ;
    const bodyContainerWidth = width - paddingLeft - paddingRight - 64 - 12;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            borderRadius,
            backgroundColor: style?.backgroundColor ?? appTheme.room.cellBackground,
            shadowColor: style?.shadowColor ?? appTheme.room.cellShadowColor,
            shadowOpacity: style?.shadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            elevation: style?.elevation ?? 4,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        touchable: {
            width,
            borderRadius,
        },
        inner: {
            width,
            borderRadius,
            paddingTop,
            paddingLeft,
            paddingRight,
            paddingBottom,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        profileContainer: {
            position: 'relative',
            width: 64,
            height: 64,
        },
        profile: {
            position: 'relative',
            width: 64,
            height: 64,
            borderRadius: 5,
        },
        rightContainer: {
            width: bodyContainerWidth,
            marginLeft: 12,
            position: 'relative',
        },
        displayName: {
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'left',
            fontSize: 20,
            marginBottom: 8,
        },
        displayNameSkeleton: {
            textAlign: 'left',
            height: 20,
            marginBottom: 8,
            width: 100,
            marginLeft: -(bodyContainerWidth - 100) / 2 - 32,
        },
        buttonContainer: {
            marginBottom: 8,
        },
        join: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.element.main,
        },
        member: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            textAlign: 'left',
            color: appTheme.button.disabled,
        },
        buttonSkeleton: {
            height: 16,
            width: 60,
        },
        counterContainer: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        postsCount: {
        },
        postsCountSkeleton: {
            textAlign: 'left',
            height: 16,
            marginBottom: 6,
            width: 60,
            marginLeft: -(bodyContainerWidth - 60) / 2 - 50,
        },
    });
};

type Styles = typeof styles;

export default class GuildPublicCellStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};