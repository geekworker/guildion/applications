import React from 'react';
import GuildPublicCellComponent from './component';
import GuildPublicCellPresenter from './presenter';

const GuildPublicCell = (props: GuildPublicCellPresenter.Input) => {
    const output = GuildPublicCellPresenter.usePresenter(props);
    return <GuildPublicCellComponent {...output} />;
};

export default React.memo(GuildPublicCell, GuildPublicCellPresenter.inputAreEqual);