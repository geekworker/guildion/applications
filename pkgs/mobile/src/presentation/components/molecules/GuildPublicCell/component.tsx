import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { Animated, Text, TouchableHighlight, View } from 'react-native';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import AsyncImage from '../../atoms/AsyncImage';
import PostsCountLabel from '../../atoms/PostsCountLabel';
import TouchableText from '../../atoms/TouchableText';
import GuildPublicCellPresenter from './presenter';

const GuildPublicCellComponent = ({ styles, appTheme, animatedStyle, guild, onPress, onPressJoin, onPressLeave, loading }: GuildPublicCellPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableHighlight style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}>
                    <View style={styles.profileContainer}>
                        <AsyncImage
                            loading={loading}
                            style={styles.getStyle('profile')}
                            url={guild.profile?.url ?? ''}
                        />
                    </View>
                    <View style={styles.rightContainer}>
                        {loading ? (
                            <SkeletonContent
                                isLoading={loading ?? false}
                                boneColor={appTheme.skeleton.boneColor}
                                highlightColor={appTheme.skeleton.highlightColor}
                                animationType="pulse"
                                layout={[
                                    { ...styles.displayNameSkeleton },
                                ]}
                            />
                        ) : (
                            <Text style={styles.displayName} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                                {guild.displayName}
                            </Text>
                        )}
                        <View style={styles.buttonContainer}>
                            {guild.currentMember && !loading ? (
                                <TouchableText textStyle={styles.getStyle('member')} onPress={onPressLeave}>
                                    {localizer.dictionary.room.show.member.toUpperCase()}
                                </TouchableText>
                            ) : loading ? (
                                <SkeletonContent
                                    isLoading={loading ?? false}
                                    boneColor={appTheme.skeleton.boneColor}
                                    highlightColor={appTheme.skeleton.highlightColor}
                                    animationType="pulse"
                                    layout={[
                                        { ...styles.buttonSkeleton },
                                    ]}
                                />
                            ) : (
                                <TouchableText textStyle={styles.getStyle('join')} onPress={onPressJoin}>
                                    {localizer.dictionary.room.show.join.toUpperCase()}
                                </TouchableText>
                            )}
                        </View>
                        <View>
                            {loading ? (
                                <SkeletonContent
                                    isLoading={loading ?? false}
                                    boneColor={appTheme.skeleton.boneColor}
                                    highlightColor={appTheme.skeleton.highlightColor}
                                    animationType="pulse"
                                    layout={[
                                        { ...styles.postsCountSkeleton },
                                    ]}
                                />
                            ) : (
                                <PostsCountLabel
                                    style={styles.getStyle('postsCount')}
                                    count={guild.activity?.postsCount ?? 0}
                                />
                            )}
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        </Animated.View>
    );
};

export default React.memo(GuildPublicCellComponent, GuildPublicCellPresenter.outputAreEqual);