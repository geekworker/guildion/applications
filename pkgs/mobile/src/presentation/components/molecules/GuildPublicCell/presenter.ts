import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildPublicCellHooks from './hooks';
import GuildPublicCellStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Guild } from '@guildion/core';

namespace GuildPublicCellPresenter {
    export type Input = {
        guild: Guild,
        onPress?: (guild: Guild) => void,
        onPressJoin?: (guild: Guild) => void,
        onPressLeave?: (guild: Guild) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildPublicCellStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        guild: Guild,
        onPress?: () => void,
        onPressJoin?: () => void,
        onPressLeave?: () => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildPublicCellHooks.useStyles({ ...props });
        const {
            onPress,
            onPressJoin,
            onPressLeave,
        } = GuildPublicCellHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress,
            onPressJoin,
            onPressLeave,
        }
    }
}

export default GuildPublicCellPresenter;