import React from 'react';
import GuildPublicCellStyles from './styles';
import type GuildPublicCellPresenter from './presenter';

namespace GuildPublicCellHooks {
    export const useStyles = (props: GuildPublicCellPresenter.Input) => {
        const styles = React.useMemo(() => new GuildPublicCellStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: GuildPublicCellPresenter.Input) => {
        const onPress = React.useCallback(() => {
            if (props.onPress) props.onPress(props.guild);
        }, [props.guild, props.onPress]);
        const onPressJoin = React.useCallback(() => {
            if (props.onPressJoin) props.onPressJoin(props.guild);
        }, [props.guild, props.onPressJoin]);
        const onPressLeave = React.useCallback(() => {
            if (props.onPressLeave) props.onPressLeave(props.guild);
        }, [props.guild, props.onPressLeave]);
        return {
            onPress,
            onPressJoin,
            onPressLeave,
        };
    }
}

export default GuildPublicCellHooks;