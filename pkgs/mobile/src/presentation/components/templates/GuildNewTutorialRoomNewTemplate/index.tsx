import React from 'react';
import GuildNewTutorialRoomNewTemplateComponent from './component';
import GuildNewTutorialRoomNewTemplatePresenter from './presenter';

const GuildNewTutorialRoomNewTemplate = (props: GuildNewTutorialRoomNewTemplatePresenter.Input) => {
    const output = GuildNewTutorialRoomNewTemplatePresenter.usePresenter(props);
    return <GuildNewTutorialRoomNewTemplateComponent {...output} />;
};

export default React.memo(GuildNewTutorialRoomNewTemplate, GuildNewTutorialRoomNewTemplatePresenter.inputAreEqual);