import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildNewTutorialRoomNewTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Guild } from '@guildion/core';
import { number, text } from '@storybook/addon-knobs';

storiesOf('GuildNewTutorialRoomNewTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildNewTutorialRoomNewTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            guild={new Guild({ displayName: text('guildDisplayName', 'test guild'), id: text('guildId',  '@test_guild') })}
        />
    ))