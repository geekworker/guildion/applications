import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const buttonBottom = 60;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            width,
            height,
            position: 'relative',
        },
        background: {
            width,
            height,
            position: 'relative',
            paddingTop: 30,
        },
        foreground: {
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        header: {
            width: style?.width,
        },
        headerInner: {
            width: style?.width,
        },
        guildHeader: {
            width: style?.width,
            height: 72,
        },
        guildMenu: {
            width: style?.width,
            height: 64,
        },
        noRoomContainer: {
            width: style?.width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        noRoomInner: {
            marginTop: 32,
        },
        noRoomTitle: {
            fontSize: 28,
            fontFamily: MontserratFont.Bold,
            textAlign: 'left',
            marginBottom: 8,
            color: appTheme.element.default,
        },
        noRoomDescription: {
            fontSize: 20,
            fontFamily: MontserratFont.Regular,
            textAlign: 'left',
            marginTop: 8,
            marginBottom: 8,
            color: appTheme.element.subp1,
        },
        noRoomButton: {
            width: 168,
            height: 44,
            marginTop: 8,
            borderRadius: 22,
            fontSize: 16,
            backgroundColor: appTheme.background.subp2,
        },
        newRoomButton: {
            position: 'absolute',
            bottom: buttonBottom,
            right: 20,
            color: appTheme.element.link,
            width: 168,
            height: 50,
            borderRadius: 25,
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
            backgroundColor: appTheme.background.subp2,
        },
        newRoomIcon: {
            color: appTheme.element.link,
            width: 26,
            height: 26,
        },
        overlay: {
            width,
            height,
            backgroundColor: appTheme.common.overlayBackgroundColor,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        arrow: {
            width: 32,
            height: 32,
            position: 'absolute',
            bottom: buttonBottom + 60,
            right: 84,
        },
        arrowIcon: {
            width: 32,
            height: 32,
            color: 'white',
        },
        tutorialText: {
            position: 'absolute',
            width: width - 40,
            bottom: buttonBottom + 104,
            right: 20,
            left: 20,
            textAlign: 'right',
            color: 'white',
            fontFamily: MontserratFont.Bold,
            fontSize: 24,
        },
    })
};

type Styles = typeof styles;

export default class GuildNewTutorialRoomNewStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};