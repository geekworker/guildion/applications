import React from 'react';
import { Animated, Text, View } from 'react-native';
import GuildShowHeader from '../../molecules/GuildShowHeader';
import GuildNewTutorialRoomNewTemplatePresenter from './presenter';
import RoundAnimatableButton from '../../atoms/RoundAnimatableButton';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import MembersIcon from '../../Icons/MembersIcon';
import IconMenuList from '../../organisms/IconMenuList';
import { localizer } from '@/shared/constants/Localizer';
import Button, { ButtonStyleType } from '../../atoms/Button';

const GuildNewTutorialRoomNewTemplateComponent = ({ styles, appTheme, animatedStyle, guild, animated, opacity, onPressNew }: GuildNewTutorialRoomNewTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <View style={styles.background}>
                    <View style={styles.headerInner}>
                            <GuildShowHeader guild={guild} style={styles.getStyle('guildHeader')}/>
                            <IconMenuList data={[{
                                title: localizer.dictionary.guild.attr.members,
                                Icon: (props) => <MembersIcon width={props.size} height={props.size} color={props.color} />,
                                countText: `${guild.activity?.membersCount ?? 0}`,
                            }]} style={styles.getStyle('guildMenu')}/>
                            <View style={styles.noRoomContainer}>
                                <View style={styles.noRoomInner}>
                                    <Text style={styles.noRoomTitle}>
                                        {localizer.dictionary.room.home.empty.title}
                                    </Text>
                                    <Text style={styles.noRoomDescription}>
                                        {localizer.dictionary.room.home.empty.description}
                                    </Text>
                                    <Button
                                        styleType={ButtonStyleType.Fill}
                                        style={styles.getStyle('noRoomButton')}
                                        title={localizer.dictionary.room.home.explore}
                                    />
                                </View>
                            </View>
                        </View>
                </View>
                <Animated.View style={{ ...styles.foreground, opacity }}>
                    <Animated.View style={{ ...styles.overlay }} />
                    <Animated.View
                        style={{
                            ...styles.arrow,
                        }}
                    >
                        <AntDesignIcon
                            name={'arrowdown'}
                            size={styles.arrowIcon.width}
                            color={styles.arrowIcon.color}
                        />
                    </Animated.View>
                    <Text style={{ ...styles.tutorialText }}>
                        {localizer.dictionary.guild.new.tutorial.roomNew}
                    </Text>
                </Animated.View>
                <RoundAnimatableButton
                    style={styles.getStyle('newRoomButton')}
                    appTheme={appTheme}
                    title={localizer.dictionary.room.home.create}
                    icon={<EntypoIcon name={'plus'} size={styles.newRoomIcon.width} color={styles.newRoomIcon.color} />}
                    onPress={onPressNew}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(GuildNewTutorialRoomNewTemplateComponent, GuildNewTutorialRoomNewTemplatePresenter.outputAreEqual);