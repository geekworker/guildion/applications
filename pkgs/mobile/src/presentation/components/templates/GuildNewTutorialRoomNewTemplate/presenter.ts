import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildNewTutorialRoomNewTemplateHooks from './hooks';
import GuildNewTutorialRoomNewTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Guild } from '@guildion/core';
import { Animated } from 'react-native';

namespace GuildNewTutorialRoomNewTemplatePresenter {
    export type Input = {
        guild: Guild,
        onPressNew?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildNewTutorialRoomNewTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        guild: Guild,
        opacity: Animated.Value | Animated.AnimatedInterpolation,
        animated: boolean,
        onPressNew?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildNewTutorialRoomNewTemplateHooks.useStyles({ ...props });
        const {
            opacity,
            animated,
        } = GuildNewTutorialRoomNewTemplateHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            opacity,
            animated,
        }
    }
}

export default GuildNewTutorialRoomNewTemplatePresenter;