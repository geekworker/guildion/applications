import React from 'react';
import GuildNewTutorialRoomNewTemplateStyles from './styles';
import type GuildNewTutorialRoomNewTemplatePresenter from './presenter';
import { Animated } from 'react-native';

namespace GuildNewTutorialRoomNewTemplateHooks {
    export const useStyles = (props: GuildNewTutorialRoomNewTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new GuildNewTutorialRoomNewTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: GuildNewTutorialRoomNewTemplatePresenter.Input) => {
        const opacity = React.useRef(new Animated.Value(0)).current;
        const [animated, setAnimated] = React.useState(false);
        React.useEffect(() => {
            Animated.spring(opacity, {
                toValue: 1,
                overshootClamping: false,
                useNativeDriver: true,
                delay: 500,
            }).start((result) => {
                if (result.finished) setAnimated(true);
            });
        }, []);
        return {
            animated,
            opacity,
        }
    }
}

export default GuildNewTutorialRoomNewTemplateHooks;