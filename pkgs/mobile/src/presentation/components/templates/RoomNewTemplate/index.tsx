import React from 'react';
import RoomNewTemplateComponent from './component';
import RoomNewTemplatePresenter from './presenter';

const RoomNewTemplate = (props: RoomNewTemplatePresenter.Input) => {
    const output = RoomNewTemplatePresenter.usePresenter(props);
    return <RoomNewTemplateComponent {...output} />;
};

export default React.memo(RoomNewTemplate, RoomNewTemplatePresenter.inputAreEqual);