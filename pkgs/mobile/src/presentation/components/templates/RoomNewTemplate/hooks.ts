import React from 'react';
import RoomNewTemplateStyles from './styles';
import type RoomNewTemplatePresenter from './presenter';

namespace RoomNewTemplateHooks {
    export const useStyles = (props: RoomNewTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new RoomNewTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomNewTemplatePresenter.Input) => {
        return {};
    }
}

export default RoomNewTemplateHooks;