import React from 'react';
import { Animated, SectionList, View, ViewBase } from 'react-native';
import { Discovery } from '@guildion/core';
import DiscoveryCell from '../../molecules/DiscoveryCell';
import ExploreHeader from '../../molecules/ExploreHeader';
import ExploreSearchContent from '../../molecules/ExploreSearchContent';
import ExploreTemplatePresenter from './presenter';

const ExploreTemplateComponent = ({ styles, appTheme, animatedStyle, items, onPress, loading, onScroll, sectionListRef, translationScroll, headerRef, onFocus, onBlur, onChangeText, onSubmitEditing, query }: ExploreTemplatePresenter.Output) => {
    const renderCell = (props: ExploreTemplatePresenter.SectionListRenderInfo) =>
        props.section.type == 'Discovery' ? (
            <DiscoveryCell
                key={props.section.key ?? props.index}
                discovery={props.item as Discovery}
                style={styles.getStyle('cell')}
                loading={loading}
                onPress={onPress}
            />
        ) : (
            <View ref={headerRef} >
                <ExploreHeader
                    style={styles.getStyle('header')}
                />
            </View>
        );
    const renderSectionHeader = (props: { section: ExploreTemplatePresenter.SectionListInfo }) =>
        props.section.type == 'Discovery' ? (
            <>
                <ExploreSearchContent
                    style={styles.getStyle('search')}
                    transition={translationScroll}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                    query={query}
                />
            </>
        ) : (
            <>
            </>
        );
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <SectionList
                    style={styles.container}
                    ref={sectionListRef}
                    directionalLockEnabled
                    automaticallyAdjustContentInsets={false}
                    alwaysBounceVertical={false}
                    alwaysBounceHorizontal={false}
                    keyboardDismissMode="on-drag"
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    snapToAlignment={'center'}  
                    sections={items.toArray()}
                    renderItem={renderCell}
                    renderSectionHeader={renderSectionHeader}
                    keyExtractor={(item: Discovery | string, index: number) => String(index)}
                    onScroll={onScroll}
                    scrollEventThrottle={16}
                    stickySectionHeadersEnabled
                    ListFooterComponent={(
                        <View style={styles.footerMargin} />
                    )}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(ExploreTemplateComponent, ExploreTemplatePresenter.outputAreEqual);