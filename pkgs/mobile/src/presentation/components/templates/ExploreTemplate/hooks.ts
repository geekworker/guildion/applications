import React from 'react';
import ExploreTemplateStyles, { BOTTOM_MARGIN } from './styles';
import type ExploreTemplatePresenter from './presenter';
import { Discoveries, Discovery, DiscoveryStatus } from '@guildion/core';
import uuid from 'react-native-uuid';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { List } from 'immutable';
import { Animated, NativeScrollEvent, NativeSyntheticEvent, SectionList, SectionListData, View } from 'react-native';

namespace ExploreTemplateHooks {
    export const useStyles = (props: ExploreTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new ExploreTemplateStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const loadingMoreMockCount: number = 2;
    export const useLoadingMoreMock = (props: ExploreTemplatePresenter.Input): Discoveries => {
        const [mocks, setMocks] = React.useState<Discoveries>(new Discoveries([]));
        React.useEffect(() => {
            props.loadingMore && mocks.size == 0 ?
            setMocks(new Discoveries(
                Array.from(Array(loadingMoreMockCount).keys()).map((i: number) => 
                    new Discovery({ id: `${uuid.v4()}-${i}`, status: DiscoveryStatus.LOADING })
                )
            )) :
            setMocks(new Discoveries([]))
        }, [props.loadingMore]);
        return mocks.size > 0 && props.loadingMore == LoadingMoreStatus.INCREMENT ?
            props.discoveries.concat(mocks) :
            mocks.size > 0 && props.loadingMore == LoadingMoreStatus.DECREMENT ?
            mocks.concat(props.discoveries) :
            props.discoveries;
    }

    export const useSkeletonMock = (): Discoveries => {
        const rooms = React.useMemo(() => new Discoveries([
            new Discovery({ id: '`skeleton1`' }),
            new Discovery({ id: '`skeleton2`' }),
            new Discovery({ id: '`skeleton3`' }),
            new Discovery({ id: '`skeleton4`' }),
            new Discovery({ id: '`skeleton5`' }),
            new Discovery({ id: '`skeleton6`' }),
        ]), []);
        return rooms;
    }

    export const useState = (props: ExploreTemplatePresenter.Input) => {
        const _discoveries = ExploreTemplateHooks.useLoadingMoreMock(props);
        const mockDiscoveries = ExploreTemplateHooks.useSkeletonMock();
        const discoveries = props.loading ? mockDiscoveries : _discoveries;
        const items: List<ExploreTemplatePresenter.SectionListInfo> = React.useMemo(() => {
            const sections: Array<ExploreTemplatePresenter.SectionListInfo> = [
                {
                    data: [''],
                    type: 'Header',
                },
                {
                    data: discoveries.toArray(),
                    type: 'Discovery',
                },
            ];
            return List(sections);
        }, [discoveries]);

        const translationScroll = React.useRef(new Animated.Value(0)).current;
        const sectionListRef = React.useRef<SectionList<Discovery | string, { type: 'Discovery' | 'Header' }>>(null);
        const headerRef = React.useRef<View>(null);
        const [shouldScrollOnFocuse, setShouldScrollOnFocuse] = React.useState(true);
        const [offsetY, setOffsetY] = React.useState(0);
        const [cacheOffsetY, setCacheOffsetY] = React.useState<number | undefined>(undefined);
        const onFocus = React.useCallback(() => {
            if(shouldScrollOnFocuse) {
                setCacheOffsetY(offsetY);
                sectionListRef.current?.scrollToLocation({
                    sectionIndex: 1,
                    itemIndex: 0,
                });
            };
        }, [shouldScrollOnFocuse, offsetY]);
        const onBlur = React.useCallback(() => {
            if (cacheOffsetY !== undefined) sectionListRef.current?.scrollToLocation({
                viewOffset: cacheOffsetY,
                itemIndex: 0,
                sectionIndex: 0,
            });
            setCacheOffsetY(undefined);
        }, [cacheOffsetY]);
        const onScroll = React.useCallback((event: NativeSyntheticEvent<NativeScrollEvent>) => {
            const contentOffsetY = event.nativeEvent.contentOffset.y;
            const visualHeight = event.nativeEvent.layoutMeasurement.height;
            const contentHeight = event.nativeEvent.contentSize.height;
            setOffsetY(contentOffsetY);
            headerRef.current?.measure((x, y, width, height, pageX, pageY) => {
                translationScroll.setValue(Math.min(Math.max(contentOffsetY / height, 0), 1));
                setShouldScrollOnFocuse(contentOffsetY < height);
            });
            if (contentOffsetY + visualHeight > contentHeight - BOTTOM_MARGIN && props.onScrollToBottom) {
                props.onScrollToBottom();
            } else if (contentOffsetY < 0 && props.onScrollToTop) {
                props.onScrollToTop();
            }
        }, [props.onScrollToBottom, props.onScrollToTop]);

        return {
            items,
            sectionListRef,
            onScroll,
            translationScroll,
            headerRef,
            onFocus,
            onBlur,
        };
    }
}

export default ExploreTemplateHooks;