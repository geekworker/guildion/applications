import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

export const BOTTOM_MARGIN = 700;

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const paddingTop = style?.getAsNumber('paddingTop') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            paddingTop: 0,
        },
        inner: {
            width,
            height,
            position: 'relative',
        },
        header: {
            width,
            // position: 'absolute',
            // top: 0,
            // left: 0,
            // right: 0,
        },
        search: {
            width: width,
            paddingLeft: 12,
            paddingRight: 12,
            paddingTop,
        },
        scrollview: {
            width,
            height,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        scrollView: {
            width,
            position: 'relative',
        },
        cellContainer: {
            backgroundColor: style?.backgroundColor ?? appTheme.common.transparent,
        },
        cell: {
            width: width - 24,
            height: 240,
            marginTop: 8,
            marginBottom: 8,
            marginLeft: 12,
            marginRight: 12,
        },
        loadingCell: {
            width: width - 24,
            height: 240,
            marginTop: 8,
            marginBottom: 8,
            marginLeft: 12,
            marginRight: 12,
        },
        footerMargin: {
            marginBottom: BOTTOM_MARGIN,
        },
    });
};

type Styles = typeof styles;

export default class ExploreTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};