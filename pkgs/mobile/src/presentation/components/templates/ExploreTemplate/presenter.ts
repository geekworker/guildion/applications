import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Animated, View } from 'react-native';
import StyleProps from '@/shared/interfaces/StyleProps';
import ExploreTemplateHooks from './hooks';
import ExploreTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Discoveries, Discovery } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { List } from 'immutable';
import { NativeScrollEvent, NativeSyntheticEvent, SectionList, SectionListData, SectionListRenderItemInfo } from 'react-native';
import { SearchInputProps } from '../../atoms/SearchInput';

namespace ExploreTemplatePresenter {
    export type Input = {
        discoveries: Discoveries,
        onPress?: (discovery: Discovery) => void,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        onScrollToTop?: () => void,
        onScrollToBottom?: () => void,
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
        query?: string,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: ExploreTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        onPress?: (discovery: Discovery) => void,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        items: List<SectionListInfo>,
        sectionListRef: React.RefObject<SectionList<Discovery | string, { type: 'Discovery' | 'Header' }>>,
        onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        translationScroll: Animated.Value | Animated.AnimatedInterpolation,
        headerRef: React.RefObject<View>,
        onFocus?: () => void,
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
        query?: string,
    };

    export type SectionListInfo = SectionListData<Discovery | string, { type: 'Discovery' | 'Header' }>;
    export type SectionListRenderInfo = SectionListRenderItemInfo<Discovery | string, { type: 'Discovery' | 'Header' }>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = ExploreTemplateHooks.useStyles({ ...props });
        const {
            items,
            sectionListRef,
            onScroll,
            headerRef,
            translationScroll,
            onFocus,
            onBlur,
        } = ExploreTemplateHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            items,
            sectionListRef,
            onScroll,
            headerRef,
            translationScroll,
            onFocus,
            onBlur,
        }
    }
}

export default ExploreTemplatePresenter;