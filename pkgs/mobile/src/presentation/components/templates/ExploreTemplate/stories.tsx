import { storiesOf } from '@storybook/react-native';
import React from 'react';
import ExploreTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { mockDiscoveries } from '../../organisms/DiscoveriesList/stories';
import { number } from '@storybook/addon-knobs';

storiesOf('ExploreTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <ExploreTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            discoveries={mockDiscoveries}
        />
    ))
    .add('Loading', () => (
        <ExploreTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            discoveries={mockDiscoveries}
            loading
        />
    ))