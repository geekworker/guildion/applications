import React from 'react';
import ExploreTemplateComponent from './component';
import ExploreTemplatePresenter from './presenter';

const ExploreTemplate = (props: ExploreTemplatePresenter.Input) => {
    const output = ExploreTemplatePresenter.usePresenter(props);
    return <ExploreTemplateComponent {...output} />;
};

export default React.memo(ExploreTemplate, ExploreTemplatePresenter.inputAreEqual);