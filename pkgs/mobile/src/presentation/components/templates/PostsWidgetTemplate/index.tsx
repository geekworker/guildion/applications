import React from 'react';
import PostsWidgetTemplateComponent from './component';
import PostsWidgetTemplatePresenter from './presenter';

const PostsWidgetTemplate = (props: PostsWidgetTemplatePresenter.Input) => {
    const output = PostsWidgetTemplatePresenter.usePresenter(props);
    return <PostsWidgetTemplateComponent {...output} />;
};

export default React.memo(PostsWidgetTemplate, PostsWidgetTemplatePresenter.inputAreEqual);