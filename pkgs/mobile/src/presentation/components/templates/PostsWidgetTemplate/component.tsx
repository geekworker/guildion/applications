import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { SortDirection } from '@guildion/core';
import React from 'react';
import { View } from 'react-native';
import PostsList from '../../organisms/PostsList';
import PostsWidgetTemplatePresenter from './presenter';

const PostsWidgetTemplateComponent = ({ styles, appTheme, loadingMore, loading, posts, onPressSender, onPressSeeMore, onPressReaction, onPressComment, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, onScrollToBottom }: PostsWidgetTemplatePresenter.Output) => {
    return (
        <View style={styles.container}>
            <PostsList
                posts={posts}
                style={styles.getStyle('list')}
                loadingMore={loading ? LoadingMoreStatus.INCREMENT : loadingMore}
                onScrollToBottom={onScrollToBottom}
                onPressSender={onPressSender}
                onPressSeeMore={onPressSeeMore}
                onPressReaction={onPressReaction}
                onPressComment={onPressComment}
                onPressFile={onPressFile}
                onPressURL={onPressURL}
                onPressEmail={onPressEmail}
                onPressPhoneNumber={onPressPhoneNumber}
                onPressMentionMember={onPressMentionMember}
                onPressMentionRoom={onPressMentionRoom}
                sortDirection={SortDirection.ASC}
                ListFooterComponent={(
                    <View style={styles.footerMargin} />
                )}
            />
        </View>
    );
};

export default React.memo(PostsWidgetTemplateComponent, PostsWidgetTemplatePresenter.outputAreEqual);