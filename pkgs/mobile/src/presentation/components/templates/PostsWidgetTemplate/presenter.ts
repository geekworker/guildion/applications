import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import PostsWidgetTemplateHooks from "./hooks";
import PostsWidgetTemplateStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Post, Posts } from "@guildion/core";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";

namespace PostsWidgetTemplatePresenter {
    export type Input = {
        posts: Posts,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        onScrollToBottom?: () => void,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PostsWidgetTemplateStyles,
        appTheme: AppTheme,
        posts: Posts,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        onScrollToBottom?: () => void,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PostsWidgetTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default PostsWidgetTemplatePresenter;