import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PostsWidgetTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { mockPosts } from '../../organisms/PostsList/stories';

storiesOf('PostsWidgetTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PostsWidgetTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 724),
            })}
            posts={mockPosts}
        />
    ))