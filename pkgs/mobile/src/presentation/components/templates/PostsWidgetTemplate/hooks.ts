import React from "react";
import PostsWidgetTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { useSafeAreaInsets } from "react-native-safe-area-context";

namespace PostsWidgetTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const insets = useSafeAreaInsets();
        const styles = React.useMemo(() => new PostsWidgetTemplateStyles({ ...props, insets }), [
            props,
            insets,
        ]);
        return styles;
    }
}

export default PostsWidgetTemplateHooks;