import React from 'react';
import { GestureResponderEvent, KeyboardAvoidingView, Text, View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import IconButton from '../../atoms/IconButton';
import AccountNewTemplateStyles from './styles';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import TextField from '../../atoms/TextField';
import CheckBoxField from '../../atoms/CheckBoxField';
import Button from '../../atoms/Button';
import { localizer } from '@/shared/constants/Localizer';
import StyleProps from '@/shared/interfaces/StyleProps';
import TouchableText from '../../atoms/TouchableText';
import DismissKeyboardView from '../../atoms/DismissKeyboardView';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    url: string,
    username?: string,
    onPressPicker?: () => void,
    onChangeUsername?: (username?: string) => void,
    isAcceptTerms?: boolean,
    isAcceptPrivacyPolicy?: boolean,
    onChangeTerms?: (value: boolean) => void,
    onChangePrivacyPolicy?: (value: boolean) => void,
    sendable?: boolean,
    usernameError?: string,
    loading?: boolean,
    onSubmit?: (event: GestureResponderEvent) => void,
} & StyleProps;

const AccountNewTemplate: React.FC<Props> = ({
    style,
    appTheme,
    url,
    username,
    onPressPicker,
    onChangeUsername,
    isAcceptTerms,
    isAcceptPrivacyPolicy,
    onChangeTerms,
    onChangePrivacyPolicy,
    sendable,
    usernameError,
    loading,
    onSubmit,
}) => {
    const styles = React.useMemo(() => new AccountNewTemplateStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    const termsAgreeText = localizer.dictionary.account.new.agree(localizer.dictionary.account.attr.terms);
    const privacyPolicyAgreeText = localizer.dictionary.account.new.agree(localizer.dictionary.account.attr.privacyPolicy);
    const termsAgreeTextSplit = termsAgreeText.split(localizer.dictionary.account.attr.terms);
    const privacyPolicyAgreeTextSplit = privacyPolicyAgreeText.split(localizer.dictionary.account.attr.privacyPolicy);
    return (
        <KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={-(Number(style?.height ?? 0) / 3 - 40)} >
            <DismissKeyboardView>
                <View style={styles.wrapper} >
                    <View style={styles.profile}>
                        <AsyncImage
                            url={url}
                            style={styles.getStyle('image')}
                        />
                        <View style={styles.cameraButtonContainer}>
                            <IconButton
                                style={styles.getStyle('cameraButton')}
                                onPress={onPressPicker}
                                icon={<FontAwesomeIcon size={24} color={styles.cameraButton.color} name={'camera'} />}
                            />
                        </View>
                    </View>
                    <View style={styles.usernameField}>
                        <TextField
                            onChangeText={onChangeUsername}
                            defaultValue={username}
                            value={username}
                            label={localizer.dictionary.account.attr.username}
                            errorMessage={usernameError}
                        />
                    </View>
                    <View style={styles.checkboxContainer}>
                        <CheckBoxField
                            style={styles.getStyle('checkboxField')}
                            value={isAcceptTerms ?? false}
                            onPress={onChangeTerms}
                        />
                        <View style={styles.checkboxTextContainer}>
                            <Text style={styles.checkboxText}>
                                {termsAgreeTextSplit[0]}
                            </Text>
                            <TouchableText style={styles.getStyle('checkboxLinkContainer')} textStyle={styles.getStyle('checkboxLink')}>
                                {localizer.dictionary.account.attr.terms}
                            </TouchableText>
                            <Text style={styles.checkboxText}>
                                {termsAgreeTextSplit[1]}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.checkboxContainer}>
                        <CheckBoxField
                            style={styles.getStyle('checkboxField')}
                            value={isAcceptPrivacyPolicy ?? false}
                            onPress={onChangePrivacyPolicy}
                        />
                        <View style={styles.checkboxTextContainer}>
                            <Text style={styles.checkboxText}>
                                {privacyPolicyAgreeTextSplit[0]}
                            </Text>
                            <TouchableText style={styles.getStyle('checkboxLinkContainer')} textStyle={styles.getStyle('checkboxLink')}>
                                {localizer.dictionary.account.attr.privacyPolicy}
                            </TouchableText>
                            <Text style={styles.checkboxText}>
                                {privacyPolicyAgreeTextSplit[1]}
                            </Text>
                        </View>
                    </View>
                    <Button
                        styleType={'fill'}
                        style={styles.getStyle('submit')}
                        loading={loading}
                        disabled={!sendable}
                        onPress={onSubmit}
                        title={localizer.dictionary.account.authentication.submit}
                    />
                </View>
            </DismissKeyboardView>
        </KeyboardAvoidingView>
    )
};

export default React.memo(AccountNewTemplate, compare);