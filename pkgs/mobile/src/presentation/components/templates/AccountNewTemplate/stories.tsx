import { storiesOf } from '@storybook/react-native';
import { boolean, number, text } from '@storybook/addon-knobs';
import React from 'react';
import AccountNewTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('AccountNewTemplate', module)
    .addDecorator((getStory) => 
        <StoreProvider>
            <CenterView>
                {getStory()}
            </CenterView>
        </StoreProvider>
    )
    .add('Default', () => (
        <AccountNewTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            url={text('url', 'https://yt3.ggpht.com/ytc/AKedOLTDLauymQQXmfG3S_r3ZTzw8ds1VstwhcwvHU_8OA=s176-c-k-c0x00ffffff-no-rj')}
            username={text('username', '')}
            usernameError={text('usernameError', '')}
            loading={boolean('loading', false)}
            sendable={boolean('sendable', false)}
        />
    ))
