import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.root,
            paddingTop: 30,
        },
        profile: {
            width: 140,
            marginLeft: (Number(style?.width ?? 0) - 140) / 2,
            marginRight: (Number(style?.width ?? 0) - 140) / 2,
            marginBottom: 0,
            position: 'relative',
        },
        image: {
            width: 140,
            height: 140,
            borderRadius: 70,
        },
        cameraButtonContainer: {
            width: 44,
            height: 44,
            borderRadius: 22,
            position: 'absolute',
            bottom: 0,
            right: 0,
        },
        cameraButton: {
            width: 44,
            height: 44,
            borderRadius: 22,
            color: appTheme.element.default,
            backgroundColor: appTheme.background.subp2,
            borderColor: appTheme.background.subp3,
            borderWidth: 1,
        },
        usernameField: {
            width: Number(style?.width ?? 0) - 50,
            marginLeft: 25,
            marginRight: 25,
            marginBottom: 10,
            marginTop: 10,
        },
        checkboxContainer: {
            width: Number(style?.width ?? 0) - 80,
            height: 20,
            marginLeft: 40,
            marginRight: 40,
            marginBottom: 20,
            flexDirection: 'row',
            alignItems: 'center',
        },
        checkboxField: {
            width: 26,
            height: 26,
        },
        checkboxTextContainer: {
            height: 20,
            marginLeft: 10,
            flexDirection: 'row',
            alignItems: 'flex-start',
            justifyContent: 'center',
        },
        checkboxText: {
            fontFamily: MontserratFont.SemiBold,
            fontSize: 14,
            color: appTheme.element.default,
            textAlign: "left",
        },
        checkboxLinkContainer: {
        },
        checkboxLink: {
            fontFamily: MontserratFont.SemiBold,
            fontSize: 14,
            color: appTheme.element.link,
            textAlign: "left",
            textDecorationLine: 'underline',
        },
        submit: {
            width: 140,
            height: 50,
            marginTop: 20,
            marginLeft: (Number(style?.width ?? 0) - 140) / 2,
            marginRight: (Number(style?.width ?? 0) - 140) / 2,
        },
    })
};

type Styles = typeof styles;

export default class AccountNewTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};