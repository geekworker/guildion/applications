import React from "react";
import RoomShowTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import MembersIcon from "../../Icons/MembersIcon";
import SyncVisionIcon from "../../Icons/SyncVisionIcon";
import { IconSwitchData } from "../../organisms/IconSwitchList";
import FontistoIcon from "react-native-vector-icons/Fontisto";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import FeatherIcon from "react-native-vector-icons/Feather";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { Animated, Easing } from "react-native";
import { useSplitView } from "@/shared/hooks/useSplitView";
import type RoomShowTemplatePresenter from "./presenter";
import type RoomControlPanelTemplatePresenter from "../RoomControlPanelTemplate/presenter";
import { useSafeAreaInsets } from "react-native-safe-area-context";

namespace RoomShowTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const shouldSplit = useSplitView();
        const styles = React.useMemo(() => new RoomShowTemplateStyles({ ...props, shouldSplit }), [
            props,
            shouldSplit,
        ]);
        return styles;
    }

    export const useRoomControlPanel = (props: RoomShowTemplatePresenter.Input): RoomControlPanelTemplatePresenter.Input => {
        return {
        }
    }

    export const useRoomTopSwitch = (props?: { showSyncVision?: boolean, showMembers?: boolean }) => {
        const [showSyncVision, $setShowSyncVision] = React.useState<boolean>(!!props?.showSyncVision);
        const [showMembers, $setShowMembers] = React.useState<boolean>(!!props?.showMembers);
        const setShowSyncVision = (state: boolean) => {
            $setShowSyncVision(state);
        };
        const setShowMembers = (state: boolean) => {
            $setShowMembers(state);
        };
        React.useEffect(() => {
            showMembers != !!props?.showMembers && setShowMembers(!!props?.showMembers);
        }, [props?.showMembers]);
        React.useEffect(() => {
            showSyncVision != !!props?.showSyncVision && setShowSyncVision(!!props?.showSyncVision);
        }, [props?.showSyncVision]);
        return {
            showSyncVision,
            setShowSyncVision,
            showMembers,
            setShowMembers,
        }
    }

    export const useRoomBottomSwitch = (props?: { showTextChat?: boolean, showMediaLibrary?: boolean, showPosts?: boolean, showSetting?: boolean }) => {
        const [showTextChat, $setShowTextChat] = React.useState<boolean>(!!props?.showTextChat);
        const [showMediaLibrary, $setShowMediaLibrary] = React.useState<boolean>(!!props?.showMediaLibrary);
        const [showPosts, $setShowPosts] = React.useState<boolean>(!!props?.showPosts);
        const [showSetting, $setShowSetting] = React.useState<boolean>(!!props?.showSetting);
        const setShowTextChat = (state: boolean) => {
            $setShowTextChat(state);
            !!state && $setShowMediaLibrary(false);
            !!state && $setShowPosts(false);
            !!state && $setShowSetting(false);
        }
        const setShowMediaLibrary = (state: boolean) => {
            !!state && $setShowTextChat(false);
            $setShowMediaLibrary(state);
            !!state && $setShowPosts(false);
            !!state && $setShowSetting(false);
        }
        const setShowPosts = (state: boolean) => {
            !!state && $setShowTextChat(false);
            !!state && $setShowMediaLibrary(false);
            $setShowPosts(state);
            !!state && $setShowSetting(false);
        }
        const setShowSetting = (state: boolean) => {
            !!state && $setShowTextChat(false);
            !!state && $setShowMediaLibrary(false);
            !!state && $setShowPosts(false);
            $setShowSetting(state);
        }
        React.useEffect(() => {
            showTextChat != !!props?.showTextChat && setShowTextChat(!!props?.showTextChat);
        }, [props?.showTextChat]);
        React.useEffect(() => {
            showMediaLibrary != !!props?.showMediaLibrary && setShowMediaLibrary(!!props?.showMediaLibrary);
        }, [props?.showMediaLibrary]);
        React.useEffect(() => {
            showPosts != !!props?.showPosts && setShowPosts(!!props?.showPosts);
        }, [props?.showPosts]);
        React.useEffect(() => {
            showSetting != !!props?.showSetting && setShowSetting(!!props?.showSetting);
        }, [props?.showSetting]);
        return {
            showTextChat,
            setShowTextChat,
            showMediaLibrary,
            setShowMediaLibrary,
            showPosts,
            setShowPosts,
            showSetting,
            setShowSetting,
        }
    }

    export const useIconSwitchListDate = (props?: { showSyncVision?: boolean, showMembers?: boolean, showTextChat?: boolean, showMediaLibrary?: boolean, showPosts?: boolean, showSetting?: boolean }) => {
        const {
            showSyncVision,
            setShowSyncVision,
            showMembers,
            setShowMembers,
        } = RoomShowTemplateHooks.useRoomTopSwitch({ ...props });
        const {
            showTextChat,
            setShowTextChat,
            showMediaLibrary,
            setShowMediaLibrary,
            showPosts,
            setShowPosts,
            showSetting,
            setShowSetting,
        } = RoomShowTemplateHooks.useRoomBottomSwitch({ ...props });
        const data: IconSwitchData = React.useMemo(() => [
            {
                title: 'MEMBER',
                Icon: (props) => <MembersIcon width={props.size} height={props.size} color={props.color} />,
                value: showMembers,
                onPress: () => setShowMembers(!showMembers),
            },
            {
                title: 'SHARE',
                Icon: (props) => <SyncVisionIcon width={props.size} height={props.size} color={props.color} />,
                value: showSyncVision,
                onPress: () => setShowSyncVision(!showSyncVision),
            },
            'border',
            {
                title: 'CHAT',
                Icon: (props) => <FontistoIcon {...props} name={'hashtag'} />,
                value: showTextChat,
                onPress: () => setShowTextChat(!showTextChat),
            },
            {
                title: 'UPLOAD',
                Icon: (props) => <FontAwesomeIcon {...props} name={'folder'} />,
                value: showMediaLibrary,
                onPress: () => setShowMediaLibrary(!showMediaLibrary),
            },
            {
                title: 'POST',
                Icon: (props) => <MaterialIcon {...props} name={'article'} />,
                value: showPosts,
                onPress: () => setShowPosts(!showPosts),
            },
            {
                title: 'SETTING',
                Icon: (props) => <FeatherIcon {...props} name={'more-horizontal'} />,
                value: showSetting,
                onPress: () => setShowSetting(!showSetting),
            },
        ], [props]);
        return {
            data,
            state: {
                showSyncVision,
                setShowSyncVision,
                showMembers,
                setShowMembers,
                showTextChat,
                setShowTextChat,
                showMediaLibrary,
                setShowMediaLibrary,
                showPosts,
                setShowPosts,
                showSetting,
                setShowSetting,
            },
        };
    }

    export const useTabInitialRouteName = (props: { showTextChat: boolean, showMediaLibrary: boolean, showPosts: boolean, showSetting: boolean }): (keyof RoomWidgetTabsNavigatorStackParams) | undefined => {
        const tabInitialRouteName = React.useMemo<(keyof RoomWidgetTabsNavigatorStackParams) | undefined>(() => {
            switch(true) {
            default: return undefined;
            case !!props.showTextChat: return 'TextChatWidget';
            case !!props.showMediaLibrary: return 'MediaLibraryWidgetNavigator';
            case !!props.showPosts: return 'RoomPostsWidgetNavigator';
            case !!props.showSetting: return 'RoomSettingWidget';
            }
        }, [props]);
        return tabInitialRouteName;
    }

    export const useState = (props: RoomShowTemplatePresenter.Input & { styles: RoomShowTemplateStyles }) => {
        const translateMembersList = React.useRef(new Animated.Value(1)).current;
        const translateSubMembersList = React.useRef(new Animated.Value(0)).current;
        const translateSyncVision = React.useRef(new Animated.Value(1)).current;
        const translateWidgetHeight = React.useRef(new Animated.Value(props.styles.getWidgetHeight(props))).current;

        React.useEffect(() => {
            Animated.timing(translateMembersList, {
                toValue: props.showMembers ? 1 : 0,
                easing: Easing.linear,
                duration: 100,
                useNativeDriver: true,
            }).start();
        }, [props.showMembers]);

        React.useEffect(() => {
            Animated.timing(translateSubMembersList, {
                toValue: props.showMembers && !props.showSyncVision ? 1 : 0,
                easing: Easing.linear,
                duration: 100,
                useNativeDriver: true,
            }).start();

            Animated.timing(translateWidgetHeight, {
                toValue: props.styles.getWidgetHeight(props),
                easing: Easing.linear,
                duration: 100,
                useNativeDriver: false,
            }).start();
        }, [props.showMembers, props.showSyncVision]);

        React.useEffect(() => {
            Animated.timing(translateSyncVision, {
                toValue: props.showSyncVision ? 1 : 0,
                easing: Easing.linear,
                duration: 100,
                useNativeDriver: true,
            }).start();
        }, [props.showSyncVision]);

        return {
            translateMembersList,
            translateSubMembersList,
            translateSyncVision,
            translateWidgetHeight,
        };
    }
}

export default RoomShowTemplateHooks