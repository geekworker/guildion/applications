import React from 'react';
import RoomShowTemplateComponent from './component';
import RoomShowTemplatePresenter from './presenter';

const RoomShowTemplate = (props: RoomShowTemplatePresenter.Input) => {
    const output = RoomShowTemplatePresenter.usePresenter(props);
    return <RoomShowTemplateComponent {...output} />;
};

export default React.memo(RoomShowTemplate, RoomShowTemplatePresenter.inputAreEqual);