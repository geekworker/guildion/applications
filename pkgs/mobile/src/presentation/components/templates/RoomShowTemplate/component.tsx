import React from 'react';
import { Animated, View } from 'react-native';
import RoomHeader from '../../molecules/RoomHeader';
import SyncVisionEmptyView from '../../molecules/SyncVisionEmptyView';
import IconSwitchList from '../../organisms/IconSwitchList';
import RoomShowTemplatePresenter from './presenter';;
import LinearGradient from 'react-native-linear-gradient';
import MembersList from '../../organisms/MembersList';
import RoomWidgetsNavigator from '../../navigators/RoomWidgetsNavigator';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import RoomControlButtonProvider from '../../molecules/RoomControlButton/provider';
import RoomControlButton from '../../molecules/RoomControlButton';

const AnimatedLinearGradient = Animated.createAnimatedComponent(LinearGradient);

const RoomShowTemplateComponent: React.FC<RoomShowTemplatePresenter.Output> = ({ styles, appTheme, guild, room, iconSwitchListProps, tabInitialRouteName, onChangeTabRouteName, translateMembersList, translateSyncVision, translateSubMembersList, translateWidgetHeight, onPressGuild, connectingMembers, loading, panelProps }) => {
    return (
        <View style={styles.container}>
            <RoomControlButtonProvider>
                <Animated.View style={{ ...styles.topContainer, opacity: translateSyncVision }}>
                    <SyncVisionEmptyView loading={loading} style={styles.getStyle('empty')} appTheme={appTheme} isAppeared={true} />
                    <AnimatedLinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0, y: 1 }}
                        colors={[
                            'rgba(0, 0, 0, 0)',
                            'rgba(0, 0, 0, 0.8)',
                        ]}
                        style={{
                            ...styles.membersContainer,
                            opacity: translateMembersList,
                        }}
                    >
                        {!loading && (
                            <MembersList
                                style={styles.getStyle('membersList')}
                                members={connectingMembers}
                                animatedStyle={new AnimatedStyle({
                                    transform: [{
                                        translateY: translateMembersList.interpolate({
                                            inputRange: [0, 1],
                                            outputRange: [styles.membersList.height, 0],
                                        })
                                    }]
                                })}
                                loading={loading}
                            />
                        )}
                    </AnimatedLinearGradient>
                </Animated.View>
                <Animated.View
                    style={{
                        ...styles.subMembersContainer,
                        opacity: translateSubMembersList,
                    }}
                >
                    {!loading && (
                        <MembersList
                            style={styles.getStyle('subMembersList')}
                            members={connectingMembers}
                            loading={loading}
                        />
                    )}
                </Animated.View>
                <Animated.View style={{
                    ...styles.bottomContainer,
                    transform: [{
                        translateY: Animated.add(
                            translateSyncVision.interpolate({
                                inputRange: [0, 1],
                                outputRange: [-styles.topContainer.height + styles.headerContainer.height, 0]
                            }),
                            translateSubMembersList.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, styles.subMembersContainer.height]
                            }),
                        ),
                    }],
                }}>
                    <IconSwitchList
                        data={iconSwitchListProps}
                        style={styles.getStyle('tabBar')}
                        appTheme={appTheme}
                        loading={loading}
                    />
                    <RoomWidgetsNavigator
                        style={styles.getStyle('pages')}
                        animatedStyle={new AnimatedStyle({ height: translateWidgetHeight })}
                        appTheme={appTheme}
                        tabInitialRouteName={tabInitialRouteName}
                        onChangeTabRouteName={onChangeTabRouteName}
                        room={room}
                        loading={loading}
                    />
                </Animated.View>
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={[
                        'rgba(0, 0, 0, 0.8)',
                        'rgba(0, 0, 0, 0)',
                    ]}
                    style={styles.headerContainer}
                >
                    <Animated.View style={{
                        ...styles.headerDarkFilter,
                        opacity: translateSyncVision.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 0],
                        })
                    }}/>
                    {!loading && (
                        <RoomHeader
                            appTheme={appTheme}
                            style={styles.getStyle('header')}
                            guild={guild}
                            room={room}
                            onPressGuild={onPressGuild}
                            loading={loading}
                        />
                    )}
                </LinearGradient>
                {!loading && (
                    <RoomControlButton
                        style={styles.getStyle('control')}
                        panelProps={panelProps}
                    />
                )}
            </RoomControlButtonProvider>
        </View>
    );
};

export default React.memo(RoomShowTemplateComponent, RoomShowTemplatePresenter.outputAreEqual);