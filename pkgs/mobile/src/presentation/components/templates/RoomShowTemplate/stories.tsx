import { Guild, Member, Members, Message, Room, RoomActivity, RoomLog } from '@guildion/core';
import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomShowTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import RootBackground from '@/presentation/components/molecules/RootBackground';
import StoreProvider from '@/infrastructure/StoreProvider';
import { number, text } from '@storybook/addon-knobs';
import StorybookNavigationContainer from '../../navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('RoomShowTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <RootBackground
                    containerStyle={new Style({
                        width: number('width', 390),
                        height: number('height', 724),
                    })}
                >
                    <StorybookNavigationContainer>
                        <CenterView>
                            {getStory()}
                        </CenterView>
                    </StorybookNavigationContainer>
                </RootBackground>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomShowTemplate
            showMembers={true}
            showSyncVision={true}
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            guild={new Guild({
                displayName: 'sample guilddddd',
            })}
            connectingMembers={
                new Members([
                    new Member({
                        id: 'test1',
                        displayName: 'test1',
                    }),
                    new Member({
                        id: 'test2',
                        displayName: 'test2',
                    }),
                    new Member({
                        id: 'test3',
                        displayName: 'test3',
                    }),
                    new Member({
                        id: 'test4',
                        displayName: 'test4',
                    }),
                ])
            }
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            isFocused
        />
    ))
    .add('Loading', () => (
        <RoomShowTemplate
            showMembers={true}
            showSyncVision={true}
            loading={true}
            room={new Room({
                displayName: text('displayName', 'Test Room'),
                activity: new RoomActivity({
                    log: new RoomLog({
                        message: new Message({
                            body: text('message', "Let talk about this video !!")
                        }),
                    }).toJSON(),
                    connectsCount: number('connectsCount', 12),
                    notificationsCount: number('notificationsCount', 1),
                    createdAt: new Date().toString(),
                }).toJSON(),
            })}
            guild={new Guild({
                displayName: 'sample guilddddd',
            })}
            connectingMembers={
                new Members([
                    new Member({
                        id: 'test1',
                        displayName: 'test1',
                    }),
                    new Member({
                        id: 'test2',
                        displayName: 'test2',
                    }),
                    new Member({
                        id: 'test3',
                        displayName: 'test3',
                    }),
                    new Member({
                        id: 'test4',
                        displayName: 'test4',
                    }),
                ])
            }
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            isFocused
        />
    ))
