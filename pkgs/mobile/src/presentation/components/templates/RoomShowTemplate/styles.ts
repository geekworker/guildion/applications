
import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const TOP_HEIGHT: number = 220;
export const BIG_TOP_HEIGHT: number = 360;
export const HEADER_HEIGHT: number = 44;
export const TABBAR_HEIGHT: number = 50;
export const MEMBERSLIST_HEIGHT: number = 40;

const styles = ({
    style,
    appTheme,
    shouldSplit,
}: StyleProps & { shouldSplit: boolean }) => {
    appTheme ||= fallbackAppTheme;
    const topHeight = shouldSplit ? BIG_TOP_HEIGHT : TOP_HEIGHT;
    return StyleSheet.create({
        container: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.subm1,
        },
        topContainer: {
            width: style?.width,
            height: topHeight,
            position: 'relative',
        },
        empty: {
            width: style?.width,
            height: topHeight,
            position: 'relative',
        },
        header: {
            width: style?.width,
            paddingTop: 8,
            height: HEADER_HEIGHT + 8,
            backgroundColor: appTheme.common.transparent,
        },
        headerContainer: {
            width: style?.width,
            height: HEADER_HEIGHT + 8 + 8,
            paddingBottom: 8,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
        },
        headerDarkFilter: {
            width: style?.width,
            height: HEADER_HEIGHT + 8 + 8,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: appTheme.room.syncVisionBackground,
        },
        tabBar: {
            width: style?.width,
            height: TABBAR_HEIGHT,
            backgroundColor: appTheme.background.root,
        },
        membersContainer: {
            width: style?.width,
            height: MEMBERSLIST_HEIGHT + 4 + 8,
            paddingTop: 8,
            paddingBottom: 4,
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
        },
        membersList: {
            width: style?.width,
            height: MEMBERSLIST_HEIGHT,
        },
        subMembersContainer: {
            width: style?.width,
            height: MEMBERSLIST_HEIGHT + 8,
            backgroundColor: appTheme.room.syncVisionBackground,
            paddingTop: 4,
            paddingBottom: 4,
            position: 'absolute',
            top: HEADER_HEIGHT + 8 + 8,
            left: 0,
            right: 0,
        },
        subMembersList: {
            width: style?.width,
            height: MEMBERSLIST_HEIGHT,
            backgroundColor: appTheme.common.transparent,
        },
        pages: {
            width: style?.width,
            height: Number(style?.height ?? 0) - (HEADER_HEIGHT + 8 + 8) - TABBAR_HEIGHT,
            position: 'relative',
            backgroundColor: appTheme.background.subm1,
        },
        bottomContainer: {
            width: style?.width,
            height: Number(style?.height ?? 0) - (HEADER_HEIGHT + 8 + 8),
            position: 'relative',
            backgroundColor: appTheme.background.subm1,
        },
        control: {
            width: 52,
            height: 52,
        },
    });
}

type Styles = typeof styles;

export default class RoomShowTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({ shouldSplit: false })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { shouldSplit: boolean }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }

    widgetFullHeight(): number {
        return this.pages.height - this.headerContainer.height;
    }

    widgetHeightWithSubMembers(): number {
        return this.pages.height - this.subMembersContainer.height - this.headerContainer.height;
    }

    widgetHeightWithSyncVision(): number {
        return this.pages.height - this.topContainer.height;
    }

    getWidgetHeight({ showMembers, showSyncVision }:{ showMembers?: boolean, showSyncVision?: boolean }): number {
        if (showSyncVision) {
            return this.widgetHeightWithSyncVision();
        } else if (showMembers) {
            return this.widgetHeightWithSubMembers();
        } else {
            return this.widgetFullHeight();
        }
    }
};