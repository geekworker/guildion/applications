import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomShowTemplateHooks from "./hooks";
import RoomShowTemplateStyles from './styles';
import type RoomControlPanelTemplatePresenter from "../RoomControlPanelTemplate/presenter";
import React from 'react';
import { Guild, Members, Room } from "@guildion/core";
import { IconSwitchData } from "../../organisms/IconSwitchList";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { Animated } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomShowTemplatePresenter {
    export type Input = {
        children?: React.ReactNode,
        guild: Guild,
        room: Room,
        connectingMembers: Members,
        showSyncVision?: boolean,
        showMembers?: boolean,
        showTextChat?: boolean,
        showMediaLibrary?: boolean,
        showPosts?: boolean,
        showSetting?: boolean,
        onPressGuild?: (guild: Guild) => void,
        loading?: boolean,
        isFocused: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomShowTemplateStyles,
        appTheme: AppTheme,
        guild: Guild,
        room: Room,
        connectingMembers: Members,
        iconSwitchListProps: IconSwitchData,
        showSyncVision: boolean,
        showMembers: boolean,
        showTextChat: boolean,
        showMediaLibrary: boolean,
        showPosts: boolean,
        showSetting: boolean,
        setShowSyncVision: (state: boolean) => void,
        setShowMembers: (state: boolean) => void,
        setShowTextChat: (state: boolean) => void,
        setShowMediaLibrary: (state: boolean) => void,
        setShowPosts: (state: boolean) => void,
        setShowSetting: (state: boolean) => void,
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        onChangeTabRouteName?: (value?: keyof RoomWidgetTabsNavigatorStackParams | 'SyncVisionWidget') => void,
        translateMembersList: Animated.Value | Animated.AnimatedInterpolation,
        translateSubMembersList: Animated.Value | Animated.AnimatedInterpolation,
        translateSyncVision: Animated.Value | Animated.AnimatedInterpolation,
        translateWidgetHeight: Animated.Value | Animated.AnimatedInterpolation,
        onPressGuild?: (guild: Guild) => void,
        loading?: boolean,
        isFocused: boolean,
        panelProps: RoomControlPanelTemplatePresenter.Input,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoomShowTemplateHooks.useStyles({ ...props });
        const {
            data: iconSwitchListProps,
            state: pageState,
        } = RoomShowTemplateHooks.useIconSwitchListDate({ ...props });
        const tabInitialRouteName = RoomShowTemplateHooks.useTabInitialRouteName(pageState);
        const {
            translateMembersList,
            translateSubMembersList,
            translateSyncVision,
            translateWidgetHeight,
        } = RoomShowTemplateHooks.useState({ ...props, ...pageState, styles });
        const panelProps = RoomShowTemplateHooks.useRoomControlPanel(props);
        return {
            ...props,
            ...pageState,
            panelProps,
            iconSwitchListProps,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            tabInitialRouteName,
            translateMembersList,
            translateSubMembersList,
            translateSyncVision,
            translateWidgetHeight,
            onChangeTabRouteName: (value) => {
                switch(value) {
                default: break;
                case 'SyncVisionWidget': return pageState.setShowSyncVision(true);
                case 'TextChatWidget': return pageState.setShowTextChat(true);
                case 'MediaLibraryWidgetNavigator': return pageState.setShowMediaLibrary(true);
                case 'RoomPostsWidgetNavigator': return pageState.setShowPosts(true);
                case 'RoomSettingWidget': return pageState.setShowSetting(true);
                };
            }
        }
    }
}

export default RoomShowTemplatePresenter;