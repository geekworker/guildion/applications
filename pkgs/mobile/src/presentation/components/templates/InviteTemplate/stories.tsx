import { storiesOf } from '@storybook/react-native';
import React from 'react';
import InviteTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Files, Guild, Invite, Room } from '@guildion/core';

storiesOf('InviteTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <InviteTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            invite={
                new Invite(
                    {
                        id: 'test',
                    },
                    {
                        room: new Room({ displayName: 'test' }, { profile: Files.getSeed().generateGuildProfile() }),
                        guild: new Guild({ displayName: 'test' }, { profile: Files.getSeed().generateGuildProfile() })
                    }
                )
            }
        />
    ))
    .add('Guild', () => (
        <InviteTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            invite={
                new Invite(
                    {
                        id: 'test',
                    },
                    {
                        guild: new Guild({ displayName: 'test' }, { profile: Files.getSeed().generateGuildProfile() })
                    }
                )
            }
        />
    ))