import React from 'react';
import InviteTemplateComponent from './component';
import InviteTemplatePresenter from './presenter';

const InviteTemplate = (props: InviteTemplatePresenter.Input) => {
    const output = InviteTemplatePresenter.usePresenter(props);
    return <InviteTemplateComponent {...output} />;
};

export default React.memo(InviteTemplate, InviteTemplatePresenter.inputAreEqual);