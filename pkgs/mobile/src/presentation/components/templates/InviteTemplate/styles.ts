import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            width,
            height,
            paddingTop: 30,
        },
        headContainer: {
            width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        profile: {
            width: 44,
            height: 44,
            borderRadius: 5,
        },
        displayName: {
            fontFamily: MontserratFont.SemiBold,
            fontSize: 16,
            color: appTheme.element.default,
            textAlign: 'left',
            marginLeft: 12,
        },
        qrContainer: {
            marginTop: 32,
            marginBottom: 20,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        zoomIconTouchable: {
            width: 32,
            height: 32,
            color: appTheme.element.default,
            marginRight: 32,
        },
        zoomIcon: {
            width: 24,
            height: 24,
            color: appTheme.element.default,
        },
        downloadIconTouchable: {
            width: 32,
            height: 32,
            color: appTheme.element.default,
            marginLeft: 32,
        },
        downloadIcon: {
            width: 24,
            height: 24,
            color: appTheme.element.default,
        },
        qrCode: {
            width: 174,
            height: 174,
        },
        urlField: {
            width: width - 50,
            marginLeft: 25,
            marginRight: 25,
            marginBottom: 20,
            marginTop: 25,
        },
        detailLabel: {
            width: width - 50,
            marginLeft: 25,
            marginRight: 25,
            fontFamily: MontserratFont.SemiBold,
            fontSize: 18,
            textAlign: 'left',
            color: appTheme.element.default,
            marginBottom: 12,
        },
        expireControl: {
            width: width - 50,
            marginLeft: 25,
            marginRight: 25,
            marginBottom: 20,
        },
        userLimitControl: {
            width: width - 50,
            marginLeft: 25,
            marginRight: 25,
        },
        share: {
            width: 140,
            height: 50,
            marginTop: 44,
            marginLeft: (width - 140) / 2,
            marginRight: (width - 140) / 2,
        },
    });
};

type Styles = typeof styles;

export default class InviteTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};