import React from 'react';
import { Animated, Text, View } from 'react-native';
import AsyncImage from '../../atoms/AsyncImage';
import InviteTemplatePresenter from './presenter';
import QRCode from 'react-native-qrcode-svg';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import TouchableCircleRipple from '../../atoms/TouchableCircleRipple';
import ReadonlyTextField from '../../atoms/ReadonlyTextField';
import { localizer } from '@/shared/constants/Localizer';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import SegumentedControl from '../../organisms/SegumentedControl';
import Button from '../../atoms/Button';

const logoImage = require('@/assets/images/brands/white-brand-logo.png')

const InviteTemplateComponent = ({ styles, appTheme, animatedStyle, getRef, invite, onPressDownload, onPressZoom, onPressShare, onPressURL, onPressExpired, onPressUsersLimit, usersLimitData, expiredData }: InviteTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <View style={styles.headContainer}>
                    <AsyncImage
                        url={invite.records.room?.profile?.url ?? invite.records.guild?.profile?.url ?? ''}
                        style={styles.getStyle('profile')}
                    />
                    <Text style={styles.displayName}>
                        {invite.records.room?.displayName ?? invite.records.guild?.displayName ?? ''}
                    </Text>
                </View>
                <View style={styles.qrContainer}>
                    <TouchableCircleRipple
                        onPress={onPressZoom}
                        style={styles.getStyle('zoomIconTouchable')}
                    >
                        <MaterialIcon name={'zoom-out-map'} color={styles.zoomIcon.color} size={styles.zoomIcon.width} />
                    </TouchableCircleRipple>
                    <QRCode
                        value={invite.urlstring}
                        logo={logoImage}
                        logoSize={40}
                        logoBackgroundColor='transparent'
                        size={styles.qrCode.width}
                        getRef={getRef}
                    />
                    <TouchableCircleRipple
                        onPress={onPressDownload}
                        style={styles.getStyle('downloadIconTouchable')}
                    >
                        <AntDesignIcon name={'download'} color={styles.downloadIcon.color} size={styles.downloadIcon.width} />
                    </TouchableCircleRipple>
                </View>
                <ReadonlyTextField
                    style={styles.getStyle('urlField')}
                    title={localizer.dictionary.guild.invite.urlTitle}
                    value={invite.urlstring}
                    Icon={(props) => <EntypoIcon {...props} name={'link'} />}
                    onPress={onPressURL}
                />
                <Text style={styles.detailLabel}>
                    {localizer.dictionary.guild.invite.settingTitle}
                </Text>
                <SegumentedControl
                    data={expiredData}
                    style={styles.getStyle('expireControl')}
                    title={localizer.dictionary.guild.invite.expire.title}
                    activedId={expiredData[0].id}
                />
                <SegumentedControl
                    data={usersLimitData}
                    style={styles.getStyle('userLimitControl')}
                    title={localizer.dictionary.guild.invite.usersLimit.title}
                    activedId={usersLimitData[0].id}
                />
                <Button
                    styleType={'fill'}
                    style={styles.getStyle('share')}
                    onPress={onPressShare}
                    title={localizer.dictionary.g.share}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(InviteTemplateComponent, InviteTemplatePresenter.outputAreEqual);