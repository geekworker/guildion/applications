import React from 'react';
import InviteTemplateStyles from './styles';
import type InviteTemplatePresenter from './presenter';
import Share from 'react-native-share';
import { localizer } from '@/shared/constants/Localizer';
import { Invite } from '@guildion/core';
import SegumentedControlInputPresenter from '../../atoms/SegumentedControlInput/presenter';

namespace InviteTemplateHooks {
    export const useStyles = (props: InviteTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new InviteTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: InviteTemplatePresenter.Input) => {
        const [ref, getRef] = React.useState<any>(null); /*QRCode*/
        const getQRURL = React.useCallback((base64: string) => `data:image/png;base64,${base64}`, []);
        const onPressZoom = React.useCallback(() => {
            ref?.toDataURL((dataURL: string) => {
                props.onPressZoom && props.onPressZoom(getQRURL(dataURL));
            })
        }, [props.onPressZoom, ref]);
        const onPressDownload = React.useCallback(() => {
            ref?.toDataURL((dataURL: string) => {
                props.onPressDownload && props.onPressDownload(getQRURL(dataURL));
            })
        }, [props.onPressDownload, ref]);
        const onPressShare = React.useCallback(() => {
            ref?.toDataURL((dataURL: string) => {
                Share.open({
                    url: getQRURL(dataURL),
                    urls: [props.invite.urlstring],
                    title: localizer.dictionary.guild.invite.share.title(props.invite.room?.displayName ?? props.invite.guild?.displayName ?? ''),
                    message: localizer.dictionary.guild.invite.share.message(props.invite.room?.displayName ?? props.invite.guild?.displayName ?? ''),
                });
                props.onPressShare && props.onPressShare();
            })
        }, [props.onPressShare, ref, props.invite]);
        const expiredData = React.useMemo<SegumentedControlInputPresenter.Input[]>(() => [
            {
                id: 'infinity',
                text: localizer.dictionary.guild.invite.expire.none,
            },
            {
                id: '7d',
                text: localizer.dictionary.guild.invite.expire.d7,
            },
            {
                id: '1d',
                text: localizer.dictionary.guild.invite.expire.d1,
            },
            {
                id: '1dh',
                text: localizer.dictionary.guild.invite.expire.hd1,
            },
        ], [localizer]);
        const usersLimitData = React.useMemo<SegumentedControlInputPresenter.Input[]>(() => [
            {
                id: 'none',
                text: localizer.dictionary.guild.invite.usersLimit.none,
            },
            {
                id: '1',
                text: '1',
            },
            {
                id: '1d',
                text: '5',
            },
            {
                id: '1dh',
                text: '10',
            },
        ], [localizer]);
        const onPressExpired = React.useCallback((activeId?: string) => {
            if (!props.onPressExpired || !activeId) return;
            switch(activeId) {
            case expiredData[0].id: props.onPressExpired(undefined); break;
            case expiredData[1].id: props.onPressExpired(Invite.getExpiredByDay(7)); break;
            case expiredData[2].id: props.onPressExpired(Invite.getExpiredByDay(1)); break;
            case expiredData[3].id: props.onPressExpired(Invite.getExpiredByDay(0.5)); break;
            }
        }, [props.onPressExpired, expiredData]);
        const onPressUsersLimit = React.useCallback((activeId?: string) => {
            if (!props.onPressUsersLimit || !activeId) return;
            switch(activeId) {
            case usersLimitData[0].id: props.onPressUsersLimit(undefined); break;
            case usersLimitData[1].id: props.onPressUsersLimit(Number(usersLimitData[1].value)); break;
            case usersLimitData[2].id: props.onPressUsersLimit(Number(usersLimitData[2].value)); break;
            case usersLimitData[3].id: props.onPressUsersLimit(Number(usersLimitData[3].value)); break;
            }
        }, [props.onPressUsersLimit, usersLimitData]);
        return {
            ref,
            onPressZoom,
            onPressDownload,
            onPressShare,
            expiredData,
            usersLimitData,
            onPressExpired,
            onPressUsersLimit,
            getRef,
        };
    }
}

export default InviteTemplateHooks;