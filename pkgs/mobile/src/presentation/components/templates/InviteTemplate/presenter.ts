import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import InviteTemplateHooks from './hooks';
import InviteTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Invite } from '@guildion/core';
import React from 'react';
import SegumentedControlInputPresenter from '../../atoms/SegumentedControlInput/presenter';

namespace InviteTemplatePresenter {
    export type Input = {
        invite: Invite,
        onPressExpired?: (expiredAt?: Date) => void,
        onPressUsersLimit?: (limit?: number) => void,
        onPressZoom?: (urlstring: string) => void,
        onPressDownload?: (urlstring: string) => void,
        onPressURL?: () => void,
        onPressShare?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: InviteTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        invite: Invite,
        onPressExpired?: (activeId?: string) => void,
        onPressUsersLimit?: (activeId?: string) => void,
        onPressZoom?: () => void,
        onPressDownload?: () => void,
        onPressURL?: () => void,
        onPressShare?: () => void,
        getRef: (ref: any) => void,
        expiredData: SegumentedControlInputPresenter.Input[],
        usersLimitData: SegumentedControlInputPresenter.Input[],
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = InviteTemplateHooks.useStyles({ ...props });
        const {
            getRef,
            onPressDownload,
            onPressZoom,
            onPressShare,
            expiredData,
            usersLimitData,
            onPressExpired,
            onPressUsersLimit,
        } = InviteTemplateHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            getRef,
            onPressDownload,
            onPressZoom,
            onPressShare,
            expiredData,
            usersLimitData,
            onPressExpired,
            onPressUsersLimit,
        }
    }
}

export default InviteTemplatePresenter;