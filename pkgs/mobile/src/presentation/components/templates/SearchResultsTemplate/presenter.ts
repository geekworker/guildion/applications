import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import SearchResultsTemplateHooks from './hooks';
import SearchResultsTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { File, Guild, Guilds, Member, Post, Posts, Room, Rooms } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { SearchInputProps } from '../../atoms/SearchInput';

namespace SearchResultsTemplatePresenter {
    export type Input = {
        guilds: Guilds,
        rooms: Rooms,
        posts: Posts,
        query?: string,
        onPressGuild?: (guild: Guild) => void,
        onPressRoom?: (room: Room) => void,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        onPressBack?: () => void,
        loading?: boolean,
        postsLoadingMore?: LoadingMoreStatus,
        onFocus?: SearchInputProps['onFocus'],
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SearchResultsTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
    } & Omit<Input, 'style' | 'appTheme'>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SearchResultsTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default SearchResultsTemplatePresenter;