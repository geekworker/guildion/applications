import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { Animated, View } from 'react-native';
import NotFoundView from '../../atoms/NotFoundView';
import SearchResultsHeader from '../../molecules/SearchResultsHeader';
import SearchResultsList from '../../organisms/SearchResultsList';
import SearchResultsTemplatePresenter from './presenter';

const SearchResultsTemplateComponent = ({ styles, appTheme, animatedStyle, guilds, rooms, posts, onPressGuild, onPressRoom, onPressSender, onPressSeeMore, onPressComment, onPressReaction, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, loading, postsLoadingMore, onPressBack, query, onBlur, onChangeText, onFocus, onSubmitEditing }: SearchResultsTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <SearchResultsList
                    guilds={guilds}
                    rooms={rooms}
                    posts={posts}
                    onPressGuild={onPressGuild}
                    onPressRoom={onPressRoom}
                    onPressSender={onPressSender}
                    onPressSeeMore={onPressSeeMore}
                    onPressReaction={onPressReaction}
                    onPressComment={onPressComment}
                    onPressFile={onPressFile}
                    onPressURL={onPressURL}
                    onPressEmail={onPressEmail}
                    onPressPhoneNumber={onPressPhoneNumber}
                    onPressMentionMember={onPressMentionMember}
                    onPressMentionRoom={onPressMentionRoom}
                    loading={loading}
                    postsLoadingMore={postsLoadingMore}
                    style={styles.getStyle('scrollview')}
                    ListHeaderComponent={guilds.size === 0 && rooms.size === 0 && posts.size === 0 && !loading ? (
                        <NotFoundView
                            style={styles.getStyle('notfound')}
                            title={localizer.dictionary.explore.notfound.title}
                            description={localizer.dictionary.explore.notfound.description}
                        />
                    ) : undefined}
                    ListFooterComponent={<View style={styles.bottomMargin} />}
                />
                <SearchResultsHeader
                    style={styles.getStyle('header')}
                    onPressBack={onPressBack}
                    query={query}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(SearchResultsTemplateComponent, SearchResultsTemplatePresenter.outputAreEqual);