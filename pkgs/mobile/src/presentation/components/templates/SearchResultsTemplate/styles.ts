import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const HEADER_HEIGHT = 64;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            position: 'relative',
            width,
            height,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        header: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: HEADER_HEIGHT,
            width: style?.width,
            backgroundColor: appTheme.background.subm1,
        },
        scrollview: {
            marginTop: HEADER_HEIGHT,
            width,
            height: height - HEADER_HEIGHT,
        },
        notfound: {
            width,
            marginTop: 44,
        },
        bottomMargin: {
            width,
            height: 230,
        },
    });
};

type Styles = typeof styles;

export default class SearchResultsTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};