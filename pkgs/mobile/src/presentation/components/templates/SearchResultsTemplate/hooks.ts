import React from 'react';
import SearchResultsTemplateStyles from './styles';
import type SearchResultsTemplatePresenter from './presenter';

namespace SearchResultsTemplateHooks {
    export const useStyles = (props: SearchResultsTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new SearchResultsTemplateStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SearchResultsTemplatePresenter.Input) => {
        return {};
    }
}

export default SearchResultsTemplateHooks;