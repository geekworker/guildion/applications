import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SearchResultsTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { mockPosts } from '../../organisms/PostsList/stories';
import { mockRooms } from '../../organisms/RoomPublicHorizontalList/stories';
import { Guilds } from '@guildion/core';
import { mockGuild } from '../../molecules/GuildPublicCell/stories';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

storiesOf('SearchResultsTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SearchResultsTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
        />
    ))
    .add('Loading', () => (
        <SearchResultsTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
            loading
        />
    ))
    .add('LoadingMore(increment)', () => (
        <SearchResultsTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
            postsLoadingMore={LoadingMoreStatus.INCREMENT}
        />
    ))
    .add('LoadingMore(decrement)', () => (
        <SearchResultsTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
            postsLoadingMore={LoadingMoreStatus.DECREMENT}
        />
    ))