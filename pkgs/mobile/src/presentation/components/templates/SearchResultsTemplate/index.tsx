import React from 'react';
import SearchResultsTemplateComponent from './component';
import SearchResultsTemplatePresenter from './presenter';

const SearchResultsTemplate = (props: SearchResultsTemplatePresenter.Input) => {
    const output = SearchResultsTemplatePresenter.usePresenter(props);
    return <SearchResultsTemplateComponent {...output} />;
};

export default React.memo(SearchResultsTemplate, SearchResultsTemplatePresenter.inputAreEqual);