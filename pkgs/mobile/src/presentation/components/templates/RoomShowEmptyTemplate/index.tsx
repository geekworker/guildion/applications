import React from 'react';
import RoomShowEmptyTemplateStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, View } from 'react-native';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
} & Partial<StyleProps>;

const RoomShowEmptyTemplate: React.FC<Props> = ({
    style,
    appTheme,
}) => {
    const styles = React.useMemo(() => new RoomShowEmptyTemplateStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <Text style={styles.text}>
                Please select room from guild in left.
            </Text>
        </View>
    )
};

export default React.memo(RoomShowEmptyTemplate, compare);