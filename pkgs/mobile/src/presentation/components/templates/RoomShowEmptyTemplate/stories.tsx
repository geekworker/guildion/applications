import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomShowEmptyTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { number } from '@storybook/addon-knobs';
import { Style } from '@/shared/interfaces/Style';

storiesOf('RoomShowEmptyTemplate', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <RoomShowEmptyTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
        />
    ))
