import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildNewInputTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { boolean, number, text } from '@storybook/addon-knobs';

storiesOf('GuildNewInputTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildNewInputTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            url={text('url', 'https://yt3.ggpht.com/ytc/AKedOLTDLauymQQXmfG3S_r3ZTzw8ds1VstwhcwvHU_8OA=s176-c-k-c0x00ffffff-no-rj')}
            guildname={text('guildname', '')}
            guildnameError={text('guildnameError', '')}
            loading={boolean('loading', false)}
            sendable={boolean('sendable', false)}
        />
    ))