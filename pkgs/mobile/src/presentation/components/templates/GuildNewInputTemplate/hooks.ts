import React from 'react';
import GuildNewInputTemplateStyles from './styles';
import type GuildNewInputTemplatePresenter from './presenter';

namespace GuildNewInputTemplateHooks {
    export const useStyles = (props: GuildNewInputTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new GuildNewInputTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: GuildNewInputTemplatePresenter.Input) => {
        return {};
    }
}

export default GuildNewInputTemplateHooks;