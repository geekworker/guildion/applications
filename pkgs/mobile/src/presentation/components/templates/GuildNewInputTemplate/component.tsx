import React from 'react';
import { Animated, KeyboardAvoidingView, View } from 'react-native';
import GuildNewInputTemplatePresenter from './presenter';
import AsyncImage from '../../atoms/AsyncImage';
import DismissKeyboardView from '../../atoms/DismissKeyboardView';
import IconButton from '../../atoms/IconButton';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import TextField from '../../atoms/TextField';
import { localizer } from '@/shared/constants/Localizer';
import Button from '../../atoms/Button';

const GuildNewInputTemplateComponent = ({ styles, appTheme, animatedStyle, loading, sendable, url, guildname, guildnameError, onPressPicker, onSubmit, onChangeGuildname }: GuildNewInputTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={-styles.inner.height / 3} >
                <DismissKeyboardView>
                    <View style={styles.inner} >
                        <View style={styles.profile}>
                            <AsyncImage
                                url={url}
                                style={styles.getStyle('image')}
                            />
                            <View style={styles.cameraButtonContainer}>
                                <IconButton
                                    style={styles.getStyle('cameraButton')}
                                    onPress={onPressPicker}
                                    icon={<FontAwesomeIcon size={24} color={styles.cameraButton.color} name={'camera'} />}
                                />
                            </View>
                        </View>
                        <View style={styles.guildnameField}>
                            <TextField
                                onChangeText={onChangeGuildname}
                                defaultValue={guildname}
                                value={guildname}
                                label={localizer.dictionary.guild.attr.guildname}
                                errorMessage={guildnameError}
                            />
                        </View>
                        <Button
                            styleType={'fill'}
                            style={styles.getStyle('submit')}
                            loading={loading}
                            disabled={!sendable}
                            onPress={onSubmit}
                            title={localizer.dictionary.g.next}
                        />
                    </View>
                </DismissKeyboardView>
            </KeyboardAvoidingView>
        </Animated.View>
    );
};

export default React.memo(GuildNewInputTemplateComponent, GuildNewInputTemplatePresenter.outputAreEqual);