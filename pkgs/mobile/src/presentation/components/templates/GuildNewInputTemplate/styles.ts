import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            width,
            height,
            position: 'relative',
            paddingTop: 30,
        },
        profile: {
            width: 140,
            marginLeft: (Number(style?.width ?? 0) - 140) / 2,
            marginRight: (Number(style?.width ?? 0) - 140) / 2,
            marginBottom: 0,
            position: 'relative',
        },
        image: {
            width: 140,
            height: 140,
            borderRadius: 20,
        },
        cameraButtonContainer: {
            width: 44,
            height: 44,
            borderRadius: 22,
            position: 'absolute',
            bottom: -10,
            right: -10,
        },
        cameraButton: {
            width: 44,
            height: 44,
            borderRadius: 22,
            color: appTheme.element.default,
            backgroundColor: appTheme.background.subp2,
            borderColor: appTheme.background.subp3,
            borderWidth: 1,
        },
        guildnameField: {
            width: Number(style?.width ?? 0) - 50,
            marginLeft: 25,
            marginRight: 25,
            marginTop: 32,
        },
        submit: {
            width: 140,
            height: 50,
            marginTop: 44,
            marginLeft: (Number(style?.width ?? 0) - 140) / 2,
            marginRight: (Number(style?.width ?? 0) - 140) / 2,
        },
    })
};

type Styles = typeof styles;

export default class GuildNewInputTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};