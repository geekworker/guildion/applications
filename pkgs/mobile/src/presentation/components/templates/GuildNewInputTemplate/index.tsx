import React from 'react';
import GuildNewInputTemplateComponent from './component';
import GuildNewInputTemplatePresenter from './presenter';

const GuildNewInputTemplate = (props: GuildNewInputTemplatePresenter.Input) => {
    const output = GuildNewInputTemplatePresenter.usePresenter(props);
    return <GuildNewInputTemplateComponent {...output} />;
};

export default React.memo(GuildNewInputTemplate, GuildNewInputTemplatePresenter.inputAreEqual);