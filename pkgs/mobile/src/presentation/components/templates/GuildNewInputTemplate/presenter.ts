import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildNewInputTemplateHooks from './hooks';
import GuildNewInputTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { GestureResponderEvent } from 'react-native';

namespace GuildNewInputTemplatePresenter {
    export type Input = {
        url: string,
        guildname?: string,
        onPressPicker?: () => void,
        onChangeGuildname?: (guildname?: string) => void,
        sendable?: boolean,
        guildnameError?: string,
        loading?: boolean,
        onSubmit?: (event: GestureResponderEvent) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildNewInputTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        url: string,
        guildname?: string,
        onPressPicker?: () => void,
        onChangeGuildname?: (guildname?: string) => void,
        sendable?: boolean,
        guildnameError?: string,
        loading?: boolean,
        onSubmit?: (event: GestureResponderEvent) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildNewInputTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default GuildNewInputTemplatePresenter;