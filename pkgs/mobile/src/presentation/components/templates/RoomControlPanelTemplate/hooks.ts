import React from 'react';
import RoomControlPanelTemplateStyles from './styles';
import type RoomControlPanelTemplatePresenter from './presenter';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

namespace RoomControlPanelTemplateHooks {
    export const useStyles = (props: RoomControlPanelTemplatePresenter.Input) => {
        const insets = useSafeAreaInsets();
        const styles = React.useMemo(() => new RoomControlPanelTemplateStyles({
            ...props,
            insets,
        }), [
            props.appTheme,
            props.style,
            insets,
        ]);
        return styles;
    }

    export const useState = (props: RoomControlPanelTemplatePresenter.Input) => {
        return {};
    }
}

export default RoomControlPanelTemplateHooks;