import React from 'react';
import RoomControlPanelTemplateComponent from './component';
import RoomControlPanelTemplatePresenter from './presenter';

const RoomControlPanelTemplate = (props: RoomControlPanelTemplatePresenter.Input) => {
    const output = RoomControlPanelTemplatePresenter.usePresenter(props);
    return (
        <RoomControlPanelTemplateComponent {...output} />
    );
};

export default React.memo(RoomControlPanelTemplate, RoomControlPanelTemplatePresenter.inputAreEqual);