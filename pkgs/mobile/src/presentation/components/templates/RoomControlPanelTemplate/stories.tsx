import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomControlPanelTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import RootBackground from '../../molecules/RootBackground';

storiesOf('RoomControlPanelTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <RootBackground>
                        <CenterView>
                            {getStory()}
                        </CenterView>
                    </RootBackground>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomControlPanelTemplate
            style={new Style({
                width: 64,
                height: 64,
            })}
        />
    ))