import React from 'react';
import RoomControlPanelTemplatePresenter from './presenter';
import RoomControlPanel from '../../organisms/RoomControlPanel';

const RoomControlPanelTemplateComponent = ({ styles, appTheme, animatedStyle, enableVoice, onPressInvite, onPressReaction, onPressVoice, onPressNewFile }: RoomControlPanelTemplatePresenter.Output) => {
    return (
        <RoomControlPanel
            enableVoice={enableVoice}
            onPressInvite={onPressInvite}
            onPressReaction={onPressReaction}
            onPressNewFile={onPressNewFile}
            onPressVoice={onPressVoice}
            style={styles.getStyle('control')}
        />
    );
};

export default React.memo(RoomControlPanelTemplateComponent, RoomControlPanelTemplatePresenter.outputAreEqual);