import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { EdgeInsetsZero } from '@/shared/constants/EdgeInsets';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';
import { EdgeInsets } from 'react-native-safe-area-context';

export const PANEL_WIDTH = 128;

type Props = {
    insets: EdgeInsets,
} & StyleProps;

const styles = ({ appTheme, style, insets }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            width,
            height,
            borderRadius: width / 2,
            position: 'absolute',
            bottom: 0,
            right: 0,
        },
        inner: {
            width,
            height,
            borderRadius: width / 2,
        },
        control: {
            position: 'absolute',
            bottom: insets.bottom + 40,
            right: insets.right + 0,
            width: PANEL_WIDTH,
        },
    })
};

type Styles = typeof styles;

export default class RoomControlPanelTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({ insets: EdgeInsetsZero })
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};