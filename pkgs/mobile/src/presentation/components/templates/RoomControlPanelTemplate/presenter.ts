import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomControlPanelTemplateHooks from './hooks';
import RoomControlPanelTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

namespace RoomControlPanelTemplatePresenter {
    export type Input = {
        enableVoice?: boolean,
        onDismiss?: () => void,
        onPressVoice?: (shouldEnableVoice: boolean) => void,
        onPressReaction?: () => void,
        onPressNewFile?: () => void,
        onPressInvite?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomControlPanelTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        enableVoice?: boolean,
        onDismiss?: () => void,
        onPressVoice?: (shouldEnableVoice: boolean) => void,
        onPressReaction?: () => void,
        onPressNewFile?: () => void,
        onPressInvite?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomControlPanelTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomControlPanelTemplatePresenter;