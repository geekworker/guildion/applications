import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PlayerTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Member, Members, PlayerStatus, Room, Files } from '@guildion/core';

storiesOf('PlayerTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PlayerTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            file={Files.getSeed().generateGuildProfile()}
            status={PlayerStatus.PLAY}
        />
    ))
    .add('Room', () => (
        <PlayerTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            room={new Room({
                id: 'test',
                displayName: 'test',
            })}
            file={Files.getSeed().generateGuildProfile()}
            status={PlayerStatus.PLAY}
        />
    ))
    .add('RoomMembers', () => (
        <PlayerTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            file={Files.getSeed().generateGuildProfile()}
            status={PlayerStatus.PLAY}
            room={new Room({
                id: 'test',
                displayName: 'test',
            })}
            members={new Members([
                new Member(),
                new Member(),
                new Member(),
            ])}
        />
    ))