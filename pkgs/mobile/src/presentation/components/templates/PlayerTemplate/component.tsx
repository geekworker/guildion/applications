import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import React from 'react';
import { Animated, TouchableWithoutFeedback, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import VideoPlayer from '../../atoms/VideoPlayer';
import SyncVisionHeader from '../../molecules/SyncVisionHeader';
import VideoPlayerControls from '../../molecules/VideoPlayerControls';
import VideoPlayerOverlay from '../../molecules/VideoPlayerOverlay';
import PlayerBottomSheetTemplate from '../PlayerBottomSheetTemplate';
import PlayerTemplatePresenter from './presenter';

const PlayerTemplateComponent = ({ styles, appTheme, containerRef, file, room, members, onPressPlayerButton, status, showOverlay, hideOverlayDelay, canBackSkip, canNextSkip, onPressBackSkip, onPressNextSkip, onChangeSnapPoint, snapTransition, overlayTransition, onPress, onPressAnywhere, handleOnStart, handleOnActive, handleOnEnd }: PlayerTemplatePresenter.Output) => {
    return (
        <View style={styles.container} ref={containerRef}>
            <TouchableWithoutFeedback style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}/>
            </TouchableWithoutFeedback>
            <View style={styles.inner} pointerEvents={"box-none"}>
                <Animated.View
                    style={{
                        ...styles.playerContainer,
                        transform: [
                            // {
                            //     translateY: snapTransition,
                            // }
                        ],
                    }}
                >
                    <VideoPlayer
                        file={file}
                        style={styles.getStyle('player')}
                    />
                    <VideoPlayerOverlay
                        status={status}
                        style={styles.getStyle('playerOverlay')}
                        onPress={onPress}
                        onPressPlayerButton={onPressPlayerButton}
                        canBackSkip={canBackSkip}
                        canNextSkip={canNextSkip}
                        onPressBackSkip={onPressBackSkip}
                        onPressNextSkip={onPressNextSkip}
                        transition={overlayTransition}
                    />
                </Animated.View>
                <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={[
                        'rgba(0, 0, 0, 0.8)',
                        'rgba(0, 0, 0, 0)',
                    ]}
                    style={styles.headerFilter}
                    pointerEvents='none'
                />
                <LinearGradient
                    start={{ x: 0, y: 1 }}
                    end={{ x: 0, y: 0 }}
                    colors={[
                        'rgba(0, 0, 0, 0.8)',
                        'rgba(0, 0, 0, 0)',
                    ]}
                    style={styles.footerFilter}
                    pointerEvents='none'
                />
                <SyncVisionHeader
                    appTheme={appTheme}
                    style={styles.getStyle('header')}
                    file={file}
                    room={room}
                    members={members}
                    opacityAnimated={overlayTransition}
                />
                <VideoPlayerControls
                    style={styles.getStyle('controls')}
                    animatedStyle={new AnimatedStyle({
                        transform: [{
                            translateY: overlayTransition.interpolate({
                                inputRange: [0, 1],
                                outputRange: [80 ,0]
                            })
                        }]
                    })}
                    file={file}
                    durationMs={0}
                    transition={overlayTransition}
                />
            </View>
            {!hideOverlayDelay && (
                <PlayerBottomSheetTemplate
                    onChangeSnapPoint={onChangeSnapPoint}
                    handleOnStart={handleOnStart}
                    handleOnActive={handleOnActive}
                    handleOnEnd={handleOnEnd}
                    hidden={!showOverlay}
                />
            )}
        </View>
    );
};

export default React.memo(PlayerTemplateComponent, PlayerTemplatePresenter.outputAreEqual);