import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const HEADER_HEIGHT = 100;
const playerHeight = 700;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const marginSide = 8;
    const width = style?.getAsNumber('width') ?? 0;
    const height = style?.getAsNumber('height') ?? 0;
    const playerWidth = width - (marginSide * 2);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            backgroundColor: style?.backgroundColor ?? appTheme.background.player,
            position: 'relative',
            width,
            height,
        },
        touchable: {
            width,
            height,
        },
        touchableInner: {
            width: width,
            height: height,
            position: 'relative',
            backgroundColor: style?.backgroundColor ?? appTheme.background.player,
        },
        inner: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0,
            width: width,
            height: height,
        },
        header: {
            width,
            height: HEADER_HEIGHT + 8,
            paddingTop: 8,
            backgroundColor: appTheme.common.transparent,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
        },
        headerFilter: {
            width: style?.width,
            height: HEADER_HEIGHT + 8,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
        },
        footerFilter: {
            width: style?.width,
            height: HEADER_HEIGHT + 8,
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
        },
        playerContainer: {
            position: 'absolute',
            width: playerWidth,
            height: playerHeight,
            top: (height - playerHeight) / 2,
            left: marginSide,
            right: marginSide,
        },
        playerInner: {
            position: 'relative',
            width: playerWidth,
            height: playerHeight,
        },
        player: {
            width: playerWidth,
            height: playerHeight,
            borderRadius: 5,
        },
        playerOverlay: {
            width: playerWidth,
            height: playerHeight,
            borderRadius: 5,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        controls: {
            width: playerWidth - (marginSide * 2),
            position: 'absolute',
            bottom: 100,
            left: marginSide * 2,
            right: marginSide * 2,
        },
    })
};

type Styles = typeof styles;

export default class PlayerTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};