import React from 'react';
import PlayerTemplateComponent from './component';
import PlayerTemplatePresenter from './presenter';

const PlayerTemplate = (props: PlayerTemplatePresenter.Input) => {
    const output = PlayerTemplatePresenter.usePresenter(props);
    return <PlayerTemplateComponent {...output} />;
};

export default React.memo(PlayerTemplate, PlayerTemplatePresenter.inputAreEqual);