import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import PlayerTemplateHooks from './hooks';
import PlayerTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import { File, Members, PlayerStatus, Room } from '@guildion/core';
import PlayerBottomSheetTemplateHooks from '../PlayerBottomSheetTemplate/hooks';
import { Animated, View } from 'react-native';
import { HandleOnActiveCallbackType, HandleOnEndCallbackType, HandleOnStartCallbackType } from '@/shared/hooks/useBottomSheetGestureEventsHandlers';
import { RefObject } from 'react';

namespace PlayerTemplatePresenter {
    export type Input = {
        file: File,
        room?: Room,
        members?: Members,
        status: PlayerStatus,
        onPressPlayerButton?: (status: PlayerStatus) => void,
        onPressNextSkip?: () => void,
        onPressBackSkip?: () => void,
        canNextSkip?: boolean,
        canBackSkip?: boolean,
        showOverlay?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PlayerTemplateStyles,
        appTheme: AppTheme,
        file: File,
        room?: Room,
        members?: Members,
        containerRef: RefObject<View>,
        status: PlayerStatus,
        onPressPlayerButton: (status: PlayerStatus) => void,
        onPressNextSkip?: () => void,
        onPressBackSkip?: () => void,
        canNextSkip?: boolean,
        canBackSkip?: boolean,
        onChangeSnapPoint: (snapPoint: PlayerBottomSheetTemplateHooks.SnapPoint) => void,
        snapTransition: Animated.Value | Animated.AnimatedInterpolation,
        overlayTransition: Animated.Value | Animated.AnimatedInterpolation,
        showOverlay: boolean,
        hideOverlayDelay: boolean,
        onPress: () => void,
        onPressAnywhere: () => void,
        handleOnStart: HandleOnStartCallbackType,
        handleOnActive: HandleOnActiveCallbackType,
        handleOnEnd: HandleOnEndCallbackType,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PlayerTemplateHooks.useStyles({ ...props });
        const {
            status,
            onPressPlayerButton,
            onChangeSnapPoint,
            snapTransition,
            overlayTransition,
            showOverlay,
            hideOverlayDelay,
            onPress,
            onPressAnywhere,
            handleOnStart,
            handleOnActive,
            handleOnEnd,
            containerRef,
        } = PlayerTemplateHooks.useState({ ...props, styles });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            status,
            onPressPlayerButton,
            onChangeSnapPoint,
            snapTransition,
            overlayTransition,
            showOverlay,
            hideOverlayDelay,
            onPress,
            onPressAnywhere,
            handleOnStart,
            handleOnActive,
            handleOnEnd,
            containerRef,
        }
    }
}

export default PlayerTemplatePresenter;