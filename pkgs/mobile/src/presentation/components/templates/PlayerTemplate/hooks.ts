import React from 'react';
import PlayerTemplateStyles from './styles';
import type PlayerTemplatePresenter from './presenter';
import { ConnectionDestroyRequest, PlayerStatus } from '@guildion/core';
import { Animated, Easing, View } from 'react-native';
import PlayerBottomSheetTemplateHooks from '../PlayerBottomSheetTemplate/hooks';
import { HandleOnActiveCallbackType, HandleOnEndCallbackType, HandleOnStartCallbackType } from '@/shared/hooks/useBottomSheetGestureEventsHandlers';

namespace PlayerTemplateHooks {
    export const useStyles = (props: PlayerTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new PlayerTemplateStyles({ ...props }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: PlayerTemplatePresenter.Input & { styles: PlayerTemplateStyles }) => {
        const containerRef = React.useRef<View>(null);
        const [status, setStatus] = React.useState(props.status);
        React.useEffect(() => {
            status != props.status && setStatus(props.status);
        }, [props.status]);

        const onPressPlayerButton = React.useCallback((newStatus: PlayerStatus) => {
            setStatus(newStatus);
        }, [status, props.onPressPlayerButton]);

        const [overlayCompositeAnimation, setOverlayCompositeAnimation] = React.useState<Animated.CompositeAnimation | undefined>(undefined);
        const [showOverlay, setShowOverlay] = React.useState(props.showOverlay ?? false);
        const [hideOverlayDelay, setHideOverlayDelay] = React.useState(props.showOverlay ?? false);
        const [snapPoint, setSnapPoint] = React.useState<PlayerBottomSheetTemplateHooks.SnapPoint>(80);
        React.useEffect(() => {
            showOverlay != props.showOverlay && props.showOverlay !== undefined && setShowOverlay(props.showOverlay);
        }, [props.showOverlay]);

        const overlayTransition = React.useRef(new Animated.Value(0)).current;
        const snapTransition = React.useRef(new Animated.Value(0)).current;
        const minSnapTransition = React.useMemo(() => 0, []);
        const maxSnapTransition = React.useMemo(() => {
            const { styles, members } = props;
            const dy = (styles.container.height / 2) - (styles.header.height + (members && members.size > 0 ? 0 : 50));
            return dy;
        }, [props.styles, props.members]);
        const onChangeSnapPoint = React.useCallback((snapPoint: PlayerBottomSheetTemplateHooks.SnapPoint) => {
            setSnapPoint(snapPoint);
            switch(snapPoint) {
            case 80:
                Animated.spring(snapTransition, {
                    useNativeDriver: true,
                    toValue: minSnapTransition,
                    overshootClamping: false,
                }).start();
                break;
            case '60%':
            case '100%':
                Animated.spring(snapTransition, {
                    useNativeDriver: true,
                    toValue: -maxSnapTransition,
                    overshootClamping: false,
                }).start();
                break;
            };
        }, []);

        const onPress = React.useCallback(() => {
            if (overlayCompositeAnimation) overlayCompositeAnimation.stop();
            if (showOverlay) {
                Animated.spring(overlayTransition, {
                    useNativeDriver: false,
                    toValue: 0,
                    overshootClamping: false,
                }).start((result) => {
                    if (result.finished) {
                        setHideOverlayDelay(true);
                    }
                });
                setOverlayCompositeAnimation(undefined);
                setShowOverlay(false);
            } else {
                Animated.spring(overlayTransition, {
                    useNativeDriver: false,
                    toValue: 1,
                    overshootClamping: false,
                }).start((result) => {
                    if (result.finished && !!showOverlay) {
                        const _overlayCompositeAnimation = Animated.timing(overlayTransition, {
                            useNativeDriver: false,
                            easing: Easing.ease,
                            toValue: 0,
                            duration: 100,
                            delay: 5000,
                        });
                        setOverlayCompositeAnimation(_overlayCompositeAnimation);
                        _overlayCompositeAnimation.start((result) => {
                            if (result.finished) {
                                setShowOverlay(false);
                                setOverlayCompositeAnimation(undefined);
                            }
                        });
                    }
                });
                setHideOverlayDelay(false);
                setShowOverlay(true);
            }
        }, [showOverlay, overlayCompositeAnimation]);

        const onPressAnywhere = React.useCallback(() => {
        }, []);

        const handleOnStart = React.useCallback<HandleOnStartCallbackType>(({
            translationY,
        }) => {
        }, []);
        const handleOnActive = React.useCallback<HandleOnActiveCallbackType>(({
            animatedPosition,
        }) => {
            containerRef.current?.measure((x, y, width, height, pageX, pageY) => {
                const minSnap = 80;
                const maxSnap = height * 0.4;
                const value = height - animatedPosition.value - minSnap;
                const currentSnap = Math.min(Math.max(value, minSnap), maxSnap);
                const ratio = (currentSnap - minSnap) / (maxSnap - minSnap);
                snapTransition.setValue((-maxSnapTransition * ratio));
            });
        }, []);
        const handleOnEnd = React.useCallback<HandleOnEndCallbackType>(({
            position,
            source,
            velocity,
        }) => {
        }, []);

        return {
            containerRef,
            status,
            onPressPlayerButton,
            onChangeSnapPoint,
            snapTransition,
            overlayTransition,
            showOverlay,
            hideOverlayDelay,
            onPress,
            onPressAnywhere,
            handleOnStart,
            handleOnActive,
            handleOnEnd,
        };
    }
}

export default PlayerTemplateHooks;