import React from 'react';
import { Animated, Text, View } from 'react-native';
import SyncVisionEmptyView from '../../molecules/SyncVisionEmptyView';
import RoomNewTutorialMemberInviteTemplatePresenter from './presenter';
import LinearGradient from 'react-native-linear-gradient';
import MembersList from '../../organisms/MembersList';
import IconSwitchList from '../../organisms/IconSwitchList';
import FontistoIcon from "react-native-vector-icons/Fontisto";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import FeatherIcon from "react-native-vector-icons/Feather";
import MembersIcon from "../../Icons/MembersIcon";
import SyncVisionIcon from "../../Icons/SyncVisionIcon";
import RoomHeader from '../../molecules/RoomHeader';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { localizer } from '@/shared/constants/Localizer';
import Button from '../../atoms/Button';

const AnimatedLinearGradient = Animated.createAnimatedComponent(LinearGradient);

const RoomNewTutorialMemberInviteTemplateComponent = ({ styles, appTheme, animatedStyle, room, connectingMembers, onPressInvite, opacity, animated }: RoomNewTutorialMemberInviteTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <View style={styles.background}>
                    <Animated.View style={{ ...styles.topContainer }}>
                        <SyncVisionEmptyView style={styles.getStyle('empty')} appTheme={appTheme} isAppeared={true} />
                    </Animated.View>
                    <Animated.View style={{
                        ...styles.bottomContainer,
                    }}>
                        <IconSwitchList
                            data={
                                React.useMemo(() => [
                                    {
                                        title: 'MEMBER',
                                        Icon: (props) => <MembersIcon width={props.size} height={props.size} color={props.color} />,
                                        value: true,
                                    },
                                    {
                                        title: 'SHARE',
                                        Icon: (props) => <SyncVisionIcon width={props.size} height={props.size} color={props.color} />,
                                        value: true,
                                    },
                                    'border',
                                    {
                                        title: 'CHAT',
                                        Icon: (props) => <FontistoIcon {...props} name={'hashtag'} />,
                                        value: false,
                                    },
                                    {
                                        title: 'UPLOAD',
                                        Icon: (props) => <FontAwesomeIcon {...props} name={'folder'} />,
                                        value: false,
                                    },
                                    {
                                        title: 'POST',
                                        Icon: (props) => <MaterialIcon {...props} name={'article'} />,
                                        value: false,
                                    },
                                    {
                                        title: 'SETTING',
                                        Icon: (props) => <FeatherIcon {...props} name={'more-horizontal'} />,
                                        value: false,
                                    },
                                ], [])
                            }
                            style={styles.getStyle('tabBar')}
                            appTheme={appTheme}
                        />
                    </Animated.View>
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0, y: 1 }}
                        colors={[
                            'rgba(0, 0, 0, 0.8)',
                            'rgba(0, 0, 0, 0)',
                        ]}
                        style={styles.headerContainer}
                    >
                        <RoomHeader
                            appTheme={appTheme}
                            style={styles.getStyle('header')}
                            guild={room.records.guild!}
                            room={room}
                        />
                    </LinearGradient>
                </View>
                <Animated.View style={{ ...styles.foreground }}>
                <Animated.View style={{ ...styles.overlay, opacity }} />
                    <Animated.View
                        style={{
                            ...styles.arrow,
                            opacity,
                        }}
                    >
                        <AntDesignIcon
                            name={'arrowup'}
                            size={styles.arrowIcon.width}
                            color={styles.arrowIcon.color}
                        />
                    </Animated.View>
                    <Animated.View style={{ ...styles.tutorialContainer, opacity }}>
                        <Text style={{ ...styles.tutorialText }}>
                            {localizer.dictionary.room.new.tutorial.invite}
                        </Text>
                        <Button
                            styleType={'fill'}
                            style={styles.getStyle('next')}
                            onPress={onPressInvite}
                            title={localizer.dictionary.g.next}
                        />
                    </Animated.View>
                </Animated.View>
                <AnimatedLinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={[
                        'rgba(0, 0, 0, 0)',
                        'rgba(0, 0, 0, 0.8)',
                    ]}
                    style={{
                        ...styles.membersContainer,
                    }}
                >
                    <MembersList
                        style={styles.getStyle('membersList')}
                        members={connectingMembers}
                    />
                </AnimatedLinearGradient>
            </View>
        </Animated.View>
    );
};

export default React.memo(RoomNewTutorialMemberInviteTemplateComponent, RoomNewTutorialMemberInviteTemplatePresenter.outputAreEqual);