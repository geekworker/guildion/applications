import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomNewTutorialMemberInviteTemplateHooks from './hooks';
import RoomNewTutorialMemberInviteTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Members, Room } from '@guildion/core';
import { Animated } from 'react-native';

namespace RoomNewTutorialMemberInviteTemplatePresenter {
    export type Input = {
        room: Room,
        connectingMembers: Members,
        onPressInvite?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomNewTutorialMemberInviteTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        room: Room,
        connectingMembers: Members,
        onPressInvite?: () => void,
        opacity: Animated.Value | Animated.AnimatedInterpolation,
        animated: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomNewTutorialMemberInviteTemplateHooks.useStyles({ ...props });
        const {
            opacity,
            animated,
        } = RoomNewTutorialMemberInviteTemplateHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            opacity,
            animated,
        }
    }
}

export default RoomNewTutorialMemberInviteTemplatePresenter;