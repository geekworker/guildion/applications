import React from 'react';
import RoomNewTutorialMemberInviteTemplateStyles from './styles';
import type RoomNewTutorialMemberInviteTemplatePresenter from './presenter';
import { useSplitView } from '@/shared/hooks/useSplitView';
import { Animated } from 'react-native';

namespace RoomNewTutorialMemberInviteTemplateHooks {
    export const useStyles = (props: RoomNewTutorialMemberInviteTemplatePresenter.Input) => {
        const shouldSplit = useSplitView();
        const styles = React.useMemo(() => new RoomNewTutorialMemberInviteTemplateStyles({ ...props, shouldSplit }), [
            props.appTheme,
            props.style,
            shouldSplit,
        ]);
        return styles;
    }

    export const useState = (props: RoomNewTutorialMemberInviteTemplatePresenter.Input) => {
        const opacity = React.useRef(new Animated.Value(0)).current;
        const [animated, setAnimated] = React.useState(false);
        React.useEffect(() => {
            Animated.spring(opacity, {
                toValue: 1,
                overshootClamping: false,
                useNativeDriver: true,
                delay: 500,
            }).start((result) => {
                if (result.finished) setAnimated(true);
            });
        }, []);
        return {
            animated,
            opacity,
        }
    }
}

export default RoomNewTutorialMemberInviteTemplateHooks;