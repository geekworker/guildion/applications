import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';
import { BIG_TOP_HEIGHT, TOP_HEIGHT, HEADER_HEIGHT, TABBAR_HEIGHT, MEMBERSLIST_HEIGHT } from '../RoomShowTemplate/styles';

type Props = {
    shouldSplit?: boolean,
} & StyleProps;

const styles = ({ appTheme, style, shouldSplit }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const topHeight = shouldSplit ? BIG_TOP_HEIGHT : TOP_HEIGHT;
    const paddingTop = 30;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            width,
            height,
            position: 'relative',
        },
        background: {
            width,
            height,
            position: 'relative',
            paddingTop: paddingTop,
        },
        foreground: {
            width,
            height,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        topContainer: {
            width: style?.width,
            height: topHeight,
            position: 'relative',
        },
        empty: {
            width: style?.width,
            height: topHeight,
            position: 'relative',
        },
        header: {
            width: style?.width,
            paddingTop: 8,
            height: HEADER_HEIGHT + 8,
            backgroundColor: appTheme.common.transparent,
        },
        headerContainer: {
            width: style?.width,
            height: HEADER_HEIGHT + 8 + 8,
            paddingBottom: 8,
            position: 'absolute',
            top: paddingTop,
            left: 0,
            right: 0,
        },
        tabBar: {
            width: style?.width,
            height: TABBAR_HEIGHT,
            backgroundColor: appTheme.background.root,
        },
        membersContainer: {
            width: style?.width,
            height: MEMBERSLIST_HEIGHT + 4 + 8,
            paddingTop: 8,
            paddingBottom: 4,
            position: 'absolute',
            top: topHeight - MEMBERSLIST_HEIGHT + 18,
            left: 0,
            right: 0,
        },
        membersList: {
            width: style?.width,
            height: MEMBERSLIST_HEIGHT,
        },
        bottomContainer: {
            width: style?.width,
            height: Number(style?.height ?? 0) - (HEADER_HEIGHT + 8 + 8),
            position: 'relative',
            backgroundColor: appTheme.background.subm1,
        },
        control: {
            width: 52,
            height: 52,
        },
        overlay: {
            width,
            height,
            backgroundColor: appTheme.common.overlayBackgroundColor,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        arrow: {
            width: 32,
            height: 32,
            position: 'absolute',
            top: topHeight + 18 + 12,
            left: 20,
        },
        arrowIcon: {
            width: 32,
            height: 32,
            color: 'white',
        },
        tutorialContainer: {
            position: 'absolute',
            width: width - 40,
            top: topHeight + 18 + 12 + 44,
            right: 20,
            left: 20,
        },
        tutorialText: {
            textAlign: 'left',
            color: 'white',
            fontFamily: MontserratFont.Bold,
            fontSize: 24,
            marginBottom: 20,
        },
        next: {
            width: 140,
            height: 50,
            marginLeft: 0,
        },
    })
};

type Styles = typeof styles;

export default class RoomNewTutorialMemberInviteTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};