import React from 'react';
import RoomNewTutorialMemberInviteTemplateComponent from './component';
import RoomNewTutorialMemberInviteTemplatePresenter from './presenter';

const RoomNewTutorialMemberInviteTemplate = (props: RoomNewTutorialMemberInviteTemplatePresenter.Input) => {
    const output = RoomNewTutorialMemberInviteTemplatePresenter.usePresenter(props);
    return <RoomNewTutorialMemberInviteTemplateComponent {...output} />;
};

export default React.memo(RoomNewTutorialMemberInviteTemplate, RoomNewTutorialMemberInviteTemplatePresenter.inputAreEqual);