import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomNewTutorialMemberInviteTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number, text } from '@storybook/addon-knobs';
import { Member, Members, Room } from '@guildion/core';

storiesOf('RoomNewTutorialMemberInviteTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomNewTutorialMemberInviteTemplate
            room={new Room({
                displayName: text('displayName', 'Test Room'),
            })}
            connectingMembers={
                new Members([
                    new Member({
                        id: 'test1',
                        displayName: 'test1',
                    }),
                ])
            }
            style={new Style({
                width: number('width', 390),
                height: number('height', 720),
            })}
        />
    ))