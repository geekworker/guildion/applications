import React from 'react';
import GuildsTemplateComponent from './component';
import GuildsTemplatePresenter from './presenter';

const GuildsTemplate = (props: GuildsTemplatePresenter.Input) => {
    const output = GuildsTemplatePresenter.usePresenter(props);
    return <GuildsTemplateComponent {...output} />;
};

export default React.memo(GuildsTemplate, GuildsTemplatePresenter.inputAreEqual);