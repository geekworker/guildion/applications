import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildsTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Guild, GuildActivity, Guilds, Member, Members, Message, Room, RoomActivity, RoomLog, Rooms } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';
import { number, text } from '@storybook/addon-knobs';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import { List, Map } from 'immutable';

storiesOf('GuildsTemplate', module)
    .addDecorator((getStory) => 
        <StoreProvider>
            <CenterView style={{ backgroundColor: fallbackAppTheme.background.subm1 }}>
                {getStory()}
            </CenterView>
        </StoreProvider>
    )
    .add('Default', () => (
        <GuildsTemplate
            guilds={new Guilds([
                new Guild({
                    id: '1',
                    displayName: 'sample guilddddd',
                }),
                new Guild({
                    id: '2',
                    displayName: 'sample guilddddd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '3',
                    displayName: '3sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '4',
                    displayName: '4sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '5',
                    displayName: '5sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '6',
                    displayName: '6sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '7',
                    displayName: '7sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
            ])}
            roomsPageState={List([
                Map({
                    guildId: '1',
                    rooms: new Rooms([
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: text('roomDisplayName1', 'Test Room1'),
                            activity: new RoomActivity({
                                log: new RoomLog({
                                    message: new Message({
                                        body: text('message', "Let talk about this video !!")
                                    }),
                                }).toJSON(),
                                connectsCount: number('roomConnectsCount1', 12),
                                notificationsCount: number('roomNotificationsCount1', 1),
                                connectingMembers: new Members([
                                    new Member({
                                        id: 'test1',
                                        displayName: 'test1',
                                    }),
                                    new Member({
                                        id: 'test2',
                                        displayName: 'test2',
                                    }),
                                    new Member({
                                        id: 'test3',
                                        displayName: 'test3',
                                    }),
                                    new Member({
                                        id: 'test4',
                                        displayName: 'test4',
                                    }),
                                ]).toJSON(),
                                createdAt: new Date().toString(),
                            }).toJSON(),
                        }),
                        new Room({
                            id: 'roomDisplayName2',
                            displayName: text('roomDisplayName2', 'Test Room2'),
                            activity: new RoomActivity({
                                log: new RoomLog({
                                    message: new Message({
                                        body: text('message', "Let talk about this video !!")
                                    }),
                                }).toJSON(),
                                connectsCount: number('roomConnectsCount2', 2),
                                notificationsCount: number('roomNotificationsCount2', 12),
                                connectingMembers: new Members([
                                    new Member({
                                        id: 'test1',
                                        displayName: 'test1',
                                    }),
                                    new Member({
                                        id: 'test2',
                                        displayName: 'test2',
                                    }),
                                ]).toJSON(),
                                createdAt: new Date().toString(),
                            }).toJSON(),
                        }),
                    ]),
                })
            ])}
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            loading={false}
        />
    ))
    .add('Loading', () => (
        <GuildsTemplate
            loading={true}
            guilds={new Guilds([
                new Guild({
                    id: '1',
                    displayName: 'sample guilddddd',
                }),
                new Guild({
                    id: '2',
                    displayName: 'sample guilddddd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '3',
                    displayName: '3sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '4',
                    displayName: '4sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '5',
                    displayName: '5sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '6',
                    displayName: '6sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
                new Guild({
                    id: '7',
                    displayName: '7sample guildd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
            ])}
            roomsPageState={List([
                Map({
                    guildId: '1',
                    loading: true,
                    rooms: new Rooms([
                        new Room({
                            id: 'roomDisplayName1',
                            displayName: text('roomDisplayName1', 'Test Room1'),
                            activity: new RoomActivity({
                                log: new RoomLog({
                                    message: new Message({
                                        body: text('message', "Let talk about this video !!")
                                    }),
                                }).toJSON(),
                                connectsCount: number('roomConnectsCount1', 12),
                                notificationsCount: number('roomNotificationsCount1', 1),
                                connectingMembers: new Members([
                                    new Member({
                                        id: 'test1',
                                        displayName: 'test1',
                                    }),
                                    new Member({
                                        id: 'test2',
                                        displayName: 'test2',
                                    }),
                                    new Member({
                                        id: 'test3',
                                        displayName: 'test3',
                                    }),
                                    new Member({
                                        id: 'test4',
                                        displayName: 'test4',
                                    }),
                                ]).toJSON(),
                                createdAt: new Date().toString(),
                            }).toJSON(),
                        }),
                        new Room({
                            id: 'roomDisplayName2',
                            displayName: text('roomDisplayName2', 'Test Room2'),
                            activity: new RoomActivity({
                                log: new RoomLog({
                                    message: new Message({
                                        body: text('message', "Let talk about this video !!")
                                    }),
                                }).toJSON(),
                                connectsCount: number('roomConnectsCount2', 2),
                                notificationsCount: number('roomNotificationsCount2', 12),
                                connectingMembers: new Members([
                                    new Member({
                                        id: 'test1',
                                        displayName: 'test1',
                                    }),
                                    new Member({
                                        id: 'test2',
                                        displayName: 'test2',
                                    }),
                                ]).toJSON(),
                                createdAt: new Date().toString(),
                            }).toJSON(),
                        }),
                    ]),
                })
            ])}
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
        />
    ))
