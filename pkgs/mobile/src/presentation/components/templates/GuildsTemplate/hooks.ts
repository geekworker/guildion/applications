import React from "react";
import GuildsTemplateStyles, { SCROLL_INTERVAL } from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Animated } from "react-native";

namespace GuildsTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new GuildsTemplateStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useEffectScrollAnimation = (): {
        scrollY: Animated.Value,
        diffClamp: Animated.AnimatedDiffClamp,
    } => {
        const scrollY = React.useRef(new Animated.Value(0)).current;
        const diffClamp = React.useRef(Animated.diffClamp(scrollY, 0, SCROLL_INTERVAL)).current;
        return {
            scrollY,
            diffClamp
        };
    }
}

export default GuildsTemplateHooks