import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const HEADER_HEIGHT: number = 44;
export const TABBAR_HEIGHT: number = 90;
export const SCROLL_INTERVAL: number = HEADER_HEIGHT + TABBAR_HEIGHT;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.subm1,
            overflow: 'hidden',
        },
        headline: {
            zIndex: 100,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            width: style?.width,
            backgroundColor: appTheme.guild.tabBarBackground,
            shadowColor: style?.shadowColor ?? appTheme.guild.tabShadowColor,
            shadowOpacity: style?.shadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
            // height: SCROLL_INTERVAL,
        },
        header: {
            height: HEADER_HEIGHT,
            width: style?.width,
            backgroundColor: appTheme.guild.tabBarBackground,
        },
        tabbar: {
            height: TABBAR_HEIGHT,
            width: style?.width,
            backgroundColor: appTheme.guild.tabBarBackground,
            borderBottomColor: appTheme.guild.tabBarShadow,
            borderBottomWidth: 0.4,
        },
        pager: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: appTheme.guild.tabBackground,
            height: Number(style?.height ?? 0),
            width: style?.width,
        },
        page: {
            zIndex: 2,
            position: 'absolute',
            left: 0,
            right: 0,
            backgroundColor: appTheme.guild.tabBackground,
            height: Number(style?.height ?? 0),
            width: style?.width,
        },
        pageInner: {
            backgroundColor: appTheme.guild.tabBackground,
            height: Number(style?.height ?? 0),
            width: style?.width,
        },
        pageInnerScrollView: {
            marginTop: SCROLL_INTERVAL,
        },
        notfound: {
            width: style?.width,
            height: style?.height,
        },
    })
};

type Styles = typeof styles;

export default class GuildsTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};