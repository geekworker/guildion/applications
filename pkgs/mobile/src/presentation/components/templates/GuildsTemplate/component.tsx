import React from 'react';
import { Animated, View } from 'react-native';
import GuildsHeader from '../../molecules/GuildsHeader';
import GuildsTabBar from '../../organisms/GuildsTabBar';
import GuildsTemplatePresenter from './presenter';
import PagerView from 'react-native-pager-view';
import GuildShowTemplate from '../GuildShowTemplate';
import { IS_STORYBOOK, Rooms } from '@guildion/core';
import { SCROLL_INTERVAL } from './styles';
import NotFoundView from '../../atoms/NotFoundView';
import { localizer } from '@/shared/constants/Localizer';

const AnimatedPagerView = Animated.createAnimatedComponent(PagerView);

const GuildsTemplateComponent: React.FC<GuildsTemplatePresenter.Output> = ({ styles, appTheme, guilds, selectedID, pagerViewState, onPressTab, onScroll, onFocus, onPressRoom, diffClamp, roomsPageState, loading }) => {
    const renderShowPage = () => guilds.toArray().map((guild, key) => {
        const roomPageState = roomsPageState.find(p => p.get('guildId') == guild.id);
        const roomsFetchLoading = !IS_STORYBOOK() && roomPageState?.get('loading') == undefined ? true : roomPageState?.get('loading');
        return (
            <View key={key} style={styles.page}>
                <GuildShowTemplate
                    style={styles.getStyle('pageInner')}
                    appTheme={appTheme}
                    guild={guild}
                    rooms={roomPageState?.get('rooms') ?? new Rooms([])}
                    roomsFetchLoading={roomsFetchLoading || loading}
                    loading={roomsFetchLoading || loading}
                    scrollInsets={{ top: styles.pageInnerScrollView.marginTop }}
                    onScroll={onScroll}
                    translation={diffClamp.interpolate({
                        inputRange: [0, SCROLL_INTERVAL],
                        outputRange: [1, 0],
                        extrapolate: 'extend',
                    })}
                    onPressRoom={onPressRoom}
                />
            </View>
        )
    });
    return (
        <View style={styles.container}>
            <Animated.View style={{
                ...styles.headline,
                transform: [{
                    translateY: diffClamp.interpolate({
                        inputRange: [0, SCROLL_INTERVAL],
                        outputRange: [0, -SCROLL_INTERVAL],
                        extrapolate: 'extend',
                    }),
                }],
            }}>
                <GuildsHeader style={styles.getStyle('header')} appTheme={appTheme}/>
                {loading || guilds.length !== 0 && (
                    <GuildsTabBar
                        guilds={guilds}
                        selectedID={selectedID}
                        style={styles.getStyle('tabbar')}
                        appTheme={appTheme}
                        onPressTab={onPressTab}
                        onFocus={onFocus}
                        loading={loading}
                    />
                )}
            </Animated.View>
            {loading || guilds.length !== 0 && (
                <AnimatedPagerView
                    ref={pagerViewState.pagerViewRef}
                    style={styles.pager}
                    initialPage={pagerViewState.selectedIndex}
                    scrollEnabled={false}
                    onPageScroll={pagerViewState.onPageScroll}
                    onPageSelected={pagerViewState.onPageSelected}
                >
                    {renderShowPage()}
                </AnimatedPagerView>
            )}
            {!loading && guilds.length === 0 && (
                <NotFoundView
                    style={styles.getStyle('notfound')}
                    title={localizer.dictionary.guild.home.empty.title}
                    description={localizer.dictionary.guild.home.empty.description}
                />
            )}
        </View>
    );
};

export default React.memo(GuildsTemplateComponent, GuildsTemplatePresenter.outputAreEqual);