import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import GuildsTemplateHooks from "./hooks";
import GuildsTemplateStyles, { SCROLL_INTERVAL } from './styles';
import React from 'react';
import { Guild, Guilds, Room } from "@guildion/core";
import { usePagerView, UsePagerViewOutput } from "@/shared/hooks/usePagerView";
import { Animated, NativeScrollEvent, NativeSyntheticEvent } from "react-native";
import { GuildRoomsPageState } from "@/presentation/redux/Room/RoomReducer";
import { compare } from "@/shared/modules/ObjectCompare";

namespace GuildsTemplatePresenter {
    export type Input = {
        children?: React.ReactNode,
        loading?: boolean,
        guilds: Guilds,
        roomsPageState: GuildRoomsPageState,
        selectedID?: string,
        onPressTab?: (guild: Guild) => void,
        onFocus?: (guild: Guild) => void,
        onPressRoom?: (room: Room) => void,
        onPressDashboard?: (guild: Guild) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildsTemplateStyles,
        appTheme: AppTheme,
        guilds: Guilds,
        roomsPageState: GuildRoomsPageState,
        selectedID?: string,
        onPressTab: (guild: Guild) => void,
        onFocus?: (guild: Guild) => void,
        onPressRoom?: (room: Room) => void,
        onPressDashboard?: (guild: Guild) => void,
        onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        pagerViewState: UsePagerViewOutput & { selectedID?: string },
        scrollY: Animated.Value,
        diffClamp: Animated.AnimatedDiffClamp,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = GuildsTemplateHooks.useStyles({ ...props });
        const propsSelectedID = props.selectedID ?? props.guilds.first?.id;
        const Index = props.guilds.toArray().findIndex(g => g.id == propsSelectedID);
        const propsSelectedIndex = Index < 0 ? 0 : Index;
        const pagerViewState = usePagerView({ selectedIndex: propsSelectedIndex });
        const pagerSelectedID = props.guilds.find((g, i) => i == pagerViewState.selectedIndex)?.id;
        const { scrollY, diffClamp } = GuildsTemplateHooks.useEffectScrollAnimation();
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            selectedID: propsSelectedID,
            pagerViewState: { ...pagerViewState, selectedID: pagerSelectedID },
            onPressTab: (guild: Guild) => {
                const index = props.guilds.findIndex(g => g.id == guild.id);
                if (index >= 0) pagerViewState.setPage(index);
                if (props.onPressTab) props.onPressTab(guild);
            },
            onScroll: (e) => {
                if (e.nativeEvent.contentOffset.y < -SCROLL_INTERVAL || e.nativeEvent.contentOffset.y > e.nativeEvent.layoutMeasurement.height - (4 * SCROLL_INTERVAL)) return;
                scrollY.setValue(e.nativeEvent.contentOffset.y)
            },
            scrollY,
            diffClamp,
        }
    }
}

export default GuildsTemplatePresenter;