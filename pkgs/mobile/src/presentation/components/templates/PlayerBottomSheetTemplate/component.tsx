import React from 'react';
import PlayerBottomSheetTemplatePresenter from './presenter';
import BottomSheet from '@gorhom/bottom-sheet';
import { Animated } from 'react-native';
import IconsTabBar from '../../organisms/IconsTabBar';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/Feather'
import { PlayerBottomSheetTemplateTabId, initialPlayerBottomSheetTemplateTabId } from './constants';
import { localizer } from '@/shared/constants/Localizer';
import PlayerBottomSheetTemplateHooks from './hooks';
import { GestureTranslationProvider } from '@/shared/hooks/useBottomSheetGestureEventsHandlers';

const PlayerBottomSheetTemplateComponent = ({ styles, appTheme, hidden, bottomSheetRef, snapPoints, animationConfigs, handleSnapPress, hiddenTransition, onChange, onChangeTabId, activeTabId, currentSnapPoint, gestureTranslationY, gestureEventsHandlersHook }: PlayerBottomSheetTemplatePresenter.Output) => {
    return (
        <GestureTranslationProvider value={gestureTranslationY}>
            <BottomSheet
                ref={bottomSheetRef}
                index={PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint)}
                snapPoints={[...snapPoints]}
                animationConfigs={animationConfigs}
                handleStyle={{ opacity: PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint) == 0 && hidden ? 0 : 1 }}
                enableContentPanningGesture={!(hidden && PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint) == 0)}
                enableHandlePanningGesture={!(hidden && PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint) == 0)}
                enablePanDownToClose={(hidden && PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint) == 0)}
                backgroundComponent={(props) => (
                    <Animated.View
                        style={[
                            props.style,
                            styles.background,
                            { opacity: PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint) == 0 ? hiddenTransition : 1 },
                        ]}
                    />
                )}
                onChange={onChange}
                gestureEventsHandlersHook={gestureEventsHandlersHook}
            >
                <Animated.View
                    style={{ opacity: PlayerBottomSheetTemplateHooks.snapPoints.indexOf(currentSnapPoint) == 0 ? hiddenTransition : 1 }}
                >
                    <IconsTabBar
                        style={styles.getStyle('bar')}
                        activeId={initialPlayerBottomSheetTemplateTabId}
                        data={[
                            {
                                title: localizer.dictionary.player.tabs.playNext,
                                id: PlayerBottomSheetTemplateTabId.Playlist,
                                Icon: (props) => <FontAwesomeIcon {...props} name={'list'} />,
                            },
                            {
                                title: localizer.dictionary.player.tabs.detail,
                                id: PlayerBottomSheetTemplateTabId.Detail,
                                Icon: (props) => <FeatherIcon {...props} name={'code'} />,
                            },
                        ]}
                        onPress={onChangeTabId}
                    />
                </Animated.View>
            </BottomSheet>
        </GestureTranslationProvider>
    );
};

export default React.memo(PlayerBottomSheetTemplateComponent, PlayerBottomSheetTemplatePresenter.outputAreEqual);