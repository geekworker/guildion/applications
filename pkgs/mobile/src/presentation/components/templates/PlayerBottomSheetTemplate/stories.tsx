import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PlayerBottomSheetTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

storiesOf('PlayerBottomSheetTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView style={{ backgroundColor: fallbackAppTheme.background.player }}>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PlayerBottomSheetTemplate
            style={new Style({})}
        />
    ))
    .add('CustomHooks', () => (
        <PlayerBottomSheetTemplate
            style={new Style({})}
            handleOnStart={() => {}}
            handleOnActive={() => {}}
            handleOnEnd={() => {}}
        />
    ))