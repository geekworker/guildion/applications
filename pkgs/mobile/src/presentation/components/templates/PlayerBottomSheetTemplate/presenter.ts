import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import PlayerBottomSheetTemplateHooks from './hooks';
import PlayerBottomSheetTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import BottomSheet, { GestureEventsHandlersHookType } from '@gorhom/bottom-sheet';
import Animated from 'react-native-reanimated';
import { Animated as NativeAnimated } from 'react-native';
import React from 'react';
import { PlayerBottomSheetTemplateTabId } from './constants';
import { BottomSheetGestureEvents } from '@/shared/hooks/useBottomSheetGestureEventsHandlers';

namespace PlayerBottomSheetTemplatePresenter {
    export type Input = {
        activeTabId?: PlayerBottomSheetTemplateTabId,
        onChangeSnapPoint?: (snapPoint: PlayerBottomSheetTemplateHooks.SnapPoint) => void,
        onChangeTabId?: (id: PlayerBottomSheetTemplateTabId) => void,
        hidden?: boolean,
    } & Partial<StyleProps> & BottomSheetGestureEvents;
    
    export type Output = {
        styles: PlayerBottomSheetTemplateStyles,
        appTheme: AppTheme,
        bottomSheetRef: React.RefObject<BottomSheet>,
        snapPoints: typeof PlayerBottomSheetTemplateHooks.snapPoints,
        animationConfigs: Omit<Animated.WithSpringConfig, "velocity">,
        handleSnapPress: (point: PlayerBottomSheetTemplateHooks.SnapPoint) => void,
        currentSnapPoint: PlayerBottomSheetTemplateHooks.SnapPoint,
        onChange: (index: number) => void,
        activeTabId: PlayerBottomSheetTemplateTabId,
        onChangeTabId: (id: string) => void,
        gestureTranslationY: Animated.SharedValue<number>,
        gestureEventsHandlersHook: GestureEventsHandlersHookType,
        hiddenTransition: NativeAnimated.Value | NativeAnimated.AnimatedInterpolation,
        hidden?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PlayerBottomSheetTemplateHooks.useStyles({ ...props });
        const {
            bottomSheetRef,
            snapPoints,
            animationConfigs,
            handleSnapPress,
            currentSnapPoint,
            onChange,
            activeTabId,
            onChangeTabId,
            gestureTranslationY,
            gestureEventsHandlersHook,
            hiddenTransition,
        } = PlayerBottomSheetTemplateHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            bottomSheetRef,
            snapPoints,
            animationConfigs,
            handleSnapPress,
            currentSnapPoint,
            onChange,
            activeTabId,
            onChangeTabId,
            gestureTranslationY,
            gestureEventsHandlersHook,
            hiddenTransition,
        }
    }
}

export default PlayerBottomSheetTemplatePresenter;