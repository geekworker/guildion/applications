import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { ScaledSizeZero } from '@/shared/constants/ScaledSize';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { ScaledSize, StyleSheet } from 'react-native';

const styles = ({ appTheme, style, window }: StyleProps & { window: ScaledSize }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        background: {
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            backgroundColor: appTheme.background.subm1,
            shadowColor: style?.shadowColor ?? appTheme.shadow.defaultShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.shadow.defaultShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        bar: {
            width: window.width,
            height: 40,
        },
    })
};

type Styles = typeof styles;

export default class PlayerBottomSheetTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({ window: ScaledSizeZero })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { window: ScaledSize }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};