import React from 'react';
import PlayerBottomSheetTemplateStyles from './styles';
import type PlayerBottomSheetTemplatePresenter from './presenter';
import BottomSheet, { useBottomSheetSpringConfigs } from '@gorhom/bottom-sheet';
import { Animated, Easing, useWindowDimensions } from 'react-native';
import { initialPlayerBottomSheetTemplateTabId, PlayerBottomSheetTemplateTabId } from './constants';
import { CaseIterable, guardCase } from '@guildion/core';
import { useSharedValue, runOnJS } from 'react-native-reanimated';
import { useBottomSheetGestureEventsHandlers } from '@/shared/hooks/useBottomSheetGestureEventsHandlers';

namespace PlayerBottomSheetTemplateHooks {
    export const useStyles = (props: PlayerBottomSheetTemplatePresenter.Input) => {
        const window = useWindowDimensions();
        const styles = React.useMemo(() => new PlayerBottomSheetTemplateStyles({ ...props, window }), [
            props.appTheme,
            props.style,
            window,
        ]);
        return styles;
    }

    export const snapPoints = <const>[80, '60%', '100%'];
    export type SnapPoint = typeof snapPoints[number];

    export const useState = (props: PlayerBottomSheetTemplatePresenter.Input) => {
        const bottomSheetRef = React.useRef<BottomSheet>(null);
        const snapPoints = React.useMemo(() => PlayerBottomSheetTemplateHooks.snapPoints, []);
        const animationConfigs = useBottomSheetSpringConfigs({
            damping: 80,
            overshootClamping: true,
            restDisplacementThreshold: 0.1,
            restSpeedThreshold: 0.1,
            stiffness: 500,
        });
        const handleSnapPress = React.useCallback((point: SnapPoint) => {
            bottomSheetRef.current?.snapToIndex(snapPoints.indexOf(point));
        }, []);

        const [currentSnapPoint, setCurrentSnapPoint] = React.useState<SnapPoint>(snapPoints[0]);
        const onChange = React.useCallback((index: number) => {
            const newPoint = snapPoints[index];
            if (newPoint) {
                setCurrentSnapPoint(newPoint);
                props.onChangeSnapPoint && props.onChangeSnapPoint(newPoint)
            }
        }, []);

        const [activeTabId, setActiveTabId] = React.useState<PlayerBottomSheetTemplateTabId>(props.activeTabId ?? initialPlayerBottomSheetTemplateTabId);
        const onChangeTabId = React.useCallback((id: string) => {
            const newId = guardCase<CaseIterable<PlayerBottomSheetTemplateTabId>>(id, { cases: PlayerBottomSheetTemplateTabId, fallback: activeTabId })
            setActiveTabId(newId);
            props.onChangeTabId && props.onChangeTabId(newId);
            if (currentSnapPoint == snapPoints[0]) {
                setCurrentSnapPoint(snapPoints[1]);
                bottomSheetRef.current?.snapToIndex(1);
            }
        }, [activeTabId, props.onChangeTabId, currentSnapPoint]);

        const gestureTranslationY = useSharedValue(0);
        const gestureEventsHandlersHook = useBottomSheetGestureEventsHandlers({ ...props });

        const hiddenTransition = React.useRef(new Animated.Value(props.hidden ? 0 : 1)).current;
        const [hidden, setHidden] = React.useState<boolean>(props.hidden ?? false);
        React.useEffect(() => {
            if (props.hidden && hidden != props.hidden) {
                setHidden(true);
                Animated.timing(hiddenTransition, {
                    toValue: 0,
                    easing: Easing.ease,
                    useNativeDriver: true,
                    duration: 90,
                }).start();
            } else if (!props.hidden && hidden != props.hidden) {
                Animated.timing(hiddenTransition, {
                    toValue: 1,
                    easing: Easing.ease,
                    useNativeDriver: true,
                    duration: 90,
                }).start();
                setHidden(false);
            }
        }, [props.hidden]);

        return {
            bottomSheetRef,
            snapPoints,
            animationConfigs,
            handleSnapPress,
            currentSnapPoint,
            onChange,
            activeTabId,
            onChangeTabId,
            gestureTranslationY,
            gestureEventsHandlersHook,
            hiddenTransition,
        };
    }
}

export default PlayerBottomSheetTemplateHooks;