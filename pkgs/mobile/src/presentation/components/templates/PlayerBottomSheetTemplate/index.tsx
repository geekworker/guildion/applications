import React from 'react';
import PlayerBottomSheetTemplateComponent from './component';
import PlayerBottomSheetTemplatePresenter from './presenter';

const PlayerBottomSheetTemplate = (props: PlayerBottomSheetTemplatePresenter.Input) => {
    const output = PlayerBottomSheetTemplatePresenter.usePresenter(props);
    return <PlayerBottomSheetTemplateComponent {...output} />;
};

export default React.memo(PlayerBottomSheetTemplate, PlayerBottomSheetTemplatePresenter.inputAreEqual);