export const PlayerBottomSheetTemplateTabId = {
    Playlist: 'Playlist',
    Detail: 'Detail',
} as const;

export type PlayerBottomSheetTemplateTabId = typeof PlayerBottomSheetTemplateTabId[keyof typeof PlayerBottomSheetTemplateTabId];

export const initialPlayerBottomSheetTemplateTabId = PlayerBottomSheetTemplateTabId.Playlist;