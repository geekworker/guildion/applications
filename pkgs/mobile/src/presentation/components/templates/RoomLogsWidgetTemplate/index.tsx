import React from 'react';
import RoomLogsWidgetTemplateComponent from './component';
import RoomLogsWidgetTemplatePresenter from './presenter';

const RoomLogsWidgetTemplate = (props: RoomLogsWidgetTemplatePresenter.Input) => {
    const output = RoomLogsWidgetTemplatePresenter.usePresenter(props);
    return <RoomLogsWidgetTemplateComponent {...output} />;
};

export default React.memo(RoomLogsWidgetTemplate, RoomLogsWidgetTemplatePresenter.inputAreEqual);