import React from "react";
import RoomLogsWidgetTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace RoomLogsWidgetTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomLogsWidgetTemplateStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default RoomLogsWidgetTemplateHooks