import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomLogsWidgetTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Guild, Room, RoomLogs } from '@guildion/core';

storiesOf('RoomLogsWidgetTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomLogsWidgetTemplate
            style={new Style({})}
            room={new Room()}
            guild={new Guild()}
            logs={new RoomLogs([])}
        />
    ))