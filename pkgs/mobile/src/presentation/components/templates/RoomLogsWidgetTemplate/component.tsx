import { localizer } from '@/shared/constants/Localizer';
import { staticRoomWidgets } from '@/shared/constants/StaticRoomWidgets';
import React from 'react';
import { Text, View } from 'react-native';
import RoomLogsList from '../../organisms/RoomLogsList';
import WidgetsList from '../../organisms/WidgetsList';
import RoomLogsWidgetTemplatePresenter from './presenter';

const RoomLogsWidgetTemplateComponent: React.FC<RoomLogsWidgetTemplatePresenter.Output> = ({ styles, appTheme, logs, onPressWidget, loading }) => {
    return (
        <View style={styles.container}>
            <RoomLogsList
                appTheme={appTheme}
                style={styles.getStyle('scrollView')}
                logs={logs}
                ListHeaderComponent={logs.size > 0 && !loading ? (
                    <View style={styles.headerInner}>
                        <Text style={styles.logsTitle}>
                            {localizer.dictionary.room.show.logs.title}
                        </Text>
                        {logs.size == 0 && (
                            <Text style={styles.logsEmptyText}>
                                {localizer.dictionary.room.show.logs.emptyText}
                            </Text>
                        )}
                    </View>
                ) : undefined}
                ListHeaderComponentStyle={styles.header}
                ListFooterComponent={!loading ? (
                    <View style={styles.footerInner}>
                        <Text style={styles.widgetsTitle}>
                            {localizer.dictionary.room.show.widgets.title}
                        </Text>
                        <WidgetsList
                            widgets={staticRoomWidgets}
                            style={styles.getStyle('widgetsList')}
                            onPressWidget={onPressWidget}
                        />
                    </View>
                ) : null}
                ListFooterComponentStyle={styles.footer}
            />
        </View>
    );
};

export default React.memo(RoomLogsWidgetTemplateComponent, RoomLogsWidgetTemplatePresenter.outputAreEqual);