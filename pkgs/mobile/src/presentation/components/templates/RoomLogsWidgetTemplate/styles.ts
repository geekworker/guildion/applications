import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.subm1,
        },
        scrollView: {
            width: style?.width,
            height: style?.height,
        },
        header: {
            width: style?.width,
        },
        headerInner: {
            width: style?.width,
        },
        footer: {
            width: style?.width,
        },
        footerInner: {
            width: style?.width,
        },
        logsTitle: {
            fontSize: 24,
            fontFamily: MontserratFont.Bold,
            textAlign: 'left',
            marginLeft: 12,
            marginTop: 12,
            color: appTheme.element.default,
        },
        logsEmptyText: {
            fontSize: 14,
            fontFamily: MontserratFont.Regular,
            textAlign: 'left',
            marginLeft: 12,
            marginTop: 4,
            color: appTheme.element.subp1,
        },
        widgetsTitle: {
            fontSize: 24,
            fontFamily: MontserratFont.Bold,
            textAlign: 'left',
            marginLeft: 12,
            marginTop: 20,
            color: appTheme.element.default,
        },
        widgetsList: {
            width: Number(style?.width ?? 0) - 8,
            marginTop: 8,
            paddingLeft: 4,
            paddingRight: 4,
        },
    })
};

type Styles = typeof styles;

export default class RoomLogsWidgetTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};