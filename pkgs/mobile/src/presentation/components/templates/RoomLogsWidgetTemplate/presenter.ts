import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomLogsWidgetTemplateHooks from "./hooks";
import RoomLogsWidgetTemplateStyles from './styles';
import React from 'react';
import { Guild, Room, RoomLogs, Widget } from "@guildion/core";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomLogsWidgetTemplatePresenter {
    export type Input = {
        children?: React.ReactNode,
        room: Room,
        guild: Guild,
        logs: RoomLogs,
        onPressWidget?: (widget: Widget) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomLogsWidgetTemplateStyles,
        appTheme: AppTheme,
        room: Room,
        guild: Guild,
        logs: RoomLogs,
        onPressWidget?: (widget: Widget) => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoomLogsWidgetTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomLogsWidgetTemplatePresenter;