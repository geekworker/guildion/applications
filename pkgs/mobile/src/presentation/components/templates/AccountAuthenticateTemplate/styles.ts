import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.root,
            paddingTop: 30,
        },
        usernameField: {
            width: Number(style?.width ?? 0) - 50,
            marginLeft: 25,
            marginRight: 25,
            marginBottom: 20,
        },
        submit: {
            width: 140,
            height: 50,
            marginLeft: (Number(style?.width ?? 0) - 140) / 2,
            marginRight: (Number(style?.width ?? 0) - 140) / 2,
        },
    })
};

type Styles = typeof styles;

export default class AccountAuthenticateTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};