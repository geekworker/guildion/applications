import React from 'react';
import { GestureResponderEvent, View } from 'react-native';
import AccountAuthenticateTemplateStyles from './styles';
import TextField from '../../atoms/TextField';
import Button from '../../atoms/Button';
import { localizer } from '@/shared/constants/Localizer';
import StyleProps from '@/shared/interfaces/StyleProps';
import DismissKeyboardView from '../../atoms/DismissKeyboardView';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    username?: string,
    sendable?: boolean,
    usernameError?: string,
    loading?: boolean,
    onChangeUsername?: (username?: string) => void,
    onSubmit?: (event: GestureResponderEvent) => void,
} & StyleProps;

const AccountAuthenticateTemplate: React.FC<Props> = ({
    style,
    appTheme,
    username,
    sendable,
    usernameError,
    loading,
    onChangeUsername,
    onSubmit,
}) => {
    const styles = React.useMemo(() => new AccountAuthenticateTemplateStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <DismissKeyboardView>
            <View style={styles.wrapper} >
                <View style={styles.usernameField}>
                    <TextField
                        label={localizer.dictionary.account.attr.username}
                        onChangeText={(text) => onChangeUsername && onChangeUsername(text)}
                        value={username}
                        errorMessage={usernameError}
                    />
                </View>
                <Button
                    styleType={'fill'}
                    style={styles.getStyle('submit')}
                    loading={loading}
                    disabled={!sendable}
                    onPress={onSubmit}
                    title={localizer.dictionary.account.authentication.submit}
                />
            </View>
        </DismissKeyboardView>
    )
};

export default React.memo(AccountAuthenticateTemplate, compare);