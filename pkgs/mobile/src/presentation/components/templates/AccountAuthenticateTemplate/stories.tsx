import { storiesOf } from '@storybook/react-native';
import { boolean, number, text } from '@storybook/addon-knobs';
import React from 'react';
import AccountAuthenticateTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('AccountAuthenticateTemplate', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <AccountAuthenticateTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            username={text('username', '')}
            usernameError={text('usernameError', '')}
            loading={boolean('loading', false)}
            sendable={boolean('sendable', false)}
        />
    ))
