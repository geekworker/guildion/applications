import { storiesOf } from '@storybook/react-native';
import React from 'react';
import TextChatWidgetTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import RootBackground from '../../molecules/RootBackground';
import { number } from '@storybook/addon-knobs';
import { Messages, Message, Member, OGP, OGPs, Files } from '@guildion/core';
import uuid from 'react-native-uuid';
import RoomControlButtonProvider from '../../molecules/RoomControlButton/provider';

const mockMessages = new Messages([
    new Message({
        id: `${uuid.v4()}`,
        body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
        createdAt: new Date("2021-11-19T15:00:00.000Z").toISOString(),
    }, {
        sender: new Member({
            displayName: 'user1',
        }),
        ogps: new OGPs([
            new OGP({
                id: `${uuid.v4()}`,
                title: 'guildion - video community service -',
                url: 'https://www.guildion.co',
                imageURL: Files.getSeed().generateGuildProfile().url,
            }),
        ]),
        files: new Files([
            Files.getSeed().generateGuildProfile(),
        ])
    }),
    new Message({
        id: `${uuid.v4()}`,
        body: 'hello',
        createdAt: new Date("2021-11-21T10:00:00.000Z").toISOString(),
    }, {
        sender: new Member({
            displayName: 'user1',
        }),
    }),
    new Message({
        id: `${uuid.v4()}`,
        body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
        createdAt: new Date().toISOString(),
    }, {
        sender: new Member({
            displayName: 'user1',
        }),
        ogps: new OGPs([
            new OGP({
                id: `${uuid.v4()}`,
                title: 'guildion - video community service -',
                url: 'https://www.guildion.co',
                imageURL: Files.getSeed().generateGuildProfile().url,
            }),
        ]),
        files: new Files([
            Files.getSeed().generateGuildProfile(),
        ])
    }),
    new Message({
        id: `${uuid.v4()}`,
        body: 'hello',
        createdAt: new Date().toISOString(),
    }, {
        sender: new Member({
            displayName: 'user1',
        }),
    }),
    new Message({
        id: `${uuid.v4()}`,
        body: `@room Hello [!sample:321e32dew] this is an example of the **ParsedText**, check this 34:22 and links like http://www.google.com or http://www.facebook.com are clickable and phone number 444-555-6666 can call too.
But you can also do more with this package, for example [@michel:5455345] will change style and David too. foo@gmail.com
And the magic number is 42! \`THANKS\`
\`\`\`() => 'This is Guildion';\`\`\`
test test test`,
        createdAt: new Date().toISOString(),
    }, {
        sender: new Member({
            displayName: 'user1',
        }),
        ogps: new OGPs([
            new OGP({
                id: `${uuid.v4()}`,
                title: 'guildion - video community service -',
                url: 'https://www.guildion.co',
                imageURL: Files.getSeed().generateGuildProfile().url,
            }),
        ]),
        files: new Files([
            Files.getSeed().generateGuildProfile(),
        ])
    }),
    new Message({
        id: `${uuid.v4()}`,
        body: 'hello',
        createdAt: new Date().toISOString(),
    }, {
        sender: new Member({
            displayName: 'user1',
        }),
    })
]);

storiesOf('TextChatWidgetTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <RootBackground
                    containerStyle={new Style({
                        width: number('width', 390),
                        height: number('height', 724),
                    })}
                >
                    <RoomControlButtonProvider>
                        <StorybookNavigationContainer>
                            <CenterView>
                                {getStory()}
                            </CenterView>
                        </StorybookNavigationContainer>
                    </RoomControlButtonProvider>
                </RootBackground>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <TextChatWidgetTemplate
            style={new Style({
                width: number('width', 390),
                height: number('height', 724),
            })}
            messages={mockMessages}
            message={new Message()}
            isFocused={true}
        />
    ))