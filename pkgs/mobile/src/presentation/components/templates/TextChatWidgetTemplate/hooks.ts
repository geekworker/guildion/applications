import React from "react";
import TextChatWidgetTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import type TextChatWidgetTemplatePresenter from "./presenter";
import RoomControlButtonRedux from "../../molecules/RoomControlButton/redux";
import TopMostsRedux from "../../molecules/TopMosts/redux";

namespace TextChatWidgetTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const insets = useSafeAreaInsets();
        const styles = React.useMemo(() => new TextChatWidgetTemplateStyles({ ...props, insets }), [
            props,
            insets,
        ]);
        return styles;
    }

    export const usePositionFixed = (props: TextChatWidgetTemplatePresenter.Input & { styles: TextChatWidgetTemplateStyles }) => {
        const context = React.useContext(TopMostsRedux.Context);
        const controlContext = React.useContext(RoomControlButtonRedux.Context);
        React.useEffect(() => {
            if (props.isFocused) {
                controlContext.dispatch(RoomControlButtonRedux.Action.setOffsetBottom(44));
                context.dispatch(TopMostsRedux.Action.showMessageInputBar({
                    message: props.message,
                    style: props.styles.getStyle('inputBar'),
                    submitting: props.submitting,
                    onPressMedia: props.onPressMedia,
                    onSubmit: props.onSubmit,
                }));
            };
        }, [props.isFocused, props.message, props.submitting, props.onPressMedia, props.onSubmit]);
        React.useEffect(() => {
            if(!props.isFocused) {
                controlContext.dispatch(RoomControlButtonRedux.Action.setOffsetBottom(0));
                context.dispatch(TopMostsRedux.Action.hideMessageInputBar());
            }
        }, [props.isFocused]);
        React.useEffect(() => {
            return () => {
                controlContext.dispatch(RoomControlButtonRedux.Action.setOffsetBottom(0));
                context.dispatch(TopMostsRedux.Action.hideMessageInputBar());
            };
        }, []);
    }
}

export default TextChatWidgetTemplateHooks;