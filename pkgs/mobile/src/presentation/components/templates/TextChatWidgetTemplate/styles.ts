import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { EdgeInsetsZero } from "@/shared/constants/EdgeInsets";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native'; 
import { EdgeInsets } from "react-native-safe-area-context";
import { TOP_HEIGHT } from "../RoomShowTemplate/styles";

const styles = ({ appTheme, style, insets }: StyleProps & { insets: EdgeInsets }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        list: {
            width: style?.width,
            height: (style?.getAsNumber('height') ?? 0) - (insets.bottom + 44),
            paddingBottom: 20,
        },
        inputBar: {
            position: 'absolute',
            bottom: 0,
            left: 0,
            right: 0,
            width: style?.width,
            paddingBottom: insets.bottom,
            height: insets.bottom + 44,
            backgroundColor: appTheme.background.subp1,
        },
        footerMargin: {
            marginBottom: TOP_HEIGHT,
        },
    })
};

type Styles = typeof styles;

export default class TextChatWidgetTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({ insets: EdgeInsetsZero })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { insets: EdgeInsets }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};