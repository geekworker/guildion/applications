import React from 'react';
import TextChatWidgetTemplateComponent from './component';
import TextChatWidgetTemplatePresenter from './presenter';

const TextChatWidgetTemplate = (props: TextChatWidgetTemplatePresenter.Input) => {
    const output = TextChatWidgetTemplatePresenter.usePresenter(props);
    return <TextChatWidgetTemplateComponent {...output} />;
};

export default React.memo(TextChatWidgetTemplate, TextChatWidgetTemplatePresenter.inputAreEqual);