import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import TextChatWidgetTemplateHooks from "./hooks";
import TextChatWidgetTemplateStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Message, Messages } from "@guildion/core";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";

namespace TextChatWidgetTemplatePresenter {
    export type Input = {
        message: Message,
        editMessage?: Message,
        messages: Messages,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        isFocused: boolean,
        decrementable?: boolean,
        incrementable?: boolean,
        onPressSender?: (sender: Member) => void,
        onLongPressMessage?: (message: Message) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        onScrollToTop?: () => void;
        onScrollToBottom?: () => void;
        submitting?: boolean,
        onSubmit?: (message: Message) => void,
        onPressMedia?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: TextChatWidgetTemplateStyles,
        appTheme: AppTheme,
        animatedStyle?: AnimatedStyle,
        message: Message,
        editMessage?: Message,
        messages: Messages,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        isFocused: boolean,
        decrementable?: boolean,
        incrementable?: boolean,
        onPressSender?: (sender: Member) => void,
        onLongPressMessage?: (message: Message) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        onScrollToTop?: () => void;
        onScrollToBottom?: () => void;
        submitting?: boolean,
        onSubmit?: (message: Message) => void,
        onPressMedia?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = TextChatWidgetTemplateHooks.useStyles({ ...props });
        TextChatWidgetTemplateHooks.usePositionFixed({ ...props, styles });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default TextChatWidgetTemplatePresenter;