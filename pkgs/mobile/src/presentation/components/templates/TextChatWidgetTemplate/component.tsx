import React from 'react';
import { Animated } from 'react-native';
import MessagesList from '../../organisms/MessagesList';
import TextChatWidgetTemplatePresenter from './presenter';

const TextChatWidgetTemplateComponent = ({ styles, animatedStyle, incrementable, decrementable, messages, editMessage, loadingMore, onPressSender, onLongPressMessage, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, onScrollToBottom, onScrollToTop, loading }: TextChatWidgetTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <MessagesList
                messages={messages}
                editingMessageId={editMessage?.id}
                style={styles.getStyle('list')}
                animatedStyle={animatedStyle}
                loadingMore={loadingMore}
                loading={loading}
                onPressSender={onPressSender}
                onLongPressMessage={onLongPressMessage}
                onPressFile={onPressFile}
                onPressURL={onPressURL}
                onPressEmail={onPressEmail}
                onPressPhoneNumber={onPressPhoneNumber}
                onPressMentionMember={onPressMentionMember}
                onPressMentionRoom={onPressMentionRoom}
                onScrollToBottom={onScrollToBottom}
                onScrollToTop={onScrollToTop}
                incrementable={incrementable}
                decrementable={decrementable}
            />
        </Animated.View>
    );
};

export default React.memo(TextChatWidgetTemplateComponent, TextChatWidgetTemplatePresenter.outputAreEqual);