import React from 'react';
import FileNewBottomSheetTemplateStyles from './styles';
import type FileNewBottomSheetTemplatePresenter from './presenter';

namespace FileNewBottomSheetTemplateHooks {
    export const useStyles = (props: FileNewBottomSheetTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new FileNewBottomSheetTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: FileNewBottomSheetTemplatePresenter.Input) => {
        return {};
    }
}

export default FileNewBottomSheetTemplateHooks;