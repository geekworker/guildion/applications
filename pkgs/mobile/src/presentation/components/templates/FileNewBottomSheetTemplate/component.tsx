import React from 'react';
import { Animated, View } from 'react-native';
import FileNewBottomSheetTemplatePresenter from './presenter';

const FileNewBottomSheetTemplateComponent = ({ styles, appTheme, animatedStyle,  }: FileNewBottomSheetTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
        </Animated.View>
    );
};

export default React.memo(FileNewBottomSheetTemplateComponent, FileNewBottomSheetTemplatePresenter.outputAreEqual);