import React from 'react';
import FileNewBottomSheetTemplateComponent from './component';
import FileNewBottomSheetTemplatePresenter from './presenter';

const FileNewBottomSheetTemplate = (props: FileNewBottomSheetTemplatePresenter.Input) => {
    const output = FileNewBottomSheetTemplatePresenter.usePresenter(props);
    return <FileNewBottomSheetTemplateComponent {...output} />;
};

export default React.memo(FileNewBottomSheetTemplate, FileNewBottomSheetTemplatePresenter.inputAreEqual);