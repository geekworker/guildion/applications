import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import FileNewBottomSheetTemplateHooks from './hooks';
import FileNewBottomSheetTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

namespace FileNewBottomSheetTemplatePresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: FileNewBottomSheetTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = FileNewBottomSheetTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default FileNewBottomSheetTemplatePresenter;