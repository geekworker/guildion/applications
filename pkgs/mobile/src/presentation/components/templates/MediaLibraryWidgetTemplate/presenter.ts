import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import MediaLibraryWidgetTemplateHooks from './hooks';
import MediaLibraryWidgetTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import { File, Files, Guild, Room, SocialLink } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

namespace MediaLibraryWidgetTemplatePresenter {
    export type Input = {
        room?: Room,
        guild?: Guild,
        title?: string,
        sectionTitle?: string,
        loading?: boolean,
        files: Files,
        loadingMore?: LoadingMoreStatus,
        isFocused: boolean,
        showAlbumButton?: boolean,
        showPlaylistButton?: boolean,
        showRoomButton?: boolean,
        showRoomHeader?: boolean,
        showHeader?: boolean,
        showSearchBar?: boolean,
        onSearch?: (query: string) => void,
        onPressRoomFiles?: () => void,
        onPressFile?: (file: File) => void,
        onPressFolder?: (file: File) => void,
        onPressMenu?: (file: File) => void,
        onPressPlaylists?: () => void,
        onPressAlbums?: () => void,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        onScrollToBottom?: () => void,
        onPressClose?: () => void,
        onPressBack?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MediaLibraryWidgetTemplateStyles,
        appTheme: AppTheme,
        room?: Room,
        guild?: Guild,
        title?: string,
        sectionTitle?: string,
        loading?: boolean,
        files: Files,
        loadingMore?: LoadingMoreStatus,
        isFocused: boolean,
        showAlbumButton?: boolean,
        showPlaylistButton?: boolean,
        showRoomButton?: boolean,
        showRoomHeader?: boolean,
        showHeader?: boolean,
        showSearchBar?: boolean,
        onSearch?: (query: string) => void,
        onPressRoomFiles?: () => void,
        onPressFile?: (file: File) => void,
        onPressFolder?: (file: File) => void,
        onPressMenu?: (file: File) => void,
        onPressPlaylists?: () => void,
        onPressAlbums?: () => void,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        onScrollToBottom?: () => void,
        onPressClose?: () => void,
        onPressBack?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MediaLibraryWidgetTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default MediaLibraryWidgetTemplatePresenter;