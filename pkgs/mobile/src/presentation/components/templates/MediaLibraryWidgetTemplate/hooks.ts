import React from 'react';
import MediaLibraryWidgetTemplateStyles from './styles';
import type MediaLibraryWidgetTemplatePresenter from './presenter';

namespace MediaLibraryWidgetTemplateHooks {
    export const useStyles = (props: MediaLibraryWidgetTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: MediaLibraryWidgetTemplatePresenter.Input) => {
        return {};
    }
}

export default MediaLibraryWidgetTemplateHooks;