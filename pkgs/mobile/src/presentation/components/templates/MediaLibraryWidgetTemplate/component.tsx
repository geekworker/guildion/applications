import React from 'react';
import MediaLibraryWidgetTemplatePresenter from './presenter';
import { SortDirection } from '@guildion/core';
import { localizer } from '@/shared/constants/Localizer';
import { Text, View, Animated } from 'react-native';
import ButtonRow from '../../atoms/ButtonRow';
import SearchInput from '../../atoms/SearchInput';
import FilesList from '../../organisms/FilesList';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import AsyncImage from '../../atoms/AsyncImage';
import { Style } from '@/shared/interfaces/Style';
import RoomPublicProfile from '../../molecules/RoomPublicProfile';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import MediaLibraryHeader from '../../molecules/MediaLibraryHeader';

const MediaLibraryWidgetTemplateComponent = ({ styles, appTheme, loading, files, room, title, sectionTitle, onPressPlaylists, onPressAlbums, onPressRoomFiles, showAlbumButton, showPlaylistButton, showRoomButton, showRoomHeader, showHeader, showSearchBar, onPressSocialLink, loadingMore, onPressClose, onPressBack, onScrollToBottom }: MediaLibraryWidgetTemplatePresenter.Output) => {
    const notfoundView = (
        <View style={styles.notFoundContainer}>
            <View style={styles.notFoundInner}>
                <Text style={styles.notFoundTitle}>
                    {localizer.dictionary.room.mediaLibrary.empty.title}
                </Text>
                <Text style={styles.notFoundDescription}>
                    {localizer.dictionary.room.mediaLibrary.empty.description}
                </Text>
            </View>
        </View>
    );
    return (
        <Animated.View style={styles.container}>
            {showHeader && (
                <MediaLibraryHeader
                    onPressBack={onPressBack}
                    onPressClose={onPressClose}
                    title={title}
                    style={styles.getStyle('navigation')}
                />
            )}
            <FilesList
                files={files}
                loadingMore={loadingMore ?? loading ? LoadingMoreStatus.INCREMENT : undefined}
                sortDirection={SortDirection.ASC}
                style={styles.getStyle('scrollview')}
                onScrollToBottom={onScrollToBottom}
                ListHeaderComponentStyle={styles.header}
                ListHeaderComponent={(
                    <View style={styles.headerInner}>
                        {room && showRoomHeader && (
                            <RoomPublicProfile
                                style={styles.getStyle('profile')}
                                room={room}
                                loading={loading}
                                onPressSocialLink={onPressSocialLink}
                            />
                        )}
                        {showSearchBar && (
                            <SearchInput
                                style={styles.getStyle('search')}
                                placeholder={localizer.dictionary.guild.files.index.searchPlaceholder}
                            />
                        )}
                        {showPlaylistButton && showAlbumButton && showRoomButton && (
                            <Text style={styles.title}>
                                {localizer.dictionary.g.db.menus}
                            </Text>
                        )}
                        {showPlaylistButton && (
                            <ButtonRow
                                style={styles.getStyle('navButton')}
                                text={localizer.dictionary.g.db.playlists}
                                Icon={(props) => <EntypoIcon {...props} size={props.size} name={'folder-video'} />}
                                onPress={onPressPlaylists}
                            />
                        )}
                        {showAlbumButton && (
                            <ButtonRow
                                style={styles.getStyle('navButton')}
                                text={localizer.dictionary.g.db.albums}
                                Icon={(props) => <MaterialIcon {...props} name={'photo-library'} />}
                                onPress={onPressAlbums}
                            />
                        )}
                        {showRoomButton && room && (
                            <ButtonRow
                                style={styles.getStyle('navButton')}
                                text={room.displayName}
                                Icon={(props) => <AsyncImage url={room.profile?.url ?? ''} style={new Style({ width: props.size + 8, height: props.size + 8, marginTop: -4, marginLeft: -4, borderRadius: props.size / 5 })} />}
                                onPress={onPressRoomFiles}
                            />
                        )}
                        {sectionTitle && (
                            <Text style={styles.title}>
                                {sectionTitle}
                            </Text>
                        )}
                        {files.size === 0 && !loading && notfoundView}
                    </View>
                )}
                ListFooterComponent={(
                    <View style={styles.footerMargin} />
                )}
            />
        </Animated.View>
    );
};

export default React.memo(MediaLibraryWidgetTemplateComponent, MediaLibraryWidgetTemplatePresenter.outputAreEqual);