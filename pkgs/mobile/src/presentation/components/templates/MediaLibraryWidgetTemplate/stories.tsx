import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MediaLibraryWidgetTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, Room, RoomActivity, SocialLink, SocialLinks } from '@guildion/core';
import uuid from 'react-native-uuid';
import { number } from '@storybook/addon-knobs';
import RootBackground from '../../molecules/RootBackground';
import { localizer } from '@/shared/constants/Localizer';

const mockFiles = new Files([
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
]);

storiesOf('MediaLibraryWidgetTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <RootBackground
                    containerStyle={new Style({
                        width: number('width', 390),
                        height: number('height', 724),
                    })}
                >
                    <StorybookNavigationContainer>
                        <CenterView>
                            {getStory()}
                        </CenterView>
                    </StorybookNavigationContainer>
                </RootBackground>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MediaLibraryWidgetTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            files={mockFiles}
            room={new Room()}
            isFocused={true}
            showHeader
            showSearchBar
            title={localizer.dictionary.guild.files.index.title}
            sectionTitle={localizer.dictionary.guild.files.index.listTitle}
        />
    ))
    .add('Room', () => (
        <MediaLibraryWidgetTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            files={mockFiles}
            room={new Room({ displayName: 'TestRoom' })}
            showAlbumButton
            showPlaylistButton
            showRoomButton
            showSearchBar
            isFocused={true}
            title={localizer.dictionary.guild.files.index.title}
            sectionTitle={localizer.dictionary.guild.files.index.listTitle}
        />
    ))
    .add('RoomHeader', () => (
        <MediaLibraryWidgetTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            files={mockFiles}
            room={new Room(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateRoomProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new RoomActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
            showAlbumButton
            showPlaylistButton
            showRoomButton
            showRoomHeader
            showSearchBar
            isFocused={true}
            title={localizer.dictionary.guild.files.index.title}
            sectionTitle={localizer.dictionary.guild.files.index.listTitle}
        />
    ))