import React from 'react';
import MediaLibraryWidgetTemplateComponent from './component';
import MediaLibraryWidgetTemplatePresenter from './presenter';

const MediaLibraryWidgetTemplate = (props: MediaLibraryWidgetTemplatePresenter.Input) => {
    const output = MediaLibraryWidgetTemplatePresenter.usePresenter(props);
    return <MediaLibraryWidgetTemplateComponent {...output} />;
};

export default React.memo(MediaLibraryWidgetTemplate, MediaLibraryWidgetTemplatePresenter.inputAreEqual);