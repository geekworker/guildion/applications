import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';
import { TOP_HEIGHT } from '../RoomShowTemplate/styles';

export const NAVIGATION_HEADER_HEIGHT = 44;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const innerWidth: number = (style?.getAsNumber('width') ?? 0) - 40;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: 20,
            paddingRight: 20,
            position: 'relative',
        },
        search: {
            width: innerWidth,
            height: 44,
            marginTop: 20,
            marginBottom: 0,
        },
        navigation: {
            position: 'absolute',
            left: -20,
            right: 0,
            top: 0,
            height: NAVIGATION_HEADER_HEIGHT,
            width: style?.width,
            backgroundColor: appTheme.background.root,
            shadowColor: style?.shadowColor ?? appTheme.shadow.defaultShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.shadow.defaultShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 8,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        },
        navButton: {
            width: innerWidth - 0,
            height: 44,
            marginTop: 8,
            marginLeft: 0,
            marginRight: 0,
        },
        header: {
            marginLeft: 20,
            width: style?.width,
        },
        headerInner: {
            width: style?.width,
        },
        profile: {
            width: innerWidth,
            paddingTop: 20,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        title: {
            fontFamily: MontserratFont.Bold,
            fontSize: 20,
            color: appTheme.element.default,
            textAlign: 'left',
            marginTop: 20,
            marginBottom: 4,
        },
        scrollview: {
            width: style?.width,
            height: style?.height,
            marginLeft: -20,
        },
        footerMargin: {
            marginBottom: TOP_HEIGHT,
        },
        notFoundContainer: {
            width: innerWidth,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        notFoundInner: {
            marginTop: 88,
            marginBottom: 88,
        },
        notFoundTitle: {
            fontSize: 24,
            fontFamily: MontserratFont.Bold,
            textAlign: 'center',
            marginBottom: 8,
            color: appTheme.element.default,
        },
        notFoundDescription: {
            fontSize: 20,
            fontFamily: MontserratFont.Regular,
            textAlign: 'center',
            marginTop: 8,
            marginBottom: 8,
            color: appTheme.element.subp1,
        },
    });
};

type Styles = typeof styles;

export default class MediaLibraryWidgetTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};