import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import UserShowTemplateHooks from "./hooks";
import UserShowTemplateStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";

namespace UserShowTemplatePresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: UserShowTemplateStyles,
        appTheme: AppTheme,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = UserShowTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default UserShowTemplatePresenter;