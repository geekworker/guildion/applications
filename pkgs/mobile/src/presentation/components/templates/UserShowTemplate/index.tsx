import React from 'react';
import UserShowTemplateComponent from './component';
import UserShowTemplatePresenter from './presenter';

const UserShowTemplate = (props: UserShowTemplatePresenter.Input) => {
    const output = UserShowTemplatePresenter.usePresenter(props);
    return <UserShowTemplateComponent {...output} />;
};

export default React.memo(UserShowTemplate, UserShowTemplatePresenter.inputAreEqual);