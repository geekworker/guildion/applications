import React from 'react';
import UserShowTemplatePresenter from './presenter';

const UserShowTemplateComponent = ({ styles, appTheme }: UserShowTemplatePresenter.Output) => {
    return (
        <></>
    );
};

export default React.memo(UserShowTemplateComponent, UserShowTemplatePresenter.outputAreEqual);