import React from "react";
import UserShowTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace UserShowTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new UserShowTemplateStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default UserShowTemplateHooks;