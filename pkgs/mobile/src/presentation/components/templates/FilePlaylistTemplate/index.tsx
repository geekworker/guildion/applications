import React from 'react';
import FilePlaylistTemplateComponent from './component';
import FilePlaylistTemplatePresenter from './presenter';

const FilePlaylistTemplate = (props: FilePlaylistTemplatePresenter.Input) => {
    const output = FilePlaylistTemplatePresenter.usePresenter(props);
    return <FilePlaylistTemplateComponent {...output} />;
};

export default React.memo(FilePlaylistTemplate, FilePlaylistTemplatePresenter.inputAreEqual);