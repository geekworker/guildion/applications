import React from 'react';
import { View } from 'react-native';
import FilePlaylistTemplatePresenter from './presenter';

const FilePlaylistTemplateComponent = ({ styles, appTheme }: FilePlaylistTemplatePresenter.Output) => {
    return (
        <View style={styles.container}>
        </View>
    );
};

export default React.memo(FilePlaylistTemplateComponent, FilePlaylistTemplatePresenter.outputAreEqual);