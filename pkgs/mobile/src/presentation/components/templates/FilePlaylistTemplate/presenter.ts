import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import FilePlaylistTemplateHooks from './hooks';
import FilePlaylistTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';

namespace FilePlaylistTemplatePresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: FilePlaylistTemplateStyles,
        appTheme: AppTheme,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = FilePlaylistTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default FilePlaylistTemplatePresenter;