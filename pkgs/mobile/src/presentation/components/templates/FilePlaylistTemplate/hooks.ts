import React from 'react';
import FilePlaylistTemplateStyles from './styles';
import type FilePlaylistTemplatePresenter from './presenter';

namespace FilePlaylistTemplateHooks {
    export const useStyles = (props: FilePlaylistTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new FilePlaylistTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: FilePlaylistTemplatePresenter.Input) => {
        return {};
    }
}

export default FilePlaylistTemplateHooks;