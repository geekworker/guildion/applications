import React from 'react';
import RoomNewInputTemplateComponent from './component';
import RoomNewInputTemplatePresenter from './presenter';

const RoomNewInputTemplate = (props: RoomNewInputTemplatePresenter.Input) => {
    const output = RoomNewInputTemplatePresenter.usePresenter(props);
    return <RoomNewInputTemplateComponent {...output} />;
};

export default React.memo(RoomNewInputTemplate, RoomNewInputTemplatePresenter.inputAreEqual);