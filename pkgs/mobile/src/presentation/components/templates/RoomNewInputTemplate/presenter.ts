import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomNewInputTemplateHooks from './hooks';
import RoomNewInputTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { GestureResponderEvent } from 'react-native';

namespace RoomNewInputTemplatePresenter {
    export type Input = {
        url: string,
        roomname?: string,
        onPressPicker?: () => void,
        onChangeRoomname?: (roomname?: string) => void,
        sendable?: boolean,
        roomnameError?: string,
        loading?: boolean,
        onSubmit?: (event: GestureResponderEvent) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomNewInputTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        url: string,
        roomname?: string,
        onPressPicker?: () => void,
        onChangeRoomname?: (roomname?: string) => void,
        sendable?: boolean,
        roomnameError?: string,
        loading?: boolean,
        onSubmit?: (event: GestureResponderEvent) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomNewInputTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomNewInputTemplatePresenter;