import React from 'react';
import RoomNewInputTemplateStyles from './styles';
import type RoomNewInputTemplatePresenter from './presenter';

namespace RoomNewInputTemplateHooks {
    export const useStyles = (props: RoomNewInputTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new RoomNewInputTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomNewInputTemplatePresenter.Input) => {
        return {};
    }
}

export default RoomNewInputTemplateHooks;