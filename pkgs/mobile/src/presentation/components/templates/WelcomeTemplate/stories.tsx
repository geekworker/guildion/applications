import { storiesOf } from '@storybook/react-native';
import { boolean, number } from '@storybook/addon-knobs';
import React from 'react';
import WelcomeTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { AppThemes } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('WelcomeTemplate', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <WelcomeTemplate
            appTheme={AppThemes.Dark}
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            isAppeared={boolean('isAppeared', true)}
        />
    ))
