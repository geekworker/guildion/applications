import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { View, Image, Text } from 'react-native';
import WelcomeBalloonBackground from '../../molecules/WelcomeBalloonBackground';
import WelcomeTemplateStyles from './styles';
import Button from '@/presentation/components/atoms/Button';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { Style } from '@/shared/interfaces/Style';

type Props = {
    onClickRegister?: () => void,
    onClickLogin?: () => void,
    isAppeared?: boolean,
} & StyleProps;

const WelcomeTemplate: React.FC<Props> = ({ style, appTheme, onClickLogin, onClickRegister, isAppeared }) => {
    const styles = React.useMemo(() => new WelcomeTemplateStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.wrapper} >
            <View style={styles.backgrond} >
                <WelcomeBalloonBackground
                    style={new Style({
                        width: style?.width,
                        height: style?.height,
                    })}
                    appTheme={appTheme}
                    isAppeared={isAppeared}
                />
            </View>
            <View style={styles.foregrond} >
                <View style={styles.foregrondContainer} >
                    <Image style={styles.icon} source={require('@/assets/images/brands/logo.png')} />
                    <Text style={styles.title}>
                        {localizer.dictionary.welcome.title}
                    </Text>
                    <Text style={styles.description}>
                        {localizer.dictionary.welcome.description}
                    </Text>
                    <Button
                        title={localizer.dictionary.welcome.register}
                        style={styles.getStyle('registerButton')}
                        appTheme={appTheme}
                        styleType={'fill'}
                        onPress={onClickRegister}
                        raised
                    />
                    <Button
                        title={localizer.dictionary.welcome.login}
                        style={styles.getStyle('loginButton')}
                        appTheme={appTheme}
                        styleType={'border'}
                        onPress={onClickLogin}
                        raised
                    />
                </View>
            </View>
        </View>
    )
};

export default React.memo(WelcomeTemplate, compare);