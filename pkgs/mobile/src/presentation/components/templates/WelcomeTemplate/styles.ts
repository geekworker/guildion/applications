import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.root,
        },
        backgrond: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.common.transparent,
        },
        foregrond: {
            width: style?.width,
            height: style?.height,
            position: 'absolute',
            top: 0,
            left: 0,
            backgroundColor: appTheme.common.transparent,
        },
        foregrondContainer: {
            width: style?.width,
            height: 370,
            position: 'relative',
            backgroundColor: appTheme.common.transparent,
            marginTop: Number(style?.height ?? 0) - 370,
        },
        icon: {
            width: 84,
            height: 84,
            marginLeft: (Number(style?.width ?? 0) - 84) / 2,
            marginRight: (Number(style?.width ?? 0) - 84) / 2,
            marginBottom: 32,
        },
        title: {
            color: 'white',
            fontFamily: MontserratFont.Bold,
            fontSize: 30,
            textAlign: 'center',
            marginBottom: 16,
        },
        description: {
            color: 'white',
            fontFamily: MontserratFont.SemiBold,
            fontSize: 14,
            textAlign: 'center',
            marginBottom: 40,
        },
        registerButtonContainerStyle: {
            backgroundColor: appTheme.button.default,
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 12,
            borderRadius: 25,
            height: 50,
        },
        registerButton: {
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 12,
            borderRadius: 25,
            height: 50,
        },
        loginButtonContainerStyle: {
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 12,
            borderRadius: 25,
            height: 50,
        },
        loginButton: {
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 12,
            borderRadius: 25,
            height: 50,
        },
    });
};

type Styles = typeof styles;

export default class WelcomeTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};