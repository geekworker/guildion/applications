import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.root,
            display: "flex",
            alignItems: 'center',
            justifyContent: 'center',
        },
        logo: {
            width: 90,
            height: 90,
            marginBottom: 30,
        },
        headline: {
            fontFamily: MontserratFont.Bold,
            fontSize: 18,
            textAlign: 'center',
            color: appTheme.element.default,
            marginBottom: 34,
        },
        indicator: {
            width: 120,
            height: 84,
            backgroundColor: appTheme.common.transparent,
            marginBottom: 100,
        },
    })
};

type Styles = typeof styles;

export default class ScreenLoadingTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};