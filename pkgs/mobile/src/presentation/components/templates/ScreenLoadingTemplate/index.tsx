import { localizer } from '@/shared/constants/Localizer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import React from 'react';
import { Image, Text, View } from 'react-native';
import LinerLoadingIndicator from '../../atoms/LinesLoadingIndicator';
import ScreenLoadingTemplateStyles from './styles';

type Props = {
} & StyleProps;

const ScreenLoadingTemplate: React.FC<Props> = ({ style, appTheme }) => {
    const styles = React.useMemo(() => new ScreenLoadingTemplateStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.wrapper} >
            <Image
                source={require("@/assets/images/brands/logo.png")}
                style={styles.logo}
            />
            <Text style={styles.headline} >
                {localizer.dictionary.account.loading.headline}
            </Text>
            <View style={styles.indicator}>
                <LinerLoadingIndicator appTheme={appTheme} />
            </View>
        </View>
    )
};

export default React.memo(ScreenLoadingTemplate, compare);