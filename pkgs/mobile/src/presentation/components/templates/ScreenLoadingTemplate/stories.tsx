import { storiesOf } from '@storybook/react-native';
import { number } from '@storybook/addon-knobs';
import React from 'react';
import ScreenLoadingTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { AppThemes } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('ScreenLoadingTemplate', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <ScreenLoadingTemplate
            appTheme={AppThemes.Dark}
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
        />
    ))
