import React from "react";
import GuildShowTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace GuildShowTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new GuildShowTemplateStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default GuildShowTemplateHooks