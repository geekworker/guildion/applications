import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            width: style?.width,
            height: style?.height,
            backgroundColor: appTheme.background.subm1,
        },
        scrollview: {
            width: style?.width,
            height: style?.height,
        },
        header: {
            width: style?.width,
        },
        headerInner: {
            width: style?.width,
        },
        guildHeader: {
            width: style?.width,
            height: 72,
        },
        guildMenu: {
            width: style?.width,
            height: 64,
        },
        noRoomContainer: {
            width: style?.width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        noRoomInner: {
            marginTop: 32,
        },
        noRoomTitle: {
            fontSize: 28,
            fontFamily: MontserratFont.Bold,
            textAlign: 'left',
            marginBottom: 8,
            color: appTheme.element.default,
        },
        noRoomDescription: {
            fontSize: 20,
            fontFamily: MontserratFont.Regular,
            textAlign: 'left',
            marginTop: 8,
            marginBottom: 8,
            color: appTheme.element.subp1,
        },
        noRoomButton: {
            width: 168,
            height: 44,
            marginTop: 8,
            borderRadius: 22,
            fontSize: 16,
            backgroundColor: appTheme.background.subp2,
        },
        newRoomButton: {
            position: 'absolute',
            bottom: 20,
            right: 20,
            color: appTheme.element.link,
            width: 168,
            height: 50,
            borderRadius: 25,
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
            backgroundColor: appTheme.background.subp2,
        },
        newRoomIcon: {
            color: appTheme.element.link,
            width: 26,
            height: 26,
        },
        bottomMargin: {
            backgroundColor: appTheme.common.transparent,
            marginBottom: Number(style?.height ?? 0),
            width: style?.width,
        },
        dashboard: {
            width: Number(style?.width ?? 0) - 20,
            height: 44,
            marginTop: 12,
            marginBottom: 12,
            marginLeft: 10,
            marginRight: 10,
        },
    })
};

type Styles = typeof styles;

export default class GuildShowTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};