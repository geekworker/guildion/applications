import { storiesOf } from '@storybook/react-native';
import { number, text } from '@storybook/addon-knobs';
import React from 'react';
import GuildShowTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Guild, Member, Members, Message, Room, RoomActivity, RoomLog, Rooms } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('GuildShowTemplate', module)
    .addDecorator((getStory) => 
        <StoreProvider>
            <CenterView>
                {getStory()}
            </CenterView>
        </StoreProvider>
    )
    .add('Default', () => (
        <GuildShowTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            rooms={new Rooms([
                new Room({
                    id: 'roomDisplayName1',
                    displayName: text('roomDisplayName1', 'Test Room1'),
                    activity: new RoomActivity({
                        log: new RoomLog({
                            message: new Message({
                                body: text('message', "Let talk about this video !!")
                            }),
                        }).toJSON(),
                        connectsCount: number('roomConnectsCount1', 12),
                        notificationsCount: number('roomNotificationsCount1', 1),
                        connectingMembers: new Members([
                            new Member({
                                id: 'test1',
                                displayName: 'test1',
                            }),
                            new Member({
                                id: 'test2',
                                displayName: 'test2',
                            }),
                            new Member({
                                id: 'test3',
                                displayName: 'test3',
                            }),
                            new Member({
                                id: 'test4',
                                displayName: 'test4',
                            }),
                        ]).toJSON(),
                        createdAt: new Date().toString(),
                    }).toJSON(),
                }),
                new Room({
                    id: 'roomDisplayName2',
                    displayName: text('roomDisplayName2', 'Test Room2'),
                    activity: new RoomActivity({
                        log: new RoomLog({
                            message: new Message({
                                body: text('message', "Let talk about this video !!")
                            }),
                        }).toJSON(),
                        connectsCount: number('roomConnectsCount2', 2),
                        notificationsCount: number('roomNotificationsCount2', 12),
                        connectingMembers: new Members([
                            new Member({
                                id: 'test1',
                                displayName: 'test1',
                            }),
                            new Member({
                                id: 'test2',
                                displayName: 'test2',
                            }),
                        ]).toJSON(),
                        createdAt: new Date().toString(),
                    }).toJSON(),
                }),
            ])}
            guild={new Guild({ displayName: text('guildDisplayName', 'test guild'), id: text('guildId',  '@test_guild') })}
        />
    ))
    .add('Loading', () => (
        <GuildShowTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            rooms={new Rooms([])}
            guild={new Guild({ displayName: text('guildDisplayName', 'test guild'), id: text('guildId',  '@test_guild') })}
            loading={true}
        />
    ))
