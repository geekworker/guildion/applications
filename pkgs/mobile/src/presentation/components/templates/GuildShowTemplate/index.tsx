import React from 'react';
import GuildShowTemplateComponent from './component';
import GuildShowTemplatePresenter from './presenter';

const GuildShowTemplate = (props: GuildShowTemplatePresenter.Input) => {
    const output = GuildShowTemplatePresenter.usePresenter(props);
    return <GuildShowTemplateComponent {...output} />;
};

export default React.memo(GuildShowTemplate, GuildShowTemplatePresenter.inputAreEqual);