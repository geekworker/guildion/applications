import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import GuildShowTemplateHooks from "./hooks";
import GuildShowTemplateStyles from './styles';
import React from 'react';
import { Guild, Room, Rooms } from '@guildion/core';
import { Animated, Insets, NativeScrollEvent, NativeSyntheticEvent } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";

namespace GuildShowTemplatePresenter {
    export type Input = {
        children?: React.ReactNode,
        rooms: Rooms,
        guild: Guild,
        roomsFetchLoading?: boolean,
        scrollInsets?: Insets,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        onPressRoom?: (room: Room) => void,
        onPressDashboard?: (guild: Guild) => void,
        translation?: Animated.Value | Animated.AnimatedInterpolation,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildShowTemplateStyles,
        appTheme: AppTheme,
        rooms: Rooms,
        guild: Guild,
        roomsFetchLoading?: boolean,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        onPressRoom?: (room: Room) => void,
        onPressDashboard?: (guild: Guild) => void,
        scrollInsets?: Insets,
        translation?: Animated.Value | Animated.AnimatedInterpolation,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildShowTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default GuildShowTemplatePresenter;