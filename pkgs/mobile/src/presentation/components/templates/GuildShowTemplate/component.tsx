import React from 'react';
import GuildShowTemplatePresenter from './presenter';
import { Text, View, Animated } from 'react-native';
import RoomsList from '../../organisms/RoomsList';
import GuildShowHeader from '../../molecules/GuildShowHeader';
import IconMenuList from '../../organisms/IconMenuList';
import Button, { ButtonStyleType } from '../../atoms/Button';
import { localizer } from '@/shared/constants/Localizer';
import RoundAnimatableButton from '../../atoms/RoundAnimatableButton';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MembersIcon from '../../Icons/MembersIcon';
import ButtonRow from '../../atoms/ButtonRow';

const GuildShowTemplateComponent: React.FC<GuildShowTemplatePresenter.Output> = ({ styles, appTheme, guild, rooms, onScroll, onPressRoom, roomsFetchLoading, scrollInsets, translation, loading, onPressDashboard }) => {
    const empty = !guild.activity?.publicRoomsCount || guild.activity?.publicRoomsCount > 0;
    const notRoomsView = (
        <View style={styles.noRoomContainer}>
            <View style={styles.noRoomInner}>
                <Text style={styles.noRoomTitle}>
                    {empty ? localizer.dictionary.room.home.empty.title : localizer.dictionary.room.home.notJoin.title}
                </Text>
                <Text style={styles.noRoomDescription}>
                    {empty ? localizer.dictionary.room.home.empty.description : localizer.dictionary.room.home.notJoin.description}
                </Text>
                {!empty && (
                    <Button
                        styleType={ButtonStyleType.Fill}
                        style={styles.getStyle('noRoomButton')}
                        title={localizer.dictionary.room.home.explore}
                    />
                )}
            </View>
        </View>
    );
    return (
        <Animated.View style={styles.wrapper}>
            <RoomsList
                rooms={rooms}
                style={styles.getStyle('scrollview')}
                ListHeaderComponentStyle={styles.header}
                ListHeaderComponent={(
                    <View style={styles.headerInner}>
                        <GuildShowHeader loading={loading} guild={guild} style={styles.getStyle('guildHeader')}/>
                        <IconMenuList loading={loading} data={[{
                            title: localizer.dictionary.guild.attr.members,
                            Icon: (props) => <MembersIcon width={props.size} height={props.size} color={props.color} />,
                            countText: `${guild.activity?.membersCount ?? 0}`,
                        }]} style={styles.getStyle('guildMenu')}/>
                        {!loading && true && (
                            <ButtonRow
                                style={styles.getStyle('dashboard')}
                                text={localizer.dictionary.guild.home.dashboard}
                                onPress={() => onPressDashboard && onPressDashboard(guild)}
                            />
                        )}
                        {rooms.size == 0 && !roomsFetchLoading && !loading && notRoomsView}
                    </View>
                )}
                ListFooterComponent={(
                    <View style={styles.bottomMargin}/>
                )}
                onScroll={onScroll}
                scrollInsets={scrollInsets}
                onPressCell={onPressRoom}
                loading={loading || roomsFetchLoading}
            />
            {!loading && (
                <RoundAnimatableButton
                    style={styles.getStyle('newRoomButton')}
                    appTheme={appTheme}
                    title={localizer.dictionary.room.home.create}
                    icon={<EntypoIcon name={'plus'} size={styles.newRoomIcon.width} color={styles.newRoomIcon.color} />}
                    translation={translation}
                />
            )}
        </Animated.View>
    );
};

export default React.memo(GuildShowTemplateComponent, GuildShowTemplatePresenter.outputAreEqual);