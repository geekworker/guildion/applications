import React from 'react';
import { Animated, View } from 'react-native';
import NotificationsTemplatePresenter from './presenter';

const NotificationsTemplateComponent = ({ styles, appTheme, animatedStyle,  }: NotificationsTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            
        </Animated.View>
    );
};

export default React.memo(NotificationsTemplateComponent, NotificationsTemplatePresenter.outputAreEqual);