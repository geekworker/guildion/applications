import React from 'react';
import NotificationsTemplateComponent from './component';
import NotificationsTemplatePresenter from './presenter';

const NotificationsTemplate = (props: NotificationsTemplatePresenter.Input) => {
    const output = NotificationsTemplatePresenter.usePresenter(props);
    return <NotificationsTemplateComponent {...output} />;
};

export default React.memo(NotificationsTemplate, NotificationsTemplatePresenter.inputAreEqual);