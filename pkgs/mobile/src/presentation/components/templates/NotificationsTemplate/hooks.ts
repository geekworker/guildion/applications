import React from 'react';
import NotificationsTemplateStyles from './styles';
import type NotificationsTemplatePresenter from './presenter';

namespace NotificationsTemplateHooks {
    export const useStyles = (props: NotificationsTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new NotificationsTemplateStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: NotificationsTemplatePresenter.Input) => {
        return {};
    }
}

export default NotificationsTemplateHooks;