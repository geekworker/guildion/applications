import { localizer } from '@/shared/constants/Localizer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import React from 'react';
import { View, Text, GestureResponderEvent } from 'react-native';
import Button from '../../atoms/Button';
import TouchableText from '../../atoms/TouchableText';
import AccountVerifyTemplateStyles from './styles';

type Props = {
    loading?: boolean,
    sendable?: boolean,
    usernameError?: string,
    onSubmit?: (event: GestureResponderEvent) => void,
} & StyleProps;

const AccountVerifyTemplate: React.FC<Props> = ({ style, appTheme, loading, sendable, onSubmit, usernameError }) => {
    const styles = React.useMemo(() => new AccountVerifyTemplateStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.wrapper} >
            <Text style={styles.title}>
                {localizer.dictionary.account.verify.headline}
            </Text>
            <Text style={styles.description}>
                {localizer.dictionary.account.verify.description}
            </Text>
            <TouchableText textStyle={styles.getStyle('resendLink')}>
                {localizer.dictionary.account.verify.resend}
            </TouchableText>
            {!!usernameError && usernameError.length > 0 && (
                <Text style={styles.error}>
                    {usernameError}
                </Text>
            )}
            <Button
                styleType={'fill'}
                style={styles.getStyle('submit')}
                loading={loading}
                disabled={!sendable}
                onPress={onSubmit}
                title={localizer.dictionary.account.verify.submit}
            />
        </View>
    )
};

export default React.memo(AccountVerifyTemplate, compare);