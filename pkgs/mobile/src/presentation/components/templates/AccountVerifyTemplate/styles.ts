import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.background.root,
            paddingTop: 30,
        },
        title: {
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 6,
            fontSize: 18,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'left',
        },
        description: {
            fontSize: 14,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            textAlign: 'left',
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 15,
        },
        resendLink: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            color: appTheme.element.link,
            textAlign: "left",
            textDecorationLine: 'underline',
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
        },
        error: {
            marginTop: 20,
            fontSize: 12,
            color: appTheme.element.error,
            textAlign: 'left',
            width: Number(style?.width ?? 0) - 40,
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 15,
        },
        submit: {
            width: 140,
            height: 50,
            marginTop: 30,
            marginLeft: (Number(style?.width ?? 0) - 140) / 2,
            marginRight: (Number(style?.width ?? 0) - 140) / 2,
        },
    })
};

type Styles = typeof styles;

export default class AccountVerifyTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};