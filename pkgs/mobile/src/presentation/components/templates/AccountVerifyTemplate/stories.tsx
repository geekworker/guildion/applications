import { storiesOf } from '@storybook/react-native';
import { boolean, number } from '@storybook/addon-knobs';
import React from 'react';
import AccountVerifyTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('AccountVerifyTemplate', module)
    .addDecorator((getStory) => 
        <CenterView>
            {getStory()}
        </CenterView>
    )
    .add('Default', () => (
        <AccountVerifyTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 800),
            })}
            loading={boolean('loading', false)}
            sendable={boolean('sendable', false)}
        />
    ))
