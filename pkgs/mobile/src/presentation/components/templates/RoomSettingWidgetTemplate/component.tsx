import React from 'react';
import { Animated, View } from 'react-native';
import RoomSettingWidgetTemplatePresenter from './presenter';

const RoomSettingWidgetTemplateComponent = ({ styles, appTheme, animatedStyle,  }: RoomSettingWidgetTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            
        </Animated.View>
    );
};

export default React.memo(RoomSettingWidgetTemplateComponent, RoomSettingWidgetTemplatePresenter.outputAreEqual);