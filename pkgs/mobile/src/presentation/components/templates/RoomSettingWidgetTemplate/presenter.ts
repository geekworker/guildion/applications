import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomSettingWidgetTemplateHooks from './hooks';
import RoomSettingWidgetTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

namespace RoomSettingWidgetTemplatePresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomSettingWidgetTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
    } & Omit<Input, 'style' | 'appTheme'>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomSettingWidgetTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomSettingWidgetTemplatePresenter;