import React from 'react';
import RoomSettingWidgetTemplateComponent from './component';
import RoomSettingWidgetTemplatePresenter from './presenter';

const RoomSettingWidgetTemplate = (props: RoomSettingWidgetTemplatePresenter.Input) => {
    const output = RoomSettingWidgetTemplatePresenter.usePresenter(props);
    return <RoomSettingWidgetTemplateComponent {...output} />;
};

export default React.memo(RoomSettingWidgetTemplate, RoomSettingWidgetTemplatePresenter.inputAreEqual);