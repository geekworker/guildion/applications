import React from 'react';
import RoomSettingWidgetTemplateStyles from './styles';
import type RoomSettingWidgetTemplatePresenter from './presenter';

namespace RoomSettingWidgetTemplateHooks {
    export const useStyles = (props: RoomSettingWidgetTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new RoomSettingWidgetTemplateStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomSettingWidgetTemplatePresenter.Input) => {
        return {};
    }
}

export default RoomSettingWidgetTemplateHooks;