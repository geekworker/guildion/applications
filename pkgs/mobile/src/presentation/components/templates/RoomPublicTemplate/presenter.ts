import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomPublicTemplateHooks from './hooks';
import RoomPublicTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { File, Member, Post, Posts, Room, SocialLink } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

namespace RoomPublicTemplatePresenter {
    export type Input = {
        room: Room,
        posts: Posts,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomPublicTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        room: Room,
        posts: Posts,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomPublicTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomPublicTemplatePresenter;