import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomPublicTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { mockPosts } from '../../organisms/PostsList/stories';
import { number } from '@storybook/addon-knobs';
import { Posts, Room, RoomActivity, SocialLink, SocialLinks, Files } from '@guildion/core';

storiesOf('RoomPublicTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomPublicTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            posts={mockPosts}
            room={new Room(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateRoomProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new RoomActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))
    .add('Loading', () => (
        <RoomPublicTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            loading
            posts={new Posts([])}
            room={new Room(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateRoomProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new RoomActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))