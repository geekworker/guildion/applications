import React from 'react';
import RoomPublicTemplateStyles from './styles';
import type RoomPublicTemplatePresenter from './presenter';

namespace RoomPublicTemplateHooks {
    export const useStyles = (props: RoomPublicTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new RoomPublicTemplateStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomPublicTemplatePresenter.Input) => {
        return {};
    }
}

export default RoomPublicTemplateHooks;