import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            position: 'relative',
            width,
            height,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        profile: {
            width,
            paddingTop: 20,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        postsSection: {
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        postsSectionTitle: {
            paddingLeft: style?.paddingLeft ?? 20,
            paddingRight: style?.paddingRight ?? 20,
            fontFamily: MontserratFont.Bold,
            fontSize: 24,
            marginBottom: 12,
            textAlign: 'left',
            color: appTheme.element.default,
        },
        scrollview: {
            width,
            height,
        },
        bottomMargin: {
            width,
            height: 230,
        },
    });
};

type Styles = typeof styles;

export default class RoomPublicTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};