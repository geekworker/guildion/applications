import React from 'react';
import RoomPublicTemplateComponent from './component';
import RoomPublicTemplatePresenter from './presenter';

const RoomPublicTemplate = (props: RoomPublicTemplatePresenter.Input) => {
    const output = RoomPublicTemplatePresenter.usePresenter(props);
    return <RoomPublicTemplateComponent {...output} />;
};

export default React.memo(RoomPublicTemplate, RoomPublicTemplatePresenter.inputAreEqual);