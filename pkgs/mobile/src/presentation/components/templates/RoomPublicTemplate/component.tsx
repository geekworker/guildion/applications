import { SortDirection } from '@guildion/core';
import React from 'react';
import { Animated, View, Text } from 'react-native';
import RoomPublicProfile from '../../molecules/RoomPublicProfile';
import PostsList from '../../organisms/PostsList';
import RoomPublicTemplatePresenter from './presenter';
import { localizer } from '@/shared/constants/Localizer';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const RoomPublicTemplateComponent = ({ styles, appTheme, animatedStyle, room, loading, loadingMore, posts, onPressSocialLink, onPressComment, onPressReaction, onPressSender, onPressSeeMore, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom }: RoomPublicTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <PostsList
                    posts={posts}
                    style={styles.getStyle('scrollview')}
                    ListHeaderComponent={(
                        <>
                            <RoomPublicProfile
                                style={styles.getStyle('profile')}
                                room={room}
                                loading={loading}
                                onPressSocialLink={onPressSocialLink}
                            />
                            {!loading && (
                                <View style={styles.postsSection}>
                                    <Text style={styles.postsSectionTitle}>
                                        {localizer.dictionary.room.posts.title}
                                    </Text>
                                </View>
                            )}
                        </>
                    )}
                    ListFooterComponent={(
                        <View style={styles.bottomMargin}/>
                    )}
                    // FIXME: should add loading story for PostsList
                    loadingMore={loading ? LoadingMoreStatus.INCREMENT : loadingMore}
                    onPressSender={onPressSender}
                    onPressSeeMore={onPressSeeMore}
                    onPressReaction={onPressReaction}
                    onPressComment={onPressComment}
                    onPressFile={onPressFile}
                    onPressURL={onPressURL}
                    onPressEmail={onPressEmail}
                    onPressPhoneNumber={onPressPhoneNumber}
                    onPressMentionMember={onPressMentionMember}
                    onPressMentionRoom={onPressMentionRoom}
                    sortDirection={SortDirection.ASC}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(RoomPublicTemplateComponent, RoomPublicTemplatePresenter.outputAreEqual);