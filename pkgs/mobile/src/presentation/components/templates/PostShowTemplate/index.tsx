import React from 'react';
import PostShowTemplateComponent from './component';
import PostShowTemplatePresenter from './presenter';

const PostShowTemplate = (props: PostShowTemplatePresenter.Input) => {
    const output = PostShowTemplatePresenter.usePresenter(props);
    return <PostShowTemplateComponent {...output} />;
};

export default React.memo(PostShowTemplate, PostShowTemplatePresenter.inputAreEqual);