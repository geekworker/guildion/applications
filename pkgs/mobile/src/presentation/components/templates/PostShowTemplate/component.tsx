import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { SortDirection } from '@guildion/core';
import React from 'react';
import { View } from 'react-native';
import PostCommentsSection from '../../molecules/PostCommentsSection';
import PostNewCommentSection from '../../molecules/PostNewCommentSection';
import PostShowHeader from '../../molecules/PostShowHeader';
import PostShowItem from '../../molecules/PostShowItem';
import PostStatsBar from '../../molecules/PostStatsBar';
import PostsList from '../../organisms/PostsList';
import PostShowTemplatePresenter from './presenter';

const PostShowTemplateComponent = ({ styles, appTheme, posts, post, currentMember, onPressNewComment, onPressSender, onPressSeeMore, onPressReaction, onPressComment, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, onPressBack, loading, loadingMore, onScrollToBottom }: PostShowTemplatePresenter.Output) => {
    return (
        <View style={styles.container}>
            <PostShowHeader post={post} style={styles.getStyle('header')} onPressBack={onPressBack} onPressSender={onPressSender} />
            <PostsList
                posts={posts}
                style={styles.getStyle('list')}
                sortDirection={SortDirection.DESC}
                loadingMore={loading ? LoadingMoreStatus.INCREMENT : loadingMore}
                onScrollToBottom={onScrollToBottom}
                ListHeaderComponentStyle={styles.post}
                ListHeaderComponent={(
                    <View style={styles.postInner}>
                        <PostShowItem post={post} style={styles.getStyle('item')} />
                        <PostStatsBar post={post} style={styles.getStyle('bar')} />
                        <PostCommentsSection post={post} style={styles.getStyle('commentsSection')} />
                        {currentMember && (
                            <PostNewCommentSection currentMember={currentMember} style={styles.getStyle('commentNewSection')} />
                        )}
                    </View>
                )}
                ListFooterComponent={(
                    <View style={styles.footerMargin} />
                )}
            />
        </View>
    );
};

export default React.memo(PostShowTemplateComponent, PostShowTemplatePresenter.outputAreEqual);