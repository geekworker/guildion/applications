import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { BIG_TOP_HEIGHT } from "../RoomShowTemplate/styles";

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            position: 'relative',
        },
        header: {
            width: style?.width,
            height: 44,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
        },
        post: {
            width: style?.width,
        },
        postInner: {
            width: style?.width,
            backgroundColor: appTheme.background.subm1,
        },
        item: {
            width: style?.width,
            marginBottom: 12,
            backgroundColor: appTheme.background.subm1,
        },
        bar: {
            width: style?.width,
            backgroundColor: appTheme.background.subm1,
        },
        commentsSection: {
            width: style?.width,
            backgroundColor: appTheme.background.subm1,
        },
        commentNewSection: {
            width: style?.width,
            height: 64,
            backgroundColor: appTheme.background.subm1,
        },
        list: {
            width: style?.width,
        },
        footerMargin: {
            marginBottom: BIG_TOP_HEIGHT,
        },
    })
};

type Styles = typeof styles;

export default class PostShowTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};