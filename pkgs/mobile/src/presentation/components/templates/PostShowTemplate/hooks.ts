import React from "react";
import PostShowTemplateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace PostShowTemplateHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new PostShowTemplateStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default PostShowTemplateHooks;