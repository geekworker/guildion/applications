import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import PostShowTemplateHooks from "./hooks";
import PostShowTemplateStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Post, Posts } from "@guildion/core";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";

namespace PostShowTemplatePresenter {
    export type Input = {
        posts: Posts,
        post: Post,
        currentMember?: Member,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        onPressBack?: () => void,
        onScrollToBottom?: () => void,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressNewComment?: () => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PostShowTemplateStyles,
        appTheme: AppTheme,
        posts: Posts,
        post: Post,
        currentMember?: Member,
        loadingMore?: LoadingMoreStatus,
        loading?: boolean,
        onPressBack?: () => void,
        onScrollToBottom?: () => void,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressNewComment?: () => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PostShowTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default PostShowTemplatePresenter;