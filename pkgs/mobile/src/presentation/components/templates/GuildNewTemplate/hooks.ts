import React from 'react';
import GuildNewTemplateStyles from './styles';
import type GuildNewTemplatePresenter from './presenter';

namespace GuildNewTemplateHooks {
    export const useStyles = (props: GuildNewTemplatePresenter.Input) => {
        const styles = React.useMemo(() => new GuildNewTemplateStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: GuildNewTemplatePresenter.Input) => {
        return {};
    }
}

export default GuildNewTemplateHooks;