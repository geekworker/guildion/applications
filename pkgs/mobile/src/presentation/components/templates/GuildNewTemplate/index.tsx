import React from 'react';
import GuildNewTemplateComponent from './component';
import GuildNewTemplatePresenter from './presenter';

const GuildNewTemplate = (props: GuildNewTemplatePresenter.Input) => {
    const output = GuildNewTemplatePresenter.usePresenter(props);
    return <GuildNewTemplateComponent {...output} />;
};

export default React.memo(GuildNewTemplate, GuildNewTemplatePresenter.inputAreEqual);