import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const videoHeight = height - 300;
    const videoWidth = videoHeight / 2 + 40; 
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            width,
            height,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
            alignContent: 'center',
        },
        video: {
            width: videoWidth,
            height: videoHeight,
            marginLeft: (width - videoWidth) / 2,
            marginRight: (width - videoWidth) / 2,
        },
        footer: {
            width,
            marginBottom: 54,
            marginTop: 20,
            paddingLeft: 20,
            paddingRight: 20,
        },
        title: {
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            textAlign: 'center',
            fontSize: 24,
            marginBottom: 8,
        },
        description: {
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            textAlign: 'center',
            fontSize: 14,
            lineHeight: 24,
            marginBottom: 20,
        },
        next: {
            width: 140,
            height: 50,
            marginLeft: (width - 140 - 40) / 2,
            marginRight: (width - 140 - 40) / 2,
        },
    })
};

type Styles = typeof styles;

export default class GuildNewTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};