import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildNewTemplateHooks from './hooks';
import GuildNewTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

namespace GuildNewTemplatePresenter {
    export type Input = {
        onPressNext?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildNewTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        onPressNext?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildNewTemplateHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default GuildNewTemplatePresenter;