import { localizer } from '@/shared/constants/Localizer';
import React from 'react';
import { Animated, Text, View } from 'react-native';
import AutoPlayVideo from '../../atoms/AutoPlayVideo';
import Button from '../../atoms/Button';
import GuildNewTemplatePresenter from './presenter';

const GuildNewTemplateComponent = ({ styles, appTheme, animatedStyle, onPressNext }: GuildNewTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <AutoPlayVideo
                    source={require('@/assets/videos/group-watching@mobile.mp4')}
                    style={styles.getStyle('video')}
                    resizeMode='cover'
                />
                <View style={styles.footer}>
                    <Text style={styles.title}>
                        {localizer.dictionary.g.db.guild}
                    </Text>
                    <Text style={styles.description}>
                        {localizer.dictionary.guild.new.welcome.description}
                    </Text>
                    <Button
                        styleType={'fill'}
                        style={styles.getStyle('next')}
                        onPress={onPressNext}
                        title={localizer.dictionary.g.start}
                    />
                </View>
            </View>
        </Animated.View>
    );
};

export default React.memo(GuildNewTemplateComponent, GuildNewTemplatePresenter.outputAreEqual);