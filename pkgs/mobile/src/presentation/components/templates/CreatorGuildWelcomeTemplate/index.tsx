import React from 'react';
import CreatorGuildWelcomeTemplateStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { View } from 'react-native';

type Props = {
} & Partial<StyleProps>;

const CreatorGuildWelcomeTemplate: React.FC<Props> = ({
    style,
    appTheme,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new CreatorGuildWelcomeTemplateStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
        </View>
    );
};

export default React.memo(CreatorGuildWelcomeTemplate, compare);