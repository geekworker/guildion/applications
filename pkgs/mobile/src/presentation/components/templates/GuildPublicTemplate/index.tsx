import React from 'react';
import GuildPublicTemplateComponent from './component';
import GuildPublicTemplatePresenter from './presenter';

const GuildPublicTemplate = (props: GuildPublicTemplatePresenter.Input) => {
    const output = GuildPublicTemplatePresenter.usePresenter(props);
    return <GuildPublicTemplateComponent {...output} />;
};

export default React.memo(GuildPublicTemplate, GuildPublicTemplatePresenter.inputAreEqual);