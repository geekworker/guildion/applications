import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import GuildPublicTemplateHooks from './hooks';
import GuildPublicTemplateStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { Guild, Room, RoomCategories, SocialLink } from '@guildion/core';
import { Animated, NativeScrollEvent, NativeSyntheticEvent } from 'react-native';

namespace GuildPublicTemplatePresenter {
    export type Input = {
        loading?: boolean,
        loadingMoreStatus?: LoadingMoreStatus,
        guild: Guild,
        categories: RoomCategories,
        onPressRoom?: (room: Room) => void,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        onPressClose?: (guild: Guild) => void,
        onPressEdit?: (guild: Guild) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildPublicTemplateStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        loading?: boolean,
        loadingMoreStatus?: LoadingMoreStatus,
        guild: Guild,
        categories: RoomCategories,
        onPressRoom?: (room: Room) => void,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        scrollY: Animated.Value | Animated.AnimatedInterpolation,
        transition: Animated.Value | Animated.AnimatedInterpolation,
        onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        onPressClose?: (guild: Guild) => void,
        onPressEdit?: (guild: Guild) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = GuildPublicTemplateHooks.useStyles({ ...props });
        const {
            onScroll,
            scrollY,
            transition,
        } = GuildPublicTemplateHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            scrollY,
            onScroll,
            transition,
        }
    }
}

export default GuildPublicTemplatePresenter;