import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { EdgeInsetsZero } from '@/shared/constants/EdgeInsets';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';
import { EdgeInsets } from 'react-native-safe-area-context';

type Props = {
    insets: EdgeInsets,
} & StyleProps;

export const HEADER_HEIGHT: number = 44;

const styles = ({ appTheme, style, insets }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        inner: {
            position: 'relative',
            width,
            height,
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        filterTop: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            height: HEADER_HEIGHT + 12,
        },
        header: {
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            paddingTop: insets.top,
            height: HEADER_HEIGHT,
            width: style?.width,
            backgroundColor: appTheme.background.subm1,
        },
        profile: {
            width,
            height: 430,
        },
        scrollview: {
            width,
            height,
        },
        bottomMargin: {
            width,
            height: 830,
        },
    });
};

type Styles = typeof styles;

export default class GuildPublicTemplateStyles extends Record<ReturnType<Styles>>({
    ...styles({ insets: EdgeInsetsZero })
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};