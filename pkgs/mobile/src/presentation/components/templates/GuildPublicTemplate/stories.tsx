import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildPublicTemplate from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Files, Guild, GuildActivity, Room, RoomActivity, RoomCategories, RoomCategory, Rooms, SocialLink, SocialLinks } from '@guildion/core';

storiesOf('GuildPublicTemplate', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildPublicTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            categories={mockCategories}
            guild={new Guild(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new GuildActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
        />
    ))
    .add('Loading', () => (
        <GuildPublicTemplate
            style={new Style({
                width: number('width', 385),
                height: number('height', 720),
            })}
            categories={mockCategories}
            guild={new Guild(
                {
                    displayName: 'test',
                },
                {
                    profile: Files.getSeed().generateGuildProfile(),
                    socialLinks: new SocialLinks([
                        new SocialLink({
                            url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                        }),
                        new SocialLink({
                            url: 'https://twitter.com/__I_OXO_I__',
                        }),
                        new SocialLink({
                            url: 'https://www.guildion.co',
                        }),
                    ]),
                    activity: new GuildActivity({
                        connectsCount: 12,
                        membersCount: 100,
                    }),
                }
            )}
            loading
        />
    ))


const mockCategories = new RoomCategories([
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
]);