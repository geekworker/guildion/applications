import React from 'react';
import { Animated, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import GuildPublicHeader from '../../molecules/GuildPublicHeader';
import GuildPublicProfile from '../../molecules/GuildPublicProfile';
import RoomCategoriesList from '../../organisms/RoomCategoriesList';
import GuildPublicTemplatePresenter from './presenter';

const AnimatedLinearGradient = Animated.createAnimatedComponent(LinearGradient);

const GuildPublicTemplateComponent = ({ styles, appTheme, animatedStyle, loading, loadingMoreStatus, categories, guild, onPressRoom, onPressSocialLink, onScroll, transition, onPressClose, onPressEdit }: GuildPublicTemplatePresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <View style={styles.inner}>
                <RoomCategoriesList
                    categories={categories}
                    style={styles.getStyle('scrollview')}
                    ListHeaderComponent={(
                        <GuildPublicProfile
                            style={styles.getStyle('profile')}
                            guild={guild}
                            loading={loading}
                            onPressSocialLink={onPressSocialLink}
                        />
                    )}
                    ListFooterComponent={(
                        <View style={styles.bottomMargin}/>
                    )}
                    onPressRoom={onPressRoom}
                    loading={loading}
                    onScroll={onScroll}
                />
                <AnimatedLinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0, y: 1 }}
                    colors={[
                        'rgba(0, 0, 0, 0.6)',
                        'rgba(0, 0, 0, 0)',
                    ]}
                    style={{
                        ...styles.filterTop,
                        opacity: transition.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, 0],
                        }),
                    }}
                />
                <GuildPublicHeader
                    style={styles.getStyle('header')}
                    guild={guild}
                    transition={transition}
                    onPressClose={onPressClose}
                    onPressEdit={onPressEdit}
                />
            </View>
        </Animated.View>
    );
};

export default React.memo(GuildPublicTemplateComponent, GuildPublicTemplatePresenter.outputAreEqual);