import React from 'react';
import GuildPublicTemplateStyles from './styles';
import type GuildPublicTemplatePresenter from './presenter';
import { Animated, NativeScrollEvent, NativeSyntheticEvent } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

namespace GuildPublicTemplateHooks {
    export const SCROLL_INTERVAL = 400;

    export const useStyles = (props: GuildPublicTemplatePresenter.Input) => {
        const insets = useSafeAreaInsets();
        const styles = React.useMemo(() => new GuildPublicTemplateStyles({
            appTheme: props.appTheme,
            style: props.style,
            insets,
        }), [
            props.appTheme,
            props.style,
            insets,
        ]);
        return styles;
    }

    export const useState = (props: GuildPublicTemplatePresenter.Input) => {
        const scrollY = React.useRef(new Animated.Value(0)).current;
        const onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event> = React.useCallback((event: NativeSyntheticEvent<NativeScrollEvent>) => {
            const contentOffsetY = event.nativeEvent.contentOffset.y;
            scrollY.setValue(contentOffsetY);
        }, []);
        const transition = scrollY.interpolate({
            inputRange: [0, SCROLL_INTERVAL],
            outputRange: [0, 1],
            extrapolateRight: 'clamp',
        });
        return {
            scrollY,
            onScroll,
            transition,
        };
    }
}

export default GuildPublicTemplateHooks;