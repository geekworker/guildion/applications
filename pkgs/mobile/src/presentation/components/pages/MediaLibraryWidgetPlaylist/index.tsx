import React from 'react';
import MediaLibraryWidgetPlaylistPresenter from './presenter';
import MediaLibraryWidgetPlaylistComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetPlaylistPresenter.MergedProps) => {
    const output = MediaLibraryWidgetPlaylistPresenter.usePresenter(props);
    return <MediaLibraryWidgetPlaylistComponent {...output} />;
};

const MediaLibraryWidgetPlaylist = connect<MediaLibraryWidgetPlaylistPresenter.StateProps, MediaLibraryWidgetPlaylistPresenter.DispatchProps, MediaLibraryWidgetPlaylistPresenter.Input, MediaLibraryWidgetPlaylistPresenter.MergedProps, RootState>(
    MediaLibraryWidgetPlaylistPresenter.mapStateToProps,
    MediaLibraryWidgetPlaylistPresenter.mapDispatchToProps,
    MediaLibraryWidgetPlaylistPresenter.mergeProps,
    MediaLibraryWidgetPlaylistPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetPlaylist;