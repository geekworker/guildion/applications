import React from 'react';
import { Animated, View } from 'react-native';
import MediaLibraryWidgetPlaylistPresenter from './presenter';

const MediaLibraryWidgetPlaylistComponent = ({ styles, appTheme, animatedStyle,  }: MediaLibraryWidgetPlaylistPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            
        </Animated.View>
    );
};

export default React.memo(MediaLibraryWidgetPlaylistComponent, MediaLibraryWidgetPlaylistPresenter.outputAreEqual);