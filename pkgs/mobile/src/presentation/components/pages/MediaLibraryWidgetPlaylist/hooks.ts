import React from 'react';
import MediaLibraryWidgetPlaylistStyles from './styles';
import type MediaLibraryWidgetPlaylistPresenter from './presenter';
import StyleProps from '@/shared/interfaces/StyleProps';

namespace MediaLibraryWidgetPlaylistHooks {
    export const useStyles = (props: MediaLibraryWidgetPlaylistPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetPlaylistStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: MediaLibraryWidgetPlaylistPresenter.MergedProps) => {
        return {};
    }
}

export default MediaLibraryWidgetPlaylistHooks;