import { useMemo, useEffect } from "react";
import AccountVerifyStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type AccountVerifyPresenter from "./presenter";

namespace AccountVerifyHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = useMemo(() => new AccountVerifyStyles({ ...props }), [props]);
        return styles;
    }

    export const useSuccess = (onSuccess: () => void, { pageState, didSuccess }: AccountVerifyPresenter.MergedProps) => {
        useEffect(() => {
            if (!!pageState.get('success')) {
                onSuccess();
                didSuccess();
            };
        },[pageState.get('success')])
    }
}

export default AccountVerifyHooks