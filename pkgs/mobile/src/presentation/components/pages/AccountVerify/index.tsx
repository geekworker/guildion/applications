import React from 'react';
import AccountVerifyPresenter from './presenter';
import AccountVerifyComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: AccountVerifyPresenter.MergedProps) => {
    const output = AccountVerifyPresenter.usePresenter(props);
    return <AccountVerifyComponent {...output} />;
};

const AccountVerify = connect<AccountVerifyPresenter.StateProps, AccountVerifyPresenter.DispatchProps, AccountVerifyPresenter.Input, AccountVerifyPresenter.MergedProps, RootState>(
    AccountVerifyPresenter.mapStateToProps,
    AccountVerifyPresenter.mapDispatchToProps,
    AccountVerifyPresenter.mergeProps,
    AccountVerifyPresenter.connectOptions,
)(Container);

export default AccountVerify;