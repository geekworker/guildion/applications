import React from 'react';
import { View } from 'react-native'
import AccountVerifyPresenter from './presenter';
import AccountVerifyTemplate from '../../templates/AccountVerifyTemplate';
import { Style } from '@/shared/interfaces/Style';

const AccountVerifyComponent = ({ styles, innerWidth, innerHeight, appTheme, pageState, onSubmit, usernameError }: AccountVerifyPresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <AccountVerifyTemplate
                style={new Style({
                    width: innerWidth,
                    height: innerHeight,
                })}
                appTheme={appTheme}
                usernameError={usernameError}
                loading={pageState.get('loading')}
                sendable={true}
                onSubmit={onSubmit}
            />
        </View>
    );
};

export default React.memo(AccountVerifyComponent, AccountVerifyPresenter.outputAreEqual);