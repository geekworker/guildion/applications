import { Action, AnyAction } from "redux";
import AccountVerifyHooks from "./hooks";
import AccountVerifyStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { WelcomeNavigatorRoutes, WelcomeNavigatorStackParams } from '@/presentation/components/navigators/WelcomeNavigator/constants';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { AccountAction, AccountVerifyPageState } from "@/presentation/redux/Account/AccountReducer";
import { AppTheme } from "@/shared/constants/AppTheme";
import { accountUsernameErrorSelector } from "@/presentation/redux/App/AppSelector";
import StyleProps from "@/shared/interfaces/StyleProps";
import { useInsets } from "@/shared/hooks/useInsets";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace AccountVerifyPresenter {
    export type StackParams = {
        username?: string,
    }

    export type StateProps = {
        pageState: AccountVerifyPageState,
        usernameError?: string,
    }
    
    export type DispatchProps = {
        didSuccess: () => Action,
        verify: (username: string) => Action,
    }
    
    export type Input = NativeStackScreenProps<WelcomeNavigatorStackParams, 'AccountVerify'> & StyleProps;
    
    export type Output = {
        styles: AccountVerifyStyles,
        appTheme?: AppTheme,
        innerWidth: number,
        innerHeight: number,
        usernameError?: string,
        onSubmit: () => void,
        pageState: AccountVerifyPageState,
    };

    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            pageState: state.account.pages.verify,
            usernameError: accountUsernameErrorSelector(state),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            didSuccess: () => AccountAction.setVerifyState({ success: false }),
            verify: (username: string) => AccountAction.verify.started({ username }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.pages.verify == next.account.pages.verify &&
            prev.app.errors == next.app.errors
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = AccountVerifyHooks.useStyles(props);
        const insets = useInsets({ width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        AccountVerifyHooks.useSuccess(() => props.navigation.push(WelcomeNavigatorRoutes.AccountLoading, {}), props);
        return {
            ...props,
            ...insets,
            styles,
            onSubmit: () => props.verify(props.pageState.get('username')!),
        }
    }
}

export default AccountVerifyPresenter;