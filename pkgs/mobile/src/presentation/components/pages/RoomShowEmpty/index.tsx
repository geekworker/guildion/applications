import React from 'react';
import RoomShowEmptyPresenter from './presenter';
import RoomShowEmptyComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomShowEmptyPresenter.MergedProps) => {
    const output = RoomShowEmptyPresenter.usePresenter(props);
    return (
        <RoomShowEmptyComponent { ...output } />
    );
};

const RoomShowEmpty = connect<RoomShowEmptyPresenter.StateProps, RoomShowEmptyPresenter.DispatchProps, RoomShowEmptyPresenter.Input, RoomShowEmptyPresenter.MergedProps, RootState>(
    RoomShowEmptyPresenter.mapStateToProps,
    RoomShowEmptyPresenter.mapDispatchToProps,
    RoomShowEmptyPresenter.mergeProps,
    RoomShowEmptyPresenter.connectOptions,
)(Container);

export default RoomShowEmpty;