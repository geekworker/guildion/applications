import RoomShowEmptyHooks from "./hooks";
import RoomShowEmptyStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { InsetsOutput, useInsets } from "@/shared/hooks/useInsets";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomSplitScreenNavigatorStackParams } from "../../navigators/RoomSplitScreenNavigator/constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomShowEmptyPresenter {
    export type StackParams = {
    }

    export type StateProps = {
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<RoomSplitScreenNavigatorStackParams, 'RoomShowEmpty'> & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomShowEmptyStyles,
        appTheme: AppTheme,
        insets: InsetsOutput,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = RoomShowEmptyHooks.useStyles(props);
        const insets = useInsets({ bottomExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        RoomShowEmptyHooks.useObserveNavigation(props.navigation);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            insets,
        }
    }
}

export default RoomShowEmptyPresenter;