import { Style } from '@/shared/interfaces/Style';
import React from 'react';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import RoomShowEmptyTemplate from '../../templates/RoomShowEmptyTemplate';
import RoomShowEmptyPresenter from './presenter';

const RoomShowEmptyComponent: React.FC<RoomShowEmptyPresenter.Output> = ({ appTheme, styles, insets }) => {
    return (
        <View style={styles.wrapper}>
            <SafeAreaView style={styles.safearea} edges={['top', 'right', 'left']}>
                <RoomShowEmptyTemplate
                    style={new Style({
                        width: insets.innerWidth,
                        height: insets.innerHeight,
                    })}
                    appTheme={appTheme}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(RoomShowEmptyComponent, RoomShowEmptyPresenter.outputAreEqual);