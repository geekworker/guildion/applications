import React from "react";
import RoomShowEmptyStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type RoomShowEmptyPresenter from "./presenter";
import RootRedux from "@/infrastructure/Root/redux";

namespace RoomShowEmptyHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomShowEmptyStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useObserveNavigation = (navigation: RoomShowEmptyPresenter.Input['navigation']) => {
        const context = React.useContext(RootRedux.Context);
        React.useEffect(() => {
            context.dispatch(RootRedux.Action.setCurrentRoomSplitSceneNavigation(navigation));
        },[navigation]);
    }
}

export default RoomShowEmptyHooks