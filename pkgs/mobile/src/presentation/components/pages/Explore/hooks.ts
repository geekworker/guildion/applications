import React from 'react';
import ExploreStyles from './styles';
import type ExplorePresenter from './presenter';
import { ExploreNavigatorRoutes } from '../../navigators/ExploreNavigator/constants';

namespace ExploreHooks {
    export const useStyles = (props: ExplorePresenter.MergedProps) => {
        const styles = React.useMemo(() => new ExploreStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: ExplorePresenter.MergedProps) => {
        const [query, setQuery] = React.useState('');
        const onChangeText = React.useCallback((value: string) => {
            setQuery(value);
        }, []);
        const onSubmitEditing = React.useCallback(() => {
            if (query) {
                props.navigation.push(ExploreNavigatorRoutes.SearchResults, { query: query });
                setQuery('');
            }
        }, [props.navigation, query]);
        return {
            onSubmitEditing,
            onChangeText,
            query,
        };
    }
}

export default ExploreHooks;