import ExploreHooks from './hooks';
import ExploreStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ExploreNavigatorStackParams } from '../../navigators/ExploreNavigator/constants';
import { useInsets } from '@/shared/hooks/useInsets';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { useSplitView } from '@/shared/hooks/useSplitView';
import { SearchInputProps } from '../../atoms/SearchInput';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

namespace ExplorePresenter {
    export type StackParams = {
    }

    export type StateProps = {
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<ExploreNavigatorStackParams, 'Explore'> & StyleProps;
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: ExploreStyles,
        appTheme: AppTheme,
        innerWidth: number,
        innerHeight: number,
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
        safeareaTop: number,
        query: string,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = ExploreHooks.useStyles(props);
        const insets = useInsets({ bottomExclude: true, topExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        const { top: safeareaTop } = useSafeAreaInsets();
        const tabbarHeight = useBottomTabBarHeight();
        const shouldSplit = useSplitView();
        const {
            onSubmitEditing,
            onChangeText,
            query,
        } = ExploreHooks.useState(props);
        return {
            ...props,
            ...insets,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            innerHeight: shouldSplit ? insets.innerHeight : insets.innerHeight - tabbarHeight,
            onSubmitEditing,
            onChangeText,
            safeareaTop,
            query,
        }
    }
}

export default ExplorePresenter;