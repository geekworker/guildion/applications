import React from 'react';
import ExplorePresenter from './presenter';
import ExploreComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: ExplorePresenter.MergedProps) => {
    const output = ExplorePresenter.usePresenter(props);
    return <ExploreComponent {...output} />;
};

const Explore = connect<ExplorePresenter.StateProps, ExplorePresenter.DispatchProps, ExplorePresenter.Input, ExplorePresenter.MergedProps, RootState>(
    ExplorePresenter.mapStateToProps,
    ExplorePresenter.mapDispatchToProps,
    ExplorePresenter.mergeProps,
    ExplorePresenter.connectOptions,
)(Container);

export default Explore;