import { Style } from '@/shared/interfaces/Style';
import React from 'react';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Discoveries } from '@guildion/core';
import ExploreTemplate from '../../templates/ExploreTemplate';
import ExplorePresenter from './presenter';

const ExploreComponent = ({ styles, appTheme, animatedStyle, innerHeight, innerWidth, onChangeText, onSubmitEditing, safeareaTop, query }: ExplorePresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <SafeAreaView style={styles.safearea} edges={['right', 'left']}>
                <ExploreTemplate
                    style={new Style({
                        paddingTop: safeareaTop,
                        width: innerWidth,
                        height: innerHeight,
                    })}
                    appTheme={appTheme}
                    discoveries={new Discoveries([])}
                    onChangeText={onChangeText}
                    onSubmitEditing={onSubmitEditing}
                    query={query}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(ExploreComponent, ExplorePresenter.outputAreEqual);