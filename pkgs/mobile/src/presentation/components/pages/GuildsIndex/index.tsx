import React from 'react';
import GuildsIndexPresenter from './presenter';
import GuildsIndexComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: GuildsIndexPresenter.MergedProps) => {
    const output = GuildsIndexPresenter.usePresenter(props);
    return <GuildsIndexComponent {...output} />;
};

const GuildsIndex = connect<GuildsIndexPresenter.StateProps, GuildsIndexPresenter.DispatchProps, GuildsIndexPresenter.Input, GuildsIndexPresenter.MergedProps, RootState>(
    GuildsIndexPresenter.mapStateToProps,
    GuildsIndexPresenter.mapDispatchToProps,
    GuildsIndexPresenter.mergeProps,
    GuildsIndexPresenter.connectOptions,
)(Container);

export default GuildsIndex;