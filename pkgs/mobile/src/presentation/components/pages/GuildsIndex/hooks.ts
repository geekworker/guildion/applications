import React from "react";
import GuildsStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type GuildsIndexPresenter from "./presenter";

namespace GuildsIndexHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new GuildsStyles({ ...props }), [props]);
        return styles;
    }

    export const useViewWillAppear = (props: GuildsIndexPresenter.MergedProps) => {
        React.useEffect(() => {
            if (props.navigation.isFocused()) {
                props.fetchGuilds();
            };
        }, [props.navigation.isFocused()]);
    }
}

export default GuildsIndexHooks