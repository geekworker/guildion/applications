import { Style } from '@/shared/interfaces/Style';
import React from 'react';
import { View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import GuildsTemplate from '../../templates/GuildsTemplate';
import GuildsIndexPresenter from './presenter';

const GuildsIndexComponent: React.FC<GuildsIndexPresenter.Output> = ({ styles, appTheme, innerHeight, innerWidth, pageState, onFocus, roomsPageState, onPressRoom }) => {
    return (
        <View style={styles.wrapper} >
            <SafeAreaView style={styles.safearea} edges={['top', 'right', 'left']}>
                <GuildsTemplate
                    guilds={pageState.get('guilds')}
                    style={new Style({
                        width: innerWidth,
                        height: innerHeight,
                    })}
                    appTheme={appTheme}
                    onFocus={onFocus}
                    roomsPageState={roomsPageState}
                    onPressRoom={onPressRoom}
                    loading={pageState.get('loading') ?? true}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(GuildsIndexComponent, GuildsIndexPresenter.outputAreEqual);