import GuildsIndexHooks from "./hooks";
import GuildsStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { GuildsNavigatorStackParams } from "@/presentation/components/navigators/GuildsNavigator/constants";
import { useInsets } from "@/shared/hooks/useInsets";
import { Guild, Room } from "@guildion/core";
import { GuildAction, GuildsPageState } from "@/presentation/redux/Guild/GuildReducer";
import { GuildRoomsPageState, RoomAction } from "@/presentation/redux/Room/RoomReducer";
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import StyleProps from "@/shared/interfaces/StyleProps";
import { useSplitView } from "@/shared/hooks/useSplitView";
import { usePushRoom } from "@/shared/hooks/usePushRoom";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace GuildsIndexPresenter {
    export type StackParams = {
    }
    
    export type StateProps = {
        roomsPageState: GuildRoomsPageState,
        pageState: GuildsPageState,
    }
    
    export type DispatchProps = {
        fetchGuilds: () => Action,
        fetchRooms: (guildId: string) => Action,
        fetchGuild: (id: string) => Action,
    }
    
    export type Input = NativeStackScreenProps<GuildsNavigatorStackParams, 'GuildsIndex'> & StyleProps;
    
    export type Output = {
        styles: GuildsStyles,
        appTheme: AppTheme,
        innerWidth: number,
        innerHeight: number,
        pageState: GuildsPageState,
        roomsPageState: GuildRoomsPageState,
        onPressTab: (guild: Guild) => void,
        onFocus: (guild: Guild) => void,
        onPressRoom: (room: Room) => void,
    };

    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            pageState: state.guild.pages.index,
            roomsPageState: state.room.pages.index,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchGuilds: () => GuildAction.fetchIndex.started({}),
            fetchRooms: (guildId: string) => RoomAction.fetchIndex.started({ guildId }),
            fetchGuild: (id: string) => GuildAction.fetchShow.started({ id }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.guild.pages.index == next.guild.pages.index &&
            prev.room.pages.index == next.room.pages.index
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };

    export function usePresenter(props: MergedProps): Output {
        const styles = GuildsIndexHooks.useStyles(props);
        const insets = useInsets({ bottomExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        const shouldSplit = useSplitView();
        const tabbarHeight = useBottomTabBarHeight();
        GuildsIndexHooks.useViewWillAppear(props);
        const onPressRoom = usePushRoom();
        return {
            ...props,
            ...insets,
            appTheme: props.appTheme ?? fallbackAppTheme,
            roomsPageState: props.roomsPageState,
            innerHeight: shouldSplit ? insets.innerHeight : insets.innerHeight - tabbarHeight,
            styles,
            onPressTab: (guild: Guild) => {
            },
            onFocus: (guild: Guild) => {
                props.fetchRooms(guild.id!);
                props.fetchGuild(guild.id!);
            },
            onPressRoom,
        }
    }
}

export default GuildsIndexPresenter;