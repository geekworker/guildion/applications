import React from 'react';
import { View } from 'react-native';
import SearchResultsPresenter from './presenter';
import { SafeAreaView } from 'react-native-safe-area-context';
import SearchResultsTemplate from '../../templates/SearchResultsTemplate';
import { Style } from '@/shared/interfaces/Style';
import { Guilds, Rooms, Posts } from '@guildion/core';

const SearchResultsComponent = ({ styles, appTheme, animatedStyle, innerHeight, innerWidth, guildsPageState, roomsPageState, postsPageState, navigation, query, onBlur, onChangeText, onFocus, onSubmitEditing, onPressRoom, onPressGuild }: SearchResultsPresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <SafeAreaView style={styles.safearea} edges={['top', 'right', 'left']}>
                <SearchResultsTemplate
                    style={new Style({
                        width: innerWidth,
                        height: innerHeight,
                    })}
                    appTheme={appTheme}
                    guilds={guildsPageState?.get('guilds') ?? new Guilds([])}
                    rooms={roomsPageState?.get('rooms') ?? new Rooms([])}
                    posts={postsPageState?.get('posts') ?? new Posts([])}
                    onPressBack={() => navigation.canGoBack() && navigation.goBack()}
                    loading={postsPageState?.get('loading') || guildsPageState?.get('loading') || roomsPageState?.get('loading')}
                    postsLoadingMore={postsPageState?.get('loadingMoreStatus')}
                    query={query}
                    onBlur={onBlur}
                    onChangeText={onChangeText}
                    onFocus={onFocus}
                    onSubmitEditing={onSubmitEditing}
                    onPressRoom={onPressRoom}
                    onPressGuild={onPressGuild}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(SearchResultsComponent, SearchResultsPresenter.outputAreEqual);