import React from 'react';
import SearchResultsStyles from './styles';
import type SearchResultsPresenter from './presenter';
import { ExploreNavigatorRoutes } from '../../navigators/ExploreNavigator/constants';
import { Guild, Room } from '@guildion/core';

namespace SearchResultsHooks {
    export const useStyles = (props: SearchResultsPresenter.MergedProps) => {
        const styles = React.useMemo(() => new SearchResultsStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SearchResultsPresenter.MergedProps) => {
        React.useEffect(() => {
            props.searchGuilds();
            props.searchRooms();
            props.searchPosts();
        }, [props.route.params.query]);
        const [query, setQuery] = React.useState(props.query);
        const onChangeText = React.useCallback((value: string) => {
            setQuery(value);
        }, []);
        const onSubmitEditing = React.useCallback(() => {
            query && props.navigation.push(ExploreNavigatorRoutes.SearchResults, { query: query });
        }, [props.navigation, query]);
        const onPressRoom = React.useCallback((room: Room) => {
            room.id && props.navigation.navigate(ExploreNavigatorRoutes.RoomPublic, { roomId: room.id });
        }, [props.navigation]);
        const onPressGuild = React.useCallback((guild: Guild) => {
            guild.id && props.navigation.navigate(ExploreNavigatorRoutes.GuildPublic, { guildId: guild.id });
        }, [props.navigation]);
        return {
            onSubmitEditing,
            onChangeText,
            query,
            onPressRoom,
            onPressGuild,
        };
    }
}

export default SearchResultsHooks;