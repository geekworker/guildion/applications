import SearchResultsHooks from './hooks';
import SearchResultsStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ExploreNavigatorStackParams } from '../../navigators/ExploreNavigator/constants';
import { RoomAction, SearchRoomsPageState } from '@/presentation/redux/Room/RoomReducer';
import { GuildAction, SearchGuildsPageState } from '@/presentation/redux/Guild/GuildReducer';
import { PostAction, SearchPostsPageState } from '@/presentation/redux/Post/PostReducer';
import { guildsSearchPageStateSelector } from '@/presentation/redux/Guild/GuildSelector';
import { roomsSearchPageStateSelector } from '@/presentation/redux/Room/RoomSelector';
import { postsSearchPageStateSelector } from '@/presentation/redux/Post/PostSelector';
import { useInsets } from '@/shared/hooks/useInsets';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { useSplitView } from '@/shared/hooks/useSplitView';
import { SearchInputProps } from '../../atoms/SearchInput';
import { Guild, Room } from '@guildion/core';

namespace SearchResultsPresenter {
    export type StackParams = {
        query: string,
    }

    export type StateProps = {
        guildsPageState?: SearchGuildsPageState,
        roomsPageState?: SearchRoomsPageState,
        postsPageState?: SearchPostsPageState,
        query: string,
    }
    
    export type DispatchProps = {
        searchGuilds: () => Action,
        searchMoreGuilds: () => Action,
        searchRooms: () => Action,
        searchMoreRooms: () => Action,
        searchPosts: () => Action,
        searchMorePosts: () => Action,
    }
    
    export type Input = NativeStackScreenProps<ExploreNavigatorStackParams, 'SearchResults'> & StyleProps;
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: SearchResultsStyles,
        appTheme: AppTheme,
        query: string,
        innerWidth: number,
        innerHeight: number,
        guildsPageState?: SearchGuildsPageState,
        roomsPageState?: SearchRoomsPageState,
        postsPageState?: SearchPostsPageState,
        onFocus?: SearchInputProps['onFocus'],
        onBlur?: SearchInputProps['onBlur'],
        onChangeText?: SearchInputProps['onChangeText'],
        onSubmitEditing?: SearchInputProps['onSubmitEditing'],
        onPressRoom: (room: Room) => void,
        onPressGuild: (guild: Guild) => void,
    } & NativeStackScreenProps<ExploreNavigatorStackParams, 'SearchResults'>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            query: ownProps.route.params.query,
            guildsPageState: guildsSearchPageStateSelector(state, ownProps.route.params.query),
            roomsPageState: roomsSearchPageStateSelector(state, ownProps.route.params.query),
            postsPageState: postsSearchPageStateSelector(state, ownProps.route.params.query),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            searchGuilds: () => GuildAction.search.started({ query: props.route.params.query }),
            searchMoreGuilds: () => GuildAction.searchMore.started({ query: props.route.params.query }),
            searchRooms: () => RoomAction.search.started({ query: props.route.params.query }),
            searchMoreRooms: () => RoomAction.searchMore.started({ query: props.route.params.query }),
            searchPosts: () => PostAction.search.started({ query: props.route.params.query }),
            searchMorePosts: () => PostAction.searchMore.started({ query: props.route.params.query }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.guild.pages.search.toJSON(), next.guild.pages.search.toJSON()) &&
            compare(prev.room.pages.search.toJSON(), next.room.pages.search.toJSON()) &&
            compare(prev.post.pages.search.toJSON(), next.post.pages.search.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = SearchResultsHooks.useStyles(props);
        const {
            onChangeText,
            onSubmitEditing,
            query,
            onPressRoom,
            onPressGuild,
        } = SearchResultsHooks.useState(props);
        const insets = useInsets({ bottomExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        const tabbarHeight = useBottomTabBarHeight();
        const shouldSplit = useSplitView();
        return {
            ...props,
            ...insets,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            innerHeight: shouldSplit ? insets.innerHeight : insets.innerHeight - tabbarHeight,
            onChangeText,
            onSubmitEditing,
            query,
            onPressRoom,
            onPressGuild,
        }
    }
}

export default SearchResultsPresenter;