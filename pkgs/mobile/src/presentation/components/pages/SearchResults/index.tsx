import React from 'react';
import SearchResultsPresenter from './presenter';
import SearchResultsComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: SearchResultsPresenter.MergedProps) => {
    const output = SearchResultsPresenter.usePresenter(props);
    return <SearchResultsComponent {...output} />;
};

const SearchResults = connect<SearchResultsPresenter.StateProps, SearchResultsPresenter.DispatchProps, SearchResultsPresenter.Input, SearchResultsPresenter.MergedProps, RootState>(
    SearchResultsPresenter.mapStateToProps,
    SearchResultsPresenter.mapDispatchToProps,
    SearchResultsPresenter.mergeProps,
    SearchResultsPresenter.connectOptions,
)(Container);

export default SearchResults;