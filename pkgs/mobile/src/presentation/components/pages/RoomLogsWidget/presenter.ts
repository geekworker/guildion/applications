import RoomLogsWidgetHooks from "./hooks";
import RoomLogsWidgetStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomWidgetsNavigatorStackParams } from "../../navigators/RoomWidgetsNavigator/constants";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { MediaLibraryWidgetName, PostWidgetName, Room, SettingWidgetName, SyncVisionWidgetName, TextChatWidgetName, Widget } from "@guildion/core";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";

namespace RoomLogsWidgetPresenter {
    export type StackParams = {
    }

    export type StateProps = {
    }
    
    export type DispatchProps = {
    }

    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        onChangeTabRouteName?: (value?: keyof RoomWidgetTabsNavigatorStackParams | 'SyncVisionWidget') => void,
        loading?: boolean,
        room: Room,
    } & NativeStackScreenProps<RoomWidgetsNavigatorStackParams, 'RoomLogsWidget'> & Partial<StyleProps>;

    export type Output = {
        styles: RoomLogsWidgetStyles,
        appTheme: AppTheme,
        animatedStyle?: AnimatedStyle,
        onPressWidget?: (widget: Widget) => void,
        loading?: boolean,
        room: Room,
    };

    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = RoomLogsWidgetHooks.useStyles(props);
        RoomLogsWidgetHooks.useTabNavigationPush(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPressWidget: (widget) => {
                if (props.onChangeTabRouteName)
                switch(widget.widgetname) {
                default: return props.onChangeTabRouteName!(undefined);
                case SyncVisionWidgetName: return props.onChangeTabRouteName('SyncVisionWidget');
                case TextChatWidgetName: return props.onChangeTabRouteName('TextChatWidget');
                case PostWidgetName: return props.onChangeTabRouteName('RoomPostsWidgetNavigator');
                case MediaLibraryWidgetName: return props.onChangeTabRouteName('MediaLibraryWidgetNavigator');
                case SettingWidgetName: return props.onChangeTabRouteName('RoomSettingWidget');
                }
            },
        }
    }
}

export default RoomLogsWidgetPresenter;