import React from "react";
import RoomLogsWidgetStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import RoomWidgetTabsNavigatorPresenter from "../../navigators/RoomWidgetTabsNavigator/presenter";
import { useNavigation } from "@react-navigation/core";
import RoomControlButtonRedux from "../../molecules/RoomControlButton/redux";
import TopMostsRedux from "../../molecules/TopMosts/redux";

namespace RoomLogsWidgetHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomLogsWidgetStyles({ ...props }), [
            props,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: { tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams }) => {
        const [tabShown, setTabShown] = React.useState<boolean>(false);
        const navigation = useNavigation<RoomWidgetTabsNavigatorPresenter.Input['navigation']>();
        const topMostsContext = React.useContext(TopMostsRedux.Context);
        const controlContext = React.useContext(RoomControlButtonRedux.Context);
        React.useEffect(() => {
            if (!props.tabInitialRouteName && tabShown) {
                controlContext.dispatch(RoomControlButtonRedux.Action.setOffsetBottom(0));
                topMostsContext.dispatch(TopMostsRedux.Action.hideMessageInputBar())
                setTabShown(false);
                navigation.goBack();
            } else if (!!props.tabInitialRouteName && !tabShown) {
                setTabShown(true);
                navigation.push('RoomWidgetTabsNavigator', {});
            };
        }, [props.tabInitialRouteName]);
    }
}

export default RoomLogsWidgetHooks