import { Style } from '@/shared/interfaces/Style';
import { Guild, Room, RoomLogs } from '@guildion/core';
import React from 'react';
import { Animated } from 'react-native';
import RoomLogsWidgetTemplate from '../../templates/RoomLogsWidgetTemplate';
import RoomLogsWidgetPresenter from './presenter';

const RoomLogsWidgetComponent: React.FC<RoomLogsWidgetPresenter.Output> = ({ styles, animatedStyle, appTheme, onPressWidget, loading }) => {
    return (
        <Animated.View style={{ ...styles.wrapper, ...animatedStyle?.toStyleObject() }}>
            <RoomLogsWidgetTemplate
                room={new Room()}
                guild={new Guild()}
                logs={new RoomLogs([])}
                appTheme={appTheme}
                onPressWidget={onPressWidget}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                })}
                animatedStyle={animatedStyle}
                loading={loading}
            />
        </Animated.View>
    );
};

export default React.memo(RoomLogsWidgetComponent, RoomLogsWidgetPresenter.outputAreEqual);