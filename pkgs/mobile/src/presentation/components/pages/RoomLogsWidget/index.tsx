import React from 'react';
import RoomLogsWidgetPresenter from './presenter';
import RoomLogsWidgetComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomLogsWidgetPresenter.MergedProps) => {
    const output = RoomLogsWidgetPresenter.usePresenter(props);
    return <RoomLogsWidgetComponent {...output} />;
};

const RoomLogsWidget = connect<RoomLogsWidgetPresenter.StateProps, RoomLogsWidgetPresenter.DispatchProps, RoomLogsWidgetPresenter.Input, RoomLogsWidgetPresenter.MergedProps, RootState>(
    RoomLogsWidgetPresenter.mapStateToProps,
    RoomLogsWidgetPresenter.mapDispatchToProps,
    RoomLogsWidgetPresenter.mergeProps,
    RoomLogsWidgetPresenter.connectOptions,
)(Container);

export default RoomLogsWidget;