import React from 'react';
import MediaLibraryWidgetAlbumStyles from './styles';
import type MediaLibraryWidgetAlbumPresenter from './presenter';
import StyleProps from '@/shared/interfaces/StyleProps';

namespace MediaLibraryWidgetAlbumHooks {
    export const useStyles = (props: MediaLibraryWidgetAlbumPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetAlbumStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: MediaLibraryWidgetAlbumPresenter.MergedProps) => {
        return {};
    }
}

export default MediaLibraryWidgetAlbumHooks;