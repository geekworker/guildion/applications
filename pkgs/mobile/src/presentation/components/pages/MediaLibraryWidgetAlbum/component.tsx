import React from 'react';
import { Animated, View } from 'react-native';
import MediaLibraryWidgetAlbumPresenter from './presenter';

const MediaLibraryWidgetAlbumComponent = ({ styles, appTheme, animatedStyle,  }: MediaLibraryWidgetAlbumPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            
        </Animated.View>
    );
};

export default React.memo(MediaLibraryWidgetAlbumComponent, MediaLibraryWidgetAlbumPresenter.outputAreEqual);