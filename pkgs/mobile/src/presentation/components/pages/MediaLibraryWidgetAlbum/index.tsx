import React from 'react';
import MediaLibraryWidgetAlbumPresenter from './presenter';
import MediaLibraryWidgetAlbumComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetAlbumPresenter.MergedProps) => {
    const output = MediaLibraryWidgetAlbumPresenter.usePresenter(props);
    return <MediaLibraryWidgetAlbumComponent {...output} />;
};

const MediaLibraryWidgetAlbum = connect<MediaLibraryWidgetAlbumPresenter.StateProps, MediaLibraryWidgetAlbumPresenter.DispatchProps, MediaLibraryWidgetAlbumPresenter.Input, MediaLibraryWidgetAlbumPresenter.MergedProps, RootState>(
    MediaLibraryWidgetAlbumPresenter.mapStateToProps,
    MediaLibraryWidgetAlbumPresenter.mapDispatchToProps,
    MediaLibraryWidgetAlbumPresenter.mergeProps,
    MediaLibraryWidgetAlbumPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetAlbum;