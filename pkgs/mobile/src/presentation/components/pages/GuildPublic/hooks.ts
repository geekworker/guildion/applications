import React from 'react';
import GuildPublicStyles from './styles';
import type GuildPublicPresenter from './presenter';
import { Guild } from '@guildion/core';

namespace GuildPublicHooks {
    export const useStyles = (props: GuildPublicPresenter.MergedProps) => {
        const styles = React.useMemo(() => new GuildPublicStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useViewWillAppear = (props: GuildPublicPresenter.MergedProps) => {
        React.useEffect(() => {
            if ((
                props.exploreScreenProps?.navigation?.isFocused() ||
                props.modalScreenProps?.navigation?.isFocused()
            ) && !!props.guildId) {
                props.fetchGuild(props.guildId);
            };
        }, [props.exploreScreenProps?.navigation, props.modalScreenProps?.navigation, props.guildId]);
    }

    export const useState = (props: GuildPublicPresenter.MergedProps) => {
        const onPressClose = React.useCallback((guild: Guild) => {
            props.exploreScreenProps?.navigation.goBack();
            props.modalScreenProps?.navigation.goBack()
        }, [props.exploreScreenProps?.navigation, props.modalScreenProps?.navigation]);
        const onPressEdit = React.useCallback((guild: Guild) => {
        }, [props.exploreScreenProps?.navigation, props.modalScreenProps?.navigation]);
        return {
            onPressClose,
            onPressEdit
        };
    }
}

export default GuildPublicHooks;