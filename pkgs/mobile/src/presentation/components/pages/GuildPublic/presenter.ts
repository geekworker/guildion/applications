import GuildPublicHooks from './hooks';
import GuildPublicStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ExploreNavigatorStackParams } from '../../navigators/ExploreNavigator/constants';
import { GuildAction, GuildPageState } from '@/presentation/redux/Guild/GuildReducer';
import { guildShowPageSelector } from '@/presentation/redux/Guild/GuildSelector';
import { useInsets } from '@/shared/hooks/useInsets';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { useSplitView } from '@/shared/hooks/useSplitView';
import { RootNavigatorStackParams } from '../../navigators/RootNavigator/constants';
import { Guild } from '@guildion/core';

namespace GuildPublicPresenter {
    export type StackParams = {
        guildId: string,
    }

    export type StateProps = {
        guildId: string,
        guildPageState?: GuildPageState,
    }
    
    export type DispatchProps = {
        fetchGuild: (id: string) => Action,
    }

    export type ExploreScreenProps = NativeStackScreenProps<ExploreNavigatorStackParams, 'GuildPublic'>;
    export type ModalScreenProps = NativeStackScreenProps<RootNavigatorStackParams, 'GuildPublic'>;
    export type Input = StyleProps & {
        exploreScreenProps?: ExploreScreenProps,
        modalScreenProps?: ModalScreenProps,
    };
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: GuildPublicStyles,
        appTheme: AppTheme,
        guildId: string,
        guildPageState?: GuildPageState,
        innerWidth: number,
        innerHeight: number,
        onPressClose?: (guild: Guild) => void,
        onPressEdit?: (guild: Guild) => void,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        const guildId = ownProps.modalScreenProps?.route.params.guildId ?? ownProps.exploreScreenProps?.route.params.guildId ?? '';
        return {
            guildPageState: guildShowPageSelector(state, { id: guildId, isPublic: true }),
            guildId,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchGuild: (id: string) => GuildAction.fetchShow.started({ id, isPublic: true }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.guild.pages.show.toJSON(), next.guild.pages.show.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = GuildPublicHooks.useStyles(props);
        const {
            onPressClose,
            onPressEdit,
        } = GuildPublicHooks.useState(props);
        GuildPublicHooks.useViewWillAppear(props);
        const insets = useInsets({ bottomExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        const tabbarHeight = useBottomTabBarHeight();
        const shouldSplit = useSplitView();
        return {
            ...props,
            ...insets,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            innerHeight: shouldSplit ? insets.innerHeight : insets.innerHeight - tabbarHeight,
            onPressClose,
            onPressEdit,
        }
    }
}

export default GuildPublicPresenter;