import React from 'react';
import { Animated, View } from 'react-native';
import { Guild, RoomCategories } from '@guildion/core';
import GuildPublicTemplate from '../../templates/GuildPublicTemplate';
import GuildPublicPresenter from './presenter';
import { Style } from '@/shared/interfaces/Style';

const GuildPublicComponent = ({ styles, appTheme, animatedStyle, guildPageState, guildId, innerHeight, innerWidth, onPressClose, onPressEdit }: GuildPublicPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.wrapper, ...animatedStyle?.toStyleObject() }}>
            <GuildPublicTemplate
                appTheme={appTheme}
                guild={guildPageState?.get('guild') ?? new Guild()}
                categories={new RoomCategories([])}
                loading={guildPageState?.get('loading') ?? true}
                style={new Style({
                    width: innerWidth,
                    height: innerHeight,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
                onPressClose={onPressClose}
                onPressEdit={onPressEdit}
            />
        </Animated.View>
    );
};

export default React.memo(GuildPublicComponent, GuildPublicPresenter.outputAreEqual);