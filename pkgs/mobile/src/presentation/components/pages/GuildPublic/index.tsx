import React from 'react';
import GuildPublicPresenter from './presenter';
import GuildPublicComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: GuildPublicPresenter.MergedProps) => {
    const output = GuildPublicPresenter.usePresenter(props);
    return <GuildPublicComponent {...output} />;
};

const GuildPublic = connect<GuildPublicPresenter.StateProps, GuildPublicPresenter.DispatchProps, GuildPublicPresenter.Input, GuildPublicPresenter.MergedProps, RootState>(
    GuildPublicPresenter.mapStateToProps,
    GuildPublicPresenter.mapDispatchToProps,
    GuildPublicPresenter.mergeProps,
    GuildPublicPresenter.connectOptions,
)(Container);

export default GuildPublic;