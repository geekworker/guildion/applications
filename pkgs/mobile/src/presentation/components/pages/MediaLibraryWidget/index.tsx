import React from 'react';
import MediaLibraryWidgetPresenter from './presenter';
import MediaLibraryWidgetComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetPresenter.MergedProps) => {
    const output = MediaLibraryWidgetPresenter.usePresenter(props);
    return <MediaLibraryWidgetComponent {...output} />;
};

const MediaLibraryWidget = connect<MediaLibraryWidgetPresenter.StateProps, MediaLibraryWidgetPresenter.DispatchProps, MediaLibraryWidgetPresenter.Input, MediaLibraryWidgetPresenter.MergedProps, RootState>(
    MediaLibraryWidgetPresenter.mapStateToProps,
    MediaLibraryWidgetPresenter.mapDispatchToProps,
    MediaLibraryWidgetPresenter.mergeProps,
    MediaLibraryWidgetPresenter.connectOptions,
)(Container);

export default MediaLibraryWidget;