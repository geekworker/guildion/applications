import MediaLibraryWidgetHooks from "./hooks";
import MediaLibraryWidgetStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { Room } from "@guildion/core";
import { RoomFileAction, RoomRecentFilesPageState } from "@/presentation/redux/RoomFile/RoomFileReducer";
import { roomFilesRecentPageStateSelector } from "@/presentation/redux/RoomFile/RoomFileSelector";
import { MediaLibraryWidgetNavigatorStackParams } from "../../navigators/MediaLibraryWidgetNavigator/constants";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";

namespace MediaLibraryWidgetPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState?: RoomRecentFilesPageState,
    }
    
    export type DispatchProps = {
        fetchFiles: () => Action,
        fetchMoreFiles: () => Action,
    }

    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        parentStackScreen: NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'MediaLibraryWidgetNavigator'> & Partial<StyleProps>
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidget'> & Partial<StyleProps>;

    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: MediaLibraryWidgetStyles,
        appTheme: AppTheme,
        isFocused: boolean,
        room: Room,
        pageState?: RoomRecentFilesPageState,
        onScrollToBottom: () => void,
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidget'>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, props) => {
        return {
            pageState: roomFilesRecentPageStateSelector(state, props.room.id!, props.room.guildId!),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchFiles: () => RoomFileAction.fetchRecentFiles.started({ roomId: props.room.id ?? '', guildId: props.room.guildId ?? '' }),
            fetchMoreFiles: () => RoomFileAction.fetchMoreRecentFiles.started({ roomId: props.room.id ?? '', guildId: props.room.guildId ?? '' }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.roomFile.pages.recents.toJSON(), next.roomFile.pages.recents.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };

    export function usePresenter(props: MergedProps): Output {
        const styles = MediaLibraryWidgetHooks.useStyles(props);
        MediaLibraryWidgetHooks.useTabNavigationPush(props);
        const isFocused = MediaLibraryWidgetHooks.useFocused(props);
        const {
            onScrollToBottom,
        } = MediaLibraryWidgetHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToBottom,
        }
    }
}

export default MediaLibraryWidgetPresenter;