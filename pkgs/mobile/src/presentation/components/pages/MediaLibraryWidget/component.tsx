import { localizer } from '@/shared/constants/Localizer';
import { Style } from '@/shared/interfaces/Style';
import { Files } from '@guildion/core';
import React from 'react';
import { View } from 'react-native';
import { MediaLibraryWidgetNavigatorRoutes } from '../../navigators/MediaLibraryWidgetNavigator/constants';
import MediaLibraryWidgetTemplate from '../../templates/MediaLibraryWidgetTemplate';
import MediaLibraryWidgetPresenter from './presenter';

const MediaLibraryWidgetComponent: React.FC<MediaLibraryWidgetPresenter.Output> = ({ appTheme, styles, isFocused, pageState, room, onScrollToBottom, navigation }) => {
    return (
        <View style={styles.wrapper}>
            <MediaLibraryWidgetTemplate
                appTheme={appTheme}
                files={pageState?.get('files') ?? new Files([])}
                loading={pageState?.get('loading')}
                loadingMore={pageState?.get('loadingMoreStatus')}
                room={room}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
                isFocused={isFocused}
                showAlbumButton
                showPlaylistButton
                showRoomButton
                showHeader
                showSearchBar
                title={localizer.dictionary.guild.files.index.title}
                sectionTitle={localizer.dictionary.guild.files.index.listTitle}
                onScrollToBottom={onScrollToBottom}
                onPressRoomFiles={() => navigation.navigate(MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoom, {})}
                onPressAlbums={() => navigation.navigate(MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetAlbums, {})}
                onPressPlaylists={() => navigation.navigate(MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetPlaylists, {})}
            />
        </View>
    );
};

export default React.memo(MediaLibraryWidgetComponent, MediaLibraryWidgetPresenter.outputAreEqual);