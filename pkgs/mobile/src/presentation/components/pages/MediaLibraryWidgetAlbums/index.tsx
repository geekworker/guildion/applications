import React from 'react';
import MediaLibraryWidgetAlbumsPresenter from './presenter';
import MediaLibraryWidgetAlbumsComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetAlbumsPresenter.MergedProps) => {
    const output = MediaLibraryWidgetAlbumsPresenter.usePresenter(props);
    return <MediaLibraryWidgetAlbumsComponent {...output} />;
};

const MediaLibraryWidgetAlbums = connect<MediaLibraryWidgetAlbumsPresenter.StateProps, MediaLibraryWidgetAlbumsPresenter.DispatchProps, MediaLibraryWidgetAlbumsPresenter.Input, MediaLibraryWidgetAlbumsPresenter.MergedProps, RootState>(
    MediaLibraryWidgetAlbumsPresenter.mapStateToProps,
    MediaLibraryWidgetAlbumsPresenter.mapDispatchToProps,
    MediaLibraryWidgetAlbumsPresenter.mergeProps,
    MediaLibraryWidgetAlbumsPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetAlbums;