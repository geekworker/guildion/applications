import React from 'react';
import MediaLibraryWidgetAlbumsStyles from './styles';
import type MediaLibraryWidgetAlbumsPresenter from './presenter';
import { RoomWidgetTabsNavigatorRoutes } from '../../navigators/RoomWidgetTabsNavigator/constants';

namespace MediaLibraryWidgetAlbumsHooks {
    export const useStyles = (props: MediaLibraryWidgetAlbumsPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetAlbumsStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: MediaLibraryWidgetAlbumsPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'MediaLibraryWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: MediaLibraryWidgetAlbumsPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            isFocused && props.fetchFiles();
        }, [props.room.id, props.room.guildId, isFocused]);
        return isFocused;
    }

    export const useState = (props: MediaLibraryWidgetAlbumsPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.pageState?.get('paginatable') && props.fetchMoreFiles(), [props.pageState?.get('paginatable'), props.fetchMoreFiles]);
        return {
            onScrollToBottom,
        };
    }
}

export default MediaLibraryWidgetAlbumsHooks;