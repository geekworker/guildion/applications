import React from 'react';
import AccountAuthenticatePresenter from './presenter';
import AccountAuthenticateComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: AccountAuthenticatePresenter.MergedProps) => {
    const output = AccountAuthenticatePresenter.usePresenter(props);
    return <AccountAuthenticateComponent {...output} />;
};

const AccountAuthenticate = connect<AccountAuthenticatePresenter.StateProps, AccountAuthenticatePresenter.DispatchProps, AccountAuthenticatePresenter.Input, AccountAuthenticatePresenter.MergedProps, RootState>(
    AccountAuthenticatePresenter.mapStateToProps,
    AccountAuthenticatePresenter.mapDispatchToProps,
    AccountAuthenticatePresenter.mergeProps,
    AccountAuthenticatePresenter.connectOptions,
)(Container);

export default AccountAuthenticate;
