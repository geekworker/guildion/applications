import { Action } from "redux";
import AccountAuthenticateHooks from "./hooks";
import AccountAuthenticateStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { WelcomeNavigatorRoutes, WelcomeNavigatorStackParams } from '@/presentation/components/navigators/WelcomeNavigator/constants';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { AccountAction, AccountAuthenticatePageState } from "@/presentation/redux/Account/AccountReducer";
import { AppTheme } from "@/shared/constants/AppTheme";
import { accountUsernameErrorSelector, accountUsernameErrorsSelector } from "@/presentation/redux/App/AppSelector";
import { useInsets } from "@/shared/hooks/useInsets";
import StyleProps from "@/shared/interfaces/StyleProps";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace AccountAuthenticatePresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState: AccountAuthenticatePageState,
        usernameError?: string,
    }
    
    export type DispatchProps = {
        authenticate: (username: string) => Action,
        setUsername: (username?: string) => Action,
        didSuccess: () => Action,
    }
    
    export type Input = NativeStackScreenProps<WelcomeNavigatorStackParams, 'AccountAuthenticate'> & StyleProps;
    
    export type Output = {
        styles: AccountAuthenticateStyles,
        appTheme?: AppTheme,
        innerWidth: number,
        innerHeight: number,
        usernameError?: string,
        onSubmit: () => void,
        onChangeUsername: (username?: string) => void,
        pageState: AccountAuthenticatePageState,
        username?: string,
        sendable: boolean,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            pageState: state.account.pages.authenticate,
            usernameError: accountUsernameErrorSelector(state),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            authenticate: (username: string) => AccountAction.authenticate.started({ username }),
            setUsername: (username?: string) => AccountAction.setAuthenticateState({ username }),
            didSuccess: () => AccountAction.setAuthenticateState({ success: false }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.pages.authenticate == next.account.pages.authenticate &&
            prev.app.errors == next.app.errors
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = AccountAuthenticateHooks.useStyles(props);
        const insets = useInsets({ width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        AccountAuthenticateHooks.useSuccess(() => onSuccess(props), props);
        const {
            username,
            onChangeUsername,
            sendable,
            onSubmit,
        } = AccountAuthenticateHooks.useState(props);
        return {
            ...props,
            ...insets,
            styles,
            onSubmit,
            onChangeUsername,
            username,
            sendable,
        }
    }

    function onSuccess(props: MergedProps) {
        props.navigation.push(
            props.pageState.get('shouldTwoFactorAuth') ?
                WelcomeNavigatorRoutes.AccountVerify :
                WelcomeNavigatorRoutes.AccountLoading,
            {});
    }
}

export default AccountAuthenticatePresenter;