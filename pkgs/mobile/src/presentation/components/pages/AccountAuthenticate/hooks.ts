import React from "react";
import AccountAuthenticateStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type AccountAuthenticatePresenter from "./presenter";

namespace AccountAuthenticateHooks {

    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new AccountAuthenticateStyles({ ...props }), [props]);
        return styles;
    }

    export const useSuccess = (onSuccess: () => void, { pageState, didSuccess }: AccountAuthenticatePresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!pageState.get('success')) {
                onSuccess();
                didSuccess();
            };
        },[pageState.get('success')])
    }

    export const useState = (props: AccountAuthenticatePresenter.MergedProps) => {
        const [username, setUsername] = React.useState<string | undefined>(props.pageState.get('username'));
        React.useEffect(() => {
            props.pageState.get('username') != username && setUsername(props.pageState.get('username'))
        }, [props.pageState.get('username')]);
        const sendable = React.useMemo(() => 
            !!username && username.length > 0,
            [username]
        );
        const onChangeUsername = React.useCallback(
            (username?: string) => {
                setUsername(username);
            },
            [username, setUsername]
        );
        const onSubmit = React.useCallback(
            () => {
                props.setUsername(username);
                if (sendable && username && username.length > 0) props.authenticate(username);
            },
            [props.authenticate, username]
        );
        React.useEffect(() => {
            props.setUsername(username);
        });
        return {
            username,
            setUsername,
            onChangeUsername,
            sendable,
            onSubmit,
        };
    }
}

export default AccountAuthenticateHooks