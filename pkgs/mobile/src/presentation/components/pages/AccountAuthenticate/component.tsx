import React from 'react';
import { View } from 'react-native'
import AccountAuthenticatePresenter from './presenter';
import AccountAuthenticateTemplate from '../../templates/AccountAuthenticateTemplate';
import { Style } from '@/shared/interfaces/Style';

const AccountAuthenticateComponent = ({ styles, innerWidth, innerHeight, appTheme, username, sendable, onSubmit, onChangeUsername, usernameError, pageState }: AccountAuthenticatePresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <AccountAuthenticateTemplate
                style={new Style({
                    width: innerWidth,
                    height: innerHeight,
                })}
                appTheme={appTheme}
                sendable={sendable}
                onSubmit={onSubmit}
                username={username}
                usernameError={usernameError}
                onChangeUsername={onChangeUsername}
                loading={pageState.get('loading')}
            />
        </View>
    );
};

export default React.memo(AccountAuthenticateComponent, AccountAuthenticatePresenter.outputAreEqual);