import { Style } from '@/shared/interfaces/Style';
import { Posts } from '@guildion/core';
import React from 'react';
import { View } from 'react-native';
import PostsWidgetTemplate from '../../templates/PostsWidgetTemplate';
import RoomPostsWidgetPresenter from './presenter';

const RoomPostsWidgetComponent: React.FC<RoomPostsWidgetPresenter.Output> = ({ appTheme, styles, animatedStyle, isFocused, pageState, room, onScrollToBottom, navigation }) => {
    return (
        <View style={styles.wrapper}>
            <PostsWidgetTemplate
                appTheme={appTheme}
                posts={pageState?.get('posts') ?? new Posts([])}
                loadingMore={pageState?.get('loadingMoreStatus')}
                onScrollToBottom={onScrollToBottom}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
                onPressSeeMore={(post) => navigation.navigate('RoomPostsWidgetShow', { postId: post.id! })}
            />
        </View>
    );
};

export default React.memo(RoomPostsWidgetComponent, RoomPostsWidgetPresenter.outputAreEqual);