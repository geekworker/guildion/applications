import React from 'react';
import RoomPostsWidgetPresenter from './presenter';
import RoomPostsWidgetComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomPostsWidgetPresenter.MergedProps) => {
    const output = RoomPostsWidgetPresenter.usePresenter(props);
    return <RoomPostsWidgetComponent {...output} />;
};

const RoomPostsWidget = connect<RoomPostsWidgetPresenter.StateProps, RoomPostsWidgetPresenter.DispatchProps, RoomPostsWidgetPresenter.Input, RoomPostsWidgetPresenter.MergedProps, RootState>(
    RoomPostsWidgetPresenter.mapStateToProps,
    RoomPostsWidgetPresenter.mapDispatchToProps,
    RoomPostsWidgetPresenter.mergeProps,
    RoomPostsWidgetPresenter.connectOptions,
)(Container);

export default RoomPostsWidget;