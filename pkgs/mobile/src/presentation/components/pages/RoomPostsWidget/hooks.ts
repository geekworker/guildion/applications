import React from "react";
import RoomPostsWidgetStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { RoomWidgetTabsNavigatorRoutes } from "../../navigators/RoomWidgetTabsNavigator/constants";
import type RoomPostsWidgetPresenter from "./presenter";

namespace RoomPostsWidgetHooks {
    export const useStyles = (props: Partial<StyleProps>) => {
        const styles = React.useMemo(() => new RoomPostsWidgetStyles({ ...props }), [
            props,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: RoomPostsWidgetPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'RoomPostsWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: RoomPostsWidgetPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.RoomPostsWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            isFocused && props.fetch();
        }, [props.room.id, isFocused]);
        return isFocused;
    }

    export const useState = (props: RoomPostsWidgetPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.pageState?.get('paginatable') && props.fetchMore(), [props.pageState?.get('paginatable'), props.fetchMore]);
        return {
            onScrollToBottom,
        };
    }
}

export default RoomPostsWidgetHooks