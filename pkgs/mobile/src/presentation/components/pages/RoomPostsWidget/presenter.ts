import RoomPostsWidgetHooks from "./hooks";
import RoomPostsWidgetStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { Room } from "@guildion/core";
import { RoomPostsWidgetNavigatorStackParams } from "../../navigators/RoomPostsWidgetNavigator/constants";
import { PostAction, RoomPostsPageState } from "@/presentation/redux/Post/PostReducer";
import { postsIndexPageStateSelector } from "@/presentation/redux/Post/PostSelector";

namespace RoomPostsWidgetPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState?: RoomPostsPageState,
    }
    
    export type DispatchProps = {
        fetch: () => Action,
        fetchMore: () => Action,
    }

    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        parentStackScreen: NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'RoomPostsWidgetNavigator'> & Partial<StyleProps>
    } & NativeStackScreenProps<RoomPostsWidgetNavigatorStackParams, 'RoomPostsWidget'> & Partial<StyleProps>;

    export type Output = {
        styles: RoomPostsWidgetStyles,
        appTheme: AppTheme,
        isFocused: boolean,
        room: Room,
        pageState?: RoomPostsPageState,
        onScrollToBottom: () => void,
    } & NativeStackScreenProps<RoomPostsWidgetNavigatorStackParams, 'RoomPostsWidget'> & Partial<StyleProps>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            pageState: postsIndexPageStateSelector(state, ownProps.room.id!),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetch: () => PostAction.fetchIndex.started({ roomId: props.room.id ?? '' }),
            fetchMore: () => PostAction.fetchIndex.started({ roomId: props.room.id ?? '' }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.post.pages.index.toJSON(), next.post.pages.index.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
        
    export function usePresenter(props: MergedProps): Output {
        const styles = RoomPostsWidgetHooks.useStyles(props);
        RoomPostsWidgetHooks.useTabNavigationPush(props);
        const isFocused = RoomPostsWidgetHooks.useFocused(props);
        const {
            onScrollToBottom,
        } = RoomPostsWidgetHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToBottom,
        }
    }
}

export default RoomPostsWidgetPresenter;