import React from 'react';
import { View } from 'react-native'
import AccountLoadingPresenter from './presenter';
import { SafeAreaView } from 'react-native-safe-area-context';
import ScreenLoadingTemplate from '../../templates/ScreenLoadingTemplate';
import { Style } from '@/shared/interfaces/Style';

const AccountLoadingComponent = ({ styles, innerHeight, innerWidth, appTheme }: AccountLoadingPresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <SafeAreaView style={styles.safearea} edges={['top', 'right', 'left']}>
                <ScreenLoadingTemplate
                    style={new Style({
                        width: innerWidth,
                        height: innerHeight,
                    })}
                    appTheme={appTheme}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(AccountLoadingComponent, AccountLoadingPresenter.outputAreEqual);