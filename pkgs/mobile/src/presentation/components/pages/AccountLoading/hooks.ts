import { useMemo } from "react";
import AccountLoadingStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace AccountLoadingHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = useMemo(() => new AccountLoadingStyles({ ...props }), [props]);
        return styles;
    }
}

export default AccountLoadingHooks