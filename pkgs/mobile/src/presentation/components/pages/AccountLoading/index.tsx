import React from 'react';
import AccountLoadingPresenter from './presenter';
import AccountLoadingComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: AccountLoadingPresenter.MergedProps) => {
    const output = AccountLoadingPresenter.usePresenter(props);
    return <AccountLoadingComponent {...output} />;
};

const AccountLoading = connect<AccountLoadingPresenter.StateProps, AccountLoadingPresenter.DispatchProps, AccountLoadingPresenter.Input, AccountLoadingPresenter.MergedProps, RootState>(
    AccountLoadingPresenter.mapStateToProps,
    AccountLoadingPresenter.mapDispatchToProps,
    AccountLoadingPresenter.mergeProps,
    AccountLoadingPresenter.connectOptions,
)(Container);

export default AccountLoading;
