import AccountLoadingHooks from "./hooks";
import AccountLoadingStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import type { WelcomeNavigatorStackParams } from '@/presentation/components/navigators/WelcomeNavigator/constants';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { AppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import { useInsets } from "@/shared/hooks/useInsets";
import { AccountLoadingPageState } from "@/presentation/redux/Account/AccountReducer";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace AccountLoadingPresenter {
    export type StackParams = {  
    }

    export type StateProps = {
        pageState: AccountLoadingPageState,
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<WelcomeNavigatorStackParams, 'AccountLoading'> & StyleProps;
    
    export type Output = {
        styles: AccountLoadingStyles,
        appTheme?: AppTheme,
        innerWidth: number,
        innerHeight: number,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            pageState: state.account.pages.loading,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.pages.loading == next.account.pages.loading
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = AccountLoadingHooks.useStyles(props);
        const insets = useInsets({ width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        return {
            ...props,
            ...insets,
            styles,
        }
    }
}

export default AccountLoadingPresenter;