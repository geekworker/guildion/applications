import React from 'react';
import RoomSettingWidgetPresenter from './presenter';
import RoomSettingWidgetComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomSettingWidgetPresenter.MergedProps) => {
    const output = RoomSettingWidgetPresenter.usePresenter(props);
    return <RoomSettingWidgetComponent {...output} />;
};

const RoomSettingWidget = connect<RoomSettingWidgetPresenter.StateProps, RoomSettingWidgetPresenter.DispatchProps, RoomSettingWidgetPresenter.Input, RoomSettingWidgetPresenter.MergedProps, RootState>(
    RoomSettingWidgetPresenter.mapStateToProps,
    RoomSettingWidgetPresenter.mapDispatchToProps,
    RoomSettingWidgetPresenter.mergeProps,
    RoomSettingWidgetPresenter.connectOptions,
)(Container);

export default RoomSettingWidget;