import React from "react";
import RoomSettingWidgetStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import type RoomSettingWidgetPresenter from "./presenter";
import { useNavigation } from "@react-navigation/core";

namespace RoomSettingWidgetHooks {
    export const useStyles = (props: Partial<StyleProps>) => {
        const styles = React.useMemo(() => new RoomSettingWidgetStyles({ ...props }), [
            props,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: { tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams }) => {
        const navigation = useNavigation<RoomSettingWidgetPresenter.Input['navigation']>();
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'RoomSettingWidget') {
                navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }
}

export default RoomSettingWidgetHooks