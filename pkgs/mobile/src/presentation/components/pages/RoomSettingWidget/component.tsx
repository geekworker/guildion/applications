import React from 'react';
import { Text, View } from 'react-native';
import RoomSettingWidgetPresenter from './presenter';

const RoomSettingWidgetComponent: React.FC<RoomSettingWidgetPresenter.Output> = ({ styles, appTheme }) => {
    return (
        <View style={styles.wrapper}>
            <Text style={styles.text}>SETTING</Text>
        </View>
    );
};

export default React.memo(RoomSettingWidgetComponent, RoomSettingWidgetPresenter.outputAreEqual);