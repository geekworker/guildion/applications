import React from 'react';
import MediaLibraryWidgetPlaylistsStyles from './styles';
import type MediaLibraryWidgetPlaylistsPresenter from './presenter';
import { RoomWidgetTabsNavigatorRoutes } from '../../navigators/RoomWidgetTabsNavigator/constants';

namespace MediaLibraryWidgetPlaylistsHooks {
    export const useStyles = (props: MediaLibraryWidgetPlaylistsPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetPlaylistsStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: MediaLibraryWidgetPlaylistsPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'MediaLibraryWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: MediaLibraryWidgetPlaylistsPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            isFocused && props.fetchFiles();
        }, [props.room.id, props.room.guildId, isFocused]);
        return isFocused;
    }

    export const useState = (props: MediaLibraryWidgetPlaylistsPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.pageState?.get('paginatable') && props.fetchMoreFiles(), [props.pageState?.get('paginatable'), props.fetchMoreFiles]);
        return {
            onScrollToBottom,
        };
    }
}

export default MediaLibraryWidgetPlaylistsHooks;