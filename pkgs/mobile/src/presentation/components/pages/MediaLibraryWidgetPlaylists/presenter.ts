import MediaLibraryWidgetPlaylistsHooks from './hooks';
import MediaLibraryWidgetPlaylistsStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { GuildPlaylistsPageState, GuildFileAction } from '@/presentation/redux/GuildFile/GuildFileReducer';
import { RoomWidgetTabsNavigatorStackParams } from '../../navigators/RoomWidgetTabsNavigator/constants';
import { Room } from '@guildion/core';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { MediaLibraryWidgetNavigatorStackParams } from '../../navigators/MediaLibraryWidgetNavigator/constants';
import { guildFilePlaylistsPageStateSelector } from '@/presentation/redux/GuildFile/GuildFileSelector';

namespace MediaLibraryWidgetPlaylistsPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState?: GuildPlaylistsPageState,
    }
    
    export type DispatchProps = {
        fetchFiles: () => Action,
        fetchMoreFiles: () => Action,
    }

    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        parentStackScreen: NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'MediaLibraryWidgetNavigator'> & Partial<StyleProps>
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidgetPlaylists'> & Partial<StyleProps>;

    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: MediaLibraryWidgetPlaylistsStyles,
        appTheme: AppTheme,
        isFocused: boolean,
        room: Room,
        pageState?: GuildPlaylistsPageState,
        onScrollToBottom: () => void,
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidgetPlaylists'>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            pageState: guildFilePlaylistsPageStateSelector(state, ownProps.room.guildId!, ownProps.room.id),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchFiles: () => GuildFileAction.fetchPlaylists.started({ guildId: props.room.guildId ?? '', roomId: props.room.id }),
            fetchMoreFiles: () => GuildFileAction.fetchMorePlaylists.started({ guildId: props.room.guildId ?? '', roomId: props.room.id }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.guildFile.pages.playlists.toJSON(), next.guildFile.pages.playlists.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = MediaLibraryWidgetPlaylistsHooks.useStyles(props);
        MediaLibraryWidgetPlaylistsHooks.useTabNavigationPush(props);
        const isFocused = MediaLibraryWidgetPlaylistsHooks.useFocused(props);
        const {
            onScrollToBottom,
        } = MediaLibraryWidgetPlaylistsHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToBottom,
        }
    }
}

export default MediaLibraryWidgetPlaylistsPresenter;