import React from 'react';
import MediaLibraryWidgetPlaylistsPresenter from './presenter';
import MediaLibraryWidgetPlaylistsComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetPlaylistsPresenter.MergedProps) => {
    const output = MediaLibraryWidgetPlaylistsPresenter.usePresenter(props);
    return <MediaLibraryWidgetPlaylistsComponent {...output} />;
};

const MediaLibraryWidgetPlaylists = connect<MediaLibraryWidgetPlaylistsPresenter.StateProps, MediaLibraryWidgetPlaylistsPresenter.DispatchProps, MediaLibraryWidgetPlaylistsPresenter.Input, MediaLibraryWidgetPlaylistsPresenter.MergedProps, RootState>(
    MediaLibraryWidgetPlaylistsPresenter.mapStateToProps,
    MediaLibraryWidgetPlaylistsPresenter.mapDispatchToProps,
    MediaLibraryWidgetPlaylistsPresenter.mergeProps,
    MediaLibraryWidgetPlaylistsPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetPlaylists;