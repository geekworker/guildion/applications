import React from 'react';
import MediaLibraryWidgetRoomAlbumsStyles from './styles';
import type MediaLibraryWidgetRoomAlbumsPresenter from './presenter';
import { RoomWidgetTabsNavigatorRoutes } from '../../navigators/RoomWidgetTabsNavigator/constants';

namespace MediaLibraryWidgetRoomAlbumsHooks {
    export const useStyles = (props: MediaLibraryWidgetRoomAlbumsPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetRoomAlbumsStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: MediaLibraryWidgetRoomAlbumsPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'MediaLibraryWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: MediaLibraryWidgetRoomAlbumsPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            isFocused && props.fetchFiles();
        }, [props.room.id, props.room.guildId, isFocused]);
        return isFocused;
    }

    export const useState = (props: MediaLibraryWidgetRoomAlbumsPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.pageState?.get('paginatable') && props.fetchMoreFiles(), [props.pageState?.get('paginatable'), props.fetchMoreFiles]);
        return {
            onScrollToBottom,
        };
    }
}

export default MediaLibraryWidgetRoomAlbumsHooks;