import React from 'react';
import MediaLibraryWidgetRoomAlbumsPresenter from './presenter';
import MediaLibraryWidgetRoomAlbumsComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetRoomAlbumsPresenter.MergedProps) => {
    const output = MediaLibraryWidgetRoomAlbumsPresenter.usePresenter(props);
    return <MediaLibraryWidgetRoomAlbumsComponent {...output} />;
};

const MediaLibraryWidgetRoomAlbums = connect<MediaLibraryWidgetRoomAlbumsPresenter.StateProps, MediaLibraryWidgetRoomAlbumsPresenter.DispatchProps, MediaLibraryWidgetRoomAlbumsPresenter.Input, MediaLibraryWidgetRoomAlbumsPresenter.MergedProps, RootState>(
    MediaLibraryWidgetRoomAlbumsPresenter.mapStateToProps,
    MediaLibraryWidgetRoomAlbumsPresenter.mapDispatchToProps,
    MediaLibraryWidgetRoomAlbumsPresenter.mergeProps,
    MediaLibraryWidgetRoomAlbumsPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetRoomAlbums;