import MediaLibraryWidgetRoomAlbumsHooks from './hooks';
import MediaLibraryWidgetRoomAlbumsStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { RoomWidgetTabsNavigatorStackParams } from '../../navigators/RoomWidgetTabsNavigator/constants';
import { Room } from '@guildion/core';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { MediaLibraryWidgetNavigatorStackParams } from '../../navigators/MediaLibraryWidgetNavigator/constants';
import { RoomAlbumsPageState, RoomFileAction } from '@/presentation/redux/RoomFile/RoomFileReducer';
import { roomFileAlbumsPageStateSelector } from '@/presentation/redux/RoomFile/RoomFileSelector';

namespace MediaLibraryWidgetRoomAlbumsPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState?: RoomAlbumsPageState,
    }
    
    export type DispatchProps = {
        fetchFiles: () => Action,
        fetchMoreFiles: () => Action,
    }
    
    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        parentStackScreen: NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'MediaLibraryWidgetNavigator'> & Partial<StyleProps>
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidgetRoomAlbums'> & Partial<StyleProps>;
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: MediaLibraryWidgetRoomAlbumsStyles,
        appTheme: AppTheme,
        isFocused: boolean,
        room: Room,
        pageState?: RoomAlbumsPageState,
        onScrollToBottom: () => void,
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidgetRoomAlbums'>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            pageState: roomFileAlbumsPageStateSelector(state, ownProps.room.id!),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchFiles: () => RoomFileAction.fetchAlbums.started({ roomId: props.room.id ?? '' }),
            fetchMoreFiles: () => RoomFileAction.fetchMoreAlbums.started({ roomId: props.room.id ?? '' }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.roomFile.pages.albums.toJSON(), next.roomFile.pages.albums.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = MediaLibraryWidgetRoomAlbumsHooks.useStyles(props);
        MediaLibraryWidgetRoomAlbumsHooks.useTabNavigationPush(props);
        const isFocused = MediaLibraryWidgetRoomAlbumsHooks.useFocused(props);
        const {
            onScrollToBottom,
        } = MediaLibraryWidgetRoomAlbumsHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToBottom,
        }
    }
}

export default MediaLibraryWidgetRoomAlbumsPresenter;