import React from 'react';
import MediaLibraryWidgetRoomPresenter from './presenter';
import MediaLibraryWidgetRoomComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetRoomPresenter.MergedProps) => {
    const output = MediaLibraryWidgetRoomPresenter.usePresenter(props);
    return <MediaLibraryWidgetRoomComponent {...output} />;
};

const MediaLibraryWidgetRoom = connect<MediaLibraryWidgetRoomPresenter.StateProps, MediaLibraryWidgetRoomPresenter.DispatchProps, MediaLibraryWidgetRoomPresenter.Input, MediaLibraryWidgetRoomPresenter.MergedProps, RootState>(
    MediaLibraryWidgetRoomPresenter.mapStateToProps,
    MediaLibraryWidgetRoomPresenter.mapDispatchToProps,
    MediaLibraryWidgetRoomPresenter.mergeProps,
    MediaLibraryWidgetRoomPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetRoom;