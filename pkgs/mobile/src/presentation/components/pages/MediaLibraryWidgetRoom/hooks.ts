import React from 'react';
import MediaLibraryWidgetRoomStyles from './styles';
import type MediaLibraryWidgetRoomPresenter from './presenter';
import { RoomWidgetTabsNavigatorRoutes } from '../../navigators/RoomWidgetTabsNavigator/constants';

namespace MediaLibraryWidgetRoomHooks {
    export const useStyles = (props: MediaLibraryWidgetRoomPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetRoomStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: MediaLibraryWidgetRoomPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'MediaLibraryWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: MediaLibraryWidgetRoomPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            isFocused && props.fetchFiles();
        }, [props.room.id, props.room.guildId, isFocused]);
        return isFocused;
    }

    export const useState = (props: MediaLibraryWidgetRoomPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.pageState?.get('paginatable') && props.fetchMoreFiles(), [props.pageState?.get('paginatable'), props.fetchMoreFiles]);
        return {
            onScrollToBottom,
        };
    }
}

export default MediaLibraryWidgetRoomHooks;