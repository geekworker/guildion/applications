import { localizer } from '@/shared/constants/Localizer';
import { Style } from '@/shared/interfaces/Style';
import React from 'react';
import { Animated, View } from 'react-native';
import { Files } from '@guildion/core';
import { MediaLibraryWidgetNavigatorRoutes } from '../../navigators/MediaLibraryWidgetNavigator/constants';
import MediaLibraryWidgetTemplate from '../../templates/MediaLibraryWidgetTemplate';
import MediaLibraryWidgetRoomPresenter from './presenter';

const MediaLibraryWidgetRoomComponent = ({ styles, appTheme, animatedStyle, isFocused, pageState, room, onScrollToBottom, navigation }: MediaLibraryWidgetRoomPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.wrapper, ...animatedStyle?.toStyleObject() }}>
            <MediaLibraryWidgetTemplate
                appTheme={appTheme}
                files={pageState?.get('files') ?? new Files([])}
                loading={pageState?.get('loading')}
                loadingMore={pageState?.get('loadingMoreStatus')}
                room={room}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
                isFocused={isFocused}
                showAlbumButton
                showPlaylistButton
                showHeader
                showRoomHeader
                showSearchBar
                title={room.displayName}
                sectionTitle={localizer.dictionary.guild.files.index.listTitle}
                onScrollToBottom={onScrollToBottom}
                onPressBack={() => navigation.goBack()}
                onPressAlbums={() => navigation.navigate(MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoomAlbums, {})}
                onPressPlaylists={() => navigation.navigate(MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoomPlaylists, {})}
            />
        </Animated.View>
    );
};

export default React.memo(MediaLibraryWidgetRoomComponent, MediaLibraryWidgetRoomPresenter.outputAreEqual);