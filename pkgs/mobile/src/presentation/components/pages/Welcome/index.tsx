import React from 'react';
import WelcomePresenter from './presenter';
import WelcomeComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: WelcomePresenter.MergedProps) => {
    const output = WelcomePresenter.usePresenter(props);
    return <WelcomeComponent {...output} />;
};

const Welcome = connect<WelcomePresenter.StateProps, WelcomePresenter.DispatchProps, WelcomePresenter.Input, WelcomePresenter.MergedProps, RootState>(
    WelcomePresenter.mapStateToProps,
    WelcomePresenter.mapDispatchToProps,
    WelcomePresenter.mergeProps,
    WelcomePresenter.connectOptions,
)(Container);

export default Welcome;

