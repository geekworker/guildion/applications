import React from 'react';
import { View } from 'react-native'
import WelcomePresenter from './presenter';
import { SafeAreaView } from 'react-native-safe-area-context';
import WelcomeTemplate from '../../templates/WelcomeTemplate';
import { Style } from '@/shared/interfaces/Style';

const WelcomeComponent = ({ styles, innerHeight, innerWidth, appTheme, onClickLogin, onClickRegister, isAppeared }: WelcomePresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <SafeAreaView style={styles.safearea} edges={['top', 'right', 'left']}>
                <WelcomeTemplate
                    style={new Style({
                        width: innerWidth,
                        height: innerHeight,
                    })}
                    appTheme={appTheme}
                    onClickLogin={onClickLogin}
                    onClickRegister={onClickRegister}
                    isAppeared={isAppeared}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(WelcomeComponent, WelcomePresenter.outputAreEqual);