import WelcomeHooks from "./hooks";
import WelcomeStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { WelcomeNavigatorRoutes, WelcomeNavigatorStackParams } from '@/presentation/components/navigators/WelcomeNavigator/constants';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { AppTheme } from "@/shared/constants/AppTheme";
import { useInsets } from "@/shared/hooks/useInsets";
import StyleProps from "@/shared/interfaces/StyleProps";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace WelcomePresenter {
    export type StackParams = {
    }

    export type StateProps = {
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<WelcomeNavigatorStackParams, 'Welcome'> & StyleProps;
    
    export type Output = {
        styles: WelcomeStyles,
        appTheme?: AppTheme,
        innerWidth: number,
        innerHeight: number,
        onClickRegister: () => void,
        onClickLogin: () => void,
        isAppeared: boolean,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            true
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = WelcomeHooks.useStyles(props);
        const insets = useInsets({ width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        return {
            ...props,
            ...insets,
            onClickLogin: () => { onClickLogin(props) },
            onClickRegister: () => { onClickRegister(props) },
            styles,
            isAppeared: props.navigation.isFocused(),
        }
    }

    export const onClickRegister = (props: MergedProps) => {
        props.navigation.push(WelcomeNavigatorRoutes.AccountNew, {})
    };

    export const onClickLogin = (props: MergedProps) => {
        props.navigation.push(WelcomeNavigatorRoutes.AccountAuthenticate, {})
    };
}

export default WelcomePresenter;