import { useMemo } from "react";
import WelcomeStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace WelcomeHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = useMemo(() => new WelcomeStyles({ ...props }), [props]);
        return styles;
    }
}

export default WelcomeHooks