import React from 'react';
import RoomPostsWidgetShowPresenter from './presenter';
import RoomPostsWidgetShowComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomPostsWidgetShowPresenter.MergedProps) => {
    const output = RoomPostsWidgetShowPresenter.usePresenter(props);
    return <RoomPostsWidgetShowComponent {...output} />;
};

const RoomPostsWidgetShow = connect<RoomPostsWidgetShowPresenter.StateProps, RoomPostsWidgetShowPresenter.DispatchProps, RoomPostsWidgetShowPresenter.Input, RoomPostsWidgetShowPresenter.MergedProps, RootState>(
    RoomPostsWidgetShowPresenter.mapStateToProps,
    RoomPostsWidgetShowPresenter.mapDispatchToProps,
    RoomPostsWidgetShowPresenter.mergeProps,
    RoomPostsWidgetShowPresenter.connectOptions,
)(Container);

export default RoomPostsWidgetShow;