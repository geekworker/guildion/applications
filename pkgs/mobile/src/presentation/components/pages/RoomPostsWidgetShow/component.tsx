import { Style } from '@/shared/interfaces/Style';
import React from 'react';
import { View } from 'react-native';
import { Post, Posts } from '@guildion/core';
import PostShowTemplate from '../../templates/PostShowTemplate';
import RoomPostsWidgetShowPresenter from './presenter';

const RoomPostsWidgetShowComponent = ({ styles, appTheme, animatedStyle, pageState, childrenPageState, roomPageState, room, isFocused, navigation, onScrollToBottom }: RoomPostsWidgetShowPresenter.Output) => {
    return (
        <View style={styles.wrapper}>
            <PostShowTemplate
                appTheme={appTheme}
                post={pageState?.get('post') ?? new Post()}
                posts={childrenPageState?.get('posts') ?? new Posts([])}
                loadingMore={childrenPageState?.get('loadingMoreStatus')}
                onScrollToBottom={onScrollToBottom}
                currentMember={roomPageState?.get('room').records.currentMember}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
                onPressBack={() => navigation.goBack()}
            />
        </View>
    );
};

export default React.memo(RoomPostsWidgetShowComponent, RoomPostsWidgetShowPresenter.outputAreEqual);