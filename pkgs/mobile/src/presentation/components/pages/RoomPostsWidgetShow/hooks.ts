import React from 'react';
import RoomPostsWidgetShowStyles from './styles';
import type RoomPostsWidgetShowPresenter from './presenter';
import { RoomWidgetTabsNavigatorRoutes } from '../../navigators/RoomWidgetTabsNavigator/constants';

namespace RoomPostsWidgetShowHooks {
    export const useStyles = (props: RoomPostsWidgetShowPresenter.MergedProps) => {
        const styles = React.useMemo(() => new RoomPostsWidgetShowStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: RoomPostsWidgetShowPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'RoomPostsWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: RoomPostsWidgetShowPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.RoomPostsWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            if (isFocused) {
                props.fetch();
                props.fetchShow();
            };
        }, [props.route.params.postId, isFocused]);
        return isFocused;
    }

    export const useState = (props: RoomPostsWidgetShowPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.childrenPageState?.get('paginatable') && props.fetchMore(), [props.childrenPageState?.get('paginatable'), props.fetchMore]);
        return {
            onScrollToBottom,
        };
    }
}

export default RoomPostsWidgetShowHooks;