import RoomPostsWidgetShowHooks from "./hooks";
import RoomPostsWidgetShowStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { Room } from "@guildion/core";
import { RoomPostsWidgetNavigatorStackParams } from "../../navigators/RoomPostsWidgetNavigator/constants";
import { PostAction, PostPageState, RoomPostsPageState } from "@/presentation/redux/Post/PostReducer";
import { postShowPageStateSelector, postsIndexPageStateSelector } from "@/presentation/redux/Post/PostSelector";
import { roomShowPageStateSelector } from "@/presentation/redux/Room/RoomSelector";
import { RoomPageState } from "@/presentation/redux/Room/RoomReducer";

namespace RoomPostsWidgetShowPresenter {
    export type StackParams = {
        postId: string,
    }

    export type StateProps = {
        pageState?: PostPageState,
        childrenPageState?: RoomPostsPageState,
        roomPageState?: RoomPageState,
    }
    
    export type DispatchProps = {
        fetchShow: () => Action,
        fetch: () => Action,
        fetchMore: () => Action,
    }

    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        parentStackScreen: NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'RoomPostsWidgetNavigator'> & Partial<StyleProps>
    } & NativeStackScreenProps<RoomPostsWidgetNavigatorStackParams, 'RoomPostsWidgetShow'> & Partial<StyleProps>;

    export type Output = {
        styles: RoomPostsWidgetShowStyles,
        appTheme: AppTheme,
        isFocused: boolean,
        room: Room,
        pageState?: PostPageState,
        childrenPageState?: RoomPostsPageState,
        roomPageState?: RoomPageState,
        onScrollToBottom: () => void,
    } & NativeStackScreenProps<RoomPostsWidgetNavigatorStackParams, 'RoomPostsWidgetShow'> & Partial<StyleProps>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            childrenPageState: postsIndexPageStateSelector(state, ownProps.room.id!, ownProps.route.params.postId),
            pageState: postShowPageStateSelector(state, ownProps.route.params.postId),
            roomPageState: roomShowPageStateSelector(state, ownProps.room.id!),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchShow: () => PostAction.fetchShow.started({ id: props.route.params.postId }),
            fetch: () => PostAction.fetchIndex.started({ roomId: props.room.id ?? '', postId: props.route.params.postId, }),
            fetchMore: () => PostAction.fetchIndex.started({ roomId: props.room.id ?? '', postId: props.route.params.postId, }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.post.pages.index.toJSON(), next.post.pages.index.toJSON()) && compare(prev.post.pages.show.toJSON(), next.post.pages.show.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
        
    export function usePresenter(props: MergedProps): Output {
        const styles = RoomPostsWidgetShowHooks.useStyles(props);
        RoomPostsWidgetShowHooks.useTabNavigationPush(props);
        const isFocused = RoomPostsWidgetShowHooks.useFocused(props);
        const {
            onScrollToBottom,
        } = RoomPostsWidgetShowHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToBottom,
        }
    }
}

export default RoomPostsWidgetShowPresenter;