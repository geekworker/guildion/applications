import { AppTheme } from "@/shared/constants/AppTheme";
import { Action } from "redux";
import AccountNewHooks from "./hooks";
import AccountNewStyles from './styles';
import { bindActionCreators } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { WelcomeNavigatorRoutes, WelcomeNavigatorStackParams } from '@/presentation/components/navigators/WelcomeNavigator/constants';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { File, User } from "@guildion/core";
import { accountUsernameErrorSelector } from "@/presentation/redux/App/AppSelector";
import { AccountAction, AccountNewPageState } from "@/presentation/redux/Account/AccountReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { useInsets } from "@/shared/hooks/useInsets";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { randomUserProfileSelector } from "@/presentation/redux/File/FileSelector";

namespace AccountNewPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState: AccountNewPageState,
        usernameError?: string,
        randomProfile: File,
    }
    
    export type DispatchProps = {
        register: (user: User) => Action,
        setUser: (user: User) => Action,
        didSuccess: () => Action,
    }
    
    export type Input = NativeStackScreenProps<WelcomeNavigatorStackParams, 'AccountNew'> & StyleProps;
    
    export type Output = {
        styles: AccountNewStyles,
        appTheme?: AppTheme,
        innerWidth: number,
        innerHeight: number,
        usernameError?: string,
        checkedTerms: boolean,
        checkedPrivacyPolicy: boolean,
        pageState: AccountNewPageState,
        sendable: boolean,
        user: User,
        onSubmit: () => void,
        onChangeProfile: (profile: File) => void,
        onChangeUsername: (username?: string) => void,
        onChangeTermsCheck: (value: boolean) => void,
        onChangePrivacyPolicyCheck: (value: boolean) => void,
    };

    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            pageState: state.account.pages.new,
            usernameError: accountUsernameErrorSelector(state),
            randomProfile: randomUserProfileSelector(state),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            register: (user: User) => AccountAction.register.started({ user }),
            setUser: (user: User) => AccountAction.setNewState({ user }),
            didSuccess: () => AccountAction.setNewState({ success: false }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.pages.new == next.account.pages.new &&
            prev.app.errors == next.app.errors
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = AccountNewHooks.useStyles(props);
        const insets = useInsets({ width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        AccountNewHooks.useSuccess(() => props.navigation.push(WelcomeNavigatorRoutes.AccountLoading, {}), props);
        const {
            checkedTerms,
            checkTerms,
            checkedPrivacyPolicy,
            checkPrivacyPolicy,
            user,
            sendable,
            onChangeUsername,
            onChangeProfile,
            onSubmit,
        } = AccountNewHooks.useState(props);
        return {
            ...props,
            ...insets,
            styles,
            sendable,
            user,
            onSubmit,
            onChangeUsername,
            onChangeProfile,
            checkedTerms,
            checkedPrivacyPolicy,
            onChangeTermsCheck: checkTerms,
            onChangePrivacyPolicyCheck: checkPrivacyPolicy,
        }
    }
}

export default AccountNewPresenter;