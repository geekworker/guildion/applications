import React from "react";
import AccountNewStyles from './styles';
import { File, User } from "@guildion/core";
import StyleProps from "@/shared/interfaces/StyleProps";
import type AccountNewPresenter from "./presenter";

namespace AccountNewHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new AccountNewStyles({ ...props }), [props]);
        return styles;
    }

    export const useSuccess = (onSuccess: () => void, { pageState, didSuccess }: AccountNewPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!pageState.get('success')) {
                onSuccess();
                didSuccess();
            };
        },[pageState.get('success')])
    }

    export const useState = (props: AccountNewPresenter.MergedProps) => {
        const [checkedTerms, checkTerms] = React.useState(false);
        const [checkedPrivacyPolicy, checkPrivacyPolicy] = React.useState(false);
        const [user, setUser] = React.useState<User>(props.pageState.get('user'));
        React.useEffect(() => {
            props.pageState.get('user') != user && setUser(props.pageState.get('user'))
        }, [props.pageState.get('user')]);
        const sendable = React.useMemo(() => 
            !!user.username && user.username.length > 0 && checkedTerms && checkedPrivacyPolicy,
            [user.username, checkedTerms, checkedPrivacyPolicy]
        );
        const onChangeUsername = React.useCallback(
            (username?: string) => {
                setUser(user.set('username', username ?? ''));
            },
            [user, setUser]
        );
        const onChangeProfile = React.useCallback(
            (profile: File) => {
                setUser(user.setRecord('profile', profile).set('profileId', profile.id));
            },
            [user, setUser]
        );
        React.useEffect(() => {
            if (!user.profileId) {
                onChangeProfile(props.randomProfile);
            }
        }, [props.randomProfile])
        const onSubmit = React.useCallback(
            () => {
                props.setUser(user);
                props.register(user);
            },
            [props.register, user]
        );
        React.useEffect(() => {
            props.setUser(user);
        });
        return {
            checkedTerms,
            checkTerms,
            checkedPrivacyPolicy,
            checkPrivacyPolicy,
            user,
            setUser,
            sendable,
            onChangeUsername,
            onChangeProfile,
            onSubmit,
        };
    }
}

export default AccountNewHooks