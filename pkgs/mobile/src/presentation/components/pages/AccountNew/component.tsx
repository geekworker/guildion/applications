import React from 'react';
import { View } from 'react-native'
import AccountNewPresenter from './presenter';
import AccountNewTemplate from '../../templates/AccountNewTemplate';
import { Style } from '@/shared/interfaces/Style';

const AccountNewComponent = ({ styles, appTheme, innerWidth, innerHeight, usernameError, sendable, pageState, onSubmit, onChangeProfile, onChangeUsername, user, checkedPrivacyPolicy, checkedTerms, onChangePrivacyPolicyCheck, onChangeTermsCheck }: AccountNewPresenter.Output) => {
    return (
        <View style={styles.wrapper} >
            <AccountNewTemplate
                style={new Style({
                    width: innerWidth,
                    height: innerHeight,
                })}
                appTheme={appTheme}
                sendable={sendable}
                onSubmit={onSubmit}
                username={user.username}
                url={user.profile?.url ?? ''}
                usernameError={usernameError}
                onChangeUsername={onChangeUsername}
                loading={pageState.get('loading')}
                onChangePrivacyPolicy={onChangePrivacyPolicyCheck}
                onChangeTerms={onChangeTermsCheck}
                isAcceptPrivacyPolicy={checkedPrivacyPolicy}
                isAcceptTerms={checkedTerms}
            />
        </View>
    );
};

export default React.memo(AccountNewComponent, AccountNewPresenter.outputAreEqual);