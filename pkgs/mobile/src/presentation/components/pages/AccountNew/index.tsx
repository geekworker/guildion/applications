import React from 'react';
import AccountNewPresenter from './presenter';
import AccountNewComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: AccountNewPresenter.MergedProps) => {
    const output = AccountNewPresenter.usePresenter(props);
    return <AccountNewComponent {...output} />;
};

const AccountNew = connect<AccountNewPresenter.StateProps, AccountNewPresenter.DispatchProps, AccountNewPresenter.Input, AccountNewPresenter.MergedProps, RootState>(
    AccountNewPresenter.mapStateToProps,
    AccountNewPresenter.mapDispatchToProps,
    AccountNewPresenter.mergeProps,
    AccountNewPresenter.connectOptions,
)(Container);

export default AccountNew;
