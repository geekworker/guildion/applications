import TextChatWidgetHooks from "./hooks";
import TextChatWidgetStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomWidgetTabsNavigatorStackParams } from "../../navigators/RoomWidgetTabsNavigator/constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { MessageAction, MessagesPageState } from "@/presentation/redux/Message/MessageReducer";
import { roomMessagesPageStateSelector } from "@/presentation/redux/Message/MessageSelector";
import { Message, Room } from "@guildion/core";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";

namespace TextChatWidgetPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState?: MessagesPageState,
    }
    
    export type DispatchProps = {
        fetchMessages: () => Action,
        incrementMessages: () => Action,
        decrementMessages: () => Action,
        create: (message: Message) => Action,
    }
    
    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
    } & NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'TextChatWidget'> & Partial<StyleProps>;
    
    export type Output = {
        styles: TextChatWidgetStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        isFocused: boolean,
        pageState?: MessagesPageState,
        onScrollToTop: () => void,
        onScrollToBottom: () => void,
        onSubmit: (message: Message) => void,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, props) => {
        return {
            pageState: roomMessagesPageStateSelector(state, props.room.id!),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchMessages: () => MessageAction.fetchIndex.started({ roomId: props.room.id ?? '' }),
            incrementMessages: () => MessageAction.incrementIndex.started({ roomId: props.room.id ?? '' }),
            decrementMessages: () => MessageAction.decrementIndex.started({ roomId: props.room.id ?? '' }),
            create: (message: Message) => MessageAction.create.started({ message }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.message.pages.index.toJSON(), next.message.pages.index.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = TextChatWidgetHooks.useStyles(props);
        TextChatWidgetHooks.useTabNavigationPush(props);
        const isFocused = TextChatWidgetHooks.useFocused(props);
        const {
            onScrollToTop,
            onScrollToBottom,
            onSubmit,
        } = TextChatWidgetHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToTop,
            onScrollToBottom,
            onSubmit,
        }
    }
}

export default TextChatWidgetPresenter;