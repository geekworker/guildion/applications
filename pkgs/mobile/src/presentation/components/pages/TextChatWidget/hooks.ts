import React from "react";
import TextChatWidgetStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { RoomWidgetTabsNavigatorStackParams, RoomWidgetTabsNavigatorRoutes } from "../../navigators/RoomWidgetTabsNavigator/constants";
import type TextChatWidgetPresenter from "./presenter";
import { useNavigation } from "@react-navigation/core";
import { Message } from "@guildion/core";

namespace TextChatWidgetHooks {

    export const useStyles = (props: Partial<StyleProps>) => {
        const styles = React.useMemo(() => new TextChatWidgetStyles({ ...props }), [
            props,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: { tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams }) => {
        const navigation = useNavigation<TextChatWidgetPresenter.Input['navigation']>();
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'TextChatWidget') {
                navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: TextChatWidgetPresenter.MergedProps) => {
        const styles = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.TextChatWidget,
            [props.tabInitialRouteName]
        );
        return styles;
    }

    export const useState = (props: TextChatWidgetPresenter.MergedProps) => {

        React.useEffect(() => {
            props.pageState && props.pageState.get('messages').size > 0 ?
                props.incrementMessages() :
                props.fetchMessages();
        }, [props.room.id]);

        const onScrollToTop = React.useCallback(() => {
            props.decrementMessages();
        }, [props.decrementMessages]);

        const onScrollToBottom = React.useCallback(() => {
            props.incrementMessages();
        }, [props.incrementMessages]);

        const onSubmit = React.useCallback((message: Message) => {
            if (message.id && (message.body.length > 0 || (message.files?.length ?? 0) > 0 || (message.records.files?.size ?? 0) > 0)) {
                props.create(message);
            }
        }, [props.create]);

        return {
            onScrollToTop,
            onScrollToBottom,
            onSubmit,
        };
    }
}

export default TextChatWidgetHooks