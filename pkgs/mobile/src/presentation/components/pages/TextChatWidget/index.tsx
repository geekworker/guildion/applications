import React from 'react';
import TextChatWidgetPresenter from './presenter';
import TextChatWidgetComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: TextChatWidgetPresenter.MergedProps) => {
    const output = TextChatWidgetPresenter.usePresenter(props);
    return <TextChatWidgetComponent {...output} />;
};

const TextChatWidget = connect<TextChatWidgetPresenter.StateProps, TextChatWidgetPresenter.DispatchProps, TextChatWidgetPresenter.Input, TextChatWidgetPresenter.MergedProps, RootState>(
    TextChatWidgetPresenter.mapStateToProps,
    TextChatWidgetPresenter.mapDispatchToProps,
    TextChatWidgetPresenter.mergeProps,
    TextChatWidgetPresenter.connectOptions,
)(Container);

export default TextChatWidget;