import { Style } from '@/shared/interfaces/Style';
import { Message, Messages } from '@guildion/core';
import React from 'react';
import { Animated } from 'react-native';
import TextChatWidgetTemplate from '../../templates/TextChatWidgetTemplate';
import TextChatWidgetPresenter from './presenter';

const TextChatWidgetComponent: React.FC<TextChatWidgetPresenter.Output> = ({ appTheme, animatedStyle, styles, isFocused, pageState, onScrollToBottom, onScrollToTop, onSubmit }) => {
    return (
        <Animated.View style={{ ...styles.wrapper, ...animatedStyle?.toStyleObject() }}>
            <TextChatWidgetTemplate
                appTheme={appTheme}
                animatedStyle={animatedStyle}
                message={pageState?.get('newMessage') ?? new Message()}
                messages={pageState?.get('messages') ?? new Messages([])}
                loadingMore={pageState?.get('loadingMoreStatus')}
                loading={!pageState || pageState.get('loading')}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                })}
                isFocused={isFocused}
                onScrollToBottom={onScrollToBottom}
                onScrollToTop={onScrollToTop}
                incrementable={pageState?.get('incrementable')}
                decrementable={pageState?.get('decrementable')}
                submitting={pageState?.get('submitting')}
                onSubmit={onSubmit}
            />
        </Animated.View>
    );
};

export default React.memo(TextChatWidgetComponent, TextChatWidgetPresenter.outputAreEqual);