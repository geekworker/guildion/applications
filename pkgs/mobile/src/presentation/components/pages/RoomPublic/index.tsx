import React from 'react';
import RoomPublicPresenter from './presenter';
import RoomPublicComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomPublicPresenter.MergedProps) => {
    const output = RoomPublicPresenter.usePresenter(props);
    return <RoomPublicComponent {...output} />;
};

const RoomPublic = connect<RoomPublicPresenter.StateProps, RoomPublicPresenter.DispatchProps, RoomPublicPresenter.Input, RoomPublicPresenter.MergedProps, RootState>(
    RoomPublicPresenter.mapStateToProps,
    RoomPublicPresenter.mapDispatchToProps,
    RoomPublicPresenter.mergeProps,
    RoomPublicPresenter.connectOptions,
)(Container);

export default RoomPublic;