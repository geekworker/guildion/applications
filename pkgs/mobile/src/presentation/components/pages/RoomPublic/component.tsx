import React from 'react';
import { Animated } from 'react-native';
import { Posts, Room } from '@guildion/core';
import RoomPublicTemplate from '../../templates/RoomPublicTemplate';
import RoomPublicPresenter from './presenter';
import { Style } from '@/shared/interfaces/Style';

const RoomPublicComponent = ({ styles, appTheme, animatedStyle, roomPageState, innerWidth, innerHeight }: RoomPublicPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.wrapper, ...animatedStyle?.toStyleObject() }}>
            <RoomPublicTemplate
                appTheme={appTheme}
                room={roomPageState?.get('room') ?? new Room()}
                posts={new Posts([])}
                loading={roomPageState?.get('loading')}
                style={new Style({
                    width: innerWidth,
                    height: innerHeight,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
            />
        </Animated.View>
    );
};

export default React.memo(RoomPublicComponent, RoomPublicPresenter.outputAreEqual);