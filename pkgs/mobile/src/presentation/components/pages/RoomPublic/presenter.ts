import RoomPublicHooks from './hooks';
import RoomPublicStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { ExploreNavigatorStackParams } from '../../navigators/ExploreNavigator/constants';
import { RoomAction, RoomPageState } from '@/presentation/redux/Room/RoomReducer';
import { roomShowPageStateSelector } from '@/presentation/redux/Room/RoomSelector';
import { useInsets } from '@/shared/hooks/useInsets';
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';
import { useSplitView } from '@/shared/hooks/useSplitView';
import { RootNavigatorStackParams } from '../../navigators/RootNavigator/constants';

namespace RoomPublicPresenter {
    export type StackParams = {
        roomId: string,
    }

    export type StateProps = {
        roomPageState?: RoomPageState,
        roomId: string,
    }
    
    export type DispatchProps = {
        fetchRoom: (id: string) => Action,
    }

    export type ExploreScreenProps = NativeStackScreenProps<ExploreNavigatorStackParams, 'RoomPublic'>;
    export type ModalScreenProps = NativeStackScreenProps<RootNavigatorStackParams, 'RoomPublic'>;
    export type Input = StyleProps & {
        exploreScreenProps?: ExploreScreenProps,
        modalScreenProps?: ModalScreenProps,
    };
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: RoomPublicStyles,
        appTheme: AppTheme,
        roomId: string,
        roomPageState?: RoomPageState,
        innerWidth: number,
        innerHeight: number,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        const roomId = ownProps.modalScreenProps?.route.params.roomId ?? ownProps.exploreScreenProps?.route.params.roomId ?? '';
        return {
            roomPageState: roomShowPageStateSelector(state, roomId),
            roomId,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchRoom: (id: string) => RoomAction.fetchShow.started({ id, isPublic: true }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.room.pages.show.toJSON(), next.room.pages.show.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = RoomPublicHooks.useStyles(props);
        const {} = RoomPublicHooks.useState(props);
        RoomPublicHooks.useViewWillAppear(props);
        const insets = useInsets({ bottomExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        const tabbarHeight = useBottomTabBarHeight();
        const shouldSplit = useSplitView();
        return {
            ...props,
            ...insets,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            innerHeight: shouldSplit ? insets.innerHeight : insets.innerHeight - tabbarHeight,
        }
    }
}

export default RoomPublicPresenter;