import React from 'react';
import RoomPublicStyles from './styles';
import type RoomPublicPresenter from './presenter';

namespace RoomPublicHooks {
    export const useStyles = (props: RoomPublicPresenter.MergedProps) => {
        const styles = React.useMemo(() => new RoomPublicStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useViewWillAppear = (props: RoomPublicPresenter.MergedProps) => {
        React.useEffect(() => {
            if ((
                props.exploreScreenProps?.navigation?.isFocused() ||
                props.modalScreenProps?.navigation?.isFocused()
            ) && !!props.roomId) {
                props.fetchRoom(props.roomId);
            };
        }, [props.exploreScreenProps?.navigation, props.modalScreenProps?.navigation, props.roomId]);
    }

    export const useState = (props: RoomPublicPresenter.MergedProps) => {
        return {};
    }
}

export default RoomPublicHooks;