import { localizer } from '@/shared/constants/Localizer';
import { Style } from '@/shared/interfaces/Style';
import React from 'react';
import { Animated, View } from 'react-native';
import { Files } from '@guildion/core';
import MediaLibraryWidgetTemplate from '../../templates/MediaLibraryWidgetTemplate';
import MediaLibraryWidgetRoomPlaylistsPresenter from './presenter';

const MediaLibraryWidgetRoomPlaylistsComponent = ({ styles, appTheme, animatedStyle, isFocused, pageState, room, onScrollToBottom, navigation }: MediaLibraryWidgetRoomPlaylistsPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.wrapper, ...animatedStyle?.toStyleObject() }}>
            <MediaLibraryWidgetTemplate
                appTheme={appTheme}
                files={pageState?.get('files') ?? new Files([])}
                loading={pageState?.get('loading')}
                loadingMore={pageState?.get('loadingMoreStatus')}
                room={room}
                style={new Style({
                    width: styles.wrapper.width,
                    height: styles.wrapper.height,
                    borderTopWidth: 1,
                    borderColor: appTheme.background.subm1,
                })}
                isFocused={isFocused}
                showHeader
                title={room.displayName}
                sectionTitle={localizer.dictionary.g.db.playlists}
                onScrollToBottom={onScrollToBottom}
                onPressBack={() => navigation.goBack()}
            />
        </Animated.View>
    );
};

export default React.memo(MediaLibraryWidgetRoomPlaylistsComponent, MediaLibraryWidgetRoomPlaylistsPresenter.outputAreEqual);