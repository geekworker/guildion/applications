import MediaLibraryWidgetRoomPlaylistsHooks from './hooks';
import MediaLibraryWidgetRoomPlaylistsStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import { RootState } from '@/presentation/redux/RootReducer';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from 'react-redux';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { RoomWidgetTabsNavigatorStackParams } from '../../navigators/RoomWidgetTabsNavigator/constants';
import { Room } from '@guildion/core';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { MediaLibraryWidgetNavigatorStackParams } from '../../navigators/MediaLibraryWidgetNavigator/constants';
import { RoomPlaylistsPageState, RoomFileAction } from '@/presentation/redux/RoomFile/RoomFileReducer';
import { roomFilePlaylistsPageStateSelector } from '@/presentation/redux/RoomFile/RoomFileSelector';

namespace MediaLibraryWidgetRoomPlaylistsPresenter {
    export type StackParams = {
    }

    export type StateProps = {
        pageState?: RoomPlaylistsPageState,
    }
    
    export type DispatchProps = {
        fetchFiles: () => Action,
        fetchMoreFiles: () => Action,
    }
    
    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        parentStackScreen: NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'MediaLibraryWidgetNavigator'> & Partial<StyleProps>
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidgetRoomPlaylists'> & Partial<StyleProps>;
    
    export type Output = {
        animatedStyle?: AnimatedStyle,
        styles: MediaLibraryWidgetRoomPlaylistsStyles,
        appTheme: AppTheme,
        isFocused: boolean,
        room: Room,
        pageState?: RoomPlaylistsPageState,
        onScrollToBottom: () => void,
    } & NativeStackScreenProps<MediaLibraryWidgetNavigatorStackParams, 'MediaLibraryWidgetRoomPlaylists'>;
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            pageState: roomFilePlaylistsPageStateSelector(state, ownProps.room.id!),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchFiles: () => RoomFileAction.fetchPlaylists.started({ roomId: props.room.id ?? '' }),
            fetchMoreFiles: () => RoomFileAction.fetchMorePlaylists.started({ roomId: props.room.id ?? '' }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            compare(prev.roomFile.pages.playlists.toJSON(), next.roomFile.pages.playlists.toJSON())
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = MediaLibraryWidgetRoomPlaylistsHooks.useStyles(props);
        MediaLibraryWidgetRoomPlaylistsHooks.useTabNavigationPush(props);
        const isFocused = MediaLibraryWidgetRoomPlaylistsHooks.useFocused(props);
        const {
            onScrollToBottom,
        } = MediaLibraryWidgetRoomPlaylistsHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            isFocused,
            onScrollToBottom,
        }
    }
}

export default MediaLibraryWidgetRoomPlaylistsPresenter;