import React from 'react';
import MediaLibraryWidgetRoomPlaylistsPresenter from './presenter';
import MediaLibraryWidgetRoomPlaylistsComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: MediaLibraryWidgetRoomPlaylistsPresenter.MergedProps) => {
    const output = MediaLibraryWidgetRoomPlaylistsPresenter.usePresenter(props);
    return <MediaLibraryWidgetRoomPlaylistsComponent {...output} />;
};

const MediaLibraryWidgetRoomPlaylists = connect<MediaLibraryWidgetRoomPlaylistsPresenter.StateProps, MediaLibraryWidgetRoomPlaylistsPresenter.DispatchProps, MediaLibraryWidgetRoomPlaylistsPresenter.Input, MediaLibraryWidgetRoomPlaylistsPresenter.MergedProps, RootState>(
    MediaLibraryWidgetRoomPlaylistsPresenter.mapStateToProps,
    MediaLibraryWidgetRoomPlaylistsPresenter.mapDispatchToProps,
    MediaLibraryWidgetRoomPlaylistsPresenter.mergeProps,
    MediaLibraryWidgetRoomPlaylistsPresenter.connectOptions,
)(Container);

export default MediaLibraryWidgetRoomPlaylists;