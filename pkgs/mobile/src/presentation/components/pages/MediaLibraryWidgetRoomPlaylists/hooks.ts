import React from 'react';
import MediaLibraryWidgetRoomPlaylistsStyles from './styles';
import type MediaLibraryWidgetRoomPlaylistsPresenter from './presenter';
import { RoomWidgetTabsNavigatorRoutes } from '../../navigators/RoomWidgetTabsNavigator/constants';

namespace MediaLibraryWidgetRoomPlaylistsHooks {
    export const useStyles = (props: MediaLibraryWidgetRoomPlaylistsPresenter.MergedProps) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetRoomPlaylistsStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: MediaLibraryWidgetRoomPlaylistsPresenter.MergedProps) => {
        React.useEffect(() => {
            if (!!props.tabInitialRouteName && props.tabInitialRouteName != 'MediaLibraryWidgetNavigator') {
                props.parentStackScreen.navigation.navigate(props.tabInitialRouteName, {});
            };
        }, [props.tabInitialRouteName]);
    }

    export const useFocused = (props: MediaLibraryWidgetRoomPlaylistsPresenter.MergedProps) => {
        const isFocused = React.useMemo(() =>
            props.tabInitialRouteName == RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator,
            [props.tabInitialRouteName]
        );
        React.useEffect(() => {
            isFocused && props.fetchFiles();
        }, [props.room.id, props.room.guildId, isFocused]);
        return isFocused;
    }

    export const useState = (props: MediaLibraryWidgetRoomPlaylistsPresenter.MergedProps) => {
        const onScrollToBottom = React.useCallback(() => props.pageState?.get('paginatable') && props.fetchMoreFiles(), [props.pageState?.get('paginatable'), props.fetchMoreFiles]);
        return {
            onScrollToBottom,
        };
    }
}

export default MediaLibraryWidgetRoomPlaylistsHooks;