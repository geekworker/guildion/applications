import React from "react";
import RoomShowStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type RoomShowPresenter from "./presenter";
import RootRedux from "@/infrastructure/Root/redux";

namespace RoomShowHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomShowStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useViewWillAppear = (props: RoomShowPresenter.MergedProps) => {
        const roomId = props.modalScreenProps?.route?.params?.id ?? props.splitScreenProps?.route?.params?.id;
        const guildId = props.modalScreenProps?.route?.params?.guildId ?? props.splitScreenProps?.route?.params?.guildId;
        React.useEffect(() => {
            if ((
                props.splitScreenProps?.navigation?.isFocused() || props.modalScreenProps?.navigation?.isFocused()
            ) && !!guildId && !!roomId) {
                // props.fetchGuild(guildId);
                props.fetchRoom(roomId);
                // props.fetchLogs(roomId);
                props.fetchConnectingMembers(roomId);
            };
        }, [props.splitScreenProps?.navigation, props.modalScreenProps?.navigation, roomId, guildId]);
    }

    export const useObserveNavigation = (navigation: RoomShowPresenter.SplitScreenProps['navigation']) => {
        const context = React.useContext(RootRedux.Context);
        React.useEffect(() => {
            context.dispatch(RootRedux.Action.setCurrentRoomSplitSceneNavigation(navigation));
        },[navigation]);
    }
}

export default RoomShowHooks