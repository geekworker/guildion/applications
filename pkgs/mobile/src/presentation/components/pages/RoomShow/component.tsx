import React from 'react';
import RoomShowPresenter from './presenter';
import { View } from "react-native";
import { SafeAreaView } from 'react-native-safe-area-context';
import RoomShowTemplate from '../../templates/RoomShowTemplate';
import { Guild, Members, Room } from '@guildion/core';
import { Style } from '@/shared/interfaces/Style';

const RoomShowComponent: React.FC<RoomShowPresenter.Output> = ({ styles, appTheme, modalScreenProps, splitScreenProps, guild, roomPageState, insets, onPressGuild, connectingMembersPageState, roomId }) => {
    return (
        <View style={styles.wrapper}>
            <SafeAreaView style={styles.safearea} edges={['top', 'right', 'left']}>
                <RoomShowTemplate
                    guild={guild ?? new Guild()}
                    room={roomPageState?.get('room') ?? new Room({ id: roomId })}
                    connectingMembers={connectingMembersPageState?.get('members') ?? new Members([])}
                    style={new Style({
                        width: insets.innerWidth,
                        height: insets.innerHeight,
                    })}
                    appTheme={appTheme}
                    showSyncVision={true}
                    showMembers={true}
                    onPressGuild={onPressGuild}
                    loading={roomPageState?.get('loading') ?? true}
                    isFocused={(modalScreenProps ?? splitScreenProps)?.navigation.isFocused() ?? false}
                />
            </SafeAreaView>
        </View>
    );
};

export default React.memo(RoomShowComponent, RoomShowPresenter.outputAreEqual);