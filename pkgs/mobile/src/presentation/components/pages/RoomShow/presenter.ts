import RoomShowHooks from "./hooks";
import RoomShowStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { RootNavigatorStackParams } from "../../navigators/RootNavigator/constants";
import { RoomSplitScreenNavigatorStackParams } from "../../navigators/RoomSplitScreenNavigator/constants";
import { RoomAction, RoomPageState } from "@/presentation/redux/Room/RoomReducer";
import { RoomLogAction, RoomLogsPageState } from "@/presentation/redux/RoomLog/RoomLogReducer";
import { RoomConnectingMembersPageState, RoomMemberAction } from "@/presentation/redux/RoomMember/RoomMemberReducer";
import { roomLogsPageStateSelector } from "@/presentation/redux/RoomLog/RoomLogSelector";
import { InsetsOutput, useInsets } from "@/shared/hooks/useInsets";
import { GuildAction } from "@/presentation/redux/Guild/GuildReducer";
import { guildShowSelector } from "@/presentation/redux/Guild/GuildSelector";
import { Guild } from "@guildion/core";
import { roomShowPageStateSelector } from "@/presentation/redux/Room/RoomSelector";
import { roomConnectingMembersPageStateSelector } from "@/presentation/redux/RoomMember/RoomMemberSelector";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomShowPresenter {
    export type StackParams = {
        id: string,
        guildId: string,
    }
    
    export type StateProps = {
        guild?: Guild,
        roomPageState?: RoomPageState,
        logsPageState?: RoomLogsPageState,
        connectingMembersPageState?: RoomConnectingMembersPageState,
    }
    
    export type DispatchProps = {
        fetchRoom: (id: string) => Action,
        fetchGuild: (id: string) => Action,
        fetchGuilds: () => Action,
        fetchLogs: (roomId: string) => Action,
        fetchConnectingMembers: (roomId: string) => Action,
        disconnectRoom: (id: string) => Action,
    }
    
    export type ModalScreenProps = NativeStackScreenProps<RootNavigatorStackParams, "RoomShow">;
    export type SplitScreenProps = NativeStackScreenProps<RoomSplitScreenNavigatorStackParams, "RoomShow">;
    export type Input = Partial<StyleProps> & {
        modalScreenProps?: ModalScreenProps,
        splitScreenProps?: SplitScreenProps,
    };
    
    export type Output = {
        styles: RoomShowStyles,
        appTheme: AppTheme,
        insets: InsetsOutput,
        guild?: Guild,
        roomId?: string,
        roomPageState?: RoomPageState,
        logsPageState?: RoomLogsPageState,
        connectingMembersPageState?: RoomConnectingMembersPageState,
        onPressGuild: (guild: Guild) => void,
        modalScreenProps?: ModalScreenProps,
        splitScreenProps?: SplitScreenProps,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        const roomId = ownProps?.modalScreenProps?.route?.params?.id ?? ownProps?.splitScreenProps?.route?.params?.id;
        const guildId = ownProps?.modalScreenProps?.route?.params?.guildId ?? ownProps?.splitScreenProps?.route?.params?.guildId;
        return {
            guild: guildId ? guildShowSelector(state, guildId) : undefined,
            roomPageState: roomId ? roomShowPageStateSelector(state, roomId) : undefined,
            logsPageState: roomId ? roomLogsPageStateSelector(state, roomId) : undefined,
            connectingMembersPageState: roomId ? roomConnectingMembersPageStateSelector(state, roomId) : undefined,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetchRoom: (id: string) => RoomAction.fetchShow.started({ id }),
            fetchGuilds: () => GuildAction.fetchIndex.started({}),
            fetchGuild: (id: string) => GuildAction.fetchShow.started({ id }),
            fetchLogs: (roomId: string) => RoomLogAction.fetchIndex.started({ roomId }),
            fetchConnectingMembers: (roomId: string) => RoomMemberAction.fetchConnectingIndex.started({ roomId }),
            disconnectRoom: (id: string) => RoomAction.disconnect.started({ id }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.guild.pages.show == next.guild.pages.show &&
            prev.room.pages.show == next.room.pages.show &&
            prev.roomLog.pages.index == next.roomLog.pages.index &&
            prev.roomMember.pages.connectings == next.roomMember.pages.connectings
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const roomId = props.modalScreenProps?.route?.params?.id ?? props.splitScreenProps?.route?.params?.id;
        const styles = RoomShowHooks.useStyles(props);
        const insets = useInsets({ bottomExclude: true, width: Number(props.style?.width) ?? 0, height: Number(props.style?.height) ?? 0 });
        RoomShowHooks.useViewWillAppear(props);
        props.splitScreenProps?.navigation && RoomShowHooks.useObserveNavigation(props.splitScreenProps.navigation);
        return {
            ...props,
            insets,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            roomId,
            onPressGuild: (guild: Guild) => {
                props.disconnectRoom(roomId!);
                props.modalScreenProps?.navigation.goBack() ?? props.splitScreenProps?.navigation.goBack()
            },
        }
    }
}

export default RoomShowPresenter;