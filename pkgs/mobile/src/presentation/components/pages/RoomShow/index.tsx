import React from 'react';
import RoomShowPresenter from './presenter';
import RoomShowComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: RoomShowPresenter.MergedProps) => {
    const output = RoomShowPresenter.usePresenter(props);
    return <RoomShowComponent {...output} />;
};

const RoomShow = connect<RoomShowPresenter.StateProps, RoomShowPresenter.DispatchProps, RoomShowPresenter.Input, RoomShowPresenter.MergedProps, RootState>(
    RoomShowPresenter.mapStateToProps,
    RoomShowPresenter.mapDispatchToProps,
    RoomShowPresenter.mergeProps,
    RoomShowPresenter.connectOptions,
)(Container);

export default RoomShow;