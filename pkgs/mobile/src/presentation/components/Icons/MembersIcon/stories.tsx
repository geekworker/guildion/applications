import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MembersIcon from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { number, text } from '@storybook/addon-knobs';

storiesOf('MembersIcon', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <MembersIcon
            width={number('width', 50)}
            height={number('height', 50)}
            color={text('color', '#FFFFFF')}
        />
    ))
