import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import Svg, { Path, G } from "react-native-svg";

const MembersIcon: React.FC<IconProps> = ({ width, height, color }) => {
    return (
        <Svg
            x="0px"
            y="0px"
            viewBox="0 0 512 512"
            width={width}
            height={height}
        >
            <G>
                <Path fill={color} d="M200,331.5c60.9,0,110.3-49.4,110.3-110.3c0-60.9-49.4-110.3-110.3-110.3c-60.9,0-110.3,49.4-110.3,110.3
                    C89.7,282.1,139.1,331.5,200,331.5z"/>
                <Path fill={color} d="M393.6,476.6c-23.4-71.9-109.2-111-193.6-111c-84.4,0-170.2,39.1-193.6,111c-3.8,11.5-5.8,23-6.4,34.1h400
                    C399.3,499.6,397.3,488.1,393.6,476.6z"/>
                <Path fill={color} d="M330.4,121.9c-16.1,0-31.3,3.8-44.7,10.6c27.1,20.1,44.7,52.3,44.7,88.7c0,36.3-17.6,68.6-44.7,88.7
                    c13.4,6.8,28.6,10.6,44.7,10.6c54.8,0,99.3-44.5,99.3-99.3C429.7,166.4,385.2,121.9,330.4,121.9z"/>
                <Path fill={color} d="M506.6,481.2c-20.1-65-96.8-101.3-172.7-102.4c-5.8-0.1-11.7,0-17.5,0.4c47.2,18.5,85.5,51.1,99.4,96
                    c3.6,11.6,5.5,23,6,34.2l-269.6-3.9c0,0.4-0.1,0.9-0.1,1.3l360,5.2C511.6,502,509.9,491.6,506.6,481.2z"/>
            </G>
        </Svg>
    );
};

export default React.memo(MembersIcon, compare);
