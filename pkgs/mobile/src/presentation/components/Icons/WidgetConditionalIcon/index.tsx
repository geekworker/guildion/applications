import React from 'react';
import StyleProps from '@/shared/interfaces/StyleProps';
import { MediaLibraryWidgetName, PostWidgetName, SettingWidgetName, SyncVisionWidgetName, TextChatWidgetName, Widget } from '@guildion/core';
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import FeatherIcon from "react-native-vector-icons/Feather";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";
import SyncVisionIcon from '../SyncVisionIcon';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    widget: Widget,
} & Partial<StyleProps>;

const WidgetConditionalIcon: React.FC<Props> = ({
    style,
    appTheme,
    widget,
}) => {
    switch(widget.widgetname) {
    case TextChatWidgetName: return <FontistoIcon size={Number(style?.width ?? 0)} color={String(style?.color)} name={'hashtag'}/>;
    case SyncVisionWidgetName: return <SyncVisionIcon width={Number(style?.width ?? 0)} height={Number(style?.height ?? 0)} color={String(style?.color)}/>;
    case MediaLibraryWidgetName: return <FontAwesomeIcon size={Number(style?.width ?? 0)} color={String(style?.color)} name={'folder'}/>;
    case PostWidgetName: return <MaterialIcon size={Number(style?.width ?? 0)} color={String(style?.color)} name={'article'}/>;
    case SettingWidgetName: return <FeatherIcon size={Number(style?.width ?? 0)} color={String(style?.color)} name={'more-horizontal'}/>;
    default: return null;
    };
};

export default React.memo(WidgetConditionalIcon, compare);