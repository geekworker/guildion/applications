import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import Svg, { Polygon } from "react-native-svg";

const BrandIcon: React.FC<IconProps> = ({ width, height, color }) => {
    return (
        <Svg
            x="0px"
            y="0px"
            viewBox="0 0 421.04 422.05"
            width={width}
            height={height}
        >
            <Polygon fill={color} points="421.04 210.01 254.84 111.19 254.52 156.64 382.64 232.82 421.04 210.01"/>
            <Polygon fill={color} points="421.04 210.01 254.84 308.82 254.52 263.37 382.64 187.19 421.04 210.01"/>
            <Polygon fill={color} points="51.78 0 49.3 193.35 88.83 215.8 90.73 21.85 51.78 0"/>
            <Polygon fill={color} points="51.78 0 220.46 94.53 220.16 139.99 51.22 44.66 51.78 0"/>
            <Polygon fill={color} points="52.66 420 221.35 325.47 221.05 280.02 52.11 375.34 52.66 420"/>
            <Polygon fill={color} points="52.66 420 50.19 226.65 89.71 249.1 91.62 398.15 52.66 420"/>
        </Svg>
    );
};

export default React.memo(BrandIcon, compare);