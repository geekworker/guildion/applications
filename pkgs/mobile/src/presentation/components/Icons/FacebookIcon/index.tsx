import { IconProps } from "@guildion/ui";
import { compare } from "@/shared/modules/ObjectCompare";
import React from "react";
import Svg, { Path } from "react-native-svg";

const FacebookIcon: React.FC<IconProps> = ({ width, height, color }) => {
    return (
        <Svg
            viewBox="0 0 195 193.8"
            width={width}
            height={height}
        >
            <Path
                fill={color}
                d="M222.56,125.07a97.5,97.5,0,1,0-112.7,96.3v-68.1H85.06v-.1H85V125h24.9v-21.5c0-24.4,14.5-38,36.7-38a133.34,133.34,0,0,1,21.7,2.1v.08l.1,0v23.8h-12.2c-12.1,0-16,7.6-16,15.3V125h27v.1h.11l-4.3,28.2h-22.6v68.07A97.52,97.52,0,0,0,222.56,125.07Z"
                transform="translate(-27.56 -27.57)"
            />
        </Svg>
    );
};

export default React.memo(FacebookIcon, compare);
