import React from 'react';
import StyleProps from '@/shared/interfaces/StyleProps';
import { SocialLink, SocialLinkType } from '@guildion/core';
import { compare } from '@/shared/modules/ObjectCompare';
import FacebookIcon from '../FacebookIcon';
import TwitterIcon from '../TwitterIcon';
import InstgramIcon from '../InstgramIcon';
import YouTubeIcon from '../YouTubeIcon';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';

type Props = {
    socialLink: SocialLink,
} & Partial<StyleProps>;

const WidgetConditionalIcon: React.FC<Props> = ({
    style,
    appTheme,
    socialLink,
}) => {
    switch(socialLink.type) {
    case SocialLinkType.FacebookProfile: return <FacebookIcon width={Number(style?.width ?? 0)} height={Number(style?.height ?? 0)} color={String(style?.color)}/>;
    case SocialLinkType.TwitterProfile: return <TwitterIcon width={Number(style?.width ?? 0)} height={Number(style?.height ?? 0)} color={String(style?.color)}/>;
    case SocialLinkType.InstagramProfile: return <InstgramIcon width={Number(style?.width ?? 0)} height={Number(style?.height ?? 0)} color={String(style?.color)}/>;
    case SocialLinkType.YouTubeChannel: return <YouTubeIcon width={Number(style?.width ?? 0)} height={Number(style?.height ?? 0)} color={String(style?.color)}/>;
    case SocialLinkType.Custom: return <AntDesignIcon size={Number(style?.width ?? 0)} color={String(style?.color)} name={'earth'} />;
    default: return null;
    };
};

export default React.memo(WidgetConditionalIcon, compare);