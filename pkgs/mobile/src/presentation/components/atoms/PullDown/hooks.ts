import React from 'react';
import PullDownStyles from './styles';
import type PullDownPresenter from './presenter';
import { List } from 'immutable';
import { localizer } from '@/shared/constants/Localizer';

namespace PullDownHooks {
    export const useStyles = (props: PullDownPresenter.Input) => {
        const styles = React.useMemo(() => new PullDownStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: PullDownPresenter.Input) => {
        const options = React.useMemo<List<PullDownPresenter.SelectOption>>(
            () => props.presence ? List([
                { value: undefined, displayValue: localizer.dictionary.g.noSelection },
                ...(props.options ?? List([])).toArray(),
            ]) : (props.options ?? List([]))
        , [props.options, props.presence]);
        const [value, setValue] = React.useState<string | undefined>(undefined);
        const selectedOption = React.useMemo(() => options.find(op => op.value == value), [options, value]);
        React.useEffect(() => {
            if (value != props.value) {
                if (selectedOption) {
                    setValue(selectedOption.value);
                } else {
                    setValue(undefined)
                }
            };
        }, [props.value, props.options, selectedOption])
        return {};
    }
}

export default PullDownHooks;