import React from 'react';
import PullDownComponent from './component';
import PullDownPresenter from './presenter';

const PullDown = (props: PullDownPresenter.Input) => {
    const output = PullDownPresenter.usePresenter(props);
    return <PullDownComponent {...output} />;
};

export default React.memo(PullDown, PullDownPresenter.inputAreEqual);