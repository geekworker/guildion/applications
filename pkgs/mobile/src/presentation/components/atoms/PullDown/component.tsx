import React from 'react';
import { Animated, View } from 'react-native';
import PullDownPresenter from './presenter';

const PullDownComponent = ({ styles, appTheme, animatedStyle }: PullDownPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
        </Animated.View>
    );
};

export default React.memo(PullDownComponent, PullDownPresenter.outputAreEqual);