import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import PullDownHooks from './hooks';
import PullDownStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { List } from 'immutable';

namespace PullDownPresenter {
    export type SelectOption = {
        displayValue?: string,
        value?: string,
    };

    export type Input = {
        title?: string,
        value?: string,
        placeholder?: string,
        options?: List<SelectOption>,
        presence?: boolean,
        onChangeValue?: (value?: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PullDownStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        title?: string,
        value?: string,
        placeholder?: string,
        options?: List<SelectOption>,
        presence?: boolean,
        onChangeValue?: (value?: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PullDownHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default PullDownPresenter;