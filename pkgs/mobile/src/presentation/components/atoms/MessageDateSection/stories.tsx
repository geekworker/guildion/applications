import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MessageDateSection from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Message } from '@guildion/core';

storiesOf('MessageDateSection', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MessageDateSection
            style={new Style({
                width: number('width', 375),
                height: number('height', 44),
            })}
            message={
                new Message({ createdAt: new Date().toISOString() })
            }
        />
    ))