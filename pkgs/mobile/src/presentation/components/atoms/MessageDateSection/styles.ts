import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { MontserratFont } from '@/shared/constants/Font';

const styles = ({
    style,
    appTheme,
    text,
}: StyleProps & { text?: string }) => {
    appTheme ||= fallbackAppTheme;
    const width = Number(style?.width ?? 0);
    const height = Number(style?.height ?? 0);
    const textWidth = Math.max((text?.length ?? 0) * 14 / 2, 60);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'relative',
        },
        borderLeft: {
            width: (width / 2) - 30 - (textWidth / 2),
            height: 0.5,
            marginRight: 10,
            backgroundColor: appTheme.element.subp1,
            borderRadius: 0.5,
        },
        borderRight: {
            width: (width / 2) - 30 - (textWidth / 2),
            height: 0.5,
            marginLeft: 10,
            backgroundColor: appTheme.element.subp1,
            borderRadius: 0.5,
        },
        title: {
            fontSize: 14,
            fontFamily: MontserratFont.Medium,
            color: appTheme.element.subp1,
            textAlign: 'center',
            width: textWidth,
        },
    });
}

type Styles = typeof styles;

export default class MessageDateSectionStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { text?: string }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};