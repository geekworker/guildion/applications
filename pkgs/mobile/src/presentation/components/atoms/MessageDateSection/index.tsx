import React from 'react';
import MessageDateSectionStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, View } from 'react-native';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Message } from '@guildion/core';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

export type MessageDateSectionProps = {
    message: Message,
} & Partial<StyleProps>;

const MessageDateSection: React.FC<MessageDateSectionProps> = ({
    style,
    appTheme,
    message,
}) => {
    appTheme ||= fallbackAppTheme;
    const text = message.getCreatedDateSection(CURRENT_CONFIG.DEFAULT_TIMEZONE, CURRENT_CONFIG.DEFAULT_LANG);
    const styles = React.useMemo(() => new MessageDateSectionStyles({
        style,
        appTheme,
        text,
    }), [
        style,
        appTheme,
        text,
    ]);
    return (
        <View style={styles.container}>
            <View style={styles.borderLeft}/>
            <Text style={styles.title} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                {text}
            </Text>
            <View style={styles.borderRight}/>
        </View>
    );
};

export default React.memo(MessageDateSection, compare);