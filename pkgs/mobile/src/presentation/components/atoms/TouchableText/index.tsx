import React from "react";
import StyleProps from "@/shared/interfaces/StyleProps";
import { Text, TextProps } from "react-native";
import { Style } from "@/shared/interfaces/Style";
import { compare } from "@/shared/modules/ObjectCompare";
import { TouchableOpacity } from "react-native-gesture-handler";

type Props = {
    onPress?: () => void,
    textStyle?: Style,
    children?: string,
    isTouchableHightlight?: boolean,
} & StyleProps & TextProps;

const TouchableText: React.FC<Props> = ({ onPress, style, textStyle, children, isTouchableHightlight, ...textProps }) => {
    return (
        <TouchableOpacity onPress={onPress} style={style?.toStyleObject()} >
            <Text {...textProps} style={textStyle?.toStyleObject()}>
                {children}
            </Text>
        </TouchableOpacity>
    );
};

export default React.memo(TouchableText, compare);