import { storiesOf } from '@storybook/react-native';
import { text } from '@storybook/addon-knobs';
import React from 'react';
import TouchableText from '.';
import CenterView from '../CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('TouchableText', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <TouchableText
            textStyle={new Style({
                color: "#FFFFFF",
                fontSize: 12,
            })}
        >
            {text('text', 'aaaaa')}
        </TouchableText>
    ))
