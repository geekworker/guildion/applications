import React from 'react';
import IconTabStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';

export type IconTabProps = {
    Icon: IconNodeClass,
    title?: string,
    id: string,
    onPress?: (id: string) => void,
    active?: boolean,
} & Partial<StyleProps>;

const IconTab: React.FC<IconTabProps> = ({
    style,
    appTheme,
    Icon,
    title,
    id,
    onPress,
    active,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new IconTabStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    const [state, setState] = React.useState({
        active: active ?? false,
    });
    React.useEffect(() => {
        active !== undefined && active != state.active && setState({ ...state, active, });
    }, [active]);
    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.touchable}
                onPress={
                    React.useCallback(() => {
                        onPress && onPress(id);
                    }, [onPress, id])
                }
            >
                <View style={styles.inner}>
                    <View style={state.active ? styles.activeIcon : styles.icon}>
                        <Icon
                            size={(state.active ? styles.activeIcon : styles.icon).width}
                            color={(state.active ? styles.activeIcon : styles.icon).color}
                        />
                    </View>
                    <Text style={state.active ? styles.activeTitle : styles.title} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                        {title}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default React.memo(IconTab, compare);