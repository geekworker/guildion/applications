import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconTab from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import Icon from 'react-native-vector-icons/FontAwesome';

storiesOf('IconTab', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <IconTab
            title={'Play Next'}
            style={new Style({
                width: 60,
                height: 40,
            })}
            id='test'
            Icon={(props) => <Icon size={props.size} color={props.color} name={'list'} />}
        />
    ))