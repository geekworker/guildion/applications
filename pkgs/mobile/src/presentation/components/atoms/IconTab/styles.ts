import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
        },
        touchable: {
            width: style?.width,
            height: style?.height,
        },
        inner: {
            width: style?.width,
            height: style?.height,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        icon: {
            width: 18,
            height: 18,
            color: appTheme.element.subp15,
        },
        activeIcon: {
            width: 18,
            height: 18,
            color: appTheme.element.default,
        },
        title: {
            marginTop: 4,
            fontSize: 10,
            fontFamily: MontserratFont.SemiBold,
            color: appTheme.element.subp15,
            textAlign: 'center',
        },
        activeTitle: {
            marginTop: 4,
            fontSize: 10,
            fontFamily: MontserratFont.SemiBold,
            color: appTheme.element.default,
            textAlign: 'center',
        },
    })
};

type Styles = typeof styles;

export default class IconTabStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};