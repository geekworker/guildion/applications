import React from "react";
import { Keyboard } from "react-native";
import { TouchableNativeFeedback } from "react-native-gesture-handler";

interface Props {
    children?: React.ReactNode,
}

const DismissKeyboardView: React.FC<Props> = ({ children }) => {
    return (
        <TouchableNativeFeedback onPress={() => Keyboard.dismiss()}>
            {children}
        </TouchableNativeFeedback>
    );
};

export default DismissKeyboardView;