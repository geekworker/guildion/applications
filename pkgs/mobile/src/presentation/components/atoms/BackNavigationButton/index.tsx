import React from 'react';
import RootRedux from '@/infrastructure/Root/redux';
import { IS_STORYBOOK } from '@guildion/core';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import BackNavigationButtonStyles from './styles';
import FeatherIcon from 'react-native-vector-icons/Feather';
import { Text, View, TouchableOpacity } from 'react-native';
import { compare } from '@/shared/modules/ObjectCompare';
import { localizer } from '@/shared/constants/Localizer';

type Props = {
    onPress?: () => void,
    label?: string,
    size?: number,
};

const BackNavigationButton: React.FC<Props> = ({ onPress, label, size }) => {
    const context = React.useContext(RootRedux.Context);
    const appTheme = IS_STORYBOOK() ? fallbackAppTheme : context.state.appTheme;
    const styles = React.useMemo(() => new BackNavigationButtonStyles({ appTheme }), [appTheme]);
    return (
        <TouchableOpacity
            onPress={onPress}
            style={styles.highlight}
        >
            <View style={styles.container}>
                <FeatherIcon name={"chevron-left"} size={size ?? 24} color={appTheme.navigation.defaultText} />
                <Text style={styles.text}>
                    {label ?? localizer.dictionary.g.back}
                </Text>
            </View>
        </TouchableOpacity>
    );
};

export default React.memo(BackNavigationButton, compare);