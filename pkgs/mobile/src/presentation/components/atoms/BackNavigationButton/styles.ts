import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        highlight: {
            paddingLeft: 6,
            height: '100%',
            paddingRight: 8,
            paddingTop: 10,
        },
        container: {
            flexDirection: 'row',
            alignItems: 'center',
        },
        text: {
            marginLeft: -2,
            color: appTheme.navigation.defaultText,
            fontSize: 14,
            fontFamily: MontserratFont.SemiBold,
        },
    })
};

type Styles = typeof styles;

export default class BackNavigationButtonStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};