import React from 'react';
import GuildTabComponent from './component';
import GuildTabPresenter from './presenter';

const GuildTab = (props: GuildTabPresenter.Input) => {
    const output = GuildTabPresenter.usePresenter(props);
    return <GuildTabComponent {...output} />;
};

export default React.memo(GuildTab, GuildTabPresenter.inputAreEqual);