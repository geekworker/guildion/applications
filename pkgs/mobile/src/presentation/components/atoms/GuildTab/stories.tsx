import { Guild, GuildActivity } from '@guildion/core';
import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildTab from '.';
import CenterView from '../CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { boolean, number } from '@storybook/addon-knobs';
import { Style } from '@/shared/interfaces/Style';

storiesOf('GuildTab', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <CenterView>
                {getStory()}
            </CenterView>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildTab
            guild={new Guild({
                displayName: 'sample guilddddd',
            })}
            isFocused={boolean('isFocused', false)}
            loading={boolean('loading', false)}
            style={new Style({
                width: 74,
                height: 74,
            })}
        >
        </GuildTab>
    ))
    .add('Badge', () => (
        <GuildTab
            guild={new Guild({
                displayName: 'sample guilddddd',
                activity: new GuildActivity({
                    notificationsCount: number('notificationsCount', 1),
                })
            })}
            isFocused={boolean('isFocused', false)}
            loading={boolean('loading', false)}
            style={new Style({
                width: 74,
                height: 74,
            })}
        >
        </GuildTab>
    ))
    .add('Loading', () => (
        <GuildTab
            guild={new Guild({})}
            loading={boolean('loading', true)}
            style={new Style({
                width: 74,
                height: 74,
            })}
        >
        </GuildTab>
    ))
