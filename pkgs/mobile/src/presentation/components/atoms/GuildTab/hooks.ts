import React from "react";
import GuildTabStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace GuildTabHooks {
    export const useStyles = (props: StyleProps & { isFocused?: boolean }) => {
        const styles = React.useMemo(() => new GuildTabStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default GuildTabHooks