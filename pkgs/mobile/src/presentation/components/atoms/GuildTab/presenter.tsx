import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import GuildTabHooks from "./hooks";
import GuildTabStyles from './styles';
import React from 'react';
import { Guild } from "@guildion/core";
import { compare } from "@/shared/modules/ObjectCompare";

namespace GuildTabPresenter {
    export type Input = {
        children?: React.ReactNode,
        guild: Guild,
        isFocused?: boolean,
        onPress?: (guild: Guild) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildTabStyles,
        appTheme: AppTheme,
        guild: Guild,
        onPress: () => void,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = GuildTabHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            onPress: () => props.onPress && props.onPress(props.guild)
        }
    }
}

export default GuildTabPresenter;