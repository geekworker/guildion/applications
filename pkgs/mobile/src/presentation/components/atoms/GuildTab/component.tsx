import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Badge } from 'react-native-elements';
import AsyncImage from '../AsyncImage';
import GuildTabPresenter from './presenter';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';

const GuildTabComponent = ({ styles, guild, onPress, loading, appTheme }: GuildTabPresenter.Output) => {
    return (
        <TouchableOpacity onPress={onPress} disabled={loading}>
            <View style={styles.container}>
                <AsyncImage
                    loading={loading}
                    url={guild.profile?.url ?? ""}
                    style={styles.getStyle('image')}
                />
                <SkeletonContent
                    isLoading={loading ?? false}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[
                        { ...styles.nameSkeleton },
                    ]}
                >
                    <Text style={styles.name} numberOfLines={1}>
                        {guild.displayName}
                    </Text>
                </SkeletonContent>
                {!loading && !!guild.activity?.notificationsCount && guild.activity.notificationsCount > 0 && (
                    <Badge
                        status={'error'}
                        containerStyle={styles.notificationContainerStyle}
                        badgeStyle={styles.notificationBadgeStyle}
                        textStyle={styles.notificationTextStyle}
                        value={guild.activity.notificationsCount}
                    />
                )}
                {!loading && !!guild.activity?.connectsCount && guild.activity.connectsCount > 0 && (
                    <Badge
                        status={'success'}
                        containerStyle={styles.connectContainerStyle}
                        badgeStyle={styles.connectBadgeStyle}
                        textStyle={styles.connectTextStyle}
                        value={guild.activity.connectsCount}
                    />
                )}
            </View>
        </TouchableOpacity>
    );
};

export default React.memo(GuildTabComponent, GuildTabPresenter.outputAreEqual);