import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { MontserratFont } from '@/shared/constants/Font';

export const GUILD_TAB_ACTIVE_RATIO: number = 1.5;
const styles = ({
    style,
    appTheme,
    isFocused
}: StyleProps & {
    isFocused?: boolean,
}) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius ?? 5,
            paddingTop: style?.paddingTop ?? 4,
            paddingBottom: style?.paddingBottom ?? 4,
            backgroundColor: isFocused ? appTheme.guild.tabBackgroundActive : appTheme.guild.tabBackground,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
        },
        skeleton: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius ?? 5,
            position: 'relative',
        },
        notificationContainerStyle: {
            position: 'absolute',
            top: 2,
            right: 8,
        },
        connectContainerStyle: {
            position: 'absolute',
            top: 2,
            right: 24,
        },
        notificationBadgeStyle: {
            width: 16,
            height: 18,
            borderRadius: 9,
            borderWidth: 1,
            borderColor: isFocused ? appTheme.guild.tabBackgroundActive : appTheme.guild.tabBackground,
        },
        connectBadgeStyle: {
            width: 16,
            height: 18,
            borderRadius: 9,
            borderWidth: 1,
            borderColor: isFocused ? appTheme.guild.tabBackgroundActive : appTheme.guild.tabBackground,
        },
        notificationTextStyle: {
            fontSize: 10,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
        },
        connectTextStyle: {
            fontSize: 10,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
        },
        image: {
            width: Number(style?.width ?? 0) / GUILD_TAB_ACTIVE_RATIO,
            height: Number(style?.height ?? 0) / GUILD_TAB_ACTIVE_RATIO,
            borderRadius: 5,
        },
        nameSkeleton: {
            width: Number(style?.width ?? 0) - 4,
            height: 12,
            marginLeft: 2,
            marginRight: 2,
            marginTop: 2,
            borderRadius: style?.borderRadius ?? 5,
            position: 'relative',
        },
        name: {
            fontSize: 10,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            textAlign: 'center',
            width: Number(style?.width ?? 0) - 4,
            marginLeft: 2,
            marginRight: 2,
            marginTop: 2,
            height: 12,
        },
    });
}

type Styles = typeof styles;

export default class GuildTabStyles extends Record<ReturnType<Styles>>({
    ...styles({ isFocused: undefined })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & {
        isFocused?: boolean,
    }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};