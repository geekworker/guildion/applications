import React from "react";
import RoomParsedTextStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type RoomParsedTextPresenter from "./presenter";
import { AutoCompletePrefix, Message } from "@guildion/core";

namespace RoomParsedTextHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomParsedTextStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useRenderMembersMention = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.membersMentionRegExp);
            return match ? `${AutoCompletePrefix.Member}${match[1]}` : '';
        }, []);
    }

    export const useRenderMemberMention = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.memberMentionRegExp);
            return match ? `${match[1]}` : '';
        }, []);
    }

    export const useRenderRoomMention = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.roomMentionRegExp);
            return match ? `${match[1]}` : '';
        }, []);
    }

    export const useRenderDuration = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.durationRegExp);
            return match ? `${match[0]}` : '';
        }, []);
    }

    export const useRenderCodeBlock = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.codeBlockRegExp);
            return match ? `${match[1]}\n` : '';
        }, []);
    }

    export const useRenderBold = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.boldRegExp);
            return match ? `${match[1]}` : '';
        }, []);
    }

    export const useRenderStrong = () => {
        return React.useCallback((matchingString: string, matches: string[]): string => {
            const match = matchingString.match(Message.strongRegExp);
            return match ? `${match[1]}` : '';
        }, []);
    }

    export const useOnPress = (props: RoomParsedTextPresenter.Input) => {
        const onPressURL = React.useCallback((text: string, index: number) => {
            props.onPressURL && props.onPressURL(text);
        }, [props.onPressURL]);
        const onPressEmail = React.useCallback((text: string, index: number) => {
            props.onPressEmail && props.onPressEmail(text);
        }, [props.onPressEmail]);
        const onPressPhoneNumber = React.useCallback((text: string, index: number) => {
            props.onPressPhoneNumber && props.onPressPhoneNumber(text);
        }, [props.onPressPhoneNumber]);
        const onPressMentionMember = React.useCallback((text: string, index: number) => {
            props.onPressMentionMember && props.onPressMentionMember(text);
        }, [props.onPressMentionMember]);
        const onPressDuration = React.useCallback((text: string, index: number) => {
            if (props.onPressDuration) {
                const splits = text.split(':');
                splits.length == 3 && !Number(splits[0]) ?
                props.onPressDuration({ mm: Number(splits[1]), ss: Number(splits[2]) }) :
                splits.length == 3 ?
                props.onPressDuration({ hh: Number(splits[0]), mm: Number(splits[1]), ss: Number(splits[2]) }) :
                splits.length == 2 && !Number(splits[0]) ?
                props.onPressDuration({ ss: Number(splits[2]) }) :
                splits.length == 2 ?
                props.onPressDuration({ mm: Number(splits[0]), ss: Number(splits[1]) }) :
                undefined;
            };
        }, [props.onPressDuration]);
        return {
            onPressURL,
            onPressEmail,
            onPressPhoneNumber,
            onPressMentionMember,
            onPressDuration,
        };
    }
}

export default RoomParsedTextHooks;