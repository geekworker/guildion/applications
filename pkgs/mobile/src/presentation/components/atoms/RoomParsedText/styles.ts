import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const fontSize: number = 14;
const lineHeight: number = 20;
const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            justifyContent: 'center',
            alignItems: 'center',
            color: 'white',
            fontFamily: MontserratFont.Regular,
            lineHeight,
            fontSize,
        },
        duration: {
            fontSize,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.link,
            lineHeight,
        },
        url: {
            fontSize,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.url,
            textDecorationLine: 'underline',
            lineHeight,
        },
        email: {
            fontSize,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.url,
            textDecorationLine: 'underline',
            lineHeight,
        },
        text: {
            fontSize,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            lineHeight,
        },
        phone: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.url,
            textDecorationLine: 'underline',
            lineHeight,
        },
        mentionYou: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Medium,
            color: appTheme.mention.yourColor,
            backgroundColor: appTheme.mention.yourBackgroundColor,
            lineHeight,
        },
        mentionMember: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Medium,
            color: appTheme.mention.memberColor,
            backgroundColor: appTheme.mention.memberBackgroundColor,
            lineHeight,
        },
        mentionRoom: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Medium,
            color: appTheme.mention.roomColor,
            backgroundColor: appTheme.mention.roomBackgroundColor,
            lineHeight,
        },
        bold: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
            lineHeight,
        },
        codeBlock: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            backgroundColor: appTheme.background.subp2,
            lineHeight,
        },
        strong: {
            fontSize: fontSize,
            fontFamily: MontserratFont.Medium,
            color: appTheme.mention.yourColor,
            lineHeight,
        },
    })
};

type Styles = typeof styles;

export default class RoomParsedTextStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};