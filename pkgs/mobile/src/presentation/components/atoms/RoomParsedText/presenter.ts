import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomParsedTextHooks from "./hooks";
import RoomParsedTextStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomParsedTextPresenter {
    export type Input = {
        text?: string,
        numberOfLines?: number,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        onPressDuration?: ({ hh, mm, ss }: { hh?: number, mm?: number, ss: number }) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomParsedTextStyles,
        appTheme: AppTheme,
        text?: string,
        numberOfLines?: number,
        renderMembersMention: (matchingString: string, matches: string[]) => string,
        renderMemberMention: (matchingString: string, matches: string[]) => string,
        renderRoomMention: (matchingString: string, matches: string[]) => string,
        renderCodeBlock: (matchingString: string, matches: string[]) => string,
        renderBold: (matchingString: string, matches: string[]) => string,
        renderStrong: (matchingString: string, matches: string[]) => string,
        renderDuration: (matchingString: string, matches: string[]) => string,
        onPressURL?: (text: string, index: number) => void,
        onPressEmail?: (text: string, index: number) => void,
        onPressPhoneNumber?: (text: string, index: number) => void,
        onPressMentionMember?: (text: string, index: number) => void,
        onPressMentionRoom?: (text: string, index: number) => void,
        onPressDuration?: (text: string, index: number) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomParsedTextHooks.useStyles({ ...props });
        const renderMembersMention = RoomParsedTextHooks.useRenderMembersMention();
        const renderMemberMention = RoomParsedTextHooks.useRenderMemberMention();
        const renderRoomMention = RoomParsedTextHooks.useRenderRoomMention();
        const renderCodeBlock = RoomParsedTextHooks.useRenderCodeBlock();
        const renderBold = RoomParsedTextHooks.useRenderBold();
        const renderStrong = RoomParsedTextHooks.useRenderStrong();
        const renderDuration = RoomParsedTextHooks.useRenderDuration();
        const {
            onPressURL,
            onPressEmail,
            onPressPhoneNumber,
            onPressMentionMember,
            onPressDuration,
        } = RoomParsedTextHooks.useOnPress(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            renderMembersMention,
            renderMemberMention,
            renderRoomMention,
            renderCodeBlock,
            renderBold,
            renderStrong,
            renderDuration,
            onPressURL,
            onPressEmail,
            onPressPhoneNumber,
            onPressMentionMember,
            onPressDuration,
        }
    }
}

export default RoomParsedTextPresenter;