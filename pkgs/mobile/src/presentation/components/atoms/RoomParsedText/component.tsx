import React from 'react';
import RoomParsedTextPresenter from './presenter';
import ParsedText from 'react-native-parsed-text';
import { Message } from "@guildion/core";

const RoomParsedTextComponent = ({ styles, text, renderMemberMention, renderMembersMention, renderRoomMention, renderCodeBlock, renderBold, renderStrong, renderDuration, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, onPressDuration, numberOfLines }: RoomParsedTextPresenter.Output) => {
    return (
        <ParsedText
            style={styles.container}
            suppressHighlighting
            numberOfLines={numberOfLines ?? 0}
            parse={
                [
                    {
                        type: 'url',
                        style: styles.url,
                        onPress: onPressURL,
                    },
                    {
                        type: 'phone',
                        style: styles.phone,
                        onPress: onPressPhoneNumber,
                    },
                    {
                        type: 'email',
                        style: styles.email,
                        onPress: onPressEmail,
                    },
                    {
                        pattern: Message.durationRegExp,
                        style: styles.duration,
                        renderText: renderDuration,
                        onPress: onPressDuration,
                    },
                    {
                        pattern: Message.membersMentionRegExp,
                        style: styles.mentionYou,
                        renderText: renderMembersMention,
                    },
                    {
                        pattern: Message.memberMentionRegExp,
                        style: styles.mentionMember,
                        renderText: renderMemberMention,
                        onPress: onPressMentionMember,
                    },
                    {
                        pattern: Message.roomMentionRegExp,
                        style: styles.mentionRoom,
                        renderText: renderRoomMention,
                        onPress: onPressMentionRoom,
                    },
                    {
                        pattern: Message.codeBlockRegExp,
                        style: styles.codeBlock,
                        renderText: renderCodeBlock,
                    },
                    {
                        pattern: Message.strongRegExp,
                        style: styles.strong,
                        renderText: renderStrong,
                    },
                    {
                        pattern: Message.boldRegExp,
                        style: styles.bold,
                        renderText: renderBold,
                    },
                ]
              }
              childrenProps={{ allowFontScaling: false }}
        >
            {text}
        </ParsedText>
    );
};

export default React.memo(RoomParsedTextComponent, RoomParsedTextPresenter.outputAreEqual);