import React from 'react';
import RoomParsedTextComponent from './component';
import RoomParsedTextPresenter from './presenter';

const RoomParsedText = (props: RoomParsedTextPresenter.Input) => {
    const output = RoomParsedTextPresenter.usePresenter(props);
    return <RoomParsedTextComponent {...output} />;
};

export default React.memo(RoomParsedText, RoomParsedTextPresenter.inputAreEqual);