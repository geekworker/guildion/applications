import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style, text }: StyleProps & { text?: string }) => {
    appTheme ||= fallbackAppTheme;
    const width = text ? text.length * 12 : undefined;
    return StyleSheet.create({
        container: {
            ...styles,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        badge: {
            width: 6,
            height: 6,
            borderRadius: 3,
            backgroundColor: appTheme.element.input,
            marginRight: 6,
        },
        label: {
            fontFamily: MontserratFont.Regular,
            fontSize: 12,
            color: appTheme.element.default,
            textAlign: 'left',
            // width,
        },
        badgeSkeleton: {
            width: 6,
            height: 6,
            borderRadius: 3,
            marginRight: 6,
        },
        labelSkeleton: {
            // width,
            height: 12,
        },
    });
};

type Styles = typeof styles;

export default class MembersCountLabelStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};