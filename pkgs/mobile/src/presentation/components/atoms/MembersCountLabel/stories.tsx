import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MembersCountLabel from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { number } from '@storybook/addon-knobs';

storiesOf('MembersCountLabel', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <MembersCountLabel
            count={number('count', 0)}
        />
    ))
    .add('Loading', () => (
        <MembersCountLabel
            count={number('count', 0)}
            loading={true}
        />
    ))
