import React from 'react';
import ReadonlyTextFieldStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Text, View } from 'react-native';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';
import { TouchableHighlight } from 'react-native-gesture-handler';

type Props = {
    Icon?: IconNodeClass,
    title?: string,
    value?: string,
    onPress?: () => void,
} & Partial<StyleProps>;

const ReadonlyTextField: React.FC<Props> = ({
    style,
    appTheme,
    Icon,
    title,
    value,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new ReadonlyTextFieldStyles({
        style,
        appTheme,
        showIcon: !!Icon,
    }), [
        style,
        appTheme,
        !!Icon,
    ]);
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={onPress}>
                <View style={styles.inner}>
                    <View style={styles.topContainer}>
                        {Icon && (
                            <View style={styles.iconContainer}>
                                <Icon size={styles.icon.width} color={styles.icon.color} />
                            </View>
                        )}
                        <View style={styles.textContainer}>
                            <Text style={styles.title}>
                                {title}
                            </Text>
                            <Text style={styles.value} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                                {value}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.border}/>
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(ReadonlyTextField, compare);