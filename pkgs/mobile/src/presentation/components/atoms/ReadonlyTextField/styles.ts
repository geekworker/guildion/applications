import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
    showIcon?: boolean,
} & StyleProps;

const styles = ({ appTheme, style, showIcon }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const iconSize = 20;
    const iconMargin = 12;
    const iconContainerWidth = showIcon ? iconSize + iconMargin : 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
        },
        touchable: {
            width,
        },
        inner: {
            width,
        },
        topContainer: {
            width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingBottom: 12,
        },
        iconContainer: {
            width: iconContainerWidth,
            paddingRight: iconMargin,
            paddingTop: 14,
        },
        icon: {
            width: iconSize,
            height: iconSize,
            color: appTheme.element.link,
        },
        textContainer: {
            width: width - iconContainerWidth,
        },
        title: {
            fontFamily: MontserratFont.Regular,
            fontSize: 14,
            color: appTheme.element.subp1,
            textAlign: 'left',
            marginBottom: 2,
        },
        value: {
            fontFamily: MontserratFont.SemiBold,
            fontSize: 18,
            color: appTheme.element.link,
            textAlign: 'left',
            marginRight: 8,
        },
        border: {
            width,
            height: 1,
            backgroundColor: appTheme.background.subp2,
        },
    });
};

type Styles = typeof styles;

export default class ReadonlyTextFieldStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};