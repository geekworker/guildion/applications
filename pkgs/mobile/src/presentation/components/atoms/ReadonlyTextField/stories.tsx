import { storiesOf } from '@storybook/react-native';
import React from 'react';
import ReadonlyTextField from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import EntypoIcon from 'react-native-vector-icons/Entypo';

storiesOf('ReadonlyTextField', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <ReadonlyTextField
            style={new Style({
                width: number('width', 300),
                height: number('height', 50),
            })}
            title='test'
            value='testtesttest'
            Icon={(props) => <EntypoIcon {...props} name={'link'} />}
            onPress={() => {}}
        />
    ))
    .add('OnlyText', () => (
        <ReadonlyTextField
            style={new Style({
                width: number('width', 300),
                height: number('height', 50),
            })}
            title='test'
            value='testtesttest'
            onPress={() => {}}
        />
    ))