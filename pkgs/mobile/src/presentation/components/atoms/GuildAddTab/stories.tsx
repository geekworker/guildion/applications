import { Style } from '@/shared/interfaces/Style';
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildAddTab from '.';
import CenterView from '../CenterView';

storiesOf('GuildAddTab', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <GuildAddTab
            isFocused={boolean('isFocused', false)}
            style={new Style({
                width: 74,
                height: 74,
            })}
        />
    ))
