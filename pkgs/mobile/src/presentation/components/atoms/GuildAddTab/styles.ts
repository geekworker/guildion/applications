import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { MontserratFont } from '@/shared/constants/Font';

export const GUILD_TAB_ACTIVE_RATIO: number = 1.5;

const styles = ({ appTheme, style, isFocused }: StyleProps & { isFocused?: boolean }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius ?? 5,
            backgroundColor: isFocused ? appTheme.guild.tabBackgroundActive : appTheme.guild.tabBackground,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
        },
        wrapper: {
            width: Number(style?.width ?? 0) / GUILD_TAB_ACTIVE_RATIO,
            height: Number(style?.height ?? 0) / GUILD_TAB_ACTIVE_RATIO,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
        },
        icon: {
            width: (Number(style?.width ?? 0) / GUILD_TAB_ACTIVE_RATIO) - 4,
            height: (Number(style?.height ?? 0) / GUILD_TAB_ACTIVE_RATIO) - 4,
            color: appTheme.element.default,
        },
        name: {
            fontSize: 10,
            fontFamily: MontserratFont.Regular,
            color: appTheme.element.default,
            textAlign: 'center',
            width: Number(style?.width ?? 0) - 4,
            marginLeft: 2,
            marginRight: 2,
            marginTop: 2,
            height: 12,
        },
    })
};

type Styles = typeof styles;

export default class GuildAddTabStyles extends Record<ReturnType<Styles>>({
    ...styles({ isFocused: false })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { isFocused?: boolean }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};