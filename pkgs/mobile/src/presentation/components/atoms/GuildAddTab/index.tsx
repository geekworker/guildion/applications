import React from 'react';
import GuildAddTabStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { GestureResponderEvent, Text, View, TouchableHighlight } from 'react-native';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import { localizer } from '@/shared/constants/Localizer';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    isFocused?: boolean,
    onPress?: (event?: GestureResponderEvent) => void,
} & Partial<StyleProps>;

const GuildAddTab: React.FC<Props> = ({
    style,
    appTheme,
    isFocused,
    onPress,
}) => {
    const styles = React.useMemo(() => new GuildAddTabStyles({
        style,
        appTheme,
        isFocused,
    }), [
        style,
        appTheme,
        isFocused,
    ]);
    return (
        <TouchableHighlight onPress={onPress}>
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <EntypoIcon name={'plus'} size={styles.icon.width} color={styles.icon.color} />
                </View>
                <Text style={styles.name} numberOfLines={1}>
                    {localizer.dictionary.guild.home.create}
                </Text>
            </View>
        </TouchableHighlight>
    );
};

export default React.memo(GuildAddTab, compare);