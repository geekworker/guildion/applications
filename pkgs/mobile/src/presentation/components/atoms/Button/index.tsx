import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import React from 'react';
import { View } from 'react-native';
import { ButtonProps, Button as _Button } from 'react-native-elements';
import ButtonStyles from './styles';

export const ButtonStyleType = {
    Border: 'border',
    Fill: 'fill',
} as const;
export type ButtonStyleType = typeof ButtonStyleType[keyof typeof ButtonStyleType];

type Props = {
    styleType: ButtonStyleType,
} & StyleProps & Omit<ButtonProps, 'style' | 'titleStyle' | 'buttonStyle' | 'containerStyle' | 'loadingStyle' | 'disabledStyle' | 'disabledTitleStyle' | 'type'>

const Button: React.FC<Props> = (props) => {
    const {
        styleType,
        appTheme,
        style,
    } = props;
    const styles = React.useMemo(() => new ButtonStyles({
        appTheme,
        style,
    }), [
        appTheme,
        style,
    ]);
    switch(styleType) {
    case 'border': return (
        <View style={style?.toObject()}>
            <_Button
                {...props}
                style={styles.borderStyle}
                titleStyle={styles.borderTitleStyle}
                containerStyle={styles.borderContainerStyle}
                buttonStyle={styles.borderButtonStyle}
                disabledStyle={styles.borderDisabledStyle}
                disabledTitleStyle={styles.borderDisabledTitleStyle}
                TouchableComponent={props.TouchableComponent ?? appTheme?.touchableComponent}
            />
        </View>
    );
    case 'fill': return (
        <View style={style?.toObject()}>
            <_Button
                {...props}
                style={styles.fillStyle}
                titleStyle={styles.fillTitleStyle}
                containerStyle={styles.fillContainerStyle}
                buttonStyle={styles.fillButtonStyle}
                disabledStyle={styles.fillDisabledStyle}
                disabledTitleStyle={styles.fillDisabledTitleStyle}
                TouchableComponent={props.TouchableComponent ?? appTheme?.touchableComponent}
            />
        </View>
    );
    }
}

export default React.memo(Button, compare);