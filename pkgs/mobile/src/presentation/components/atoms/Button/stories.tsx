import { storiesOf } from '@storybook/react-native';
import { boolean, number, text } from '@storybook/addon-knobs';
import React from 'react';
import Button, { ButtonStyleType } from '.';
import CenterView from '../CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('Button', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Fill', () => (
        <Button
            styleType={ButtonStyleType.Fill}
            style={new Style({
                width: number('width', 200),
                height: number('height', 50),
            })}
            disabled={boolean('disabled', false)}
            loading={boolean('loading', false)}
            raised={boolean('raised', false)}
            title={text('title', 'next')}
        />
    ))
    .add('Border', () => (
        <Button
            styleType={ButtonStyleType.Border}
            style={new Style({
                width: number('width', 200),
                height: number('height', 50),
            })}
            disabled={boolean('disabled', false)}
            loading={boolean('loading', false)}
            raised={boolean('raised', false)}
            title={text('title', 'next')}
        />
    ));
