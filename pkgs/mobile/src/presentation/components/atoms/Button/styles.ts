import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const borderRadius = style?.borderRadius ?? Number(style?.height ?? 0) / 2;
    return StyleSheet.create({
        fillStyle: {
            borderRadius,
        },
        fillContainerStyle: {
            backgroundColor: style?.backgroundColor ?? appTheme.button.default,
            width: style?.width,
            borderRadius,
            height: style?.height,
        },
        fillButtonStyle: {
            backgroundColor: style?.backgroundColor ?? appTheme.button.default,
            borderRadius,
            height: style?.height,
        },
        fillTitleStyle: style?.fontSize ? {
            color: style?.color ?? appTheme?.element.default,
            textAlign: 'center',
            fontFamily: style?.fontFamily ?? MontserratFont.SemiBold,
            fontSize: style.fontSize,
        } : {
            color: style?.color ?? appTheme?.element.default,
            textAlign: 'center',
            fontFamily: style?.fontFamily ?? MontserratFont.SemiBold,
        },
        fillDisabledStyle: {
            backgroundColor: style?.backgroundColor ?? appTheme.button.disabled,
        },
        fillDisabledTitleStyle: {
            color: style?.color ?? appTheme.background.root,
            fontFamily: MontserratFont.SemiBold,
            // fontSize: style?.fontSize,
        },
        borderStyle: {
            borderRadius,
        },
        borderContainerStyle: {
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            width: style?.width,
            height: style?.height,
            borderRadius,
        },
        borderButtonStyle: {
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            borderColor: style?.borderColor ?? appTheme.button.default,
            borderWidth: 3,
            borderRadius,
            height: style?.height,
        },
        borderTitleStyle: {
            color: style?.color ?? appTheme.button.default,
            textAlign: 'center',
            fontFamily: MontserratFont.SemiBold,
            // fontSize: style?.fontSize,
        },
        borderDisabledStyle: {
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            borderColor: appTheme.button.disabled,
        },
        borderDisabledTitleStyle: {
            color: style?.color ?? appTheme.button.disabled,
            fontFamily: MontserratFont.SemiBold,
            // fontSize: style?.fontSize,
        },
    });
};

type Styles = typeof styles;

export default class ButtonStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};