import React from 'react';
import VideoPlayerComponent from './component';
import VideoPlayerPresenter from './presenter';

const VideoPlayer = (props: VideoPlayerPresenter.Input) => {
    const output = VideoPlayerPresenter.usePresenter(props);
    return <VideoPlayerComponent {...output} />;
};

export default React.memo(VideoPlayer, VideoPlayerPresenter.inputAreEqual);