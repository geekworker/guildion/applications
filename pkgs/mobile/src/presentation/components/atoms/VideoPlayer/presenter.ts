import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import VideoPlayerHooks from "./hooks";
import VideoPlayerStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File } from "@guildion/core";

namespace VideoPlayerPresenter {
    export type Input = {
        file: File,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: VideoPlayerStyles,
        appTheme: AppTheme,
        file: File,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = VideoPlayerHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default VideoPlayerPresenter;