import React from "react";
import VideoPlayerStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace VideoPlayerHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new VideoPlayerStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default VideoPlayerHooks;