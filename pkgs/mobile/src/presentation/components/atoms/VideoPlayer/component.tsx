import React from 'react';
import AsyncImage from '../AsyncImage';
import VideoPlayerPresenter from './presenter';

const VideoPlayerComponent = ({ appTheme, styles, file }: VideoPlayerPresenter.Output) => {
    return (
        <AsyncImage style={styles.getStyle('container')} url={file.url} />
    );
};

export default React.memo(VideoPlayerComponent, VideoPlayerPresenter.outputAreEqual);