import React from 'react';
import { 
    ActivityIndicator,
    GestureResponderEvent,
    TouchableOpacity,
    View,
} from 'react-native';
import { IconNode } from 'react-native-elements/dist/icons/Icon';
import IconButtonStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { BlurView } from '@react-native-community/blur';

type Props = {
    onPress?: (event: GestureResponderEvent) => void,
    icon: IconNode,
    disabled?: boolean,
    loading?: boolean,
    isBlur?: boolean,
} & StyleProps;

const IconButton: React.FC<Props> = ({ onPress, icon, style, appTheme, disabled, loading, isBlur }) => {
    const styles = React.useMemo(() => new IconButtonStyles({ style, appTheme }), [style, appTheme])
    return (
        <TouchableOpacity style={styles.highlight} onPress={onPress} disabled={disabled || loading}>
            {isBlur ? (
                <BlurView
                    style={styles.container}
                    blurType={'light'}
                    blurAmount={10}
                    reducedTransparencyFallbackColor="white"
                >
                    {loading ? (
                        <ActivityIndicator style={styles.indicator} color={styles.highlight.color} />
                    ) : icon}
                </BlurView>
            ) : (
                <View style={styles.container}>
                    {loading ? (
                        <ActivityIndicator style={styles.indicator} color={styles.highlight.color} />
                    ) : icon}
                </View>
            )}
        </TouchableOpacity>
    )
};

export default React.memo(IconButton, compare);