import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        highlight: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
            borderColor: style?.borderColor,
        },
        container: {
            paddingRight: Number(style?.borderWidth ?? 0),
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
        indicator: {
            width: 4,
            height: 4,
        },
    });
};

type Styles = typeof styles;

export default class IconButtonStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};