import { storiesOf } from '@storybook/react-native';
import { number } from '@storybook/addon-knobs';
import React from 'react';
import IconButton from '.';
import CenterView from '../CenterView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('IconButton', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <IconButton
            style={new Style({
                width: number('width', 50),
                height: number('height', 50),
                backgroundColor: fallbackAppTheme.background.subp2,
                borderColor: fallbackAppTheme.background.subp3,
                borderRadius: number('radius', 25),
                borderWidth: 1,
            })}
            icon={<Icon size={number('icon size', 24)} color={"#FFFFFF"} name={'camera'} />}
            onPress={() => {}}
        />
    ))
    .add('Blur', () => (
        <IconButton
            style={new Style({
                width: number('width', 50),
                height: number('height', 50),
                borderRadius: number('radius', 25),
            })}
            icon={<Icon size={number('icon size', 24)} color={"#FFFFFF"} name={'camera'} />}
            onPress={() => {}}
            isBlur
        />
    ))
