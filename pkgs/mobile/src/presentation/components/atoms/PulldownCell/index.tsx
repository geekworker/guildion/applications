import React from 'react';
import PulldownCellStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { View } from 'react-native';
import { Style } from '@/shared/interfaces/Style';

export type PullDownCellProps = {
    value?: string,
    title?: string,
    imageUrl?: string,
    imageStyle?: Style,
    key: string,
    onPress?: ({ key, value }: { key: string, value?: string }) => void,
} & Partial<StyleProps>;

const PulldownCell: React.FC<PullDownCellProps> = ({
    style,
    appTheme,
    key,
    title,
    value,
    onPress,
    imageStyle,
    imageUrl,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new PulldownCellStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    const $onPress = React.useCallback(() => {
        if (onPress) onPress({ key, value });
    }, [onPress, key, value]);
    return (
        <View style={styles.container}>
        </View>
    );
};

export default React.memo(PulldownCell, compare);