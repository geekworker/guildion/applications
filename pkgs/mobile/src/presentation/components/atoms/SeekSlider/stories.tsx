import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SeekSlider from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Animated } from 'react-native';
import useInterval from 'use-interval';

const TimerSlider: React.FC<{}> = () => {
    const [panActive, setPanActive] = React.useState(false);
    const [value, setValue] = React.useState(0);
    const [bufferValue, setBufferValue] = React.useState(0);
    useInterval(() => {
        if (!panActive) {
            setValue(value + 0.1);
            setBufferValue(bufferValue + 0.2)
        }
    }, 100);
    return (
        <SeekSlider
            style={new Style({
                width: number('width', 320),
                height: number('height', 6)
            })}
            maxValue={100}
            minValue={0}
            value={value}
            bufferValue={bufferValue}
            onChange={setValue}
            willPanGesture={() => setPanActive(true)}
            didPanGesture={() => setPanActive(false)}
        />
    )
}

storiesOf('SeekSlider', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SeekSlider
            style={new Style({
                width: number('width', 320),
                height: number('height', 6)
            })}
        />
    ))
    .add('ValueChange', () => (
        <SeekSlider
            style={new Style({
                width: number('width', 320),
                height: number('height', 6)
            })}
            maxValue={100}
            minValue={0}
            value={50}
            bufferValue={number('bufferValue', 60)}
        />
    ))
    .add('NoneActive', () => (
        <SeekSlider
            style={new Style({
                width: number('width', 320),
                height: number('height', 6)
            })}
            maxValue={100}
            minValue={0}
            value={50}
            transition={new Animated.Value(0)}
            bufferValue={number('bufferValue', 60)}
        />
    ))
    .add('Timer', () => (<TimerSlider/>))