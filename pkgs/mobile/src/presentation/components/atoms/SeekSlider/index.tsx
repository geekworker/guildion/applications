import React from 'react';
import SeekSliderComponent from './component';
import SeekSliderPresenter from './presenter';

const SeekSlider = (props: SeekSliderPresenter.Input) => {
    const output = SeekSliderPresenter.usePresenter(props);
    return <SeekSliderComponent {...output} />;
};

export default React.memo(SeekSlider, SeekSliderPresenter.inputAreEqual);