import React from "react";
import SeekSliderStyles from './styles';
import type SeekSliderPresenter from "./presenter";
import { Animated, PanResponder, View, Easing } from "react-native";

namespace SeekSliderHooks {
    export const useStyles = (props: SeekSliderPresenter.Input) => {
        const styles = React.useMemo(() => new SeekSliderStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SeekSliderPresenter.Input) => {
        const transition = (props.transition ?? new Animated.Value(1)).interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
        });
        const bufferWidthTransition = React.useRef(new Animated.Value(0)).current;
        const containerRef = React.useRef<View>(null);
        const panX = React.useRef(new Animated.Value(0)).current;
        const panTransition = React.useRef(new Animated.Value(0)).current;

        const [minValue, setMinValue] = React.useState(props.minValue ?? 0);
        React.useEffect(() => {
            minValue != (props.minValue ?? 0) && setMinValue(minValue ?? 0);
        }, [props.minValue]);

        const [maxValue, setMaxValue] = React.useState(props.maxValue ?? 0);
        React.useEffect(() => {
            maxValue != (props.maxValue ?? 1) && setMaxValue(maxValue ?? 1);
        }, [props.maxValue]);

        const [value, setValue] = React.useState(props.value && props.value >= minValue && props.value >= maxValue ? props.value ?? 0 : 0);
        React.useEffect(() => {
            props.value ??= minValue;
            if (value != props.value && props.value >= minValue && props.value <= maxValue) {
                setValue(props.value);
                const ratio = (props.value - minValue) / ((maxValue - minValue) || 1);
                containerRef.current?.measure((x, y, width, height, pageX, pageY) => {
                    const result = (width * ratio);
                    panX.setValue(result);
                });
            };
        }, [props.value, minValue, maxValue]);

        const [bufferValue, setBufferValue] = React.useState(props.bufferValue && props.bufferValue >= minValue && props.bufferValue >= maxValue ? props.bufferValue ?? 0 : 0);
        React.useEffect(() => {
            props.bufferValue ??= minValue;
            if (bufferValue != props.bufferValue && props.bufferValue >= minValue && props.bufferValue <= maxValue) {
                setBufferValue(props.bufferValue);
                const ratio = (props.bufferValue - minValue) / ((maxValue - minValue) || 1);
                containerRef.current?.measure((x, y, width, height, pageX, pageY) => {
                    const result = (width * ratio);
                    bufferWidthTransition.setValue(result || 0);
                });
            }
        }, [props.bufferValue, minValue, maxValue]);

        const panResponder = React.useRef(
            PanResponder.create({
                onMoveShouldSetPanResponder: () => true,
                onPanResponderGrant: (e, gestureState) => {
                    Animated.timing(panTransition, {
                        useNativeDriver: false,
                        easing: Easing.ease,
                        toValue: 1,
                        duration: 50
                    }).start();
                    containerRef.current?.measure((x, y, width, height, pageX, pageY) => {
                        const newPanX = gestureState.x0 - pageX;
                        panX.setValue(newPanX);
                        props.willPanGesture && props.willPanGesture(e, gestureState);
                        const newValue = maxValue * (newPanX / width);
                        setValue(newValue);
                        props.onChange && props.onChange(newValue);
                    });
                },
                onPanResponderMove: (e, gestureState) => {
                    containerRef.current?.measure((x, y, width, height, pageX, pageY) => {
                        const _newPanX = gestureState.x0 + gestureState.dx - pageX;
                        const newPanX = Math.max(Math.min(_newPanX, width), 0);
                        panX.setValue(newPanX);
                        props.didMovedPanGesture && props.didMovedPanGesture(e, gestureState);
                        const newValue = maxValue * (newPanX / width);
                        setValue(newValue);
                        props.onChange && props.onChange(newValue);
                    });
                },
                onPanResponderRelease: (e, gestureState) => {
                    Animated.timing(panTransition, {
                        useNativeDriver: false,
                        easing: Easing.ease,
                        toValue: 0,
                        duration: 50
                    }).start();
                    props.didPanGesture && props.didPanGesture(e, gestureState);
                },
            })
        ).current;

        const defaultHeight: number = props.style?.getAsNumber('height') ?? 0;
        const stateTransition = Animated.add(transition, panTransition);
        const heightTransition = stateTransition.interpolate({
            inputRange: [0, 2],
            outputRange: [defaultHeight / 2, defaultHeight],
        });
        const thumbSizeTransition = stateTransition.interpolate({
            inputRange: [0, 1, 2],
            outputRange: [0, 1, 1.2],
        });

        return {
            value,
            setValue,
            minValue,
            setMinValue,
            maxValue,
            setMaxValue,
            bufferValue,
            setBufferValue,
            panResponder,
            panX,
            containerRef,
            transition,
            panTransition,
            bufferWidthTransition,
            stateTransition,
            heightTransition,
            thumbSizeTransition,
        }
    }
}

export default SeekSliderHooks;