import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const thumbPlusSize: number = 8;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const borderRadius = style?.getAsNumber('borderRadius') ?? (style?.getAsNumber('height') ?? 0) / 2;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            display: "flex",
            flexDirection: 'row',
            alignItems: 'center',
        },
        inner: {
            position: 'relative',
            width: style?.width,
            height: style?.height,
            borderRadius,
        },
        background: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
            backgroundColor: appTheme.slider.playerBackgroundColor,
            borderRadius,
        },
        buffer: {
            width: style?.width,
            height: style?.height,
            backgroundColor: appTheme.slider.playerBufferColor,
            borderRadius,
        },
        middle: {
            width: style?.width,
            height: style?.height,
            borderRadius,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        bar: {
            width: style?.width,
            height: style?.height,
            backgroundColor: appTheme.slider.playerSeekColor,
            borderRadius,
        },
        foregrond: {
            width: style?.width,
            height: style?.height,
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
        },
        thumb: {
            width: (style?.getAsNumber('height') ?? 0) + thumbPlusSize,
            height: (style?.getAsNumber('height') ?? 0) + thumbPlusSize,
            backgroundColor: appTheme.slider.playerSeekColor,
            borderRadius: ((style?.getAsNumber('height') ?? 0) + thumbPlusSize) / 2,
            position: 'absolute',
            top: -thumbPlusSize / 2,
            left: 0,
        },
    })
};

type Styles = typeof styles;

export default class SeekSliderStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    static thumbPlusSize: number = thumbPlusSize;

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }

    get defaultHeight(): number {
        return Number(this.container.height) || 0;
    }

    get thumbMaxSize(): number {
        return this.defaultHeight + (thumbPlusSize * 2);
    }
};