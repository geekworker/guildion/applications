import React from 'react';
import { Animated } from 'react-native';
import SeekSliderPresenter from './presenter';

const SeekSliderComponent = ({ styles, appTheme, value, transition, minValue, maxValue, bufferValue, panResponder, panX, containerRef, panTransition, bufferWidthTransition, stateTransition, heightTransition, thumbSizeTransition }: SeekSliderPresenter.Output) => {
    const borderRadius = Animated.divide(heightTransition, 2);
    return (
        <Animated.View style={{ ...styles.container }} ref={containerRef} {...panResponder.panHandlers}>
            <Animated.View style={{ ...styles.inner, height: heightTransition, borderRadius }}>
                <Animated.View style={{ ...styles.background, height: heightTransition, borderRadius }}>
                    <Animated.View style={{ ...styles.buffer, width: bufferWidthTransition, height: heightTransition, borderRadius }} />
                </Animated.View>
                <Animated.View style={{ ...styles.middle, height: heightTransition, borderRadius }}>
                    <Animated.View
                        style={{
                            ...styles.bar,
                            width: panX,
                            height: heightTransition,
                            borderRadius,
                        }}
                    />
                </Animated.View>
                <Animated.View style={{ ...styles.foregrond, height: heightTransition, borderRadius }} >
                    <Animated.View
                        style={{
                            ...styles.thumb,
                            transform: [
                                {
                                    translateX: Animated.subtract(
                                        panX,
                                        stateTransition.interpolate({
                                            inputRange: [0, 2],
                                            outputRange: [0, styles.thumbMaxSize / 2]
                                        }),
                                    ),
                                },
                                {
                                    scale: thumbSizeTransition,
                                }
                            ]
                        }}>
                    </Animated.View>
                </Animated.View>
            </Animated.View>
        </Animated.View>
    );
};

export default React.memo(SeekSliderComponent, SeekSliderPresenter.outputAreEqual);