import React from "react";
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import SeekSliderHooks from "./hooks";
import SeekSliderStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { Animated, GestureResponderEvent, PanResponderGestureState, PanResponderInstance, View } from "react-native";
import { Style } from "@/shared/interfaces/Style";

namespace SeekSliderPresenter {
    export type Input = {
        value?: number,
        bufferValue?: number,
        maxValue?: number,
        minValue?: number,
        onChange?: (value: number) => void,
        willPanGesture?: (e: GestureResponderEvent, gestureState: PanResponderGestureState) => void,
        didMovedPanGesture?: (e: GestureResponderEvent, gestureState: PanResponderGestureState) => void,
        didPanGesture?: (e: GestureResponderEvent, gestureState: PanResponderGestureState) => void,
        transition?: Animated.Value | Animated.AnimatedInterpolation,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SeekSliderStyles,
        appTheme: AppTheme,
        value?: number,
        bufferValue?: number,
        maxValue?: number,
        minValue?: number,
        onChange?: (value: number) => void,
        transition: Animated.Value | Animated.AnimatedInterpolation,
        panResponder: PanResponderInstance,
        panX: Animated.Value,
        containerRef: React.RefObject<View>,
        panTransition: Animated.Value | Animated.AnimatedInterpolation,
        bufferWidthTransition: Animated.Value | Animated.AnimatedInterpolation,
        stateTransition: Animated.Value | Animated.AnimatedInterpolation,
        heightTransition: Animated.Value | Animated.AnimatedInterpolation,
        thumbSizeTransition: Animated.Value | Animated.AnimatedInterpolation,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SeekSliderHooks.useStyles({ ...props });
        const {
            value,
            setValue,
            minValue,
            setMinValue,
            maxValue,
            setMaxValue,
            bufferValue,
            setBufferValue,
            panResponder,
            panX,
            containerRef,
            transition,
            panTransition,
            bufferWidthTransition,
            stateTransition,
            heightTransition,
            thumbSizeTransition,
        } = SeekSliderHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            value,
            minValue,
            maxValue,
            bufferValue,
            panResponder,
            panX,
            containerRef,
            transition,
            panTransition,
            bufferWidthTransition,
            stateTransition,
            heightTransition,
            thumbSizeTransition,
        }
    }
}

export default SeekSliderPresenter;