export const SeekSliderPanStatus = {
    DEFAULT: 'default',
    ACTIVE: 'active',
    SEEKING: 'seeking',
} as const;

export type SeekSliderPanStatus = typeof SeekSliderPanStatus[keyof typeof SeekSliderPanStatus];