import React from 'react';
import GuildTabBorderStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Animated, ViewStyle } from 'react-native';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    animationStyle: Animated.AnimatedProps<ViewStyle>,
} & Partial<StyleProps>;

const GuildTabBorder: React.FC<Props> = ({
    style,
    appTheme,
    animationStyle,
}) => {
    const styles = React.useMemo(() => new GuildTabBorderStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <Animated.View style={{ ...styles.bar, ...animationStyle }} />
    )
};

export default React.memo(GuildTabBorder, compare);