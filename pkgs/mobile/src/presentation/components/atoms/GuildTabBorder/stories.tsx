import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildTabBorder from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';

storiesOf('GuildTabBorder', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildTabBorder
            style={new Style({})}
            animationStyle={new AnimatedStyle()}
        />
    ))