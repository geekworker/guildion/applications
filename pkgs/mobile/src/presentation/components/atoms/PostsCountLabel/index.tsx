import React from 'react';
import PostsCountLabelStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, View } from 'react-native';
import { localizer } from '@/shared/constants/Localizer';
import { compare } from '@/shared/modules/ObjectCompare';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

type Props = {
    count: number,
    loading?: boolean,
} & Partial<StyleProps>;

const PostsCountLabel: React.FC<Props> = ({
    style,
    appTheme,
    count,
    loading,
}) => {
    appTheme ||= fallbackAppTheme;
    const text = React.useMemo(() => localizer.dictionary.room.db.postsCounter(count), [localizer, count]);
    const styles = React.useMemo(() => new PostsCountLabelStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
        text,
    ]);
    return !loading ? (
        <View style={styles.container}>
            {loading ? (
                <SkeletonContent
                    isLoading={loading}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.badgeSkeleton]}
                />
            ) : (
                <View style={styles.badge}/>
            )}
            {loading ? (
                <SkeletonContent
                    isLoading={loading}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.labelSkeleton]}
                />
            ) : (
                <Text style={styles.label}>
                    {text}
                </Text>
            )}
        </View>
    ) : null;
};

export default React.memo(PostsCountLabel, compare);