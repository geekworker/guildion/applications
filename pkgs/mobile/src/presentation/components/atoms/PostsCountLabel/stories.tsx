import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PostsCountLabel from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { number } from '@storybook/addon-knobs';

storiesOf('PostsCountLabel', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <PostsCountLabel
            count={number('count', 0)}
        />
    ))
    .add('Loading', () => (
        <PostsCountLabel
            count={number('count', 0)}
            loading={true}
        />
    ))
