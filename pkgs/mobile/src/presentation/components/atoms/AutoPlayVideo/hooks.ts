import React from 'react';
import AutoPlayVideoStyles from './styles';
import type AutoPlayVideoPresenter from './presenter';

namespace AutoPlayVideoHooks {
    export const useStyles = (props: AutoPlayVideoPresenter.Input) => {
        const styles = React.useMemo(() => new AutoPlayVideoStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: AutoPlayVideoPresenter.Input) => {
        return {};
    }
}

export default AutoPlayVideoHooks;