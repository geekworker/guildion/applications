import React from 'react';
import AutoPlayVideoPresenter from './presenter';
import Video from 'react-native-video';

const AutoPlayVideoComponent = ({ styles, style, appTheme, animatedStyle, source, resizeMode }: AutoPlayVideoPresenter.Output) => {
    return (
        <Video
            source={source}
            resizeMode={resizeMode}
            style={style?.toStyleObject() as any}
            volume={1.0}
            muted={true}
            paused={false}
            repeat={true}
            playInBackground={false}
            playWhenInactive={false}
        />
    );
};

export default React.memo(AutoPlayVideoComponent, AutoPlayVideoPresenter.outputAreEqual);