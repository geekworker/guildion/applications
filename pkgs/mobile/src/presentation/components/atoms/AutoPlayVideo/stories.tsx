import { storiesOf } from '@storybook/react-native';
import React from 'react';
import AutoPlayVideo from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';

storiesOf('AutoPlayVideo', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <AutoPlayVideo
            style={new Style({
                width: number('width', 140),
                height: number('height', 140),
            })}
            source={require("@/assets/videos/group-watching@mobile.mp4")}
        />
    ))