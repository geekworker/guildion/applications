import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import AutoPlayVideoHooks from './hooks';
import AutoPlayVideoStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Style } from '@/shared/interfaces/Style';

namespace AutoPlayVideoPresenter {
    export type Input = {
        resizeMode?: "stretch" | "contain" | "cover" | "none" | undefined;
        source: { uri?: string | undefined, headers?: {[key: string]: string } | undefined, type?: string | undefined } | number,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: AutoPlayVideoStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        style?: Style,
        resizeMode?: "stretch" | "contain" | "cover" | "none" | undefined,
        source: { uri?: string | undefined, headers?: {[key: string]: string } | undefined, type?: string | undefined } | number,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = AutoPlayVideoHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default AutoPlayVideoPresenter;