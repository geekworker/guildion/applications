import React from 'react';
import AutoPlayVideoComponent from './component';
import AutoPlayVideoPresenter from './presenter';

const AutoPlayVideo = (props: AutoPlayVideoPresenter.Input) => {
    const output = AutoPlayVideoPresenter.usePresenter(props);
    return <AutoPlayVideoComponent {...output} />;
};

export default React.memo(AutoPlayVideo, AutoPlayVideoPresenter.inputAreEqual);