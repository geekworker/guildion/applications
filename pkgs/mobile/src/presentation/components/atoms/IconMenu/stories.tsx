import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconMenu from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { boolean, number } from '@storybook/addon-knobs';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('IconMenu', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <IconMenu
            title={'Rooms'}
            style={new Style({
                width: 84,
                height: 64,
                color: fallbackAppTheme.element.subp2,
            })}
            Icon={(props) => <Icon {...props} name={'camera'} />}
            countText={'0'}
            loading={boolean('loading', false)}
            showLeftBorder={boolean('showLeftBorder', true)}
            showRightBorder={boolean('showRightBorder', true)}
            notificationBadgeText={number('notificationBadgeText', 1)}
            connectingBadgeText={number('connectingBadgeText', 0)}
        />
    ))
    .add('Loading', () => (
        <IconMenu
            title={'Rooms'}
            style={new Style({
                width: 84,
                height: 64,
                color: fallbackAppTheme.element.subp2,
            })}
            Icon={(props) => <Icon {...props} name={'camera'} />}
            countText={'0'}
            loading={true}
            showLeftBorder={boolean('showLeftBorder', true)}
            showRightBorder={boolean('showRightBorder', true)}
            notificationBadgeText={number('notificationBadgeText', 1)}
            connectingBadgeText={number('connectingBadgeText', 0)}
        />
    ))
