import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const ICON_MENU_WIDTH_RATIO: number = 0.3;
export const ICON_MENU_HEIGHT_RATIO: number = 0.38;
const styles = ({
    style,
    appTheme,
}: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            position: 'relative',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        menuContainer: {
            ...style?.toStyleObject(),
            width: Number(style?.width ?? 0) - 2,
            height: style?.height,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
        },
        border: {
            width: 1,
            height: Number(style?.height ?? 0) - 8,
            backgroundColor: style?.color ?? appTheme.element.subp2,
        },
        notificationContainerStyle: {
            position: 'absolute',
            top: 8,
            right: 16,
        },
        connectContainerStyle: {
            position: 'absolute',
            top: 8,
            right: 32,
        },
        notificationBadgeStyle: {
            width: 16,
            height: 18,
            borderRadius: 9,
            borderWidth: 1,
            borderColor: style?.backgroundColor,
        },
        connectBadgeStyle: {
            width: 16,
            height: 18,
            borderRadius: 9,
            borderWidth: 1,
            borderColor: style?.backgroundColor,
        },
        notificationTextStyle: {
            fontSize: 10,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
        },
        connectTextStyle: {
            fontSize: 10,
            fontFamily: MontserratFont.Bold,
            color: appTheme.element.default,
        },
        name: {
            fontSize: 10,
            fontFamily: MontserratFont.Bold,
            color: style?.color ?? appTheme.element.subp2,
            textAlign: 'center',
            width: Number(style?.width ?? 0) - 8,
            marginLeft: 4,
            marginRight: 4,
            marginTop: 2,
            marginBottom: 8,
        },
        nameSkeleton: {
            height: 10,
            width: Number(style?.width ?? 0) - 8,
            marginLeft: 4,
            marginRight: 4,
            marginTop: 2,
            marginBottom: 8,
        },
        counter: {
            fontSize: 12,
            fontFamily: MontserratFont.Bold,
            color: style?.color ?? appTheme.element.subp2,
            textAlign: 'center',
            width: Number(style?.width ?? 0) - 8,
            marginLeft: 4,
            marginRight: 4,
            marginTop: 4,
            marginBottom: 4,
        },
        counterSkeleton: {
            height: 12,
            width: Number(style?.width ?? 0) - 8,
            marginLeft: 4,
            marginRight: 4,
            marginTop: 4,
            marginBottom: 4,
        },
        iconSkeleton: {
            height: 24,
            width: 24,
            borderRadius: 10,
        },
        icon: {
            height: 20,
            width: 20,
            color: appTheme.element.subp2,
        },
    });
}

type Styles = typeof styles;

export default class IconMenuStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};