import React from 'react';
import IconMenuStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Text, View, TouchableOpacity } from 'react-native';
import { Badge } from 'react-native-elements';
import { compare } from '@/shared/modules/ObjectCompare';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';

type Props = {
    onPress?: () => void,
    Icon: IconNodeClass,
    title: string,
    countText: string,
    notificationBadgeText?: string | number,
    connectingBadgeText?: string | number,
    showRightBorder?: boolean,
    showLeftBorder?: boolean,
    loading?: boolean,
} & Partial<StyleProps>;

export type IconMenuProps = Props;

const IconMenu: React.FC<Props> = ({
    style,
    appTheme,
    onPress,
    Icon,
    title,
    countText,
    notificationBadgeText,
    connectingBadgeText,
    showRightBorder,
    showLeftBorder,
    loading,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new IconMenuStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            {!!showLeftBorder && (<View style={styles.border}/>)}
            <TouchableOpacity style={styles.menuContainer} onPress={onPress} disabled={loading}>
                <SkeletonContent
                    isLoading={loading ?? false}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.nameSkeleton]}
                >
                    <Text style={styles.name}>
                        {title}
                    </Text>
                </SkeletonContent>
                <SkeletonContent
                    isLoading={loading ?? false}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.iconSkeleton]}
                >
                    <Icon size={styles.icon.width} color={styles.icon.color} />
                </SkeletonContent>
                <SkeletonContent
                    isLoading={loading ?? false}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.counterSkeleton]}
                >
                    <Text style={styles.counter}>
                        {countText}
                    </Text>
                </SkeletonContent>
                {!loading && !!notificationBadgeText && `${notificationBadgeText}`.length > 0 && (
                    <Badge
                        status={'error'}
                        containerStyle={styles.notificationContainerStyle}
                        badgeStyle={styles.notificationBadgeStyle}
                        textStyle={styles.notificationTextStyle}
                        value={notificationBadgeText}
                    />
                )}
                {!loading && !!connectingBadgeText && `${connectingBadgeText}`.length > 0 && (
                    <Badge
                        status={'success'}
                        containerStyle={styles.connectContainerStyle}
                        badgeStyle={styles.connectBadgeStyle}
                        textStyle={styles.connectTextStyle}
                        value={connectingBadgeText}
                    />
                )}
            </TouchableOpacity>
            {!!showRightBorder && (<View style={styles.border}/>)}
        </View>
    );
};

export default React.memo(IconMenu, compare);