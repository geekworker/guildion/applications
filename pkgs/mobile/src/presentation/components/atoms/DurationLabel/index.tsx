import React from 'react';
import DurationLabelStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { View, Text } from 'react-native';
import { formatMs } from '@guildion/core';

type Props = {
    durationMs: number,
} & Partial<StyleProps>;

const DurationLabel: React.FC<Props> = ({
    style,
    appTheme,
    durationMs,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new DurationLabelStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    const formatedDuration = React.useMemo(() => formatMs(durationMs), [durationMs]);
    return (
        <View style={styles.container}>
            <View style={styles.inner}>
                <Text style={styles.text}>
                    {formatedDuration}
                </Text>
            </View>
        </View>
    );
};

export default React.memo(DurationLabel, compare);