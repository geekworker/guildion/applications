import React from 'react';
import PostAttachmentStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { TouchableHighlight, View } from 'react-native';
import { File, FileType } from '@guildion/core';
import AsyncImage from '../AsyncImage';

type Props = {
    file: File,
    onPress?: (file: File) => void,
} & Partial<StyleProps>;

const PostAttachment: React.FC<Props> = ({
    style,
    appTheme,
    file,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new PostAttachmentStyles({
        style,
        appTheme,
        file,
    }), [
        style,
        appTheme,
        file,
    ]);
    const url = React.useMemo<string | null | undefined>(() => {
        switch(file.type) {
        case FileType.Image: return file.url;
        default: return undefined;
        }
    }, [file]);
    const _onPress = React.useCallback(() => {
        onPress && onPress(file);
    }, [onPress, file]);
    return (
        <View style={styles.container}>
            <TouchableHighlight style={styles.touchable} onPress={_onPress}>
                <View style={styles.inner}>
                    {url && (<AsyncImage url={url} style={styles.getStyle('media')}/>)}
                </View>
            </TouchableHighlight>
        </View>
    );
};

export default React.memo(PostAttachment, compare);