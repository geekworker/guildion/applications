import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { File } from "@guildion/core";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style, file }: StyleProps & { file: File }) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') ?? 0;
    const heightAspect = (file.heightPx ?? 1) / (file.widthPx ?? 1);
    const height = Math.min(width * 2, width * heightAspect);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            position: 'relative',
            overflow: "hidden",
            backgroundColor: 'black',
        },
        touchable: {
            width,
            height,
            position: 'relative',
            overflow: "hidden",
            backgroundColor: 'black',
        },
        inner: {
            width,
            height,
            position: 'relative',
            overflow: "hidden",
            backgroundColor: 'black',
        },
        media: {
            width,
            height,
            overflow: "hidden",
        },
    })
};

type Styles = typeof styles;

export default class PostAttachmentStyles extends Record<ReturnType<Styles>>({
    ...styles({ file: new File() })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { file: File }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};