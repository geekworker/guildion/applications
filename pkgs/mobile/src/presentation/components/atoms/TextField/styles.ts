import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        containerStyle: {
            ...style?.toStyleObject(),
        },
        disabledInputStyle: {
        },
        errorStyle: {
            fontSize: 14,
        },
        inputContainerStyle: {
            height: 44,
            borderColor: appTheme.common.transparent,
            backgroundColor: appTheme.textField.default,
            marginTop: 8,
            borderRadius: 5,
            paddingLeft: 12,
            paddingRight: 12,
            color: appTheme.textField.placeholder,
            fontFamily: MontserratFont.Regular,
        },
        inputStyle: {
            color: appTheme.textField.value,
            fontFamily: MontserratFont.Bold,
            fontSize: 18,
        },
        labelStyle: {
            color: appTheme.textField.label,
            fontFamily: MontserratFont.SemiBold,
            fontSize: 16,
        },
        leftIconContainerStyle: {
        },
        rightIconContainerStyle: {
        },
    })
};

type Styles = typeof styles;

export default class TextFieldStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};