import { storiesOf } from '@storybook/react-native';
import { boolean, number, text } from '@storybook/addon-knobs';
import React from 'react';
import TextField from '.';
import CenterView from '../CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('TextField', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <TextField
            style={new Style({
                width: number('width', 300),
                height: number('height', 50),
            })}
            disabled={boolean('disabled', false)}
            label={text('label', '')}
            placeholder={text('placeholder', 'placeholder')}
            value={text('value', 'Sample')}
        />
    ))
    .add('Label', () => (
        <TextField
            style={new Style({
                width: number('width', 300),
                height: number('height', 50),
            })}
            disabled={boolean('disabled', false)}
            label={text('label', 'USERNAME')}
            placeholder={text('placeholder', 'placeholder')}
            value={text('value', 'Sample')}
        />
    ))
    .add('Error', () => (
        <TextField
            style={new Style({
                width: number('width', 300),
                height: number('height', 50),
            })}
            disabled={boolean('disabled', false)}
            label={text('label', '')}
            errorMessage={text('errorMessage', 'Input value')}
            placeholder={text('placeholder', 'placeholder')}
            value={text('value', 'Sample')}
        />
    ))
    .add('Error with Label', () => (
        <TextField
            style={new Style({
                width: number('width', 300),
                height: number('height', 50),
            })}
            disabled={boolean('disabled', false)}
            label={text('label', 'USERNAME')}
            errorMessage={text('errorMessage', 'Input value')}
            placeholder={text('placeholder', 'placeholder')}
            value={text('value', 'Sample')}
        />
    ))
