import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import React from 'react';
import { Input, InputProps } from 'react-native-elements';
import TextFieldStyles from './styles';

type Props = {
} & StyleProps & Omit<InputProps,
    'containerStyle' | 
    'disabledInputStyle' | 
    'errorStyle' | 
    'inputContainerStyle' | 
    'inputStyle' | 
    'labelStyle' | 
    'leftIconContainerStyle' | 
    'rightIconContainerStyle'
>

const TextField: React.FC<Props> = (props) => {
    const {
        style,
        appTheme,
        ...rest
    } = props;
    const styles = React.useMemo(() => new TextFieldStyles({
        appTheme,
        style,
    }), [
        appTheme,
        style,
    ]);
    return (
        <Input
            {...rest}
            containerStyle={styles.containerStyle}
            disabledInputStyle={styles.disabledInputStyle}
            errorStyle={styles.errorStyle}
            inputContainerStyle={styles.inputContainerStyle}
            inputStyle={styles.inputStyle}
            labelStyle={styles.labelStyle}
            leftIconContainerStyle={styles.leftIconContainerStyle}
            rightIconContainerStyle={styles.rightIconContainerStyle}
            placeholderTextColor={styles.inputContainerStyle.color}
        />
    );
};

export default React.memo(TextField, compare);