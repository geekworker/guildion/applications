import { BlurView } from '@react-native-community/blur';
import React from 'react';
import { View, ImageStyle, TextStyle, ViewStyle } from 'react-native';
import styles from './styles';

type Props = {
    children: any,
    style?: ViewStyle & TextStyle & ImageStyle,
    blur?: boolean,
}

const CenterView: React.FC<Props> = ({ children, style, blur }: Props) => {
    return !blur ? (
        <View style={{ ...styles.main, ...style }} >
            {children}
        </View>
    ) :  (
        <BlurView blurType='dark' blurAmount={10} style={{ ...styles.main, ...style, backgroundColor: undefined }} >
            {children}
        </BlurView>
    )
}

export default CenterView;