import { AppThemes } from "@/shared/constants/AppTheme";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
    main: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: AppThemes.Dark.background.subm1,
    },
});
