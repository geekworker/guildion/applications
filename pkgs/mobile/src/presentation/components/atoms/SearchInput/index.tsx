import React from 'react';
import SearchInputStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import SearchBar from '@nghinv/react-native-search-bar';
import { View } from 'react-native';

export type SearchInputProps = {
} & Partial<StyleProps> & Partial<Omit<React.ComponentProps<typeof SearchBar>, 'style' | 'containerStyle' | 'textInputStyle' | 'cancelTitleStyle' | 'searchIcon' | 'clearIcon' | 'theme' | 'isDarkTheme'>>;

const SearchInput: React.FC<SearchInputProps> = (props) => {
    let {
        appTheme,
        style,
    } = props;
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new SearchInputStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={style?.toObject()}>
            <SearchBar
                {...props}
                containerStyle={styles.containerStyle}
                textInputStyle={styles.textInputStyle}
                cancelTitleStyle={styles.cancelTitleStyle}
                theme={appTheme.searchBarTheme}
            />
        </View>
    );
};

export default React.memo(SearchInput, compare);