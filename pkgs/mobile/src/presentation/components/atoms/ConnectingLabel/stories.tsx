import { storiesOf } from '@storybook/react-native';
import React from 'react';
import ConnectingLabel from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { number } from '@storybook/addon-knobs';

storiesOf('ConnectingLabel', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <ConnectingLabel
            count={number('count', 0)}
        />
    ))
    .add('Loading', () => (
        <ConnectingLabel
            count={number('count', 0)}
            loading={true}
        />
    ))
