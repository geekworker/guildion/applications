import { storiesOf } from '@storybook/react-native';
import React from 'react';
import ButtonRow from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import Icon from 'react-native-vector-icons/FontAwesome';
import { number } from '@storybook/addon-knobs';

storiesOf('ButtonRow', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <ButtonRow
            style={new Style({
                width: number('width', 380),
            })}
            Icon={(props) => <Icon {...props} name={'camera'} />}
            text={'SAMPLE'}
        />
    ))