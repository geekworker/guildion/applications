import React from 'react';
import ButtonRowStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';
import { Text, TouchableOpacity, View } from 'react-native';
import FeatherIcon from 'react-native-vector-icons/Feather';

type Props = {
    text?: string,
    Icon?: IconNodeClass,
    onPress?: () => void,
} & Partial<StyleProps>;

const ButtonRow: React.FC<Props> = ({
    style,
    appTheme,
    text,
    Icon,
    onPress,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new ButtonRowStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPress} style={styles.touchable}>
                <View style={styles.inner}>
                    {Icon && (
                        <View style={styles.icon}>
                            <Icon size={styles.icon.width} color={styles.icon.color} />
                        </View>
                    )}
                    <Text style={{ ...styles.text, marginLeft: !!Icon ? styles.text.marginLeft : 0 }} numberOfLines={0}>
                        {text}
                    </Text>
                    <View style={styles.chevron}>
                        <FeatherIcon name={'chevron-right'} size={styles.chevron.width} color={styles.chevron.color} />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default React.memo(ButtonRow, compare);