import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius ?? 5,
            paddingLeft: 0,
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            padding: 0,
        },
        touchable: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius ?? 5,
        },
        inner: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius ?? 5,
            borderWidth: 1,
            borderColor: style?.borderColor ?? appTheme.background.subp3,
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingLeft: style?.paddingLeft ?? 20,
            paddingRight: style?.paddingRight ?? 20,
            paddingTop: style?.paddingTop ?? 10,
            paddingBottom: style?.paddingBottom ?? 10,
            position: 'relative',
        },
        icon: {
            width: 20,
            height: 20,
            color: appTheme.element.default,
        },
        text: {
            fontFamily: MontserratFont.Bold,
            fontSize: 14,
            color: appTheme.element.default,
            textAlign: 'left',
            marginLeft: 12,
        },
        chevron: {
            position: 'absolute',
            right: 8,
            width: 16,
            height: 16,
            color: appTheme.element.subp15,
        },
    })
};

type Styles = typeof styles;

export default class ButtonRowStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};