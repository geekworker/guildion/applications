import React from 'react';
import RoundAnimatableButtonComponent from './component';
import RoundAnimatableButtonPresenter from './presenter';

const RoundAnimatableButton = (props: RoundAnimatableButtonPresenter.Input) => {
    const output = RoundAnimatableButtonPresenter.usePresenter(props);
    return <RoundAnimatableButtonComponent {...output} />;
};

export default React.memo(RoundAnimatableButton, RoundAnimatableButtonPresenter.inputAreEqual);