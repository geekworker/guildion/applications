import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoundAnimatableButtonHooks from "./hooks";
import RoundAnimatableButtonStyles from './styles';
import React from 'react';
import { IconNode } from 'react-native-elements/dist/icons/Icon';
import { Animated } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoundAnimatableButtonPresenter {
    export type Input = {
        children?: React.ReactNode,
        icon: IconNode,
        title: string,
        animated?: boolean,
        onPress?: () => void,
        translation?: Animated.Value | Animated.AnimatedInterpolation,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoundAnimatableButtonStyles,
        appTheme: AppTheme,
        icon: IconNode,
        title: string,
        animated?: boolean,
        translation: Animated.Value | Animated.AnimatedInterpolation,
        onPress?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoundAnimatableButtonHooks.useStyles({ ...props });
        const translation = props.translation ?? RoundAnimatableButtonHooks.useAnimation({ ...props });
        return {
            ...props,
            styles,
            translation,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoundAnimatableButtonPresenter;