import React from "react";
import RoundAnimatableButtonStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Animated } from "react-native";

namespace RoundAnimatableButtonHooks {
    export const useStyles = (props: StyleProps & { title: string }) => {
        const styles = React.useMemo(() => new RoundAnimatableButtonStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useAnimation = (props: { animated?: boolean }): Animated.Value => {
        const translation = React.useRef(new Animated.Value(0)).current;
        React.useEffect(() => {
            const toValue = !!props.animated ? 0 : 1;
            Animated.spring(translation, {
                // duration: 0.3,
                // easing: Easing.linear,
                overshootClamping: false,
                useNativeDriver: false,
                toValue,
            }).start();
        }, [props.animated]);
        return translation;
    }
}

export default RoundAnimatableButtonHooks