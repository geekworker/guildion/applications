import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoundAnimatableButton from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { boolean, number } from '@storybook/addon-knobs';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';

storiesOf('RoundAnimatableButton', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <RoundAnimatableButton
            title={'Create Room'}
            style={new Style({
                width: 164,
                height: 44,
                borderRadius: 22,
                color: fallbackAppTheme.element.link,
                backgroundColor: fallbackAppTheme.background.subp2,
                fontSize: 12,
                fontFamily: MontserratFont.Bold,
            })}
            icon={<Icon size={number('icon size', 18)} color={fallbackAppTheme.element.link} name={'camera'} />}
            animated={boolean('animated', false)}
            onPress={() => {}}
        />
    ))
    .add('ChangedChars', () => (
        <RoundAnimatableButton
            title={'Test'}
            style={new Style({
                width: 124,
                height: 44,
                borderRadius: 22,
                color: fallbackAppTheme.element.link,
                backgroundColor: fallbackAppTheme.background.subp2,
                fontSize: 12,
                fontFamily: MontserratFont.Bold,
            })}
            icon={<Icon size={number('icon size', 18)} color={fallbackAppTheme.element.link} name={'camera'} />}
            animated={boolean('animated', false)}
            onPress={() => {}}
        />
    ))
