import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const PADDING: number = 20;

const styles = ({ appTheme, style, title }: StyleProps & { title: string }) => {
    appTheme ||= fallbackAppTheme;
    const innerHeight = Number(style?.height ?? 0) - 26;
    return StyleSheet.create({
        wrapper: {
            ...style?.toStyleObject(),
        },
        container: {
            backgroundColor: style?.backgroundColor,
            borderRadius: Number(style?.height ?? 0) / 2,
            width: style?.width,
            height: style?.height,
            overflow: 'hidden',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
        },
        icon: {
            width: innerHeight,
            height: innerHeight,
            color: appTheme.element.link,
        },
        title: {
            color: style?.color,
            fontSize: style?.fontSize,
            fontFamily: style?.fontFamily,
            textAlign: 'left',
            marginLeft: 8,
            width: Number(style?.width ?? 0) - innerHeight - 8 - PADDING - PADDING,
        },
    })
};

type Styles = typeof styles;

export default class RoundAnimatableButtonStyles extends Record<ReturnType<Styles>>({
    ...styles({ title: '' })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { title: string }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};