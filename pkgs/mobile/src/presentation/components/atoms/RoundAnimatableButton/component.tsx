import React from 'react';
import { Animated, TouchableOpacity } from 'react-native';
import RoundAnimatableButtonPresenter from './presenter';

const RoundAnimatableButtonComponent = ({ styles, title, icon, translation, onPress, animated }: RoundAnimatableButtonPresenter.Output) => {
    return (
        <Animated.View style={{
            ...styles.wrapper,
            width: translation.interpolate({
                inputRange: [0, 1],
                outputRange: [Number(styles.wrapper.height ?? 0), Number(styles.wrapper.width ?? 0)],
            }),
        }}>
            <TouchableOpacity onPress={onPress}>
                <Animated.View style={{
                    ...styles.container,
                    width: translation.interpolate({
                        inputRange: [0, 1],
                        outputRange: [Number(styles.container.height ?? 0), Number(styles.container.width ?? 0)],
                    }),
                }}>
                    {icon}
                    <Animated.Text numberOfLines={1} ellipsizeMode={'clip'} style={{
                        ...styles.title,
                        opacity: translation,
                        width: translation.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, Number(styles.title.width ?? 0)]
                        }),
                        marginLeft: translation.interpolate({
                            inputRange: [0, 1],
                            outputRange: [0, Number(styles.title.marginLeft ?? 0)]
                        }),
                    }}>
                        {title}
                    </Animated.Text>
                </Animated.View>
            </TouchableOpacity>
        </Animated.View>
    );
};

export default React.memo(RoundAnimatableButtonComponent, RoundAnimatableButtonPresenter.outputAreEqual);