import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import ControlButtonHooks from './hooks';
import ControlButtonStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';
import { Animated } from 'react-native';

namespace ControlButtonPresenter {
    export type Input = {
        title?: string,
        activeTitle?: string,
        Icon?: IconNodeClass,
        ActiveIcon?: IconNodeClass,
        onPress?: (active?: boolean) => void,
        disabled?: boolean,
        active?: boolean,
        shadow?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: ControlButtonStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        title?: string,
        activeTitle?: string,
        Icon?: IconNodeClass,
        ActiveIcon?: IconNodeClass,
        onPress?: () => void,
        active?: boolean,
        transition: Animated.Value | Animated.AnimatedInterpolation,
        disabled?: boolean,
        shadow?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = ControlButtonHooks.useStyles({ ...props });
        const {
            active,
            transition,
            onPress,
        } = ControlButtonHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            active,
            transition,
            onPress,
        }
    }
}

export default ControlButtonPresenter;