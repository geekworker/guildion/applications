import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const defaultFontSize = 16;
const defaultMarginTop = 8;
export const calcHeight = ({ width, fontSize = defaultFontSize }: { width: number, fontSize?: number }) => width + (fontSize || defaultFontSize) + defaultMarginTop;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const fontSize = style?.getAsNumber('fontSize') || defaultFontSize;
    const marginTop = defaultMarginTop;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || (width + fontSize + marginTop);
    const padding = style?.getAsNumber('padding') || ((width / 4) + 2);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        touchable: {
            width,
            height,
        },
        inner: {
            width,
            height,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            opacity: 1,
        },
        inactive: {
            opacity: 0.6,
        },
        title: {
            width,
            fontSize,
            fontFamily: MontserratFont.Regular,
            color: style?.color ?? appTheme.element.default,
            textAlign: 'center',
            marginTop,
        },
        activeTitle: {
            width,
            fontSize,
            fontFamily: MontserratFont.Regular,
            color: style?.color ?? appTheme.element.default,
            textAlign: 'center',
            marginTop,
        },
        button: {
            width,
            height: width,
            borderRadius: width / 2,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
            borderColor: style?.borderColor ?? appTheme.background.subp2,
        },
        icon: {
            color: style?.color ?? appTheme.element.default,
            width: width - (padding * 2),
        },
        activeIcon: {
            color: style?.borderColor ?? appTheme.element.main,
            width: width - (padding * 2),
        },
        shawdow: {
            shadowColor: style?.shadowColor ?? appTheme.shadow.defaultShadowColor,
            shadowOpacity: style?.shadowOpacity ?? appTheme.shadow.thickShadowOpacity,
            shadowRadius: style?.shadowRadius ?? 4,
            shadowOffset: style?.shadowOffset ?? { width: 2, height: 2 },
        }
    })
};

type Styles = typeof styles;

export default class ControlButtonStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};