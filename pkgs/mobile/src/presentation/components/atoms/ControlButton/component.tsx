import { TouchableOpacity } from '@gorhom/bottom-sheet';
import { BlurView } from '@react-native-community/blur';
import React from 'react';
import { Animated, Text } from 'react-native';
import ControlButtonPresenter from './presenter';

const ControlButtonComponent = ({ styles, appTheme, animatedStyle, onPress, title, activeTitle, Icon, ActiveIcon, active, disabled, transition, shadow }: ControlButtonPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableOpacity style={styles.touchable} activeOpacity={0.2} onPress={onPress} disabled={disabled}>
                <Animated.View
                    style={{
                        ...styles.inner,
                        opacity: transition.interpolate({
                            inputRange: [0, 1],
                            outputRange: [styles.inactive.opacity, styles.inner.opacity],
                        }),
                        ...(shadow ? styles.shawdow : {}),
                    }}
                >
                    <BlurView
                        style={{
                            ...styles.button,
                            backgroundColor: !!active ? 'white' : undefined,
                            borderColor: !!active ? 'white' : styles.button.borderColor,
                        }}
                        blurType={!!active ? 'light' : 'dark'}
                        blurAmount={10}
                        reducedTransparencyFallbackColor="white"
                    >
                        {!!ActiveIcon && !!active ? (
                            <ActiveIcon size={styles.icon.width} color={String(styles.activeIcon.color)} />
                        ) : !!Icon && !!active ? (
                            <Icon size={styles.icon.width} color={String(styles.activeIcon.color)} />
                        ) : Icon ? (
                            <Icon size={styles.icon.width} color={String(styles.icon.color)} />
                        ) : null}
                    </BlurView>
                    {!!active ? (
                        <Text
                            style={styles.activeTitle}
                            numberOfLines={1}
                            minimumFontScale={0.8}
                            adjustsFontSizeToFit
                        >
                            {activeTitle ?? title}
                        </Text>
                    ) : (
                        <Text
                            style={styles.title}
                            numberOfLines={1}
                            minimumFontScale={0.8}
                            adjustsFontSizeToFit
                        >
                            {title}
                        </Text>
                    )}
                </Animated.View>
            </TouchableOpacity>
        </Animated.View>
    );
};

export default React.memo(ControlButtonComponent, ControlButtonPresenter.outputAreEqual);