import React from 'react';
import ControlButtonComponent from './component';
import ControlButtonPresenter from './presenter';

const ControlButton = (props: ControlButtonPresenter.Input) => {
    const output = ControlButtonPresenter.usePresenter(props);
    return <ControlButtonComponent {...output} />;
};

export default React.memo(ControlButton, ControlButtonPresenter.inputAreEqual);