import { storiesOf } from '@storybook/react-native';
import React from 'react';
import ControlButton from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

storiesOf('ControlButton', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView blur>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <ControlButton
            style={new Style({
                width: 72,
            })}
            title={'test'}
            Icon={(props) => <FontAwesome {...props} name={'camera'} />}
        />
    ))
    .add('Active', () => (
        <ControlButton
            style={new Style({
                width: 72,
            })}
            title={'inactive'}
            activeTitle={'active'}
            active
            Icon={(props) => <FontAwesome {...props} name={'camera'} />}
            ActiveIcon={(props) => <FontAwesome {...props} name={'list'} />}
        />
    ))
    .add('Disabled', () => (
        <ControlButton
            style={new Style({
                width: 72,
            })}
            disabled
            title={'disabled'}
            Icon={(props) => <FontAwesome {...props} name={'camera'} />}
        />
    ))