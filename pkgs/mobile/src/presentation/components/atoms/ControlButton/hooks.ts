import React from 'react';
import ControlButtonStyles from './styles';
import type ControlButtonPresenter from './presenter';
import { Animated } from 'react-native';

namespace ControlButtonHooks {
    export const useStyles = (props: ControlButtonPresenter.Input) => {
        const styles = React.useMemo(() => new ControlButtonStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: ControlButtonPresenter.Input) => {
        const [active, setActive] = React.useState(props.active);
        const transition = React.useRef(new Animated.Value(!!props.disabled ? 1 : 0)).current;
        React.useEffect(() => {
            if (active !== props.active) setActive(props.active);
        }, [props.active]);
        React.useEffect(() => {
            Animated.spring(transition, {
                toValue: !!props.disabled ? 0 : 1,
                overshootClamping: false,
                useNativeDriver: true,
            }).start();
        }, [props.disabled]);
        const onPress = React.useCallback(() => {
            if (props.disabled) return;
            let $active = active;
            switch(active) {
            case false: $active = true; break;
            case true: $active = false; break;
            case undefined: break;
            }
            setActive($active);
            if (props.onPress) props.onPress($active);
        }, [props.onPress, active, props.disabled]);
        return {
            active,
            transition,
            onPress,
        };
    }
}

export default ControlButtonHooks;