import { storiesOf } from '@storybook/react-native';
import React from 'react';
import TitleSectionListHeader from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { text } from '@storybook/addon-knobs';

storiesOf('TitleSectionListHeader', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <TitleSectionListHeader
            style={new Style({
                width: 280,
                height: 44,
            })}
            title={text('title', 'test')}
        />
    ))