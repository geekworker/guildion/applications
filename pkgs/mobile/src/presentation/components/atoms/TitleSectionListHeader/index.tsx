import React from 'react';
import { View, Text } from 'react-native';
import TitleSectionListHeaderStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';

type Props = {
    title?: string,
} & Partial<StyleProps>;

const TitleSectionListHeader: React.FC<Props> = ({
    style,
    appTheme,
    title,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new TitleSectionListHeaderStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <Text style={styles.title}>
                {title ?? ''}
            </Text>
        </View>
    );
};

export default React.memo(TitleSectionListHeader, compare);