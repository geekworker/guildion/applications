import React from 'react';
import { Animated } from 'react-native';
import WelcomeBalloonStyles from './styles';
import useInterval from 'use-interval';
import RootRedux from '@/infrastructure/Root/redux';
import { CURRENT_CONFIG } from '@/shared/constants/Config';
import StyleProps from '@/shared/interfaces/StyleProps';

namespace WelcomeBalloonHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new WelcomeBalloonStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useAnimated = (initialAnimated?: boolean) => {
        const [animated, setAnimated] = React.useState<boolean>(!!initialAnimated);
        React.useEffect(() => {
            !!initialAnimated != animated && setAnimated(!!initialAnimated);
        }, [initialAnimated]);
        return animated;
    }

    export const useAnimation = ({ diameter, animated, interval, reverse }: { diameter: number, interval?: number, animated?: boolean, reverse?: boolean }): {
        translationX: Animated.Value,
        translationY: Animated.Value,
    } => {
        const translationY = React.useRef(new Animated.Value(0.00001)).current;
        const translationX = React.useRef(new Animated.Value(0.00001)).current;
        const center = React.useRef(diameter).current;
        const radius = React.useRef(diameter / 10).current;
        const [t, setT] = React.useState(0);
        const context = React.useContext(RootRedux.Context);
        const isVisible = CURRENT_CONFIG.NODE_ENV == 'storybook' ? true : context.state.isVisible;
        useInterval(() => {
            if (!animated) return;
            const x = center + Math.cos(t) * radius;
            const y = center + Math.sin(t) * radius;
            setT(reverse ? t - 0.01 : t + 0.01);
            translationX.setValue(x);
            translationY.setValue(y);
        }, !!animated ? (isVisible ? interval || 10 : Number.MAX_SAFE_INTEGER) : false);
        return {
            translationX,
            translationY,
        }
    }
}

export default WelcomeBalloonHooks;