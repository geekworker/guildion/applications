import React from 'react';
import { Animated } from 'react-native';
import WelcomeBalloonPresenter from './presenter';

const WelcomeBalloon: React.FC<WelcomeBalloonPresenter.Output> = ({ styles, type, translationX, translationY }) => {
    return (
        <Animated.View style={{
            ...styles[type],
            transform: [
                { translateX: translationX },
                { translateY: translationY }
            ],
        }} />
    ); 
};

export default React.memo(WelcomeBalloon, WelcomeBalloonPresenter.outputAreEqual);
