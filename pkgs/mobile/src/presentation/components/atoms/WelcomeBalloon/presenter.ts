import StyleProps from "@/shared/interfaces/StyleProps";
import { compare } from "@/shared/modules/ObjectCompare";
import { Animated } from "react-native";
import WelcomeBalloonHooks from "./hooks";
import WelcomeBalloonStyles from './styles';

namespace WelcomeBalloonPresenter {
    export const Type = {
        Fill: 'fill',
        Border: 'border',
    } as const;
    
    export type Type = typeof Type[keyof typeof Type];
    
    export type Input = {
        type: Type,
        animated?: boolean,
        reverse?: boolean,
        interval?: number,
    } & StyleProps;
    
    export type Output = {
        styles: WelcomeBalloonStyles,
        type: Type,
        translationX: Animated.Value,
        translationY: Animated.Value,
    }

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const diameter = Number(props.style?.width ?? props.style?.height ?? 0);
        const animated = WelcomeBalloonHooks.useAnimated(props.animated);
        const translations = WelcomeBalloonHooks.useAnimation({ ...props, diameter: diameter, animated });
        const styles = WelcomeBalloonHooks.useStyles({ ...props });
        return {
            ...props,
            ...translations,
            styles,
        }
    }
}

export default WelcomeBalloonPresenter;