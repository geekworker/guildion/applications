import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        fill: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
            backgroundColor: style?.backgroundColor ?? appTheme.welcome.balloon,
            position: 'absolute',
            top: style?.top,
            left: style?.left,
            bottom: style?.bottom,
            right: style?.right,
            opacity: .5,
        },
        border: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
            backgroundColor: style?.backgroundColor ?? appTheme.common.transparent,
            borderColor: style?.borderColor ?? appTheme.welcome.balloon,
            borderWidth: 10,
            position: 'absolute',
            top: style?.top,
            left: style?.left,
            bottom: style?.bottom,
            right: style?.right,
            opacity: .5,
        },
    })
};

type Styles = typeof styles;

export default class WelcomeBalloonStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};