import { storiesOf } from '@storybook/react-native';
import { boolean, number } from '@storybook/addon-knobs';
import React from 'react';
import WelcomeBalloon from '.';
import CenterView from '../CenterView';
import WelcomeBalloonPresenter from './presenter';
import { Style } from '@/shared/interfaces/Style';

storiesOf('WelcomeBalloon', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Fill', () => (
        <WelcomeBalloon
            type={WelcomeBalloonPresenter.Type.Fill}
            style={new Style({
                width: number('balloon diameter', 130),
                height: number('balloon diameter', 130),
                borderRadius: number('balloon diameter', 130) / 2,
                top: 150,
                left: 0,
            })}
            animated={boolean('balloon animation', true)}
        />
    ))
    .add('Border', () => (
        <WelcomeBalloon
            type={WelcomeBalloonPresenter.Type.Border}
            style={new Style({
                width: number('balloon diameter', 130),
                height: number('balloon diameter', 130),
                borderRadius: number('balloon diameter', 130) / 2,
                top: 150,
                left: 0,
            })}
            animated={boolean('balloon animation', true)}
        />
    ));
