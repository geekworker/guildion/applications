import React from 'react';
import LinerLoadingIndicatorPresenter from './presenter';
import {
    View,
    Animated,
} from 'react-native';
import { barHeight, barActiveHeight } from './styles';

const LinerLoadingIndicator: React.FC<LinerLoadingIndicatorPresenter.Output> = ({ styles, animatedValues }) => {
    return (
        <View style={styles.container}>
            <Animated.View
                style={{
                    ...styles.indicator,
                    transform: [{
                        scaleY: animatedValues.translation1.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, barActiveHeight / barHeight],
                        }),
                    }],
                }}
            />
            <Animated.View
                style={{
                    ...styles.indicator,
                    transform: [{
                        scaleY: animatedValues.translation2.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, barActiveHeight / barHeight],
                        }),
                    }],
                }}
            />
            <Animated.View
                style={{
                    ...styles.indicator,
                    transform: [{
                        scaleY: animatedValues.translation3.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, barActiveHeight / barHeight],
                        }),
                    }],
                }}
            />
            <Animated.View
                style={{
                    ...styles.indicator,
                    transform: [{
                        scaleY: animatedValues.translation4.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, barActiveHeight / barHeight],
                        }),
                    }],
                }}
            />
            <Animated.View
                style={{
                    ...styles.indicator,
                    transform: [{
                        scaleY: animatedValues.translation5.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, barActiveHeight / barHeight],
                        }),
                    }],
                }}
            />
            <Animated.View
                style={{
                    ...styles.indicator,
                    transform: [{
                        scaleY: animatedValues.translation6.interpolate({
                            inputRange: [0, 1],
                            outputRange: [1, barActiveHeight / barHeight],
                        }),
                    }],
                }}
            />
        </View>
    ); 
};

export default React.memo(LinerLoadingIndicator, LinerLoadingIndicatorPresenter.outputAreEqual);
