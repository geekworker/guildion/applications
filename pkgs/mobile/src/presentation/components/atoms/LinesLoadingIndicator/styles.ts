import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const barHeight: number = 44;
export const barActiveHeight: number = 64;
export const barWidth: number = 8;
export const barActiveWidth: number = 8;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: 90,
            height: 64,
            flexDirection: 'row',
            alignItems: 'center',
        },
        indicator: {
            width: barWidth,
            marginLeft: 6,
            marginRight: 6,
            backgroundColor: appTheme.indicator.active,
            height: barHeight,
            borderRadius: 4,
        },
        activeIndicator: {
            width: barActiveWidth,
            marginLeft: 12,
            marginRight: 12,
            backgroundColor: appTheme.indicator.active,
            height: barActiveHeight,
            borderRadius: 4,
        },
    })
};

type Styles = typeof styles;

export default class LinesLoadingIndicatorStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};