import React from 'react';
import LinesLoadingIndicatorStyles from './styles';
import { Animated, Easing } from 'react-native';
import StyleProps from '@/shared/interfaces/StyleProps';

namespace LinerLoadingIndicatorHooks {
    export const useStyles = ({
        appTheme,
    }: StyleProps) => {
        const styles = React.useMemo(() => new LinesLoadingIndicatorStyles({
            appTheme: appTheme!,
        }), [
            appTheme,
        ]);
        return styles;
    }

    export const useAnimation = (): {
        translation1: Animated.Value,
        translation2: Animated.Value,
        translation3: Animated.Value,
        translation4: Animated.Value,
        translation5: Animated.Value,
        translation6: Animated.Value,
    } => {
        const translation1 = React.useRef(new Animated.Value(0)).current;
        const translation2 = React.useRef(new Animated.Value(0)).current;
        const translation3 = React.useRef(new Animated.Value(0)).current;
        const translation4 = React.useRef(new Animated.Value(0)).current;
        const translation5 = React.useRef(new Animated.Value(0)).current;
        const translation6 = React.useRef(new Animated.Value(0)).current;
        const duration = 300;
        React.useEffect(() => {
            const animation = Animated.sequence([
                Animated.parallel([
                    Animated.timing(translation6, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 0,
                    }),
                    Animated.timing(translation1, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 1,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing(translation1, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 0,
                    }),
                    Animated.timing(translation2, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 1,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing(translation2, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 0,
                    }),
                    Animated.timing(translation3, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 1,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing(translation3, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 0,
                    }),
                    Animated.timing(translation4, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 1,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing(translation4, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 0,
                    }),
                    Animated.timing(translation5, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 1,
                    }),
                ]),
                Animated.parallel([
                    Animated.timing(translation5, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 0,
                    }),
                    Animated.timing(translation6, {
                        duration,
                        easing: Easing.ease,
                        useNativeDriver: true,
                        toValue: 1,
                    }),
                ]),
            ]);
            Animated.loop(animation).start();
        }, []);
        return {
            translation1,
            translation2,
            translation3,
            translation4,
            translation5,
            translation6,
        }
    }
}

export default LinerLoadingIndicatorHooks;