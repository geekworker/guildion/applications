import StyleProps from "@/shared/interfaces/StyleProps";
import { compare } from "@/shared/modules/ObjectCompare";
import { Animated } from "react-native";
import LinerLoadingIndicatorHooks from "./hooks";
import LinesLoadingIndicatorStyles from './styles';

namespace LinerLoadingIndicatorPresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: LinesLoadingIndicatorStyles,
        animatedValues: {
            translation1: Animated.Value,
            translation2: Animated.Value,
            translation3: Animated.Value,
            translation4: Animated.Value,
            translation5: Animated.Value,
            translation6: Animated.Value,
        },
    }

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = LinerLoadingIndicatorHooks.useStyles({ ...props });
        const animatedValues = LinerLoadingIndicatorHooks.useAnimation();
        return {
            ...props,
            styles,
            animatedValues,
        }
    }
}

export default LinerLoadingIndicatorPresenter;