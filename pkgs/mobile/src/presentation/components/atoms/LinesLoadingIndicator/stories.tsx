import { storiesOf } from '@storybook/react-native';
import React from 'react';
import LinesLoadingIndicator from '.';
import CenterView from '../CenterView';

storiesOf('LinesLoadingIndicator', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Animation', () => (
        <LinesLoadingIndicator/>
    ));
