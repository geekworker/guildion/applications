import React from 'react';
import LinerLoadingIndicatorComponent from './component';
import LinerLoadingIndicatorPresenter from './presenter';

const LinerLoadingIndicator = (props: LinerLoadingIndicatorPresenter.Input) => {
    const output = LinerLoadingIndicatorPresenter.usePresenter(props);
    return <LinerLoadingIndicatorComponent {...output} />;
};

export default React.memo(LinerLoadingIndicator, LinerLoadingIndicatorPresenter.inputAreEqual);