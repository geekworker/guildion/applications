import { PlayerStatus } from '@guildion/core';
import React from 'react';
import { ActivityIndicator, Animated, TouchableWithoutFeedback } from 'react-native';
import TouchableCircleRipple from '../TouchableCircleRipple';
import PlayerButtonPresenter from './presenter';

const PlayerButtonComponent = ({ styles, appTheme, animatedStyle, translation, onPress, status }: PlayerButtonPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableCircleRipple
                style={styles.getStyle('touchable')}
                filterStyle={styles.getStyle('filter')}
                onPress={onPress}
                disabled={status === PlayerStatus.LOADING}
            >
                <Animated.View style={{ ...styles.inner }}>
                    {status !== PlayerStatus.LOADING && (
                        <Animated.View
                            style={{
                                ...styles.leftBorderPause,
                                width: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.leftBorderPause.width, styles.leftBorderPlay.width],
                                }),
                                height: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.leftBorderPause.height, styles.leftBorderPlay.height],
                                }),
                                borderLeftWidth: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.leftBorderPause.borderLeftWidth, styles.leftBorderPlay.borderLeftWidth],
                                }),
                                borderTopWidth: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.leftBorderPause.borderTopWidth, styles.leftBorderPlay.borderTopWidth],
                                }),
                                borderBottomWidth: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.leftBorderPause.borderBottomWidth, styles.leftBorderPlay.borderBottomWidth],
                                }),
                                backgroundColor: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.leftBorderPause.backgroundColor, styles.leftBorderPlay.backgroundColor],
                                }),
                                transform: [
                                    {
                                        translateX: translation.interpolate({
                                            inputRange: [0, 1],
                                            outputRange: [0, styles.leftBorderPlay.transform[0].translateX]
                                        })
                                    },
                                ],
                            }}
                        />
                    )}
                    {status !== PlayerStatus.LOADING && (
                        <Animated.View
                            style={{
                                ...styles.rightBorderPause,
                                width: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.rightBorderPause.width, styles.rightBorderPlay.width],
                                }),
                                height: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.rightBorderPause.height, styles.rightBorderPlay.height],
                                }),
                                opacity: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [1, styles.rightBorderPlay.opacity],
                                }),
                                borderLeftWidth: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.rightBorderPause.borderLeftWidth, styles.rightBorderPlay.borderLeftWidth],
                                }),
                                borderTopWidth: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.rightBorderPause.borderTopWidth, styles.rightBorderPlay.borderTopWidth],
                                }),
                                borderBottomWidth: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.rightBorderPause.borderBottomWidth, styles.rightBorderPlay.borderBottomWidth],
                                }),
                                backgroundColor: translation.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [styles.rightBorderPause.backgroundColor, styles.rightBorderPlay.backgroundColor],
                                }),
                                transform: [
                                    {
                                        translateX: translation.interpolate({
                                            inputRange: [0, 1],
                                            outputRange: [0, styles.rightBorderPlay.transform[0].translateX]
                                        })
                                    },
                                ],
                            }}
                        />
                    )}
                    {status === PlayerStatus.LOADING && (
                        <ActivityIndicator style={styles.indicator} color={styles.indicator.color} size={'large'} />
                    )}
                </Animated.View>
            </TouchableCircleRipple>
        </Animated.View>
    );
};

export default React.memo(PlayerButtonComponent, PlayerButtonPresenter.outputAreEqual);