import React from 'react';
import PlayerButtonStyles from './styles';
import type PlayerButtonPresenter from './presenter';
import { Animated, Easing } from 'react-native';
import { PlayerStatus } from '@guildion/core';

namespace PlayerButtonHooks {
    export const useStyles = (props: PlayerButtonPresenter.Input) => {
        const styles = React.useMemo(() => new PlayerButtonStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: PlayerButtonPresenter.Input) => {
        const [status, setStatus] = React.useState(props.status);
        React.useEffect(() => {
            status != props.status && setStatus(props.status);
        }, [props.status]);

        const translation = React.useRef(new Animated.Value(status == PlayerStatus.PLAY ? 1 : 0)).current;
        React.useEffect(() => {
            switch(status) {
            case PlayerStatus.PAUSE:
                Animated.timing(translation, {
                    toValue: 1,
                    useNativeDriver: false,
                    easing: Easing.ease,
                    duration: 200,
                }).start();
                break;
            case PlayerStatus.PLAY:
                Animated.timing(translation, {
                    toValue: 0,
                    useNativeDriver: false,
                    easing: Easing.ease,
                    duration: 200,
                }).start();
                break;
            default:
            case PlayerStatus.LOADING: break;
            }
        }, [status]);

        const onPress = React.useCallback(() => {
            switch(status) {
            case PlayerStatus.PAUSE:
                setStatus(PlayerStatus.PLAY);
                props.onPress && props.onPress(PlayerStatus.PLAY);
                break;
            case PlayerStatus.PLAY:
                setStatus(PlayerStatus.PAUSE);
                props.onPress && props.onPress(PlayerStatus.PAUSE);
                break;
            default:
            case PlayerStatus.LOADING: break; 
            }
        }, [status, props.onPress]);

        return {
            status,
            translation,
            onPress,
        };
    }
}

export default PlayerButtonHooks;