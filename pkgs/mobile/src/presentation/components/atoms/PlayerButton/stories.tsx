import { storiesOf } from '@storybook/react-native';
import React from 'react';
import PlayerButton from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { PlayerStatus } from '@guildion/core';

storiesOf('PlayerButton', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <PlayerButton
            style={new Style({
                width: number('width', 32),
                height: number('height', 32),
            })}
            status={PlayerStatus.PAUSE}
        />
    ))
    .add('Loading', () => (
        <PlayerButton
            style={new Style({
                width: number('width', 32),
                height: number('height', 32),
            })}
            status={PlayerStatus.LOADING}
        />
    ))