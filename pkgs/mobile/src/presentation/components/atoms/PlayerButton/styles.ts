import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') ?? 0;
    const height = style?.getAsNumber('height') ?? 0;
    const padding = height / 4;
    const innerWidth = width - padding;
    const innerHeight = height - padding;
    const barMargin = width / 8;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            marginLeft: -width / 2,
            marginTop: -height / 2,
        },
        touchable: {
            width: width,
            height: height,
            padding,
            position: 'relative',
        },
        inner: {
            width: innerWidth,
            height: innerHeight,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        filter: {
            position: 'absolute',
            top: -height / 2 + padding / 2,
            left: -width / 2 + padding / 2,
            bottom: 0,
            right: 0,
            width: width + (width / 3 * 2),
            height: height + (height / 3 * 2),
            borderRadius: width + (width / 3 * 2) / 2,
            backgroundColor: appTheme.background.complementary,
            opacity: 0.2,
        },
        indicator: {
            width: innerWidth,
            height: innerHeight,
            color: style?.backgroundColor ?? appTheme.player.button,
            backgroundColor: appTheme.common.transparent,
            opacity: .8,
        },
        leftBorderPause: {
            width: 4,
            height: innerHeight,
            borderRadius: 2,
            backgroundColor: appTheme.player.button,
            opacity: .8,
            marginRight: barMargin,
            borderColor: style?.backgroundColor ?? appTheme.player.button,
            borderTopColor: appTheme.common.transparent,
            borderBottomColor: appTheme.common.transparent,
            borderRightWidth: 0,
            borderLeftWidth: 0,
            borderTopWidth: 0,
            borderBottomWidth: 0,
        },
        rightBorderPause: {
            width: 4,
            height: innerHeight,
            borderRadius: 2,
            backgroundColor: appTheme.player.button,
            opacity: .8,
            marginLeft: barMargin,
            borderColor: style?.backgroundColor ?? appTheme.player.button,
            borderTopColor: appTheme.common.transparent,
            borderBottomColor: appTheme.common.transparent,
            borderRightWidth: 0,
            borderLeftWidth: 0,
            borderTopWidth: 0,
            borderBottomWidth: 0,
        },
        leftBorderPlay: {
            width: 0,
            height: 0,
            backgroundColor: appTheme.common.transparent,
            marginRight: 0,
            borderStyle: "solid",
            opacity: .8,
            borderLeftWidth: (innerHeight - barMargin),
            borderTopWidth: (innerHeight - barMargin) / 3 * 2,
            borderBottomWidth: (innerHeight - barMargin) / 3 * 2,
            borderColor: style?.backgroundColor ?? appTheme.player.button,
            borderTopColor: appTheme.common.transparent,
            borderBottomColor: appTheme.common.transparent,
            transform: [{ translateX: innerHeight - barMargin - (barMargin * 3) }],
        },
        rightBorderPlay: {
            width: 0,
            height: 0,
            backgroundColor: appTheme.common.transparent,
            marginRight: 0,
            borderStyle: "solid",
            borderColor: style?.backgroundColor ?? appTheme.player.button,
            opacity: 0,
            borderLeftWidth: ((innerHeight - barMargin)) / 4,
            borderTopWidth: ((innerHeight - barMargin) / 3 * 2) / 4,
            borderBottomWidth: ((innerHeight - barMargin) / 3 * 2) / 4,
            borderTopColor: appTheme.common.transparent,
            borderBottomColor: appTheme.common.transparent,
            transform: [{ translateX: -(((innerHeight - barMargin)) / 4) }],
        },
    })
};

type Styles = typeof styles;

export default class PlayerButtonStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};