import React from 'react';
import PlayerButtonComponent from './component';
import PlayerButtonPresenter from './presenter';

const PlayerButton = (props: PlayerButtonPresenter.Input) => {
    const output = PlayerButtonPresenter.usePresenter(props);
    return <PlayerButtonComponent {...output} />;
};

export default React.memo(PlayerButton, PlayerButtonPresenter.inputAreEqual);