import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import PlayerButtonHooks from './hooks';
import PlayerButtonStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { PlayerStatus } from '@guildion/core';
import { Animated } from 'react-native';

namespace PlayerButtonPresenter {
    export type Input = {
        status: PlayerStatus,
        onPress?: (status: PlayerStatus) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PlayerButtonStyles,
        appTheme: AppTheme,
        animatedStyle?: AnimatedStyle,
        status: PlayerStatus,
        translation: Animated.Value | Animated.AnimatedInterpolation,
        onPress: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PlayerButtonHooks.useStyles({ ...props });
        const {
            status,
            translation,
            onPress,
        } = PlayerButtonHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            status,
            translation,
            onPress,
        }
    }
}

export default PlayerButtonPresenter;