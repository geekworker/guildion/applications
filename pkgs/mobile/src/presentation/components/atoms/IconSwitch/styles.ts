import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { MontserratFont } from '@/shared/constants/Font';

const PADDING: number = 8;
export const getIconSize = (height: number): number => height - (PADDING * 2) - 10;

const styles = ({
    style,
    appTheme,
    isActive,
}: StyleProps & { isActive?: boolean }) => {
    appTheme ||= fallbackAppTheme;
    const width = Number(style?.width ?? 0);
    const height = Number(style?.height ?? 0);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            padding: PADDING,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'relative',
        },
        filter: {
            position: 'absolute',
            top: 0,
            left: (width - height) / 2,
            bottom: 0,
            right: (width - height) / 2,
            width: height,
            height,
            borderRadius: height / 2,
            backgroundColor: appTheme.background.complementary,
            opacity: 0.2,
        },
        icon: {
            width: getIconSize(height),
            height: getIconSize(height),
            color: isActive ? style?.tintColor ?? appTheme.element.link : style?.color ?? appTheme.background.complementary,
        },
        iconSkeleton: {
            width: getIconSize(height),
            height: getIconSize(height),
        },
        title: {
            fontSize: 9,
            marginTop: 2,
            marginBottom: -2,
            fontFamily: MontserratFont.Bold,
            color: isActive ? style?.tintColor ?? appTheme.element.link : style?.color ?? appTheme.background.complementary,
            textAlign: 'center',
            width: Number(style?.width ?? 0),
        },
        titleSkeleton: {
            height: 9,
            marginTop: 2,
            marginBottom: -2,
            width: Number(style?.width ?? 0) - 8,
        },
    });
}

type Styles = typeof styles;

export default class IconSwitchStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { isActive?: boolean }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};