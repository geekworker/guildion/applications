import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconSwitch from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Style } from '@/shared/interfaces/Style';

storiesOf('IconSwitch', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <IconSwitch
            title={'Rooms'}
            style={new Style({
                width: 40,
                height: 40,
            })}
            Icon={(props) => <Icon size={props.size} color={props.color} name={'camera'} />}
        />
    ))
    .add('Loading', () => (
        <IconSwitch
            title={'Rooms'}
            style={new Style({
                width: 40,
                height: 40,
            })}
            Icon={(props) => <Icon size={props.size} color={props.color} name={'camera'} />}
            loading={true}
        />
    ))
