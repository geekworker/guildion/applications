import React from 'react';
import IconSwitchStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Animated, GestureResponderEvent, Text, TouchableOpacity } from 'react-native';
import IconNodeClass from '@/shared/interfaces/IconNodeClass';
import { compare } from '@/shared/modules/ObjectCompare';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import TouchableCircleRipple from '../TouchableCircleRipple';

export type IconSwitchProps = {
    onPress?: (e: GestureResponderEvent) => void,
    Icon: IconNodeClass,
    title: string,
    value?: boolean,
    preventSwitch?: boolean,
    loading?: boolean,
} & Partial<StyleProps>;

const IconSwitch: React.FC<IconSwitchProps> = ({
    style,
    appTheme,
    onPress,
    Icon,
    title,
    value,
    preventSwitch,
    loading,
}) => {
    appTheme ||= fallbackAppTheme;
    const [stateValue, setStateValue] = React.useState<boolean>(value ?? false);
    React.useEffect(() => {
        stateValue != (value ?? false) && setStateValue(value ?? false);
    }, [value]);
    const styles = React.useMemo(() => new IconSwitchStyles({
        style,
        appTheme,
        isActive: stateValue,
    }), [
        style,
        appTheme,
        stateValue,
    ]);
    return (
        <TouchableCircleRipple
            style={styles.getStyle('container')}
            filterStyle={styles.getStyle('filter')}
            disabled={loading}
            onPress={(e: GestureResponderEvent) => {
                if (!preventSwitch) setStateValue(!stateValue);
                if (onPress) onPress(e);
            }}
        >
            {loading ? (
                <SkeletonContent
                    isLoading={loading}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.iconSkeleton]}
                />
            ) : (
                <Icon
                    size={styles.icon.width}
                    color={String(styles.icon.color)}
                />
            )}
            {loading ? (
                <SkeletonContent
                    isLoading={loading}
                    boneColor={appTheme.skeleton.boneColor}
                    highlightColor={appTheme.skeleton.highlightColor}
                    animationType="pulse"
                    layout={[styles.titleSkeleton]}
                />
            ) : (
                <Text style={styles.title}>
                    {title}
                </Text>
            )}
        </TouchableCircleRipple>
    )
};

export default React.memo(IconSwitch, compare);