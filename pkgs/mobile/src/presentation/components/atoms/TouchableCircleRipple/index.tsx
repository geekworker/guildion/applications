import React from 'react';
import TouchableCircleRippleStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { TouchableWithoutFeedback, TouchableWithoutFeedbackProps, Animated } from 'react-native';
import { Style } from '@/shared/interfaces/Style';

type Props = {
    children?: React.ReactNode,
    filterStyle?: Style,
} & Omit<TouchableWithoutFeedbackProps, 'style'> & Partial<StyleProps>;

const TouchableCircleRipple: React.FC<Props> = ({
    style,
    appTheme,
    animatedStyle,
    children,
    filterStyle,
    ...props
}) => {
    appTheme ||= fallbackAppTheme;
    const scale = React.useRef(new Animated.Value(0)).current;
    const styles = React.useMemo(() => new TouchableCircleRippleStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableWithoutFeedback
                {...props}
                onPressIn={React.useCallback((e) => {
                    Animated.timing(scale, {
                        toValue: 1,
                        duration: 100,
                        useNativeDriver: true,
                    }).start();
                    props.onPressIn && props.onPressIn(e);
                }, [])}
                onPressOut={React.useCallback((e) => {
                    Animated.timing(scale, {
                        toValue: 0,
                        duration: 50,
                        useNativeDriver: true,
                    }).start();
                    props.onPressOut && props.onPressOut(e);
                }, [])}
            >
                <Animated.View style={{ ...styles.inner }}>
                    <Animated.View
                        style={{
                            ...styles.filter,
                            ...filterStyle?.toStyleObject(),
                            transform: [{
                                scale: scale.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0.8, 1],
                                }),
                            }],
                            opacity: scale.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0, styles.filter.opacity],
                            }),
                        }}
                    />
                    {children}
                </Animated.View>
            </TouchableWithoutFeedback>
        </Animated.View>
    );
};

export default React.memo(TouchableCircleRipple, compare);