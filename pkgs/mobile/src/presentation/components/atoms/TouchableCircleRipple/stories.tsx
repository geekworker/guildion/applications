import { storiesOf } from '@storybook/react-native';
import React from 'react';
import TouchableCircleRipple from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { View } from 'react-native';

storiesOf('TouchableCircleRipple', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <TouchableCircleRipple
            style={new Style({
                width: 40,
                height: 40,
            })}
        >
            <View
                style={new Style({
                    width: 24,
                    height: 24,
                    backgroundColor: '#ffffff',
                })}
            />
        </TouchableCircleRipple>
    ))