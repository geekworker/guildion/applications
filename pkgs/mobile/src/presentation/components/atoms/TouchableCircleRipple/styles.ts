import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const width = Number(style?.width ?? 0);
    const height = Number(style?.height ?? 0);
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        touchable: {
            width,
            height,
            borderRadius: height / 2,
        },
        inner: {
            width,
            height,
            borderRadius: height / 2,
            display: style?.display ?? 'flex',
            flexDirection: style?.flexDirection ?? 'column',
            justifyContent: style?.justifyContent ?? 'center',
            alignItems: style?.alignItems ?? 'center',
            position: 'relative',
        },
        filter: {
            position: 'absolute',
            width,
            height,
            borderRadius: height / 2,
            backgroundColor: appTheme.background.complementary,
            opacity: 0.2,
        },
    })
};

type Styles = typeof styles;

export default class TouchableCircleRippleStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};