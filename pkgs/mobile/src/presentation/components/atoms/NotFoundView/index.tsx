import React from 'react';
import NotFoundViewStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { View } from 'react-native';
import { Text } from 'react-native-elements';
import Button, { ButtonStyleType } from '../Button';

type Props = {
    title?: string,
    description?: string,
    buttonProps?: {
        title?: string,
    },
} & Partial<StyleProps>;

const NotFoundView: React.FC<Props> = ({
    style,
    appTheme,
    title,
    description,
    buttonProps,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new NotFoundViewStyles({
        style,
        appTheme,
    }), [
        style,
        appTheme,
    ]);
    return (
        <View style={styles.container}>
            <View style={styles.inner}>
                <Text style={styles.title}>
                    {title}
                </Text>
                <Text style={styles.description}>
                    {description}
                </Text>
                {buttonProps && (
                    <Button
                        styleType={ButtonStyleType.Fill}
                        style={styles.getStyle('button')}
                        title={buttonProps.title}
                    />
                )}
            </View>
        </View>
    );
};

export default React.memo(NotFoundView, compare);