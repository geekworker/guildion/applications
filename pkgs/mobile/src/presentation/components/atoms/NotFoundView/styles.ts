import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
        },
        inner: {
        },
        title: {
            fontSize: 28,
            fontFamily: MontserratFont.Bold,
            textAlign: 'left',
            marginBottom: 8,
            color: appTheme.element.default,
        },
        description: {
            fontSize: 20,
            fontFamily: MontserratFont.Regular,
            textAlign: 'left',
            marginTop: 8,
            marginBottom: 8,
            color: appTheme.element.subp1,
        },
        button: {
            width: 168,
            height: 44,
            marginTop: 8,
            borderRadius: 22,
            fontSize: 16,
            backgroundColor: appTheme.background.subp2,
        },
        activeButton: {
            position: 'absolute',
            bottom: 20,
            right: 20,
            color: appTheme.element.link,
            width: 168,
            height: 50,
            borderRadius: 25,
            fontSize: 14,
            fontFamily: MontserratFont.Bold,
            backgroundColor: appTheme.background.subp2,
        },
    });
};

type Styles = typeof styles;

export default class NotFoundViewStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};