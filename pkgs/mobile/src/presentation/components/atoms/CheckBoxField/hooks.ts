import StyleProps from '@/shared/interfaces/StyleProps';
import React from 'react';
import CheckBoxFieldStyles from './styles';

namespace CheckBoxFieldHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new CheckBoxFieldStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useValue = (initalValue: boolean): {
        value: boolean,
        setValue: (val: boolean) => void,
    } => {
        const [value, setValue] = React.useState(initalValue);
        React.useEffect(() => {
            value != initalValue && setValue(initalValue);
        }, [initalValue]);
        return {
            value,
            setValue,
        };
    }
}

export default CheckBoxFieldHooks;