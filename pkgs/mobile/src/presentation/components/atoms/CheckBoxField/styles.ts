import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const padding = 2;

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        highlight: {
            ...style?.toStyleObject(),
            marginVertical: style?.marginVertical ?? 20,
            borderRadius: style?.borderRadius ?? 5,
        },
        container: {
            flexDirection: 'row',
            alignItems: 'center',
            borderRadius: 5,
        },
        inner: {
            padding,
            width: style?.width,
            height: style?.height,
            backgroundColor: appTheme.checkbox.default,
            borderRadius: 5,
        },
        activeInner: {
            padding,
            width: style?.width,
            height: style?.height,
            backgroundColor: appTheme.checkbox.active,
            borderRadius: 5,
        },
        check: { 
            backgroundColor: appTheme.checkbox.icon,
            width: Number(style?.width ?? 0) - (padding * 2),
            height: Number(style?.width ?? 0) - (padding * 2),
        },
    })
};

type Styles = typeof styles;

export default class CheckBoxFieldStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};