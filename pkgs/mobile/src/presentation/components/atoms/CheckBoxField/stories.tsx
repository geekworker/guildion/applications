import { storiesOf } from '@storybook/react-native';
import { boolean, number } from '@storybook/addon-knobs';
import React from 'react';
import CheckBoxField from '.';
import CenterView from '../CenterView';
import { Style } from '@/shared/interfaces/Style';

storiesOf('CheckBoxField', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Inactive', () => (
        <CheckBoxField
            style={new Style({
                width: number('width', 40),
                height: number('height', 40),
            })}
            value={boolean('value', false)}
        />
    ))
    .add('Active', () => (
        <CheckBoxField
            style={new Style({
                width: number('width', 40),
                height: number('height', 40),
            })}
            value={boolean('value', true)}
        />
    ));
