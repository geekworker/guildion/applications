import React from 'react';
import CheckBoxFieldComponent from './component';
import CheckBoxFieldPresenter from './presenter';

const CheckBoxField = (props: CheckBoxFieldPresenter.Input) => {
    const output = CheckBoxFieldPresenter.usePresenter(props);
    return <CheckBoxFieldComponent {...output} />;
};

export default React.memo(CheckBoxField, CheckBoxFieldPresenter.inputAreEqual);