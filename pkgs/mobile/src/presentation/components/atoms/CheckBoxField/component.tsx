import React from 'react';
import CheckBoxFieldPresenter from './presenter';
import { 
    TouchableHighlight,
    View 
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const CheckBoxField: React.FC<CheckBoxFieldPresenter.Output> = ({ styles, value, onPress }) => {
    return (
        <TouchableHighlight
            underlayColor="transparent"
            style={styles.highlight}
            onPress={() => onPress(!value)}
        >
            <View style={styles.container}>
                {(value) ? (
                    <View style={styles.activeInner}>
                        <Icon
                            name={'check'}
                            size={styles.check.width}
                            color={styles.check.backgroundColor}
                        />
                    </View>
                ) : (
                    <View style={styles.inner} />
                )}
            </View>
        </TouchableHighlight>
    ); 
};

export default React.memo(CheckBoxField, CheckBoxFieldPresenter.outputAreEqual);
