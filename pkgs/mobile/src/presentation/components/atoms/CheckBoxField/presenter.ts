import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import { compare } from "@/shared/modules/ObjectCompare";
import CheckBoxFieldHooks from "./hooks";
import CheckBoxFieldStyles from './styles';

namespace CheckBoxFieldPresenter {
    export type Input = {
        value: boolean,
        onPress?: (value: boolean) => void,
    } & StyleProps;
    
    export type Output = {
        styles: CheckBoxFieldStyles,
        appTheme: AppTheme,
        value: boolean,
        onPress: (value: boolean) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = CheckBoxFieldHooks.useStyles({ ...props });
        const {
            value,
            setValue,
        } = CheckBoxFieldHooks.useValue(props.value);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            value,
            onPress: (val: boolean) => {
                if (props.onPress) props.onPress(val);
                setValue(val);
            },
        }
    }
}

export default CheckBoxFieldPresenter;