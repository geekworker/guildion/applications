import { TouchableOpacity } from '@gorhom/bottom-sheet';
import React from 'react';
import { Animated, Text, View } from 'react-native';
import SegumentedControlInputPresenter from './presenter';

const SegumentedControlInputComponent = ({ styles, appTheme, animatedStyle, value, text, onPress, transition }: SegumentedControlInputPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableOpacity onPress={onPress} style={styles.touchable}>
                <Animated.View
                    style={{
                        ...styles.inner,
                        backgroundColor: transition.interpolate({
                            inputRange: [0, 1],
                            outputRange: [styles.inner.backgroundColor, styles.activeInner.backgroundColor],
                        }),
                    }}
                >
                    <Text style={styles.text} numberOfLines={1} minimumFontScale={0.8} adjustsFontSizeToFit>
                        {text}
                    </Text>
                </Animated.View>
            </TouchableOpacity>
        </Animated.View>
    );
};

export default React.memo(SegumentedControlInputComponent, SegumentedControlInputPresenter.outputAreEqual);