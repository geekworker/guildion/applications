import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            borderRadius: style?.borderRadius ?? 5,
        },
        touchable: {
            width,
            height,
            borderRadius: style?.borderRadius ?? 5,
        },
        inner: {
            width,
            height,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: style?.borderRadius ?? 5,
            backgroundColor: appTheme.element.input,
        },
        activeInner: {
            width,
            height,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: style?.borderRadius ?? 5,
            backgroundColor: appTheme.element.main,
        },
        text: {
            fontFamily: MontserratFont.SemiBold,
            fontSize: 14,
            color: appTheme.element.default,
        },
    });
};

type Styles = typeof styles;

export default class SegumentedControlInputStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};