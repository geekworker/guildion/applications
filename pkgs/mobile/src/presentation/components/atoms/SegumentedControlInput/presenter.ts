import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import SegumentedControlInputHooks from './hooks';
import SegumentedControlInputStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Animated } from 'react-native';

namespace SegumentedControlInputPresenter {
    export type Input = {
        value?: boolean,
        text?: string,
        id: string,
        onPress?: (id: string, value: boolean) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SegumentedControlInputStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        value: boolean,
        text?: string,
        onPress: () => void,
        id: string,
        transition: Animated.Value | Animated.AnimatedInterpolation,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SegumentedControlInputHooks.useStyles({ ...props });
        const {
            value,
            onPress,
            transition,
        } = SegumentedControlInputHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            value,
            onPress,
            transition,
        }
    }
}

export default SegumentedControlInputPresenter;