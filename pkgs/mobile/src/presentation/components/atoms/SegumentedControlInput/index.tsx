import React from 'react';
import SegumentedControlInputComponent from './component';
import SegumentedControlInputPresenter from './presenter';

const SegumentedControlInput = (props: SegumentedControlInputPresenter.Input) => {
    const output = SegumentedControlInputPresenter.usePresenter(props);
    return <SegumentedControlInputComponent {...output} />;
};

export default React.memo(SegumentedControlInput, SegumentedControlInputPresenter.inputAreEqual);