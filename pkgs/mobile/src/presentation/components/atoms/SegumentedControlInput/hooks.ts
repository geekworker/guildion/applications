import React from 'react';
import SegumentedControlInputStyles from './styles';
import type SegumentedControlInputPresenter from './presenter';
import { Animated } from 'react-native';

namespace SegumentedControlInputHooks {
    export const useStyles = (props: SegumentedControlInputPresenter.Input) => {
        const styles = React.useMemo(() => new SegumentedControlInputStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SegumentedControlInputPresenter.Input) => {
        const [value, setValue] = React.useState(props.value ?? false);
        const transition = React.useRef(new Animated.Value(value ? 1 : 0)).current;
        React.useEffect(() => {
            if (value != props.value) setValue(props.value ?? false);
        }, [props.value]);
        React.useEffect(() => {
            Animated.spring(transition, {
                useNativeDriver: false,
                overshootClamping: false,
                toValue: value ? 1 : 0,
            }).start();
        }, [value]);
        const onPress = React.useCallback(() => {
            const shouldValue = !value;
            if (!shouldValue) return;
            setValue(shouldValue);
            props.onPress && props.onPress(props.id, shouldValue);
        }, [value, props.onPress, props.id]);
        return {
            value,
            onPress,
            transition,
        };
    }
}

export default SegumentedControlInputHooks;