import { storiesOf } from '@storybook/react-native';
import { number, text } from '@storybook/addon-knobs';
import React from 'react';
import AsyncImage from '.';
import CenterView from '../CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('AsyncImage', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <CenterView>
                {getStory()}
            </CenterView>
        </StoreProvider>
    ))
    .add('Default', () => (
        <AsyncImage
            style={new Style({
                width: number('width', 40),
                height: number('height', 40),
                borderRadius: number('borderRadius', 20),
            })}
            url={text('url', 'https://yt3.ggpht.com/ytc/AKedOLTDLauymQQXmfG3S_r3ZTzw8ds1VstwhcwvHU_8OA=s176-c-k-c0x00ffffff-no-rj')}
        />
    ));
