import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        wrapper: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
        },
        touchable: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
        inner: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
        image: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
        skeleton: {
            width: style?.width,
            height: style?.height,
            borderRadius: style?.borderRadius,
        },
    })
};

type Styles = typeof styles;

export default class AsyncImageStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};