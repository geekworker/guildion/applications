import React from 'react';
import { Image, View } from 'react-native'
import AsyncImagePresenter from './presenter';
import SkeletonContent from 'react-native-skeleton-content-nonexpo';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';

const AsyncImageComponent = ({ styles, base64, appTheme, loading, onPress, disabledPress, touchableHighlight }: AsyncImagePresenter.Output) => {
    return touchableHighlight ? (
        <View style={styles.wrapper} >
            <TouchableHighlight style={styles.touchable} onPress={onPress} disabled={disabledPress || !onPress} >
                <View style={styles.inner} >
                    <SkeletonContent
                        isLoading={loading || !base64}
                        boneColor={appTheme.skeleton.boneColor}
                        highlightColor={appTheme.skeleton.highlightColor}
                        animationType="pulse"
                        layout={[
                            { ...styles.skeleton },
                        ]}
                    >
                        <Image style={styles.image} source={{ uri: `data:image/png;base64,${base64}` }} />
                    </SkeletonContent>
                </View>
            </TouchableHighlight>
        </View>
    ) : (
        <View style={styles.wrapper} >
            <TouchableOpacity style={styles.touchable} onPress={onPress} disabled={disabledPress || !onPress} >
                <View style={styles.inner} >
                    <SkeletonContent
                        isLoading={loading || !base64}
                        boneColor={appTheme.skeleton.boneColor}
                        highlightColor={appTheme.skeleton.highlightColor}
                        animationType="pulse"
                        layout={[
                            { ...styles.skeleton },
                        ]}
                    >
                        <Image style={styles.image} source={{ uri: `data:image/png;base64,${base64}` }} />
                    </SkeletonContent>
                </View>
            </TouchableOpacity>
        </View>
    );
};

export default React.memo(AsyncImageComponent, AsyncImagePresenter.outputAreEqual);