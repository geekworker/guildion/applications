import AsyncImageHooks from "./hooks";
import AsyncImageStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import { ImageAction } from "@/presentation/redux/Image/ImageReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { imageCacheSelector } from "@/presentation/redux/Image/ImageSelector";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace AsyncImagePresenter {
    export type StackParams = {
    }

    export type StateProps = {
        base64?: string,
    }
    
    export type DispatchProps = {
        fetch: (url: string) => Action<{}>,
    }
    
    export type Input = {
        url: string,
        loading?: boolean,
        onPress?: () => void,
        disabledPress?: boolean,
        touchableHighlight?: boolean,
    } & StyleProps;
    
    export type Output = {
        styles: AsyncImageStyles,
        appTheme: AppTheme,
        base64?: string,
        url: string,
        loading?: boolean,
        onPress?: () => void,
        disabledPress?: boolean,
        touchableHighlight?: boolean,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
            base64: imageCacheSelector(state, ownProps.url),
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            fetch: (url: string) => ImageAction.fetch.started({ url }),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.image.cache == next.image.cache
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = AsyncImageHooks.useStyles(props);
        AsyncImageHooks.useFetch(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default AsyncImagePresenter;