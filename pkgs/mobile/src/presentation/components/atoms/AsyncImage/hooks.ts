import React from "react";
import AsyncImageStyles from './styles';
import { Action } from "redux";
import StyleProps from "@/shared/interfaces/StyleProps";

namespace AsyncImageHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new AsyncImageStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useFetch = ({ fetch, url }: { fetch: (url: string) => Action, url: string }) => {
        React.useEffect(() => {
            fetch(url);
        }, [url]);
    }
}

export default AsyncImageHooks