import React from 'react';
import AsyncImagePresenter from './presenter';
import AsyncImageComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: AsyncImagePresenter.MergedProps) => {
    const output = AsyncImagePresenter.usePresenter(props);
    return <AsyncImageComponent {...output} />;
};

const AsyncImage = connect<AsyncImagePresenter.StateProps, AsyncImagePresenter.DispatchProps, AsyncImagePresenter.Input, AsyncImagePresenter.MergedProps, RootState>(
    AsyncImagePresenter.mapStateToProps,
    AsyncImagePresenter.mapDispatchToProps,
    AsyncImagePresenter.mergeProps,
    AsyncImagePresenter.connectOptions,
)(Container);

export default AsyncImage;
