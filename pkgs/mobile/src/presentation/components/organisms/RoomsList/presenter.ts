import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomsListHooks from "./hooks";
import RoomsListStyles from './styles';
import React from 'react';
import { Room, Rooms } from "@guildion/core";
import { Animated, Insets, NativeScrollEvent, NativeSyntheticEvent, ViewStyle } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomsListPresenter {
    export type Input = {
        children?: React.ReactNode,
        rooms: Rooms,
        onPressCell?: (room: Room) => void,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        scrollInsets?: Insets,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomsListStyles,
        appTheme: AppTheme,
        rooms: Rooms,
        onPressCell?: (room: Room) => void,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        scrollInsets?: Insets,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoomsListHooks.useStyles({ ...props });
        const mockRooms = RoomsListHooks.useSkeletonMock();
        const rooms = props.loading ? mockRooms : props.rooms;
        return {
            ...props,
            styles,
            rooms,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomsListPresenter;