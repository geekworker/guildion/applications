import React from "react";
import RoomsListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Room, Rooms } from "@guildion/core";

namespace RoomsListHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomsListStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useSkeletonMock = (): Rooms => {
        const rooms = React.useMemo(() => new Rooms([
            new Room({ id: '`skeleton1`' }),
            new Room({ id: '`skeleton2`' }),
            new Room({ id: '`skeleton3`' }),
            new Room({ id: '`skeleton4`' }),
            new Room({ id: '`skeleton5`' }),
            new Room({ id: '`skeleton6`' }),
        ]), []);
        return rooms;
    }
}

export default RoomsListHooks