import React from 'react';
import RoomsListComponent from './component';
import RoomsListPresenter from './presenter';

const RoomsList = (props: RoomsListPresenter.Input) => {
    const output = RoomsListPresenter.usePresenter(props);
    return <RoomsListComponent {...output} />;
};

export default React.memo(RoomsList, RoomsListPresenter.inputAreEqual);