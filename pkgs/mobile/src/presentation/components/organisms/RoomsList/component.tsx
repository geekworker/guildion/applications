import { Room } from '@guildion/core';
import React from 'react';
import { Animated, ListRenderItemInfo } from 'react-native';
import RoomCell from '../../molecules/RoomCell';
import RoomsListPresenter from './presenter';

const RoomsListComponent = ({ styles, rooms, appTheme, onPressCell, ListFooterComponent, ListHeaderComponent, ListHeaderComponentStyle, ListFooterComponentStyle, onScroll, scrollInsets, loading }: RoomsListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Room>)=> (
        <RoomCell
            style={styles.getStyle('cell')}
            appTheme={appTheme}
            room={props.item}
            key={props.index}
            onPress={onPressCell}
            loading={loading}
        />
    );
    return (
        <Animated.FlatList
            style={styles.container}
            contentInset={scrollInsets}
            contentOffset={scrollInsets?.top ? { y: -scrollInsets.top, x: 0 } : undefined}
            directionalLockEnabled
            automaticallyAdjustContentInsets={false}
            alwaysBounceVertical={false}
            alwaysBounceHorizontal={false}
            bounces={!loading}
            scrollEnabled={!loading}
            keyboardDismissMode="on-drag"
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            snapToAlignment={'center'}  
            data={rooms.toArray()}
            renderItem={renderCell}
            keyExtractor={(item: Room, index: number) => item.id! + String(index)}
            ListHeaderComponent={ListHeaderComponent}
            ListFooterComponent={ListFooterComponent}
            ListHeaderComponentStyle={ListHeaderComponentStyle}
            ListFooterComponentStyle={ListFooterComponentStyle}
            onScroll={onScroll}
            scrollEventThrottle={16}
        />
    );
};

export default React.memo(RoomsListComponent, RoomsListPresenter.outputAreEqual);