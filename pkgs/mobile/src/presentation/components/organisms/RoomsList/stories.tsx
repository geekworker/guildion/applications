import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomsList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Rooms } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';

storiesOf('RoomsList', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <RoomsList
            rooms={new Rooms([])}
        />
    ))
    .add('Loading', () => (
        <RoomsList
            rooms={new Rooms([])}
            loading={true}
        />
    ))
