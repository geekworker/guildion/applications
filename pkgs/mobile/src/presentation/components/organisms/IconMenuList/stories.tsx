import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconMenuList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Style } from '@/shared/interfaces/Style';
import { boolean } from '@storybook/addon-knobs';

storiesOf('IconMenuList', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <IconMenuList
            data={[
                {
                    title: 'Rooms1',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
                {
                    title: 'Rooms2',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
                {
                    title: 'Rooms3',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
                {
                    title: 'Rooms4',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
            ]}
            style={new Style({
                width: 385,
                height: 84,
            })}
            loading={boolean('loading', false)}
        />
    ))
    .add('Loading', () => (
        <IconMenuList
            data={[
                {
                    title: 'Rooms1',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
                {
                    title: 'Rooms2',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
                {
                    title: 'Rooms3',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
                {
                    title: 'Rooms4',
                    Icon: (props) => <Icon {...props} name={'camera'} />,
                    countText: '0',
                    notificationBadgeText: 0,
                    connectingBadgeText: 0,
                },
            ]}
            style={new Style({
                width: 385,
                height: 84,
            })}
            loading={true}
        />
    ))
