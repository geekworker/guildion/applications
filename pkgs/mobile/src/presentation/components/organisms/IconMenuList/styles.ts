import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: Number(style?.width ?? 0),
            height: Number(style?.height ?? 0),
            position: 'relative',
        },
        scrollView: {
            height: style?.height,
            position: 'relative',
        },
        menu: {
            width: Number(style?.height ?? 0) + 20,
            height: style?.height,
            color: style?.color,
        },
    })
};

type Styles = typeof styles;

export default class IconMenuListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};