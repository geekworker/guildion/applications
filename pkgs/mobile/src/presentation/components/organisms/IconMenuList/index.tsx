import React from 'react';
import IconMenuListStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import IconMenu, { IconMenuProps } from '../../atoms/IconMenu';
import { compare } from '@/shared/modules/ObjectCompare';

type Props = {
    data: IconMenuProps[],
    loading?: boolean,
} & Partial<StyleProps>;

const IconMenuList: React.FC<Props> = ({
    style,
    appTheme,
    data,
    loading,
}) => {
    const styles = React.useMemo(() => new IconMenuListStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const renderMenu = (props: ListRenderItemInfo<IconMenuProps>)=> (
        <IconMenu
            appTheme={appTheme}
            {...props.item}
            loading={loading}
            style={styles.getStyle('menu')}
            key={props.index}
            showLeftBorder={props.index > 0}
            showRightBorder={data.length > 1 && props.index == data.length - 1}
        />
    );
    const scrollEnabled = data.length * styles.menu.width > styles.container.width;
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={scrollEnabled}
                scrollEnabled={scrollEnabled}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}  
                data={data}
                renderItem={renderMenu}
                keyExtractor={(item: IconMenuProps, index: number) => item.title! + String(index)}
            />
        </View>
    )
};

export default React.memo(IconMenuList, compare);