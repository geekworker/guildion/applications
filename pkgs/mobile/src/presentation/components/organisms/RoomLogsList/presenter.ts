import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomLogsListHooks from "./hooks";
import RoomLogsListStyles from './styles';
import React from 'react';
import { RoomLog, RoomLogs } from "@guildion/core";
import { ViewStyle } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";

namespace RoomLogsListPresenter {
    export type Input = {
        children?: React.ReactNode,
        logs: RoomLogs,
        onPressCell?: (log: RoomLog) => void,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomLogsListStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        logs: RoomLogs,
        onPressCell?: (log: RoomLog) => void,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = RoomLogsListHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomLogsListPresenter;