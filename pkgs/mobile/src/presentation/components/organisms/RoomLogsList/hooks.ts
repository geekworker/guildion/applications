import React from "react";
import RoomLogsListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace RoomLogsListHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomLogsListStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default RoomLogsListHooks