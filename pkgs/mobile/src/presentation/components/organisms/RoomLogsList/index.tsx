import React from 'react';
import RoomLogsListComponent from './component';
import RoomLogsListPresenter from './presenter';

const RoomLogsList = (props: RoomLogsListPresenter.Input) => {
    const output = RoomLogsListPresenter.usePresenter(props);
    return <RoomLogsListComponent {...output} />;
};

export default React.memo(RoomLogsList, RoomLogsListPresenter.inputAreEqual);