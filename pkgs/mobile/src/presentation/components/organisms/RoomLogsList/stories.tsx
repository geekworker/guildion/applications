import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomLogsList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { RoomLogs } from '@guildion/core';

storiesOf('RoomLogsList', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <RoomLogsList
            logs={new RoomLogs([])}
        >
            
        </RoomLogsList>
    ))
