import { RoomLog } from '@guildion/core';
import React from 'react';
import { Animated, ListRenderItemInfo } from 'react-native';
import RoomLogCell from '../../molecules/RoomLogCell';
import RoomLogsListPresenter from './presenter';

const RoomLogsListComponent: React.FC<RoomLogsListPresenter.Output> = ({ styles, animatedStyle, logs, appTheme, ListHeaderComponent,  ListFooterComponent, ListHeaderComponentStyle, ListFooterComponentStyle }) => {
    const renderCell = (props: ListRenderItemInfo<RoomLog>)=> (
        <RoomLogCell
            style={styles.getStyle('cell')}
            appTheme={appTheme}
            log={props.item}
            key={props.index}
        />
    );
    return (
        <Animated.FlatList
            style={{ ...styles.container, height: animatedStyle?.height ?? styles.container.height }}
            directionalLockEnabled
            automaticallyAdjustContentInsets={false}
            alwaysBounceVertical={false}
            alwaysBounceHorizontal={false}
            bounces={true}
            keyboardDismissMode="on-drag"
            showsHorizontalScrollIndicator={false}
            snapToAlignment={'center'}  
            data={logs.toArray()}
            renderItem={renderCell}
            keyExtractor={(item: RoomLog, index: number) => item.id! + String(index)}
            scrollEventThrottle={16}
            ListHeaderComponent={ListHeaderComponent}
            ListFooterComponent={ListFooterComponent}
            ListHeaderComponentStyle={ListHeaderComponentStyle}
            ListFooterComponentStyle={ListFooterComponentStyle}
        />
    );
};

export default React.memo(RoomLogsListComponent, RoomLogsListPresenter.outputAreEqual);