import React from 'react';
import IconSideTabBarStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { View } from 'react-native';
import IconSwitch from '../../atoms/IconSwitch';
import BrandIcon from '../../Icons/BrandIcon';
import RootRedux from '@/infrastructure/Root/redux';
import { HomeTabsNavigatorRoutes } from '../../navigators/HomeTabsNavigator/constants';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { compare } from '@/shared/modules/ObjectCompare';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';

type Props = {
    onPressGuilds?: () => void,
    onPressExplore?: () => void,
} & Partial<StyleProps>;

const IconSideTabBar: React.FC<Props> = ({
    style,
    appTheme,
    onPressGuilds,
    onPressExplore,
}) => {
    const insets = useSafeAreaInsets();
    const styles = React.useMemo(() => new IconSideTabBarStyles({
        style,
        appTheme,
        insets,
    }), [
        style,
        appTheme,
        insets,
    ]);
    const context = React.useContext(RootRedux.Context);
    const currentHomeRoute = context.state.currentHomeRoute;
    return (
        <View style={styles.container}>
            <IconSwitch
                appTheme={appTheme}
                style={styles.getStyle('button')}
                Icon={(props) => <BrandIcon width={props.size} height={props.size} color={props.color} />}
                title={'Guilds'}
                onPress={() => {
                    if (currentHomeRoute !== HomeTabsNavigatorRoutes.GuildsNavigator) context.dispatch(RootRedux.Action.setCurrentHomeRoute(HomeTabsNavigatorRoutes.GuildsNavigator))
                    if (onPressGuilds) onPressGuilds();
                }}
                preventSwitch={true}
                value={currentHomeRoute === HomeTabsNavigatorRoutes.GuildsNavigator}
            />
            <IconSwitch
                appTheme={appTheme}
                style={styles.getStyle('button')}
                Icon={(props) => <IoniconsIcon size={props.size} name={'search'} color={props.color} />}
                title={'Explore'}
                onPress={() => {
                    if (currentHomeRoute !== HomeTabsNavigatorRoutes.ExploreNavigator) context.dispatch(RootRedux.Action.setCurrentHomeRoute(HomeTabsNavigatorRoutes.ExploreNavigator))
                    if (onPressExplore) onPressExplore();
                }}
                preventSwitch={true}
                value={currentHomeRoute === HomeTabsNavigatorRoutes.ExploreNavigator}
            />
        </View>
    )
};

export default React.memo(IconSideTabBar, compare);