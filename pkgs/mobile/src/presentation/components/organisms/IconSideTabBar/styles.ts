import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { EdgeInsets } from 'react-native-safe-area-context';

const PADDING: number = 4;
const styles = ({
    style,
    appTheme,
    insets,
}: StyleProps & { insets: EdgeInsets }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            width: Number(style?.width ?? 0),
            height: Number(style?.height ?? 0),
            paddingTop: style?.paddingTop ?? insets.top,
            paddingBottom: style?.paddingBottom ?? insets.bottom,
            position: 'relative',
            ...style?.toStyleObject(),
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
        },
        button: {
            width: Number(style?.width ?? 0),
            height: Number(style?.width ?? 0) - PADDING - PADDING,
            color: appTheme.element.subp2,
            tintColor: appTheme.element.default,
            marginTop: PADDING,
            marginBottom: PADDING,
        },
    });
}

type Styles = typeof styles;

export default class IconSideTabBarStyles extends Record<ReturnType<Styles>>({
    ...styles({ insets: { top: 0, left: 0, right: 0, bottom: 0 } })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { insets: EdgeInsets }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};