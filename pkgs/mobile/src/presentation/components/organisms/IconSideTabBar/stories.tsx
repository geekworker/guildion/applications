import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconSideBar from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Style } from '@/shared/interfaces/Style';

storiesOf('IconSideBar', module)
    .addDecorator((getStory) => <StoreProvider><CenterView>{getStory()}</CenterView></StoreProvider>)
    .add('Default', () => (
        <IconSideBar
            style={new Style({
                height: 385,
                width: 84,
            })}
        />
    ))
