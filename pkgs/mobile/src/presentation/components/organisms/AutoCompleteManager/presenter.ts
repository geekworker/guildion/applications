import AutoCompleteManagerHooks from "./hooks";
import AutoCompleteManagerStyles from './styles';
import { Action, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { AutoCompletePrefix, Member, MentionType } from "@guildion/core";
import { List } from "immutable";

namespace AutoCompleteManagerPresenter {
    export type StateProps = {
    }
    
    export type DispatchProps = {
    }
    
    export type Input = {
        query?: string,
        prefix: AutoCompletePrefix,
        onPressSystemMention?: (mention: MentionType) => void,
        onPressMemberMention?: (member: Member) => void,
    } & StyleProps;
    
    export type Output = {
        styles: AutoCompleteManagerStyles,
        appTheme: AppTheme,
        query?: string,
        prefix: AutoCompletePrefix,
        data: List<MentionType | Member>,
        scrollEnabled: boolean,
        onPressSystemMention?: (mention: MentionType) => void,
        onPressMemberMention?: (member: Member) => void,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state, ownProps) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = AutoCompleteManagerHooks.useStyles(props);
        const {
            data,
            scrollEnabled,
        } = AutoCompleteManagerHooks.useData({ ...props, styles });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            data,
            scrollEnabled,
        }
    }
}

export default AutoCompleteManagerPresenter;