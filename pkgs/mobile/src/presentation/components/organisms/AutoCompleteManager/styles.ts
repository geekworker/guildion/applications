import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: undefined,
            maxHeight: style?.maxHeight,
            position: 'relative',
            borderTopColor: appTheme.background.subp1,
            borderTopWidth: 1,
        },
        scrollView: {
            width: style?.width,
            position: 'relative',
        },
        cell: {
            width: style?.width,
            height: 44,
        },
    })
};

type Styles = typeof styles;

export default class AutoCompleteManagerStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};