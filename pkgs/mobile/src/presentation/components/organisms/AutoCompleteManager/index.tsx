import React from 'react';
import AutoCompleteManagerPresenter from './presenter';
import AutoCompleteManagerComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container = (props: AutoCompleteManagerPresenter.MergedProps) => {
    const output = AutoCompleteManagerPresenter.usePresenter(props);
    return <AutoCompleteManagerComponent {...output} />;
};

const AutoCompleteManager = connect<AutoCompleteManagerPresenter.StateProps, AutoCompleteManagerPresenter.DispatchProps, AutoCompleteManagerPresenter.Input, AutoCompleteManagerPresenter.MergedProps, RootState>(
    AutoCompleteManagerPresenter.mapStateToProps,
    AutoCompleteManagerPresenter.mapDispatchToProps,
    AutoCompleteManagerPresenter.mergeProps,
    AutoCompleteManagerPresenter.connectOptions,
)(Container);

export default AutoCompleteManager;