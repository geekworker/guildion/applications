import React from "react";
import AutoCompleteManagerStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type AutoCompleteManagerPresenter from "./presenter";
import { List } from "immutable";
import { AutoCompletePrefix, Member, MentionType } from "@guildion/core";

namespace AutoCompleteManagerHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new AutoCompleteManagerStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useData = (props: AutoCompleteManagerPresenter.MergedProps & { styles: AutoCompleteManagerStyles }) => {
        const [data, setData] = React.useState<List<MentionType | Member>>(List([]));
        React.useEffect(() => {
            switch (props.prefix) {
            case AutoCompletePrefix.Member: setData(List([MentionType.Members]));
            default: break;
            };
        }, [props.prefix, props.query]);
        const scrollEnabled = React.useMemo(() =>
            data.size * props.styles.cell.height > Number(props.styles.container.maxHeight)
        , [props.styles, data]);
        return {
            data,
            scrollEnabled,
        };
    }
}

export default AutoCompleteManagerHooks