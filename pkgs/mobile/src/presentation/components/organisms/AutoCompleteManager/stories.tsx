import { storiesOf } from '@storybook/react-native';
import React from 'react';
import AutoCompleteManager from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { AutoCompletePrefix } from '@guildion/core';
import { number, text } from '@storybook/addon-knobs';

storiesOf('AutoCompleteManager', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <AutoCompleteManager
            style={new Style({
                width: number('width', 370),
                maxHeight: number('maxHeight', 120),
            })}
            prefix={AutoCompletePrefix.Member}
            query={text('query', '')}
        />
    ))