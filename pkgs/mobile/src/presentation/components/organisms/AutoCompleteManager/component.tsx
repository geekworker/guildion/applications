import { AutoCompletePrefix, unwrapCase, Member, MentionType } from '@guildion/core';
import { isRecord } from 'immutable';
import React from 'react';
import { ListRenderItemInfo, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import MemberAutoCompleteCell from '../../molecules/MemberAutoCompleteCell';
import MentionAutoCompleteCell from '../../molecules/MentionAutoCompleteCell';
import AutoCompleteManagerPresenter from './presenter';

const AutoCompleteManagerComponent = ({ styles, appTheme, data, prefix, scrollEnabled, onPressMemberMention, onPressSystemMention }: AutoCompleteManagerPresenter.Output) => {
    const guardMember = (val: any): val is Member => isRecord(val) && prefix == AutoCompletePrefix.Member;
    const renderCell = (props: ListRenderItemInfo<MentionType | Member>) => guardMember(props.item) ? (
        <MemberAutoCompleteCell member={props.item} style={styles.getStyle('cell')} onPress={onPressMemberMention} />
    ) : unwrapCase(props.item, { cases: MentionType }) ? (
        <MentionAutoCompleteCell type={props.item} style={styles.getStyle('cell')} onPress={onPressSystemMention}/>
    ) : null;
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                style={styles.scrollView}
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={scrollEnabled}
                scrollEnabled={scrollEnabled}
                keyboardDismissMode="on-drag"
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled={scrollEnabled}
                snapToAlignment={'center'}  
                data={data.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: MentionType | Member, index: number) =>
                    guardMember(item) ?
                        item.id! :
                        unwrapCase(item, { cases: MentionType }) ?
                        item
                        : "AutoCompleteManager-" + String(index)
                }
            />
        </View>
    )
};

export default React.memo(AutoCompleteManagerComponent, AutoCompleteManagerPresenter.outputAreEqual);