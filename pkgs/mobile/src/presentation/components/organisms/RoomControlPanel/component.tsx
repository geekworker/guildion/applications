import React from 'react';
import { Animated, TouchableWithoutFeedback, View } from 'react-native';
import RoomControlPanelPresenter from './presenter';
import { localizer } from '@/shared/constants/Localizer';
import { BlurView } from '@react-native-community/blur';
import ControlButton from '../../atoms/ControlButton';
import Ionicon from 'react-native-vector-icons/Ionicons';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const RoomControlPanelComponent = ({ styles, appTheme, animatedStyle, enableVoice, onPressInvite, onPressReaction, onPressNewFile, onPressVoice }: RoomControlPanelPresenter.Output) => {
    return (
        <Animated.View pointerEvents={'box-none'} style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <TouchableWithoutFeedback style={styles.inner}>
                <BlurView
                    style={styles.blur}
                    blurType='dark'
                    blurAmount={50}
                >
                    <ControlButton
                        style={styles.getStyle('invite')}
                        onPress={onPressInvite}
                        title={localizer.dictionary.room.show.controls.invite}
                        Icon={(props) => <Ionicon {...props} name={'person-add'} />}
                    />
                    <ControlButton
                        style={styles.getStyle('reaction')}
                        onPress={onPressReaction}
                        title={localizer.dictionary.room.show.controls.reaction}
                        Icon={(props) => <EntypoIcon {...props} name={'emoji-happy'} />}
                    />
                    <ControlButton
                        style={styles.getStyle('file')}
                        onPress={onPressNewFile}
                        title={localizer.dictionary.room.show.controls.file}
                        Icon={(props) => <MaterialCommunityIcon {...props} name={'access-point'} />}
                    />
                    <ControlButton
                        style={styles.getStyle('voice')}
                        onPress={() => onPressVoice && onPressVoice(!enableVoice)}
                        title={localizer.dictionary.room.show.controls.voice}
                        activeTitle={localizer.dictionary.room.show.controls.mute}
                        active={enableVoice ?? false}
                        Icon={(props) => <FontAwesomeIcon {...props} name={'microphone'} />}
                        ActiveIcon={(props) => <FontAwesomeIcon {...props} name={'microphone-slash'} />}
                    />
                </BlurView>
            </TouchableWithoutFeedback>
        </Animated.View>
    );
};

export default React.memo(RoomControlPanelComponent, RoomControlPanelPresenter.outputAreEqual);