import React from 'react';
import RoomControlPanelStyles from './styles';
import type RoomControlPanelPresenter from './presenter';

namespace RoomControlPanelHooks {
    export const useStyles = (props: RoomControlPanelPresenter.Input) => {
        const styles = React.useMemo(() => new RoomControlPanelStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomControlPanelPresenter.Input) => {
        return {
        };
    }
}

export default RoomControlPanelHooks;