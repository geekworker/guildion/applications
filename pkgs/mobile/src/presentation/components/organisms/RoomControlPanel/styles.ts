import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';
import { calcHeight } from '../../atoms/ControlButton/styles';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const padding = 32;
    const marginCell = 20;
    const width = style?.getAsNumber('width') ?? 0;
    const innerWidth = width - (padding * 2);
    const height = (padding * 2) + (calcHeight({ width: innerWidth }) * 4) + (marginCell * 3) 
    const borderRadius = style?.getAsNumber('borderRadius') || 20;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
            borderRadius,
        },
        inner: {
            width,
            height,
            borderRadius,
        },
        blur: {
            width,
            height,
            borderRadius,
            position: 'relative',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
        },
        invite: {
            width: innerWidth,
        },
        reaction: {
            marginTop: marginCell,
            width: innerWidth,
        },
        file: {
            marginTop: marginCell,
            marginBottom: marginCell,
            width: innerWidth,
        },
        voice: {
            width: innerWidth,
        },
    })
};

type Styles = typeof styles;

export default class RoomControlPanelStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};