import React from 'react';
import RoomControlPanelComponent from './component';
import RoomControlPanelPresenter from './presenter';

const RoomControlPanel = (props: RoomControlPanelPresenter.Input) => {
    const output = RoomControlPanelPresenter.usePresenter(props);
    return <RoomControlPanelComponent {...output} />;
};

export default React.memo(RoomControlPanel, RoomControlPanelPresenter.inputAreEqual);