import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { Members } from '@guildion/core';

const styles = ({
    style,
    appTheme,
    members,
}: StyleProps & { members: Members }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: members.size * (Number(style?.height ?? 0) - 4) + 4,
            height: style?.height,
            position: 'relative',
        },
        scrollView: {
            height: style?.height,
            position: 'relative',
        },
        cell: {
            width: Number(style?.height ?? 0),
            height: style?.height,
            borderRadius: Number(style?.height ?? 0) / 2,
            marginRight: -4,
            color: style?.color,
        },
        truncateCell: {
            width: Number(style?.height ?? 0),
            height: style?.height,
            borderRadius: Number(style?.height ?? 0) / 2,
            borderWidth: 1,
            marginRight: -4,
        },
    });
}

type Styles = typeof styles;

export default class MembersOverlapListStyles extends Record<ReturnType<Styles>>({
    ...styles({ members: new Members([]) })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { members: Members }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};