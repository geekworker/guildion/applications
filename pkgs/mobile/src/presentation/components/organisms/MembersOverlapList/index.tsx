import React from 'react';
import MembersOverlapListComponent from './component';
import MembersOverlapListPresenter from './presenter';

const MembersOverlapList = (props: MembersOverlapListPresenter.Input) => {
    const output = MembersOverlapListPresenter.usePresenter(props);
    return <MembersOverlapListComponent {...output} />;
};

export default React.memo(MembersOverlapList, MembersOverlapListPresenter.inputAreEqual);