import React from "react";
import MembersOverlapListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Member, Members } from "@guildion/core";

namespace MembersOverlapListHooks {
    export const TRUNCATED_CELL_ID: string = 'TRUNCATED';
    export const useStyles = (props: StyleProps & { members: Members }) => {
        const styles = React.useMemo(() => new MembersOverlapListStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useSkeletonMock = (): Members => {
        const members = React.useMemo(() => new Members([
            new Member({ id: '`skeleton1`' }),
            new Member({ id: '`skeleton2`' }),
            new Member({ id: '`skeleton3`' }),
            new Member({ id: '`skeleton4`' }),
            new Member({ id: '`skeleton5`' }),
            new Member({ id: '`skeleton6`' }),
        ]), []);
        return members;
    }

    export const useTrucate = ({ members, truncateCount }: { members: Members, truncateCount?: number }): Members => {
        if (!!truncateCount) {
            const truncated = React.useMemo(() => {
                const results = members.toArray().filter((_, index) => index <= truncateCount)
                return new Members(results.length == truncateCount ? [...results, new Member({ id: TRUNCATED_CELL_ID })] : results);
            }, [members, truncateCount]);
            return truncated;
        } else {
            return members;
        }
    };
}

export default MembersOverlapListHooks