import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MembersOverlapList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Members } from '@guildion/core';

storiesOf('MembersOverlapList', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <MembersOverlapList
            members={new Members([])}
        />
    ))
    .add('Loading', () => (
        <MembersOverlapList
            members={new Members([])}
            loading={true}
        />
    ))
