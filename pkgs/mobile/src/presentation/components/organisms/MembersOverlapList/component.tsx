import { Member } from '@guildion/core';
import React from 'react';
import { ListRenderItemInfo, View, FlatList } from 'react-native';
import MemberCell from '../../molecules/MemberCell';
import TruncateCell from '../../molecules/TruncateCell';
import MembersOverlapListHooks from './hooks';
import MembersOverlapListPresenter from './presenter';

const MembersOverlapListComponent = ({ styles, members, appTheme, onPressCell, truncateCount, maxCount, loading }: MembersOverlapListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Member>)=> maxCount && truncateCount && props.item.id == MembersOverlapListHooks.TRUNCATED_CELL_ID && !loading ? (
        <TruncateCell
            style={styles.getStyle('cell')}
            appTheme={appTheme}
            count={maxCount - truncateCount}
            key={props.index}
        />
    ) : (
        <MemberCell
            style={styles.getStyle('cell')}
            appTheme={appTheme}
            member={props.item}
            key={props.index}
            onPress={onPressCell}
            loading={loading}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                scrollEnabled={false}
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={false}
                showsHorizontalScrollIndicator={false}
                snapToAlignment={'center'}  
                data={members.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: Member, index: number) => item.id! + String(index)}
            />
        </View>
    );
};

export default React.memo(MembersOverlapListComponent, MembersOverlapListPresenter.outputAreEqual);