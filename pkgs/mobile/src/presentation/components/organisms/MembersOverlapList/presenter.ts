import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MembersOverlapListHooks from "./hooks";
import MembersOverlapListStyles from './styles';
import React from 'react';
import { Member, Members } from "@guildion/core";
import { compare } from "@/shared/modules/ObjectCompare";

namespace MembersOverlapListPresenter {
    export type Input = {
        children?: React.ReactNode,
        members: Members,
        onPressCell?: (member: Member) => void,
        maxCount?: number,
        truncateCount?: number,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MembersOverlapListStyles,
        appTheme: AppTheme,
        members: Members,
        onPressCell?: (member: Member) => void,
        maxCount?: number,
        truncateCount?: number,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const mockMembers = MembersOverlapListHooks.useSkeletonMock();
        const $members = props.loading ? mockMembers : props.members;
        const members = MembersOverlapListHooks.useTrucate({ ...props, members: $members });
        const styles = MembersOverlapListHooks.useStyles({ ...props, members });
        return {
            ...props,
            styles,
            members,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default MembersOverlapListPresenter;