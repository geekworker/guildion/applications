import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MembersListHooks from "./hooks";
import MembersListStyles from './styles';
import React from 'react';
import { Member, Members } from "@guildion/core";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";
import { compare } from "@/shared/modules/ObjectCompare";

namespace MembersListPresenter {
    export type Input = {
        children?: React.ReactNode,
        members: Members,
        onPressCell?: (member: Member) => void,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MembersListStyles,
        appTheme: AppTheme,
        members: Members,
        onPressCell?: (member: Member) => void,
        animatedStyle?: AnimatedStyle,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = MembersListHooks.useStyles({ ...props });
        const mockMembers = MembersListHooks.useSkeletonMock();
        const members = props.loading ? mockMembers : props.members;
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            members,
        }
    }
}

export default MembersListPresenter;