import { Member } from '@guildion/core';
import React from 'react';
import { ListRenderItemInfo, FlatList, Animated } from 'react-native';
import MemberCell from '../../molecules/MemberCell';
import MembersListPresenter from './presenter';

const MembersListComponent = ({ styles, members, appTheme, onPressCell, animatedStyle, loading }: MembersListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Member>)=> (
        <MemberCell
            style={styles.getStyle('cell')}
            appTheme={appTheme}
            member={props.item}
            key={props.index}
            onPress={onPressCell}
            loading={loading}
        />
    );
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toObject() }}>
            <FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                scrollEnabled={!loading}
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={!loading}
                showsHorizontalScrollIndicator={false}
                snapToAlignment={'center'}  
                data={members.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: Member, index: number) => item.id! + String(index)}
            />
        </Animated.View>
    );
};

export default React.memo(MembersListComponent, MembersListPresenter.outputAreEqual);