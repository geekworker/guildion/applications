import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    const cellSize = Number(style?.height ?? 0) - 8;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            position: 'relative',
        },
        scrollView: {
            height: style?.height,
            position: 'relative',
        },
        cell: {
            width: cellSize,
            height: cellSize,
            borderRadius: cellSize / 2,
            margin: 4,
            color: style?.color,
        },
    })
};

type Styles = typeof styles;

export default class MembersListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};