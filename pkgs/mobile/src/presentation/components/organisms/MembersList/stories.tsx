import { storiesOf } from '@storybook/react-native';
import React from 'react';
import MembersList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { Member, Members } from '@guildion/core';
import StoreProvider from '@/infrastructure/StoreProvider';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('MembersList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <CenterView>
                {getStory()}
            </CenterView>
        </StoreProvider>
    ))
    .add('Default', () => (
        <MembersList
            members={
                new Members([
                    new Member({
                        id: 'test1',
                        displayName: 'test1',
                    }),
                    new Member({
                        id: 'test2',
                        displayName: 'test2',
                    }),
                    new Member({
                        id: 'test3',
                        displayName: 'test3',
                    }),
                    new Member({
                        id: 'test4',
                        displayName: 'test4',
                    }),
                ])
            }
            style={new Style({
                width: 370,
                height: 54,
                backgroundColor: fallbackAppTheme.background.subm1,
            })}
        />
    ))
    .add('Default', () => (
        <MembersList
            members={
                new Members([
                    new Member({
                        id: 'test1',
                        displayName: 'test1',
                    }),
                    new Member({
                        id: 'test2',
                        displayName: 'test2',
                    }),
                    new Member({
                        id: 'test3',
                        displayName: 'test3',
                    }),
                    new Member({
                        id: 'test4',
                        displayName: 'test4',
                    }),
                ])
            }
            style={new Style({
                width: 370,
                height: 54,
                backgroundColor: fallbackAppTheme.background.subm1,
            })}
            loading={true}
        />
    ))
