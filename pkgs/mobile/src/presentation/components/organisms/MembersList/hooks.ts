import React from "react";
import MembersListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Member, Members } from "@guildion/core";

namespace MembersListHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new MembersListStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useSkeletonMock = (): Members => {
        const members = React.useMemo(() => new Members([
            new Member({ id: '`skeleton1`' }),
            new Member({ id: '`skeleton2`' }),
            new Member({ id: '`skeleton3`' }),
            new Member({ id: '`skeleton4`' }),
            new Member({ id: '`skeleton5`' }),
            new Member({ id: '`skeleton6`' }),
        ]), []);
        return members;
    }
}

export default MembersListHooks