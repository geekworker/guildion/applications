import React from 'react';
import MembersListComponent from './component';
import MembersListPresenter from './presenter';

const MembersList = (props: MembersListPresenter.Input) => {
    const output = MembersListPresenter.usePresenter(props);
    return <MembersListComponent {...output} />;
};

export default React.memo(MembersList, MembersListPresenter.inputAreEqual);