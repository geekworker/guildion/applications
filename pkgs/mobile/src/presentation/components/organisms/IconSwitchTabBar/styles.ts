import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style, count }: StyleProps & { count?: number }) => {
    appTheme ||= fallbackAppTheme;
    const width = Number(style?.width ?? 0)
    const height = Number(style?.height ?? 0);
    count ||= 1;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            height,
        },
        scrollView: {
            width,
            height,
            position: 'relative',
        },
        cell: {
            width: width / count,
            height,
            color: appTheme.element.subp2,
            tintColor: appTheme.element.default,
        },
    })
};

type Styles = typeof styles;

export default class IconSwitchTabBar extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { count?: number }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};