import React from 'react';
import IconSwitchTabBarStyles from './styles';
import type IconSwitchTabBarPresenter from './presenter';

namespace IconSwitchTabBarHooks {
    export const useStyles = (props: IconSwitchTabBarPresenter.Input) => {
        const styles = React.useMemo(() => new IconSwitchTabBarStyles({ ...props, count: props.data.length }), [
            props.appTheme,
            props.style,
            props.data.length,
        ]);
        return styles;
    }

    export const useState = (props: IconSwitchTabBarPresenter.Input) => {
        return {};
    }
}

export default IconSwitchTabBarHooks;