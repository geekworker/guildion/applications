import React from 'react';
import { ListRenderItemInfo, View, FlatList } from 'react-native';
import IconSwitch, { IconSwitchProps } from '../../atoms/IconSwitch';
import IconSwitchTabBarPresenter from './presenter';

const IconSwitchTabBarComponent = ({ styles, appTheme, data, onPress }: IconSwitchTabBarPresenter.Output) => {
    const renderMenu = (props: ListRenderItemInfo<IconSwitchProps>)=> (
        <IconSwitch
            appTheme={appTheme}
            {...props.item}
            style={styles.getStyle('cell')}
            key={props.index}
            onPress={onPress}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={false}
                scrollEnabled={false}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}  
                data={data}
                renderItem={renderMenu}
                keyExtractor={(item: IconSwitchProps, index: number) => item.title! + String(index)}
            />
        </View>
    );
};

export default React.memo(IconSwitchTabBarComponent, IconSwitchTabBarPresenter.outputAreEqual);