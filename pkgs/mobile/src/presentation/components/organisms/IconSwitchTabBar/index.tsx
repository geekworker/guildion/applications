import React from 'react';
import IconSwitchTabBarComponent from './component';
import IconSwitchTabBarPresenter from './presenter';

const IconSwitchTabBar = (props: IconSwitchTabBarPresenter.Input) => {
    const output = IconSwitchTabBarPresenter.usePresenter(props);
    return <IconSwitchTabBarComponent {...output} />;
};

export default React.memo(IconSwitchTabBar, IconSwitchTabBarPresenter.inputAreEqual);