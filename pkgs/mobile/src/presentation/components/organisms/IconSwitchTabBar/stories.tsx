import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconSwitchTabBar from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import Icon from 'react-native-vector-icons/FontAwesome';

storiesOf('IconSwitchTabBar', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <IconSwitchTabBar
            style={new Style({
                width: 375,
                height: 40,
            })}
            data={[
                {
                    title: 'Explore',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
                {
                    title: 'New',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
                {
                    title: 'Manage',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
            ]}
        />
    ))