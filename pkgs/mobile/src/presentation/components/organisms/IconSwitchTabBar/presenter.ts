import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import IconSwitchTabBarHooks from './hooks';
import IconSwitchTabBarStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import { IconSwitchProps } from '../../atoms/IconSwitch';

namespace IconSwitchTabBarPresenter {
    export type Input = {
        data: IconSwitchProps[],
        onPress?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: IconSwitchTabBarStyles,
        appTheme: AppTheme,
        data: IconSwitchProps[],
        onPress?: () => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = IconSwitchTabBarHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default IconSwitchTabBarPresenter;