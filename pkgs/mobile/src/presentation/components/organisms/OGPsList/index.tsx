import React from 'react';
import OGPsListStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import { OGP, OGPs } from '@guildion/core';
import OGPCell from '../../molecules/OGPCell';

type Props = {
    ogps: OGPs,
    onPressOGP?: (ogp: OGP) => void,
} & Partial<StyleProps>;

const OGPsList: React.FC<Props> = ({
    style,
    appTheme,
    ogps,
    onPressOGP,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new OGPsListStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const renderCell = (props: ListRenderItemInfo<OGP>) => (
        <OGPCell
            appTheme={appTheme}
            ogp={props.item}
            onPressOGP={onPressOGP}
            style={styles.getStyle('cell')}
            key={props.index}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                style={styles.scrollView}
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                scrollEnabled={false}
                keyboardDismissMode="on-drag"
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}  
                data={ogps.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: OGP, index: number) => item.id + String(index)}
                listKey={ogps.toArray().map((v) => v.id!).reduce((a, b) => a + b)}
            />
        </View>
    )
};

export default React.memo(OGPsList, compare);