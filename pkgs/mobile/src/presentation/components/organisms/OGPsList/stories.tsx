import { storiesOf } from '@storybook/react-native';
import React from 'react';
import OGPsList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Files, OGP, OGPs } from '@guildion/core';

storiesOf('OGPsList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <OGPsList
            style={new Style({
                width: number('width', 300),
                height: number('height', 44),
            })}
            ogps={new OGPs([
                new OGP({
                    id: '1',
                    title: 'guildion - video community service -',
                    url: 'https://www.guildion.co',
                    imageURL: Files.getSeed().generateGuildProfile().url,
                }),
                new OGP({
                    id: '2',
                    title: 'guildion - video community service -',
                    url: 'https://www.guildion.co',
                    imageURL: Files.getSeed().generateGuildProfile().url,
                }),
                new OGP({
                    id: '3',
                    title: 'guildion - video community service -',
                    url: 'https://www.guildion.co',
                    imageURL: Files.getSeed().generateGuildProfile().url,
                }),
            ])}
        />
    ))