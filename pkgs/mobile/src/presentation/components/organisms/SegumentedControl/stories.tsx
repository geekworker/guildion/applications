import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SegumentedControl from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';

storiesOf('SegumentedControl', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SegumentedControl
            style={new Style({
                width: number('width', 365),
            })}
            data={[
                {
                    id: '1',
                    text: '1',
                },
                {
                    id: '2',
                    text: '2',
                },
                {
                    id: '3',
                    text: '3',
                },
                {
                    id: '4',
                    text: '4',
                },
            ]}
            activedId='3'
        />
    ))
    .add('Title', () => (
        <SegumentedControl
            style={new Style({
                width: number('width', 365),
            })}
            title={'test'}
            data={[
                {
                    id: '1',
                    text: '1',
                },
                {
                    id: '2',
                    text: '2',
                },
                {
                    id: '3',
                    text: '3',
                },
                {
                    id: '4',
                    text: '4',
                },
            ]}
            activedId='3'
        />
    ))