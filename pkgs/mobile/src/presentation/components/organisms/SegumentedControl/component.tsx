import React from 'react';
import { Animated, View, Text } from 'react-native';
import SegumentedControlInput from '../../atoms/SegumentedControlInput';
import SegumentedControlPresenter from './presenter';

const SegumentedControlComponent = ({ styles, appTheme, animatedStyle, activedId, data, onPress, title }: SegumentedControlPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            {title && (
                <Text style={styles.title}>
                    {title}
                </Text>
            )}
            <View style={styles.control}>
                {data.map((d, key) => (
                    <SegumentedControlInput
                        key={key}
                        {...d}
                        onPress={onPress}
                        style={styles.getStyle('cell')}
                    />
                ))}
            </View>
        </Animated.View>
    );
};

export default React.memo(SegumentedControlComponent, SegumentedControlPresenter.outputAreEqual);