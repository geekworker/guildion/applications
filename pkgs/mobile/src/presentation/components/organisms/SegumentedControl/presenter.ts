import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import SegumentedControlHooks from './hooks';
import SegumentedControlStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import type SegumentedControlInputPresenter from '../../atoms/SegumentedControlInput/presenter';

namespace SegumentedControlPresenter {
    export type Input = {
        activedId?: string,
        title?: string,
        onPress?: (activeId?: string) => void,
        data: Omit<SegumentedControlInputPresenter.Input, 'onPress'>[],
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SegumentedControlStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        activedId?: string,
        title?: string,
        onPress: (id: string, value: boolean) => void,
        data: Omit<SegumentedControlInputPresenter.Input, 'onPress'>[],
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SegumentedControlHooks.useStyles({ ...props });
        const {
            data,
            onPress,
            activedId,
        } = SegumentedControlHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            data,
            onPress,
            activedId,
        }
    }
}

export default SegumentedControlPresenter;