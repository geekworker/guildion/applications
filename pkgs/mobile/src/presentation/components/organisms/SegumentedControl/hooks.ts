import React from 'react';
import SegumentedControlStyles from './styles';
import type SegumentedControlPresenter from './presenter';

namespace SegumentedControlHooks {
    export const useStyles = (props: SegumentedControlPresenter.Input) => {
        const dataCount = React.useMemo(() => props.data.length, [props.data.length]);
        const styles = React.useMemo(() => new SegumentedControlStyles({ ...props, dataCount }), [
            props.appTheme,
            props.style,
            dataCount,
        ]);
        return styles;
    }

    export const useState = (props: SegumentedControlPresenter.Input) => {
        const getSelectedData = React.useCallback((activedId?: string) => props.data.find(d => d.id === activedId), [props.data]);
        const [activedId, setActivedId] = React.useState<string | undefined>(getSelectedData(props.activedId) ? props.activedId : undefined);
        const selectedData = React.useMemo(() => getSelectedData(activedId), [activedId]);
        React.useEffect(() => {
            if (activedId !== props.activedId && getSelectedData(props.activedId)) {
                setActivedId(activedId)
            } else if (props.activedId === undefined && !!activedId) {
                setActivedId(undefined);
            };
        }, [props.activedId]);
        const data = React.useMemo(() => 
            props.data.map((d) => {
                d.value = selectedData && selectedData.id == d.id;
                return d;
            })
        , [selectedData, props.data]);
        const onPress = React.useCallback((id: string, value: boolean) => {
            if (!value) return;
            setActivedId(value ? id : undefined);
            props.onPress && props.onPress(id);
        }, [props.onPress]);
        return {
            data,
            onPress,
            activedId,
        };
    }
}

export default SegumentedControlHooks;