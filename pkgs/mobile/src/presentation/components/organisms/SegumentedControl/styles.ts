import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { MontserratFont } from '@/shared/constants/Font';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
    dataCount?: number,
} & StyleProps;

const styles = ({ appTheme, style, dataCount }: Props) => {
    appTheme ||= fallbackAppTheme;
    dataCount ||= 0;
    const width = style?.getAsNumber('width') || 0;
    const margin = 12;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
        },
        title: {
            fontSize: 14,
            fontFamily: MontserratFont.SemiBold,
            color: appTheme.element.default,
            textAlign: 'left',
            marginBottom: 8,
        },
        control: {
            width,
            height: 32,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        cell: {
            width: (width - (margin * (dataCount > 0 ? dataCount - 1 : 0))) / (dataCount > 0 ? dataCount : 1),
            height: 32,
        },
    });
};

type Styles = typeof styles;

export default class SegumentedControlStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};