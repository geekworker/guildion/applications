import React from 'react';
import SegumentedControlComponent from './component';
import SegumentedControlPresenter from './presenter';

const SegumentedControl = (props: SegumentedControlPresenter.Input) => {
    const output = SegumentedControlPresenter.usePresenter(props);
    return <SegumentedControlComponent {...output} />;
};

export default React.memo(SegumentedControl, SegumentedControlPresenter.inputAreEqual);