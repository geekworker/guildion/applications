import React from 'react';
import FilesListStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { compare } from '@/shared/modules/ObjectCompare';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import { File, Files } from '@guildion/core';
import AttachmentCell from '../../molecules/AttachmentCell';

type Props = {
    files: Files,
    onPressFile?: (file: File) => void,
} & Partial<StyleProps>;

const AttachmentsList: React.FC<Props> = ({
    style,
    appTheme,
    files,
    onPressFile,
}) => {
    appTheme ||= fallbackAppTheme;
    const styles = React.useMemo(() => new FilesListStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const renderCell = (props: ListRenderItemInfo<File>) => (
        <AttachmentCell
            appTheme={appTheme}
            file={props.item}
            onPress={onPressFile}
            style={styles.getStyle('cell')}
            key={props.index}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                style={styles.scrollView}
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                scrollEnabled={false}
                keyboardDismissMode="on-drag"
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}  
                data={files.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: File, index: number) => item.id + String(index)}
            />
        </View>
    )
};

export default React.memo(AttachmentsList, compare);