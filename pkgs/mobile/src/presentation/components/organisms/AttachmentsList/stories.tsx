import { storiesOf } from '@storybook/react-native';
import React from 'react';
import AttachmentsList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Files } from '@guildion/core';

storiesOf('AttachmentsList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <AttachmentsList
            style={new Style({
                width: number('width', 300),
                height: number('height', 44),
            })}
            files={new Files([
                Files.getSeed().generateGuildProfile(),
                Files.getSeed().generateGuildProfile(),
                Files.getSeed().generateGuildProfile(),
                Files.getSeed().generateGuildProfile(),
            ])}
        />
    ))