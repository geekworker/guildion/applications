import React from 'react';
import SearchResultsListComponent from './component';
import SearchResultsListPresenter from './presenter';

const SearchResultsList = (props: SearchResultsListPresenter.Input) => {
    const output = SearchResultsListPresenter.usePresenter(props);
    return <SearchResultsListComponent {...output} />;
};

export default React.memo(SearchResultsList, SearchResultsListPresenter.inputAreEqual);