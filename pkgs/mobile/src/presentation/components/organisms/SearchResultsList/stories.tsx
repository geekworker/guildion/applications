import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SearchResultsList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { mockPosts } from '../PostsList/stories';
import { mockRooms } from '../RoomPublicHorizontalList/stories';
import { Guild, Guilds } from '@guildion/core';
import { mockGuild } from '../../molecules/GuildPublicCell/stories';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

storiesOf('SearchResultsList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SearchResultsList
            style={new Style({
                width: 380,
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
        />
    ))
    .add('Loading', () => (
        <SearchResultsList
            style={new Style({
                width: 380,
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
            loading
        />
    ))
    .add('LoadingMore(increment)', () => (
        <SearchResultsList
            style={new Style({
                width: 380,
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
            postsLoadingMore={LoadingMoreStatus.INCREMENT}
        />
    ))
    .add('LoadingMore(decrement)', () => (
        <SearchResultsList
            style={new Style({
                width: 380,
            })}
            posts={mockPosts}
            rooms={mockRooms}
            guilds={new Guilds([mockGuild])}
            postsLoadingMore={LoadingMoreStatus.DECREMENT}
        />
    ))