import React from 'react';
import SearchResultsListStyles from './styles';
import type SearchResultsListPresenter from './presenter';
import { SectionListData } from 'react-native';
import { Guild, Post, Posts, PostStatus, Room } from '@guildion/core';
import { List } from 'immutable';
import { localizer } from '@/shared/constants/Localizer';
import uuid from 'react-native-uuid';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

namespace SearchResultsListHooks {
    export const useStyles = (props: SearchResultsListPresenter.Input) => {
        const styles = React.useMemo(() => new SearchResultsListStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SearchResultsListPresenter.Input) => {
        const mockPosts = useSkeletonMock();
        const postsLoadingMoreMockRooms = useLoadingMoreMock(props);
        const items: List<SectionListData<Room | Guild | Post, { title: string, type: 'Guilds' | 'Rooms' | 'Posts' }>> = React.useMemo(() => {
            const sections: Array<SectionListData<Room | Guild | Post, { title: string, type: 'Guilds' | 'Rooms' | 'Posts' }>> = [];
            if (props.guilds.size > 0 && !props.loading) {
                sections.push({
                    title: localizer.dictionary.explore.attr.guilds,
                    type: 'Guilds',
                    data: props.guilds.toArray(),
                });
            }
            if (props.rooms.size > 0 && !props.loading) {
                sections.push({
                    title: localizer.dictionary.explore.attr.rooms,
                    type: 'Rooms',
                    data: props.rooms.toArray(),
                });
            }
            if (props.posts.size > 0) {
                sections.push({
                    title: localizer.dictionary.explore.attr.posts,
                    type: 'Posts',
                    data: props.loading ? mockPosts : props.postsLoadingMore ? postsLoadingMoreMockRooms.toArray() : props.posts,
                });
            }
            return List(sections);
        }, [props.rooms, props.guilds, props.posts, props.loading, props.postsLoadingMore, mockPosts, postsLoadingMoreMockRooms]);
        return {
            items,
        };
    }

    export const postsLoadingMoreMockCount: number = 4;
    export const useLoadingMoreMock = (props: SearchResultsListPresenter.Input): Posts => {
        const [mocks, setMocks] = React.useState<Posts>(new Posts([]));
        React.useEffect(() => {
            props.postsLoadingMore && mocks.size == 0 ?
                setMocks(new Posts(
                    Array.from(Array(postsLoadingMoreMockCount).keys()).map((i: number) => 
                        new Post({ id: `${uuid.v4()}-${i}`, status: PostStatus.LOADING })
                    )
                )) :
                setMocks(new Posts([]))
        }, [props.postsLoadingMore]);
        return mocks.size > 0 && props.postsLoadingMore == LoadingMoreStatus.DECREMENT ?
            props.posts.concat(mocks) :
            mocks.size > 0 && props.postsLoadingMore == LoadingMoreStatus.INCREMENT ?
            mocks.concat(props.posts) :
            props.posts;
    }

    export const useSkeletonMock = (): Posts => {
        const posts = React.useMemo(() => new Posts([
            new Post({ id: '`skeleton1`', status: PostStatus.LOADING }),
            new Post({ id: '`skeleton2`', status: PostStatus.LOADING }),
            new Post({ id: '`skeleton3`', status: PostStatus.LOADING }),
            new Post({ id: '`skeleton4`', status: PostStatus.LOADING }),
            new Post({ id: '`skeleton5`', status: PostStatus.LOADING }),
        ]), []);
        return posts;
    }
}

export default SearchResultsListHooks;