import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import SearchResultsListHooks from './hooks';
import SearchResultsListStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { File, Guild, Guilds, Member, Post, Posts, Room, Rooms } from '@guildion/core';
import { Animated, NativeScrollEvent, NativeSyntheticEvent, SectionListData, SectionListRenderItemInfo, ViewStyle } from 'react-native';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { List } from 'immutable';

namespace SearchResultsListPresenter {
    export type Input = {
        guilds: Guilds,
        rooms: Rooms,
        posts: Posts,
        onPressGuild?: (guild: Guild) => void,
        onPressRoom?: (room: Room) => void,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        loading?: boolean,
        postsLoadingMore?: LoadingMoreStatus,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SearchResultsListStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        items: List<SectionListData<Room | Guild | Post, { title: string, type: 'Guilds' | 'Rooms' | 'Posts' }>>,
    } & Omit<Input, 'style' | 'appTheme'>;

    export type SectionListInfo = SectionListData<Room | Guild | Post, { title: string, type: 'Guilds' | 'Rooms' | 'Posts' }>;
    export type SectionListRenderInfo = SectionListRenderItemInfo<Room | Guild | Post, { title: string, type: 'Guilds' | 'Rooms' | 'Posts' }>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return (
            compare(prevProps, nextProps) &&
            prevProps.items.size !== nextProps.items.size
        );
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SearchResultsListHooks.useStyles({ ...props });
        const {
            items,
        } = SearchResultsListHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            items,
        }
    }
}

export default SearchResultsListPresenter;