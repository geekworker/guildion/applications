import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            position: 'relative',
        },
        scrollView: {
            width: style?.width,
            height: style?.height,
            position: 'relative',
        },
        header: {
            width: Number(style?.width ?? 0) - 24,
            height: 44,
            marginTop: 4,
            marginBottom: 4,
            marginLeft: 12,
            marginRight: 12,
        },
        cell: {
            width: Number(style?.width ?? 0) - 24,
            marginTop: 4,
            marginBottom: 4,
            marginLeft: 12,
            marginRight: 12,
        },
        postCell: {
            width,
            marginTop: 8,
            marginBottom: 8,
        },
    });
};

type Styles = typeof styles;

export default class SearchResultsListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};