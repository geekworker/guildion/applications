import { Guild, Post, PostStatus, Room } from '@guildion/core';
import { isRecord } from 'immutable';
import React from 'react';
import { Animated } from 'react-native';
import TitleSectionListHeader from '../../atoms/TitleSectionListHeader';
import GuildPublicCell from '../../molecules/GuildPublicCell';
import PostCell from '../../molecules/PostCell';
import RoomPublicCell from '../../molecules/RoomPublicCell';
import SearchResultsListPresenter from './presenter';

const SearchResultsListComponent = ({ styles, appTheme, animatedStyle, guilds, rooms, posts, onPressGuild, onPressRoom, onPressSender, onPressSeeMore, onPressComment, onPressReaction, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, loading, postsLoadingMore, ListHeaderComponent, ListFooterComponent, onScroll, items }: SearchResultsListPresenter.Output) => {
    const renderCell = (props: SearchResultsListPresenter.SectionListRenderInfo) =>
        props.section.type == 'Guilds' && isRecord ? (
        <GuildPublicCell
            key={props.section.key ?? props.index}
            guild={props.item as Guild}
            style={styles.getStyle('cell')}
            loading={loading}
            onPress={onPressGuild}
        />
    ) : props.section.type == 'Rooms' ? (
        <RoomPublicCell
            key={props.section.key ?? props.index}
            room={props.item as Room}
            style={styles.getStyle('cell')}
            loading={loading}
            onPress={onPressRoom}
        />
    ) : props.section.type == 'Posts' ? (
        <PostCell
            key={props.section.key ?? props.index}
            post={props.item as Post}
            style={styles.getStyle('postCell')}
            loading={props.item.status == PostStatus.LOADING || loading}
            onPressSender={onPressSender}
            onPressSeeMore={onPressSeeMore}
            onPressComment={onPressComment}
            onPressReaction={onPressReaction}
            onPressFile={onPressFile}
            onPressURL={onPressURL}
            onPressEmail={onPressEmail}
            onPressPhoneNumber={onPressPhoneNumber}
            onPressMentionMember={onPressMentionMember}
            onPressMentionRoom={onPressMentionRoom}
        />
    ) : (
        <></>
    );
    const renderSectionHeader = (props: { section: SearchResultsListPresenter.SectionListInfo }) => (
        <TitleSectionListHeader
            title={props.section.title}
            style={styles.getStyle('header')}
        />
    );
    return (
        <Animated.SectionList
            style={styles.container}
            directionalLockEnabled
            automaticallyAdjustContentInsets={false}
            alwaysBounceVertical={false}
            alwaysBounceHorizontal={false}
            bounces={!loading}
            scrollEnabled={!loading}
            keyboardDismissMode="on-drag"
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            snapToAlignment={'center'}  
            sections={items.toArray()}
            renderItem={renderCell}
            renderSectionHeader={renderSectionHeader}
            keyExtractor={(item: Room | Guild | Post, index: number) => item.id! + String(index)}
            ListHeaderComponent={ListHeaderComponent}
            ListFooterComponent={ListFooterComponent}
            onScroll={onScroll}
            scrollEventThrottle={16}
            stickySectionHeadersEnabled={false}
        />
    );
};

export default React.memo(SearchResultsListComponent, SearchResultsListPresenter.outputAreEqual);