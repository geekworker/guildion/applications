import React from 'react';
import WidgetsListStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import WidgetCell from '../../molecules/WidgetCell';
import { Widget, Widgets } from '@guildion/core';

type Props = {
    widgets: Widgets,
    onPressWidget?: (widget: Widget) => void,
} & Partial<StyleProps>;

const WidgetsList: React.FC<Props> = ({
    style,
    appTheme,
    widgets,
    onPressWidget,
}) => {
    const styles = React.useMemo(() => new WidgetsListStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const renderMenu = (props: ListRenderItemInfo<Widget>)=> (
        <WidgetCell
            appTheme={appTheme}
            style={styles.getStyle('cell')}
            buttonTitle='TRY IT'
            widget={props.item}
            onPress={() => onPressWidget && onPressWidget(props.item)}
        />
    );
    const numColumns = React.useMemo(() => {
        if (!style || !style.width || !Number(style.width)) return 3;
        return Math.max(3, parseInt(`${Number(style.width) / styles.cell.maxWidth}`));
    }, [style, styles]);
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.scrollView}
                numColumns={numColumns}
                key={numColumns}
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={false}
                scrollEnabled={false}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={false}
                snapToAlignment={'center'}
                data={widgets.toArray()}
                renderItem={renderMenu}
                keyExtractor={(item: Widget, index: number) => `${numColumns}-${item.id}-${index}`}
            />
        </View>
    )
}

export default WidgetsList;