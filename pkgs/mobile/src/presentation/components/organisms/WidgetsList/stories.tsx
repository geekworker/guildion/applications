import { storiesOf } from '@storybook/react-native';
import React from 'react';
import WidgetsList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import { TextChatWidgetName, Widget, Widgets } from '@guildion/core';
import { number } from '@storybook/addon-knobs';
import { Style } from '@/shared/interfaces/Style';

storiesOf('WidgetsList', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <WidgetsList
            style={new Style({
                width: number('width', 385),
            })}
            widgets={new Widgets([
                new Widget({
                    id: '1',
                    widgetname: TextChatWidgetName,
                    enName: 'TEXT CHAT',
                    enDescription: 'You can chat with this room members while watching video' 
                }),
                new Widget({
                    id: '2',
                    widgetname: TextChatWidgetName,
                    enName: 'TEXT CHAT',
                    enDescription: 'You can chat with this room members while watching video' 
                }),
                new Widget({
                    id: '3',
                    widgetname: TextChatWidgetName,
                    enName: 'TEXT CHAT',
                    enDescription: 'You can chat with this room members while watching video' 
                }),
                new Widget({
                    id: '4',
                    widgetname: TextChatWidgetName,
                    enName: 'TEXT CHAT',
                    enDescription: 'You can chat with this room members while watching video' 
                }),
                new Widget({
                    id: '5',
                    widgetname: TextChatWidgetName,
                    enName: 'TEXT CHAT',
                    enDescription: 'You can chat with this room members while watching video' 
                }),
                new Widget({
                    id: '6',
                    widgetname: TextChatWidgetName,
                    enName: 'TEXT CHAT',
                    enDescription: 'You can chat with this room members while watching video' 
                }),
            ])}
        >
            
        </WidgetsList>
    ))
