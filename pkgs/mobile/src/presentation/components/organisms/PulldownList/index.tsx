import React from 'react';
import PulldownListComponent from './component';
import PulldownListPresenter from './presenter';

const PulldownList = (props: PulldownListPresenter.Input) => {
    const output = PulldownListPresenter.usePresenter(props);
    return <PulldownListComponent {...output} />;
};

export default React.memo(PulldownList, PulldownListPresenter.inputAreEqual);