import React from 'react';
import PulldownListStyles from './styles';
import type PulldownListPresenter from './presenter';

namespace PulldownListHooks {
    export const useStyles = (props: PulldownListPresenter.Input) => {
        const styles = React.useMemo(() => new PulldownListStyles(props), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: PulldownListPresenter.Input) => {
        return {};
    }
}

export default PulldownListHooks;