import React from 'react';
import { Animated, View } from 'react-native';
import PulldownListPresenter from './presenter';

const PulldownListComponent = ({ styles, appTheme, animatedStyle,  }: PulldownListPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            
        </Animated.View>
    );
};

export default React.memo(PulldownListComponent, PulldownListPresenter.outputAreEqual);