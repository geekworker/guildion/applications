import React from 'react';
import IconSwitchListStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import IconSwitch, { IconSwitchProps } from '../../atoms/IconSwitch';
import { compare } from '@/shared/modules/ObjectCompare';

export type IconSwitchData = (IconSwitchProps | 'border')[];

type Props = {
    data: IconSwitchData,
    loading?: boolean,
} & Partial<StyleProps>;

const IconSwitchList: React.FC<Props> = ({
    style,
    appTheme,
    data,
    loading,
}) => {
    const styles = React.useMemo(() => new IconSwitchListStyles({
        style,
        appTheme
    }), [
        style,
        appTheme,
    ]);
    const renderSwitch = (props: ListRenderItemInfo<IconSwitchProps | 'border'>) => props.item == 'border' ? (
        <View style={{ ...styles.border, display: loading ? 'none' : 'flex' }}/>
    ) : (
        <IconSwitch
            appTheme={appTheme}
            {...props.item}
            style={styles.getStyle('button')}
            key={props.index}
            loading={loading}
        />
    );
    const scrollEnabled = React.useMemo(() =>
        data.filter(d => d != 'border').length * styles.button.width > styles.container.width
    , [data, styles]);
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={scrollEnabled && !loading}
                scrollEnabled={scrollEnabled && !loading}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}  
                data={data}
                renderItem={renderSwitch}
                keyExtractor={(item: IconSwitchProps | 'border', index: number) => (item == 'border' ? item : item.title!) + String(index)}
            />
        </View>
    )
};

export default React.memo(IconSwitchList, compare);