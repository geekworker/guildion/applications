import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const PADDING: number = 4;
const styles = ({
    style,
    appTheme,
}: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: Number(style?.width ?? 0),
            height: Number(style?.height ?? 0),
            position: 'relative',
        },
        scrollView: {
            height: style?.height,
            position: 'relative',
        },
        button: {
            width: Number(style?.height ?? 0),
            height: Number(style?.height ?? 0) - PADDING - PADDING,
            color: style?.color ?? appTheme.element.default,
            marginTop: PADDING,
            marginBottom: PADDING,
        },
        border: {
            width: 1,
            height: Number(style?.height ?? 0) - 8 - PADDING - PADDING,
            marginTop: 4 + PADDING,
            marginBottom: 4 + PADDING,
            marginLeft: 4,
            marginRight: 4,
            backgroundColor: style?.color ?? appTheme.element.default,
        },
    });
}

type Styles = typeof styles;

export default class IconSwitchListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};