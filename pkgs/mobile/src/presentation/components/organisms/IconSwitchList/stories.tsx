import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconSwitchList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import Icon from 'react-native-vector-icons/FontAwesome';
import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';

storiesOf('IconSwitchList', module)
    .addDecorator((getStory) => <CenterView>{getStory()}</CenterView>)
    .add('Default', () => (
        <IconSwitchList
            data={[
                {
                    title: 'Rooms1',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms2',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                'border',
                {
                    title: 'Rooms3',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms4',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms5',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms6',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
            ]}
            style={new Style({
                width: 385,
                height: 48,
                backgroundColor: fallbackAppTheme.background.subm1,
            })}
        />
    ))
    .add('Loading', () => (
        <IconSwitchList
            data={[
                {
                    title: 'Rooms1',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms2',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                'border',
                {
                    title: 'Rooms3',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms4',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms5',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
                {
                    title: 'Rooms6',
                    Icon: (props) => <Icon size={props.size} color={props.color} name={'camera'} />,
                },
            ]}
            style={new Style({
                width: 385,
                height: 48,
                backgroundColor: fallbackAppTheme.background.subm1,
            })}
            loading={true}
        />
    ))
