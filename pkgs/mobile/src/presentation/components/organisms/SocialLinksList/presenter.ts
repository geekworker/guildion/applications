import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import SocialLinksListHooks from './hooks';
import SocialLinksListStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { SocialLink, SocialLinks } from '@guildion/core';
import { ViewStyle } from 'react-native';

namespace SocialLinksListPresenter {
    export type Input = {
        socialLinks: SocialLinks,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: SocialLinksListStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        socialLinks: SocialLinks,
        onPressSocialLink?: (socialLink: SocialLink) => void,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = SocialLinksListHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default SocialLinksListPresenter;