import React from 'react';
import SocialLinksListStyles from './styles';
import type SocialLinksListPresenter from './presenter';

namespace SocialLinksListHooks {
    export const useStyles = (props: SocialLinksListPresenter.Input) => {
        const styles = React.useMemo(() => new SocialLinksListStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: SocialLinksListPresenter.Input) => {
        return {};
    }
}

export default SocialLinksListHooks;