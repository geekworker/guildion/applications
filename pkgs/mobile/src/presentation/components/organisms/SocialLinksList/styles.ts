import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';
import { getPadding } from '../../molecules/SocialLinkCell/styles';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const height = style?.getAsNumber('height') || 0;
    const padding = getPadding(height);
    const containerHeight = padding + height;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            position: 'relative',
            height: containerHeight,
            margin: -padding,
        },
        scrollView: {
            position: 'relative',
            height: containerHeight,
        },
        cell: {
            width: height,
            height,
            padding,
            color: style?.color,
            marginLeft: height / 2,
            marginRight: height / 2,
        },
        firstCell: {
            width: height,
            height,
            padding,
            color: style?.color,
            marginLeft: 0,
            marginRight: height / 2,
            paddingLeft: padding * 2,
        },
        lastCell: {
            width: height,
            height,
            padding,
            paddingRight: padding * 2,
            color: style?.color,
            marginLeft: height / 2,
            marginRight: 0,
        },
        cellInner: {
            width: height,
            height,
            color: style?.color,
        },
    });
};

type Styles = typeof styles;

export default class SocialLinksListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};