import { storiesOf } from '@storybook/react-native';
import React from 'react';
import SocialLinksList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { SocialLink, SocialLinks } from '@guildion/core';
import { number } from '@storybook/addon-knobs';

storiesOf('SocialLinksList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <SocialLinksList
            style={new Style({
                height: number('height', 24)
            })}
            socialLinks={new SocialLinks([
                new SocialLink({
                    url: 'https://www.youtube.com/channel/UC1l8jsqYmIj1bjCzN43UPfA',
                }),
                new SocialLink({
                    url: 'https://twitter.com/__I_OXO_I__',
                }),
                new SocialLink({
                    url: 'https://www.guildion.co',
                }),
            ])}
        />
    ))