import { SocialLink } from '@guildion/core';
import React from 'react';
import { Animated, ListRenderItemInfo, View } from 'react-native';
import SocialLinkCell from '../../molecules/SocialLinkCell';
import SocialLinksListPresenter from './presenter';

const SocialLinksListComponent = ({ styles, appTheme, animatedStyle, socialLinks, onPressSocialLink, ListHeaderComponentStyle, ListHeaderComponent, ListFooterComponent, ListFooterComponentStyle }: SocialLinksListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<SocialLink>)=> (
        <View style={styles.getStyle(props.index == 0 ? 'firstCell' : props.index == socialLinks.size - 1 ? 'lastCell' : 'cell').toStyleObject()}>
            <SocialLinkCell
                style={styles.getStyle('cellInner')}
                appTheme={appTheme}
                socialLink={props.item}
                key={props.index}
                onPress={onPressSocialLink}
            />
        </View>
    );
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <Animated.FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={false}
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={false}
                scrollEnabled={false}
                data={socialLinks.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: SocialLink, index: number) => item.id! + String(index)}
                ListHeaderComponent={ListHeaderComponent}
                ListFooterComponent={ListFooterComponent}
                ListHeaderComponentStyle={ListHeaderComponentStyle}
                ListFooterComponentStyle={ListFooterComponentStyle}
            />
        </Animated.View>
    );
};

export default React.memo(SocialLinksListComponent, SocialLinksListPresenter.outputAreEqual);