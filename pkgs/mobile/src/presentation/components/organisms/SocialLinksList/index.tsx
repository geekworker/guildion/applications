import React from 'react';
import SocialLinksListComponent from './component';
import SocialLinksListPresenter from './presenter';

const SocialLinksList = (props: SocialLinksListPresenter.Input) => {
    const output = SocialLinksListPresenter.usePresenter(props);
    return <SocialLinksListComponent {...output} />;
};

export default React.memo(SocialLinksList, SocialLinksListPresenter.inputAreEqual);