import React from 'react';
import { Animated, View } from 'react-native';
import NotificationsListPresenter from './presenter';

const NotificationsListComponent = ({ styles, appTheme, animatedStyle,  }: NotificationsListPresenter.Output) => {
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            
        </Animated.View>
    );
};

export default React.memo(NotificationsListComponent, NotificationsListPresenter.outputAreEqual);