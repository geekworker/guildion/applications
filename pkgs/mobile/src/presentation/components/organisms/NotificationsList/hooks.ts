import React from 'react';
import NotificationsListStyles from './styles';
import type NotificationsListPresenter from './presenter';

namespace NotificationsListHooks {
    export const useStyles = (props: NotificationsListPresenter.Input) => {
        const styles = React.useMemo(() => new NotificationsListStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: NotificationsListPresenter.Input) => {
        return {};
    }
}

export default NotificationsListHooks;