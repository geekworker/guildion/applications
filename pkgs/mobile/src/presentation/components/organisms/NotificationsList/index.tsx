import React from 'react';
import NotificationsListComponent from './component';
import NotificationsListPresenter from './presenter';

const NotificationsList = (props: NotificationsListPresenter.Input) => {
    const output = NotificationsListPresenter.usePresenter(props);
    return <NotificationsListComponent {...output} />;
};

export default React.memo(NotificationsList, NotificationsListPresenter.inputAreEqual);