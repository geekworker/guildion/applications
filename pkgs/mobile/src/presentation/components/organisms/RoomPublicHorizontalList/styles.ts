import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    const height = style?.getAsNumber('height') || 0;
    const cellWidth = width - 80;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            paddingLeft: 0,
            paddingRight: 0,
            width,
            height,
            position: 'relative',
        },
        scrollView: {
            height,
            position: 'relative',
        },
        cell: {
            width: cellWidth,
            height,
            color: style?.color,
            marginLeft: 4,
            marginRight: 4,
        },
        firstCell: {
            width: cellWidth,
            height,
            color: style?.color,
            marginLeft: style?.paddingLeft,
            marginRight: 4,
        },
        lastCell: {
            width: cellWidth,
            height,
            color: style?.color,
            marginLeft: 4,
            marginRight: style?.paddingRight,
        },
    });
};

type Styles = typeof styles;

export default class RoomPublicHorizontalListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};