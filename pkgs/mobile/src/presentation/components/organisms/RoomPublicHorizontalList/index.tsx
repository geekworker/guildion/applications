import React from 'react';
import RoomPublicHorizontalListComponent from './component';
import RoomPublicHorizontalListPresenter from './presenter';

const RoomPublicHorizontalList = (props: RoomPublicHorizontalListPresenter.Input) => {
    const output = RoomPublicHorizontalListPresenter.usePresenter(props);
    return <RoomPublicHorizontalListComponent {...output} />;
};

export default React.memo(RoomPublicHorizontalList, RoomPublicHorizontalListPresenter.inputAreEqual);