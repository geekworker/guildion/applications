import React from 'react';
import RoomPublicHorizontalListStyles from './styles';
import type RoomPublicHorizontalListPresenter from './presenter';
import { Room, Rooms } from '@guildion/core';

namespace RoomPublicHorizontalListHooks {
    export const useStyles = (props: RoomPublicHorizontalListPresenter.Input) => {
        const styles = React.useMemo(() => new RoomPublicHorizontalListStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomPublicHorizontalListPresenter.Input) => {
        return {};
    }

    export const useSkeletonMock = (): Rooms => {
        const rooms = React.useMemo(() => new Rooms([
            new Room({ id: '`skeleton1`' }),
            new Room({ id: '`skeleton2`' }),
        ]), []);
        return rooms;
    }
}

export default RoomPublicHorizontalListHooks;