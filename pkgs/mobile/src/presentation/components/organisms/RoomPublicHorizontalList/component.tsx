import React from 'react';
import { Animated, ListRenderItemInfo, FlatList } from 'react-native';
import RoomPublicHorizontalListPresenter from './presenter';
import RoomPublicCell from '../../molecules/RoomPublicCell';
import { Room } from '@guildion/core';

const RoomPublicHorizontalListComponent = ({ styles, appTheme, animatedStyle, rooms, loading, onPress, ListHeaderComponentStyle, ListHeaderComponent, ListFooterComponent, ListFooterComponentStyle, onScroll }: RoomPublicHorizontalListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Room>)=> (
        <RoomPublicCell
            style={styles.getStyle(props.index == 0 ? 'firstCell' : props.index == rooms.size - 1 ? 'lastCell' : 'cell')}
            appTheme={appTheme}
            room={props.item}
            key={props.index}
            onPress={onPress}
            loading={loading}
        />
    );
    return (
        <Animated.View style={{ ...styles.container, ...animatedStyle?.toStyleObject() }}>
            <Animated.FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={!loading}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={!loading}
                scrollEnabled={!loading}
                data={rooms.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: Room, index: number) => item.id! + String(index)}
                ListHeaderComponent={ListHeaderComponent}
                ListFooterComponent={ListFooterComponent}
                ListHeaderComponentStyle={ListHeaderComponentStyle}
                ListFooterComponentStyle={ListFooterComponentStyle}
                onScroll={onScroll}
                scrollEventThrottle={16}
            />
        </Animated.View>
    );
};

export default React.memo(RoomPublicHorizontalListComponent, RoomPublicHorizontalListPresenter.outputAreEqual);