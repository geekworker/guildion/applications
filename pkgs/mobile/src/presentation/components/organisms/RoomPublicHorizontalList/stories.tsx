import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomPublicHorizontalList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Member, Members, Message, Room, Rooms, RoomActivity, RoomLog, Files } from '@guildion/core';
import { number, text } from '@storybook/addon-knobs';

export const mockRooms = new Rooms([
    new Room({
        displayName: text('displayName', 'Test Room'),
        profile: Files.getSeed().generateUserProfile().toJSON(),
        activity: new RoomActivity({
            log: new RoomLog({
                message: new Message({
                    body: text('message', "Let talk about this video !!")
                }),
            }).toJSON(),
            connectsCount: number('connectsCount', 12),
            notificationsCount: number('notificationsCount', 1),
            connectingMembers: new Members([
                new Member({
                    id: 'test1',
                    displayName: 'test1',
                }).toJSON(),
                new Member({
                    id: 'test2',
                    displayName: 'test2',
                }).toJSON(),
                new Member({
                    id: 'test3',
                    displayName: 'test3',
                }).toJSON(),
                new Member({
                    id: 'test4',
                    displayName: 'test4',
                }).toJSON(),
            ]).toJSON(),
            createdAt: new Date().toString(),
        }).toJSON(),
    }),
    new Room({
        displayName: text('displayName', 'Test Room'),
        profile: Files.getSeed().generateUserProfile().toJSON(),
        activity: new RoomActivity({
            log: new RoomLog({
                message: new Message({
                    body: text('message', "Let talk about this video !!")
                }),
            }).toJSON(),
            connectsCount: number('connectsCount', 12),
            notificationsCount: number('notificationsCount', 1),
            connectingMembers: new Members([
                new Member({
                    id: 'test1',
                    displayName: 'test1',
                }).toJSON(),
                new Member({
                    id: 'test2',
                    displayName: 'test2',
                }).toJSON(),
                new Member({
                    id: 'test3',
                    displayName: 'test3',
                }).toJSON(),
                new Member({
                    id: 'test4',
                    displayName: 'test4',
                }).toJSON(),
            ]).toJSON(),
            createdAt: new Date().toString(),
        }).toJSON(),
    }),
    new Room({
        displayName: text('displayName', 'Test Room'),
        profile: Files.getSeed().generateUserProfile().toJSON(),
        activity: new RoomActivity({
            log: new RoomLog({
                message: new Message({
                    body: text('message', "Let talk about this video !!")
                }),
            }).toJSON(),
            connectsCount: number('connectsCount', 12),
            notificationsCount: number('notificationsCount', 1),
            connectingMembers: new Members([
                new Member({
                    id: 'test1',
                    displayName: 'test1',
                }).toJSON(),
                new Member({
                    id: 'test2',
                    displayName: 'test2',
                }).toJSON(),
                new Member({
                    id: 'test3',
                    displayName: 'test3',
                }).toJSON(),
                new Member({
                    id: 'test4',
                    displayName: 'test4',
                }).toJSON(),
            ]).toJSON(),
            createdAt: new Date().toString(),
        }).toJSON(),
    }),
    new Room({
        displayName: text('displayName', 'Test Room'),
        profile: Files.getSeed().generateUserProfile().toJSON(),
        activity: new RoomActivity({
            log: new RoomLog({
                message: new Message({
                    body: text('message', "Let talk about this video !!")
                }),
            }).toJSON(),
            connectsCount: number('connectsCount', 12),
            notificationsCount: number('notificationsCount', 1),
            connectingMembers: new Members([
                new Member({
                    id: 'test1',
                    displayName: 'test1',
                }).toJSON(),
                new Member({
                    id: 'test2',
                    displayName: 'test2',
                }).toJSON(),
                new Member({
                    id: 'test3',
                    displayName: 'test3',
                }).toJSON(),
                new Member({
                    id: 'test4',
                    displayName: 'test4',
                }).toJSON(),
            ]).toJSON(),
            createdAt: new Date().toString(),
        }).toJSON(),
    }),
    new Room({
        displayName: text('displayName', 'Test Room'),
        profile: Files.getSeed().generateUserProfile().toJSON(),
        activity: new RoomActivity({
            log: new RoomLog({
                message: new Message({
                    body: text('message', "Let talk about this video !!")
                }),
            }).toJSON(),
            connectsCount: number('connectsCount', 12),
            notificationsCount: number('notificationsCount', 1),
            connectingMembers: new Members([
                new Member({
                    id: 'test1',
                    displayName: 'test1',
                }).toJSON(),
                new Member({
                    id: 'test2',
                    displayName: 'test2',
                }).toJSON(),
                new Member({
                    id: 'test3',
                    displayName: 'test3',
                }).toJSON(),
                new Member({
                    id: 'test4',
                    displayName: 'test4',
                }).toJSON(),
            ]).toJSON(),
            createdAt: new Date().toString(),
        }).toJSON(),
    }),
])

storiesOf('RoomPublicHorizontalList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomPublicHorizontalList
            style={new Style({
                width: number('width', 385),
                height: number('height', 88),
            })}
            rooms={mockRooms}
        />
    ))
    .add('Loading', () => (
        <RoomPublicHorizontalList
            style={new Style({
                width: number('width', 385),
                height: number('height', 88),
            })}
            rooms={new Rooms([])}
            loading={true}
        />
    ))