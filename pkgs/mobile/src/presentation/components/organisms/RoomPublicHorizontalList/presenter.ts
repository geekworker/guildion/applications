import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomPublicHorizontalListHooks from './hooks';
import RoomPublicHorizontalListStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Room, Rooms } from '@guildion/core';
import { Animated, NativeScrollEvent, NativeSyntheticEvent, ViewStyle } from 'react-native';

namespace RoomPublicHorizontalListPresenter {
    export type Input = {
        rooms: Rooms,
        onPress?: (room: Room) => void,
        loading?: boolean,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomPublicHorizontalListStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        rooms: Rooms,
        onPress?: (room: Room) => void,
        loading?: boolean,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomPublicHorizontalListHooks.useStyles({ ...props });
        const mockRooms = RoomPublicHorizontalListHooks.useSkeletonMock();
        const rooms = props.loading ? mockRooms : props.rooms;
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            rooms,
        }
    }
}

export default RoomPublicHorizontalListPresenter;