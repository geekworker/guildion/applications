import { storiesOf } from '@storybook/react-native';
import React from 'react';
import GuildsTabBar from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import { Guild, GuildActivity, Guilds } from '@guildion/core';
import { Style } from '@/shared/interfaces/Style';
import { boolean } from '@storybook/addon-knobs';

storiesOf('GuildsTabBar', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <CenterView>
                {getStory()}
            </CenterView>
        </StoreProvider>
    ))
    .add('Default', () => (
        <GuildsTabBar
            style={new Style({
                width: 375,
                height: 90,
            })}
            selectedID={'1'}
            loading={boolean('loading', false)}
            guilds={new Guilds([
                new Guild({
                    id: '1',
                    displayName: 'sample guilddddd',
                }),
                new Guild({
                    id: '2',
                    displayName: 'sample guilddddd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
            ])}
        />
    ))
    .add('Loading', () => (
        <GuildsTabBar
            style={new Style({
                width: 375,
                height: 90,
            })}
            selectedID={'1'}
            loading={boolean('loading', true)}
            guilds={new Guilds([
                new Guild({
                    id: '1',
                    displayName: 'sample guilddddd',
                }),
                new Guild({
                    id: '2',
                    displayName: 'sample guilddddd',
                    activity: new GuildActivity({
                        notificationsCount: 1,
                    })
                }),
            ])}
        />
    ))
