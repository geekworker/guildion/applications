import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

export const guildsTabBarMargin = 4;
export const guildsTabBarWidth = 78;
export const guildsTabBarHeight = 78;

const styles = ({
    style,
    appTheme,
}: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: style?.width,
            height: style?.height,
            backgroundColor: style?.backgroundColor ?? appTheme.navigation.defaultHeader,
        },
        scrollView: {
            height: style?.height,
            position: 'relative',
        },
        scrollViewContainer: {
            height: style?.height,
        },
        tab: {
            margin: guildsTabBarMargin,
            marginBottom: 12,
            height: guildsTabBarHeight,
            width: guildsTabBarWidth,
        },
        border: {
            position: 'absolute',
            left: 0,
            bottom: 0,
            width: guildsTabBarWidth,
        },
    });
}

type Styles = typeof styles;

export default class GuildsTabBarStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};