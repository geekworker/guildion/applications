import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import GuildsTabBarHooks from "./hooks";
import GuildsTabBarStyles from './styles';
import React from 'react';
import { Guild, Guilds } from "@guildion/core";
import { Animated, FlatList } from "react-native";
import { compare } from "@/shared/modules/ObjectCompare";

namespace GuildsTabBarPresenter {
    export type Input = {
        children?: React.ReactNode,
        guilds: Guilds,
        selectedID?: string,
        onPressTab?: (guild: Guild) => void,
        onFocus?: (guild: Guild) => void,
        onPageScrollOffset?: Animated.Value,
        onPageScrollPosition?: Animated.Value,
        loading?: boolean,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildsTabBarStyles,
        appTheme: AppTheme,
        guilds: Guilds,
        selectedID: string,
        onPressTab: (guild: Guild) => void,
        translateX: Animated.Value | Animated.AnimatedInterpolation,
        flatListRef: React.RefObject<FlatList<Guild> | null>,
        loading?: boolean,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    }
    
    export function usePresenter(props: Input): Output {
        const styles = GuildsTabBarHooks.useStyles({ ...props });
        const {
            selectedID,
            setSelectedID,
        } = GuildsTabBarHooks.useSelect({ ...props, initialID: props.selectedID ?? props.guilds.first?.id ?? '' });
        const flatListRef = GuildsTabBarHooks.useFlatListRef();
        const translateX = GuildsTabBarHooks.useScroll(flatListRef, {
            ...props,
            selectedID,
        });
        const offsetTranslateX = GuildsTabBarHooks.useAnimatedBarOffset({ ...props });
        GuildsTabBarHooks.useOnFocused(selectedID, (id: string) => {
            const target = props.guilds.toArray().find(g => g.id == id);
            if (!!props.onFocus && target) {
                props.onFocus(target);
            };
        });
        const mockGuilds = GuildsTabBarHooks.useSkeletonMock();
        const guilds = props.loading ? mockGuilds : props.guilds;
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            selectedID,
            flatListRef,
            translateX: offsetTranslateX ?? translateX,
            onPressTab: (guild: Guild) => {
                if (selectedID == guild.id || !guild.id || guild.id == '') return;
                setSelectedID(guild.id!);
                if (props.onPressTab) props.onPressTab(guild);
            },
            guilds,
        }
    }
}

export default GuildsTabBarPresenter;