import React from "react";
import GuildsTabBarStyles, { guildsTabBarMargin, guildsTabBarWidth } from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Guild, Guilds } from "@guildion/core";
import { Animated, FlatList, useWindowDimensions } from "react-native";
import { useAnimatedValue } from "@/shared/hooks/useAnimatedValue";

namespace GuildsTabBarHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new GuildsTabBarStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useSelect = ({ initialID }: { guilds: Guilds, initialID: string }) => {
        const [selectedID, setSelectedID] = React.useState<string>(initialID);
        React.useEffect(() => {
            selectedID != initialID && setSelectedID(initialID);
        }, [initialID]);
        return {
            selectedID,
            setSelectedID,
        };
    };

    export const useOnFocused = (selectedID: string, callback?: (selectedID: string) => void) => {
        React.useEffect(() => {
            !!callback && callback(selectedID);
        }, [selectedID]);
    }

    export const useFlatListRef = (): React.RefObject<FlatList<Guild> | null> => {
        const flatListRef = React.useRef<FlatList<Guild>>(null)
        return flatListRef;
    }

    export const useScroll = (ref: React.RefObject<FlatList<Guild> | null>, { guilds, selectedID, onScrollEnd }: { guilds: Guilds, selectedID: string, onScrollEnd?: () => void }): Animated.Value => {
        const window = useWindowDimensions();
        const translationX = React.useRef(new Animated.Value(guildsTabBarMargin)).current;
        React.useEffect(() => {
            if (!ref || ref.current == null) return;
            const index = guilds.toArray().findIndex(g => g.id == selectedID);
            if (index > -1) {
                ref.current.scrollToOffset({
                    animated: true,
                    offset: calcScrollOffset(index, window.width)
                });
                Animated.timing(translationX, {
                    toValue: calcBarOffset(index),
                    duration: 200,
                    useNativeDriver: true,
                }).start(onScrollEnd);
            }
        }, [selectedID]);
        return translationX;
    }

    export const useAnimatedBarOffset = ({ guilds, onPageScrollPosition, onPageScrollOffset }: { guilds: Guilds, onPageScrollPosition?: Animated.Value, onPageScrollOffset?: Animated.Value }): Animated.AnimatedInterpolation | undefined => {
        if (!onPageScrollPosition || !onPageScrollOffset || guilds.size < 2) return undefined;
        return Animated.add(onPageScrollPosition, onPageScrollOffset).interpolate({
            inputRange: [...Array(guilds.size)].map((_, i) => i),
            outputRange: [...Array(guilds.size)].map((_, i) => (i * (guildsTabBarMargin * 2 + guildsTabBarWidth) + guildsTabBarMargin)),
        });
    }

    export const useAnimatedScrollOffset = (ref: React.RefObject<FlatList<Guild> | null>, { guilds, onPageScrollPosition, onPageScrollOffset }: { guilds: Guilds, onPageScrollPosition?: Animated.Value, onPageScrollOffset?: Animated.Value }): void => {
        if (!onPageScrollPosition || !onPageScrollOffset || guilds.size < 2) return undefined;
        const window = useWindowDimensions();
        const onPageScrollPositionValue = useAnimatedValue(onPageScrollPosition, { throttle: 0.9 });
        const onPageScrollOffsetValue = useAnimatedValue(onPageScrollOffset, { throttle: 0.9 });
        React.useEffect(() => {
            if (!ref || ref.current == null) return;
            const offset = calcScrollOffset(onPageScrollPositionValue + onPageScrollOffsetValue, window.width);
            ref.current.scrollToOffset({
                animated: true,
                offset,
            });
        },[onPageScrollPositionValue, onPageScrollOffsetValue])
    }

    export const calcScrollOffset = (index: number, width: number): number => {
        const itemOffset = ((guildsTabBarMargin * 2 + guildsTabBarWidth) * index) + guildsTabBarMargin
        return Math.max(itemOffset - (width / 2) + (guildsTabBarWidth / 2), 0)
    }

    export const calcBarOffset = (index: number): number => {
        const itemOffset = ((guildsTabBarMargin * 2 + guildsTabBarWidth) * index)
        return Math.max(itemOffset + guildsTabBarMargin, guildsTabBarMargin)
    }

    export const useSkeletonMock = (): Guilds => {
        const guilds = React.useMemo(() => new Guilds([
            new Guild({ id: '`skeleton1`' }),
            new Guild({ id: '`skeleton2`' }),
            new Guild({ id: '`skeleton3`' }),
            new Guild({ id: '`skeleton4`' }),
            new Guild({ id: '`skeleton5`' }),
            new Guild({ id: '`skeleton6`' }),
        ]), []);
        return guilds;
    }
}

export default GuildsTabBarHooks