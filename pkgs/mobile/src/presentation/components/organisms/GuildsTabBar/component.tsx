import { Guild } from '@guildion/core';
import React from 'react';
import { View, FlatList, ListRenderItemInfo } from 'react-native';
import GuildTab from '../../atoms/GuildTab';
import GuildTabBorder from '../../atoms/GuildTabBorder';
import GuildsTabBarPresenter from './presenter';

const GuildsTabBarComponent = ({ styles, guilds, appTheme, onPressTab, selectedID, flatListRef, translateX, loading }: GuildsTabBarPresenter.Output) => {
    const renderTab = (props: ListRenderItemInfo<Guild>)=> (
        <GuildTab
            style={styles.getStyle('tab')}
            appTheme={appTheme}
            guild={props.item}
            key={props.index}
            onPress={onPressTab}
            isFocused={selectedID == props.item.id}
            loading={loading}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.scrollView}
                ref={flatListRef}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={!loading}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={!loading}
                scrollEnabled={!loading}
                snapToAlignment={'center'}  
                data={guilds.toArray()}
                renderItem={renderTab}
                keyExtractor={(item: Guild, index: number) => item.id! + String(index)}
                ListHeaderComponent={
                    !loading ? (
                        <GuildTabBorder
                            style={styles.getStyle('border')}
                            animationStyle={{
                                transform: [{
                                    translateX
                                }]
                            }}
                        />
                    ) : null
                }
            />
        </View>
    );
};

export default React.memo(GuildsTabBarComponent, GuildsTabBarPresenter.outputAreEqual);