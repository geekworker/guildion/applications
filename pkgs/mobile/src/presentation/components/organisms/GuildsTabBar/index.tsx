import React from 'react';
import GuildsTabBarComponent from './component';
import GuildsTabBarPresenter from './presenter';

const GuildsTabBar = (props: GuildsTabBarPresenter.Input) => {
    const output = GuildsTabBarPresenter.usePresenter(props);
    return <GuildsTabBarComponent {...output} />;
};

export default React.memo(GuildsTabBar, GuildsTabBarPresenter.inputAreEqual);