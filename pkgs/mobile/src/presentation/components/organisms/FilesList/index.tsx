import React from 'react';
import FilesListComponent from './component';
import FilesListPresenter from './presenter';

const FilesList = (props: FilesListPresenter.Input) => {
    const output = FilesListPresenter.usePresenter(props);
    return <FilesListComponent {...output} />;
};

export default React.memo(FilesList, FilesListPresenter.inputAreEqual);