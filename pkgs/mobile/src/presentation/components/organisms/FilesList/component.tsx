import React from 'react';
import FilesListPresenter from './presenter';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import FileCell from '../../molecules/FileCell';
import { File, FileStatus } from '@guildion/core';

const FilesListComponent = ({ styles, appTheme, loadingMore, files, onPress, onPressFolder, onPressMenu, ListFooterComponent, ListHeaderComponent, ListHeaderComponentStyle, ListFooterComponentStyle, flatListRef, onScroll }: FilesListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<File>) => (
        <FileCell
            appTheme={appTheme}
            file={props.item}
            style={styles.getStyle('cell')}
            key={props.index}
            loading={props.item.status == FileStatus.LOADING}
            onPress={onPress}
            onPressFolder={onPressFolder}
            onPressMenu={onPressMenu}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                ref={flatListRef}
                onScroll={onScroll}
                style={styles.scrollView}
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={true}
                scrollEnabled={true}
                keyboardDismissMode="on-drag"
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}
                scrollEventThrottle={16}
                data={files.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: File, index: number) => item.id + String(index)}
                listKey={files.size > 0 ? files.toArray().map((m) => m.id!).reduce((a, b) => a + b) : undefined}
                ListHeaderComponent={ListHeaderComponent}
                ListFooterComponent={ListFooterComponent}
                ListHeaderComponentStyle={ListHeaderComponentStyle}
                ListFooterComponentStyle={ListFooterComponentStyle}
            />
        </View>
    )
};

export default React.memo(FilesListComponent, FilesListPresenter.outputAreEqual);