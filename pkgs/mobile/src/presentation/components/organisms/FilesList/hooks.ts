import React from "react";
import FilesListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type FilesListPresenter from "./presenter";
import { File, Files, FileStatus, SortDirection } from "@guildion/core";
import uuid from "react-native-uuid";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import { FlatList, NativeScrollEvent, NativeSyntheticEvent } from "react-native";

namespace FilesListHooks {
    export const loadingMoreMockCount: number = 2;

    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new FilesListStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useLoadingMoreMock = (props: FilesListPresenter.Input): Files => {
        const [mocks, setMocks] = React.useState<Files>(new Files([]));
        React.useEffect(() => {
            props.loadingMore && mocks.size == 0 ?
            setMocks(new Files(
                Array.from(Array(loadingMoreMockCount).keys()).map((i: number) => 
                    new File({ id: `${uuid.v4()}-${i}`, status: FileStatus.LOADING })
                )
            )) :
            setMocks(new Files([]))
        }, [props.loadingMore]);
        return mocks.size > 0 && props.loadingMore == (props.sortDirection == SortDirection.ASC ? LoadingMoreStatus.DECREMENT : LoadingMoreStatus.INCREMENT) ?
            new Files(props.files.concat(mocks).toArray()) :
            mocks.size > 0 && props.loadingMore == (props.sortDirection == SortDirection.ASC ? LoadingMoreStatus.INCREMENT : LoadingMoreStatus.DECREMENT) ?
            new Files(mocks.concat(props.files).toArray()) :
            props.files;
    }

    export const useState = (props: FilesListPresenter.Input) => {
        const flatListRef = React.useRef<FlatList<File>>(null);
        const onScroll = React.useCallback((event: NativeSyntheticEvent<NativeScrollEvent>) => {
            const contentOffsetY = event.nativeEvent.contentOffset.y;
            const visualHeight = event.nativeEvent.layoutMeasurement.height;
            const contentHeight = event.nativeEvent.contentSize.height;
            if (contentOffsetY + visualHeight > contentHeight && props.onScrollToBottom) {
                props.onScrollToBottom();
            } else if (contentOffsetY < 0 && props.onScrollToTop) {
                props.onScrollToTop();
            }
        }, [props.onScrollToBottom, props.onScrollToTop]);
        return {
            flatListRef,
            onScroll,
        };
    }
}

export default FilesListHooks;