import { storiesOf } from '@storybook/react-native';
import React from 'react';
import FilesList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { number } from '@storybook/addon-knobs';
import { Files, SortDirection } from '@guildion/core';
import uuid from 'react-native-uuid';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const mockFiles = new Files([
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
    Files.getSeed().generateGuildProfile().set('id', `${uuid.v4()}`).setRecord('folder', Files.getSeed().generateGuildProfile()),
]);

storiesOf('FilesList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <FilesList
            style={new Style({
                width: number('width', 375),
            })}
            files={mockFiles}
            sortDirection={SortDirection.ASC}
        />
    ))
    .add('LoadingMore(increment)', () => (
        <FilesList
            style={new Style({
                width: number('width', 375),
            })}
            loadingMore={LoadingMoreStatus.INCREMENT}
            files={mockFiles}
            sortDirection={SortDirection.ASC}
        />
    ))
    .add('LoadingMore(decrement)', () => (
        <FilesList
            style={new Style({
                width: number('width', 375),
            })}
            loadingMore={LoadingMoreStatus.DECREMENT}
            files={mockFiles}
            sortDirection={SortDirection.ASC}
        />
    ))