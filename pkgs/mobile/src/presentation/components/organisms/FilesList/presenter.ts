import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import FilesListHooks from "./hooks";
import FilesListStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Files, SortDirection } from "@guildion/core";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import { FlatList, NativeScrollEvent, NativeSyntheticEvent, ViewStyle } from "react-native";
import Animated from "react-native-reanimated";

namespace FilesListPresenter {
    export type Input = {
        files: Files,
        loadingMore?: LoadingMoreStatus,
        onPress?: (file: File) => void,
        onPressFolder?: (file: File) => void,
        onPressMenu?: (file: File) => void,
        sortDirection: SortDirection,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScrollToTop?: () => void,
        onScrollToBottom?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: FilesListStyles,
        appTheme: AppTheme,
        files: Files,
        loadingMore?: LoadingMoreStatus,
        onPress?: (file: File) => void,
        onPressFolder?: (file: File) => void,
        onPressMenu?: (file: File) => void,
        sortDirection: SortDirection,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        flatListRef: React.RefObject<FlatList<File>>,
        onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = FilesListHooks.useStyles({ ...props });
        const files = FilesListHooks.useLoadingMoreMock(props);
        const {
            flatListRef,
            onScroll,
        } = FilesListHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            files,
            flatListRef,
            onScroll,
        }
    }
}

export default FilesListPresenter;