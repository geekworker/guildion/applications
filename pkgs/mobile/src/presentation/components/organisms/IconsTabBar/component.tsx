import React from 'react';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import IconTab, { IconTabProps } from '../../atoms/IconTab';
import IconsTabBarPresenter from './presenter';

const IconsTabBarComponent = ({ styles, appTheme, data, onPress, activeId }: IconsTabBarPresenter.Output) => {
    const renderMenu = (props: ListRenderItemInfo<IconTabProps>)=> (
        <IconTab
            appTheme={appTheme}
            {...props.item}
            style={styles.getStyle('cell')}
            key={props.index}
            active={activeId ? activeId == props.item.id : false}
            onPress={onPress}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                style={styles.scrollView}
                horizontal
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={false}
                alwaysBounceHorizontal={false}
                scrollEnabled={false}
                keyboardDismissMode="on-drag"
                showsHorizontalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}  
                data={data}
                renderItem={renderMenu}
                keyExtractor={(item: IconTabProps, index: number) => item.id! + String(index)}
            />
        </View>
    );
};

export default React.memo(IconsTabBarComponent, IconsTabBarPresenter.outputAreEqual);