import { storiesOf } from '@storybook/react-native';
import React from 'react';
import IconsTabBar from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import Icon from 'react-native-vector-icons/FontAwesome';

storiesOf('IconsTabBar', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <IconsTabBar
            style={new Style({
                width: 375,
                height: 40,
            })}
            activeId='Playlist'
            data={[
                {
                    title: 'Play next',
                    id: 'Playlist',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
                {
                    title: 'Schedule',
                    id: 'Schedule',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
                {
                    title: 'Detail',
                    id: 'Detail',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
            ]}
        />
    ))
    .add('2Tabs', () => (
        <IconsTabBar
            style={new Style({
                width: 375,
                height: 40,
            })}
            activeId='Playlist'
            data={[
                {
                    title: 'Play next',
                    id: 'Playlist',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
                {
                    title: 'Detail',
                    id: 'Detail',
                    Icon: (props) => <Icon {...props} name={'list'} />,
                },
            ]}
        />
    ))