import React from 'react';
import IconsTabBarStyles from './styles';
import type IconsTabBarPresenter from './presenter';

namespace IconsTabBarHooks {
    export const useStyles = (props: IconsTabBarPresenter.Input) => {
        const styles = React.useMemo(() => new IconsTabBarStyles({ ...props, count: props.data.length }), [
            props.appTheme,
            props.style,
            props.data.length,
        ]);
        return styles;
    }

    export const useState = (props: IconsTabBarPresenter.Input) => {
        const [activeId, setActiveId] = React.useState<string>(props.activeId);
        React.useEffect(() => {
            activeId != props.activeId && setActiveId(props.activeId);
        }, [props.activeId]);

        const onPress = React.useCallback((id: string) => {
            setActiveId(id);
            props.onPress && props.onPress(id);
        }, [activeId, props.onPress]);
        return {
            onPress,
            activeId,
        };
    }
}

export default IconsTabBarHooks;