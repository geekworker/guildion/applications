import React from 'react';
import IconsTabBarComponent from './component';
import IconsTabBarPresenter from './presenter';

const IconsTabBar = (props: IconsTabBarPresenter.Input) => {
    const output = IconsTabBarPresenter.usePresenter(props);
    return <IconsTabBarComponent {...output} />;
};

export default React.memo(IconsTabBar, IconsTabBarPresenter.inputAreEqual);