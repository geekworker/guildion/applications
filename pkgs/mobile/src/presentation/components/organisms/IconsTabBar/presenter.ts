import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import IconsTabBarHooks from './hooks';
import IconsTabBarStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import { IconTabProps } from '../../atoms/IconTab';

namespace IconsTabBarPresenter {
    export type Input = {
        data: Omit<IconTabProps, 'onPress'>[],
        activeId: string,
        onPress?: (id: string) => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: IconsTabBarStyles,
        appTheme: AppTheme,
        data: Omit<IconTabProps, 'onPress'>[],
        activeId?: string,
        onPress: (id: string) => void,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = IconsTabBarHooks.useStyles({ ...props });
        const {
            activeId,
            onPress,
        } = IconsTabBarHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            activeId,
            onPress,
        }
    }
}

export default IconsTabBarPresenter;