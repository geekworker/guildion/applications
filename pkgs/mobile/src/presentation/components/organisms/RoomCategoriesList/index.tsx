import React from 'react';
import RoomCategoriesListComponent from './component';
import RoomCategoriesListPresenter from './presenter';

const RoomCategoriesList = (props: RoomCategoriesListPresenter.Input) => {
    const output = RoomCategoriesListPresenter.usePresenter(props);
    return <RoomCategoriesListComponent {...output} />;
};

export default React.memo(RoomCategoriesList, RoomCategoriesListPresenter.inputAreEqual);