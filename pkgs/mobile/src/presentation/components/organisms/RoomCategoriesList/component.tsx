import { Room, RoomCategory } from '@guildion/core';
import React from 'react';
import { Animated, ListRenderItemInfo } from 'react-native';
import RoomCategoryCell from '../../molecules/RoomCategoryCell';
import RoomCategoriesListPresenter from './presenter';

const RoomCategoriesListComponent = ({ styles, appTheme, loadingMore, animatedStyle, categories, onPressRoom, loading, ListFooterComponent, ListHeaderComponent, ListHeaderComponentStyle, ListFooterComponentStyle, onScroll }: RoomCategoriesListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<RoomCategory>)=> (
        <RoomCategoryCell
            style={styles.getStyle(props.item.loading || loading ? 'loadingCell' : 'cell')}
            appTheme={appTheme}
            category={props.item}
            key={props.index}
            onPress={onPressRoom}
            loading={props.item.loading || loading}
        />
    );
    return (
        <Animated.FlatList
            style={styles.container}
            directionalLockEnabled
            automaticallyAdjustContentInsets={false}
            alwaysBounceVertical={false}
            alwaysBounceHorizontal={false}
            bounces={!loading}
            scrollEnabled={!loading}
            keyboardDismissMode="on-drag"
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            snapToAlignment={'center'}  
            data={categories.toArray()}
            renderItem={renderCell}
            keyExtractor={(item: RoomCategory, index: number) => item.id! + String(index)}
            ListHeaderComponent={ListHeaderComponent}
            ListFooterComponent={ListFooterComponent}
            ListHeaderComponentStyle={ListHeaderComponentStyle}
            ListFooterComponentStyle={ListFooterComponentStyle}
            onScroll={onScroll}
            scrollEventThrottle={16}
        />
    );
};

export default React.memo(RoomCategoriesListComponent, RoomCategoriesListPresenter.outputAreEqual);