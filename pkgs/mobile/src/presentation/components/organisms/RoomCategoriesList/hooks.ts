import React from 'react';
import RoomCategoriesListStyles from './styles';
import type RoomCategoriesListPresenter from './presenter';
import { RoomCategories, RoomCategory } from '@guildion/core';
import uuid from 'react-native-uuid';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

namespace RoomCategoriesListHooks {
    export const useStyles = (props: RoomCategoriesListPresenter.Input) => {
        const styles = React.useMemo(() => new RoomCategoriesListStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomCategoriesListPresenter.Input) => {
        return {};
    }

    export const loadingMoreMockCount: number = 2;
    export const useLoadingMoreMock = (props: RoomCategoriesListPresenter.Input): RoomCategories => {
        const [mocks, setMocks] = React.useState<RoomCategories>(new RoomCategories([]));
        React.useEffect(() => {
            props.loadingMore && mocks.size == 0 ?
            setMocks(new RoomCategories(
                Array.from(Array(loadingMoreMockCount).keys()).map((i: number) => 
                    new RoomCategory({ id: `${uuid.v4()}-${i}`, loading: true })
                )
            )) :
            setMocks(new RoomCategories([]))
        }, [props.loadingMore]);
        return mocks.size > 0 && props.loadingMore == LoadingMoreStatus.INCREMENT ?
            props.categories.concat(mocks) :
            mocks.size > 0 && props.loadingMore == LoadingMoreStatus.DECREMENT ?
            mocks.concat(props.categories) :
            props.categories;
    }

    export const useSkeletonMock = (): RoomCategories => {
        const rooms = React.useMemo(() => new RoomCategories([
            new RoomCategory({ id: '`skeleton1`' }),
            new RoomCategory({ id: '`skeleton2`' }),
            new RoomCategory({ id: '`skeleton3`' }),
            new RoomCategory({ id: '`skeleton4`' }),
            new RoomCategory({ id: '`skeleton5`' }),
            new RoomCategory({ id: '`skeleton6`' }),
        ]), []);
        return rooms;
    }
}

export default RoomCategoriesListHooks;