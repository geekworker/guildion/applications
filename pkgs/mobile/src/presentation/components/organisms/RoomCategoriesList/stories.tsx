import { storiesOf } from '@storybook/react-native';
import React from 'react';
import RoomCategoriesList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Files, Room, RoomActivity, RoomCategories, RoomCategory, Rooms } from '@guildion/core';
import { number } from '@storybook/addon-knobs';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

storiesOf('RoomCategoriesList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <RoomCategoriesList
            style={new Style({
                width: number('width', 375),
            })}
            categories={mockCategories}
        />
    ))
    .add('Loading', () => (
        <RoomCategoriesList
            style={new Style({
                width: number('width', 375),
            })}
            loading
            categories={mockCategories}
        />
    ))
    .add('LoadingMore(increment)', () => (
        <RoomCategoriesList
            style={new Style({
                width: number('width', 375),
            })}
            loadingMore={LoadingMoreStatus.INCREMENT}
            categories={mockCategories}
        />
    ))
    .add('LoadingMore(decrement)', () => (
        <RoomCategoriesList
            style={new Style({
                width: number('width', 375),
            })}
            loadingMore={LoadingMoreStatus.DECREMENT}
            categories={mockCategories}
        />
    ))


const mockCategories = new RoomCategories([
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
    new RoomCategory({
        name: 'test',
    }, {
        rooms: new Rooms([
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
            new Room({
                id: 'roomDisplayName1',
                displayName: 'Test Room1',
            }, {
                activity: new RoomActivity({
                    postsCount: 12,
                    createdAt: new Date().toString(),
                }),
                profile: Files.getSeed().generateGuildProfile(),
            }),
        ]),
    }),
]);