import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomCategoriesListHooks from './hooks';
import RoomCategoriesListStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Room, RoomCategories } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { Animated, NativeScrollEvent, NativeSyntheticEvent, ViewStyle } from 'react-native';

namespace RoomCategoriesListPresenter {
    export type Input = {
        categories: RoomCategories,
        onPressRoom?: (room: Room) => void,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomCategoriesListStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        categories: RoomCategories,
        onPressRoom?: (room: Room) => void,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomCategoriesListHooks.useStyles({ ...props });
        const _categories = RoomCategoriesListHooks.useLoadingMoreMock(props);
        const mockCategories = RoomCategoriesListHooks.useSkeletonMock();
        const categories = props.loading ? mockCategories : _categories;
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            categories,
        }
    }
}

export default RoomCategoriesListPresenter;