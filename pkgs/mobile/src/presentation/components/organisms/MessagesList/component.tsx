import React from 'react';
import MessagesListPresenter from './presenter';
import { Animated, ListRenderItemInfo, View } from 'react-native';
import MessageCell from '../../molecules/MessageCell';
import { Message, MessageStatus } from '@guildion/core';
import { CURRENT_CONFIG } from '@/shared/constants/Config';

const MessagesListComponent = ({ styles, appTheme, animatedStyle, loadingMore, decrementable, flatListRef, editingMessageId, messages, onPressSender, onLongPressMessage, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, onScroll }: MessagesListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Message>) => (
        <MessageCell
            appTheme={appTheme}
            message={props.item}
            style={styles.getStyle('cell')}
            key={props.item.id ?? props.index}
            loading={props.item.status == MessageStatus.LOADING}
            isFirstInDate={
                props.index == messages.size - 1 && props.item.status !== MessageStatus.LOADING ?
                !decrementable :
                messages.get(messages.size - 1 - (props.index + 1)) && props.item.checkTomorrowDate(messages.get(messages.size - 1 - (props.index + 1))!, CURRENT_CONFIG.DEFAULT_TIMEZONE)}
            isEditing={!!props.item.id && !!editingMessageId && props.item.id == editingMessageId}
            onPressSender={onPressSender}
            onLongPressMessage={onLongPressMessage}
            onPressFile={onPressFile}
            onPressURL={onPressURL}
            onPressEmail={onPressEmail}
            onPressPhoneNumber={onPressPhoneNumber}
            onPressMentionMember={onPressMentionMember}
            onPressMentionRoom={onPressMentionRoom}
        />
    );
    return (
        <View style={styles.container}>
            {messages.size > 0 && (
                <Animated.FlatList
                    contentContainerStyle={{ flexGrow: 1, justifyContent: 'flex-start', paddingTop: styles.scrollView.paddingBottom }}
                    style={{ ...styles.scrollView, paddingBottom: 0, ...animatedStyle?.toStyleObject() }}
                    ref={flatListRef}
                    directionalLockEnabled
                    automaticallyAdjustContentInsets={false}
                    alwaysBounceVertical={true}
                    scrollEnabled={true}
                    keyboardDismissMode="on-drag"
                    showsVerticalScrollIndicator={false}
                    nestedScrollEnabled={true}
                    scrollEventThrottle={100}
                    data={messages.toArray().reverse()}
                    renderItem={renderCell}
                    keyExtractor={(item: Message, index: number) => item.id + String(index)}
                    listKey={messages.toArray().map((m) => m.id!).reduce((a, b) => a + b)}
                    onScroll={onScroll}
                    inverted
                />
            )}
        </View>
    )
};

export default React.memo(MessagesListComponent, MessagesListPresenter.outputAreEqual);