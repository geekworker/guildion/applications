import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import MessagesListHooks from "./hooks";
import MessagesListStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Message, Messages } from "@guildion/core";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";
import { Animated, FlatList, NativeScrollEvent, NativeSyntheticEvent } from "react-native";

namespace MessagesListPresenter {
    export type Input = {
        messages: Messages,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        editingMessageId?: string,
        decrementable?: boolean,
        incrementable?: boolean,
        onPressSender?: (sender: Member) => void,
        onLongPressMessage?: (message: Message) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        onScrollToTop?: () => void,
        onScrollToBottom?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: MessagesListStyles,
        appTheme: AppTheme,
        animatedStyle?: AnimatedStyle,
        messages: Messages,
        loadingMore?: LoadingMoreStatus,
        editingMessageId?: string,
        decrementable?: boolean,
        incrementable?: boolean,
        onPressSender?: (sender: Member) => void,
        onLongPressMessage?: (message: Message) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        flatListRef: React.RefObject<FlatList<Message>>,
        onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MessagesListHooks.useStyles({ ...props });
        const messages = MessagesListHooks.useLoadingMoreMock(props);
        const {
            flatListRef,
            onScroll,
        } = MessagesListHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            messages,
            flatListRef,
            onScroll,
        }
    }
}

export default MessagesListPresenter;