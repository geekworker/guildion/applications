import React from 'react';
import MessagesListComponent from './component';
import MessagesListPresenter from './presenter';

const MessagesList = (props: MessagesListPresenter.Input) => {
    const output = MessagesListPresenter.usePresenter(props);
    return <MessagesListComponent {...output} />;
};

export default React.memo(MessagesList, MessagesListPresenter.inputAreEqual);