import React from "react";
import MessagesListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type MessagesListPresenter from "./presenter";
import { Message, Messages, MessageStatus } from "@guildion/core";
import uuid from "react-native-uuid";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import { FlatList, NativeScrollEvent, NativeSyntheticEvent } from "react-native";

namespace MessagesListHooks {
    export const loadingMoreMockCount: number = 2;

    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new MessagesListStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useLoadingMoreMock = (props: MessagesListPresenter.Input): Messages => {
        const [mocks, setMocks] = React.useState<Messages>(new Messages([]));
        React.useEffect(() => {
            props.loadingMore && mocks.size == 0 ?
            setMocks(new Messages(
                Array.from(Array(loadingMoreMockCount).keys()).map((i: number) => 
                    new Message({ id: `${uuid.v4()}-${i}`, status: MessageStatus.LOADING })
                )
            )) :
            setMocks(new Messages([]))
        }, [props.loadingMore]);
        return mocks.size > 0 && props.loadingMore == LoadingMoreStatus.INCREMENT ?
        props.messages.concat(mocks) :
        mocks.size > 0 && props.loadingMore == LoadingMoreStatus.DECREMENT ?
        mocks.concat(props.messages) :
        props.messages;
    }

    export const useState = (props: MessagesListPresenter.Input) => {
        const flatListRef = React.useRef<FlatList<Message>>(null);
        const onScroll = React.useCallback((event: NativeSyntheticEvent<NativeScrollEvent>) => {
            const contentOffsetY = event.nativeEvent.contentOffset.y;
            const visualHeight = event.nativeEvent.layoutMeasurement.height;
            const contentHeight = event.nativeEvent.contentSize.height;
            if (contentOffsetY + visualHeight > contentHeight && props.decrementable && props.onScrollToTop) {
                props.onScrollToTop();
            } else if (contentOffsetY < 0 && props.incrementable && props.onScrollToBottom) {
                props.onScrollToBottom();
            }
        }, [props.incrementable, props.decrementable]);
        return {
            flatListRef,
            onScroll,
        };
    }
}

export default MessagesListHooks;