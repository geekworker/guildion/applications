import React from "react";
import PostsListStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import type PostsListPresenter from "./presenter";
import { Post, Posts, PostStatus, SortDirection } from "@guildion/core";
import uuid from "react-native-uuid";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import { FlatList, NativeScrollEvent, NativeSyntheticEvent } from "react-native";

namespace PostsListHooks {
    export const loadingMoreMockCount: number = 2;

    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new PostsListStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useLoadingMoreMock = (props: PostsListPresenter.Input): Posts => {
        const [mocks, setMocks] = React.useState<Posts>(new Posts([]));
        React.useEffect(() => {
            props.loadingMore && mocks.size == 0 ?
            setMocks(new Posts(
                Array.from(Array(loadingMoreMockCount).keys()).map((i: number) => 
                    new Post({ id: `${uuid.v4()}-${i}`, status: PostStatus.LOADING })
                )
            )) :
            setMocks(new Posts([]))
        }, [props.loadingMore]);
        return mocks.size > 0 && props.loadingMore == (props.sortDirection == SortDirection.ASC ? LoadingMoreStatus.DECREMENT : LoadingMoreStatus.INCREMENT) ?
            props.posts.concat(mocks) :
            mocks.size > 0 && props.loadingMore == (props.sortDirection == SortDirection.ASC ? LoadingMoreStatus.INCREMENT : LoadingMoreStatus.DECREMENT) ?
            mocks.concat(props.posts) :
            props.posts;
    }

    export const useState = (props: PostsListPresenter.Input) => {
        const flatListRef = React.useRef<FlatList<Post>>(null);
        const onScroll = React.useCallback((event: NativeSyntheticEvent<NativeScrollEvent>) => {
            const contentOffsetY = event.nativeEvent.contentOffset.y;
            const visualHeight = event.nativeEvent.layoutMeasurement.height;
            const contentHeight = event.nativeEvent.contentSize.height;
            if (contentOffsetY + visualHeight > contentHeight && props.onScrollToBottom) {
                props.onScrollToBottom();
            } else if (contentOffsetY < 0 && props.onScrollToTop) {
                props.onScrollToTop();
            }
        }, [props.onScrollToBottom, props.onScrollToTop]);
        return {
            flatListRef,
            onScroll,
        };
    }
}

export default PostsListHooks;