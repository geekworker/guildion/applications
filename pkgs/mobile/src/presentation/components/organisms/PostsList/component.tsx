import React from 'react';
import PostsListPresenter from './presenter';
import { FlatList, ListRenderItemInfo, View } from 'react-native';
import PostCell from '../../molecules/PostCell';
import { Post, PostStatus } from '@guildion/core';

const PostsListComponent = ({ styles, appTheme, loadingMore, posts, onPressSender, onPressSeeMore, onPressFile, onPressURL, onPressEmail, onPressPhoneNumber, onPressMentionMember, onPressMentionRoom, ListFooterComponent, ListHeaderComponent, ListHeaderComponentStyle, ListFooterComponentStyle, onScroll, flatListRef }: PostsListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Post>) => (
        <PostCell
            appTheme={appTheme}
            post={props.item}
            style={styles.getStyle('cell')}
            key={props.index}
            loading={props.item.status == PostStatus.LOADING}
            onPressSender={onPressSender}
            onPressSeeMore={onPressSeeMore}
            onPressFile={onPressFile}
            onPressURL={onPressURL}
            onPressEmail={onPressEmail}
            onPressPhoneNumber={onPressPhoneNumber}
            onPressMentionMember={onPressMentionMember}
            onPressMentionRoom={onPressMentionRoom}
        />
    );
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                style={styles.scrollView}
                ref={flatListRef}
                onScroll={onScroll}
                directionalLockEnabled
                automaticallyAdjustContentInsets={false}
                alwaysBounceVertical={true}
                scrollEnabled={true}
                keyboardDismissMode="on-drag"
                showsVerticalScrollIndicator={false}
                nestedScrollEnabled={true}
                snapToAlignment={'center'}
                scrollEventThrottle={16}
                data={posts.toArray()}
                renderItem={renderCell}
                keyExtractor={(item: Post, index: number) => item.id + String(index)}
                listKey={posts.size > 0 ? posts.toArray().map((m) => m.id!).reduce((a, b) => a + b) : undefined}
                ListHeaderComponent={ListHeaderComponent}
                ListFooterComponent={ListFooterComponent}
                ListHeaderComponentStyle={ListHeaderComponentStyle}
                ListFooterComponentStyle={ListFooterComponentStyle}
            />
        </View>
    )
};

export default React.memo(PostsListComponent, PostsListPresenter.outputAreEqual);