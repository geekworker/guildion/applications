import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import PostsListHooks from "./hooks";
import PostsListStyles from './styles';
import { compare } from "@/shared/modules/ObjectCompare";
import { File, Member, Post, Posts, SortDirection } from "@guildion/core";
import { LoadingMoreStatus } from "@/shared/interfaces/LoadingMoreStatus";
import { ViewStyle, FlatList, NativeSyntheticEvent, NativeScrollEvent, Animated } from "react-native";

namespace PostsListPresenter {
    export type Input = {
        posts: Posts,
        loadingMore?: LoadingMoreStatus,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        sortDirection: SortDirection,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScrollToTop?: () => void,
        onScrollToBottom?: () => void,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: PostsListStyles,
        appTheme: AppTheme,
        posts: Posts,
        loadingMore?: LoadingMoreStatus,
        onPressSender?: (sender: Member) => void,
        onPressSeeMore?: (post: Post) => void,
        onPressReaction?: (post: Post) => void,
        onPressComment?: (post: Post) => void,
        onPressFile?: (file: File) => void,
        onPressURL?: (urlstring: string) => void,
        onPressEmail?: (email: string) => void,
        onPressPhoneNumber?: (phoneNumber: string) => void,
        onPressMentionMember?: (id: string) => void,
        onPressMentionRoom?: (id: string) => void,
        sortDirection: SortDirection,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        flatListRef: React.RefObject<FlatList<Post>>,
        onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = PostsListHooks.useStyles({ ...props });
        const posts = PostsListHooks.useLoadingMoreMock(props);
        const {
            flatListRef,
            onScroll,
        } = PostsListHooks.useState(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            posts,
            flatListRef,
            onScroll,
        }
    }
}

export default PostsListPresenter;