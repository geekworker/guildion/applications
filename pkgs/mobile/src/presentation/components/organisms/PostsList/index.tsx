import React from 'react';
import PostsListComponent from './component';
import PostsListPresenter from './presenter';

const PostsList = (props: PostsListPresenter.Input) => {
    const output = PostsListPresenter.usePresenter(props);
    return <PostsListComponent {...output} />;
};

export default React.memo(PostsList, PostsListPresenter.inputAreEqual);