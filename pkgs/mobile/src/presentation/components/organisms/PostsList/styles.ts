import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: Number(style?.width ?? 0),
            height: undefined,
            minHeight: style?.height,
            position: 'relative',
            backgroundColor: style?.backgroundColor ?? appTheme.background.root,
        },
        scrollView: {
            width: style?.width,
            position: 'relative',
        },
        cell: {
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
            width: style?.width,
            marginBottom: 8,
        },
    })
};

type Styles = typeof styles;

export default class PostsListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};