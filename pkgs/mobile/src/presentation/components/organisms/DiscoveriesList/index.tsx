import React from 'react';
import DiscoveriesListComponent from './component';
import DiscoveriesListPresenter from './presenter';

const DiscoveriesList = (props: DiscoveriesListPresenter.Input) => {
    const output = DiscoveriesListPresenter.usePresenter(props);
    return <DiscoveriesListComponent {...output} />;
};

export default React.memo(DiscoveriesList, DiscoveriesListPresenter.inputAreEqual);