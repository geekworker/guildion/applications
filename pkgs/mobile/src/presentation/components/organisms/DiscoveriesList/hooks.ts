import React from 'react';
import DiscoveriesListStyles from './styles';
import type DiscoveriesListPresenter from './presenter';
import { Discoveries, Discovery, DiscoveryStatus } from '@guildion/core';
import uuid from 'react-native-uuid';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

namespace DiscoveriesListHooks {
    export const useStyles = (props: DiscoveriesListPresenter.Input) => {
        const styles = React.useMemo(() => new DiscoveriesListStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: DiscoveriesListPresenter.Input) => {
        return {};
    }

    export const loadingMoreMockCount: number = 2;
    export const useLoadingMoreMock = (props: DiscoveriesListPresenter.Input): Discoveries => {
        const [mocks, setMocks] = React.useState<Discoveries>(new Discoveries([]));
        React.useEffect(() => {
            props.loadingMore && mocks.size == 0 ?
            setMocks(new Discoveries(
                Array.from(Array(loadingMoreMockCount).keys()).map((i: number) => 
                    new Discovery({ id: `${uuid.v4()}-${i}`, status: DiscoveryStatus.LOADING })
                )
            )) :
            setMocks(new Discoveries([]))
        }, [props.loadingMore]);
        return mocks.size > 0 && props.loadingMore == LoadingMoreStatus.INCREMENT ?
            props.discoveries.concat(mocks) :
            mocks.size > 0 && props.loadingMore == LoadingMoreStatus.DECREMENT ?
            mocks.concat(props.discoveries) :
            props.discoveries;
    }

    export const useSkeletonMock = (): Discoveries => {
        const rooms = React.useMemo(() => new Discoveries([
            new Discovery({ id: '`skeleton1`' }),
            new Discovery({ id: '`skeleton2`' }),
            new Discovery({ id: '`skeleton3`' }),
            new Discovery({ id: '`skeleton4`' }),
            new Discovery({ id: '`skeleton5`' }),
            new Discovery({ id: '`skeleton6`' }),
        ]), []);
        return rooms;
    }
}

export default DiscoveriesListHooks;