import { storiesOf } from '@storybook/react-native';
import React from 'react';
import DiscoveriesList from '.';
import CenterView from '@/presentation/components/atoms/CenterView';
import StoreProvider from '@/infrastructure/StoreProvider';
import StorybookNavigationContainer from '@/presentation/components/navigators/StorybookNavigationContainer';
import RootProvider from '@/infrastructure/Root/provider';
import { Style } from '@/shared/interfaces/Style';
import { Discoveries, Discovery, Files } from '@guildion/core';

storiesOf('DiscoveriesList', module)
    .addDecorator((getStory) => (
        <StoreProvider>
            <RootProvider>
                <StorybookNavigationContainer>
                    <CenterView>
                        {getStory()}
                    </CenterView>
                </StorybookNavigationContainer>
            </RootProvider>
        </StoreProvider>
    ))
    .add('Default', () => (
        <DiscoveriesList
            style={new Style({
                width: 380,
            })}
            discoveries={mockDiscoveries}
        />
    ))
    .add('Loading', () => (
        <DiscoveriesList
            style={new Style({
                width: 380,
            })}
            discoveries={mockDiscoveries}
            loading
        />
    ))


export const mockDiscoveries = new Discoveries([
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
    new Discovery({
        enTitle: 'test',
        enDescription: 'description'
    }, {
        enThumbnail: Files.getSeed().generateGuildProfile(),
    }),
])