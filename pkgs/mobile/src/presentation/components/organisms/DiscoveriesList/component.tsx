import { Discovery, DiscoveryStatus } from '@guildion/core';
import React from 'react';
import { Animated, ListRenderItemInfo, View } from 'react-native';
import DiscoveryCell from '../../molecules/DiscoveryCell';
import DiscoveriesListPresenter from './presenter';

const DiscoveriesListComponent = ({ styles, appTheme, animatedStyle, loadingMore, discoveries, onPress, loading, ListFooterComponent, ListHeaderComponent, ListHeaderComponentStyle, ListFooterComponentStyle, onScroll, stickyHeaderIndices, scrollInsets }: DiscoveriesListPresenter.Output) => {
    const renderCell = (props: ListRenderItemInfo<Discovery>)=> (
        <View
            style={styles.cellContainer}
            key={props.index}
        >
            <DiscoveryCell
                style={styles.getStyle(props.item.status == DiscoveryStatus.LOADING || loading ? 'loadingCell' : 'cell')}
                appTheme={appTheme}
                discovery={props.item}
                onPress={onPress}
                loading={props.item.status == DiscoveryStatus.LOADING || loading}
            />
        </View>
    );
    return (
        <Animated.FlatList
            style={styles.container}
            contentInset={scrollInsets}
            contentOffset={scrollInsets?.top ? { y: scrollInsets.top, x: 0 } : undefined}
            directionalLockEnabled
            automaticallyAdjustContentInsets={false}
            alwaysBounceVertical={false}
            alwaysBounceHorizontal={false}
            bounces={!loading}
            scrollEnabled={!loading}
            keyboardDismissMode="on-drag"
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            snapToAlignment={'center'}  
            data={discoveries.toArray()}
            renderItem={renderCell}
            keyExtractor={(item: Discovery, index: number) => item.id! + String(index)}
            ListHeaderComponent={ListHeaderComponent}
            ListFooterComponent={ListFooterComponent}
            ListHeaderComponentStyle={ListHeaderComponentStyle}
            ListFooterComponentStyle={ListFooterComponentStyle}
            stickyHeaderIndices={stickyHeaderIndices}
            onScroll={onScroll}
            scrollEventThrottle={16}
        />
    );
};

export default React.memo(DiscoveriesListComponent, DiscoveriesListPresenter.outputAreEqual);