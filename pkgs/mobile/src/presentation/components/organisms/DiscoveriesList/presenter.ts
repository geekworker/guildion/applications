import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import DiscoveriesListHooks from './hooks';
import DiscoveriesListStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { Discoveries, Discovery } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { Animated, Insets, NativeScrollEvent, NativeSyntheticEvent, ViewStyle } from 'react-native';

namespace DiscoveriesListPresenter {
    export type Input = {
        discoveries: Discoveries,
        onPress?: (discovery: Discovery) => void,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        stickyHeaderIndices?: number[],
        scrollInsets?: Insets,
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: DiscoveriesListStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        discoveries: Discoveries,
        onPress?: (discovery: Discovery) => void,
        loading?: boolean,
        loadingMore?: LoadingMoreStatus,
        ListHeaderComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListFooterComponent?: React.ComponentType<any> | React.ReactElement | null,
        ListHeaderComponentStyle?: ViewStyle | null,
        ListFooterComponentStyle?: ViewStyle | null,
        onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void | ReturnType<typeof Animated.event>,
        stickyHeaderIndices?: number[],
        scrollInsets?: Insets,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = DiscoveriesListHooks.useStyles({ ...props });
        const _discoveries = DiscoveriesListHooks.useLoadingMoreMock(props);
        const mockDiscoveries = DiscoveriesListHooks.useSkeletonMock();
        const discoveries = props.loading ? mockDiscoveries : _discoveries;
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            discoveries,
        }
    }
}

export default DiscoveriesListPresenter;