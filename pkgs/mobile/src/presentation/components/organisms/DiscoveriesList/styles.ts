import { fallbackAppTheme } from '@/shared/constants/AppTheme';
import { Style } from '@/shared/interfaces/Style';
import StyleProps from '@/shared/interfaces/StyleProps';
import { StylesImpl, StylesKey } from '@/shared/interfaces/Styles';
import { Record } from 'immutable';
import { StyleSheet } from 'react-native';

type Props = {
} & StyleProps;

const styles = ({ appTheme, style }: Props) => {
    appTheme ||= fallbackAppTheme;
    const width = style?.getAsNumber('width') || 0;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width,
            backgroundColor: appTheme.common.transparent,
        },
        scrollView: {
            width,
            position: 'relative',
        },
        cellContainer: {
            backgroundColor: style?.backgroundColor ?? appTheme.common.transparent,
        },
        cell: {
            width: width - 24,
            height: 240,
            marginTop: 8,
            marginBottom: 8,
            marginLeft: 12,
            marginRight: 12,
        },
        loadingCell: {
            width: width - 24,
            height: 240,
            marginTop: 8,
            marginBottom: 8,
            marginLeft: 12,
            marginRight: 12,
        },
    });
};

type Styles = typeof styles;

export default class DiscoveriesListStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: Props) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};