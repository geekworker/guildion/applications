import React from "react";
import WelcomeNavigatorStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Account } from "@guildion/core";
import type WelcomeNavigatorPresenter from "./presenter";
import { RootNavigatorRoutes } from "../RootNavigator/constants";

namespace WelcomeNavigatorHooks {
    export const useStyles = (props: StyleProps): WelcomeNavigatorStyles => {
        const styles = React.useMemo(() => new WelcomeNavigatorStyles(props), [
            props,
        ]);
        return styles;
    }

    export const useAccountObserve = ({ currentAccount, navigation, isLaunched }: { currentAccount?: Account, navigation: WelcomeNavigatorPresenter.Input['navigation'], isLaunched: boolean }) => {
        React.useEffect(() => {
            isLaunched && !!currentAccount && !!navigation.isFocused && navigation.push(RootNavigatorRoutes.HomeTabsNavigator, {});
        }, [currentAccount, navigation, isLaunched]);
    }
}

export default WelcomeNavigatorHooks