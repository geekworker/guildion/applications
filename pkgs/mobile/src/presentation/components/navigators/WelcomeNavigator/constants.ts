import WelcomePresenter from '@/presentation/components/pages/Welcome/presenter';
import AccountNewPresenter from '@/presentation/components/pages/AccountNew/presenter';
import AccountAuthenticatePresenter from '@/presentation/components/pages/AccountAuthenticate/presenter';
import AccountVerifyPresenter from '@/presentation/components/pages/AccountVerify/presenter';
import AccountLoadingPresenter from '@/presentation/components/pages/AccountLoading/presenter';

export const WelcomeNavigatorRoutes = {
    Welcome: 'Welcome',
    AccountAuthenticate: 'AccountAuthenticate',
    AccountNew: 'AccountNew',
    AccountVerify: 'AccountVerify',
    AccountLoading: 'AccountLoading',
} as const;

export type WelcomeNavigatorRoutes = typeof WelcomeNavigatorRoutes[keyof typeof WelcomeNavigatorRoutes];

export type WelcomeNavigatorStackParams = {
    [WelcomeNavigatorRoutes.Welcome]: WelcomePresenter.StackParams,
    [WelcomeNavigatorRoutes.AccountNew]: AccountNewPresenter.StackParams,
    [WelcomeNavigatorRoutes.AccountAuthenticate]: AccountAuthenticatePresenter.StackParams,
    [WelcomeNavigatorRoutes.AccountVerify]: AccountVerifyPresenter.StackParams,
    [WelcomeNavigatorRoutes.AccountLoading]: AccountLoadingPresenter.StackParams,
};