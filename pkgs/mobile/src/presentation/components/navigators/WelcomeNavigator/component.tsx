import React from 'react';
import WelcomeNavigatorPresenter from './presenter';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { WelcomeNavigatorRoutes } from './constants';
import Welcome from '@/presentation/components/pages/Welcome';
import AccountNew from '@/presentation/components/pages/AccountNew';
import AccountAuthenticate from '@/presentation/components/pages/AccountAuthenticate';
import BackNavigationButton from '@/presentation/components/atoms/BackNavigationButton';
import { localizer } from '@/shared/constants/Localizer';
import AccountVerify from '@/presentation/components/pages/AccountVerify';
import AccountLoading from '@/presentation/components/pages/AccountLoading';

const Stack = createStackNavigator();

const WelcomeNavigatorComponent: React.FC<WelcomeNavigatorPresenter.Output> = ({ styles, appTheme }) => {
    return (
        <Stack.Navigator
            initialRouteName={WelcomeNavigatorRoutes.Welcome}
            mode="modal"
            screenOptions={{
                headerStyle: styles.headerStyle,
                headerTitleStyle: styles.headerTitleStyle,
                headerTitleAlign: 'center',
            }}
        >
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={WelcomeNavigatorRoutes.Welcome}
            >
                {(props) => <Welcome {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: true,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                    headerLeft: (props) => <BackNavigationButton {...props} label={localizer.dictionary.g.back} />,
                    title: localizer.dictionary.account.new.title,
                }}
                name={WelcomeNavigatorRoutes.AccountNew}
            >
                {(props) => <AccountNew {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: true,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                    headerLeft: (props) => <BackNavigationButton {...props} label={localizer.dictionary.g.back} />,
                    title: localizer.dictionary.account.authentication.title,
                }}
                name={WelcomeNavigatorRoutes.AccountAuthenticate}
            >
                {(props) => <AccountAuthenticate {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: true,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                    headerLeft: (props) => <BackNavigationButton {...props} label={localizer.dictionary.g.back} />,
                    title: localizer.dictionary.account.verify.title,
                }}
                name={WelcomeNavigatorRoutes.AccountVerify}
            >
                {(props) => <AccountVerify {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={WelcomeNavigatorRoutes.AccountLoading}
            >
                {(props) => <AccountLoading {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(WelcomeNavigatorComponent, WelcomeNavigatorPresenter.outputAreEqual);