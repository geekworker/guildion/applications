import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { MontserratFont } from "@/shared/constants/Font";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        headerStyle: {
            backgroundColor: appTheme.navigation.defaultHeader,
            elevation: 0,
            shadowOpacity: 0,
        },
        headerTitleStyle: {
            color: appTheme.navigation.defaultText,
            fontFamily: MontserratFont.Bold,
        },
        screen: {
            ...style?.toStyleObject(),
        },
    });
}

type Styles = typeof styles;

export default class WelcomeNavigatorStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};