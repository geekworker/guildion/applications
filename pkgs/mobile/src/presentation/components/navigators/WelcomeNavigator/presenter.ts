import WelcomeNavigatorHooks from "./hooks";
import WelcomeNavigatorStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { Account } from "@guildion/core";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RootNavigatorStackParams } from "../RootNavigator/constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace WelcomeNavigatorPresenter {
    export type StackParams = {
    };

    export type StateProps = {
        currentAccount?: Account,
        isLaunched: boolean,
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<RootNavigatorStackParams, 'WelcomeNavigator'> & Partial<StyleProps>;
    
    export type Output = {
        styles: WelcomeNavigatorStyles,
        appTheme: AppTheme,
    };

    export type MergedProps = StateProps & DispatchProps & Input;
    
    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            currentAccount: state.account.currentAccount,
            isLaunched: state.app.isLaunched,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.currentAccount == next.account.currentAccount && 
            prev.app.isLaunched == next.app.isLaunched
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = WelcomeNavigatorHooks.useStyles(props);
        WelcomeNavigatorHooks.useAccountObserve(props);
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default WelcomeNavigatorPresenter;