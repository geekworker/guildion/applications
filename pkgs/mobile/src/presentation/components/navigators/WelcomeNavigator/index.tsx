import React from 'react';
import WelcomeNavigatorPresenter from './presenter';
import WelcomeNavigatorComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<WelcomeNavigatorPresenter.MergedProps> = (props) => {
    const output = WelcomeNavigatorPresenter.usePresenter(props);
    return (<WelcomeNavigatorComponent { ...output }/>);
};

const WelcomeNavigator = connect<WelcomeNavigatorPresenter.StateProps, WelcomeNavigatorPresenter.DispatchProps, WelcomeNavigatorPresenter.Input, WelcomeNavigatorPresenter.MergedProps, RootState>(
    WelcomeNavigatorPresenter.mapStateToProps,
    WelcomeNavigatorPresenter.mapDispatchToProps,
    WelcomeNavigatorPresenter.mergeProps,
    WelcomeNavigatorPresenter.connectOptions,
)(Container);

export default WelcomeNavigator;