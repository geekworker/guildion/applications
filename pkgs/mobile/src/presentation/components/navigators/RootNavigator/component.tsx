import React from 'react';
import RootNavigatorPresenter from './presenter';
import HomeTabsNavigator from "../HomeTabsNavigator";
import WelcomeNavigator from "../WelcomeNavigator";
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { RootNavigatorRoutes } from './constants';
import RoomShow from '../../pages/RoomShow';
import GuildPublic from '../../pages/GuildPublic';
import RoomPublic from '../../pages/RoomPublic';

const Stack = createStackNavigator();

const RootNavigatorComponent = ({ styles, appTheme, initialRouteName }: RootNavigatorPresenter.Output) => {
    return (
        <Stack.Navigator
            initialRouteName={initialRouteName}
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen
                name={RootNavigatorRoutes.WelcomeNavigator}
                options={{
                    ...TransitionPresets.ModalSlideFromBottomIOS,
                    gestureEnabled: false,
                }}
            >
                {(props) => <WelcomeNavigator {...props.navigation} navigation={props.navigation} route={props.route} style={styles.getStyle('screen')} appTheme={appTheme} />}
            </Stack.Screen>
            <Stack.Screen
                name={RootNavigatorRoutes.HomeTabsNavigator}
                options={{
                    ...TransitionPresets.ModalSlideFromBottomIOS,
                    gestureEnabled: false,
                }}
            >
                {(props) => <HomeTabsNavigator {...props.navigation} navigation={props.navigation} route={props.route} style={styles.getStyle('screen')} appTheme={appTheme}/>}
            </Stack.Screen>
            <Stack.Screen
                name={RootNavigatorRoutes.RoomShow}
                options={{
                    ...TransitionPresets.ModalSlideFromBottomIOS,
                    gestureEnabled: false,
                }}
            >
                {(props) => <RoomShow modalScreenProps={{ ...props.navigation, navigation: props.navigation, route: props.route }} style={styles.getStyle('screen')} appTheme={appTheme} />}
            </Stack.Screen>
            <Stack.Screen
                name={RootNavigatorRoutes.GuildPublic}
                options={{
                    ...TransitionPresets.ModalSlideFromBottomIOS,
                    gestureEnabled: false,
                }}
            >
                {(props) => <GuildPublic modalScreenProps={{ ...props.navigation, navigation: props.navigation, route: props.route }} style={styles.getStyle('screen')} appTheme={appTheme} />}
            </Stack.Screen>
            <Stack.Screen
                name={RootNavigatorRoutes.RoomPublic}
                options={{
                    ...TransitionPresets.ModalSlideFromBottomIOS,
                    gestureEnabled: false,
                }}
            >
                {(props) => <RoomPublic modalScreenProps={{ ...props.navigation, navigation: props.navigation, route: props.route }} style={styles.getStyle('screen')} appTheme={appTheme} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(RootNavigatorComponent, RootNavigatorPresenter.outputAreEqual);
