import React from 'react';
import RootNavigatorPresenter from './presenter';
import RootNavigatorComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<RootNavigatorPresenter.MergedProps> = (props) => {
    const output = RootNavigatorPresenter.usePresenter(props);
    return <RootNavigatorComponent {...output} />;
};

const RootNavigator = connect<RootNavigatorPresenter.StateProps, RootNavigatorPresenter.DispatchProps, RootNavigatorPresenter.Input, RootNavigatorPresenter.MergedProps, RootState>(
    RootNavigatorPresenter.mapStateToProps,
    RootNavigatorPresenter.mapDispatchToProps,
    RootNavigatorPresenter.mergeProps,
    RootNavigatorPresenter.connectOptions,
)(Container);

export default RootNavigator;
