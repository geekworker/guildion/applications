import React from 'react';
import RootNavigatorStyles from './styles';
import StyleProps from '@/shared/interfaces/StyleProps';
import { Account } from '@guildion/core';
import { RootNavigatorRoutes } from './constants';

namespace RootNavigationContainerHooks {
    export const useStyles = (props: Partial<StyleProps>): RootNavigatorStyles => {
        const styles = React.useMemo(() => new RootNavigatorStyles({ ...props }), [props]);
        return styles;
    }

    export const useInitialRouteName = ({ currentAccount, isLaunched }:{ currentAccount?: Account, isLaunched: boolean }): RootNavigatorRoutes => {
        return React.useMemo(() => {
            return !!currentAccount ? RootNavigatorRoutes.HomeTabsNavigator : RootNavigatorRoutes.WelcomeNavigator;
        }, [isLaunched]);
    }
}

export default RootNavigationContainerHooks;