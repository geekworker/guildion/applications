import GuildPublicPresenter from "../../pages/GuildPublic/presenter";
import RoomPublicPresenter from "../../pages/RoomPublic/presenter";
import RoomShowPresenter from "../../pages/RoomShow/presenter";
import HomeTabsNavigatorPresenter from "../HomeTabsNavigator/presenter";
import WelcomeNavigatorPresenter from "../WelcomeNavigator/presenter";

export const RootNavigatorRoutes = {
    WelcomeNavigator: "WelcomeNavigator",
    HomeTabsNavigator: "HomeTabsNavigator",
    RoomShow: "RoomShow",
    GuildPublic: "GuildPublic",
    RoomPublic: "RoomPublic",
} as const;

export type RootNavigatorRoutes = typeof RootNavigatorRoutes[keyof typeof RootNavigatorRoutes];

export type RootNavigatorStackParams = {
    [RootNavigatorRoutes.WelcomeNavigator]: WelcomeNavigatorPresenter.StackParams,
    [RootNavigatorRoutes.HomeTabsNavigator]: HomeTabsNavigatorPresenter.StackParams,
    [RootNavigatorRoutes.RoomShow]: RoomShowPresenter.StackParams,
    [RootNavigatorRoutes.GuildPublic]: GuildPublicPresenter.StackParams,
    [RootNavigatorRoutes.RoomPublic]: RoomPublicPresenter.StackParams,
};