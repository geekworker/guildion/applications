import RootNavigatorHooks from "./hooks";
import RootNavigatorStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { Account } from "@guildion/core";
import StyleProps from "@/shared/interfaces/StyleProps";
import { RootNavigatorRoutes } from "./constants";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace RootNavigatorPresenter {
    export type StateProps = {
        currentAccount?: Account,
        isLaunched: boolean,
    }
    
    export type DispatchProps = {
    }
    
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RootNavigatorStyles,
        initialRouteName: RootNavigatorRoutes,
    } & Partial<StyleProps>;

    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            currentAccount: state.account.currentAccount,
            isLaunched: state.app.isLaunched,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.currentAccount == next.account.currentAccount && 
            prev.app.isLaunched == next.app.isLaunched
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = RootNavigatorHooks.useStyles(props);
        const initialRouteName = RootNavigatorHooks.useInitialRouteName(props);
        return {
            ...props,
            styles,
            initialRouteName,
        }
    }
}

export default RootNavigatorPresenter;