import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import MediaLibraryWidgetNavigatorHooks from './hooks';
import MediaLibraryWidgetNavigatorStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { RoomWidgetTabsNavigatorStackParams } from '../RoomWidgetTabsNavigator/constants';
import { Room } from '@guildion/core';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

namespace MediaLibraryWidgetNavigatorPresenter {
    export type StackParams = {
    }

    export type Input = {
        tabInitialRouteName: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
    } & Partial<StyleProps> & NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'MediaLibraryWidgetNavigator'>;
    
    export type Output = {
        styles: MediaLibraryWidgetNavigatorStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        tabInitialRouteName: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
    } & Omit<Input, 'style' | 'appTheme'> & NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'MediaLibraryWidgetNavigator'>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = MediaLibraryWidgetNavigatorHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default MediaLibraryWidgetNavigatorPresenter;