import React from 'react';
import MediaLibraryWidgetNavigatorStyles from './styles';
import type MediaLibraryWidgetNavigatorPresenter from './presenter';

namespace MediaLibraryWidgetNavigatorHooks {
    export const useStyles = (props: MediaLibraryWidgetNavigatorPresenter.Input) => {
        const styles = React.useMemo(() => new MediaLibraryWidgetNavigatorStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: MediaLibraryWidgetNavigatorPresenter.Input) => {
        return {};
    }
}

export default MediaLibraryWidgetNavigatorHooks;