import React from 'react';
import MediaLibraryWidgetNavigatorComponent from './component';
import MediaLibraryWidgetNavigatorPresenter from './presenter';

const MediaLibraryWidgetNavigator = (props: MediaLibraryWidgetNavigatorPresenter.Input) => {
    const output = MediaLibraryWidgetNavigatorPresenter.usePresenter(props);
    return <MediaLibraryWidgetNavigatorComponent {...output} />;
};

export default React.memo(MediaLibraryWidgetNavigator, MediaLibraryWidgetNavigatorPresenter.inputAreEqual);