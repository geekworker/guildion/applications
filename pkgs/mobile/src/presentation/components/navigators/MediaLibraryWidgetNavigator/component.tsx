import React from 'react';
import MediaLibraryWidgetNavigatorPresenter from './presenter';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { MediaLibraryWidgetNavigatorRoutes } from './constants';
import MediaLibraryWidget from '@/presentation/components/pages/MediaLibraryWidget';
import MediaLibraryWidgetRoom from '../../pages/MediaLibraryWidgetRoom';
import MediaLibraryWidgetRoomAlbums from '../../pages/MediaLibraryWidgetRoomAlbums';
import MediaLibraryWidgetRoomPlaylists from '../../pages/MediaLibraryWidgetRoomPlaylists';
import MediaLibraryWidgetAlbums from '../../pages/MediaLibraryWidgetAlbums';
import MediaLibraryWidgetPlaylists from '../../pages/MediaLibraryWidgetPlaylists';

const Stack = createStackNavigator();

const MediaLibraryWidgetNavigatorComponent = ({ styles, appTheme, animatedStyle, room, tabInitialRouteName, ...rest }: MediaLibraryWidgetNavigatorPresenter.Output) => {
    return (
        <Stack.Navigator
            initialRouteName={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidget}
            mode="modal"
            screenOptions={{
                headerStyle: styles.headerStyle,
                headerTitleStyle: styles.headerTitleStyle,
                headerTitleAlign: 'center',
            }}
        >
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidget}
            >
                {(props) => <MediaLibraryWidget {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoom}
            >
                {(props) => <MediaLibraryWidgetRoom {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetAlbums}
            >
                {(props) => <MediaLibraryWidgetAlbums {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetPlaylists}
            >
                {(props) => <MediaLibraryWidgetPlaylists {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoomAlbums}
            >
                {(props) => <MediaLibraryWidgetRoomAlbums {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoomPlaylists}
            >
                {(props) => <MediaLibraryWidgetRoomPlaylists {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(MediaLibraryWidgetNavigatorComponent, MediaLibraryWidgetNavigatorPresenter.outputAreEqual);