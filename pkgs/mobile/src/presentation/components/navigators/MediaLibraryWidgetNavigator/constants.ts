import MediaLibraryWidgetPresenter from '@/presentation/components/pages/MediaLibraryWidget/presenter';
import MediaLibraryWidgetAlbumPresenter from '../../pages/MediaLibraryWidgetAlbum/presenter';
import MediaLibraryWidgetAlbumsPresenter from '../../pages/MediaLibraryWidgetAlbums/presenter';
import MediaLibraryWidgetPlaylistPresenter from '../../pages/MediaLibraryWidgetPlaylist/presenter';
import MediaLibraryWidgetPlaylistsPresenter from '../../pages/MediaLibraryWidgetPlaylists/presenter';
import MediaLibraryWidgetRoomPresenter from '../../pages/MediaLibraryWidgetRoom/presenter';
import MediaLibraryWidgetRoomAlbumsPresenter from '../../pages/MediaLibraryWidgetRoomAlbums/presenter';
import MediaLibraryWidgetRoomPlaylistsPresenter from '../../pages/MediaLibraryWidgetRoomPlaylists/presenter';

export const MediaLibraryWidgetNavigatorRoutes = {
    MediaLibraryWidget: 'MediaLibraryWidget',
    MediaLibraryWidgetAlbum: 'MediaLibraryWidgetAlbum',
    MediaLibraryWidgetAlbums: 'MediaLibraryWidgetAlbums',
    MediaLibraryWidgetPlaylist: 'MediaLibraryWidgetPlaylist',
    MediaLibraryWidgetPlaylists: 'MediaLibraryWidgetPlaylists',
    MediaLibraryWidgetRoom: 'MediaLibraryWidgetRoom',
    MediaLibraryWidgetRoomAlbums: 'MediaLibraryWidgetRoomAlbums',
    MediaLibraryWidgetRoomPlaylists: 'MediaLibraryWidgetRoomPlaylists',
} as const;

export type MediaLibraryWidgetNavigatorRoutes = typeof MediaLibraryWidgetNavigatorRoutes[keyof typeof MediaLibraryWidgetNavigatorRoutes];

export type MediaLibraryWidgetNavigatorStackParams = {
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidget]: MediaLibraryWidgetPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetAlbum]: MediaLibraryWidgetAlbumPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetAlbums]: MediaLibraryWidgetAlbumsPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetPlaylist]: MediaLibraryWidgetPlaylistPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetPlaylists]: MediaLibraryWidgetPlaylistsPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoom]: MediaLibraryWidgetRoomPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoomAlbums]: MediaLibraryWidgetRoomAlbumsPresenter.StackParams,
    [MediaLibraryWidgetNavigatorRoutes.MediaLibraryWidgetRoomPlaylists]: MediaLibraryWidgetRoomPlaylistsPresenter.StackParams,
};