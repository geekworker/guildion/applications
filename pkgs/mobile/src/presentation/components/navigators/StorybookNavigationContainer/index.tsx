import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { navigationTheme } from '@/shared/constants/AppTheme';

type Props = {
    children?: React.ReactNode,
};

const StorybookNavigationContainer: React.FC<Props> = ({
    children,
}) => {
    return (
        <NavigationContainer
            theme={navigationTheme}
        >
            {children}
        </NavigationContainer>
    )
};

export default React.memo(StorybookNavigationContainer);