import React from 'react';
import RoomSplitScreenNavigatorComponent from './component';
import RoomSplitScreenNavigatorPresenter from './presenter';

const RoomSplitScreenNavigator: React.VFC<RoomSplitScreenNavigatorPresenter.Input> = (props) => {
    const output = RoomSplitScreenNavigatorPresenter.usePresenter(props);
    return <RoomSplitScreenNavigatorComponent {...output} />;
};

export default React.memo(RoomSplitScreenNavigator, RoomSplitScreenNavigatorPresenter.inputAreEqual);