import RoomShowPresenter from "../../pages/RoomShow/presenter";
import RoomShowEmptyPresenter from "../../pages/RoomShowEmpty/presenter";

export const RoomSplitScreenNavigatorRoutes = {
    RoomShowEmpty: "RoomShowEmpty",
    RoomShow: "RoomShow",
} as const;

export type RoomSplitScreenNavigatorRoutes = typeof RoomSplitScreenNavigatorRoutes[keyof typeof RoomSplitScreenNavigatorRoutes];

export type RoomSplitScreenNavigatorStackParams = {
    [RoomSplitScreenNavigatorRoutes.RoomShowEmpty]: RoomShowEmptyPresenter.StackParams,
    [RoomSplitScreenNavigatorRoutes.RoomShow]: RoomShowPresenter.StackParams,
};