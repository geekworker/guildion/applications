import React from "react";
import RoomSplitScreenNavigatorStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace RoomSplitScreenNavigatorHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomSplitScreenNavigatorStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default RoomSplitScreenNavigatorHooks