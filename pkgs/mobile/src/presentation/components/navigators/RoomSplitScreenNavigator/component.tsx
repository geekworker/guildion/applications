import React from 'react';
import RoomSplitScreenNavigatorPresenter from './presenter';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { RoomSplitScreenNavigatorRoutes } from './constants';
import RoomShow from '../../pages/RoomShow';
import RoomShowEmpty from '../../pages/RoomShowEmpty';

const Stack = createStackNavigator();

const RoomSplitScreenNavigatorComponent: React.FC<RoomSplitScreenNavigatorPresenter.Output> = ({ styles, appTheme }) => {
    return (
        <Stack.Navigator
            initialRouteName={RoomSplitScreenNavigatorRoutes.RoomShowEmpty}
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen
                name={RoomSplitScreenNavigatorRoutes.RoomShowEmpty}
                options={{
                    gestureEnabled: false,
                    animationEnabled: false,
                }}
            >
                {(props) => <RoomShowEmpty { ...props.navigation} style={styles.getStyle('screen')} navigation={props.navigation} route={props.route} appTheme={appTheme} />}
            </Stack.Screen>
            <Stack.Screen
                name={RoomSplitScreenNavigatorRoutes.RoomShow}
                options={{
                    animationEnabled: false,
                    gestureEnabled: false,
                }}
            >
                {(props) => <RoomShow splitScreenProps={{ ...props.navigation, navigation: props.navigation, route: props.route }} style={styles.getStyle('screen')} appTheme={appTheme} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(RoomSplitScreenNavigatorComponent, RoomSplitScreenNavigatorPresenter.outputAreEqual);
