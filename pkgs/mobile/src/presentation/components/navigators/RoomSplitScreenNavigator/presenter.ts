import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomSplitScreenNavigatorHooks from "./hooks";
import RoomSplitScreenNavigatorStyles from './styles';
import React from 'react';
import { compare } from "@/shared/modules/ObjectCompare";

namespace RoomSplitScreenNavigatorPresenter {
    export type Input = {
    } & Partial<StyleProps>;
    
    export type Output = {
        styles: RoomSplitScreenNavigatorStyles,
        appTheme: AppTheme,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomSplitScreenNavigatorHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomSplitScreenNavigatorPresenter;