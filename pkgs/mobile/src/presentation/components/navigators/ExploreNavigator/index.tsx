import React from 'react';
import ExploreNavigatorComponent from './component';
import ExploreNavigatorPresenter from './presenter';

const ExploreNavigator = (props: ExploreNavigatorPresenter.Input) => {
    const output = ExploreNavigatorPresenter.usePresenter(props);
    return <ExploreNavigatorComponent {...output} />;
};

export default React.memo(ExploreNavigator, ExploreNavigatorPresenter.inputAreEqual);