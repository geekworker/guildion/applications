import React from 'react';
import ExploreNavigatorStyles from './styles';
import type ExploreNavigatorPresenter from './presenter';
import RootRedux from '@/infrastructure/Root/redux';
import { HomeTabsNavigatorRoutes } from '../HomeTabsNavigator/constants';

namespace ExploreNavigatorHooks {
    export const useStyles = (props: ExploreNavigatorPresenter.Input) => {
        const styles = React.useMemo(() => new ExploreNavigatorStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useObserveNavigation = (navigation: ExploreNavigatorPresenter.Input['navigation']) => {
        const context = React.useContext(RootRedux.Context);
        React.useEffect(() => {
            if (navigation.isFocused()) {
                context.dispatch(RootRedux.Action.setCurrentHomeRoute(HomeTabsNavigatorRoutes.ExploreNavigator));
            }
        },[navigation]);
    }

    export const useState = (props: ExploreNavigatorPresenter.Input) => {
        return {};
    }
}

export default ExploreNavigatorHooks;