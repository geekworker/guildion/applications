import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import ExploreNavigatorPresenter from './presenter';
import { ExploreNavigatorRoutes } from './constants';
import Explore from '../../pages/Explore';
import SearchResults from '../../pages/SearchResults';
import GuildPublic from '../../pages/GuildPublic';
import RoomPublic from '../../pages/RoomPublic';

const Stack = createStackNavigator();

const ExploreNavigatorComponent = ({ styles, appTheme, animatedStyle }: ExploreNavigatorPresenter.Output) => {
    return (
        <Stack.Navigator
            initialRouteName={ExploreNavigatorRoutes.Explore}
            mode="modal"
            screenOptions={{
                cardStyle: styles.screen,
                headerStyle: styles.headerStyle,
                headerTitleStyle: styles.headerTitleStyle,
                headerTitleAlign: 'center',
            }}
        >
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={ExploreNavigatorRoutes.Explore}
            >
                {(props) => <Explore {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={ExploreNavigatorRoutes.SearchResults}
            >
                {(props) => <SearchResults {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    ...TransitionPresets.ModalSlideFromBottomIOS,
                }}
                name={ExploreNavigatorRoutes.GuildPublic}
            >
                {(props) => <GuildPublic {...props.navigation} exploreScreenProps={{ ...props.navigation, navigation: props.navigation, route: props.route }} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={ExploreNavigatorRoutes.RoomPublic}
            >
                {(props) => <RoomPublic {...props.navigation} exploreScreenProps={{ ...props.navigation, navigation: props.navigation, route: props.route }} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(ExploreNavigatorComponent, ExploreNavigatorPresenter.outputAreEqual);