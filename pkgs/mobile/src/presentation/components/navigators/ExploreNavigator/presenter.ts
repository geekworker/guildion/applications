import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import ExploreNavigatorHooks from './hooks';
import ExploreNavigatorStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { HomeTabsNavigatorStackParams } from '../HomeTabsNavigator/constants';
import { NativeStackScreenProps } from '@react-navigation/native-stack';
import { EdgeInsets, useSafeAreaInsets } from 'react-native-safe-area-context';

namespace ExploreNavigatorPresenter {
    export type StackParams = {
    }

    export type Input = NativeStackScreenProps<HomeTabsNavigatorStackParams, 'ExploreNavigator'> & Partial<StyleProps>;
    
    export type Output = {
        styles: ExploreNavigatorStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        insets: EdgeInsets,
    } & Omit<Input, 'style' | 'appTheme'>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = ExploreNavigatorHooks.useStyles({ ...props });
        ExploreNavigatorHooks.useObserveNavigation(props.navigation);
        const insets = useSafeAreaInsets();
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            insets,
        }
    }
}

export default ExploreNavigatorPresenter;