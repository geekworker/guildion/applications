import ExplorePresenter from "@/presentation/components/pages/Explore/presenter";
import GuildPublicPresenter from "../../pages/GuildPublic/presenter";
import RoomPublicPresenter from "../../pages/RoomPublic/presenter";
import SearchResultsPresenter from "../../pages/SearchResults/presenter";

export const ExploreNavigatorRoutes = {
    Explore: "Explore",
    SearchResults: "SearchResults",
    GuildPublic: "GuildPublic",
    RoomPublic: "RoomPublic",
} as const;

export type ExploreNavigatorRoutes = typeof ExploreNavigatorRoutes[keyof typeof ExploreNavigatorRoutes];

export type ExploreNavigatorStackParams = {
    [ExploreNavigatorRoutes.Explore]: ExplorePresenter.StackParams,
    [ExploreNavigatorRoutes.SearchResults]: SearchResultsPresenter.StackParams,
    [ExploreNavigatorRoutes.GuildPublic]: GuildPublicPresenter.StackParams,
    [ExploreNavigatorRoutes.RoomPublic]: RoomPublicPresenter.StackParams,
};