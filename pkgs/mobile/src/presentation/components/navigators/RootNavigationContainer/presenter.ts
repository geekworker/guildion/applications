import React from "react";
import RootNavigationContainerHooks from "./hooks";
import RootNavigationContainerStyles from './styles';
import { Action, bindActionCreators } from 'redux';
import { RootState } from "@/presentation/redux/RootReducer";
import { InitialState, NavigationState } from "@react-navigation/routers";
import StyleProps from "@/shared/interfaces/StyleProps";
import { AppTheme } from "@/shared/constants/AppTheme";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";
import { AppAction } from "@/presentation/redux/App/AppReducer";

namespace RootNavigationContainerPresenter {
    export type StateProps = {
        initialState: InitialState,
    }
    
    export type DispatchProps = {
        saveNavigationState: (state: NavigationState) => Action,
    }
    
    export type Input = {
    } & StyleProps;
    
    export type Output = {
        styles: RootNavigationContainerStyles,
        appTheme?: AppTheme,
        initialState: InitialState,
        useNavigationState: (state: NavigationState) => void,
    }

    export type MergedProps = StateProps & DispatchProps & Input;
    
    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            initialState: state.app.initialNavigationState,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
            saveNavigationState: (state: NavigationState) => AppAction.saveInitialNavigationState.started(state),
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.app.initialNavigationState.routes.length > 0
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next, {
                operators: {
                    initialState: (prev: InitialState, next: InitialState) => (
                        prev.routes.length > 0
                    ),
                },
            })
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps, {
            operators: {
                initialState: (prev: InitialState, next: InitialState) => (
                    prev.routes.length > 0
                ),
            },
        });
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = RootNavigationContainerHooks.useStyles(props);
        return {
            ...props,
            styles,
            useNavigationState: React.useCallback(
                (state) => RootNavigationContainerHooks.useNavigationState(state, props),
                []
            ),
            initialState: { routes: [] },
        }
    }
}

export default RootNavigationContainerPresenter;