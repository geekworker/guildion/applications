import React from 'react';
import RootNavigationContainerStyles from './styles';
import { NavigationState } from '@react-navigation/routers';
import StyleProps from '@/shared/interfaces/StyleProps';
import type RootNavigationContainerPresenter from './presenter';

namespace RootNavigationContainerHooks {

    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RootNavigationContainerStyles({ ...props }), [props]);
        return styles;
    }

    export const useNavigationState = (state: NavigationState, props: RootNavigationContainerPresenter.MergedProps) => {
        props.saveNavigationState(state);
    }
}

export default RootNavigationContainerHooks;