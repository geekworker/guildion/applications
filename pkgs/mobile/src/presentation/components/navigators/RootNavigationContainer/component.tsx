import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import RootNavigationContainerPresenter from './presenter';
import RootNavigator from '../RootNavigator';
import { Store } from '@/infrastructure/Store';
import { AppAction } from '@/presentation/redux/App/AppReducer';
import { navigationTheme } from '@/shared/constants/AppTheme';

const RootNavigationContainerComponent = ({ initialState, useNavigationState, styles, appTheme }: RootNavigationContainerPresenter.Output) => {
    return (
        <NavigationContainer
            initialState={initialState}
            onStateChange={(state) => {
                if (state) useNavigationState(state);
                Store.dispatch(AppAction.resetAppErrors());
            }}
            theme={navigationTheme}
        >
            <RootNavigator style={styles.getStyle('screen')} appTheme={appTheme} />
        </NavigationContainer>
    )
};

export default React.memo(RootNavigationContainerComponent, RootNavigationContainerPresenter.outputAreEqual);
