import React from 'react';
import RootNavigationContainerComponent from './component';
import RootNavigationContainerPresenter from './presenter';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<RootNavigationContainerPresenter.MergedProps> = (props) => {
    const output = RootNavigationContainerPresenter.usePresenter(props);
    return <RootNavigationContainerComponent {...output} />;
};

const RootNavigationContainer = connect<RootNavigationContainerPresenter.StateProps, RootNavigationContainerPresenter.DispatchProps, RootNavigationContainerPresenter.Input, RootNavigationContainerPresenter.MergedProps, RootState>(
    RootNavigationContainerPresenter.mapStateToProps,
    RootNavigationContainerPresenter.mapDispatchToProps,
    RootNavigationContainerPresenter.mergeProps,
    RootNavigationContainerPresenter.connectOptions,
)(Container);

export default RootNavigationContainer;
