import React from 'react';
import RoomWidgetTabsNavigatorPresenter from './presenter';
import RoomWidgetTabsNavigatorComponent from './component';

const RoomWidgetTabsNavigator: React.VFC<RoomWidgetTabsNavigatorPresenter.Input> = (props) => {
    const output = RoomWidgetTabsNavigatorPresenter.usePresenter(props);
    return <RoomWidgetTabsNavigatorComponent {...output} />;
};

export default React.memo(RoomWidgetTabsNavigator, RoomWidgetTabsNavigatorPresenter.inputAreEqual);