import TextChatWidgetPresenter from "@/presentation/components/pages/TextChatWidget/presenter";
import RoomPostsWidgetNavigatorPresenter from "@/presentation/components/navigators/RoomPostsWidgetNavigator/presenter";
import RoomSettingWidgetPresenter from "@/presentation/components/pages/RoomSettingWidget/presenter";
import MediaLibraryWidgetNavigatorPresenter from "@/presentation/components/navigators/MediaLibraryWidgetNavigator/presenter";

export const RoomWidgetTabsNavigatorRoutes = {
    TextChatWidget: 'TextChatWidget',
    RoomPostsWidgetNavigator: 'RoomPostsWidgetNavigator',
    RoomSettingWidget: 'RoomSettingWidget',
    MediaLibraryWidgetNavigator: 'MediaLibraryWidgetNavigator',
} as const;

export type RoomWidgetTabsNavigatorRoutes = typeof RoomWidgetTabsNavigatorRoutes[keyof typeof RoomWidgetTabsNavigatorRoutes];

export type RoomWidgetTabsNavigatorStackParams = {
    [RoomWidgetTabsNavigatorRoutes.TextChatWidget]: TextChatWidgetPresenter.StackParams,
    [RoomWidgetTabsNavigatorRoutes.RoomPostsWidgetNavigator]: RoomPostsWidgetNavigatorPresenter.StackParams,
    [RoomWidgetTabsNavigatorRoutes.RoomSettingWidget]: RoomSettingWidgetPresenter.StackParams,
    [RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator]: MediaLibraryWidgetNavigatorPresenter.StackParams,
};