import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomWidgetTabsNavigatorHooks from "./hooks";
import RoomWidgetTabsNavigatorStyles from './styles';
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { RoomWidgetsNavigatorStackParams } from "../RoomWidgetsNavigator/constants";
import { RoomWidgetTabsNavigatorStackParams } from "./constants";
import { compare } from "@/shared/modules/ObjectCompare";
import { Room } from "@guildion/core";
import AnimatedStyle from "@/shared/interfaces/AnimatedStyle";

namespace RoomWidgetTabsNavigatorPresenter {
    export type StackParams = {
    }

    export type Input = {
        initialRouteName: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
    } & NativeStackScreenProps<RoomWidgetsNavigatorStackParams, 'RoomWidgetTabsNavigator'> & Partial<StyleProps>;
    
    export type Output = {
        initialRouteName: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
        styles: RoomWidgetTabsNavigatorStyles,
        appTheme: AppTheme,
        animatedStyle?: AnimatedStyle,
    };

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomWidgetTabsNavigatorHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomWidgetTabsNavigatorPresenter;