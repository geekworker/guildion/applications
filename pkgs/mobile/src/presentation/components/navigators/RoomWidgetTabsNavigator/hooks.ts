import React from "react";
import RoomWidgetTabsNavigator from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { RoomWidgetTabsNavigatorStackParams } from "./constants";
import { useNavigation } from "@react-navigation/core";

namespace RoomWidgetTabsNavigatorHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomWidgetTabsNavigator(props), [
            props,
        ]);
        return styles;
    }

    export const useTabNavigationPush = (props: { initialRouteName?: keyof RoomWidgetTabsNavigatorStackParams }) => {
        const navigation = useNavigation();
        React.useEffect(() => {
            if (!!props.initialRouteName) navigation.navigate(props.initialRouteName, {});
        }, [props.initialRouteName]);
    }
}

export default RoomWidgetTabsNavigatorHooks