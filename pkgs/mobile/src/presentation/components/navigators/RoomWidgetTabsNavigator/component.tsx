import React from 'react';
import RoomWidgetTabsNavigatorPresenter from './presenter';
import TextChatWidget from '@/presentation/components/pages/TextChatWidget';
import RoomSettingWidget from '@/presentation/components/pages/RoomSettingWidget';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { RoomWidgetTabsNavigatorRoutes } from './constants';
import RoomWidgetsNavigatorPresenter from '../RoomWidgetsNavigator/presenter';
import MediaLibraryWidgetNavigator from '../MediaLibraryWidgetNavigator';
import RoomPostsWidgetNavigator from '../RoomPostsWidgetNavigator';

const Tab = createBottomTabNavigator();

const RoomWidgetTabsNavigatorComponent: React.VFC<RoomWidgetTabsNavigatorPresenter.Output> = ({ styles, animatedStyle, appTheme, initialRouteName, room }) => {
    return (
        <Tab.Navigator
            initialRouteName={initialRouteName}
            screenOptions={{
                tabBarVisible: false,
            }}
        >
            <Tab.Screen
                name={RoomWidgetTabsNavigatorRoutes.TextChatWidget}
            >
                {(props) => (
                    <TextChatWidget
                        {...props.navigation}
                        navigation={props.navigation}
                        route={props.route}
                        appTheme={appTheme}
                        style={styles.getStyle('screen')}
                        animatedStyle={animatedStyle}
                        tabInitialRouteName={initialRouteName}
                        room={room}
                    />
                )}
            </Tab.Screen>
            <Tab.Screen
                name={RoomWidgetTabsNavigatorRoutes.MediaLibraryWidgetNavigator}
            >
                {(props) => <MediaLibraryWidgetNavigator {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} tabInitialRouteName={initialRouteName} room={room} />}
            </Tab.Screen>
            <Tab.Screen
                name={RoomWidgetTabsNavigatorRoutes.RoomPostsWidgetNavigator}
            >
                {(props) => <RoomPostsWidgetNavigator {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} tabInitialRouteName={initialRouteName} room={room} />}
            </Tab.Screen>
            <Tab.Screen
                name={RoomWidgetTabsNavigatorRoutes.RoomSettingWidget}
            >
                {(props) => <RoomSettingWidget {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} tabInitialRouteName={initialRouteName} room={room} />}
            </Tab.Screen>
        </Tab.Navigator>
    );
};

export default React.memo(RoomWidgetTabsNavigatorComponent, RoomWidgetsNavigatorPresenter.outputAreEqual);