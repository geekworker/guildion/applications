import React from 'react';
import RoomPostsWidgetNavigatorPresenter from './presenter';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { RoomPostsWidgetNavigatorRoutes } from './constants';
import RoomPostsWidget from '../../pages/RoomPostsWidget';
import RoomPostsWidgetShow from '../../pages/RoomPostsWidgetShow';

const Stack = createStackNavigator();

const RoomPostsWidgetNavigatorComponent = ({ styles, appTheme, animatedStyle, room, tabInitialRouteName, ...rest }: RoomPostsWidgetNavigatorPresenter.Output) => {
    return (
        <Stack.Navigator
            initialRouteName={RoomPostsWidgetNavigatorRoutes.RoomPostsWidget}
            mode="modal"
        >
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={RoomPostsWidgetNavigatorRoutes.RoomPostsWidget}
            >
                {(props) => <RoomPostsWidget {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: true,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={RoomPostsWidgetNavigatorRoutes.RoomPostsWidgetShow}
            >
                {(props) => <RoomPostsWidgetShow {...props.navigation} parentStackScreen={{ ...rest }} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} room={room} tabInitialRouteName={tabInitialRouteName} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(RoomPostsWidgetNavigatorComponent, RoomPostsWidgetNavigatorPresenter.outputAreEqual);