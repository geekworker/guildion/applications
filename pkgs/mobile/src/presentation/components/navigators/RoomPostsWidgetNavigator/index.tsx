import React from 'react';
import RoomPostsWidgetNavigatorComponent from './component';
import RoomPostsWidgetNavigatorPresenter from './presenter';

const RoomPostsWidgetNavigator = (props: RoomPostsWidgetNavigatorPresenter.Input) => {
    const output = RoomPostsWidgetNavigatorPresenter.usePresenter(props);
    return <RoomPostsWidgetNavigatorComponent {...output} />;
};

export default React.memo(RoomPostsWidgetNavigator, RoomPostsWidgetNavigatorPresenter.inputAreEqual);