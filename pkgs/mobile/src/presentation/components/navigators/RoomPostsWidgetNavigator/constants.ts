import RoomPostsWidgetPresenter from "../../pages/RoomPostsWidget/presenter";
import RoomPostsWidgetShowPresenter from "../../pages/RoomPostsWidgetShow/presenter";

export const RoomPostsWidgetNavigatorRoutes = {
    RoomPostsWidget: 'RoomPostsWidget',
    RoomPostsWidgetShow: 'RoomPostsWidgetShow',
} as const;

export type RoomPostsWidgetNavigatorRoutes = typeof RoomPostsWidgetNavigatorRoutes[keyof typeof RoomPostsWidgetNavigatorRoutes];

export type RoomPostsWidgetNavigatorStackParams = {
    [RoomPostsWidgetNavigatorRoutes.RoomPostsWidget]: RoomPostsWidgetPresenter.StackParams,
    [RoomPostsWidgetNavigatorRoutes.RoomPostsWidgetShow]: RoomPostsWidgetShowPresenter.StackParams,
};