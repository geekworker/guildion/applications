import React from 'react';
import RoomPostsWidgetNavigatorStyles from './styles';
import type RoomPostsWidgetNavigatorPresenter from './presenter';

namespace RoomPostsWidgetNavigatorHooks {
    export const useStyles = (props: RoomPostsWidgetNavigatorPresenter.Input) => {
        const styles = React.useMemo(() => new RoomPostsWidgetNavigatorStyles({
            appTheme: props.appTheme,
            style: props.style,
        }), [
            props.appTheme,
            props.style,
        ]);
        return styles;
    }

    export const useState = (props: RoomPostsWidgetNavigatorPresenter.Input) => {
        return {};
    }
}

export default RoomPostsWidgetNavigatorHooks;