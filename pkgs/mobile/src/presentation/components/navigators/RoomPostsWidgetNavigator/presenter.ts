import { AppTheme, fallbackAppTheme } from '@/shared/constants/AppTheme';
import StyleProps from '@/shared/interfaces/StyleProps';
import RoomPostsWidgetNavigatorHooks from './hooks';
import RoomPostsWidgetNavigatorStyles from './styles';
import { compare } from '@/shared/modules/ObjectCompare';
import AnimatedStyle from '@/shared/interfaces/AnimatedStyle';
import { RoomWidgetTabsNavigatorStackParams } from '../RoomWidgetTabsNavigator/constants';
import { Room } from '@guildion/core';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

namespace RoomPostsWidgetNavigatorPresenter {
    export type StackParams = {
    };

    export type Input = {
        tabInitialRouteName: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
    } & Partial<StyleProps> & NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'RoomPostsWidgetNavigator'>;
    
    export type Output = {
        styles: RoomPostsWidgetNavigatorStyles,
        animatedStyle?: AnimatedStyle,
        appTheme: AppTheme,
        tabInitialRouteName: keyof RoomWidgetTabsNavigatorStackParams,
        room: Room,
    } & Omit<Input, 'style' | 'appTheme'> & NativeStackScreenProps<RoomWidgetTabsNavigatorStackParams, 'RoomPostsWidgetNavigator'>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    }

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomPostsWidgetNavigatorHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomPostsWidgetNavigatorPresenter;