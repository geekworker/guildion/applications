import React from 'react';
import HomeTabsNavigatorPresenter from './presenter';
import HomeTabsNavigatorComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<HomeTabsNavigatorPresenter.MergedProps> = (props) => {
    const output = HomeTabsNavigatorPresenter.usePresenter(props);
    return <HomeTabsNavigatorComponent {...output} />;
};

const HomeTabsNavigator = connect<HomeTabsNavigatorPresenter.StateProps, HomeTabsNavigatorPresenter.DispatchProps, HomeTabsNavigatorPresenter.Input, HomeTabsNavigatorPresenter.MergedProps, RootState>(
    HomeTabsNavigatorPresenter.mapStateToProps,
    HomeTabsNavigatorPresenter.mapDispatchToProps,
    HomeTabsNavigatorPresenter.mergeProps,
    HomeTabsNavigatorPresenter.connectOptions,
)(Container);

export default HomeTabsNavigator;