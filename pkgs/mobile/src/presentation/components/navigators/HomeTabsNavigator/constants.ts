import { ExploreNavigatorStackParams } from "../ExploreNavigator/constants";
import { GuildsNavigatorStackParams } from "../GuildsNavigator/constants";

export const HomeTabsNavigatorRoutes = {
    GuildsNavigator: 'GuildsNavigator',
    ExploreNavigator: 'ExploreNavigator',
} as const;

export type HomeTabsNavigatorRoutes = typeof HomeTabsNavigatorRoutes[keyof typeof HomeTabsNavigatorRoutes];

export type HomeTabsNavigatorStackParams = {
    [HomeTabsNavigatorRoutes.GuildsNavigator]: GuildsNavigatorStackParams,
    [HomeTabsNavigatorRoutes.ExploreNavigator]: ExploreNavigatorStackParams,
};
