import HomeTabsNavigatorHooks from "./hooks";
import HomeTabsNavigatorStyles from './styles';
import { bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import StyleProps from "@/shared/interfaces/StyleProps";
import { RootNavigatorStackParams } from "../RootNavigator/constants";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { Account } from "@guildion/core";
import { useSplitView } from "@/shared/hooks/useSplitView";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace HomeTabsNavigatorPresenter {
    export type StackParams = {
    };
    
    export type StateProps = {
        isLaunched: boolean,
        currentAccount?: Account,
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<RootNavigatorStackParams, 'HomeTabsNavigator'> & Partial<StyleProps>;
    
    export type Output = {
        styles: HomeTabsNavigatorStyles,
        appTheme: AppTheme,
        tabBarVisible: boolean,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;
    
    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
            currentAccount: state.account.currentAccount,
            isLaunched: state.app.isLaunched,
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            prev.account.currentAccount == next.account.currentAccount && 
            prev.app.isLaunched == next.app.isLaunched
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            compare(prev, next)
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = HomeTabsNavigatorHooks.useStyles(props);
        HomeTabsNavigatorHooks.useAccountObserve(props);
        HomeTabsNavigatorHooks.useObserveNavigator(props);
        const shouldSplit = useSplitView();
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
            tabBarVisible: !shouldSplit,
        }
    }
}

export default HomeTabsNavigatorPresenter;