import { MontserratFont } from '@/shared/constants/Font';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import BrandIcon from '../../Icons/BrandIcon';
import ExploreNavigator from '../ExploreNavigator';
import GuildsNavigator from '../GuildsNavigator';
import { HomeTabsNavigatorRoutes } from './constants';
import HomeTabsNavigatorPresenter from './presenter';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

const HomeTabsNavigatorComponent: React.FC<HomeTabsNavigatorPresenter.Output> = ({ styles, appTheme, tabBarVisible }) => {
    return (
        <Tab.Navigator
            initialRouteName={HomeTabsNavigatorRoutes.GuildsNavigator}
            tabBarOptions={{
                tabStyle: {
                    backgroundColor: appTheme.tab.background,
                },
                style: {
                    borderTopColor: appTheme.tab.shadow,
                    backgroundColor: appTheme.tab.background,
                },
                activeTintColor: appTheme.tab.active,
                inactiveTintColor: appTheme.tab.inactive,
                labelStyle: {
                    fontFamily: MontserratFont.Regular,
                },
            }}
            screenOptions={{
                tabBarVisible,
            }}
            sceneContainerStyle={styles.sceneContainerStyle}
        >
            <Tab.Screen
                name={HomeTabsNavigatorRoutes.GuildsNavigator}
                options={{
                    title: 'Guilds',
                    tabBarIcon: (props) => <BrandIcon width={props.size} height={props.size} color={props.color} />,
                }}
            >
                {(props) => <GuildsNavigator {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Tab.Screen>
            <Tab.Screen
                name={HomeTabsNavigatorRoutes.ExploreNavigator}
                options={{
                    title: 'Explore',
                    tabBarIcon: (props) => <IoniconsIcon size={props.size} name={'search'} color={props.color} />,
                }}
            >
                {(props) => <ExploreNavigator {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Tab.Screen>
        </Tab.Navigator>
    )
};

export default React.memo(HomeTabsNavigatorComponent, HomeTabsNavigatorPresenter.outputAreEqual);