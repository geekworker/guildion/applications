import React from "react";
import HomeTabsNavigatorStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import { Account } from "@guildion/core";
import type HomeTabsNavigatorPresenter from "./presenter";
import { RootNavigatorRoutes } from "../RootNavigator/constants";
import RootRedux from "@/infrastructure/Root/redux";
import { useSplitView } from "@/shared/hooks/useSplitView";

namespace HomeTabsNavigatorHooks {
    export const useStyles = (props: StyleProps): HomeTabsNavigatorStyles => {
        const shouldSplit = useSplitView();
        const styles = React.useMemo(() => new HomeTabsNavigatorStyles({ ...props, shouldSplit }), [
            props,
            shouldSplit
        ]);
        return styles;
    }

    export const useAccountObserve = ({ currentAccount, navigation, isLaunched }: { currentAccount?: Account, navigation: HomeTabsNavigatorPresenter.Input['navigation'], isLaunched: boolean, }) => {
        React.useEffect(() => {
            isLaunched && !currentAccount && !!navigation.isFocused && navigation.push(RootNavigatorRoutes.WelcomeNavigator, {});
        }, [currentAccount, navigation, isLaunched]);
    }

    export const useObserveNavigator = (props: HomeTabsNavigatorPresenter.MergedProps) => {
        const context = React.useContext(RootRedux.Context);
        React.useEffect(() => {
            context.dispatch(RootRedux.Action.setCurrentHomeTabsNavigator(props.navigation));
        }, [props.navigation]);
    }
}

export default HomeTabsNavigatorHooks