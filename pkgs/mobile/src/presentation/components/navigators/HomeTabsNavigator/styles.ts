import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { leftStackWidth } from '@/shared/constants/SplitConfig';

const styles = ({
    style,
    appTheme,
    shouldSplit,
}: StyleProps & { shouldSplit: boolean }) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        sceneContainerStyle: {
            ...style?.toStyleObject(),
            backgroundColor: style?.backgroundColor ?? appTheme.background.subm1,
        },
        screen: {
            ...style?.toStyleObject(),
            width: shouldSplit ? leftStackWidth : style?.width,
        },
    });
}

type Styles = typeof styles;

export default class HomeTabsNavigatorStyles extends Record<ReturnType<Styles>>({
    ...styles({ shouldSplit: false })
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps & { shouldSplit: boolean }) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};