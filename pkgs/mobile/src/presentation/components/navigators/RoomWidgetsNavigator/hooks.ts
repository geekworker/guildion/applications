import React from "react";
import RoomWidgetsNavigatorStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";

namespace RoomWidgetsNavigatorHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomWidgetsNavigatorStyles(props), [
            props,
        ]);
        return styles;
    }
}

export default RoomWidgetsNavigatorHooks