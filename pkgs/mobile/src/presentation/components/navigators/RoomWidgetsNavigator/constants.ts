import RoomLogsWidgetPresenter from "@/presentation/components/pages/RoomLogsWidget/presenter";
import TextChatWidgetPresenter from "@/presentation/components/pages/TextChatWidget/presenter";

export const RoomWidgetsNavigatorRoutes = {
    RoomLogsWidget: 'RoomLogsWidget',
    RoomWidgetTabsNavigator: 'RoomWidgetTabsNavigator',
} as const;

export type RoomWidgetsNavigatorRoutes = typeof RoomWidgetsNavigatorRoutes[keyof typeof RoomWidgetsNavigatorRoutes];

export type RoomWidgetsNavigatorStackParams = {
    [RoomWidgetsNavigatorRoutes.RoomLogsWidget]: RoomLogsWidgetPresenter.StackParams,
    [RoomWidgetsNavigatorRoutes.RoomWidgetTabsNavigator]: TextChatWidgetPresenter.StackParams,
};