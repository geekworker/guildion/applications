import React from 'react';
import RoomWidgetsNavigatorPresenter from './presenter';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import RoomLogsWidget from '@/presentation/components/pages/RoomLogsWidget';
import RoomWidgetTabsNavigator from '../RoomWidgetTabsNavigator';
import { RoomWidgetsNavigatorRoutes } from './constants';
import { RoomWidgetTabsNavigatorRoutes } from '../RoomWidgetTabsNavigator/constants';

const Stack = createStackNavigator();

const RoomWidgetsNavigatorComponent: React.FC<RoomWidgetsNavigatorPresenter.Output> = ({ styles, animatedStyle, appTheme, tabInitialRouteName, onChangeTabRouteName, room, loading }) => {
    return (
        <Stack.Navigator
            initialRouteName={RoomWidgetsNavigatorRoutes.RoomLogsWidget}
            mode="modal"
            screenOptions={{
                headerTitleAlign: 'center',
            }}
        >
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    // ...TransitionPresets.ModalSlideFromBottomIOS,
                }}
                name={RoomWidgetsNavigatorRoutes.RoomLogsWidget}
            >
                {(props) => (
                    <RoomLogsWidget
                        {...props.navigation}
                        navigation={props.navigation} route={props.route}
                        tabInitialRouteName={tabInitialRouteName}
                        onChangeTabRouteName={onChangeTabRouteName}
                        appTheme={appTheme}
                        style={styles.getStyle('screen')}
                        animatedStyle={animatedStyle}
                        loading={loading}
                        room={room}
                    />
                )}
            </Stack.Screen>
            {!loading && (
                <Stack.Screen
                    options={{
                        headerShown: false,
                        gestureEnabled: false,
                        ...TransitionPresets.ModalSlideFromBottomIOS,
                    }}
                    name={RoomWidgetsNavigatorRoutes.RoomWidgetTabsNavigator}
                >
                    {(props) => (
                        <RoomWidgetTabsNavigator
                            {...props.navigation}
                            navigation={props.navigation} route={props.route}
                            appTheme={appTheme}
                            style={styles.getStyle('screen')}
                            animatedStyle={animatedStyle}
                            initialRouteName={tabInitialRouteName || RoomWidgetTabsNavigatorRoutes.TextChatWidget}
                            room={room}
                        />
                    )}
                </Stack.Screen>
            )}
        </Stack.Navigator>
    );
};

export default React.memo(RoomWidgetsNavigatorComponent, RoomWidgetsNavigatorPresenter.outputAreEqual);