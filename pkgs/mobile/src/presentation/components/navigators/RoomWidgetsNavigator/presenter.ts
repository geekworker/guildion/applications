import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import StyleProps from "@/shared/interfaces/StyleProps";
import RoomWidgetsNavigatorHooks from "./hooks";
import RoomWidgetsNavigatorStyles from './styles';
import { RoomWidgetTabsNavigatorStackParams } from "../RoomWidgetTabsNavigator/constants";
import { compare } from "@/shared/modules/ObjectCompare";
import { Room } from "@guildion/core";

namespace RoomWidgetsNavigatorPresenter {
    export type Input = {
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        onChangeTabRouteName?: (value?: keyof RoomWidgetTabsNavigatorStackParams | 'SyncVisionWidget') => void,
        room: Room,
        loading?: boolean,
    } & Partial<StyleProps>;

    export type Output = {
        styles: RoomWidgetsNavigatorStyles,
        tabInitialRouteName?: keyof RoomWidgetTabsNavigatorStackParams,
        onChangeTabRouteName?: (value?: keyof RoomWidgetTabsNavigatorStackParams | 'SyncVisionWidget') => void,
        room: Room,
        loading?: boolean,
    } & Partial<StyleProps>;

    export const inputAreEqual = (prevProps: Readonly<Input>, nextProps: Readonly<Input>): boolean => {
        return compare(prevProps, nextProps);
    };

    // FIXME: for some reasons, if put output type here, i got error type is diffrence with Readonly<PropsWithChildren<Output>>
    export const outputAreEqual = (prevProps: Readonly<any>, nextProps: Readonly<any>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: Input): Output {
        const styles = RoomWidgetsNavigatorHooks.useStyles({ ...props });
        return {
            ...props,
            styles,
            appTheme: props.appTheme ?? fallbackAppTheme,
        }
    }
}

export default RoomWidgetsNavigatorPresenter;