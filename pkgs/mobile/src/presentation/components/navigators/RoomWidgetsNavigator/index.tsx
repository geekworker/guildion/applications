import React from 'react';
import RoomWidgetsNavigatorPresenter from './presenter';
import RoomWidgetsNavigatorComponent from './component';

const RoomWidgetsNavigator: React.VFC<RoomWidgetsNavigatorPresenter.Input> = (props) => {
    const output = RoomWidgetsNavigatorPresenter.usePresenter(props);
    return <RoomWidgetsNavigatorComponent {...output} />;
};

export default React.memo(RoomWidgetsNavigator, RoomWidgetsNavigatorPresenter.inputAreEqual);