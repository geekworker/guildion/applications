import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import React from 'react';
import GuildsNavigatorPresenter from './presenter';
import GuildsIndex from '@/presentation/components/pages/GuildsIndex';
import { GuildsNavigatorRoutes } from './constants';

const Stack = createStackNavigator();

const GuildsNavigatorComponent = ({ styles, appTheme }: GuildsNavigatorPresenter.Output) => {
    return (
        <Stack.Navigator
            initialRouteName={GuildsNavigatorRoutes.GuildsIndex}
            mode="modal"
            screenOptions={{
                cardStyle: styles.screen,
            }}
        >
            <Stack.Screen
                options={{
                    headerShown: false,
                    gestureEnabled: false,
                    ...TransitionPresets.SlideFromRightIOS,
                }}
                name={GuildsNavigatorRoutes.GuildsIndex}
            >
                {(props) => <GuildsIndex {...props.navigation} navigation={props.navigation} route={props.route} appTheme={appTheme} style={styles.getStyle('screen')} />}
            </Stack.Screen>
        </Stack.Navigator>
    );
};

export default React.memo(GuildsNavigatorComponent, GuildsNavigatorPresenter.outputAreEqual);