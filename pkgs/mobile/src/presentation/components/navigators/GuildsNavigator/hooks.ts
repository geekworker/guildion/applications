import React from "react";
import GuildsNavigatorStyles from './styles';
import StyleProps from "@/shared/interfaces/StyleProps";
import GuildsNavigatorPresenter from "./presenter";
import RootRedux from "@/infrastructure/Root/redux";
import { HomeTabsNavigatorRoutes } from "../HomeTabsNavigator/constants";

namespace GuildsNavigatorHooks {
    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new GuildsNavigatorStyles({ ...props }), [props]);
        return styles;
    }

    export const useObserveNavigation = (navigation: GuildsNavigatorPresenter.MergedProps['navigation']) => {
        const context = React.useContext(RootRedux.Context);
        React.useEffect(() => {
            if (navigation.isFocused()) {
                context.dispatch(RootRedux.Action.setCurrentHomeRoute(HomeTabsNavigatorRoutes.GuildsNavigator));
            }
        },[navigation]);
    }
}

export default GuildsNavigatorHooks