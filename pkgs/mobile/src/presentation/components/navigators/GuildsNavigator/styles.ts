import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { MontserratFont } from "@/shared/constants/Font";

const styles = ({ appTheme, style }: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        headerStyle: {
            backgroundColor: appTheme.navigation.defaultHeader,
            elevation: 0,
            shadowOpacity: 0,
            height: 44,
        },
        headerTitleStyle: {
            color: appTheme.navigation.defaultText,
            fontFamily: MontserratFont.Bold,
            fontSize: 28,
            textAlign: 'left',
        },
        rightButtons: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'flex-end',
            justifyContent: 'center',
            height: 32,
            paddingRight: 20,
        },
        rightNewButton: {
            color: appTheme.element.default,
            backgroundColor: appTheme.button.mono,
            width: 150,
            height: 36,
            borderRadius: 18,
            marginLeft: 8,
            fontSize: 15,
        },
        rightNewIcon: {
            color: appTheme.element.default,
            width: 20,
            height: 20,
        },
        rightEditButton: {
            color: appTheme.element.default,
            backgroundColor: appTheme.button.mono,
            width: 36,
            height: 36,
            borderRadius: 18,
        },
        rightEditIcon: {
            color: appTheme.element.default,
            width: 26,
            height: 26,
        },
        screen: {
            ...style?.toStyleObject(),
        },
    });
};

type Styles = typeof styles;

export default class GuildsNavigatorStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};