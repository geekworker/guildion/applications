import GuildsIndexPresenter from "@/presentation/components/pages/GuildsIndex/presenter";

export const GuildsNavigatorRoutes = {
    GuildsIndex: "GuildsIndex",
} as const;

export type GuildsNavigatorRoutes = typeof GuildsNavigatorRoutes[keyof typeof GuildsNavigatorRoutes];

export type GuildsNavigatorStackParams = {
    [GuildsNavigatorRoutes.GuildsIndex]: GuildsIndexPresenter.StackParams,
};