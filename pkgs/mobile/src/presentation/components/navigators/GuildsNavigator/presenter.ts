import GuildsNavigatorHooks from "./hooks";
import GuildsNavigatorStyles from './styles';
import { Action, AnyAction, bindActionCreators, Dispatch } from 'redux';
import { AppTheme, fallbackAppTheme } from "@/shared/constants/AppTheme";
import { RootState } from "@/presentation/redux/RootReducer";
import { NativeStackScreenProps } from "@react-navigation/native-stack";
import { HomeTabsNavigatorStackParams } from "../HomeTabsNavigator/constants";
import { EdgeInsets, useSafeAreaInsets } from "react-native-safe-area-context";
import StyleProps from "@/shared/interfaces/StyleProps";
import { MapDispatchToProps, MapStateToProps, MergeProps, Options } from "react-redux";
import { compare } from "@/shared/modules/ObjectCompare";

namespace GuildsNavigatorPresenter {
    export type StackParams = {
    }

    export type StateProps = {
    }
    
    export type DispatchProps = {
    }
    
    export type Input = NativeStackScreenProps<HomeTabsNavigatorStackParams, 'GuildsNavigator'> & Partial<StyleProps>;
    
    export type Output = {
        styles: GuildsNavigatorStyles,
        appTheme: AppTheme,
        insets: EdgeInsets,
    };
    
    export type MergedProps = StateProps & DispatchProps & Input;

    export const mapStateToProps: MapStateToProps<StateProps, Input, RootState> = (state) => {
        return {
        };
    }
    
    export const mapDispatchToProps: MapDispatchToProps<DispatchProps, Input> = (dispatch, props) => {
        return bindActionCreators({
        }, dispatch);
    }

    export const mergeProps: MergeProps<StateProps, DispatchProps, Input, MergedProps> = (stateProps, dispatchProps, ownProps) => {
        return Object.assign({}, ownProps, stateProps, dispatchProps);
    }

    export const connectOptions: Options<RootState, StateProps, Input, MergedProps> = {
        areStatesEqual: (prev: RootState, next: RootState) => (
            true
        ),
        areOwnPropsEqual: (prev: Input, next: Input) => (
            compare(prev, next)
        ),
        areStatePropsEqual: (prev: StateProps, next: StateProps) => (
            true
        ),
        areMergedPropsEqual: (prev: MergedProps, next: MergedProps) => (
            compare(prev, next)
        ),
    };

    export const outputAreEqual = (prevProps: Readonly<Output>, nextProps: Readonly<Output>): boolean => {
        return compare(prevProps, nextProps);
    };
    
    export function usePresenter(props: MergedProps): Output {
        const styles = GuildsNavigatorHooks.useStyles(props);
        const insets = useSafeAreaInsets();
        GuildsNavigatorHooks.useObserveNavigation(props.navigation);
        return {
            ...props,
            appTheme: props.appTheme ?? fallbackAppTheme,
            insets,
            styles,
        }
    }
}

export default GuildsNavigatorPresenter;