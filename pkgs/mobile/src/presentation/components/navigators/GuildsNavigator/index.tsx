import React from 'react';
import GuildsNavigatorPresenter from './presenter';
import GuildsNavigatorComponent from './component';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<GuildsNavigatorPresenter.MergedProps> = (props) => {
    const output = GuildsNavigatorPresenter.usePresenter(props);
    return <GuildsNavigatorComponent {...output} />;
};

const GuildsNavigator = connect<GuildsNavigatorPresenter.StateProps, GuildsNavigatorPresenter.DispatchProps, GuildsNavigatorPresenter.Input, GuildsNavigatorPresenter.MergedProps, RootState>(
    GuildsNavigatorPresenter.mapStateToProps,
    GuildsNavigatorPresenter.mapDispatchToProps,
    GuildsNavigatorPresenter.mergeProps,
    GuildsNavigatorPresenter.connectOptions,
)(Container);

export default GuildsNavigator;