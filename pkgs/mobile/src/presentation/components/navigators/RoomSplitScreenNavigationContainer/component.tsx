import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import RoomSplitScreenNavigationContainerPresenter from './presenter';
import RoomSplitScreenNavigator from '../RoomSplitScreenNavigator';
import Animated from 'react-native-reanimated';
import { navigationTheme } from '@/shared/constants/AppTheme';

const RoomSplitScreenNavigationContainerComponent = ({ initialState, useNavigationState, styles, appTheme }: RoomSplitScreenNavigationContainerPresenter.Output) => {
    return (
        <Animated.View style={styles.container}>
            <NavigationContainer
                initialState={initialState}
                theme={navigationTheme}
                onStateChange={(state) => {
                    if (state) useNavigationState(state);
                }}
            >
                <RoomSplitScreenNavigator style={styles.getStyle('screen')} appTheme={appTheme} />
            </NavigationContainer>
        </Animated.View>
    )
};

export default React.memo(RoomSplitScreenNavigationContainerComponent, RoomSplitScreenNavigationContainerPresenter.outputAreEqual);
