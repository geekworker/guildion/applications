import React from 'react';
import RoomSplitScreenNavigationContainerStyles from './styles';
import { NavigationState } from '@react-navigation/routers';
import StyleProps from '@/shared/interfaces/StyleProps';
import type RoomSplitScreenNavigationContainerPresenter from './presenter';

namespace RoomSplitScreenNavigationContainerHooks {

    export const useStyles = (props: StyleProps) => {
        const styles = React.useMemo(() => new RoomSplitScreenNavigationContainerStyles({ ...props }), [props]);
        return styles;
    }

    export const useNavigationState = (state: NavigationState, props: RoomSplitScreenNavigationContainerPresenter.MergedProps) => {
        props.saveNavigationState(state);
    }
}

export default RoomSplitScreenNavigationContainerHooks;