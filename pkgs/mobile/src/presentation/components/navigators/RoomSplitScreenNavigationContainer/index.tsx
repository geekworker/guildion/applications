import React from 'react';
import RoomSplitScreenNavigationContainerComponent from './component';
import RoomSplitScreenNavigationContainerPresenter from './presenter';
import { RootState } from '@/presentation/redux/RootReducer';
import { connect } from 'react-redux';

const Container: React.VFC<RoomSplitScreenNavigationContainerPresenter.MergedProps> = (props) => {
    const output = RoomSplitScreenNavigationContainerPresenter.usePresenter(props);
    return <RoomSplitScreenNavigationContainerComponent {...output} />;
};

const RoomSplitScreenNavigationContainer = connect<RoomSplitScreenNavigationContainerPresenter.StateProps, RoomSplitScreenNavigationContainerPresenter.DispatchProps, RoomSplitScreenNavigationContainerPresenter.Input, RoomSplitScreenNavigationContainerPresenter.MergedProps, RootState>(
    RoomSplitScreenNavigationContainerPresenter.mapStateToProps,
    RoomSplitScreenNavigationContainerPresenter.mapDispatchToProps,
    RoomSplitScreenNavigationContainerPresenter.mergeProps,
    RoomSplitScreenNavigationContainerPresenter.connectOptions,
)(Container);

export default RoomSplitScreenNavigationContainer;
