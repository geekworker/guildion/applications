import { fallbackAppTheme } from "@/shared/constants/AppTheme";
import { Style } from "@/shared/interfaces/Style";
import StyleProps from "@/shared/interfaces/StyleProps";
import { StylesImpl, StylesKey } from "@/shared/interfaces/Styles";
import { Record } from "immutable";
import { StyleSheet } from 'react-native';
import { getRightStackWidth } from '@/shared/constants/SplitConfig';

const styles = ({
    style,
    appTheme,
}: StyleProps) => {
    appTheme ||= fallbackAppTheme;
    return StyleSheet.create({
        container: {
            ...style?.toStyleObject(),
            width: getRightStackWidth(Number(style?.width ?? 0)),
            height: style?.height,
            position: 'absolute',
            right: 0,
            bottom: 0,
        },
        screen: {
            borderLeftColor: appTheme.tab.shadow,
            borderLeftWidth: 1,
            width: getRightStackWidth(Number(style?.width ?? 0)),
            height: style?.height,
        },
    });
}

type Styles = typeof styles;

export default class RoomSplitScreenNavigationContainerStyles extends Record<ReturnType<Styles>>({
    ...styles({})
}) implements StylesImpl<Styles> {
    constructor(props: StyleProps) {
        super(styles(props));
    }

    getStyle(key: StylesKey<Styles>): Style {
        return new Style({
            ...this.get(key),
        });
    }
};