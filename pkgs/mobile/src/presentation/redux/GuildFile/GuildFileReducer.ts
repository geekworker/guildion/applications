import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { Files, ImmutableMap } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const actionCreator = actionCreatorFactory('GuildFile');

export interface GuildRecentFilesPageStateAttributes { guildId: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type GuildRecentFilesPageState = ImmutableMap<GuildRecentFilesPageStateAttributes>;
export type GuildsRecentFilesPageState = List<GuildRecentFilesPageState>;
export interface GuildFilesPageStateAttributes { guildId: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type GuildFilesPageState = ImmutableMap<GuildFilesPageStateAttributes>;
export type GuildsFilesPageState = List<GuildFilesPageState>;
export interface GuildAlbumsPageStateAttributes { guildId: string, roomId?: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type GuildAlbumsPageState = ImmutableMap<GuildAlbumsPageStateAttributes>;
export type GuildsAlbumsPageState = List<GuildAlbumsPageState>;
export interface GuildPlaylistsPageStateAttributes { guildId: string, roomId?: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type GuildPlaylistsPageState = ImmutableMap<GuildPlaylistsPageStateAttributes>;
export type GuildsPlaylistsPageState = List<GuildPlaylistsPageState>;

export const GuildFileAction = {
    fetchIndex: actionCreator.async<{ guildId: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_INDEX'),
    fetchMoreIndex: actionCreator.async<{ guildId: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_MORE_INDEX'),
    fetchRecentFiles: actionCreator.async<{ guildId: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_RECENT_INDEX'),
    fetchMoreRecentFiles: actionCreator.async<{ guildId: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_MORE_RECENT_INDEX'),
    fetchAlbums: actionCreator.async<{ guildId: string, roomId?: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_ALBUMS'),
    fetchMoreAlbums: actionCreator.async<{ guildId: string, roomId?: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_MORE_ALBUMS'),
    fetchPlaylists: actionCreator.async<{ guildId: string, roomId?: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_PLAYLISTS'),
    fetchMorePlaylists: actionCreator.async<{ guildId: string, roomId?: string }, { files: Files, paginatable: boolean }>('GUILD_FILE_FETCH_MORE_PLAYLISTS'),
};

export type GuildFileAction = typeof GuildFileAction[keyof typeof GuildFileAction];

export class GuildFileState extends Record<{
    pages: {
        index: GuildsFilesPageState,
        recents: GuildsRecentFilesPageState,
        albums: GuildsAlbumsPageState,
        playlists: GuildsPlaylistsPageState,
    },
}>({
    pages: {
        index: List([]),
        recents: List([]),
        albums: List([]),
        playlists: List([]),
    }
}) {}

const GuildFileReducer = reducerWithInitialState(new GuildFileState())
    .case(GuildFileAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchRecentFiles.started, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchRecentFiles.done, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchRecentFiles.failed, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreRecentFiles.started, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreRecentFiles.done, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreRecentFiles.failed, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchAlbums.started, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId && p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchAlbums.done, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchAlbums.failed, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreAlbums.started, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId && p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreAlbums.done, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMoreAlbums.failed, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchPlaylists.started, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId && p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchPlaylists.done, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchPlaylists.failed, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMorePlaylists.started, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId && p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMorePlaylists.done, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(GuildFileAction.fetchMorePlaylists.failed, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId && p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })

export default GuildFileReducer;