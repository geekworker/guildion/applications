import GuildFilesUseCase from '@/domain/usecases/GuildFilesUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { GuildFileAction } from './GuildFileReducer';

export default function* GuildFileSaga() {
    yield takeEvery(GuildFileAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchIndex, { skipStartedAction: true })(new GuildFilesUseCase().fetchIndex), payload)
    });
    yield takeEvery(GuildFileAction.fetchMoreIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchMoreIndex, { skipStartedAction: true })(new GuildFilesUseCase().fetchMoreIndex), payload)
    });
    yield takeEvery(GuildFileAction.fetchRecentFiles.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchRecentFiles, { skipStartedAction: true })(new GuildFilesUseCase().fetchRecentFiles), payload)
    });
    yield takeEvery(GuildFileAction.fetchMoreRecentFiles.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchMoreRecentFiles, { skipStartedAction: true })(new GuildFilesUseCase().fetchMoreRecentFiles), payload)
    });
    yield takeEvery(GuildFileAction.fetchAlbums.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchAlbums, { skipStartedAction: true })(new GuildFilesUseCase().fetchAlbums), payload)
    });
    yield takeEvery(GuildFileAction.fetchMoreAlbums.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchMoreAlbums, { skipStartedAction: true })(new GuildFilesUseCase().fetchMoreAlbums), payload)
    });
    yield takeEvery(GuildFileAction.fetchPlaylists.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchPlaylists, { skipStartedAction: true })(new GuildFilesUseCase().fetchPlaylists), payload)
    });
    yield takeEvery(GuildFileAction.fetchMorePlaylists.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildFileAction.fetchMorePlaylists, { skipStartedAction: true })(new GuildFilesUseCase().fetchMorePlaylists), payload)
    });
}