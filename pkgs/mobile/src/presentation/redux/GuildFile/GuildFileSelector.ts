import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';

export const guildFilesRecentPageStatesSelector = (state: RootState) => state.guildFile.pages.recents;
export const guildFilesRecentPageStateSelector = createSelector(
    guildFilesRecentPageStatesSelector,
    (_: RootState, guildId: string) => guildId,
    (pages, guildId) => pages.find(p => p.get('guildId') == guildId),
);
export const guildFilesPageStatesSelector = (state: RootState) => state.guildFile.pages.index;
export const guildFilesPageStateSelector = createSelector(
    guildFilesPageStatesSelector,
    (_: RootState, guildId: string) => guildId,
    (pages, guildId) => pages.find(p => p.get('guildId') == guildId),
);
export const guildFileAlbumsPageStatesSelector = (state: RootState) => state.guildFile.pages.albums;
export const guildFileAlbumsPageStateSelector = createSelector(
    guildFileAlbumsPageStatesSelector,
    (_: RootState, guildId: string, roomId?: string) => { return { guildId, roomId } },
    (pages, { guildId, roomId }: { guildId: string, roomId?: string }) => pages.find(p => p.get('guildId') == guildId && p.get('roomId') == roomId),
);
export const guildFilePlaylistsPageStatesSelector = (state: RootState) => state.guildFile.pages.playlists;
export const guildFilePlaylistsPageStateSelector = createSelector(
    guildFilePlaylistsPageStatesSelector,
    (_: RootState, guildId: string, roomId?: string) => { return { guildId, roomId } },
    (pages, { guildId, roomId }: { guildId: string, roomId?: string }) => pages.find(p => p.get('guildId') == guildId && p.get('roomId') == roomId),
);