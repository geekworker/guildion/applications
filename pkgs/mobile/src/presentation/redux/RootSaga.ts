import { all, fork } from 'redux-saga/effects';
import { appSaga } from '@/presentation/redux/App';
import { imageSaga } from '@/presentation/redux/Image';
import { deviceSaga } from '@/presentation/redux/Device';
import { accountSaga } from '@/presentation/redux/Account';
import { fileSaga } from '@/presentation/redux/File';
import { connectSaga } from '@/presentation/redux/Connect';
import { guildSaga } from '@/presentation/redux/Guild';
import { roomSaga } from '@/presentation/redux/Room';
import { roomLogSaga } from '@/presentation/redux/RoomLog';
import { roomMemberSaga } from '@/presentation/redux/RoomMember';
import { messageSaga } from '@/presentation/redux/Message';
import { postSaga } from '@/presentation/redux/Post';
import { guildFileSaga } from '@/presentation/redux/GuildFile';
import { roomFileSaga } from '@/presentation/redux/RoomFile';
import { discoverySaga } from '@/presentation/redux/Discovery';

export const rootSaga = function* root() {
    yield all([
        fork(appSaga),
        fork(imageSaga),
        fork(deviceSaga),
        fork(accountSaga),
        fork(fileSaga),
        fork(connectSaga),
        fork(guildSaga),
        fork(roomSaga),
        fork(roomLogSaga),
        fork(roomMemberSaga),
        fork(messageSaga),
        fork(postSaga), 
        fork(guildFileSaga),
        fork(roomFileSaga),
        fork(discoverySaga),
    ]);
};
