import { File, Files } from '@guildion/core';
import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';

export const systemFilesSelector = (state: RootState) => state.file.systemFiles;

export const userProfilesSelector = (state: RootState) => state.file.systemFiles?.userProfiles ? state.file.systemFiles.userProfiles : new Files([]);

export const randomUserProfileSelector = createSelector(
    userProfilesSelector,
    (profiles) => profiles.generateUserProfile()
);