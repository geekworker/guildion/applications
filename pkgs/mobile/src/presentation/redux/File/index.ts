import fileReducer from './FileReducer';
import fileSaga from './FileSaga';

export { fileReducer, fileSaga };
