import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Files } from '@guildion/core';

const actionCreator = actionCreatorFactory('File');

export const FileAction = {
    fetchSystemFiles: actionCreator.async<{}, { files: Files }>('FETCH_SYSTEM_FILES'),
    setSystemFiles: actionCreator<Files>('SET_SYSTEM_FILES'),
};

export type FileAction = typeof FileAction[keyof typeof FileAction];

export class FileState extends Record<{
    systemFiles?: Files,
}>({
    systemFiles: undefined
}) {}

const FileReducer = reducerWithInitialState(new FileState())
    .case(FileAction.fetchSystemFiles.done, (state, payload) => {
        state = state.set('systemFiles', payload.result.files);
        return state;
    })
    .case(FileAction.setSystemFiles, (state, payload) => {
        state = state.set('systemFiles', payload);
        return state;
    })

export default FileReducer;