import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { FileAction } from './FileReducer';
import FilesUseCase from '@/domain/usecases/FilesUseCase';

export default function* FileSaga() {
    yield takeEvery(FileAction.fetchSystemFiles.started, function*({ payload }) {
        yield call(bindAsyncAction(FileAction.fetchSystemFiles, { skipStartedAction: true })(new FilesUseCase().fetchSystemFiles), payload)
    })
}