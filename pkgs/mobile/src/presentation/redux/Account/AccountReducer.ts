import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Map, Record } from 'immutable';
import { Account, ImmutableMap, User } from '@guildion/core';

const actionCreator = actionCreatorFactory('Account');

export interface AccountNewPageStateAttributes { user: User, loading?: boolean, success?: boolean };
export type AccountNewPageState = ImmutableMap<AccountNewPageStateAttributes>
export interface AccountAuthenticatePageStateAttributes { username?: string, loading?: boolean, success?: boolean, shouldTwoFactorAuth?: boolean };
export type AccountAuthenticatePageState = ImmutableMap<AccountAuthenticatePageStateAttributes>
export interface AccountVerifyPageStateAttributes { username?: string, loading?: boolean, success?: boolean };
export type AccountVerifyPageState = ImmutableMap<AccountVerifyPageStateAttributes>
export interface AccountLoadingPageStateAttributes { loading?: boolean, success?: boolean };
export type AccountLoadingPageState = ImmutableMap<AccountLoadingPageStateAttributes>

export const AccountAction = {
    check: actionCreator.async<{}, { account?: Account }>('ACCOUNT_CHECK'),
    register: actionCreator.async<{ user: User }, { account: Account, accessToken?: string }>('ACCOUNT_REGISTER'),
    verify: actionCreator.async<{ username: string }, { account?: Account, accessToken?: string }>('ACCOUNT_VERIFY'),
    authenticate: actionCreator.async<{ username: string }, { account?: Account, accessToken?: string, shouldTwoFactorAuth: boolean }>('ACCOUNT_AUTHENTICATE'),
    setAccount: actionCreator<{ account?: Account }>('SET_ACCOUNT'),
    setAccessToken: actionCreator<{ accessToken?: string }>('SET_ACCESS_TOKEN'),
    setNewState: actionCreator<Partial<AccountNewPageStateAttributes>>('SET_ACCOUNT_NEW_STATE'),
    resetNewState: actionCreator<{}>('RESET_ACCOUNT_NEW_STATE'),
    setAuthenticateState: actionCreator<Partial<AccountAuthenticatePageStateAttributes>>('SET_ACCOUNT_AUTHENTICATE_STATE'),
    resetAuthenticateState: actionCreator<{}>('RESET_ACCOUNT_AUTHENTICATE_STATE'),
    setVerifyState: actionCreator<Partial<AccountVerifyPageStateAttributes>>('SET_ACCOUNT_VERIFY_STATE'),
    resetVerifyState: actionCreator<{}>('RESET_ACCOUNT_VERIFY_STATE'),
    setLoadingPageState: actionCreator<Partial<AccountLoadingPageStateAttributes>>('SET_ACCOUNT_LOADING_PAGE_STATE'),
    resetLoadingPageState: actionCreator<{}>('RESET_ACCOUNT_LOADING_PAGE_STATE'),
};

export type AccountAction = typeof AccountAction[keyof typeof AccountAction];

export class AccountState extends Record<{
    currentAccount?: Account,
    accessToken?: string,
    hasAccountChecked: boolean,
    pages: {
        new: AccountNewPageState,
        authenticate: AccountAuthenticatePageState,
        verify: AccountVerifyPageState,
        loading: AccountLoadingPageState,
    },
}>({
    currentAccount: undefined,
    accessToken: undefined,
    hasAccountChecked: false,
    pages: {
        new: Map({ user: new User() }),
        authenticate: Map({}),
        verify: Map({}),
        loading: Map({}),
    }
}) {}

const AccountReducer = reducerWithInitialState(new AccountState())
    .case(AccountAction.check.done, (state, payload) => {
        state = state.set('currentAccount', payload.result.account);
        state = state.set('hasAccountChecked', true);
        return state;
    })
    .case(AccountAction.register.started, (state, payload) => {
        let pageState = state.pages.new;
        pageState = pageState.set('loading', true);
        pageState = pageState.set('success', false);
        state = state.setIn(['pages', 'new'], pageState);
        return state;
    })
    .case(AccountAction.register.done, (state, payload) => {
        let pageState = state.pages.new;
        state = state.set('currentAccount', payload.result.account);
        state = state.set('hasAccountChecked', !!payload.result.account);
        pageState = pageState.set('loading', false);
        pageState = pageState.set('success', true);
        state = state.set('accessToken', payload.result.accessToken);
        state = state.setIn(['pages', 'new'], pageState);
        return state;
    })
    .case(AccountAction.register.failed, (state, payload) => {
        let pageState = state.pages.new;
        pageState = pageState.set('loading', false);
        state = state.setIn(['pages', 'new'], pageState);
        return state;
    })
    .case(AccountAction.authenticate.started, (state, payload) => {
        let pageState = state.pages.authenticate;
        pageState = pageState.set('loading', true);
        pageState = pageState.set('success', false);
        state = state.setIn(['pages', 'authenticate'], pageState);
        return state;
    })
    .case(AccountAction.authenticate.done, (state, payload) => {
        let pageState = state.pages.authenticate;
        state = state.set('currentAccount', payload.result.account);
        state = state.set('hasAccountChecked', !payload.result.shouldTwoFactorAuth);
        pageState = pageState.set('loading', false);
        pageState = pageState.set('success', true);
        pageState = pageState.set('shouldTwoFactorAuth', payload.result.shouldTwoFactorAuth);
        if (payload.result.shouldTwoFactorAuth) {
            let verifyPageState = state.pages.verify;
            verifyPageState = verifyPageState.set('username', payload.params.username)
            state = state.setIn(['pages', 'verify'], verifyPageState);
        };
        state = state.set('accessToken', payload.result.accessToken);
        state = state.setIn(['pages', 'authenticate'], pageState);
        return state;
    })
    .case(AccountAction.authenticate.failed, (state, payload) => {
        let pageState = state.pages.authenticate;
        pageState = pageState.set('loading', false);
        state = state.setIn(['pages', 'authenticate'], pageState);
        return state;
    })
    .case(AccountAction.verify.started, (state, payload) => {
        let pageState = state.pages.verify;
        pageState = pageState.set('loading', true);
        pageState = pageState.set('success', false);
        state = state.setIn(['pages', 'verify'], pageState);
        return state;
    })
    .case(AccountAction.verify.done, (state, payload) => {
        let pageState = state.pages.verify;
        pageState = pageState.set('loading', false);
        pageState = pageState.set('success', !!payload.result.account);
        state = state.set('currentAccount', payload.result.account);
        state = state.set('hasAccountChecked', !!payload.result.account);
        state = state.set('accessToken', payload.result.accessToken);
        state = state.setIn(['pages', 'verify'], pageState);
        return state;
    })
    .case(AccountAction.verify.failed, (state, payload) => {
        let pageState = state.pages.verify;
        pageState = pageState.set('loading', false);
        pageState = pageState.set('success', true);
        state = state.setIn(['pages', 'verify'], pageState);
        return state;
    })
    .case(AccountAction.setAccount, (state, payload) => {
        state = state.set('currentAccount', payload.account);
        state = state.set('hasAccountChecked', true);
        return state;
    })
    .case(AccountAction.setAccessToken, (state, payload) => {
        state = state.set('accessToken', payload.accessToken);
        return state;
    })
    .case(AccountAction.setNewState, (state, payload) => {
        let pageState = state.pages.new;
        if ("user" in payload && payload.user) pageState = pageState.set('user', payload.user);
        if ("loading" in payload) pageState = pageState.set('loading', payload.loading);
        if ("success" in payload) pageState = pageState.set('success', payload.success);
        state = state.setIn(['pages', 'new'], pageState);
        return state;
    })
    .case(AccountAction.resetNewState, (state) => {
        state = state.setIn(['pages', 'new'], Map({ user: new User() }));
        return state;
    })
    .case(AccountAction.setAuthenticateState, (state, payload) => {
        let pageState = state.pages.authenticate;
        if ("username" in payload) pageState = pageState.set('username', payload.username);
        if ("loading" in payload) pageState = pageState.set('loading', payload.loading);
        if ("shouldTwoFactorAuth" in payload) pageState = pageState.set('shouldTwoFactorAuth', payload.shouldTwoFactorAuth);
        if ("success" in payload) pageState = pageState.set('success', payload.success);
        state = state.setIn(['pages', 'authenticate'], pageState);
        return state;
    })
    .case(AccountAction.resetAuthenticateState, (state) => {
        state = state.setIn(['pages', 'authenticate'],  Map({}));
        return state;
    })
    .case(AccountAction.setVerifyState, (state, payload) => {
        let pageState = state.pages.verify;
        if ("username" in payload) pageState = pageState.set('username', payload.username);
        if ("loading" in payload) pageState = pageState.set('loading', payload.loading);
        if ("success" in payload) pageState = pageState.set('success', payload.success);
        state = state.setIn(['pages', 'verify'], pageState);
        return state;
    })
    .case(AccountAction.resetVerifyState, (state) => {
        state = state.setIn(['pages', 'verify'],  Map({}));
        return state;
    })
    .case(AccountAction.setLoadingPageState, (state, payload) => {
        let pageState = state.pages.loading;
        if ("loading" in payload) pageState = pageState.set('loading', payload.loading);
        if ("success" in payload) pageState = pageState.set('success', payload.success);
        state = state.setIn(['pages', 'loading'], pageState);
        return state;
    })
    .case(AccountAction.resetLoadingPageState, (state) => {
        state = state.setIn(['pages', 'loading'],  Map({}));
        return state;
    })

export default AccountReducer;