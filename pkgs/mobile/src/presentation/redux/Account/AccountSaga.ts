import AccountsUseCase from '@/domain/usecases/AccountsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { AccountAction } from './AccountReducer';

export default function* AccountSaga() {
    yield takeEvery(AccountAction.check.started, function*({ payload }) {
        yield call(bindAsyncAction(AccountAction.check, { skipStartedAction: true })(new AccountsUseCase().check), payload)
    });
    yield takeEvery(AccountAction.register.started, function*({ payload }) {
        yield call(bindAsyncAction(AccountAction.register, { skipStartedAction: true })(new AccountsUseCase().register), payload)
    });
    yield takeEvery(AccountAction.authenticate.started, function*({ payload }) {
        yield call(bindAsyncAction(AccountAction.authenticate, { skipStartedAction: true })(new AccountsUseCase().authenticate), payload)
    });
    yield takeEvery(AccountAction.verify.started, function*({ payload }) {
        yield call(bindAsyncAction(AccountAction.verify, { skipStartedAction: true })(new AccountsUseCase().verify), payload)
    });
}