import { RootState } from '../RootReducer';

export const accessTokenSelector = (state: RootState) => state.account.accessToken;
export const currentAccountSelector = (state: RootState) => state.account.currentAccount;