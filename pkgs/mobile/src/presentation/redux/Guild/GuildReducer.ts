import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Record, Map } from 'immutable';
import { Guild, GuildAttributes, Guilds, ImmutableMap } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const actionCreator = actionCreatorFactory('Guild');

export interface GuildsPageStateAttributes { guilds: Guilds, loading?: boolean, paginatable?: boolean, success?: boolean };
export type GuildsPageState = ImmutableMap<GuildsPageStateAttributes>;
export interface GuildPageStateAttributes { guild: Guild, loading?: boolean, success?: boolean, isPublic: boolean };
export type GuildPageState = ImmutableMap<GuildPageStateAttributes>;
export type GuildPageStates = List<GuildPageState>;
export interface SearchGuildsPageStateAttributes { query: string, guilds: Guilds, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus };
export type SearchGuildsPageState = ImmutableMap<SearchGuildsPageStateAttributes>;
export type SearchGuildPageStates = List<SearchGuildsPageState>;

export const GuildAction = {
    fetchIndex: actionCreator.async<{}, { guilds: Guilds, paginatable: boolean }>('GUILD_FETCH_HOME'),
    fetchShow: actionCreator.async<{ id: string, isPublic?: boolean }, { guild: Guild }>('GUILD_FETCH_SHOW'),
    setIndexState: actionCreator<Partial<GuildsPageStateAttributes>>('GUILD_SET_INDEX_STATE'),
    resetIndexState: actionCreator<{}>('GUILD_RESET_INDEX_STATE'),
    search: actionCreator.async<{ query: string }, { guilds: Guilds, paginatable: boolean }>('GUILD_SEARCH'),
    searchMore: actionCreator.async<{ query: string }, { guilds: Guilds, paginatable: boolean }>('GUILD_SEARCH_MORE'),
};

export type GuildAction = typeof GuildAction[keyof typeof GuildAction];

export class GuildState extends Record<{
    pages: {
        index: GuildsPageState,
        show: GuildPageStates,
        search: SearchGuildPageStates,
    },
}>({
    pages: {
        index: Map({ guilds: new Guilds([]) }),
        show: List([]),
        search: List([]),
    },
}) {
}

const GuildReducer = reducerWithInitialState(new GuildState())
    .case(GuildAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        pageState = pageState.set('loading', pageState.get('loading') == undefined ? true : false);
        pageState = pageState.set('success', false);
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        pageState = pageState.set('loading', false);
        pageState = pageState.set('success', true);
        pageState = pageState.set('guilds', payload.result.guilds);
        pageState = pageState.set('paginatable', payload.result.paginatable);
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        pageState = pageState.set('loading', false);
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('guild').id == payload.id && p.get('isPublic') == !!payload.isPublic);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ guild: new Guild({ id: payload.id }), loading: true, success: false, isPublic: !!payload.isPublic }));
        } else {
            // targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(GuildAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('guild').id == payload.params.id && p.get('isPublic') == !!payload.params.isPublic);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ guild: payload.result.guild, loading: false, success: true, isPublic: !!payload.params.isPublic }));
        } else {
            targetPage = targetPage.set('guild', payload.result.guild);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(GuildAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('guild').id == payload.params.id && p.get('isPublic') == !!payload.params.isPublic);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ guild: new Guild({ id: payload.params.id }), loading: false, success: false, isPublic: !!payload.params.isPublic }));
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(GuildAction.setIndexState, (state, payload) => {
        let pageState = state.pages.index;
        if ("guilds" in payload && payload.guilds) pageState = pageState.set('guilds', payload.guilds);
        if ("paginatable" in payload) pageState = pageState.set('paginatable', payload.paginatable);
        if ("loading" in payload) pageState = pageState.set('loading', payload.loading);
        if ("success" in payload) pageState = pageState.set('success', payload.success);
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(GuildAction.resetIndexState, (state, payload) => {
        state = state.setIn(['pages', 'index'], Map());
        return state;
    })
    .case(GuildAction.search.started, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.query,
                    guilds: new Guilds([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(GuildAction.search.done, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    guilds: payload.result.guilds,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('guilds', payload.result.guilds);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(GuildAction.search.failed, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    guilds: new Guilds([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('guilds', new Guilds([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(GuildAction.searchMore.started, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.query,
                    guilds: new Guilds([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.DECREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.DECREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(GuildAction.searchMore.done, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    guilds: payload.result.guilds,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('guilds', targetPage.get('guilds').concat(payload.result.guilds));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(GuildAction.searchMore.failed, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    guilds: new Guilds([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })

export default GuildReducer;