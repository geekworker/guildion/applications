import GuildsUseCase from '@/domain/usecases/GuildsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { GuildAction } from './GuildReducer';

export default function* GuildSaga() {
    yield takeEvery(GuildAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildAction.fetchIndex, { skipStartedAction: true })(new GuildsUseCase().fetchIndex), payload)
    });
    yield takeEvery(GuildAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildAction.fetchShow, { skipStartedAction: true })(new GuildsUseCase().fetchShow), payload)
    });
    yield takeEvery(GuildAction.search.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildAction.search, { skipStartedAction: true })(new GuildsUseCase().search), payload)
    });
    yield takeEvery(GuildAction.searchMore.started, function*({ payload }) {
        yield call(bindAsyncAction(GuildAction.searchMore, { skipStartedAction: true })(new GuildsUseCase().searchMore), payload)
    });
}