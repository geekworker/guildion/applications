import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';
import { GuildPageStates } from './GuildReducer';

export const guildShowPageStatesSelector = (state: RootState) => state.guild.pages.show;
export const guildShowSelector = createSelector(
    guildShowPageStatesSelector,
    (_: RootState, id: string) => id,
    (show: GuildPageStates, id: string) => show.find(p => p.get('guild').id == id)?.get('guild'),
);
export const guildShowPageSelector = createSelector(
    guildShowPageStatesSelector,
    (_: RootState, { id, isPublic }: {id: string, isPublic?: boolean }) => { return { id, isPublic } },
    (show: GuildPageStates, { id, isPublic }: {id: string, isPublic?: boolean }) => show.find(p => p.get('guild').id == id && p.get('isPublic') == isPublic),
);
export const guildsSearchPageStatesSelector = (state: RootState) => state.guild.pages.search;
export const guildsSearchPageStateSelector = createSelector(
    guildsSearchPageStatesSelector,
    (_: RootState, query: string) => query,
    (pages, query) => pages.find(p => p.get('query') == query),
);