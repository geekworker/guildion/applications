import { combineReducers, Reducer } from 'redux';
import appReducer, { AppAction, AppState } from '@/presentation/redux/App/AppReducer';
import imageReducer, { ImageAction, ImageState } from '@/presentation/redux/Image/ImageReducer';
import deviceReducer, { DeviceAction, DeviceState } from '@/presentation/redux/Device/DeviceReducer';
import accountReducer, { AccountAction, AccountState } from '@/presentation/redux/Account/AccountReducer';
import fileReducer, { FileAction, FileState } from '@/presentation/redux/File/FileReducer';
import connectReducer, { ConnectAction, ConnectState } from '@/presentation/redux/Connect/ConnectReducer';
import guildReducer, { GuildAction, GuildState } from '@/presentation/redux/Guild/GuildReducer';
import roomReducer, { RoomAction, RoomState } from '@/presentation/redux/Room/RoomReducer';
import roomLogReducer, { RoomLogAction, RoomLogState } from '@/presentation/redux/RoomLog/RoomLogReducer';
import roomMemberReducer, { RoomMemberAction, RoomMemberState } from '@/presentation/redux/RoomMember/RoomMemberReducer';
import messageReducer, { MessageState, MessageAction } from '@/presentation/redux/Message/MessageReducer';
import { GuildFileAction, GuildFileState } from './GuildFile/GuildFileReducer';
import { RoomFileAction, RoomFileState } from './RoomFile/RoomFileReducer';
import { guildFileReducer } from './GuildFile';
import { roomFileReducer } from './RoomFile';
import { PostAction, PostState } from './Post/PostReducer';
import { postReducer } from './Post';
import { DiscoveryAction, DiscoveryState } from './Discovery/DiscoveryReducer';
import { discoveryReducer } from './Discovery';

export interface RootState {
    app: AppState,
    image: ImageState,
    device: DeviceState,
    account: AccountState,
    file: FileState,
    connect: ConnectState,
    guild: GuildState,
    room: RoomState,
    roomLog: RoomLogState,
    roomMember: RoomMemberState,
    message: MessageState,
    post: PostState,
    guildFile: GuildFileState,
    roomFile: RoomFileState,
    discovery: DiscoveryState,
}

export type RootAction = AppAction | DeviceAction | AccountAction | ImageAction | FileAction | ConnectAction | GuildAction | RoomAction | RoomLogAction | RoomMemberAction | MessageAction | PostAction | GuildFileAction | RoomFileAction | DiscoveryAction;

export const rootReducer = (): Reducer => {
    return combineReducers({
        app: appReducer,
        image: imageReducer,
        device: deviceReducer,
        account: accountReducer,
        file: fileReducer,
        connect: connectReducer,
        guild: guildReducer,
        room: roomReducer,
        roomLog: roomLogReducer,
        roomMember: roomMemberReducer,
        message: messageReducer,
        post: postReducer,
        guildFile: guildFileReducer,
        roomFile: roomFileReducer,
        discovery: discoveryReducer,
    });
}

export const initRootState = (): Partial<RootState> => {
    return {
        app: new AppState(),
        image: new ImageState(),
        device: new DeviceState(),
        account: new AccountState(),
        file: new FileState(),
        connect: new ConnectState(),
        guild: new GuildState(),
        room: new RoomState(),
        roomLog: new RoomLogState(),
        roomMember: new RoomMemberState(),
        message: new MessageState(),
        post: new PostState(),
        guildFile: new GuildFileState(),
        roomFile: new RoomFileState(),
        discovery: new DiscoveryState(),
    };
};