import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { ImmutableMap, Message, Messages, MessageStatus } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import uuid from "react-native-uuid";

const actionCreator = actionCreatorFactory('Message');

export interface MessagesPageStateAttributes {
    roomId: string,
    messages: Messages,
    newMessage: Message,
    loading?: boolean,
    loadingMoreStatus?: LoadingMoreStatus,
    incrementable?: boolean,
    decrementable?: boolean,
    success?: boolean,
    basedAt?: Date,
    submitting?: boolean,
};
export type MessagesPageState = ImmutableMap<MessagesPageStateAttributes>;
export type RoomMessagesPageState = List<MessagesPageState>;

export const MessageAction = {
    fetchIndex: actionCreator.async<{ roomId: string }, { basedAt: Date, messages: Messages, paginatable: boolean }>('MESSAGE_FETCH_INDEX'),
    incrementIndex: actionCreator.async<{ roomId: string }, { messages: Messages, paginatable: boolean }>('MESSAGE_INCREMENT_INDEX'),
    decrementIndex: actionCreator.async<{ roomId: string }, { messages: Messages, paginatable: boolean }>('MESSAGE_DECREMENT_INDEX'),
    create: actionCreator.async<{ message: Message }, { message: Message }>('MESSAGE_CREATE'),
    update: actionCreator.async<{ message: Message }, { message: Message }>('MESSAGE_UPDATE'),
    destroy: actionCreator.async<{ id: string }, { success: boolean }>('MESSAGE_DESTROY'),
};

export type MessageAction = typeof MessageAction[keyof typeof MessageAction];

export class MessageState extends Record<{
    pages: {
        index: RoomMessagesPageState,
    },
}>({
    pages: {
        index: List([]),
    },
}) {}

const MessageReducer = reducerWithInitialState(new MessageState())
    .case(MessageAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    messages: new Messages([]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.roomId }),
                    loading: true,
                    loadingMoreStatus: undefined,
                    incrementable: false,
                    decrementable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('incrementable', false);
            targetPage = targetPage.set('decrementable', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    messages: payload.result.messages,
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.params.roomId }),
                    loading: false,
                    loadingMoreStatus: undefined,
                    incrementable: false,
                    decrementable: payload.result.paginatable,
                    success: true,
                    basedAt: payload.result.basedAt,
                })
            );
        } else {
            targetPage = targetPage.set('messages', payload.result.messages);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('incrementable', false);
            targetPage = targetPage.set('decrementable', payload.result.paginatable);
            targetPage = targetPage.set('basedAt', payload.result.basedAt);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    messages: new Messages([]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.params.roomId }),
                    loading: false,
                    loadingMoreStatus: undefined,
                    incrementable: false,
                    decrementable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('messages', new Messages([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('incrementable', false);
            targetPage = targetPage.set('decrementable', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.incrementIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    messages: new Messages([]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.roomId }),
                    loading: true,
                    loadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    incrementable: false,
                    decrementable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.incrementIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    messages: payload.result.messages,
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.params.roomId }),
                    loading: false,
                    loadingMoreStatus: undefined,
                    incrementable: payload.result.paginatable,
                    decrementable: true,
                    success: true,
                    basedAt: new Date(),
                })
            );
        } else {
            targetPage = targetPage.set('messages', targetPage.get('messages').concat(payload.result.messages));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('incrementable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.incrementIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    messages: new Messages([]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.params.roomId }),
                    loading: false,
                    loadingMoreStatus: undefined,
                    incrementable: false,
                    decrementable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('incrementable', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.decrementIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    messages: new Messages([]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.roomId }),
                    loading: true,
                    loadingMoreStatus: LoadingMoreStatus.DECREMENT,
                    incrementable: false,
                    decrementable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.DECREMENT);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.decrementIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    messages: payload.result.messages,
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.params.roomId }),
                    loading: false,
                    loadingMoreStatus: undefined,
                    incrementable: true,
                    decrementable: payload.result.paginatable,
                    success: true,
                    basedAt: new Date(),
                })
            );
        } else {
            targetPage = targetPage.set('messages', payload.result.messages.concat(targetPage.get('messages')));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('decrementable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.decrementIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    messages: new Messages([]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId: payload.params.roomId }),
                    loading: false,
                    loadingMoreStatus: undefined,
                    incrementable: false,
                    decrementable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('decrementable', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.create.started, (state, payload) => {
        let pageState = state.pages.index;
        const roomId = payload.message.roomId;
        if (!roomId) return state;
        const index = pageState.findIndex(p => p.get('roomId') == roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: roomId,
                    messages: new Messages([payload.message]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId }),
                    subumitting: true,
                })
            );
        } else {
            targetPage = targetPage.set('subumitting', true);
            // targetPage = targetPage.set('messages', new Messages([...targetPage.get('messages'), payload.message]));
            targetPage = targetPage.set('newMessage', new Message({ id: `${uuid.v4()}`, roomId }));
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.create.done, (state, payload) => {
        let pageState = state.pages.index;
        const roomId = payload.result.message.roomId;
        if (!roomId) return state;
        const index = pageState.findIndex(p => p.get('roomId') == roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: roomId,
                    messages: new Messages([payload.result.message]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId }),
                    submitting: false,
                })
            );
        } else {
            const index = targetPage.get('messages').findIndex(m => m.id == payload.result.message.id);
            if (index < 0) {
                targetPage = targetPage.set('messages', targetPage.get('messages').concat(new Messages([payload.result.message])));
            } else {
                targetPage = targetPage.set('messages', targetPage.get('messages').set(index, payload.result.message));
            };
            targetPage = targetPage.set('submitting', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(MessageAction.create.failed, (state, payload) => {
        let pageState = state.pages.index;
        const roomId = payload.params.message.roomId;
        if (!roomId) return state;
        const index = pageState.findIndex(p => p.get('roomId') == roomId);
        const message = payload.params.message.set('status', MessageStatus.FAILED);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: roomId,
                    messages: new Messages([message]),
                    newMessage: new Message({ id: `${uuid.v4()}`, roomId }),
                    submitting: false,
                })
            );
        } else {
            const index = targetPage.get('messages').findIndex(m => m.id == message.id);
            if (index < 0) {
                targetPage = targetPage.set('messages', targetPage.get('messages').concat(new Messages([payload.params.message])));
            } else {
                targetPage = targetPage.set('messages', targetPage.get('messages').set(index, message));
            }
            targetPage = targetPage.set('submitting', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })

export default MessageReducer;