import MessagesUseCase from '@/domain/usecases/MessagesUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { MessageAction } from './MessageReducer';

export default function* MessageSaga() {
    yield takeEvery(MessageAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(MessageAction.fetchIndex, { skipStartedAction: true })(new MessagesUseCase().fetchIndex), payload)
    })
    yield takeEvery(MessageAction.incrementIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(MessageAction.incrementIndex, { skipStartedAction: true })(new MessagesUseCase().incrementIndex), payload)
    })
    yield takeEvery(MessageAction.decrementIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(MessageAction.decrementIndex, { skipStartedAction: true })(new MessagesUseCase().decrementIndex), payload)
    })
    yield takeEvery(MessageAction.create.started, function*({ payload }) {
        yield call(bindAsyncAction(MessageAction.create, { skipStartedAction: true })(new MessagesUseCase().create), payload)
    })
    yield takeEvery(MessageAction.destroy.started, function*({ payload }) {
        yield call(bindAsyncAction(MessageAction.destroy, { skipStartedAction: true })(new MessagesUseCase().destroy), payload)
    })
}