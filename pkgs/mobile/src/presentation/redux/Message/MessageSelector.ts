import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';

export const roomMessagesPageStatesSelector = (state: RootState) => state.message.pages.index;
export const roomMessagesPageStateSelector = createSelector(
    roomMessagesPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (index, roomId) => index.find(p => p.get('roomId') == roomId),
);