import { applyMiddleware, createStore, Store } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { rootReducer, RootState } from './RootReducer';
import { rootSaga } from './RootSaga';
import { composeWithDevTools } from 'redux-devtools-extension';
import { setStore } from '@/infrastructure/Store';

export function configureStore(arg: { 
    preloadedState?: Partial<RootState>,
    logger: boolean,
}): Store<RootState> {
    const logger = createLogger();
    const sagaMiddleware = createSagaMiddleware();
    const middlewares: any[] = [sagaMiddleware];
    if (arg.logger) { middlewares.push(logger) };
    const middlewareEnhancer = applyMiddleware(...middlewares);
    const composeEnhancers = composeWithDevTools({});
    const store = createStore(rootReducer(), arg.preloadedState, __DEV__ ? composeEnhancers(middlewareEnhancer) : middlewareEnhancer);
    sagaMiddleware.run(rootSaga);
    return store;
}

setStore(
    configureStore({
        preloadedState: {},
        logger: false
    })
);