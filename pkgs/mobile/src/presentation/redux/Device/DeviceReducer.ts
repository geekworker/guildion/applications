import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';
import { Device } from '@guildion/core';

const actionCreator = actionCreatorFactory('Device');

export const DeviceAction = {
    register: actionCreator.async<{}, { device: Device, csrfSecret: string }>('REGISTER'),
    setDevice: actionCreator<{ device: Device, csrfSecret: string }>('SET_DEVICE'),
};

export type DeviceAction = typeof DeviceAction[keyof typeof DeviceAction];

export class DeviceState extends Record<{
    currentDevice?: Device,
    csrfSecret?: string,
    deviceChecked: boolean,
}>({
    currentDevice: undefined,
    csrfSecret: undefined,
    deviceChecked: false,
}) {}

const DeviceReducer = reducerWithInitialState(new DeviceState())
    .case(DeviceAction.register.done, (state, payload) => {
        state = state.set('currentDevice', payload.result.device);
        state = state.set('csrfSecret', payload.result.csrfSecret);
        state = state.set('deviceChecked', true);
        return state;
    })
    .case(DeviceAction.setDevice, (state, payload) => {
        state = state.set('currentDevice', payload.device);
        state = state.set('csrfSecret', payload.csrfSecret);
        state = state.set('deviceChecked', true);
        return state;
    })

export default DeviceReducer;