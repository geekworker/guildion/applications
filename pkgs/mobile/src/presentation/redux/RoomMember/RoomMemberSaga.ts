import RoomMembersUseCase from '@/domain/usecases/RoomMembersUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { RoomMemberAction } from './RoomMemberReducer';

export default function* RoomMemberSaga() {
    yield takeEvery(RoomMemberAction.fetchConnectingIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomMemberAction.fetchConnectingIndex, { skipStartedAction: true })(new RoomMembersUseCase().fetchConnectingIndex), payload)
    })
}