import { createSelector } from "reselect";
import { RootState } from "../RootReducer";

export const roomMembersPageStatesSelector = (state: RootState) => state.roomMember.pages.index;
export const roomMembersPageStateSelector = createSelector(
    roomMembersPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (index, roomId) => index.find(p => p.get('roomId') == roomId),
);

export const roomConnectingMembersPageStatesSelector = (state: RootState) => state.roomMember.pages.connectings;
export const roomConnectingMembersPageStateSelector = createSelector(
    roomConnectingMembersPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (index, roomId) => index.find(p => p.get('roomId') == roomId),
);