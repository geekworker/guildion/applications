import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { ImmutableMap, Members, Records } from '@guildion/core';

const actionCreator = actionCreatorFactory('RoomMember');

export interface RoomMembersPageStateAttributes { roomId: string, members: Members, loading?: boolean, paginatable?: boolean, success?: boolean };
export type RoomMembersPageState = ImmutableMap<RoomMembersPageStateAttributes>;
export type RoomMembersPageStates = List<RoomMembersPageState>;
export interface RoomConnectingMembersPageStateAttributes { roomId: string, members: Members, loading?: boolean, paginatable?: boolean, success?: boolean };
export type RoomConnectingMembersPageState = ImmutableMap<RoomConnectingMembersPageStateAttributes>;
export type RoomConnectingMembersPageStates = List<RoomConnectingMembersPageState>

export const RoomMemberAction = {
    fetchIndex: actionCreator.async<{ roomId: string }, { members: Members, paginatable: boolean }>('ROOM_MEMBERS_FETCH_INDEX'),
    fetchConnectingIndex: actionCreator.async<{ roomId: string }, { members: Members, paginatable: boolean }>('ROOM_CONNECTING_MEMBERS_FETCH_INDEX'),
};

export type RoomMemberAction = typeof RoomMemberAction[keyof typeof RoomMemberAction];

export class RoomMemberState extends Record<{
    pages: {
        index: RoomMembersPageStates,
        connectings: RoomConnectingMembersPageStates,
    },
}>({
    pages: {
        index: List([]),
        connectings: List([]),
    },
}) {}

const RoomMemberReducer = reducerWithInitialState(new RoomMemberState())
    .case(RoomMemberAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.roomId, members: new Members([]), loading: true, success: false, paginatable: false }));
        } else {
            targetPage = targetPage.set('members', new Members([]));
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomMemberAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.params.roomId, members: payload.result.members, loading: false, success: true, paginatable: payload.result.paginatable }));
        } else {
            targetPage = targetPage.set('members', payload.result.members);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomMemberAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.params.roomId, members: new Members([]), loading: false, success: false, paginatable: false }));
        } else {
            targetPage = targetPage.set('members', new Members([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomMemberAction.fetchConnectingIndex.started, (state, payload) => {
        let pageState = state.pages.connectings;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.roomId, members: new Members([]), loading: true, success: false, paginatable: false }));
        } else {
            targetPage = targetPage.set('members', new Members([]));
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'connectings'], pageState);
        return state;
    })
    .case(RoomMemberAction.fetchConnectingIndex.done, (state, payload) => {
        let pageState = state.pages.connectings;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.params.roomId, members: payload.result.members, loading: false, success: true, paginatable: payload.result.paginatable }));
        } else {
            targetPage = targetPage.set('members', payload.result.members);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'connectings'], pageState);
        return state;
    })
    .case(RoomMemberAction.fetchConnectingIndex.failed, (state, payload) => {
        let pageState = state.pages.connectings;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.params.roomId, members: new Members([]), loading: false, success: false, paginatable: false }));
        } else {
            targetPage = targetPage.set('members', new Members([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'connectings'], pageState);
        return state;
    })

export default RoomMemberReducer;