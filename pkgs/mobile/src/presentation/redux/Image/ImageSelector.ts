import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';

export const imageCachesSelector = (state: RootState) => state.image.cache;
export const imageCacheSelector = createSelector(
    imageCachesSelector,
    (_: RootState, url: string) => url,
    (cache, url) => cache.find(val => val.url == url)?.base64,
);