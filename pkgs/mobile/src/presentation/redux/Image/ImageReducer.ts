import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Record } from 'immutable';

const actionCreator = actionCreatorFactory('Image');

export const ImageAction = {
    fetch: actionCreator.async<{ url: string }, { base64: string }>('IMAGE_FETCH'),
    setData: actionCreator<{ url: string, base64: string }>('IMAGE_SET_DATA'),
    deleteData: actionCreator<{ url: string }>('IMAGE_DELETE_DATA'),
};

export type ImageAction = typeof ImageAction[keyof typeof ImageAction];

export class ImageState extends Record<{
    cache: List<{ url: string, base64: string }>,
}>({
    cache: List([]),
}) {}

const ImageReducer = reducerWithInitialState(new ImageState())
    .case(ImageAction.fetch.done, (state, payload) => {
        const before = state.cache.filter(val => val.url != payload.params.url);
        const result = before.push({ url: payload.params.url, base64: payload.result.base64 });
        state = state.set('cache', result);
        return state;
    })
    .case(ImageAction.setData, (state, payload) => {
        const before = state.cache.filter(val => val.url != payload.url);
        const result = before.push({ url: payload.url, base64: payload.base64 });
        state = state.set('cache', result);
        return state;
    })
    .case(ImageAction.deleteData, (state, payload) => {
        const before = state.cache.filter(val => val.url != payload.url);
        state = state.set('cache', before);
        return state;
    })

export default ImageReducer;