import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { ImageAction } from './ImageReducer';
import ImagesUseCase from '@/domain/usecases/ImagesUseCase';

export default function* ImageSaga() {
    yield takeEvery(ImageAction.fetch.started, function*({ payload }) {
        yield call(bindAsyncAction(ImageAction.fetch, { skipStartedAction: true })(new ImagesUseCase().fetch), payload)
    })
}