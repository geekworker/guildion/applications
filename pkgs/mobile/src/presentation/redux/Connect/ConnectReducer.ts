import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record } from 'immutable';

const actionCreator = actionCreatorFactory('Connect');

export const ConnectAction = {
    connect: actionCreator<void>('CONNECT'),
    disconnect: actionCreator<void>('DISCONNECT'),
};

export type ConnectAction = typeof ConnectAction[keyof typeof ConnectAction];

export class ConnectState extends Record<{
    isConnected: boolean,
}>({
    isConnected: false,
}) {}

const ConnectReducer = reducerWithInitialState(new ConnectState())
    .case(ConnectAction.connect, (state, payload) => {
        state = state.set('isConnected', true);
        return state;
    })
    .case(ConnectAction.disconnect, (state, payload) => {
        state = state.set('isConnected', false);
        return state;
    })

export default ConnectReducer;