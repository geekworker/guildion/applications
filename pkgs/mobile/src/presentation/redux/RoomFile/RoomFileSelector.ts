import { createSelector } from 'reselect';
import { RootState } from '../RootReducer';

export const roomFilesRecentPageStatesSelector = (state: RootState) => state.roomFile.pages.recents;
export const roomFilesRecentPageStateSelector = createSelector(
    roomFilesRecentPageStatesSelector,
    (_: RootState, roomId: string, guildId: string) => { return { roomId, guildId }},
    (pages, { roomId, guildId }: { roomId: string, guildId: string }) => pages.find(p => p.get('roomId') == roomId && p.get('guildId') == guildId),
);
export const roomFilesPageStatesSelector = (state: RootState) => state.roomFile.pages.index;
export const roomFilesPageStateSelector = createSelector(
    roomFilesPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (pages, roomId) => pages.find(p => p.get('roomId') == roomId),
);
export const roomFileAlbumsPageStatesSelector = (state: RootState) => state.roomFile.pages.albums;
export const roomFileAlbumsPageStateSelector = createSelector(
    roomFileAlbumsPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (pages, roomId) => pages.find(p => p.get('roomId') == roomId),
);
export const roomFilePlaylistsPageStatesSelector = (state: RootState) => state.roomFile.pages.playlists;
export const roomFilePlaylistsPageStateSelector = createSelector(
    roomFilePlaylistsPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (pages, roomId) => pages.find(p => p.get('roomId') == roomId),
);