import RoomFilesUseCase from '@/domain/usecases/RoomFilesUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { RoomFileAction } from './RoomFileReducer';

export default function* RoomFileSaga() {
    yield takeEvery(RoomFileAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchIndex, { skipStartedAction: true })(new RoomFilesUseCase().fetchIndex), payload)
    });
    yield takeEvery(RoomFileAction.fetchMoreIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchMoreIndex, { skipStartedAction: true })(new RoomFilesUseCase().fetchMoreIndex), payload)
    });
    yield takeEvery(RoomFileAction.fetchRecentFiles.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchRecentFiles, { skipStartedAction: true })(new RoomFilesUseCase().fetchRecentFiles), payload)
    });
    yield takeEvery(RoomFileAction.fetchMoreRecentFiles.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchMoreRecentFiles, { skipStartedAction: true })(new RoomFilesUseCase().fetchMoreRecentFiles), payload)
    });
    yield takeEvery(RoomFileAction.fetchAlbums.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchAlbums, { skipStartedAction: true })(new RoomFilesUseCase().fetchAlbums), payload)
    });
    yield takeEvery(RoomFileAction.fetchMoreAlbums.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchMoreAlbums, { skipStartedAction: true })(new RoomFilesUseCase().fetchMoreAlbums), payload)
    });
    yield takeEvery(RoomFileAction.fetchPlaylists.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchPlaylists, { skipStartedAction: true })(new RoomFilesUseCase().fetchPlaylists), payload)
    });
    yield takeEvery(RoomFileAction.fetchMorePlaylists.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomFileAction.fetchMorePlaylists, { skipStartedAction: true })(new RoomFilesUseCase().fetchMorePlaylists), payload)
    });
}