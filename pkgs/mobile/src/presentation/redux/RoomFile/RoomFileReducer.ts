import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { Files, ImmutableMap } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const actionCreator = actionCreatorFactory('RoomFile');

export interface RoomRecentFilesPageStateAttributes { guildId: string, roomId: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type RoomRecentFilesPageState = ImmutableMap<RoomRecentFilesPageStateAttributes>;
export type RoomsRecentFilesPageState = List<RoomRecentFilesPageState>;
export interface RoomFilesPageStateAttributes { roomId: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type RoomFilesPageState = ImmutableMap<RoomFilesPageStateAttributes>;
export type RoomsFilesPageState = List<RoomFilesPageState>;
export interface RoomAlbumsPageStateAttributes { roomId: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type RoomAlbumsPageState = ImmutableMap<RoomAlbumsPageStateAttributes>;
export type RoomsAlbumsPageState = List<RoomAlbumsPageState>;
export interface RoomPlaylistsPageStateAttributes { roomId: string, files: Files, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type RoomPlaylistsPageState = ImmutableMap<RoomPlaylistsPageStateAttributes>;
export type RoomsPlaylistsPageState = List<RoomPlaylistsPageState>;

export const RoomFileAction = {
    fetchIndex: actionCreator.async<{ roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_INDEX'),
    fetchMoreIndex: actionCreator.async<{ roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_MORE_INDEX'),
    fetchRecentFiles: actionCreator.async<{ guildId: string, roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_RECENT_INDEX'),
    fetchMoreRecentFiles: actionCreator.async<{ guildId: string, roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_MORE_RECENT_INDEX'),
    fetchAlbums: actionCreator.async<{ roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_ALBUMS'),
    fetchMoreAlbums: actionCreator.async<{ roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_MORE_ALBUMS'),
    fetchPlaylists: actionCreator.async<{ roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_PLAYLISTS'),
    fetchMorePlaylists: actionCreator.async<{ roomId: string }, { files: Files, paginatable: boolean }>('ROOM_FILE_FETCH_MORE_PLAYLISTS'),
};

export type RoomFileAction = typeof RoomFileAction[keyof typeof RoomFileAction];

export class RoomFileState extends Record<{
    pages: {
        index: RoomsFilesPageState,
        recents: RoomsRecentFilesPageState,
        albums: RoomsAlbumsPageState,
        playlists: RoomsPlaylistsPageState,
    },
}>({
    pages: {
        index: List([]),
        recents: List([]),
        albums: List([]),
        playlists: List([]),
    }
}) {}

const RoomFileReducer = reducerWithInitialState(new RoomFileState())
    .case(RoomFileAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchRecentFiles.started, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId && p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchRecentFiles.done, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchRecentFiles.failed, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreRecentFiles.started, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId && p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.guildId,
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreRecentFiles.done, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreRecentFiles.failed, (state, payload) => {
        let pageState = state.pages.recents;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    guildId: payload.params.guildId,
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'recents'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchAlbums.started, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchAlbums.done, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchAlbums.failed, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreAlbums.started, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreAlbums.done, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMoreAlbums.failed, (state, payload) => {
        let pageState = state.pages.albums;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'albums'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchPlaylists.started, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchPlaylists.done, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', payload.result.files);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchPlaylists.failed, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('files', new Files([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMorePlaylists.started, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.roomId,
                    files: new Files([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMorePlaylists.done, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: payload.result.files,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('files', targetPage.get('files').concat(payload.result.files));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })
    .case(RoomFileAction.fetchMorePlaylists.failed, (state, payload) => {
        let pageState = state.pages.playlists;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    roomId: payload.params.roomId,
                    files: new Files([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'playlists'], pageState);
        return state;
    })

export default RoomFileReducer;