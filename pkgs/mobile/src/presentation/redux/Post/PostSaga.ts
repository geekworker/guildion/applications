import PostsUseCase from '@/domain/usecases/PostsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { PostAction } from './PostReducer';

export default function* PostSaga() {
    yield takeEvery(PostAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(PostAction.fetchIndex, { skipStartedAction: true })(new PostsUseCase().fetchIndex), payload)
    });
    yield takeEvery(PostAction.fetchMoreIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(PostAction.fetchMoreIndex, { skipStartedAction: true })(new PostsUseCase().fetchMoreIndex), payload)
    });
    yield takeEvery(PostAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(PostAction.fetchShow, { skipStartedAction: true })(new PostsUseCase().fetchShow), payload)
    });
    yield takeEvery(PostAction.search.started, function*({ payload }) {
        yield call(bindAsyncAction(PostAction.search, { skipStartedAction: true })(new PostsUseCase().search), payload)
    });
    yield takeEvery(PostAction.searchMore.started, function*({ payload }) {
        yield call(bindAsyncAction(PostAction.searchMore, { skipStartedAction: true })(new PostsUseCase().searchMore), payload)
    });
}