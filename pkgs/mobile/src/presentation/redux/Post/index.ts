import postReducer from './PostReducer';
import postSaga from './PostSaga';

export { postReducer, postSaga };
