import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Map, Record } from 'immutable';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';
import { ImmutableMap, Post, Posts } from '@guildion/core';

const actionCreator = actionCreatorFactory('Post');

export interface RoomPostsPageStateAttributes { roomId: string, postId?: string, posts: Posts, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus, isPublic: boolean };
export type RoomPostsPageState = ImmutableMap<RoomPostsPageStateAttributes>;
export type RoomsPostsPageState = List<RoomPostsPageState>;
export interface PostPageStateAttributes { post: Post, loading?: boolean, success?: boolean };
export type PostPageState = ImmutableMap<PostPageStateAttributes>;
export type PostPageStates = List<PostPageState>;
export interface SearchPostsPageStateAttributes { query: string, posts: Posts, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus };
export type SearchPostsPageState = ImmutableMap<SearchPostsPageStateAttributes>;
export type SearchPostPageStates = List<SearchPostsPageState>;

export const PostAction = {
    fetchIndex: actionCreator.async<{ roomId: string, postId?: string, isPublic?: boolean }, { posts: Posts, paginatable: boolean }>('ROOM_POST_FETCH_INDEX'),
    fetchMoreIndex: actionCreator.async<{ roomId: string, postId?: string, isPublic?: boolean }, { posts: Posts, paginatable: boolean }>('ROOM_POST_FETCH_MORE_INDEX'),
    fetchShow: actionCreator.async<{ id: string }, { post: Post }>('POST_FETCH_SHOW'),
    search: actionCreator.async<{ query: string }, { posts: Posts, paginatable: boolean }>('POST_SEARCH'),
    searchMore: actionCreator.async<{ query: string }, { posts: Posts, paginatable: boolean }>('POST_SEARCH_MORE'),
};

export type PostAction = typeof PostAction[keyof typeof PostAction];

export class PostState extends Record<{
    pages: {
        index: RoomsPostsPageState,
        show: PostPageStates,
        search: SearchPostPageStates,
    },
}>({
    pages: {
        index: List([]),
        show: List([]),
        search: List([]),
    },
}) {}

const PostReducer = reducerWithInitialState(new PostState())
    .case(PostAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId && p.get('postId') == payload.postId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    postId: payload.postId,
                    roomId: payload.roomId,
                    posts: new Posts([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(PostAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('postId') == payload.params.postId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    postId: payload.params.postId,
                    roomId: payload.params.roomId,
                    posts: payload.result.posts,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('posts', payload.result.posts);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(PostAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('postId') == payload.params.postId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    postId: payload.params.postId,
                    roomId: payload.params.roomId,
                    posts: new Posts([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('posts', new Posts([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(PostAction.fetchMoreIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId && p.get('postId') == payload.postId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    postId: payload.postId,
                    roomId: payload.roomId,
                    posts: new Posts([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.INCREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.INCREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(PostAction.fetchMoreIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('postId') == payload.params.postId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    postId: payload.params.postId,
                    roomId: payload.params.roomId,
                    posts: payload.result.posts,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('posts', targetPage.get('posts').concat(payload.result.posts));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(PostAction.fetchMoreIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId && p.get('postId') == payload.params.postId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    postId: payload.params.postId,
                    roomId: payload.params.roomId,
                    posts: new Posts([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(PostAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('post').id == payload.id);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ post: new Post(), loading: true, success: false }));
        } else {
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(PostAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('post').id == payload.params.id);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ post: payload.result.post, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('post', payload.result.post);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(PostAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('post').id == payload.params.id);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ post: new Post(), loading: false, success: false }));
        } else {
            targetPage = targetPage.set('post', new Post());
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(PostAction.search.started, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.query,
                    posts: new Posts([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(PostAction.search.done, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    posts: payload.result.posts,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('posts', payload.result.posts);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(PostAction.search.failed, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    posts: new Posts([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('posts', new Posts([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(PostAction.searchMore.started, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.query,
                    posts: new Posts([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.DECREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.DECREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(PostAction.searchMore.done, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    posts: payload.result.posts,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('posts', targetPage.get('posts').concat(payload.result.posts));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(PostAction.searchMore.failed, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    posts: new Posts([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })

export default PostReducer;