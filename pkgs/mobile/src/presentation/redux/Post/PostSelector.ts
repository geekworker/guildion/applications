import { createSelector } from "reselect";
import { RootState } from "../RootReducer";

export const postShowPageStatesSelector = (state: RootState) => state.post.pages.show;
export const postShowPageStateSelector = createSelector(
    postShowPageStatesSelector,
    (_: RootState, id: string) => id,
    (show, id) => show.find(p => p.get('post').id == id),
);
export const postsIndexPageStatesSelector = (state: RootState) => state.post.pages.index;
export const postsIndexPageStateSelector = createSelector(
    postsIndexPageStatesSelector,
    (_: RootState, roomId: string, postId?: string) => { return { roomId, postId }},
    (pages, { roomId, postId }: { roomId: string, postId?: string }) => pages.find(p => p.get('roomId') == roomId && p.get('postId') == postId),
);
export const postsSearchPageStatesSelector = (state: RootState) => state.post.pages.search;
export const postsSearchPageStateSelector = createSelector(
    postsSearchPageStatesSelector,
    (_: RootState, query: string) => query,
    (pages, query) => pages.find(p => p.get('query') == query),
);