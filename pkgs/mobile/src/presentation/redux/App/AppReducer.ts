import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Record } from 'immutable';
import { AppError, ErrorType, isNetworkError, NetworkStatus } from '@guildion/core';
import { InitialState, NavigationState } from '@react-navigation/routers';
import { globalAsyncStorage } from '@/shared/modules/GlobalAsyncStorage';

const actionCreator = actionCreatorFactory('App');

export const AppAction = {
    displayErrors: actionCreator<{ errors: List<AppError> }>('DISPLAY_ERRORS'),
    resetErrors: actionCreator('RESET_ERRORS'),
    resetGlobalErrors: actionCreator('RESET_GLOBAL_ERRORS'),
    resetAppErrors: actionCreator('RESET_APP_ERRORS'),
    launch: actionCreator.async<{}, {}>('LAUNCH'),
    readInitialNavigationState: actionCreator('READ_INITIAL_NAVIGATION_STATE'),
    saveInitialNavigationState: actionCreator.async<NavigationState, {}>('SAVE_INITIAL_NAVIGATION_STATE'),
    readInitialRoomSplitScreenNavigationState: actionCreator('READ_INITIAL_ROOM_SPLIT_SCREEN_NAVIGATION_STATE'),
    saveInitialRoomSplitScreenNavigationState: actionCreator.async<NavigationState, {}>('SAVE_INITIAL_ROOM_SPLIT_SCREEN_NAVIGATION_STATE'),
};

export type AppAction = typeof AppAction[keyof typeof AppAction];

export class AppState extends Record<{
    errors: List<AppError>,
    isLaunched: boolean,
    initialNavigationState: InitialState,
    initialRoomSplitScreenNavigationState: InitialState,
    networkStatus: NetworkStatus,
}>({
    errors: List([]),
    isLaunched: false,
    initialNavigationState: { routes: [] },
    initialRoomSplitScreenNavigationState: { routes: [] },
    networkStatus: NetworkStatus.Stable,
}) {}

const AppReducer = reducerWithInitialState(new AppState())
    .case(AppAction.displayErrors, (state, payload) => {
        state = state.set('errors', payload.errors);
        return state;
    })
    .case(AppAction.resetErrors, (state) => {
        state = state.set('errors', List([]));
        return state;
    })
    .case(AppAction.resetGlobalErrors, (state) => {
        state = state.set('errors', state.errors.filter(e => e.type != ErrorType.global));
        return state;
    })
    .case(AppAction.resetAppErrors, (state) => {
        state = state.set('errors', state.errors.filter(e => e.type == ErrorType.global));
        return state;
    })
    .case(AppAction.launch.done, (state) => {
        state = state.set('isLaunched', true);
        return state;
    })
    .case(AppAction.launch.failed, (state) => {
        state = state.set('isLaunched', true);
        return state;
    })
    .case(AppAction.saveInitialNavigationState.done, (state, payload) => {
        state = state.set('initialNavigationState', payload.params);
        return state;
    })
    .case(AppAction.saveInitialRoomSplitScreenNavigationState.done, (state, payload) => {
        state = state.set('initialRoomSplitScreenNavigationState', payload.params);
        return state;
    })
    .case(AppAction.readInitialNavigationState, (state) => {
        globalAsyncStorage
        state = state.set('initialNavigationState', globalAsyncStorage.current.navigationState);
        return state;
    })
    .case(AppAction.readInitialRoomSplitScreenNavigationState, (state) => {
        globalAsyncStorage
        state = state.set('initialRoomSplitScreenNavigationState', globalAsyncStorage.current.roomSplitSceneNavigationState);
        return state;
    })

export default AppReducer;