import AppUseCase from '@/domain/usecases/AppUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { AppAction } from './AppReducer';

export default function* AppSaga() {
    yield takeEvery(AppAction.launch.started, function*({ payload }) {
        yield call(bindAsyncAction(AppAction.launch, { skipStartedAction: true })(new AppUseCase().launch), payload)
    })
    yield takeEvery(AppAction.saveInitialNavigationState.started, function*({ payload }) {
        yield call(bindAsyncAction(AppAction.saveInitialNavigationState, { skipStartedAction: true })(new AppUseCase().saveInitialNavigationState), payload)
    })
    yield takeEvery(AppAction.saveInitialRoomSplitScreenNavigationState.started, function*({ payload }) {
        yield call(bindAsyncAction(AppAction.saveInitialRoomSplitScreenNavigationState, { skipStartedAction: true })(new AppUseCase().saveInitialRoomSplitScreenNavigationState), payload)
    })
}