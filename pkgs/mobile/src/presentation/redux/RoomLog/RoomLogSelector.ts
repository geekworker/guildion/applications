import { createSelector } from "reselect";
import { RootState } from "../RootReducer";

export const roomLogsPageStatesSelector = (state: RootState) => state.roomLog.pages.index;
export const roomLogsPageStateSelector = createSelector(
    roomLogsPageStatesSelector,
    (_: RootState, roomId: string) => roomId,
    (index, roomId) => index.find(p => p.get('roomId') == roomId),
);