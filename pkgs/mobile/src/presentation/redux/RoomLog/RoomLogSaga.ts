import RoomLogsUseCase from '@/domain/usecases/RoomLogsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { RoomLogAction } from './RoomLogReducer';

export default function* RoomLogSaga() {
    yield takeEvery(RoomLogAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomLogAction.fetchIndex, { skipStartedAction: true })(new RoomLogsUseCase().fetchIndex), payload)
    })
}