import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record, List, Map } from 'immutable';
import { RoomLogs, ImmutableMap } from '@guildion/core';

const actionCreator = actionCreatorFactory('RoomLog');

export interface RoomLogsPageStateAttributes { roomId: string, logs: RoomLogs, loading?: boolean, paginatable?: boolean, success?: boolean };
export type RoomLogsPageState = ImmutableMap<RoomLogsPageStateAttributes>;
export type RoomLogsPageStates = List<RoomLogsPageState>

export const RoomLogAction = {
    fetchIndex: actionCreator.async<{ roomId: string }, { logs: RoomLogs, paginatable: boolean }>('ROOM_LOGS_FETCH_INDEX'),
};

export type RoomLogAction = typeof RoomLogAction[keyof typeof RoomLogAction];

export class RoomLogState extends Record<{
    pages: {
        index: RoomLogsPageStates,
    },
}>({
    pages: {
        index: List([]),
    },
}) {}

const RoomLogReducer = reducerWithInitialState(new RoomLogState())
    .case(RoomLogAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.roomId, logs: new RoomLogs([]), loading: true, success: false, paginatable: false }));
        } else {
            targetPage = targetPage.set('logs', new RoomLogs([]));
            targetPage = targetPage.set('loading', true);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomLogAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.params.roomId, logs: payload.result.logs, loading: false, success: true, paginatable: payload.result.paginatable }));
        } else {
            targetPage = targetPage.set('logs', payload.result.logs);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomLogAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('roomId') == payload.params.roomId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ roomId: payload.params.roomId, logs: new RoomLogs([]), loading: false, success: false, paginatable: false }));
        } else {
            targetPage = targetPage.set('logs', new RoomLogs([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })

export default RoomLogReducer;