import DiscoveriesUseCase from '@/domain/usecases/DiscoveriesUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { DiscoveryAction } from './DiscoveryReducer';

export default function* DiscoverySaga() {
    yield takeEvery(DiscoveryAction.fetchRecommend.started, function*({ payload }) {
        yield call(bindAsyncAction(DiscoveryAction.fetchRecommend, { skipStartedAction: true })(new DiscoveriesUseCase().fetchRecommend), payload)
    });
    yield takeEvery(DiscoveryAction.fetchMoreRecommend.started, function*({ payload }) {
        yield call(bindAsyncAction(DiscoveryAction.fetchMoreRecommend, { skipStartedAction: true })(new DiscoveriesUseCase().fetchMoreRecommend), payload)
    });
}