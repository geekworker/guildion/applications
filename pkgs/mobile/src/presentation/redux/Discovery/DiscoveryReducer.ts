import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { Record, Map } from 'immutable';
import { Discoveries, ImmutableMap } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const actionCreator = actionCreatorFactory('Discovery');

export interface DiscoveriesRecommendPageStateAttributes { discoveries: Discoveries, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus }[];
export type DiscoveriesRecommendPageState = ImmutableMap<DiscoveriesRecommendPageStateAttributes>;

export const DiscoveryAction = {
    fetchRecommend: actionCreator.async<{ guildId: string }, { discoveries: Discoveries, paginatable: boolean }>('DISCOVERY_FETCH_RECOMMEND'),
    fetchMoreRecommend: actionCreator.async<{ guildId: string }, { discoveries: Discoveries, paginatable: boolean }>('DISCOVERY_FETCH_MORE_RECOMMEND'),
};

export type DiscoveryAction = typeof DiscoveryAction[keyof typeof DiscoveryAction];

export class DiscoveryState extends Record<{
    pages: {
        recommend: DiscoveriesRecommendPageState,
    },
}>({
    pages: {
        recommend: Map({
            discoveries: new Discoveries([]),
            loading: true,
        }),
    },
}) {}

const DiscoveryReducer = reducerWithInitialState(new DiscoveryState())
    .case(DiscoveryAction.fetchRecommend.started, (state, payload) => {
        let pageState = state.pages.recommend;
        pageState = pageState.set('loading', false);
        pageState = pageState.set('loadingMoreStatus', undefined);
        pageState = pageState.set('success', false);
        pageState = pageState.set('paginatable', false);
        state = state.setIn(['pages', 'recommend'], pageState);
        return state;
    })
    .case(DiscoveryAction.fetchRecommend.done, (state, payload) => {
        let pageState = state.pages.recommend;
        pageState = pageState.set('discoveries', payload.result.discoveries);
        pageState = pageState.set('loading', false);
        pageState = pageState.set('loadingMoreStatus', undefined);
        pageState = pageState.set('success', true);
        pageState = pageState.set('paginatable', payload.result.paginatable);
        state = state.setIn(['pages', 'recommend'], pageState);
        return state;
    })
    .case(DiscoveryAction.fetchRecommend.failed, (state, payload) => {
        let pageState = state.pages.recommend;
        pageState = pageState.set('discoveries', new Discoveries([]));
        pageState = pageState.set('loading', false);
        pageState = pageState.set('loadingMoreStatus', undefined);
        pageState = pageState.set('success', false);
        pageState = pageState.set('paginatable', false);
        state = state.setIn(['pages', 'recommend'], pageState);
        return state;
    })
    .case(DiscoveryAction.fetchMoreRecommend.started, (state, payload) => {
        let pageState = state.pages.recommend;
        pageState = pageState.set('loading', false);
        pageState = pageState.set('loadingMoreStatus', LoadingMoreStatus.DECREMENT);
        pageState = pageState.set('success', false);
        state = state.setIn(['pages', 'recommend'], pageState);
        return state;
    })
    .case(DiscoveryAction.fetchMoreRecommend.done, (state, payload) => {
        let pageState = state.pages.recommend;
        pageState = pageState.set('discoveries', payload.result.discoveries);
        pageState = pageState.set('loading', false);
        pageState = pageState.set('loadingMoreStatus', undefined);
        pageState = pageState.set('success', true);
        pageState = pageState.set('paginatable', payload.result.paginatable);
        state = state.setIn(['pages', 'recommend'], pageState);
        return state;
    })
    .case(DiscoveryAction.fetchMoreRecommend.failed, (state, payload) => {
        let pageState = state.pages.recommend;
        pageState = pageState.set('loading', false);
        pageState = pageState.set('loadingMoreStatus', undefined);
        pageState = pageState.set('success', false);
        pageState = pageState.set('paginatable', false);
        state = state.setIn(['pages', 'recommend'], pageState);
        return state;
    })

export default DiscoveryReducer;