import { RootState } from '../RootReducer';

export const discoveryRecommendPageStateSelector = (state: RootState) => state.discovery.pages.recommend;