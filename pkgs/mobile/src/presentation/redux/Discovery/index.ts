import discoveryReducer from './DiscoveryReducer';
import discoverySaga from './DiscoverySaga';

export { discoveryReducer, discoverySaga };
