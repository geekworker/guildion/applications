import RoomsUseCase from '@/domain/usecases/RoomsUseCase';
import { call, takeEvery } from 'redux-saga/effects';
import { bindAsyncAction } from '@/shared/modules/typescript-fsa-redux-saga';
import { RoomAction } from './RoomReducer';

export default function* RoomSaga() {
    yield takeEvery(RoomAction.fetchIndex.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomAction.fetchIndex, { skipStartedAction: true })(new RoomsUseCase().fetchIndex), payload)
    });
    yield takeEvery(RoomAction.fetchShow.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomAction.fetchShow, { skipStartedAction: true })(new RoomsUseCase().fetchShow), payload)
    });
    yield takeEvery(RoomAction.disconnect.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomAction.disconnect, { skipStartedAction: true })(new RoomsUseCase().disconnect), payload)
    });
    yield takeEvery(RoomAction.search.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomAction.search, { skipStartedAction: true })(new RoomsUseCase().search), payload)
    });
    yield takeEvery(RoomAction.searchMore.started, function*({ payload }) {
        yield call(bindAsyncAction(RoomAction.searchMore, { skipStartedAction: true })(new RoomsUseCase().searchMore), payload)
    });
}