import { createSelector } from "reselect";
import { RootState } from "../RootReducer";

export const roomShowPageStatesSelector = (state: RootState) => state.room.pages.show;
export const roomShowPageStateSelector = createSelector(
    roomShowPageStatesSelector,
    (_: RootState, id: string) => id,
    (show, id) => show.find(p => p.get('room').id == id),
);
export const roomsSearchPageStatesSelector = (state: RootState) => state.room.pages.search;
export const roomsSearchPageStateSelector = createSelector(
    roomsSearchPageStatesSelector,
    (_: RootState, query: string) => query,
    (pages, query) => pages.find(p => p.get('query') == query),
);