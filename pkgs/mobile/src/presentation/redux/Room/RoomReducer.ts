import actionCreatorFactory from 'typescript-fsa';
import { reducerWithInitialState } from 'typescript-fsa-reducers';
import { List, Record, Map } from 'immutable';
import { Room, Rooms, ImmutableMap } from '@guildion/core';
import { LoadingMoreStatus } from '@/shared/interfaces/LoadingMoreStatus';

const actionCreator = actionCreatorFactory('Room');

export interface RoomsPageStateAttributes { guildId: string, rooms: Rooms, loading?: boolean, paginatable?: boolean, success?: boolean, isPublic: boolean }[];
export type RoomsPageState = ImmutableMap<RoomsPageStateAttributes>;
export type GuildRoomsPageState = List<RoomsPageState>;
export interface RoomPageStateAttributes { room: Room, loading?: boolean, success?: boolean, isPublic: boolean }[];
export type RoomPageState = ImmutableMap<RoomPageStateAttributes>;
export type RoomPageStates = List<RoomPageState>;
export interface SearchRoomsPageStateAttributes { query: string, rooms: Rooms, loading?: boolean, paginatable?: boolean, success?: boolean, loadingMoreStatus?: LoadingMoreStatus };
export type SearchRoomsPageState = ImmutableMap<SearchRoomsPageStateAttributes>;
export type SearchRoomPageStates = List<SearchRoomsPageState>;

export const RoomAction = {
    fetchIndex: actionCreator.async<{ guildId: string, isPublic?: boolean }, { rooms: Rooms, paginatable: boolean }>('ROOM_FETCH_INDEX'),
    fetchShow: actionCreator.async<{ id: string, isPublic?: boolean }, { room: Room }>('ROOM_FETCH_SHOW'),
    disconnect: actionCreator.async<{ id: string }, {}>('ROOM_DISCONNECT'),
    search: actionCreator.async<{ query: string }, { rooms: Rooms, paginatable: boolean }>('ROOM_SEARCH'),
    searchMore: actionCreator.async<{ query: string }, { rooms: Rooms, paginatable: boolean }>('ROOM_SEARCH_MORE'),
};

export type RoomAction = typeof RoomAction[keyof typeof RoomAction];

export class RoomState extends Record<{
    pages: {
        index: GuildRoomsPageState,
        show: RoomPageStates,
        search: SearchRoomPageStates,
    },
}>({
    pages: {
        index: List([]),
        show: List([]),
        search: List([]),
    },
}) {}

const RoomReducer = reducerWithInitialState(new RoomState())
    .case(RoomAction.fetchIndex.started, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({ guildId: payload.guildId, rooms: new Rooms([]), loading: true, paginatable: false, success: false })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomAction.fetchIndex.done, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ rooms: payload.result.rooms, guildId: payload.params.guildId, loading: false, paginatable: payload.result.paginatable, success: true }));
        } else {
            targetPage = targetPage.set('rooms', payload.result.rooms);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomAction.fetchIndex.failed, (state, payload) => {
        let pageState = state.pages.index;
        const index = pageState.findIndex(p => p.get('guildId') == payload.params.guildId);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ guildId: payload.params.guildId, rooms: new Rooms([]), loading: false, paginatable: false, success: false }));
        } else {
            targetPage = targetPage.set('rooms', new Rooms([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'index'], pageState);
        return state;
    })
    .case(RoomAction.fetchShow.started, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('room').id == payload.id);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ room: new Room(), loading: true, success: false }));
        } else {
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(RoomAction.fetchShow.done, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('room').id == payload.params.id);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ room: payload.result.room, loading: false, success: true }));
        } else {
            targetPage = targetPage.set('room', payload.result.room);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(RoomAction.fetchShow.failed, (state, payload) => {
        let pageState = state.pages.show;
        const index = pageState.findIndex(p => p.get('room').id == payload.params.id);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(Map({ room: new Room(), loading: false, success: false }));
        } else {
            targetPage = targetPage.set('room', new Room());
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'show'], pageState);
        return state;
    })
    .case(RoomAction.search.started, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.query,
                    rooms: new Rooms([]),
                    loading: true,
                    success: false,
                    paginatable: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(RoomAction.search.done, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    rooms: payload.result.rooms,
                    loading: false,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('rooms', payload.result.rooms);
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(RoomAction.search.failed, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    rooms: new Rooms([]),
                    loading: false,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('rooms', new Rooms([]));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(RoomAction.searchMore.started, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.query,
                    rooms: new Rooms([]),
                    loading: false,
                    LoadingMoreStatus: LoadingMoreStatus.DECREMENT,
                    paginatable: false,
                    success: false,
                })
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', LoadingMoreStatus.DECREMENT);
            targetPage = targetPage.set('success', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(RoomAction.searchMore.done, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    rooms: payload.result.rooms,
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: payload.result.paginatable,
                    success: true,
                })
            );
        } else {
            targetPage = targetPage.set('rooms', targetPage.get('rooms').concat(payload.result.rooms));
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('success', true);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('paginatable', payload.result.paginatable);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })
    .case(RoomAction.searchMore.failed, (state, payload) => {
        let pageState = state.pages.search;
        const index = pageState.findIndex(p => p.get('query') == payload.params.query);
        let targetPage = pageState.get(index);
        if (!targetPage || index < 0) {
            pageState = pageState.push(
                Map({
                    query: payload.params.query,
                    rooms: new Rooms([]),
                    loading: false,
                    loadingMoreStatus: undefined,
                    paginatable: false,
                    success: false,
                })    
            );
        } else {
            targetPage = targetPage.set('loading', false);
            targetPage = targetPage.set('loadingMoreStatus', undefined);
            targetPage = targetPage.set('success', false);
            targetPage = targetPage.set('paginatable', false);
            pageState = pageState.set(index, targetPage!);
        }
        state = state.setIn(['pages', 'search'], pageState);
        return state;
    })

export default RoomReducer;