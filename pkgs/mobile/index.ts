/**
 * @format
 */

import './src/infrastructure/Core';
import './src/presentation/redux/ConfigureStore';
import { AppRegistry } from 'react-native';
import App from './src/infrastructure/App';
import { name as appName } from './app.json';
import { IS_STORYBOOK } from '@guildion/core';
import getStorybookUIRoot from './storybook';

AppRegistry.registerComponent(appName, () => 
    IS_STORYBOOK() ? getStorybookUIRoot() :
    App
);
