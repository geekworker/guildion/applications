/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

const path = require('path');

module.exports = {
    projectRoot: path.join(__dirname),
    transformer: {
        getTransformOptions: async () => ({
            transform: {
                experimentalImportSupport: false,
                inlineRequires: true,
            },
        }),
    },
    resolver: {
        alias: {
            '@': path.resolve(__dirname, 'src'),
        },
    },
    watchFolders: [
        path.resolve(__dirname, '..', 'core'),
        path.resolve(__dirname, '..', 'ui'),
        path.resolve(__dirname, '..', '..', 'node_modules'),
    ],
};
