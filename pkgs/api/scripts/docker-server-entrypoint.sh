#!/bin/sh

set -e

raise(){
 echo "raise exception"
 exit 1
}

echo "* build mode:" $NODE_ENV

echo "bootstraping server"
cd /app/pkgs/api/lib
node api.min.js