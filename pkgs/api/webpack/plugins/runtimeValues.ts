import dotenv from 'dotenv';
import { GET_NODE_ENV, LOCAL_API_PORT } from "@guildion/core";

export type RuntimeValues = {
    NODE_ENV: string,
    PORT: string,
    NUM_PROCESSES?: string,
    JWT_SECRET: string,
    AWS_ACCESS_KEY_ID: string,
    AWS_SECRET_ACCESS_KEY: string,
    AWS_REGION: string,
    AWS_S3_REGION: string,
    AWS_S3_CDN_BUCKET: string,
    GOOGLE_ANALYSTICS_CLIENT: string,
    ONESIGNAL_APP_ID: string,
    ONESIGNAL_USER_AUTH_KEY: string,
    ONESIGNAL_APP_AUTH_KEY: string,
    ONESIGNAL_SAFARI_ID: string,
    DATABASE_USERNAME: string,
    DATABASE_PASSWORD?: string,
    DATABASE_NAME: string,
    DATABASE_HOST: string,
    DATABASE_PORT: string,
    CONNECT_DEVICE_USERNAME: string,
    CONNECT_DEVICE_UDID: string,
}

export let currentRuntimeValues: RuntimeValues;

export const updateCurrentRuntimeValues = () => {
    currentRuntimeValues = {
        NODE_ENV: GET_NODE_ENV(process.env.NODE_ENV!),
        PORT: `${process.env.PORT ?? process.env.API_PORT ?? LOCAL_API_PORT}`,
        NUM_PROCESSES: `${!!process.env.API_NUM_PROCESSES ? Number(process.env.API_NUM_PROCESSES) || undefined : undefined}`,
        JWT_SECRET: process.env.API_JWT_SECRET!,
        AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID!,
        AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY!,
        AWS_REGION: process.env.AWS_REGION || process.env.AWS_DEFAULT_REGION || 'ap-northeast-1',
        AWS_S3_REGION: process.env.AWS_S3_REGION || process.env.AWS_DEFAULT_REGION || 'ap-northeast-1',
        AWS_S3_CDN_BUCKET: process.env.AWS_S3_CDN_BUCKET!,
        GOOGLE_ANALYSTICS_CLIENT: process.env.GOOGLE_ANALYSTICS_CLIENT!,
        ONESIGNAL_APP_ID: process.env.ONESIGNAL_APP_ID!,
        ONESIGNAL_USER_AUTH_KEY: process.env.ONESIGNAL_USER_AUTH_KEY!,
        ONESIGNAL_APP_AUTH_KEY: process.env.ONESIGNAL_APP_AUTH_KEY!,
        ONESIGNAL_SAFARI_ID: process.env.ONESIGNAL_SAFARI_ID!,
        DATABASE_USERNAME: process.env.API_DATABASE_USERNAME!,
        DATABASE_PASSWORD: process.env.API_DATABASE_PASSWORD,
        DATABASE_NAME: process.env.API_DATABASE_NAME!,
        DATABASE_HOST: process.env.API_DATABASE_HOST!,
        DATABASE_PORT: `${Number(process.env.API_DATABASE_PORT) || 5432}`,
        CONNECT_DEVICE_USERNAME: process.env.CONNECT_DEVICE_USERNAME!,
        CONNECT_DEVICE_UDID: process.env.CONNECT_DEVICE_UDID!,
    };
};

function configureRuntimeValues() {
    dotenv.config();
    updateCurrentRuntimeValues();
}

configureRuntimeValues();