const path = require('path');

const ROOT_PATH: string = 'src';

const alias: { [key: string]: string } = {
    '@': path.join(__dirname, '..', '..', ROOT_PATH),
};

export default alias;