import baseConfig from './webpack.config';
import webpack from 'webpack';
import { merge } from 'webpack-merge';
import { NODE_ENV } from '@guildion/core';
import { currentRuntimeValues } from './plugins/runtimeValues';

export default merge(baseConfig, {
    mode: NODE_ENV.PRODUCTION,
    plugins: [
        new webpack.DefinePlugin({
            'RUNTIME_VALUES': JSON.stringify({
                ...currentRuntimeValues,
                NODE_ENV: NODE_ENV.STAGING,
            }),
        }),
    ],
    optimization: {
        // FIXME: occur dependency error when seeding.
        minimize: false,
    },
});
