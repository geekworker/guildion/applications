import baseConfig from './webpack.config';
import { merge } from 'webpack-merge';
import webpack from 'webpack';
import { NODE_ENV } from '@guildion/core';
import { currentRuntimeValues } from './plugins/runtimeValues';

const hotConf = 'webpack-hot-middleware/client?path=/__webpack_hmr&name=api'

export default merge(baseConfig, {
    mode: NODE_ENV.DEVELOPMENT,
    entry: {
        api: [
            hotConf,
        ],
    },
    devtool: 'eval', // 'eval-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'RUNTIME_VALUES': JSON.stringify({
                ...currentRuntimeValues,
                NODE_ENV: NODE_ENV.DEVELOPMENT,
            }),
        }),
        new webpack.HotModuleReplacementPlugin(),
    ],
});
