require('@babel/register')({
    extensions: ['.js', '.jsx', '.tsx', '.ts'],
});
import webpack from 'webpack';
import webpackConfig from './webpack.config.production';

const compiler: webpack.Compiler = webpack(webpackConfig);

compiler.run((err, result) => {
    if (result?.hasErrors()) console.log(result?.compilation.errors)
});