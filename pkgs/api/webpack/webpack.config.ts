import path from 'path';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import webpack from 'webpack';
import alias from './plugins/alias';

// tslint:disable-next-line:no-var-requires
const nodeExternals = require('webpack-node-externals');

const config: webpack.Configuration = {
    name: 'api',
    target: 'node',
    node: {
        __dirname: false,
        __filename: false,
    },
    entry: {
        api: [
            './src/index.ts',
        ],
        vendor: [
            '@guildion/core',
            '@guildion/db',
            '@guildion/node',
        ],
    },
    output: {
        path: path.resolve('lib'),
        filename: '[name].min.js',
        chunkFilename: '[name].[fullhash].min.js',
        publicPath: '/assets/',
    },
    module: {
        rules: [
            {
                test: /\.bin$|\.exec$|^ffprobe$|ffprobe$|^ffprobe/, use: 'file-loader',
            },
            {
                test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf)(\?[a-z0-9=.]+)?$/,
                use: 'url-loader?limit=100000'
            },
            {
                test: /\.html$/i,
                loader: "html-loader",
            },
            { test: /\.json$/, use: 'json-loader', type: "javascript/auto" },
            {
                test: /\.js$|\.jsx$/,
                exclude: /node_modules\/(?!(date-fns)\/).*/,
                use: 'babel-loader',
            },
            { test: /\.svg$/, use: 'svg-inline-loader' },
            {
                test: /\.(ts|tsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        configFile: path.join(__dirname, '..', 'tsconfig.json'),
                    },
                }
            },
            { test: /\.txt$/i, use: 'raw-loader', },
            { test: /\.md$|\.txt$/, use: 'raw-loader' },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'autoprefixer-loader'
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'autoprefixer-loader',
                    'sass-loader',
                ]
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin(/*'[name]-[chunkhash].css'*/),
        new CleanWebpackPlugin(),
        new webpack.IgnorePlugin({ resourceRegExp: /^pg-native$/ }),
    ],
    resolve: {
        alias,
        extensions: ['.ts', '.js', '.tsx', '.jsx', '.json'],
        modules: [
            path.join(__dirname, '../src'),
            path.join(__dirname, '../core'),
            path.join(__dirname, '../db'),
            path.join(__dirname, '../node'),
            'node_modules',
            path.join(__dirname, '../node_modules'),
            path.join(__dirname, '../../../node_modules'),
        ],
        plugins: [
        ],
    },
    externals: [
        { 'sharp': 'commonjs sharp' },
    ],
    externalsPresets: {
        node: true
    },
}

export default config;