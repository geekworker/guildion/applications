import webpack from 'webpack';
import webpackDevConfig from '../webpack/webpack.config.development';
import server from './app/infrastructure/server';
import clustering from './app/infrastructure/clustering';
import seed from './app/infrastructure/seed';
import { configureAWS } from './app/modules/aws/config';
import configureENV from './app/infrastructure/ENV';
import configureCore from './app/infrastructure/core';
import configureNode from './app/infrastructure/node';
import configureDB from './app/infrastructure/db';

export const compiler: webpack.Compiler | undefined = process.env.NODE_ENV == 'development' ? webpack(webpackDevConfig) : undefined;

(async () => {
    if (!!process.env.SCENARIO) return;
    configureENV();
    configureAWS();
    configureCore();
    configureNode();
    await configureDB();
    await seed();
    await clustering(server);
})();