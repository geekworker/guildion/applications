import { APP_NAME } from '@guildion/core';
import readline from 'readline';
import { APIScenarioBooks } from './books';
import configureScenario from './infrastructure';
import APIContext from './infrastructure/APIContext';

export const cli = readline.createInterface({ input: process.stdin, output: process.stdout });
export const apiScenarioBooks = new APIScenarioBooks();


const bootingCli = async () => {
    try {
        await APIContext.instance().restore().catch(e => console.log("Context restore failed for some reasons..."));
        APIContext.instance().logger();
        await apiScenarioBooks.question(cli);
    } catch (e) {
        await bootingCli();
    }
}

(async() => {
    console.log(`Welcome to ${APP_NAME} SCENARIO CLI v0.0.1`);
    await configureScenario();
    await bootingCli();
})();
