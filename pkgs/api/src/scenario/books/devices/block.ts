import { DevicesEndpoint, Device, DeviceBlockRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { DeviceEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<DeviceBlockRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class DeviceBlockCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = {
            data: new DeviceBlockRequest({ 
                id: APIContext.instance().getCurrentDevice()!.id!,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        };
        const entity = await entitySelectQuestion(cli, DeviceEntity, { where: {}, attribute: 'udid', count: 100, allowNull: true });
        if (!!entity) {
            props.data.id = entity.id;
        }
        return props;
    } 
}

export class DeviceBlockScenario extends APIScenario(DevicesEndpoint.Block) {
    public prefix = 'block';
    public description = 'block device manually';
    public isDefault = true;
    public cli = DeviceBlockCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class DeviceBlockSection extends ScenarioSection {
    public prefix = 'block';
    public description = 'Block device through API';
    public cases = [new DeviceBlockScenario()];
}