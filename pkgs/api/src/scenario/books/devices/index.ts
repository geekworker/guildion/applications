import { ScenarioBook } from "@guildion/node";
import { DeviceRegisterSection } from "./register";
import { DeviceBlockSection } from "./block";
import { DeviceBlocksListSection } from "./blocksList";
import { DeviceUnBlockSection } from "./unblock";
import { DeviceTrustSection } from "./trust";
import { DevicesListSection } from "./list";

export class DevicesBook extends ScenarioBook {
    public prefix = 'devices';
    public description = 'devices scenario books';
    public cases = [
        new DeviceRegisterSection(),
        new DeviceBlockSection(),
        new DeviceBlocksListSection(),
        new DeviceUnBlockSection(),
        new DeviceTrustSection(),
        new DevicesListSection(),
    ];
}