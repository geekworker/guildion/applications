import { DevicesEndpoint, Device, fallbackLanguageCode, fallbackCountryCode, DeviceTrustRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { DeviceEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<DeviceTrustRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class DeviceTrustCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = {
            data: new DeviceTrustRequest({ 
                id: APIContext.instance().getCurrentDevice()!.id!,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        };
        const entity = await entitySelectQuestion(cli, DeviceEntity, { attribute: 'udid', count: 100, allowNull: true });
        if (!!entity) {
            props.data.id = entity.id;
        }
        return props;
    } 
}

export class DeviceTrustScenario extends APIScenario(DevicesEndpoint.Trust) {
    public prefix = 'trust';
    public description = 'trust device manually';
    public isDefault = true;
    public cli = DeviceTrustCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class DeviceTrustSection extends ScenarioSection {
    public prefix = 'trust';
    public description = 'Trust device through API';
    public cases = [new DeviceTrustScenario()];
}