import { DevicesEndpoint, Device, DeviceBlocksListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import Colors from 'colors';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<DeviceBlocksListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class DeviceBlocksListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new DeviceBlocksListRequest({
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: false,
        });
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: false,
        });
        return props;
    } 
}

export class DeviceBlocksListScenario extends APIScenario(DevicesEndpoint.List) {
    public prefix = 'blocksList';
    public description = 'list blocking devices manually';
    public isDefault = true;
    public cli = DeviceBlocksListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.devices);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class DeviceBlocksListSection extends ScenarioSection {
    public prefix = 'blocksList';
    public description = 'List blocking device through API';
    public cases = [new DeviceBlocksListScenario()];
}