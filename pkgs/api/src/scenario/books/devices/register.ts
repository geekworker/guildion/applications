import { DevicesEndpoint, Device, fallbackLanguageCode, fallbackCountryCode, DeviceRegisterRequest, DeviceStatus, fallbackTimeZoneID } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { ulid } from "ulid";
import { DeviceEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<DeviceRegisterRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
}

interface Finale extends ScenarioFinale {
}

export class DeviceRegisterCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = {
            data: new DeviceRegisterRequest({ 
                device: {
                    udid: ulid(),
                    os: 'iOS',
                    model: 'iPhone X',
                    languageCode: fallbackLanguageCode,
                    countryCode: fallbackCountryCode,
                    timezone: fallbackTimeZoneID,
                    appVersion: '',
                    status: DeviceStatus.NEW,
                },
            }).toJSON()
        };
        const entity = await entitySelectQuestion(cli, DeviceEntity, { attribute: 'udid', count: 100, allowNull: true });
        if (!!entity) {
            props.data.device.udid = entity.udid;
            props.data.device.os = entity.os;
            props.data.device.model = entity.model;
            props.data.device.languageCode = entity.languageCode;
            props.data.device.countryCode = entity.countryCode;
            props.data.device.onesignalNotificationId = entity.onesignalNotificationId;
            props.data.device.notificationToken = entity.notificationToken;
        }
        return props;
    } 
}

export class DeviceRegisterScenario extends APIScenario(DevicesEndpoint.Register) {
    public prefix = 'register';
    public description = 'register device manually';
    public isDefault = true;
    public cli = DeviceRegisterCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.device && result.csrfSecret) {
            APIContext.current().csrfSecret = result.csrfSecret;
            APIContext.current().currentDevice = result.device;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class DeviceRegisterSection extends ScenarioSection {
    public prefix = 'register';
    public description = 'Register device through API';
    public cases = [new DeviceRegisterScenario()];
}