import { RoomRolesEndpoint, Device, RoomRoleShowRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoleEntity, RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomRoleShowRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomRoleShowCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomRoleShowRequest({
                roomId: APIContext.current().currentRoom?.id!,
                roleId: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName' }))?.id ?? APIContext.current().currentRoom?.id!;
        props.data.roleId = (await entitySelectQuestion(cli, RoleEntity, { attribute: 'type' }))?.id!;
        return props;
    } 
}

export class RoomRoleShowScenario extends APIScenario(RoomRolesEndpoint.Show) {
    public prefix = 'show';
    public description = 'show role manually';
    public isDefault = true;
    public cli = RoomRoleShowCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomRoleShowSection extends ScenarioSection {
    public prefix = 'show';
    public description = 'Show role through API';
    public cases = [new RoomRoleShowScenario()];
}