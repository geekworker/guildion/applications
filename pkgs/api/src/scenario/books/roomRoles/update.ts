import { RoomRolesEndpoint, Device, RoomRoleUpdateRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsBooleanQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoleEntity, RoomEntity, RoomRoleEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomRoleUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomRoleUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomRoleUpdateRequest({
                role: {
                    id: '',
                    roomId: '',
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        const room = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName' })) ?? APIContext.current().currentRoom!;
        const role = (await entitySelectQuestion(cli, RoleEntity, { attribute: 'type', allowNull: false, where: { guildId: room.guildId! } }))!;

        const roomRole = await RoomRoleEntity.findOne({
            where: {
                roomId: room.id!,
                roleId: role.id,
            }
        });
        props.data.role = {
            id: role.id,
            roomId: room.id!,
            ...await roomRole!.transform(),
        }
        return props;
    } 
}

export class RoomRoleUpdateScenario extends APIScenario(RoomRolesEndpoint.Update) {
    public prefix = 'update';
    public description = 'update role manually';
    public isDefault = true;
    public cli = RoomRoleUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomRoleUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update role through API';
    public cases = [new RoomRoleUpdateScenario()];
}