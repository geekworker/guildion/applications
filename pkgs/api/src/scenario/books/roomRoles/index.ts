import { ScenarioBook } from "@guildion/node";
import { RoomRoleCreateSection } from "./create";
import { RoomRoleUpdateSection } from "./update";
import { RoomRoleDestroySection } from "./destroy";
import { RoomRolesListSection } from "./list";
import { RoomRoleShowSection } from "./show";

export class RoomRolesBook extends ScenarioBook {
    public prefix = 'roomRoles';
    public description = 'room roles scenario books';
    public cases = [
        new RoomRoleCreateSection(),
        new RoomRoleUpdateSection(),
        new RoomRoleDestroySection(),
        new RoomRolesListSection(),
        new RoomRoleShowSection(),
    ];
}