import { RoomRolesEndpoint, Device, RoomRolesListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomRolesListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomRolesListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomRolesListRequest({
                roomId: APIContext.current().currentRoom?.id!
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        // props.data.count = await answerPropsNumberQuestion(cli, {
        //     props: props.data,
        //     propsname: 'LIST API',
        //     key: 'count',
        //     nullable: false,
        // });
        // props.data.offset = await answerPropsNumberQuestion(cli, {
        //     props: props.data,
        //     propsname: 'LIST API',
        //     key: 'offset',
        //     nullable: false,
        // });
        return props;
    } 
}

export class RoomRolesListScenario extends APIScenario(RoomRolesEndpoint.List) {
    public prefix = 'show';
    public description = 'show role manually';
    public isDefault = true;
    public cli = RoomRolesListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.roles);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomRolesListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List role through API';
    public cases = [new RoomRolesListScenario()];
}