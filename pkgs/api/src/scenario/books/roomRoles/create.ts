import { RoomRolesEndpoint, Device, RoomRoleCreateRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoleEntity, RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomRoleCreateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomRoleCreateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomRoleCreateRequest({
                roomId: APIContext.current().currentRoom?.id!,
                roleId: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName' }))?.id ?? APIContext.current().currentRoom?.id!;
        props.data.roleId = (await entitySelectQuestion(cli, RoleEntity, { attribute: 'type' }))?.id!;
        return props;
    } 
}

export class RoomRoleCreateScenario extends APIScenario(RoomRolesEndpoint.Create) {
    public prefix = 'create';
    public description = 'create role manually';
    public isDefault = true;
    public cli = RoomRoleCreateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomRoleCreateSection extends ScenarioSection {
    public prefix = 'create';
    public description = 'Create role through API';
    public cases = [new RoomRoleCreateScenario()];
}