import { ScenarioBooks } from "@guildion/node";
import { DevicesBook } from "./devices";
import { AccountsBook } from "./accounts";
import { UsersBook } from "./users";
import { GuildsBook } from "./guilds";
import { MembersBook } from "./members";
import { RoomsBook } from "./rooms";
import { RolesBook } from "./roles";
import { RoomLogsBook } from "./roomLogs";
import { RoomRolesBook } from "./roomRoles";
import { MemberRolesBook } from "./memberRoles";
import { FilesBook } from "./files";
import APIContext from "../infrastructure/APIContext";
import { MessagesBook } from "./messages";
import { GuildFilesBook } from "./guildFiles";
import { RoomFilesBook } from "./roomFiles";
import { PostsBook } from "./posts";
import { DiscoveriesBook } from "./discoveries";

export class APIScenarioBooks extends ScenarioBooks {
    public books = [
        new DevicesBook(),
        new AccountsBook(),
        new UsersBook(),
        new FilesBook(),
        new GuildsBook(),
        new RolesBook(),
        new MembersBook(),
        new MemberRolesBook(),
        new RoomsBook(),
        new RoomRolesBook(),
        new RoomLogsBook(),
        new GuildFilesBook(),
        new RoomFilesBook(),
        new MessagesBook(),
        new PostsBook(),
        new DiscoveriesBook(),
    ];

    public displayScenario = async () => {
        APIContext.instance().logger();
    }
}