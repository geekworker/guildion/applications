import { UsersEndpoint, Device, UserUpdateRequest, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { FileEntity, UserEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<UserUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class UserUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new UserUpdateRequest({
                user: {
                    id: APIContext.current().currentUser?.id ?? '',
                    ...APIContext.current().currentUser,
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.user.id = (await entitySelectQuestion(cli, UserEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentUser?.id ?? '';
        props.data.user.displayName = await answerPropsQuestion(cli, {
            props: props.data.user,
            propsname: 'user',
            key: "displayName",
            nullable: true,
        }) || props.data.user.displayName;
        props.data.user.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        props.data.user.description = await answerPropsQuestion(cli, {
            props: props.data.user,
            propsname: 'user',
            key: "description",
            nullable: true,
        }) || props.data.user.description;
        return props;
    } 
}

export class UserUpdateScenario extends APIScenario(UsersEndpoint.Update) {
    public prefix = 'update';
    public description = 'update user manually';
    public isDefault = true;
    public cli = UserUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.user) {
            APIContext.current().currentUser = result.user;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class UserUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update user through API';
    public cases = [new UserUpdateScenario()];
}