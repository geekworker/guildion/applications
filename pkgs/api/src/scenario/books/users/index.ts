import { ScenarioBook } from "@guildion/node";
import { UserUpdateSection } from "./update";
import { UserDestroySection } from "./destroy";
import { UserBlockSection } from "./block";
import { UserUnBlockSection } from "./unblock";
import { UserMuteSection } from "./mute";
import { UserUnMuteSection } from "./unmute";

export class UsersBook extends ScenarioBook {
    public prefix = 'users';
    public description = 'users scenario books';
    public cases = [
        new UserUpdateSection(),
        new UserDestroySection(),
        new UserBlockSection(),
        new UserUnBlockSection(),
        new UserMuteSection(),
        new UserUnMuteSection(),
    ];
}