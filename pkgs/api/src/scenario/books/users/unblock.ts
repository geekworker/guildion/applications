import { UsersEndpoint, Device, UserUnBlockRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import { UserEntity } from "@guildion/db";
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<UserUnBlockRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class UserUnBlockCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new UserUnBlockRequest({
                id: APIContext.current().currentUser?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, UserEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentUser?.id ?? '';
        return props;
    } 
}

export class UserUnBlockScenario extends APIScenario(UsersEndpoint.UnBlock) {
    public prefix = 'unblock';
    public description = 'unblock user manually';
    public isDefault = true;
    public cli = UserUnBlockCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class UserUnBlockSection extends ScenarioSection {
    public prefix = 'unblock';
    public description = 'UnBlock user through API';
    public cases = [new UserUnBlockScenario()];
}