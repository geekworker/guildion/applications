import { UsersEndpoint, Device, UserDestroyRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { UserEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<UserDestroyRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class UserDestroyCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new UserDestroyRequest({
                id: APIContext.current().currentUser?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, UserEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentUser?.id ?? '';
        return props;
    } 
}

export class UserDestroyScenario extends APIScenario(UsersEndpoint.Destroy) {
    public prefix = 'destroy';
    public description = 'destroy user manually';
    public isDefault = true;
    public cli = UserDestroyCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.success && APIContext.current().currentUser?.id == props.data.id) {
            APIContext.current().currentUser = undefined;
            await APIContext.instance().save();
        }
        if (result.success && APIContext.current().currentMember?.userId == props.data.id) {
            APIContext.current().currentMember = undefined;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class UserDestroySection extends ScenarioSection {
    public prefix = 'destroy';
    public description = 'Destroy user through API';
    public cases = [new UserDestroyScenario()];
}