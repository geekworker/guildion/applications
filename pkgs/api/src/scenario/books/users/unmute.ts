import { UsersEndpoint, Device, UserUnMuteRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { UserEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<UserUnMuteRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class UserUnMuteCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new UserUnMuteRequest({
                id: APIContext.current().currentUser?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, UserEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentUser?.id ?? '';
        return props;
    } 
}

export class UserUnMuteScenario extends APIScenario(UsersEndpoint.UnMute) {
    public prefix = 'unmute';
    public description = 'unmute user manually';
    public isDefault = true;
    public cli = UserUnMuteCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class UserUnMuteSection extends ScenarioSection {
    public prefix = 'unmute';
    public description = 'UnMute user through API';
    public cases = [new UserUnMuteScenario()];
}