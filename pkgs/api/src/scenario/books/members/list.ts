import { MembersEndpoint, Device, MembersListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MembersListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MembersListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MembersListRequest({
                guildId: APIContext.current().currentGuild?.id ?? '',
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.guildId = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: false,
        });
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: false,
        });
        return props;
    } 
}

export class MembersListScenario extends APIScenario(MembersEndpoint.List) {
    public prefix = 'list';
    public description = 'list members manually';
    public isDefault = true;
    public cli = MembersListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.members);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MembersListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List member through API';
    public cases = [new MembersListScenario()];
}