import { MembersEndpoint, Device, MemberUpdateRequest, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { FileEntity, MemberEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MemberUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MemberUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MemberUpdateRequest({
                member: {
                    id: APIContext.current().currentMember?.id ?? '',
                    ...APIContext.current().currentMember,
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.member.id = (await entitySelectQuestion(cli, MemberEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentMember?.id ?? '';
        props.data.member.displayName = await answerPropsQuestion(cli, {
            props: props.data.member,
            propsname: 'member',
            key: "displayName",
            nullable: true,
        }) || props.data.member.displayName;
        props.data.member.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        props.data.member.description = await answerPropsQuestion(cli, {
            props: props.data.member,
            propsname: 'member',
            key: "description",
            nullable: true,
        }) || props.data.member.description;
        return props;
    } 
}

export class MemberUpdateScenario extends APIScenario(MembersEndpoint.Update) {
    public prefix = 'update';
    public description = 'update member manually';
    public isDefault = true;
    public cli = MemberUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.member) {
            APIContext.current().currentMember = result.member;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MemberUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update member through API';
    public cases = [new MemberUpdateScenario()];
}