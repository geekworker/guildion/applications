import { MembersEndpoint, Device, MembersSearchRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MembersSearchRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MembersSearchCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MembersSearchRequest({
                query: '',
                guildId: APIContext.current().currentGuild?.id ?? '',
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.guildId = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.query = (await answerPropsQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'query',
            nullable: false,
        }))!;
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: false,
        });
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: false,
        });
        return props;
    } 
}

export class MembersSearchScenario extends APIScenario(MembersEndpoint.Search) {
    public prefix = 'search';
    public description = 'search members manually';
    public isDefault = true;
    public cli = MembersSearchCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.members);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MembersSearchSection extends ScenarioSection {
    public prefix = 'search';
    public description = 'Search member through API';
    public cases = [new MembersSearchScenario()];
}