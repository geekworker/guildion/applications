import { ScenarioBook } from "@guildion/node";
// import { MembersSeedSection } from "./seed";
import { MemberUpdateSection } from "./update";
import { MemberDestroySection } from "./destroy";
import { MemberKickSection } from "./kick";
import { MembersListSection } from "./list";
import { MembersSearchSection } from "./search";

export class MembersBook extends ScenarioBook {
    public prefix = 'members';
    public description = 'members scenario books';
    public cases = [
        new MemberUpdateSection(),
        new MemberDestroySection(),
        new MemberKickSection(),
        new MembersListSection(),
        new MembersSearchSection(),
    ];
}