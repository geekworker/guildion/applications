import { MembersEndpoint, Device, MemberDestroyRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { MemberEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MemberDestroyRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MemberDestroyCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MemberDestroyRequest({
                id: APIContext.current().currentMember?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, MemberEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentMember?.id ?? '';
        return props;
    } 
}

export class MemberDestroyScenario extends APIScenario(MembersEndpoint.Destroy) {
    public prefix = 'destroy';
    public description = 'destroy member manually';
    public isDefault = true;
    public cli = MemberDestroyCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.success && APIContext.current().currentMember?.id == props.data.id) {
            APIContext.current().currentMember = undefined;
            if (result.success && APIContext.current().currentGuild?.id == APIContext.current().currentMember?.guildId) {
                APIContext.current().currentGuild = undefined;
            }
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MemberDestroySection extends ScenarioSection {
    public prefix = 'destroy';
    public description = 'Destroy member through API';
    public cases = [new MemberDestroyScenario()];
}