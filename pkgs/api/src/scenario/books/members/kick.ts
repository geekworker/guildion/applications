import { MembersEndpoint, MemberKickRequest, Device } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { MemberEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MemberKickRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MemberKickCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MemberKickRequest({
                id: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, MemberEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentMember?.id ?? '';
        return props;
    } 
}

export class MemberKickScenario extends APIScenario(MembersEndpoint.Kick) {
    public prefix = 'kick';
    public description = 'kick member manually';
    public isDefault = true;
    public cli = MemberKickCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.success && APIContext.current().currentMember?.id == props.data.id) {
            APIContext.current().currentMember = undefined;
            if (result.success && APIContext.current().currentGuild?.id == APIContext.current().currentMember?.guildId) {
                APIContext.current().currentGuild = undefined;
            }
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MemberKickSection extends ScenarioSection {
    public prefix = 'kick';
    public description = 'Kick member through API';
    public cases = [new MemberKickScenario()];
}