import { GuildFilesEndpoint, Device, GuildFileCreateRequest, File, FileExtension, ContentType, contentTypeToFileType, HTTPStatusCode } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, selectEnumQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildFileCreateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildFileCreateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildFileCreateRequest({
                file: new File({
                    contentType: ContentType.png,
                }).toJSON(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.file.guildId = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.file.contentType = await selectEnumQuestion(cli, {
            props: ContentType,
            propsname: 'contentType',
            key: 'png',
            defaultValue: ContentType.png,
            nullable: true,
        }) ?? props.data.file.contentType;
        props.data.file.type = contentTypeToFileType(props.data.file.contentType);
        props.data.file.extension = FileExtension(props.data.file.contentType);
        props.data.file.displayName = await answerPropsQuestion(cli, {
            props: props.data.file,
            propsname: 'file',
            key: "displayName",
            nullable: true,
        }) || props.data.file.displayName;
        props.data.file.description = await answerPropsQuestion(cli, {
            props: props.data.file,
            propsname: 'file',
            key: "description",
            nullable: true,
        }) || props.data.file.description;
        return props;
    } 
}

export class GuildFileCreateScenario extends APIScenario(GuildFilesEndpoint.Create) {
    public prefix = 'create';
    public description = 'create guild file manually';
    public isDefault = true;
    public cli = GuildFileCreateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.status == HTTPStatusCode.OK && result.file.presignedURL) {
            if (!APIContext.current().presigneds) APIContext.current().presigneds = [];
            APIContext.current().presigneds?.push(result.file);
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFileCreateSection extends ScenarioSection {
    public prefix = 'create';
    public description = 'create guild file through API';
    public cases = [new GuildFileCreateScenario()];
}