import { ScenarioBook } from "@guildion/node";
import { GuildFileCreateSection } from "./create";
import { GuildFilesListSection } from "./list";
import { GuildFilesRecentListSection } from "./recentList";

export class GuildFilesBook extends ScenarioBook {
    public prefix = 'guildFiles';
    public description = 'guild files scenario books';
    public cases = [
        new GuildFileCreateSection(),
        new GuildFilesListSection(),
        new GuildFilesRecentListSection(),
    ];
}