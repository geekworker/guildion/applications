import { GuildFilesEndpoint, Device, GuildFilesListRequest, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildFilesListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildFilesListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildFilesListRequest({
                id: APIContext.current().currentGuild?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        return props;
    } 
}

export class GuildFilesListScenario extends APIScenario(GuildFilesEndpoint.List) {
    public prefix = 'list';
    public description = 'list guild files manually';
    public isDefault = true;
    public cli = GuildFilesListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.files);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFileAlbumsListScenario extends APIScenario(GuildFilesEndpoint.List) {
    public prefix = 'albumsList';
    public description = 'list guild albums manually';
    public isDefault = false;
    public cli = GuildFilesListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        props.data.types = [FileType.Image]
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.files);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFilePlaylistsListScenario extends APIScenario(GuildFilesEndpoint.List) {
    public prefix = 'playlistsList';
    public description = 'list guild playlists manually';
    public isDefault = false;
    public cli = GuildFilesListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        props.data.types = [FileType.Video]
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.files);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFilesListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List guildFiles through API';
    public cases = [new GuildFilesListScenario(), new GuildFileAlbumsListScenario(), new GuildFilePlaylistsListScenario()];
}