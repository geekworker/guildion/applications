import { GuildFilesEndpoint, Device, GuildFilesRecentListRequest, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildFilesRecentListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildFilesRecentListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildFilesRecentListRequest({
                id: APIContext.current().currentGuild?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        return props;
    } 
}

export class GuildFilesRecentListScenario extends APIScenario(GuildFilesEndpoint.RecentList) {
    public prefix = 'recentList';
    public description = 'recent list guild files manually';
    public isDefault = true;
    public cli = GuildFilesRecentListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFileAlbumsRecentListScenario extends APIScenario(GuildFilesEndpoint.RecentList) {
    public prefix = 'albumsRecentList';
    public description = 'recent list guild albums manually';
    public isDefault = false;
    public cli = GuildFilesRecentListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        props.data.types = [FileType.Image]
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFilePlaylistsRecentListScenario extends APIScenario(GuildFilesEndpoint.RecentList) {
    public prefix = 'playlistsRecentList';
    public description = 'recent list guild playlists manually';
    public isDefault = false;
    public cli = GuildFilesRecentListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        props.data.types = [FileType.Video]
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildFilesRecentListSection extends ScenarioSection {
    public prefix = 'recentList';
    public description = 'Recent list guildFiles through API';
    public cases = [new GuildFilesRecentListScenario(), new GuildFileAlbumsRecentListScenario(), new GuildFilePlaylistsRecentListScenario()];
}