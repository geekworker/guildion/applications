import { RoomLogsEndpoint, Device, RoomLogsListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsDateQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomLogsListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomLogsListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomLogsListRequest({
                roomId: APIContext.current().currentRoom?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        });
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        });
        const ltAt = await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'ltAt',
            nullable: true,
        }) || props.data.ltAt;
        props.data.ltAt = ltAt ? `${ltAt}` : undefined;
        const gtAt = await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'ltAt',
            nullable: true,
        }) || props.data.ltAt;
        props.data.gtAt = gtAt ? `${gtAt}` : undefined;
        return props;
    } 
}

export class RoomLogsListScenario extends APIScenario(RoomLogsEndpoint.List) {
    public prefix = 'list';
    public description = 'list room logs manually';
    public isDefault = true;
    public cli = RoomLogsListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.logs);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomLogsListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List room logs through API';
    public cases = [new RoomLogsListScenario()];
}