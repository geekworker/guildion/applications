import { ScenarioBook } from "@guildion/node";
import { RoomLogsListSection } from "./list";

export class RoomLogsBook extends ScenarioBook {
    public prefix = 'roomLogs';
    public description = 'roomLogs scenario books';
    public cases = [
        new RoomLogsListSection(),
    ];
}