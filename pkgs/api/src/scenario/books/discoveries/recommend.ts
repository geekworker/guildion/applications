import { DiscoveriesEndpoint, Device, DiscoveriesRecommendRequest, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<DiscoveriesRecommendRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class DiscoveriesRecommendCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new DiscoveriesRecommendRequest({
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        return props;
    } 
}

export class DiscoveriesRecommendScenario extends APIScenario(DiscoveriesEndpoint.Recommend) {
    public prefix = 'recommend';
    public description = 'recommend discoveries manually';
    public isDefault = true;
    public cli = DiscoveriesRecommendCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class DiscoveriesRecommendSection extends ScenarioSection {
    public prefix = 'recommend';
    public description = 'Recommend discoveries through API';
    public cases = [new DiscoveriesRecommendScenario()];
}