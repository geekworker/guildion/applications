import { ScenarioBook } from "@guildion/node";
import { DiscoveriesRecommendSection } from "./recommend";

export class DiscoveriesBook extends ScenarioBook {
    public prefix = 'discoveries';
    public description = 'discoveries scenario books';
    public cases = [
        new DiscoveriesRecommendSection(),
    ];
}