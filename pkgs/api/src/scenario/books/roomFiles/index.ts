import { ScenarioBook } from "@guildion/node";
import { RoomFilesListSection } from "./list";
import { RoomFileCreateSection } from "./create";

export class RoomFilesBook extends ScenarioBook {
    public prefix = 'roomFiles';
    public description = 'room files scenario books';
    public cases = [
        new RoomFileCreateSection(),
        new RoomFilesListSection(),
    ];
}