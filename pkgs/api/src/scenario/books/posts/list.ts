import { PostsEndpoint, Device, PostsListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsDateQuestion, entitySelectQuestion, answerPropsBooleanQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { PostEntity, RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<PostsListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class PostsListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new PostsListRequest({
                roomId: APIContext.current().currentRoom?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken,
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        props.data.postId = (await entitySelectQuestion(cli, PostEntity, { attribute: 'body', allowNull: true }))?.id;
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        props.data.isPublic = await answerPropsBooleanQuestion(cli, {
            props: props.data,
            propsname: 'data',
            key: "isPublic",
            nullable: true,
        }) ?? props.data.isPublic;
        return props;
    } 
}

export class PostsListScenario extends APIScenario(PostsEndpoint.List) {
    public prefix = 'list';
    public description = 'list posts manually';
    public isDefault = true;
    public cli = PostsListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class PostsListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List post through API';
    public cases = [new PostsListScenario()];
}