import { PostsEndpoint, Device, PostCreateRequest, Post, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitiesSelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import faker from 'faker';
import { FileEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<PostCreateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class PostCreateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new PostCreateRequest({
                post: new Post({
                    roomId: APIContext.current().currentRoom?.id!,
                    body: faker.lorem.sentences(1),
                }).toJSON(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.post.body = await answerPropsQuestion(cli, {
            props: props.data.post,
            propsname: 'post',
            key: "body",
            nullable: true,
        }) || props.data.post.body;
        const files = (await entitiesSelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }));
        props.data.post.files = await Promise.all(files.map(file => file.transform()));
        return props;
    } 
}

export class PostCreateScenario extends APIScenario(PostsEndpoint.Create) {
    public prefix = 'create';
    public description = 'create post manually';
    public isDefault = true;
    public cli = PostCreateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        if (result.post) {
            APIContext.current().currentPost = result.post;
            await APIContext.instance().save();
        }
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class PostCreateSection extends ScenarioSection {
    public prefix = 'create';
    public description = 'Create post through API';
    public cases = [new PostCreateScenario()];
}