import { PostsEndpoint, Device, PostUpdateRequest, Post, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitiesSelectQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { FileEntity, PostEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<PostUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class PostUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new PostUpdateRequest({
                post: {
                    id: '',
                    ...new Post().toJSON(),
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.post.id = (await entitySelectQuestion(cli, PostEntity, { attribute: 'body', allowNull: true }))?.id ?? '';
        props.data.post.body = await answerPropsQuestion(cli, {
            props: props.data.post,
            propsname: 'post',
            key: "body",
            nullable: true,
        }) || props.data.post.body;
        const files = (await entitiesSelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }));
        props.data.post.files = await Promise.all(files.map(file => file.transform()));
        return props;
    } 
}

export class PostUpdateScenario extends APIScenario(PostsEndpoint.Update) {
    public prefix = 'update';
    public description = 'update post manually';
    public isDefault = true;
    public cli = PostUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class PostUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update post through API';
    public cases = [new PostUpdateScenario()];
}