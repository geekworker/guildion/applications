import { ScenarioBook } from "@guildion/node";
import { PostCreateSection } from "./create";
import { PostDestroySection } from "./destroy";
import { PostUpdateSection } from "./update";
import { PostsListSection } from "./list";
import { PostsSearchSection } from "./search";
import { PostShowSection } from "./show";

export class PostsBook extends ScenarioBook {
    public prefix = 'posts';
    public description = 'posts scenario books';
    public cases = [
        new PostsListSection(),
        new PostShowSection(),
        new PostCreateSection(),
        new PostUpdateSection(),
        new PostDestroySection(),
        new PostsSearchSection(),
    ];
}