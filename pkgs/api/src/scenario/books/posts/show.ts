import { PostsEndpoint, Device, PostShowRequest } from "@guildion/core";
import { PostEntity } from "@guildion/db";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<PostShowRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class PostShowCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new PostShowRequest({
                id: APIContext.current().currentPost?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken,
        }
        props.data.id = (await entitySelectQuestion(cli, PostEntity, { attribute: 'body', allowNull: true }))?.id ?? APIContext.current().currentPost?.id ?? '';
        return props;
    } 
}

export class PostShowScenario extends APIScenario(PostsEndpoint.Show) {
    public prefix = 'show';
    public description = 'show post manually';
    public isDefault = true;
    public cli = PostShowCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.post) {
            APIContext.current().currentPost = result.post;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class PostShowSection extends ScenarioSection {
    public prefix = 'show';
    public description = 'Show post through API';
    public cases = [new PostShowScenario()];
}