import { RoomsEndpoint, Device, RoomsSearchRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<RoomsSearchRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomsSearchCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomsSearchRequest({
                query: '',
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.query = await answerPropsQuestion(cli, {
            props: props.data,
            propsname: 'query',
            key: "query",
            nullable: false,
        }) || props.data.query;
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        return props;
    } 
}

export class RoomsSearchScenario extends APIScenario(RoomsEndpoint.Search) {
    public prefix = 'search';
    public description = 'search rooms manually';
    public isDefault = true;
    public cli = RoomsSearchCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.rooms);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomsSearchSection extends ScenarioSection {
    public prefix = 'search';
    public description = 'Search room through API';
    public cases = [new RoomsSearchScenario()];
}