import { RoomsEndpoint, Device, RoomMemberUpdateRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<RoomMemberUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomMemberUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomMemberUpdateRequest({
                member: {
                    id: APIContext.current().currentMember?.id ?? '',
                    roomId: APIContext.current().currentMember?.roomId ?? '',
                    ...APIContext.current().currentMember,
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        return props;
    } 
}

export class RoomMemberUpdateScenario extends APIScenario(RoomsEndpoint.MemberUpdate) {
    public prefix = 'update';
    public description = 'update room manually';
    public isDefault = true;
    public cli = RoomMemberUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomMemberUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update room through API';
    public cases = [new RoomMemberUpdateScenario()];
}