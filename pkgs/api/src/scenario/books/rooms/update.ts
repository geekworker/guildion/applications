import { RoomsEndpoint, Device, RoomUpdateRequest, RoomType, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, answerPropsBooleanQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { FileEntity, RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomUpdateRequest({
                room: {
                    id: APIContext.current().currentRoom?.id ?? '',
                    ...APIContext.current().currentRoom,
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.room.id = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        props.data.room.displayName = await answerPropsQuestion(cli, {
            props: props.data.room,
            propsname: 'room',
            key: "displayName",
            nullable: true,
        }) || props.data.room.displayName;
        props.data.room.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        props.data.room.description = await answerPropsQuestion(cli, {
            props: props.data.room,
            propsname: 'room',
            key: "description",
            nullable: true,
        }) || props.data.room.description;
        const isPrivate = await answerPropsBooleanQuestion(cli, {
            props: props.data.room,
            propsname: 'room',
            key: "type",
            nullable: true,
        });
        props.data.room.type = isPrivate ? RoomType.PRIVATE : RoomType.PUBLIC;
        return props;
    } 
}

export class RoomUpdateScenario extends APIScenario(RoomsEndpoint.Update) {
    public prefix = 'update';
    public description = 'update room manually';
    public isDefault = true;
    public cli = RoomUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.room) {
            APIContext.current().currentRoom = result.room;
            APIContext.current().currentMember = result.room.currentMember;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update room through API';
    public cases = [new RoomUpdateScenario()];
}