import { RoomsEndpoint, Device, RoomMemberKickRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoomMemberEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomMemberKickRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomMemberKickCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomMemberKickRequest({
                id: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, RoomMemberEntity, { attribute: 'memberId', allowNull: false }))!.id;
        return props;
    } 
}

export class RoomMemberKickScenario extends APIScenario(RoomsEndpoint.MemberKick) {
    public prefix = 'memberKick';
    public description = 'kick room member manually';
    public isDefault = true;
    public cli = RoomMemberKickCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomMemberKickSection extends ScenarioSection {
    public prefix = 'memberKick';
    public description = 'MemberKick room through API';
    public cases = [new RoomMemberKickScenario()];
}