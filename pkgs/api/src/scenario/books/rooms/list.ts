import { RoomsEndpoint, Device, RoomsListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsDateQuestion, entitySelectQuestion, answerPropsBooleanQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomsListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomsListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomsListRequest({
                guildId: APIContext.current().currentGuild?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.guildId = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        const ltAt = (await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'ltAt',
            nullable: true,
        }))?.toISOString() || props.data.ltAt;
        props.data.ltAt = ltAt;
        const gtAt = (await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'gtAt',
            nullable: true,
        }))?.toISOString() || props.data.gtAt;
        props.data.gtAt = gtAt;
        props.data.isPublic = await answerPropsBooleanQuestion(cli, {
            props: props.data,
            propsname: 'data',
            key: "isPublic",
            nullable: true,
        }) ?? props.data.isPublic;
        return props;
    } 
}

export class RoomsListScenario extends APIScenario(RoomsEndpoint.List) {
    public prefix = 'list';
    public description = 'list rooms manually';
    public isDefault = true;
    public cli = RoomsListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.rooms);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomsListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List room through API';
    public cases = [new RoomsListScenario()];
}