import { ScenarioBook } from "@guildion/node";
import { RoomCreateSection } from "./create";
import { RoomUpdateSection } from "./update";
import { RoomDestroySection } from "./destroy";
import { RoomsListSection } from "./list";
import { RoomsSearchSection } from "./search";
import { RoomShowSection } from "./show";
import { RoomJoinSection } from "./join";
import { RoomLeaveSection } from "./leave";
import { RoomMemberUpdateSection } from "./memberUpdate";
import { RoomMembersListSection } from "./membersList";
import { RoomMemberKickSection } from "./memberKick";
import { RoomConnectingMembersListSection } from "./connectingMembersList";

export class RoomsBook extends ScenarioBook {
    public prefix = 'rooms';
    public description = 'rooms scenario books';
    public cases = [
        new RoomCreateSection(),
        new RoomUpdateSection(),
        new RoomDestroySection(),
        new RoomConnectingMembersListSection(),
        new RoomsListSection(),
        new RoomsSearchSection(),
        new RoomShowSection(),
        new RoomJoinSection(),
        new RoomLeaveSection(),
        new RoomMemberUpdateSection(),
        new RoomMembersListSection(),
        new RoomMemberKickSection(),
    ];
}