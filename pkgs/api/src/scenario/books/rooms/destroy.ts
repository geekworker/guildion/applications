import { RoomsEndpoint, Device, RoomDestroyRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomDestroyRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomDestroyCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomDestroyRequest({
                id: APIContext.current().currentRoom?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        return props;
    } 
}

export class RoomDestroyScenario extends APIScenario(RoomsEndpoint.Destroy) {
    public prefix = 'destroy';
    public description = 'destroy room manually';
    public isDefault = true;
    public cli = RoomDestroyCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.success && APIContext.current().currentRoom?.id == props.data.id) {
            APIContext.current().currentRoom = undefined;
            await APIContext.instance().save();
        }
        if (result.success && APIContext.current().currentMember?.roomId == props.data.id) {
            APIContext.current().currentMember = undefined;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomDestroySection extends ScenarioSection {
    public prefix = 'destroy';
    public description = 'Destroy room through API';
    public cases = [new RoomDestroyScenario()];
}