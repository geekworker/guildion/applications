import { RoomsEndpoint, Device, RoomConnectingMembersListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsDateQuestion, answerPropsBooleanQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<RoomConnectingMembersListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomConnectingMembersListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomConnectingMembersListRequest({
                roomId: APIContext.current().currentRoom?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: false,
        });
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: false,
        });
        const ltAt = await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'ltAt',
            nullable: true,
        }) || props.data.ltAt;
        props.data.ltAt = ltAt ? `${ltAt}` : undefined;
        const gtAt = await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'ltAt',
            nullable: true,
        }) || props.data.ltAt;
        props.data.gtAt = gtAt ? `${gtAt}` : undefined;
        return props;
    } 
}

export class RoomConnectingMembersListScenario extends APIScenario(RoomsEndpoint.MembersList) {
    public prefix = 'connectingMembersList';
    public description = 'list connecting room members manually';
    public isDefault = true;
    public cli = RoomConnectingMembersListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(result.members);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomConnectingMembersListSection extends ScenarioSection {
    public prefix = 'membersList';
    public description = 'List connecting room members through API';
    public cases = [new RoomConnectingMembersListScenario()];
}