import { RoomsEndpoint, Device, RoomShowRequest } from "@guildion/core";
import { RoomEntity } from "@guildion/db";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion, answerPropsBooleanQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<RoomShowRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomShowCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomShowRequest({
                id: APIContext.current().currentRoom?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        props.data.isPublic = await answerPropsBooleanQuestion(cli, {
            props: props.data,
            propsname: 'data',
            key: "isPublic",
            nullable: true,
        }) ?? props.data.isPublic;
        return props;
    } 
}

export class RoomShowScenario extends APIScenario(RoomsEndpoint.Show) {
    public prefix = 'show';
    public description = 'show room manually';
    public isDefault = true;
    public cli = RoomShowCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.room) {
            APIContext.current().currentRoom = result.room;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomShowSection extends ScenarioSection {
    public prefix = 'show';
    public description = 'Show room through API';
    public cases = [new RoomShowScenario()];
}