import { RoomsEndpoint, Device, RoomCreateRequest, Room, LanguageCode, CountryCode, RoomType, FileType, RoleType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, answerPropsBooleanQuestion, entitySelectQuestion, entitiesSelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import faker from 'faker';
import { FileEntity, MemberEntity, RoleEntity } from "@guildion/db";
import { Not } from 'typeorm';

interface Props extends ScenarioProps {
    data: ReturnType<RoomCreateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class RoomCreateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new RoomCreateRequest({
                room: new Room({
                    ownerId: APIContext.current().currentMember?.id!,
                    guildId: APIContext.current().currentGuild?.id!,
                    displayName: faker.name.title(),
                    description: '',
                    languageCode: LanguageCode.English,
                    countryCode: CountryCode.JAPAN,
                }).toJSON(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.room.displayName = await answerPropsQuestion(cli, {
            props: props.data.room,
            propsname: 'room',
            key: "displayName",
            nullable: true,
        }) || props.data.room.displayName;
        props.data.room.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        const isPrivate = await answerPropsBooleanQuestion(cli, {
            props: props.data.room,
            propsname: 'room',
            key: "type",
            nullable: true,
        });
        props.data.room.type = isPrivate ? RoomType.PRIVATE : RoomType.PUBLIC;
        const members = await entitiesSelectQuestion(
            cli,
            MemberEntity,
            { 
                where: { 
                    guildId: props.data.room.guildId,
                },
                count: 100,
                attribute: 'displayName',
            }
        );
        props.data.room.members = await Promise.all(members.map(member => member.transform()));
        const roles = await entitiesSelectQuestion(
            cli,
            RoleEntity,
            { 
                where: { 
                    guildId: props.data.room.guildId,
                },
                count: 100,
                attribute: 'type',
            }
        );
        props.data.room.roles = await Promise.all(roles.map(role => role.transform()));
        return props;
    } 
}

export class RoomCreateScenario extends APIScenario(RoomsEndpoint.Create) {
    public prefix = 'create';
    public description = 'create room manually';
    public isDefault = true;
    public cli = RoomCreateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.room) {
            APIContext.current().currentRoom = result.room;
            APIContext.current().currentMember = result.room.currentMember;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class RoomCreateSection extends ScenarioSection {
    public prefix = 'create';
    public description = 'Create room through API';
    public cases = [new RoomCreateScenario()];
}