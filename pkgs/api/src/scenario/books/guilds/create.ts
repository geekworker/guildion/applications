import { GuildsEndpoint, Device, GuildCreateRequest, Guild, LanguageCode, CountryCode, GuildType, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, answerPropsBooleanQuestion, entitySelectQuestion, selectEnumQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import faker from 'faker';
import { FileEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildCreateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildCreateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildCreateRequest({
                guild: new Guild({
                    ownerId: APIContext.current().currentUser?.id!,
                    displayName: faker.name.title(),
                    description: '',
                    languageCode: LanguageCode.English,
                    countryCode: CountryCode.JAPAN,
                }).toJSON(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.guild.displayName = await answerPropsQuestion(cli, {
            props: props.data.guild,
            propsname: 'guild',
            key: "displayName",
            nullable: true,
        }) || props.data.guild.displayName;
        props.data.guild.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        props.data.guild.type = await selectEnumQuestion(cli, {
            props: GuildType,
            propsname: 'type',
            key: 'PUBLIC',
            defaultValue: GuildType.PRIVATE,
            nullable: true,
        });
        return props;
    } 
}

export class GuildCreateScenario extends APIScenario(GuildsEndpoint.Create) {
    public prefix = 'create';
    public description = 'create guild manually';
    public isDefault = true;
    public cli = GuildCreateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.guild) {
            APIContext.current().currentGuild = result.guild;
            APIContext.current().currentMember = result.guild.currentMember;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildCreateSection extends ScenarioSection {
    public prefix = 'create';
    public description = 'Create guild through API';
    public cases = [new GuildCreateScenario()];
}