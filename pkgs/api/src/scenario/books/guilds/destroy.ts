import { GuildsEndpoint, Device, GuildDestroyRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildDestroyRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildDestroyCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildDestroyRequest({
                id: APIContext.current().currentGuild?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        return props;
    } 
}

export class GuildDestroyScenario extends APIScenario(GuildsEndpoint.Destroy) {
    public prefix = 'destroy';
    public description = 'destroy guild manually';
    public isDefault = true;
    public cli = GuildDestroyCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.success && APIContext.current().currentGuild?.id == props.data.id) {
            APIContext.current().currentGuild = undefined;
            await APIContext.instance().save();
        }
        if (result.success && APIContext.current().currentMember?.guildId == props.data.id) {
            APIContext.current().currentMember = undefined;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildDestroySection extends ScenarioSection {
    public prefix = 'destroy';
    public description = 'Destroy guild through API';
    public cases = [new GuildDestroyScenario()];
}