import { GuildsEndpoint, Device, GuildLeaveRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildLeaveRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildLeaveCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildLeaveRequest({
                id: APIContext.current().currentGuild?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        return props;
    } 
}

export class GuildLeaveScenario extends APIScenario(GuildsEndpoint.Leave) {
    public prefix = 'leave';
    public description = 'leave guild manually';
    public isDefault = true;
    public cli = GuildLeaveCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildLeaveSection extends ScenarioSection {
    public prefix = 'leave';
    public description = 'Leave guild through API';
    public cases = [new GuildLeaveScenario()];
}