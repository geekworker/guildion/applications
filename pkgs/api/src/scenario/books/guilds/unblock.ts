import { GuildsEndpoint, Device, GuildUnBlockRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity, UserEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildUnBlockRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildUnBlockCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildUnBlockRequest({
                id: APIContext.current().currentGuild?.id ?? '',
                receiverId:'',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.receiverId = (await entitySelectQuestion(cli, UserEntity, { attribute: 'displayName', allowNull: false }))!.id;
        return props;
    } 
}

export class GuildUnBlockScenario extends APIScenario(GuildsEndpoint.UnBlock) {
    public prefix = 'unblock';
    public description = 'unblock guild manually';
    public isDefault = true;
    public cli = GuildUnBlockCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildUnBlockSection extends ScenarioSection {
    public prefix = 'unblock';
    public description = 'UnBlock guild through API';
    public cases = [new GuildUnBlockScenario()];
}