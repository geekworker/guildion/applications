import { GuildsEndpoint, Device, GuildShowRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion, answerPropsBooleanQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildShowRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildShowCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildShowRequest({
                id: APIContext.current().currentGuild?.id ?? '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.isPublic = await answerPropsBooleanQuestion(cli, {
            props: props.data,
            propsname: 'data',
            key: "isPublic",
            nullable: true,
        }) ?? props.data.isPublic;
        return props;
    } 
}

export class GuildShowScenario extends APIScenario(GuildsEndpoint.Show) {
    public prefix = 'show';
    public description = 'show guild manually';
    public isDefault = true;
    public cli = GuildShowCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.guild) {
            APIContext.current().currentGuild = result.guild;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildShowSection extends ScenarioSection {
    public prefix = 'show';
    public description = 'Show guild through API';
    public cases = [new GuildShowScenario()];
}