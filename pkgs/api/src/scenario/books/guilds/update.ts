import { GuildsEndpoint, Device, GuildUpdateRequest, GuildType, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, answerPropsBooleanQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { FileEntity, GuildEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<GuildUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class GuildUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new GuildUpdateRequest({
                guild: {
                    id: APIContext.current().currentGuild?.id ?? '',
                    ...APIContext.current().currentGuild,
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.guild.id = (await entitySelectQuestion(cli, GuildEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentGuild?.id ?? '';
        props.data.guild.displayName = await answerPropsQuestion(cli, {
            props: props.data.guild,
            propsname: 'guild',
            key: "displayName",
            nullable: true,
        }) || props.data.guild.displayName;
        props.data.guild.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        props.data.guild.description = await answerPropsQuestion(cli, {
            props: props.data.guild,
            propsname: 'guild',
            key: "description",
            nullable: true,
        }) || props.data.guild.description;
        const isPrivate = await answerPropsBooleanQuestion(cli, {
            props: props.data.guild,
            propsname: 'guild',
            key: "type",
            nullable: true,
        });
        props.data.guild.type = isPrivate ? GuildType.PRIVATE : GuildType.PUBLIC;
        return props;
    } 
}

export class GuildUpdateScenario extends APIScenario(GuildsEndpoint.Update) {
    public prefix = 'update';
    public description = 'update guild manually';
    public isDefault = true;
    public cli = GuildUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.guild) {
            APIContext.current().currentGuild = result.guild;
            APIContext.current().currentMember = result.guild.currentMember;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class GuildUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update guild through API';
    public cases = [new GuildUpdateScenario()];
}