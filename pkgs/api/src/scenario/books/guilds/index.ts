import { ScenarioBook } from "@guildion/node";
import { GuildCreateSection } from "./create";
import { GuildUpdateSection } from "./update";
import { GuildDestroySection } from "./destroy";
import { GuildShowSection } from "./show";
import { GuildsListSection } from "./list";
import { GuildsSearchSection } from "./search";
import { GuildJoinSection } from "./join";
import { GuildLeaveSection } from "./leave";
import { GuildBlockSection } from "./block";
import { GuildUnBlockSection } from "./unblock";

export class GuildsBook extends ScenarioBook {
    public prefix = 'guilds';
    public description = 'guilds scenario books';
    public cases = [
        new GuildCreateSection(),
        new GuildUpdateSection(),
        new GuildDestroySection(),
        new GuildShowSection(),
        new GuildsListSection(),
        new GuildsSearchSection(),
        new GuildJoinSection(),
        new GuildLeaveSection(),
        new GuildBlockSection(),
        new GuildUnBlockSection(),
    ];
}