import { ScenarioBook } from "@guildion/node";
import { MemberRoleMakeAdminSection } from "./makeAdmin";
import { MemberRoleRevertMembersSection } from "./revertMembers";
import { MemberRolesListSection } from "./list";

export class MemberRolesBook extends ScenarioBook {
    public prefix = 'memberRoles';
    public description = 'member roles scenario books';
    public cases = [
        new MemberRoleMakeAdminSection(),
        new MemberRoleRevertMembersSection(),
        new MemberRolesListSection(),
    ];
}