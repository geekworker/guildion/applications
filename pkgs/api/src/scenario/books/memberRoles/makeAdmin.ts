import { MemberRolesEndpoint, Device, MemberRoleMakeAdminRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { MemberEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MemberRoleMakeAdminRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MemberRoleMakeAdminCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MemberRoleMakeAdminRequest({
                memberId: APIContext.current().currentMember?.id!,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.memberId = (await entitySelectQuestion(cli, MemberEntity, { attribute: 'displayName' }))?.id ?? APIContext.current().currentMember?.id!;
        return props;
    } 
}

export class MemberRoleMakeAdminScenario extends APIScenario(MemberRolesEndpoint.MakeAdmin) {
    public prefix = 'makeAdmin';
    public description = 'make admin role manually';
    public isDefault = true;
    public cli = MemberRoleMakeAdminCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MemberRoleMakeAdminSection extends ScenarioSection {
    public prefix = 'makeAdmin';
    public description = 'MakeAdmin role through API';
    public cases = [new MemberRoleMakeAdminScenario()];
}