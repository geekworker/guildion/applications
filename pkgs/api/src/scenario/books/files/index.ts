import { ScenarioBook } from "@guildion/node";
import { FilesListSection } from "./list";
import { FileUploadSection } from "./upload";
import { FileProfilePresignSection } from "./profilePresign";
import { FileShowSection } from "./show";

export class FilesBook extends ScenarioBook {
    public prefix = 'files';
    public description = 'files scenario books';
    public cases = [
        new FilesListSection(),
        new FileProfilePresignSection(),
        new FileUploadSection(),
        new FileShowSection(),
    ];
}