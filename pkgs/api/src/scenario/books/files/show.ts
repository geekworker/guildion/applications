import { Device, File, FilesEndpoint, FileShowRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, selectArrayElementQuestion, APIScenario } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<FileShowRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class FileShowCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = {
            data: new FileShowRequest({
                id: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        const file = APIContext.current().presigneds ? await selectArrayElementQuestion(cli, {
            props: APIContext.current().presigneds!,
            propsname: 'presign files',
            nullable: true,
            defaultValue: APIContext.current().presigneds![APIContext.current().presigneds!.length - 1],
            key: 'id',
        }) : new File().toJSON();
        props.data.id = file.id ?? '';
        return props;
    } 
}

export class FileShowScenario extends APIScenario(FilesEndpoint.Show) {
    public prefix = 'show';
    public description = 'show file with s3 presignedURL';
    public isDefault = true;
    public cli = FileShowCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(104, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class FileShowSection extends ScenarioSection {
    public prefix = 'show';
    public description = 'Show file with s3 presignedURL';
    public cases = [new FileShowScenario()];
}