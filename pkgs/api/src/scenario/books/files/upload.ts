import { Device, File, FilesEndpoint, FileUploadRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, answerPropsQuestion, uploadLocalFile, selectArrayElementQuestion, APIScenario } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import path from 'path';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    localFilePath: string,
    presignedURL: string,
    data: ReturnType<FileUploadRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class FileUploadCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = {
            localFilePath: path.join(__dirname, '../../../../../abs/assets/brand_logos/original.png'),
            presignedURL: '',
            data: new FileUploadRequest({
                id: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        const file = APIContext.current().presigneds ? await selectArrayElementQuestion(cli, {
            props: APIContext.current().presigneds!,
            propsname: 'presign files',
            nullable: true,
            defaultValue: APIContext.current().presigneds![APIContext.current().presigneds!.length - 1],
            key: 'id',
        }) : new File().toJSON();
        props.data.id = file.id ?? '';
        props.presignedURL = file.presignedURL ?? '';
        props.localFilePath = await answerPropsQuestion(cli, {
            props: props,
            propsname: 'props',
            key: "localFilePath",
            nullable: true,
        }) || props.localFilePath;
        return props;
    } 
}

export class FileUploadScenario extends APIScenario(FilesEndpoint.Upload) {
    public prefix = 'upload';
    public description = 'upload file with s3 presignedURL';
    public isDefault = true;
    public cli = FileUploadCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(104, 0, { speed: "N/A" });
        await uploadLocalFile({
            localFilePath: props.localFilePath,
            presignedURL: props.presignedURL,
            onUploadProgress: (progressEvent) => {
                const progress = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                bar.update(progress);
            },
        });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (APIContext.current().presigneds && result.success) {
            APIContext.current().presigneds = APIContext.current().presigneds?.filter(presigned => presigned.id !== props.data.id);
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class FileUploadSection extends ScenarioSection {
    public prefix = 'upload';
    public description = 'Upload file with s3 presignedURL';
    public cases = [new FileUploadScenario()];
}