import { FilesEndpoint, Device, FilesListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import Colors from 'colors';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<FilesListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class SystemFilesListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new FilesListRequest({
                isJSONString: false,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        return props;
    } 
}

export class SystemFilesListJSONCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new FilesListRequest({
                isJSONString: true,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        return props;
    } 
}

export class SystemFilesListScenario extends APIScenario(FilesEndpoint.List) {
    public prefix = 'systemList';
    public description = 'list system files manually';
    public isDefault = true;
    public cli = SystemFilesListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(props.data.isJSONString ? result.getFilesFromJSON()?.toJSON() : result.files);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class SystemFilesListJSONScenario extends APIScenario(FilesEndpoint.List) {
    public prefix = 'jsonSystemList';
    public description = 'list system files json manually';
    public isDefault = true;
    public cli = SystemFilesListJSONCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        console.log(props.data.isJSONString ? result.getFilesFromJSON()?.toJSON() : result.files);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class FilesListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List file through API';
    public cases = [new SystemFilesListScenario(), new SystemFilesListJSONScenario()];
}