import { ScenarioBook } from "@guildion/node";
import { AccountRegisterSection } from "./register";
import { AccountAuthenticateSection } from "./authenticate";
import { AccountAllowSessionSection } from "./allowSession";
import { AccountDenySessionSection } from "./denySession";

export class AccountsBook extends ScenarioBook {
    public prefix = 'accounts';
    public description = 'accounts scenario books';
    public cases = [
        new AccountRegisterSection(),
        new AccountAuthenticateSection(),
        new AccountAllowSessionSection(),
        new AccountDenySessionSection(),
    ];
}