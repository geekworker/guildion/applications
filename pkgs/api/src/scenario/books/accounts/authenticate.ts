import { AccountsEndpoint, Device, AccountAuthenticateRequest } from "@guildion/core";
import { ScenarioSection, logger, NodeLogColor, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { ulid } from "ulid";
import { UserEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<AccountAuthenticateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
}

interface Finale extends ScenarioFinale {
}

export class AccountAuthenticateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new AccountAuthenticateRequest({
                username: ulid(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device()
        }
        const entity = await entitySelectQuestion(cli, UserEntity, { attribute: 'username' });
        if (entity) {
            props.data.username = entity.username;
        } else {
            props.data.username = (await answerPropsQuestion(cli, {
                props: props.data,
                propsname: 'account',
                key: "username",
                nullable: false,
            }))!
        }
        return props;
    } 
}

export class AccountAuthenticateScenario extends APIScenario(AccountsEndpoint.Authenticate) {
    public prefix = 'authenticate';
    public description = 'authenticate account manually';
    public isDefault = true;
    public cli = AccountAuthenticateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.accessToken && result.user) {
            APIContext.current().accessToken = result.accessToken;
            APIContext.current().currentUser = result.user;
            await APIContext.instance().save();
        }
        if (result.shouldTwoFactorAuth) logger.color(NodeLogColor.FgYellow, 'Please verity Two-Factor Authentication');
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class AccountAuthenticateSection extends ScenarioSection {
    public prefix = 'authenticate';
    public description = 'Authenticate account through API';
    public cases = [new AccountAuthenticateScenario()];
}