import { AccountsEndpoint, Device, AccountDenySessionRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<AccountDenySessionRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class AccountDenySessionCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new AccountDenySessionRequest({
                intervalSec: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        return props;
    } 
}

export class AccountDenySessionScenario extends APIScenario(AccountsEndpoint.DenySession) {
    public prefix = 'denySession';
    public description = 'denySession account manually';
    public isDefault = true;
    public cli = AccountDenySessionCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class AccountDenySessionSection extends ScenarioSection {
    public prefix = 'denySession';
    public description = 'DenySession account through API';
    public cases = [new AccountDenySessionScenario()];
}