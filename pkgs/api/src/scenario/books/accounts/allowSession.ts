import { AccountsEndpoint, Device, AccountAllowSessionRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";

interface Props extends ScenarioProps {
    data: ReturnType<AccountAllowSessionRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class AccountAllowSessionCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new AccountAllowSessionRequest({
                intervalSec: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.intervalSec = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'sec of interval for allowing session',
            key: 'intervalSec',
            nullable: false,
        }) || props.data.intervalSec;
        return props;
    } 
}

export class AccountAllowSessionScenario extends APIScenario(AccountsEndpoint.AllowSession) {
    public prefix = 'allowSession';
    public description = 'allowSession account manually';
    public isDefault = true;
    public cli = AccountAllowSessionCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class AccountAllowSessionSection extends ScenarioSection {
    public prefix = 'allowSession';
    public description = 'AllowSession account through API';
    public cases = [new AccountAllowSessionScenario()];
}