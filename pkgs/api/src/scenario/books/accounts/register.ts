import { AccountsEndpoint, Device, User, AccountRegisterRequest, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { ulid } from "ulid";
import { FileEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<AccountRegisterRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
}

interface Finale extends ScenarioFinale {
}

export class AccountRegisterCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new AccountRegisterRequest({
                user: new User({ username: ulid() }).toJSON(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device()
        }
        props.data.user.username = await answerPropsQuestion(cli, {
            props: props.data.user,
            propsname: 'user',
            key: "username",
            nullable: true,
        }) || props.data.user.username;
        props.data.user.profileId = (await entitySelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }))?.id;
        return props;
    } 
}

export class AccountRegisterScenario extends APIScenario(AccountsEndpoint.Register) {
    public prefix = 'register';
    public description = 'register account manually';
    public isDefault = true;
    public cli = AccountRegisterCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        if (result.accessToken && result.user) {
            APIContext.current().accessToken = result.accessToken;
            APIContext.current().currentUser = result.user;
            await APIContext.instance().save();
        }
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class AccountRegisterSection extends ScenarioSection {
    public prefix = 'register';
    public description = 'Register account through API';
    public cases = [new AccountRegisterScenario()];
}