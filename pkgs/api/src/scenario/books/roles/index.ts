import { ScenarioBook } from "@guildion/node";
import { RolesListSection } from "./list";

export class RolesBook extends ScenarioBook {
    public prefix = 'roles';
    public description = 'roles scenario books';
    public cases = [
        new RolesListSection(),
    ];
}