import { MessagesEndpoint, Device, MessageDestroyRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { MessageEntity, RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MessageDestroyRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MessageDestroyCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MessageDestroyRequest({
                id: '',
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        const roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id;
        props.data.id = (await entitySelectQuestion(cli, MessageEntity, { where: { roomId }, attribute: 'body', allowNull: true }))!.id;
        return props;
    } 
}

export class MessageDestroyScenario extends APIScenario(MessagesEndpoint.Destroy) {
    public prefix = 'destroy';
    public description = 'destroy message manually';
    public isDefault = true;
    public cli = MessageDestroyCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MessageDestroySection extends ScenarioSection {
    public prefix = 'destroy';
    public description = 'Destroy message through API';
    public cases = [new MessageDestroyScenario()];
}