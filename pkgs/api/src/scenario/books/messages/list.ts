import { MessagesEndpoint, Device, MessagesListRequest } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsNumberQuestion, answerPropsDateQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import { RoomEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MessagesListRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MessagesListCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MessagesListRequest({
                roomId: APIContext.current().currentRoom?.id!,
                count: 10,
                offset: 0,
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.roomId = (await entitySelectQuestion(cli, RoomEntity, { attribute: 'displayName', allowNull: true }))?.id ?? APIContext.current().currentRoom?.id ?? '';
        props.data.count = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'count',
            nullable: true,
        }) ?? props.data.count;
        props.data.offset = await answerPropsNumberQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'offset',
            nullable: true,
        }) ?? props.data.offset;
        const ltAt = (await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'ltAt',
            nullable: true,
        }))?.toISOString() || props.data.ltAt;
        props.data.ltAt = ltAt;
        const gtAt = (await answerPropsDateQuestion(cli, {
            props: props.data,
            propsname: 'LIST API',
            key: 'gtAt',
            nullable: true,
        }))?.toISOString() || props.data.gtAt;
        props.data.gtAt = gtAt;
        return props;
    } 
}

export class MessagesListScenario extends APIScenario(MessagesEndpoint.List) {
    public prefix = 'list';
    public description = 'list messages manually';
    public isDefault = true;
    public cli = MessagesListCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MessagesListSection extends ScenarioSection {
    public prefix = 'list';
    public description = 'List message through API';
    public cases = [new MessagesListScenario()];
}