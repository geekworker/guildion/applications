import { MessagesEndpoint, Device, MessageUpdateRequest, Message, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitiesSelectQuestion, entitySelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import faker from 'faker';
import { FileEntity, MessageEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MessageUpdateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MessageUpdateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MessageUpdateRequest({
                message: {
                    id: '',
                    ...new Message().toJSON(),
                },
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.message.id = (await entitySelectQuestion(cli, MessageEntity, { attribute: 'body', allowNull: true }))?.id ?? '';
        props.data.message.body = await answerPropsQuestion(cli, {
            props: props.data.message,
            propsname: 'message',
            key: "body",
            nullable: true,
        }) || props.data.message.body;
        const files = (await entitiesSelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }));
        props.data.message.files = await Promise.all(files.map(file => file.transform()));
        return props;
    } 
}

export class MessageUpdateScenario extends APIScenario(MessagesEndpoint.Update) {
    public prefix = 'update';
    public description = 'update message manually';
    public isDefault = true;
    public cli = MessageUpdateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MessageUpdateSection extends ScenarioSection {
    public prefix = 'update';
    public description = 'Update message through API';
    public cases = [new MessageUpdateScenario()];
}