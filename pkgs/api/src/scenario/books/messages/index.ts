import { ScenarioBook } from "@guildion/node";
import { MessageCreateSection } from "./create";
import { MessageDestroySection } from "./destroy";
import { MessageUpdateSection } from "./update";
import { MessagesListSection } from "./list";

export class MessagesBook extends ScenarioBook {
    public prefix = 'messages';
    public description = 'messages scenario books';
    public cases = [
        new MessagesListSection(),
        new MessageCreateSection(),
        new MessageUpdateSection(),
        new MessageDestroySection(),
    ];
}