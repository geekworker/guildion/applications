import { MessagesEndpoint, Device, MessageCreateRequest, Message, FileType } from "@guildion/core";
import { ScenarioSection, ScenarioProps, ScenarioCli, ScenarioFinale, APIScenario, answerPropsQuestion, entitiesSelectQuestion } from "@guildion/node";
import { Interface } from "readline";
import { SingleBar } from 'cli-progress';
import APIContext from "@/scenario/infrastructure/APIContext";
import faker from 'faker';
import { FileEntity } from "@guildion/db";

interface Props extends ScenarioProps {
    data: ReturnType<MessageCreateRequest["toJSON"]>,
    device?: Device,
    csrfSecret?: string,
    accessToken?: string,
}

interface Finale extends ScenarioFinale {
}

export class MessageCreateCli extends ScenarioCli<Props> {
    async question(cli: Interface): Promise<Props> {
        const props: Props = { 
            data: new MessageCreateRequest({
                message: new Message({
                    roomId: APIContext.current().currentRoom?.id!,
                    body: faker.lorem.sentences(1),
                }).toJSON(),
            }).toJSON(),
            csrfSecret: APIContext.current().csrfSecret || '',
            device: APIContext.instance().getCurrentDevice() || new Device(),
            accessToken: APIContext.current().accessToken || '',
        }
        props.data.message.body = await answerPropsQuestion(cli, {
            props: props.data.message,
            propsname: 'message',
            key: "body",
            nullable: true,
        }) || props.data.message.body;
        const files = (await entitiesSelectQuestion(cli, FileEntity, { where: { type: FileType.Image }, attribute: 'url' }));
        props.data.message.files = await Promise.all(files.map(file => file.transform()));
        return props;
    } 
}

export class MessageCreateScenario extends APIScenario(MessagesEndpoint.Create) {
    public prefix = 'create';
    public description = 'create message manually';
    public isDefault = true;
    public cli = MessageCreateCli;

    async run(props: Props, {
        bar,
    }: {
        bar: SingleBar,
    }) {
        bar.start(4, 0, { speed: "N/A" });
        bar.update(1);
        const result = await super.run(props, { bar });
        bar.update(2);
        bar.update(3);
        bar.update(4);
        bar.stop();
        return result;
    }
}

export class MessageCreateSection extends ScenarioSection {
    public prefix = 'create';
    public description = 'Create message through API';
    public cases = [new MessageCreateScenario()];
}