import { Device, DeviceAttributes, File, Guild, GuildAttributes, Member, MemberAttributes, Room, RoomAttributes, User, UserAttributes, PostAttributes, Post, FileAttributes, Files } from "@guildion/core";
import { ScenarioContextType } from "@guildion/db";
import { logger, NodeLogColor, ScenarioContext } from "@guildion/node";

interface Context {
    currentDevice?: DeviceAttributes,
    currentUser?: UserAttributes,
    currentGuild?: GuildAttributes,
    currentMember?: MemberAttributes,
    currentRoom?: RoomAttributes,
    currentPost?: PostAttributes,
    csrfSecret?: string,
    accessToken?: string,
    lastRoomsFetch?: Date,
    presigneds?: FileAttributes[],
}

export const defaultContext: Context = {
}

class APIContext extends ScenarioContext {
    private static Instance: APIContext;
    private Current: Context = defaultContext;

    public static instance(): APIContext {
        if (!this.Instance) {
            this.Instance = new APIContext(APIContext.instance);
        }
        return this.Instance;
    }

    public static current(): Context {
        return APIContext.instance().Current;
    }

    public static setCurrent(context: Context) {
        APIContext.instance().setContext(context);
    }
    
    constructor(caller: Function) {
        super(caller);
        this.type = ScenarioContextType.API;
        if (caller != APIContext.instance && APIContext.Instance) throw new Error('Singleton instance already exists');
        if (caller != APIContext.instance && !APIContext.Instance) throw new Error('Caller argument is incorrect');
    }

    logger() {
        console.log(' ');
        logger.divider2();
        logger.color(NodeLogColor.FgCyan, '<Current Scenario Context>');
        console.log(this.Current);
        logger.divider2();
        console.log(' ');
    }

    setContext(context: Context) {
        this.current = context;
        this.Current = context;
    }

    getContext(): Context {
        return this.current;
    }

    override async save() {
        this.setContext(this.Current);
        super.save();
    }

    override async restore() {
        await super.restore();
        this.setContext(this.current);
    }

    public getCurrentDevice(): Device | undefined {
        const result = this.Current['currentDevice'];
        return result ? new Device(result) : undefined;
    }

    public getCurrentUser(): User | undefined {
        const result = this.Current['currentUser'];
        return result ? new User(result) : undefined;
    }

    public getCurrentGuild(): Guild | undefined {
        const result = this.Current['currentGuild'];
        return result ? new Guild(result) : undefined;
    }

    public getCurrentMember(): Member | undefined {
        const result = this.Current['currentMember'];
        return result ? new Member(result) : undefined;
    }

    public getCurrentRoom(): Room | undefined {
        const result = this.Current['currentRoom'];
        return result ? new Room(result) : undefined;
    }

    public getCurrentPost(): Post | undefined {
        const result = this.Current['currentPost'];
        return result ? new Post(result) : undefined;
    }

    public getPresigneds(): Files | undefined {
        const result = this.Current['presigneds'];
        return result ? new Files(result) : undefined;
    }
}

export default APIContext;