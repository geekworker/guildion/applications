import { CURRENT_ENV } from '@/app/constants/ENV';
import { configure } from '@guildion/core';

export default function configureCore() {
    configure({ CURRENT_NODE_ENV: CURRENT_ENV.NODE_ENV, IS_MOBILE: false });
}