import { UPDATE_CURRENT_ENV } from "@/app/constants/ENV";
import dotenv from 'dotenv';

export default function configureENV() {
    dotenv.config();
    UPDATE_CURRENT_ENV();
}