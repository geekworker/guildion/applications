import configureENV from './ENV';
import configureCore from './Core';
import configureNode from './Node';
import configureDB from '@/app/infrastructure/db';

export default async function configureScenario() {
    configureENV();
    configureCore();
    configureNode();
    await configureDB();
}