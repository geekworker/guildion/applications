import { configure } from '@guildion/node';
import { CURRENT_ENV } from '@/app/constants/ENV';

export default function configureNode() {
    configure({ CURRENT_NODE_ENV: CURRENT_ENV.NODE_ENV, IS_MOBILE: false, JWT_SECRET: CURRENT_ENV.JWT_SECRET });
}