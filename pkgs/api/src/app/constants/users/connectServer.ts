import { UserEntity, AdministratorEntity, AccountEntity, DeviceEntity } from "@guildion/db";
import { AdministratorRole, Device, fallbackCountryCode, fallbackLanguageCode, fallbackTimeZone, LanguageCode, Localizer, UserStatus } from '@guildion/core';
import AccessTokensDataStore from "@/app/datastores/AccessTokensDataStore";
import AccountsDataStore from "@/app/datastores/AccountsDataStore";
import DevicesDataStore from "@/app/datastores/DevicesDataStore";
import UserDevicesDataStore from "@/app/datastores/UserDevicesDataStore";
import CSRFsDataStore from '@/app/datastores/CSRFsDataStore';
import { CURRENT_ENV } from '../ENV';
import { seedFiles } from "@guildion/db/src/seeds/files";

export let ConnectServerUserEntity: UserEntity;
export let ConnectServerAccountEntity: AccountEntity;
export let ConnectServerDeviceEntity: DeviceEntity;
export let ConnectServerAdministratorEntity: AdministratorEntity;
export const SeedConnectServerUserEntity = async (): Promise<UserEntity> => {
    ConnectServerUserEntity ||= (await UserEntity.findOne({ 
        where: {
            username: CURRENT_ENV.CONNECT_DEVICE_USERNAME,
        } 
    }))!;
    if (!ConnectServerUserEntity) {
        const localizer = new Localizer(LanguageCode.English);
        const accountsDataStore = new AccountsDataStore({ localizer });
        const accessTokensDataStore = new AccessTokensDataStore({ localizer });
        const devicesDataStore = new DevicesDataStore({ localizer });
        const userDevicesDataStore = new UserDevicesDataStore({ localizer });
        const csrfsDataStore = new CSRFsDataStore({ localizer });
        const device = new Device({
            udid: CURRENT_ENV.CONNECT_DEVICE_UDID,
            os: 'Linux OS',
            model: 'Amazon Linux 2',
            languageCode: fallbackLanguageCode,
            countryCode: fallbackCountryCode,
            appVersion: '0.0.0',
        }).toJSON();
        ConnectServerDeviceEntity = await devicesDataStore.register({ device });
        const { csrfSecret } = await csrfsDataStore.createAndSaveSecret({ device: ConnectServerDeviceEntity });
        const { user, account } = await accountsDataStore.create({
            profileId: seedFiles.userProfiles[0].id,
            username: CURRENT_ENV.CONNECT_DEVICE_USERNAME,
            displayName: CURRENT_ENV.CONNECT_DEVICE_USERNAME,
            description: '',
            status: UserStatus.CREATED,
            languageCode: fallbackLanguageCode,
            countryCode: fallbackCountryCode,
            timezone: fallbackTimeZone.id,
        });
        ConnectServerUserEntity = user;
        ConnectServerAccountEntity = account;
        const [accessToken, userDevice] = await Promise.all([
            accessTokensDataStore.createAndSaveSecret({
                account,
                device: ConnectServerDeviceEntity,
            }),
            userDevicesDataStore.findOrCreate({ device: ConnectServerDeviceEntity, user: user }),
        ]);
    } else {
        ConnectServerAccountEntity = await ConnectServerUserEntity!.account;
        ConnectServerDeviceEntity = (await DeviceEntity.findOne({
            where: {
                udid: CURRENT_ENV.CONNECT_DEVICE_UDID,
            }
        }))!;
    }
    ConnectServerAdministratorEntity = await AdministratorEntity.findOne({ 
        where: {
            userId: ConnectServerUserEntity.id,
        } 
    }) || await AdministratorEntity.create({
        userId: ConnectServerUserEntity.id,
        role: AdministratorRole.CONNECT_SERVER,
    }).save();
    return ConnectServerUserEntity;
}