import { UserEntity } from "@guildion/db";
import { SeedConnectServerUserEntity } from "./connectServer";

export let systemUsers: UserEntity[];

export const seedAllUsers = async () => {
    systemUsers = await Promise.all([
        SeedConnectServerUserEntity(),
    ]);
    return systemUsers;
}