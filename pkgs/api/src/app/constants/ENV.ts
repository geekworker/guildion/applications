import { NODE_ENV, GET_NODE_ENV, LOCAL_API_PORT } from "@guildion/core";
import { RuntimeValues } from "webpack/plugins/runtimeValues";
import webpackDevConfig from '../../../webpack/webpack.config.development';

declare let RUNTIME_VALUES: RuntimeValues;
export let runtimeValues: RuntimeValues;

export type ENV = {
    NODE_ENV: NODE_ENV,
    PORT: string | number,
    NUM_PROCESSES?: number,
    JWT_SECRET: string,
    AWS_ACCESS_KEY_ID: string,
    AWS_SECRET_ACCESS_KEY: string,
    AWS_REGION: string,
    AWS_S3_REGION: string,
    AWS_S3_CDN_BUCKET: string,
    GOOGLE_ANALYSTICS_CLIENT: string,
    ONESIGNAL_APP_ID: string,
    ONESIGNAL_USER_AUTH_KEY: string,
    ONESIGNAL_APP_AUTH_KEY: string,
    ONESIGNAL_SAFARI_ID: string,
    DATABASE_USERNAME: string,
    DATABASE_PASSWORD?: string,
    DATABASE_NAME: string,
    DATABASE_HOST: string,
    DATABASE_PORT: number,
    CONNECT_DEVICE_USERNAME: string,
    CONNECT_DEVICE_UDID: string,
}

export let CURRENT_ENV: ENV;

export const UPDATE_CURRENT_ENV = () => {
    if (process.env.NODE_ENV == 'development') {
        const DefinePlugin: any = require('@/index').compiler?.options.plugins[webpackDevConfig.plugins!.length - 2];
        runtimeValues = JSON.parse(DefinePlugin.definitions.RUNTIME_VALUES);
    } else {
        runtimeValues = RUNTIME_VALUES;
    }
    const NUM_PROCESSES: string | undefined = process.env.API_NUM_PROCESSES ?? process.env.NUM_PROCESSES ?? runtimeValues.NUM_PROCESSES;
    CURRENT_ENV = {
        NODE_ENV: GET_NODE_ENV(runtimeValues.NODE_ENV),
        PORT: process.env.PORT ?? process.env.API_PORT ?? runtimeValues.PORT ?? LOCAL_API_PORT,
        NUM_PROCESSES: !!NUM_PROCESSES ? Number(NUM_PROCESSES) || undefined : undefined,
        JWT_SECRET: process.env.API_JWT_SECRET ?? runtimeValues.JWT_SECRET,
        AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID ?? runtimeValues.AWS_ACCESS_KEY_ID,
        AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY ?? runtimeValues.AWS_SECRET_ACCESS_KEY,
        AWS_REGION: process.env.AWS_REGION ?? process.env.AWS_DEFAULT_REGION ?? runtimeValues.AWS_REGION,
        AWS_S3_REGION: process.env.AWS_S3_REGION ?? process.env.AWS_DEFAULT_REGION ?? runtimeValues.AWS_S3_REGION,
        AWS_S3_CDN_BUCKET: process.env.AWS_S3_CDN_BUCKET ?? runtimeValues.AWS_S3_CDN_BUCKET,
        GOOGLE_ANALYSTICS_CLIENT: process.env.GOOGLE_ANALYSTICS_CLIENT ?? runtimeValues.GOOGLE_ANALYSTICS_CLIENT,
        ONESIGNAL_APP_ID: process.env.ONESIGNAL_APP_ID ?? runtimeValues.ONESIGNAL_APP_ID,
        ONESIGNAL_USER_AUTH_KEY: process.env.ONESIGNAL_USER_AUTH_KEY ?? runtimeValues.ONESIGNAL_USER_AUTH_KEY,
        ONESIGNAL_APP_AUTH_KEY: process.env.ONESIGNAL_APP_AUTH_KEY ?? runtimeValues.ONESIGNAL_APP_AUTH_KEY,
        ONESIGNAL_SAFARI_ID: process.env.ONESIGNAL_SAFARI_ID ?? runtimeValues.ONESIGNAL_SAFARI_ID,
        DATABASE_USERNAME: process.env.API_DATABASE_USERNAME ?? runtimeValues.DATABASE_NAME,
        DATABASE_PASSWORD: process.env.API_DATABASE_PASSWORD ?? runtimeValues.DATABASE_PASSWORD,
        DATABASE_NAME: process.env.API_DATABASE_NAME! ?? runtimeValues.DATABASE_NAME,
        DATABASE_HOST: process.env.API_DATABASE_HOST! ?? runtimeValues.DATABASE_HOST,
        DATABASE_PORT: Number(process.env.API_DATABASE_PORT ?? runtimeValues.DATABASE_PORT) || 5432,
        CONNECT_DEVICE_USERNAME: process.env.CONNECT_DEVICE_USERNAME ?? runtimeValues.CONNECT_DEVICE_USERNAME,
        CONNECT_DEVICE_UDID: process.env.CONNECT_DEVICE_UDID ?? runtimeValues.CONNECT_DEVICE_UDID,
    };
};


export const UPDATE_CURRENT_SCENARIO_ENV = () => {
    CURRENT_ENV = {
        NODE_ENV: GET_NODE_ENV(process.env.NODE_ENV!),
        PORT: process.env.PORT ?? LOCAL_API_PORT,
        NUM_PROCESSES: undefined,
        JWT_SECRET: process.env.API_JWT_SECRET!,
        AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID!,
        AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY!,
        AWS_REGION: process.env.AWS_REGION ?? process.env.AWS_DEFAULT_REGION!,
        AWS_S3_REGION: process.env.AWS_S3_REGION ?? process.env.AWS_DEFAULT_REGION!,
        AWS_S3_CDN_BUCKET: process.env.AWS_S3_CDN_BUCKET!,
        GOOGLE_ANALYSTICS_CLIENT: process.env.GOOGLE_ANALYSTICS_CLIENT!,
        ONESIGNAL_APP_ID: process.env.ONESIGNAL_APP_ID!,
        ONESIGNAL_USER_AUTH_KEY: process.env.ONESIGNAL_USER_AUTH_KEY!,
        ONESIGNAL_APP_AUTH_KEY: process.env.ONESIGNAL_APP_AUTH_KEY!,
        ONESIGNAL_SAFARI_ID: process.env.ONESIGNAL_SAFARI_ID!,
        DATABASE_USERNAME: process.env.API_DATABASE_USERNAME!,
        DATABASE_PASSWORD: process.env.API_DATABASE_PASSWORD!,
        DATABASE_NAME: process.env.API_DATABASE_NAME!,
        DATABASE_HOST: process.env.API_DATABASE_HOST!,
        DATABASE_PORT: Number(process.env.API_DATABASE_PORT ?? 5432) || 5432,
        CONNECT_DEVICE_USERNAME: process.env.CONNECT_DEVICE_USERNAME!,
        CONNECT_DEVICE_UDID: process.env.CONNECT_DEVICE_UDID!,
    };
};