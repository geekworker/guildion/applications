import { Files, IS_DEV, SUPPORTED_LANGUAGES_KEY } from '@guildion/core';
import { Client } from 'onesignal-node';
import { CURRENT_ENV } from '../constants/ENV';

export const pushNotification = async ({
    icon,
    url,
    ids,
    titles,
    contents,
}: {
    icon?: string,
    url?: string,
    ids: string[],
    titles: { [key in SUPPORTED_LANGUAGES_KEY]: string },
    contents: { [key in SUPPORTED_LANGUAGES_KEY]: string },
}): Promise<string> => {
    if (IS_DEV()) {
        console.log({
            headings: titles,
            contents,
            url: url,
            includePlayerIds: ids,
        });
        return '';
    }
    const client = new Client(`${CURRENT_ENV.ONESIGNAL_APP_ID}`, `${CURRENT_ENV.ONESIGNAL_APP_AUTH_KEY}`);
    const response = await client.createNotification({
        headings: titles,
        contents,
        small_icon: icon || Files.getSeed().brandLogos.find(bl => bl.providerKey.includes('original'))?.url,
        large_icon: icon || Files.getSeed().brandLogos.find(bl => bl.providerKey.includes('original'))?.url,
        url: url,
        include_player_ids: ids,
    });
    return `${response.body['notificationId']}`;
}