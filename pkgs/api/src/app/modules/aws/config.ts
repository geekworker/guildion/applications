import { CURRENT_ENV } from '@/app/constants/ENV';
import AWS from 'aws-sdk';

export let s3: AWS.S3;
export let ec2: AWS.EC2;

export const configureAWS = () => {
    AWS.config.update({
        accessKeyId: CURRENT_ENV.AWS_ACCESS_KEY_ID,
        secretAccessKey: CURRENT_ENV.AWS_SECRET_ACCESS_KEY,
        region: CURRENT_ENV.AWS_REGION,
        signatureVersion: 'v4',
        credentials: new AWS.Credentials(
            CURRENT_ENV.AWS_ACCESS_KEY_ID!,
            CURRENT_ENV.AWS_SECRET_ACCESS_KEY!
        ),
    });
    s3 = new AWS.S3({
        accessKeyId: CURRENT_ENV.AWS_ACCESS_KEY_ID,
        secretAccessKey: CURRENT_ENV.AWS_SECRET_ACCESS_KEY,
        region: CURRENT_ENV.AWS_S3_REGION,
        signatureVersion: 'v4',
        credentials: new AWS.Credentials(
            CURRENT_ENV.AWS_ACCESS_KEY_ID!,
            CURRENT_ENV.AWS_SECRET_ACCESS_KEY!
        ),
    });
    new AWS.EC2({
        accessKeyId: CURRENT_ENV.AWS_ACCESS_KEY_ID,
        secretAccessKey: CURRENT_ENV.AWS_SECRET_ACCESS_KEY,
        region: CURRENT_ENV.AWS_REGION,
        credentials: new AWS.Credentials(
            CURRENT_ENV.AWS_ACCESS_KEY_ID!,
            CURRENT_ENV.AWS_SECRET_ACCESS_KEY!
        ),
    });
}