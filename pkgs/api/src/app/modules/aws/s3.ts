import AWS from 'aws-sdk';
import { CDN_PROXY_URL, ContentType, Localizer, safeURL } from '@guildion/core';
import { s3 } from './config';
import { CURRENT_ENV } from '@/app/constants/ENV';

export default class S3Adaptor {
    public localizer: Localizer;
    
    constructor({ localizer }: { localizer: Localizer }) {
        this.localizer = localizer;
    }

    public guardFileKeyFromURL = (url: URL): string => {
        return this.guardFileKeyFromURLstring(url.toString());
    };

    public guardFileKeyFromURLstring = (urlstring: string): string => {
        const parser = safeURL(urlstring);
        if (!parser) return urlstring;
        return parser.pathname
            .replace(`/${CURRENT_ENV.AWS_S3_CDN_BUCKET}`, '')
            .replace(`/`, '');
    };

    public deletable(urlstring?: string): boolean {
        if (!urlstring || urlstring.length == 0) return false;
        const parser = safeURL(urlstring);
        if (!parser) return false;
        return (
            parser.host ==
                `${CURRENT_ENV.AWS_S3_CDN_BUCKET}.s3-${CURRENT_ENV.AWS_S3_REGION}.amazonaws.com` ||
            parser.host == `s3-${CURRENT_ENV.AWS_S3_REGION}.amazonaws.com` ||
            parser.host == CDN_PROXY_URL.host
        )
    };

    async checkObjectExists(urlstring: string): Promise<boolean> {
        const key = this.guardFileKeyFromURLstring(urlstring);
        const params: AWS.S3.Types.GetObjectRequest = {
            Bucket: CURRENT_ENV.AWS_S3_CDN_BUCKET!,
            Key: key,
        };
        return new Promise((resolve, reject) => {
            s3.getObject(params, (err, result) => {
                if (err) resolve(false);
                resolve(true);
            });
        });
    };

    async getObject(urlstring: string): Promise<AWS.S3.Types.GetObjectOutput | undefined> {
        const key = this.guardFileKeyFromURLstring(urlstring);
        const params: AWS.S3.Types.GetObjectRequest = {
            Bucket: CURRENT_ENV.AWS_S3_CDN_BUCKET!,
            Key: key,
        };
        return new Promise((resolve, reject) => {
            s3.getObject(params, (err, result) => {
                if (err) resolve(undefined);
                resolve(result);
            });
        });
    };

    async getListObjects(urlstring: string): Promise<AWS.S3.Types.ListObjectsV2Output> {
        const path = this.guardFileKeyFromURLstring(urlstring);
        const params: AWS.S3.Types.ListObjectsV2Request = {
            Bucket: CURRENT_ENV.AWS_S3_CDN_BUCKET!,
            Prefix: path,
        };
        return new Promise((resolve, reject) => {
            s3.listObjectsV2(params, (err, result) => {
                if (err) reject(err);
                resolve(result);
            });
        });
    };

    async getSignedURLForPut({ key, contentType }: { key: string, contentType: ContentType }): Promise<string> {
        const params = {
            Bucket: CURRENT_ENV.AWS_S3_CDN_BUCKET!,
            Expires: 3600,
            ContentType: decodeURIComponent(contentType),
            Key: decodeURIComponent(key),
        };
        const urlstring = s3.getSignedUrl('putObject', params);
        return urlstring;
    }

    async getSignedURLForGet({ key }: { key: string }): Promise<string> {
        const params = {
            Bucket: CURRENT_ENV.AWS_S3_CDN_BUCKET!,
            Expires: 3600,
            Key: decodeURIComponent(key),
        };
        const urlstring = s3.getSignedUrl('getObject', params);
        return urlstring;
    }

    async deleteFile(urlstring: string): Promise<AWS.S3.Types.DeleteObjectOutput | undefined> {
        if (!this.deletable(urlstring)) return;
        return await this.deleteFileFromKey(this.guardFileKeyFromURLstring(urlstring));
    };

    async deleteFileFromKey(key: string): Promise<AWS.S3.Types.DeleteObjectOutput> {
        const params: AWS.S3.Types.DeleteObjectRequest = {
            Bucket: CURRENT_ENV.AWS_S3_CDN_BUCKET!,
            Key: key,
        };
        return new Promise((resolve, reject) => {
            s3.deleteObject(params, (err, result) => {
                if (err) reject(err)
                resolve(result);
            });
        });
    };

    async deleteDirectory(urlstring: string): Promise<AWS.S3.Types.DeleteObjectOutput[]> {
        const listOutput = await this.getListObjects(urlstring);
        const deleted = await Promise.all(
            (listOutput.Contents || [])
                .map(obj => obj.Key)
                .filter((key): key is string => !!key)
                .map(key => this.deleteFileFromKey(key))
        );
        return deleted;
    };
}