import { FastifyRequest, FastifyReply, FastifyInstance } from "fastify";
import { HTTPStatusCode, ContentType, Endpoint, Router, HTTPMethod, EndpointOfRequest, EndpointOfResponse, Device, HEADER_APPLICATION_DEVICE, Localizer, HEADER_APPLICATION_CSRF, HEADER_APPLICATION_ACCESS_TOKEN, Unwrapper, fallbackLanguageCode, AppError, guardCase, LanguageCode, UserDeviceStatus } from '@guildion/core';
import { NodeRequestLogger, NodeResponseLogger } from '@guildion/node';
import { HandlerConstructor } from "@/app/handlers/Handler";
import CSRFsDataStore from "@/app/datastores/CSRFsDataStore";
import AccessTokensDataStore from "@/app/datastores/AccessTokensDataStore";
import DevicesValidation from "@/app/validations/DevicesValidation";
import { AccountEntity, DeviceEntity, UserDeviceEntity, UserEntity } from "@guildion/db"

export type GatewayProps = {
    device?: DeviceEntity,
    account?: AccountEntity,
}

export default class UniversalGateway<T extends Endpoint<Router.Factory<any>, any, any>> {
    public endpoint: T;
    public fastify: FastifyInstance;
    private handler: HandlerConstructor<T>;

    constructor(
        endpoint: T,
        fastify: FastifyInstance,
        handler: HandlerConstructor<T>,
    ) {
        this.endpoint = endpoint;
        this.fastify = fastify;
        this.handler = handler;
        this.register();
    }

    register() {
        switch(this.endpoint.method) {
            default:
            case HTTPMethod.GET: this.fastify.get((new this.endpoint.router).path.replace(this.fastify.prefix, ''), async (req: FastifyRequest, reply: FastifyReply) => await this.body(req, reply));
            case HTTPMethod.POST: this.fastify.post((new this.endpoint.router).path.replace(this.fastify.prefix, ''), async (req: FastifyRequest, reply: FastifyReply) => await this.body(req, reply));
        }
    }

    async body(this: UniversalGateway<T>, req: FastifyRequest, reply: FastifyReply) {
        const startMs = NodeRequestLogger({ url: req.url, method: req.method, header: req.headers, body: req.body });
        const request: EndpointOfRequest<T> = new this.endpoint.request(req.body);
        const lang = guardCase(
            req.headers ? req.headers["accept-language"] ?? req.headers["Accept-Language"] : fallbackLanguageCode,
            {
                cases: LanguageCode,
                fallback: fallbackLanguageCode,
            }
        );
        const localizer = new Localizer(lang);
        const accessTokenChecked = await this.checkAccessToken(req, reply, { startMs, localizer });
        const csrfChecked = await this.checkCSRF(req, reply, { startMs, localizer });
        if (!accessTokenChecked.result) return;
        if (!csrfChecked.result) return;
        if (!!accessTokenChecked.account && !!accessTokenChecked.account.user && !!csrfChecked.device) await this.checkUserDevice(req, reply, { startMs, localizer, user: await accessTokenChecked.account.user, device: csrfChecked.device });
        const handlerInstance = new this.handler(this.endpoint, { lang });
        const response: EndpointOfResponse<T> | undefined = await handlerInstance.run(request, { 
            device: csrfChecked.device,
            account: accessTokenChecked.account,
        }).catch(e => {
            reply
                .status(HTTPStatusCode.UNPROCESSABLE_ENTITY)
                .send({ ...e, message: e.message, error: e.error })
                .then(() => {
                    NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e })
                }, (err) => {
                    NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e }) 
                });
            return undefined;
        });
        if (!response) return;
        const json = response.toJSON();
        switch(this.endpoint.responseType) {
        default:
        case ContentType.json:
            reply
                .status(response.status)
                .type(this.endpoint.responseType)
                .send(json)
                .then(() => { 
                    NodeResponseLogger({ url: req.url, method: req.method, startMs, status: response.status, header: reply.getHeaders(), body: this.endpoint.options.skipLogging ? "skipped..." : json }) 
                }, (err) => {
                    NodeResponseLogger({ url: req.url, method: req.method, startMs, status: response.status, header: reply.getHeaders(), body: json, err }) 
                })
        }
    }

    async checkAccessToken(
        this: UniversalGateway<T>,
        req: FastifyRequest,
        reply: FastifyReply,
        { localizer, startMs }: { localizer: Localizer, startMs: number }
    ): Promise<{
        result: boolean,
        account?: AccountEntity
    }> {
        const {
            accessTokenRequired,
            accessTokenOptional,
            adminRoles,
        } = this.endpoint.options;
        if (!accessTokenRequired && !accessTokenOptional && !adminRoles) return { result: true };
        if (!req.headers[HEADER_APPLICATION_ACCESS_TOKEN] && !accessTokenOptional) {
            await this.sendAccessTokenError(req, reply, { localizer, startMs });
            return { result: false };
        } else if (!req.headers[HEADER_APPLICATION_ACCESS_TOKEN] && accessTokenOptional) {
            return { result: true };
        } else {
            const header = req.headers[HEADER_APPLICATION_DEVICE];
            const token = req.headers[HEADER_APPLICATION_ACCESS_TOKEN];
            if (Unwrapper.isString(header) && Unwrapper.isString(token)) {
                const device = new Device(JSON.parse(header));
                const datastore = new AccessTokensDataStore({ localizer });
                try {
                    const account = await datastore.verify(token, device, { adminRoles });
                    return { result: true, account };
                } catch (e) {
                    console.log(e);
                    !accessTokenOptional && await this.sendAccessTokenError(req, reply, { localizer, startMs });
                    return { result: accessTokenOptional ?? false };
                }
            } else {
                !accessTokenOptional && await this.sendAccessTokenError(req, reply, { localizer, startMs });
                return { result: accessTokenOptional ?? false };
            }
        }
    }

    async checkCSRF(this: UniversalGateway<T>, req: FastifyRequest, reply: FastifyReply, { localizer, startMs }: { localizer: Localizer, startMs: number }): Promise<{ result: boolean, device?: DeviceEntity }> {
        if (!this.endpoint.options.deviceRequired) return { result: true };
        if (!req.headers[HEADER_APPLICATION_DEVICE]) {
            await this.sendCSRFError(req, reply, { localizer, startMs });
            return { result: false };
        } else {
            try {
                const header = `${req.headers[HEADER_APPLICATION_DEVICE]}`;
                const csrfSecret = `${req.headers[HEADER_APPLICATION_CSRF]}`;
                const device = new Device(JSON.parse(header));
                const entity = await DeviceEntity.findOne({ where: { id: device.id } });
                await (new DevicesValidation({ localizer })).checkDeviceState(entity);
                const datastore = new CSRFsDataStore({ localizer });
                await datastore.compare({ device: entity!, csrfSecret });
                return { result: true, device: entity! };
            } catch(e) {
                console.log(e);
                await this.sendCSRFError(req, reply, { localizer, startMs });
                return { result: false };
            }
        }
    }

    async checkUserDevice(this: UniversalGateway<T>, req: FastifyRequest, reply: FastifyReply, { localizer, startMs, user, device }: { localizer: Localizer, startMs: number, user: UserEntity, device: DeviceEntity }) {
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        let e: AppError | undefined = undefined;
        if (!userDevice) e = new AppError(localizer.dictionary.error.device.relatedError);
        if (userDevice?.status == UserDeviceStatus.LOCKED) e = new AppError(localizer.dictionary.error.device.lockedError);
        if (!!e) {
            reply
                .status(HTTPStatusCode.UNPROCESSABLE_ENTITY)
                .send(e)
                .then(() => {
                    NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e })
                }, (err) => {
                    NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e }) 
                });
        }
    }

    async sendCSRFError(this: UniversalGateway<T>, req: FastifyRequest, reply: FastifyReply, { localizer, startMs }: { localizer: Localizer, startMs: number }) {
        const e = new AppError(localizer.dictionary.error.csrf.invalidToken);
        reply
            .status(HTTPStatusCode.UNPROCESSABLE_ENTITY)
            .send(e)
            .then(() => {
                NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e })
            }, (err) => {
                NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e }) 
            });
    }

    async sendAccessTokenError(this: UniversalGateway<T>, req: FastifyRequest, reply: FastifyReply, { localizer, startMs }: { localizer: Localizer, startMs: number }) {
        const e = new AppError(localizer.dictionary.error.accessToken.invalidToken);
        reply
            .status(HTTPStatusCode.UNPROCESSABLE_ENTITY)
            .send(e)
            .then(() => {
                NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e })
            }, (err) => {
                NodeResponseLogger({ url: req.url, method: req.method, startMs, status: HTTPStatusCode.UNPROCESSABLE_ENTITY, header: reply.getHeaders(), body: e, err: e }) 
            });
    }
}