import { IS_DEV, LanguageCode, Localizer } from '@guildion/core';
import { connect } from '@guildion/db';
import type { FileEntity } from '@guildion/db';
import { CURRENT_ENV } from '../constants/ENV';
import FilesDataStore from '../datastores/FilesDataStore';

export default async function configureDB() {
    const filesDataStore = new FilesDataStore({ localizer: new Localizer(LanguageCode.English) });
    await connect({
        logging: IS_DEV(),
        DATABASE_USERNAME: CURRENT_ENV.DATABASE_USERNAME,
        DATABASE_PASSWORD: CURRENT_ENV.DATABASE_PASSWORD,
        DATABASE_NAME: CURRENT_ENV.DATABASE_NAME,
        DATABASE_HOST: CURRENT_ENV.DATABASE_HOST,
        DATABASE_PORT: CURRENT_ENV.DATABASE_PORT,
        options: {
            afterFileCreate: (file: FileEntity) => filesDataStore.uploadFromEntity(file),
        },
    });
}