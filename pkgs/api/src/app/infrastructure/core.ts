import { configure } from '@guildion/core';
import { CURRENT_ENV } from '../constants/ENV';

export default function configureCore() {
    configure({ CURRENT_NODE_ENV: CURRENT_ENV.NODE_ENV, IS_MOBILE: false });
}