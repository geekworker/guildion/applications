import { seedAllUsers } from "../constants/users";

export default async function() {
    await seedAllUsers();
}