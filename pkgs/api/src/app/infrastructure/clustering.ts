import { IS_DEV } from '@guildion/core';
import cluster from 'cluster';
import { FastifyInstance } from "fastify";
import os from 'os';
import { CURRENT_ENV } from '../constants/ENV';

export default async function clustering(fastify: FastifyInstance) {
    const numProcesses: number = CURRENT_ENV.NUM_PROCESSES || os.cpus().length;
    const PORT: number | string = CURRENT_ENV.PORT;
    if (!IS_DEV()) {
        if (cluster.isMaster) {
            console.log('application server starting, please wait.');
            for (var i: number = 0; i < numProcesses; i++) {
                cluster.fork();
            }
            cluster.on('exit', function(worker) {
                console.log(
                    'error: worker %d died, starting a new one',
                    worker.id
                );
                cluster.fork();
            });
        } else {
            fastify.listen(PORT, '0.0.0.0', function (err: Error) {
                if (err) {
                    console.error(err);
                    process.exit(1);
                }
                console.log('Worker process started for port %s', PORT);
            });
        };
    } else {
        fastify.listen(PORT, '0.0.0.0', function (err: Error) {
            if (err) {
                console.error(err);
                process.exit(1);
            }
            console.log('Application started on port %s', PORT);
        });
    }
}
