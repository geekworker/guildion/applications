import { FastifyInstance } from "fastify";
import APIMiddleware from "../middlewares/api";
import RootMiddleware from "../middlewares/routes";

export default async function router(fastify: FastifyInstance) {
    fastify.register(RootMiddleware);
    fastify.register(APIMiddleware, { prefix: '/api' });
}