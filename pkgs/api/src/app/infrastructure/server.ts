import fastify from "fastify";
import router from '@/app/infrastructure/router';

const server = fastify();

server.register(router);

export default server;