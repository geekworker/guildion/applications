import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MemberKickRequest, MemberKickResponse, MembersEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import MembersDataStore from "../../datastores/MembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MembersEndpoint.Kick> = class MemberKickHandler extends BaseHandler<MembersEndpoint.Kick> implements HandlerImpl<MembersEndpoint.Kick> {
    private membersDataStore: MembersDataStore;

    constructor(endpoint: MembersEndpoint.Kick, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.membersDataStore = new MembersDataStore({ localizer: this.localizer });
    }

    async run(req: MemberKickRequest, props: GatewayProps): Promise<MemberKickResponse> {
        await this.membersDataStore.kick(req.id, {
            user: await props.account?.user!,
        });
        
        return new MemberKickResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;