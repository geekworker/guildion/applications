import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MemberUpdateRequest, MemberUpdateResponse, MembersEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import MembersDataStore from "../../datastores/MembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MembersEndpoint.Update> = class MemberUpdateHandler extends BaseHandler<MembersEndpoint.Update> implements HandlerImpl<MembersEndpoint.Update> {
    private membersDataStore: MembersDataStore;

    constructor(endpoint: MembersEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.membersDataStore = new MembersDataStore({ localizer: this.localizer });
    }

    async run(req: MemberUpdateRequest, props: GatewayProps): Promise<MemberUpdateResponse> {
        const member = await this.membersDataStore.update(req.member, { user: await props.account!.user! });
        return new MemberUpdateResponse({
            status: HTTPStatusCode.OK,
            member: await member.transform(),
        });
    }
}

export default handler;