import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MembersListRequest, MembersListResponse, MembersEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import MembersDataStore from "../../datastores/MembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MembersEndpoint.List> = class MembersListHandler extends BaseHandler<MembersEndpoint.List> implements HandlerImpl<MembersEndpoint.List> {
    private membersDataStore: MembersDataStore;

    constructor(endpoint: MembersEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.membersDataStore = new MembersDataStore({ localizer: this.localizer });
    }

    async run(req: MembersListRequest, props: GatewayProps): Promise<MembersListResponse> {
        const {
            members,
            paginatable,
        } = await this.membersDataStore.gets(
            req.guildId,
            { 
                count: req.count,
                offset: req.offset,
                user: await props.account?.user!,
            }
        );
        
        return new MembersListResponse({
            status: HTTPStatusCode.OK,
            members: await Promise.all(members.map(member => member.transform({ roles: true }))),
            paginatable,
        });
    }
}

export default handler;