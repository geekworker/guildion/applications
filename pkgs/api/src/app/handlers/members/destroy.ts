import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MemberDestroyRequest, MemberDestroyResponse, MembersEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import { MemberEntity } from "@guildion/db";
import MembersDataStore from "../../datastores/MembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MembersEndpoint.Destroy> = class MemberDestroyHandler extends BaseHandler<MembersEndpoint.Destroy> implements HandlerImpl<MembersEndpoint.Destroy> {
    private membersDataStore: MembersDataStore;

    constructor(endpoint: MembersEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.membersDataStore = new MembersDataStore({ localizer: this.localizer });
    }

    async run(req: MemberDestroyRequest, props: GatewayProps): Promise<MemberDestroyResponse> {
        const member = await MemberEntity.findOne({
            where: {
                id: req.id,
            },
        });
        await this.membersDataStore.destroy(req.id, {
            user: await props.account?.user!,
            force: false,
        });
        
        return new MemberDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;