import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { FilesListRequest, FilesListResponse, FilesEndpoint, HTTPStatusCode, LanguageCode, FileAttributes, Files } from "@guildion/core";
import { fetchOrFindSeedFiles, seedFiles } from "@guildion/db/src/seeds/files";
import FilesDataStore from "../../datastores/FilesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

let filesCache: FileAttributes[];

const handler: HandlerConstructor<FilesEndpoint.List> = class FilesListHandler extends BaseHandler<FilesEndpoint.List> implements HandlerImpl<FilesEndpoint.List> {
    private filesDataStore: FilesDataStore;

    constructor(endpoint: FilesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filesDataStore = new FilesDataStore({ localizer: this.localizer });
    }

    async run(req: FilesListRequest, props: GatewayProps): Promise<FilesListResponse> {
        // const files = await this.filesDataStore.getsSystem();
        return new FilesListResponse({
            status: HTTPStatusCode.OK,
            files: req.isJSONString ? undefined : await this.getSystemFiles(),
            filesjsonstring: req.isJSONString ? JSON.stringify(await this.getSystemFiles()) : undefined,
        });
    }

    async getSystemFiles(): Promise<FileAttributes[]> {
        if (!!filesCache) {
            return filesCache
        } else {
            await fetchOrFindSeedFiles();
            filesCache = seedFiles.toArray();
        };
        return filesCache;
    }
}

export default handler;