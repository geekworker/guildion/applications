import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileShowRequest, FileShowResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilesDataStore from '../../datastores/FilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.Show> = class FileShowHandler extends BaseHandler<FilesEndpoint.Show> implements HandlerImpl<FilesEndpoint.Show> {
    private filesDataStore: FilesDataStore;

    constructor(endpoint: FilesEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filesDataStore = new FilesDataStore({ localizer: this.localizer });
    }

    async run(req: FileShowRequest, props: GatewayProps): Promise<FileShowResponse> {
        const file = await this.filesDataStore.get(req.id, { user: await props.account?.user! })
        return new FileShowResponse({
            status: HTTPStatusCode.OK,
            file: await file.transform(),
        });
    }
}

export default handler;