import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileDestroyRequest, FileDestroyResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilesDataStore from '../../datastores/FilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.Destroy> = class FileDestroyHandler extends BaseHandler<FilesEndpoint.Destroy> implements HandlerImpl<FilesEndpoint.Destroy> {
    private filesDataStore: FilesDataStore;

    constructor(endpoint: FilesEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filesDataStore = new FilesDataStore({ localizer: this.localizer });
    }

    async run(req: FileDestroyRequest, props: GatewayProps): Promise<FileDestroyResponse> {
        await this.filesDataStore.destroy(req.id, { user: await props.account?.user! })
        return new FileDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;