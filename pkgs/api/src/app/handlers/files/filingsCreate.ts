import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileFilingsCreateRequest, FileFilingsCreateResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilingsDataStore from '../../datastores/FilingsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.FilingsCreate> = class FileFilingsCreateHandler extends BaseHandler<FilesEndpoint.FilingsCreate> implements HandlerImpl<FilesEndpoint.FilingsCreate> {
    private filingsDataStore: FilingsDataStore;

    constructor(endpoint: FilesEndpoint.FilingsCreate, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filingsDataStore = new FilingsDataStore({ localizer: this.localizer });
    }

    async run(req: FileFilingsCreateRequest, props: GatewayProps): Promise<FileFilingsCreateResponse> {
        const filings = await this.filingsDataStore.creates({ fileIds: req.fileIds, folderId: req.folderId, user: await props.account?.user! })
        return new FileFilingsCreateResponse({
            status: HTTPStatusCode.OK,
            files: await Promise.all(
                filings.map(filing => filing.transform())
            ),
        });
    }
}

export default handler;