import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileFilingsUpdateRequest, FileFilingsUpdateResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilingsDataStore from '../../datastores/FilingsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.FilingsUpdate> = class FileFilingsUpdateHandler extends BaseHandler<FilesEndpoint.FilingsUpdate> implements HandlerImpl<FilesEndpoint.FilingsUpdate> {
    private filingsDataStore: FilingsDataStore;

    constructor(endpoint: FilesEndpoint.FilingsUpdate, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filingsDataStore = new FilingsDataStore({ localizer: this.localizer });
    }

    async run(req: FileFilingsUpdateRequest, props: GatewayProps): Promise<FileFilingsUpdateResponse> {
        const filings = await this.filingsDataStore.updates(req.files, { folderId: req.folderId, user: await props.account?.user! })
        return new FileFilingsUpdateResponse({
            status: HTTPStatusCode.OK,
            files: await Promise.all(
                filings.map(filing => filing.transform())
            ),
        });
    }
}

export default handler;