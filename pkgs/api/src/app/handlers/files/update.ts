import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileUpdateRequest, FileUpdateResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilesDataStore from '../../datastores/FilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.Update> = class FileUpdateHandler extends BaseHandler<FilesEndpoint.Update> implements HandlerImpl<FilesEndpoint.Update> {
    private filesDataStore: FilesDataStore;

    constructor(endpoint: FilesEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filesDataStore = new FilesDataStore({ localizer: this.localizer });
    }

    async run(req: FileUpdateRequest, props: GatewayProps): Promise<FileUpdateResponse> {
        const file = await this.filesDataStore.update(req.file, { user: await props.account?.user! })
        return new FileUpdateResponse({
            file: await file.transform(),
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;