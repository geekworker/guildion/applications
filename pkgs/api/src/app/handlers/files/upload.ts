import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileUploadRequest, FileUploadResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilesDataStore from '../../datastores/FilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.Upload> = class FileUploadHandler extends BaseHandler<FilesEndpoint.Upload> implements HandlerImpl<FilesEndpoint.Upload> {
    private filesDataStore: FilesDataStore;

    constructor(endpoint: FilesEndpoint.Upload, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filesDataStore = new FilesDataStore({ localizer: this.localizer });
    }

    async run(req: FileUploadRequest, props: GatewayProps): Promise<FileUploadResponse> {
        await this.filesDataStore.upload(req.id);
        return new FileUploadResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;