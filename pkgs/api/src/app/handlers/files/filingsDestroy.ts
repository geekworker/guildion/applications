import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileFilingsDestroyRequest, FileFilingsDestroyResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilingsDataStore from '../../datastores/FilingsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.FilingsDestroy> = class FileFilingsDestroyHandler extends BaseHandler<FilesEndpoint.FilingsDestroy> implements HandlerImpl<FilesEndpoint.FilingsDestroy> {
    private filingsDataStore: FilingsDataStore;

    constructor(endpoint: FilesEndpoint.FilingsDestroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filingsDataStore = new FilingsDataStore({ localizer: this.localizer });
    }

    async run(req: FileFilingsDestroyRequest, props: GatewayProps): Promise<FileFilingsDestroyResponse> {
        await this.filingsDataStore.destroys({ filingIds: req.filingIds, user: await props.account?.user! })
        return new FileFilingsDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;