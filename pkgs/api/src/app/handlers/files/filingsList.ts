import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileFilingsListRequest, FileFilingsListResponse, FilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import FilingsDataStore from '../../datastores/FilingsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<FilesEndpoint.FilingsList> = class FileFilingsListHandler extends BaseHandler<FilesEndpoint.FilingsList> implements HandlerImpl<FilesEndpoint.FilingsList> {
    private filingsDataStore: FilingsDataStore;

    constructor(endpoint: FilesEndpoint.FilingsList, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filingsDataStore = new FilingsDataStore({ localizer: this.localizer });
    }

    async run(req: FileFilingsListRequest, props: GatewayProps): Promise<FileFilingsListResponse> {
        const {
            filings,
            paginatable,
        } = await this.filingsDataStore.gets(req.id, { user: await props.account?.user!, count: req.count, offset: req.offset, isRandom: req.isRandom })
        return new FileFilingsListResponse({
            status: HTTPStatusCode.OK,
            files: await Promise.all(
                filings.map(filing => filing.transform())
            ),
            paginatable,
        });
    }
}

export default handler;