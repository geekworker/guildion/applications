import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { FileProfilePresignRequest, FileProfilePresignResponse, FilesEndpoint, HTTPStatusCode, LanguageCode, File, FileExtension } from '@guildion/core';
import FilesDataStore from '../../datastores/FilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';
import { v4 as uuidv4 } from 'uuid';

const handler: HandlerConstructor<FilesEndpoint.ProfilePresign> = class FileProfilePresignHandler extends BaseHandler<FilesEndpoint.ProfilePresign> implements HandlerImpl<FilesEndpoint.ProfilePresign> {
    private filesDataStore: FilesDataStore;

    constructor(endpoint: FilesEndpoint.ProfilePresign, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.filesDataStore = new FilesDataStore({ localizer: this.localizer });
    }

    async run(req: FileProfilePresignRequest, props: GatewayProps): Promise<FileProfilePresignResponse> {
        const attributes = req.file;
        const id = attributes.id || uuidv4();
        attributes.providerKey = File.generateProfileKey({ id, extension: FileExtension(attributes.contentType) });
        const { file, presignedURL } = await this.filesDataStore.presign(attributes);
        return new FileProfilePresignResponse({
            file: await file.transform({
                presignedURL,
            }),
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;