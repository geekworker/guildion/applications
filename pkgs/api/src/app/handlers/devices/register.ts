import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { DevicesEndpoint, DeviceRegisterRequest, DeviceRegisterResponse, HTTPStatusCode, LanguageCode } from "@guildion/core";
import CSRFsDataStore from "../../datastores/CSRFsDataStore";
import DevicesDataStore from "../../datastores/DevicesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<DevicesEndpoint.Register> = class DevicesRegisterHandler extends BaseHandler<DevicesEndpoint.Register> implements HandlerImpl<DevicesEndpoint.Register> {
    private devicesDataStore: DevicesDataStore;
    private csrfsDataStore: CSRFsDataStore;

    constructor(endpoint: DevicesEndpoint.Register, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.devicesDataStore = new DevicesDataStore({ localizer: this.localizer });
        this.csrfsDataStore = new CSRFsDataStore({ localizer: this.localizer });
    }

    async run(req: DeviceRegisterRequest, props: GatewayProps): Promise<DeviceRegisterResponse> {
        const device = await this.devicesDataStore.register({ device: req.device });
        const { csrfSecret } = await this.csrfsDataStore.create({ device });

        return new DeviceRegisterResponse({
            status: HTTPStatusCode.OK,
            csrfSecret,
            device: await device.transform(),
        });
    }
}

export default handler;