import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { DeviceUnBlockRequest, DeviceUnBlockResponse, DevicesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import DeviceBlocksDataStore from "../../datastores/DeviceBlocksDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<DevicesEndpoint.UnBlock> = class DeviceUnBlockHandler extends BaseHandler<DevicesEndpoint.UnBlock> implements HandlerImpl<DevicesEndpoint.UnBlock> {
    private deviceBlocksDataStore: DeviceBlocksDataStore;

    constructor(endpoint: DevicesEndpoint.UnBlock, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.deviceBlocksDataStore = new DeviceBlocksDataStore({ localizer: this.localizer });
    }

    async run(req: DeviceUnBlockRequest, props: GatewayProps): Promise<DeviceUnBlockResponse> {
        await this.deviceBlocksDataStore.destroy(req.id, { user: await props.account!.user! });
        return new DeviceUnBlockResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;