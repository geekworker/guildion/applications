import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { DeviceBlocksListRequest, DeviceBlocksListResponse, DevicesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import DeviceBlocksDataStore from "../../datastores/DeviceBlocksDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<DevicesEndpoint.List> = class DeviceBlocksListHandler extends BaseHandler<DevicesEndpoint.List> implements HandlerImpl<DevicesEndpoint.List> {
    private deviceBlocksDataStore: DeviceBlocksDataStore;

    constructor(endpoint: DevicesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.deviceBlocksDataStore = new DeviceBlocksDataStore({ localizer: this.localizer });
    }

    async run(req: DeviceBlocksListRequest, props: GatewayProps): Promise<DeviceBlocksListResponse> {
        const {
            devices,
            paginatable,
        } = await this.deviceBlocksDataStore.gets({ 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user!,
        });
        return new DeviceBlocksListResponse({
            status: HTTPStatusCode.OK,
            devices: await Promise.all(
                devices.map(device => device.transform())
            ),
            paginatable,
        });
    }
}

export default handler;