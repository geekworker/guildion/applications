import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { DeviceBlockRequest, DeviceBlockResponse, DevicesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import DeviceBlocksDataStore from "../../datastores/DeviceBlocksDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<DevicesEndpoint.Block> = class DeviceBlockHandler extends BaseHandler<DevicesEndpoint.Block> implements HandlerImpl<DevicesEndpoint.Block> {
    private deviceBlocksDataStore: DeviceBlocksDataStore;

    constructor(endpoint: DevicesEndpoint.Block, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.deviceBlocksDataStore = new DeviceBlocksDataStore({ localizer: this.localizer });
    }

    async run(req: DeviceBlockRequest, props: GatewayProps): Promise<DeviceBlockResponse> {
        await this.deviceBlocksDataStore.findOrCreate(req.id, { user: await props.account!.user! });
        return new DeviceBlockResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;