import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { DevicesListRequest, DevicesListResponse, DevicesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import UserDevicesDataStore from "../../datastores/UserDevicesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<DevicesEndpoint.List> = class DevicesListHandler extends BaseHandler<DevicesEndpoint.List> implements HandlerImpl<DevicesEndpoint.List> {
    private userDevicesDataStore: UserDevicesDataStore;

    constructor(endpoint: DevicesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.userDevicesDataStore = new UserDevicesDataStore({ localizer: this.localizer });
    }

    async run(req: DevicesListRequest, props: GatewayProps): Promise<DevicesListResponse> {
        const {
            devices,
            paginatable,
        } = await this.userDevicesDataStore.gets({ 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user!,
        });
        return new DevicesListResponse({
            status: HTTPStatusCode.OK,
            devices: await Promise.all(
                devices.map(device => device.transform())
            ),
            paginatable,
        });
    }
}

export default handler;