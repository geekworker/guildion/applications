import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { DeviceTrustRequest, DeviceTrustResponse, DevicesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import { UserDeviceEntity } from "@guildion/db";
import UserDevicesDataStore from "../../datastores/UserDevicesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<DevicesEndpoint.Trust> = class UserDeviceHandler extends BaseHandler<DevicesEndpoint.Trust> implements HandlerImpl<DevicesEndpoint.Trust> {
    private userDevicesDataStore: UserDevicesDataStore;

    constructor(endpoint: DevicesEndpoint.Trust, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.userDevicesDataStore = new UserDevicesDataStore({ localizer: this.localizer });
    }

    async run(req: DeviceTrustRequest, props: GatewayProps): Promise<DeviceTrustResponse> {
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: (await props.account!.user!).id,
                deviceId: props.device!.id,
            }
        })
        await this.userDevicesDataStore.validation.checkUserDeviceSessionState(userDevice);
        await this.userDevicesDataStore.trust(req.id, { user: await props.account!.user!, device: props.device! });
        return new DeviceTrustResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;