import { LanguageCode, Localizer, Endpoint, Router, EndpointOfRequest, EndpointOfResponse } from "@guildion/core";

export default class BaseHandler<T extends Endpoint<Router.Factory<any>, any, any>> {
    public lang: LanguageCode;
    public localizer: Localizer;
    public endpoint: T;

    constructor(endpoint: T, { lang }: { lang: LanguageCode }) {
        this.endpoint = endpoint;
        this.lang = lang;
        this.localizer = new Localizer(lang);
    }
}