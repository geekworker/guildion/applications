import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomRoleDestroyRequest, RoomRoleDestroyResponse, RoomRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomRolesDataStore from "../../datastores/RoomRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomRolesEndpoint.Destroy> = class RoomRoleDestroyHandler extends BaseHandler<RoomRolesEndpoint.Destroy> implements HandlerImpl<RoomRolesEndpoint.Destroy> {
    private roomRolesDataStore: RoomRolesDataStore;

    constructor(endpoint: RoomRolesEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomRolesDataStore = new RoomRolesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomRoleDestroyRequest, props: GatewayProps): Promise<RoomRoleDestroyResponse> {
        await this.roomRolesDataStore.destroy({
            roleId: req.roleId,
            roomId: req.roomId,
            user: await props.account!.user!,
        });
        return new RoomRoleDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;