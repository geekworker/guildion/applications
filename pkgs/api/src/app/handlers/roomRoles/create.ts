import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomRoleCreateRequest, RoomRoleCreateResponse, RoomRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomRolesDataStore from "../../datastores/RoomRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomRolesEndpoint.Create> = class RoomRoleCreateHandler extends BaseHandler<RoomRolesEndpoint.Create> implements HandlerImpl<RoomRolesEndpoint.Create> {
    private roomRolesDataStore: RoomRolesDataStore;

    constructor(endpoint: RoomRolesEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomRolesDataStore = new RoomRolesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomRoleCreateRequest, props: GatewayProps): Promise<RoomRoleCreateResponse> {
        const roomRole = await this.roomRolesDataStore.create({
            roleId: req.roleId,
            roomId: req.roomId,
            user: await props.account!.user!,
        });
        return new RoomRoleCreateResponse({
            status: HTTPStatusCode.OK,
            role: await roomRole.transform(),
        });
    }
}

export default handler;