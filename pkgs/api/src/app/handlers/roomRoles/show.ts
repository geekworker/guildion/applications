import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomRoleShowRequest, RoomRoleShowResponse, RoomRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomRolesDataStore from "../../datastores/RoomRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomRolesEndpoint.Show> = class RoomRoleShowHandler extends BaseHandler<RoomRolesEndpoint.Show> implements HandlerImpl<RoomRolesEndpoint.Show> {
    private roomRolesDataStore: RoomRolesDataStore;

    constructor(endpoint: RoomRolesEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomRolesDataStore = new RoomRolesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomRoleShowRequest, props: GatewayProps): Promise<RoomRoleShowResponse> {
        const roomRole = await this.roomRolesDataStore.get({
            roleId: req.roleId,
            roomId: req.roomId,
            // user: await props.account!.user!,
        });
        return new RoomRoleShowResponse({
            status: HTTPStatusCode.OK,
            role: await roomRole.transform(),
        });
    }
}

export default handler;