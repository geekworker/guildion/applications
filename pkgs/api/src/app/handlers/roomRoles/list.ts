import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomRolesListRequest, RoomRolesListResponse, RoomRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomRolesDataStore from "../../datastores/RoomRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomRolesEndpoint.List> = class RoomRolesListHandler extends BaseHandler<RoomRolesEndpoint.List> implements HandlerImpl<RoomRolesEndpoint.List> {
    private roomRolesDataStore: RoomRolesDataStore;

    constructor(endpoint: RoomRolesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomRolesDataStore = new RoomRolesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomRolesListRequest, props: GatewayProps): Promise<RoomRolesListResponse> {
        const roomRoles = await this.roomRolesDataStore.gets(req.roomId);
        return new RoomRolesListResponse({
            status: HTTPStatusCode.OK,
            roles: await Promise.all(
                roomRoles.map(roomRole => roomRole.transform())
            ),
        });
    }
}

export default handler;