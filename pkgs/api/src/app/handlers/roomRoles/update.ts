import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomRoleUpdateRequest, RoomRoleUpdateResponse, RoomRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomRolesDataStore from "../../datastores/RoomRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomRolesEndpoint.Update> = class RoomRoleUpdateHandler extends BaseHandler<RoomRolesEndpoint.Update> implements HandlerImpl<RoomRolesEndpoint.Update> {
    private roomRolesDataStore: RoomRolesDataStore;

    constructor(endpoint: RoomRolesEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomRolesDataStore = new RoomRolesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomRoleUpdateRequest, props: GatewayProps): Promise<RoomRoleUpdateResponse> {
        const roomRole = await this.roomRolesDataStore.update(req.role, {
            user: await props.account!.user!,
        });
        return new RoomRoleUpdateResponse({
            status: HTTPStatusCode.OK,
            role: await roomRole.transform(),
        });
    }
}

export default handler;