import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomFileCreateRequest, RoomFileCreateResponse, RoomFilesEndpoint, HTTPStatusCode, LanguageCode, File } from '@guildion/core';
import RoomFilesDataStore from '../../datastores/RoomFilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomFilesEndpoint.Create> = class RoomFileCreateHandler extends BaseHandler<RoomFilesEndpoint.Create> implements HandlerImpl<RoomFilesEndpoint.Create> {
    private roomFilesDataStore: RoomFilesDataStore;

    constructor(endpoint: RoomFilesEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomFilesDataStore = new RoomFilesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomFileCreateRequest, props: GatewayProps): Promise<RoomFileCreateResponse> {
        const {
            file,
            filing,
            roomFile,
            presignedURL,
        } = await this.roomFilesDataStore.create(req.file, { user: await props.account?.user! });
        return new RoomFileCreateResponse({
            status: HTTPStatusCode.OK,
            file: await file.transform({
                filing,
                roomFile,
                presignedURL,
            }),
        });
    }
}

export default handler;