import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomFilesListRequest, RoomFilesListResponse, RoomFilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import RoomFilesDataStore from '../../datastores/RoomFilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomFilesEndpoint.List> = class RoomFilesListHandler extends BaseHandler<RoomFilesEndpoint.List> implements HandlerImpl<RoomFilesEndpoint.List> {
    private roomFilesDataStore: RoomFilesDataStore;

    constructor(endpoint: RoomFilesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomFilesDataStore = new RoomFilesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomFilesListRequest, props: GatewayProps): Promise<RoomFilesListResponse> {
        const {
            filings,
            paginatable
        } = await this.roomFilesDataStore.gets(req.id, { user: await props.account?.user!, count: req.count, offset: req.offset, types: req.types, statuses: req.statuses, });
        return new RoomFilesListResponse({
            status: HTTPStatusCode.OK,
            files: await Promise.all(
                filings.map(filing => filing.transform({ folder: true }))
            ),
            paginatable,
        });
    }
}

export default handler;