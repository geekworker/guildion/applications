import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentSectionsListRequest, DocumentSectionsListResponse, DocumentSectionsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import Bluebird from 'bluebird';
import DocumentSectionsDataStore from '../../datastores/DocumentSectionsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentSectionsEndpoint.List> = class DocumentSectionsListHandler extends BaseHandler<DocumentSectionsEndpoint.List> implements HandlerImpl<DocumentSectionsEndpoint.List> {
    private documentSectionsDataStore: DocumentSectionsDataStore;

    constructor(endpoint: DocumentSectionsEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentSectionsDataStore = new DocumentSectionsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentSectionsListRequest, props: GatewayProps): Promise<DocumentSectionsListResponse> {
        const { group, sections } = await this.documentSectionsDataStore.gets(req.groupSlug, { includes: req.includes ?? {} });
        return new DocumentSectionsListResponse({
            status: HTTPStatusCode.OK,
            group: await group.transform(),
            sections: await Bluebird.map(
                sections,
                async val => {
                    return val.section.transform({
                        documents: req.includes?.documents ? await Promise.all(
                            val.documents.map(d => {
                                d.jaData = '';
                                d.enData = '';
                                return d.transform();
                            })
                        ) : undefined,
                    })
                },
                { concurrency: 10 }
            ),
            paginatable: false,
        });
    }
}

export default handler;