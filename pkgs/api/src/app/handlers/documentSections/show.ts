import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentSectionShowRequest, DocumentSectionShowResponse, DocumentSectionsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import DocumentSectionsDataStore from '../../datastores/DocumentSectionsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentSectionsEndpoint.Show> = class DocumentSectionShowHandler extends BaseHandler<DocumentSectionsEndpoint.Show> implements HandlerImpl<DocumentSectionsEndpoint.Show> {
    private documentSectionsDataStore: DocumentSectionsDataStore;

    constructor(endpoint: DocumentSectionsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentSectionsDataStore = new DocumentSectionsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentSectionShowRequest, props: GatewayProps): Promise<DocumentSectionShowResponse> {
        const { group, document, section } = await this.documentSectionsDataStore.get(req.groupSlug, req.slug);
        const documentJSON = await document.transform({ prev: true, next: true });
        return new DocumentSectionShowResponse({
            status: HTTPStatusCode.OK,
            group: await group.transform(),
            section: await section.transform({
                documents: [documentJSON],
            }),
            document: documentJSON,
        });
    }
}

export default handler;