import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentSectionsShowPathsRequest, DocumentSectionsShowPathsResponse, DocumentSectionsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import DocumentSectionsDataStore from '../../datastores/DocumentSectionsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentSectionsEndpoint.ShowPaths> = class DocumentSectionsShowPathsHandler extends BaseHandler<DocumentSectionsEndpoint.ShowPaths> implements HandlerImpl<DocumentSectionsEndpoint.ShowPaths> {
    private documentSectionsDataStore: DocumentSectionsDataStore;

    constructor(endpoint: DocumentSectionsEndpoint.ShowPaths, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentSectionsDataStore = new DocumentSectionsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentSectionsShowPathsRequest, props: GatewayProps): Promise<DocumentSectionsShowPathsResponse> {
        const { paths } = await this.documentSectionsDataStore.getPaths();
        return new DocumentSectionsShowPathsResponse({
            paths,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;