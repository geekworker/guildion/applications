import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomMemberKickRequest, RoomMemberKickResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, Room } from "@guildion/core";
import { RoomMemberEntity } from "@guildion/db";
import RoomMembersDataStore from "../../datastores/RoomMembersDataStore";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.MemberKick> = class RoomMemberKickHandler extends BaseHandler<RoomsEndpoint.MemberKick> implements HandlerImpl<RoomsEndpoint.MemberKick> {
    private roomMembersDataStore: RoomMembersDataStore;
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.MemberKick, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMembersDataStore = new RoomMembersDataStore({ localizer: this.localizer });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomMemberKickRequest, props: GatewayProps): Promise<RoomMemberKickResponse> {
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                id: req.id,
            }
        });
        await this.roomsDataStore.validation.checkRoomMemberState(roomMember);
        await this.roomMembersDataStore.kick(roomMember!, { user: await props.account!.user! });
        return new RoomMemberKickResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;