import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomConnectingMembersListRequest, RoomConnectingMembersListResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomMembersDataStore from "../../datastores/RoomMembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.ConnectingMembersList> = class RoomConnectingMembersListHandler extends BaseHandler<RoomsEndpoint.ConnectingMembersList> implements HandlerImpl<RoomsEndpoint.ConnectingMembersList> {
    private roomMembersDataStore: RoomMembersDataStore;

    constructor(endpoint: RoomsEndpoint.ConnectingMembersList, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMembersDataStore = new RoomMembersDataStore({ localizer: this.localizer });
    }

    async run(req: RoomConnectingMembersListRequest, props: GatewayProps): Promise<RoomConnectingMembersListResponse> {
        const {
            roomMembers,
            paginatable,
        } = await this.roomMembersDataStore.getConnectings(req.roomId, { 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user!,
            ltAt: req.getLtAt(),
            gtAt: req.getGtAt(),
        });
        
        return new RoomConnectingMembersListResponse({
            status: HTTPStatusCode.OK,
            members: await Promise.all(
                roomMembers.map(roomMember => roomMember.transform())
            ),
            paginatable,
        });
    }
}

export default handler;