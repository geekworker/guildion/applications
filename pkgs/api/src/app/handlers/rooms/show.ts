import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomShowRequest, RoomShowResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Show> = class RoomShowHandler extends BaseHandler<RoomsEndpoint.Show> implements HandlerImpl<RoomsEndpoint.Show> {
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomShowRequest, props: GatewayProps): Promise<RoomShowResponse> {
        const {
            room,
        } = req.isPublic ? await this.roomsDataStore.getPublic(req.id, { 
            user: await props.account?.user,
        }) : await this.roomsDataStore.get(req.id, { 
            user: await props.account?.user,
        });
        
        return new RoomShowResponse({
            status: HTTPStatusCode.OK,
            room: await room.transform({
                currentMember: await room.currentMember?.transform({ roles: true, roomMember: room.currentRoomMember }),
                activity: true,
                notificationsCount: true,
                postNotificationsCount: true,
                messageNotificationsCount: true,
                roles: true,
            }),
        });
    }
}

export default handler;