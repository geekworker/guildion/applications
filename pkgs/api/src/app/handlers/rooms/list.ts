import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomsListRequest, RoomsListResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.List> = class RoomsListHandler extends BaseHandler<RoomsEndpoint.List> implements HandlerImpl<RoomsEndpoint.List> {
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomsListRequest, props: GatewayProps): Promise<RoomsListResponse> {
        const {
            rooms,
            paginatable,
        } = req.isPublic ?  await this.roomsDataStore.getsPublic(req.guildId, { 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user,
        }) : await this.roomsDataStore.gets(req.guildId, { 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user,
            ltAt: req.getLtAt(),
            gtAt: req.getGtAt(),
        });
        
        return new RoomsListResponse({
            status: HTTPStatusCode.OK,
            rooms: await Promise.all(
                rooms.map(async room => room.transform({
                    activity: true,
                    notificationsCount: !req.isPublic,
                    connectingMembers: !req.isPublic,
                    activedBaseAt: !req.isPublic ? req.getLtAt() ?? req.getGtAt() : new Date(),
                    currentMember: await room.currentMember?.transform({ roomMember: room.currentRoomMember }),
                }))
            ),
            paginatable,
        });
    }
}

export default handler;