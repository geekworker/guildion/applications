import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomsSearchRequest, RoomsSearchResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import RoomsDataStore from '../../datastores/RoomsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomsEndpoint.Search> = class RoomsSearchHandler extends BaseHandler<RoomsEndpoint.Search> implements HandlerImpl<RoomsEndpoint.Search> {
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.Search, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomsSearchRequest, props: GatewayProps): Promise<RoomsSearchResponse> {
        const { rooms, paginatable } = await this.roomsDataStore.search(req.query, { count: req.count, offset: req.offset, guildId: req.guildId, user: await props.account?.user });
        return new RoomsSearchResponse({
            status: HTTPStatusCode.OK,
            rooms: await Promise.all(
                rooms.map(async room => room.transform({
                    currentMember: await room.currentMember?.transform({ roles: true, roomMember: room.currentRoomMember }),
                    activity: true,
                    notificationsCount: true,
                    activedBaseAt: new Date(),
                }))
            ),
            paginatable,
        });
    }
}

export default handler;