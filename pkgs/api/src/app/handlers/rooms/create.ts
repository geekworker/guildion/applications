import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomCreateRequest, RoomCreateResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Create> = class RoomCreateHandler extends BaseHandler<RoomsEndpoint.Create> implements HandlerImpl<RoomsEndpoint.Create> {
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomCreateRequest, props: GatewayProps): Promise<RoomCreateResponse> {
        const room = await this.roomsDataStore.create(req.room, { user: await props.account!.user! });
        return new RoomCreateResponse({
            status: HTTPStatusCode.OK,
            room: await room.transform(),
        });
    }
}

export default handler;