import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomMemberUpdateRequest, RoomMemberUpdateResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, Room } from "@guildion/core";
import RoomMembersDataStore from "../../datastores/RoomMembersDataStore";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.MemberUpdate> = class RoomMemberUpdateHandler extends BaseHandler<RoomsEndpoint.MemberUpdate> implements HandlerImpl<RoomsEndpoint.MemberUpdate> {
    private roomMembersDataStore: RoomMembersDataStore;
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.MemberUpdate, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMembersDataStore = new RoomMembersDataStore({ localizer: this.localizer });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomMemberUpdateRequest, props: GatewayProps): Promise<RoomMemberUpdateResponse> {
        const {
            member,
            room,
        } = await this.roomsDataStore.get(req.member!.roomId!, { user: await props.account?.user!, })
        const roomMember = await this.roomMembersDataStore.update(req.member, { 
            member,
            room,
        });
        return new RoomMemberUpdateResponse({
            status: HTTPStatusCode.OK,
            member: await roomMember.transform(),
        });
    }
}

export default handler;