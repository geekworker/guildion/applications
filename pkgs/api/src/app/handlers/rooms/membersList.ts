import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomMembersListRequest, RoomMembersListResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomMembersDataStore from "../../datastores/RoomMembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.MembersList> = class RoomMembersListHandler extends BaseHandler<RoomsEndpoint.MembersList> implements HandlerImpl<RoomsEndpoint.MembersList> {
    private roomMembersDataStore: RoomMembersDataStore;

    constructor(endpoint: RoomsEndpoint.MembersList, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMembersDataStore = new RoomMembersDataStore({ localizer: this.localizer });
    }

    async run(req: RoomMembersListRequest, props: GatewayProps): Promise<RoomMembersListResponse> {
        const {
            roomMembers,
            paginatable,
        } = await this.roomMembersDataStore.gets(req.roomId, { 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user!,
        });
        
        return new RoomMembersListResponse({
            status: HTTPStatusCode.OK,
            members: await Promise.all(
                roomMembers.map(roomMember => roomMember.transform())
            ),
            paginatable,
        });
    }
}

export default handler;