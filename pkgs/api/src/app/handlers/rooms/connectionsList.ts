import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomConnectionsListRequest, RoomConnectionsListResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomMemberConnectionsDataStore from "../../datastores/RoomMemberConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.ConnectionsList> = class RoomConnectionsListHandler extends BaseHandler<RoomsEndpoint.ConnectionsList> implements HandlerImpl<RoomsEndpoint.ConnectionsList> {
    private roomMemberConnectionsDataStore: RoomMemberConnectionsDataStore;

    constructor(endpoint: RoomsEndpoint.ConnectionsList, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMemberConnectionsDataStore = new RoomMemberConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomConnectionsListRequest, props: GatewayProps): Promise<RoomConnectionsListResponse> {
        const {
            roomMemberConnections,
            paginatable,
        } = await this.roomMemberConnectionsDataStore.gets(req.id, {
            user: await props.account!.user!,
            count: req.count,
            offset: req.offset,
        });
        return new RoomConnectionsListResponse({
            status: HTTPStatusCode.OK,
            connections: await Promise.all(
                roomMemberConnections.map(
                    async roomMemberConnection => (await roomMemberConnection.connection).transform()
                )
            ),
            paginatable,
        });
    }
}

export default handler;