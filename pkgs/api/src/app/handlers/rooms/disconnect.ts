import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomDisconnectRequest, RoomDisconnectResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomMemberConnectionsDataStore from "../../datastores/RoomMemberConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Disconnect> = class RoomDisconnectHandler extends BaseHandler<RoomsEndpoint.Disconnect> implements HandlerImpl<RoomsEndpoint.Disconnect> {
    private roomMemberConnectionsDataStore: RoomMemberConnectionsDataStore;

    constructor(endpoint: RoomsEndpoint.Disconnect, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMemberConnectionsDataStore = new RoomMemberConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomDisconnectRequest, props: GatewayProps): Promise<RoomDisconnectResponse> {
        const results = await this.roomMemberConnectionsDataStore.destroyAll({ user: await props.account!.user!, device: props.device! });
        return new RoomDisconnectResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;