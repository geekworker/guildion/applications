import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomJoinRequest, RoomJoinResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import { RoomEntity } from "@guildion/db";
import RoomMembersDataStore from "../../datastores/RoomMembersDataStore";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Join> = class RoomJoinHandler extends BaseHandler<RoomsEndpoint.Join> implements HandlerImpl<RoomsEndpoint.Join> {
    private roomsDataStore: RoomsDataStore;
    private roomMembersDataStore: RoomMembersDataStore;

    constructor(endpoint: RoomsEndpoint.Join, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
        this.roomMembersDataStore = new RoomMembersDataStore({ localizer: this.localizer });
    }

    async run(req: RoomJoinRequest, props: GatewayProps): Promise<RoomJoinResponse> {
        const room = await RoomEntity.findOne({
            where: {
                id: req.id,
            }
        });
        await this.roomsDataStore.validation.checkRoomState(room);
        const roomMember = await this.roomMembersDataStore.join({ room: room!, user: await props.account!.user! });
        return new RoomJoinResponse({
            status: HTTPStatusCode.OK,
            room: await room!.transform(),
            member: await roomMember.transform(),
        });
    }
}

export default handler;