import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomDestroyRequest, RoomDestroyResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Destroy> = class RoomDestroyHandler extends BaseHandler<RoomsEndpoint.Destroy> implements HandlerImpl<RoomsEndpoint.Destroy> {
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomDestroyRequest, props: GatewayProps): Promise<RoomDestroyResponse> {
        await this.roomsDataStore.destroy(req.id, { user: await props.account!.user! });
        return new RoomDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;