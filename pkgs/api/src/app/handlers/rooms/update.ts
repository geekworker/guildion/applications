import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomUpdateRequest, RoomUpdateResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Update> = class RoomUpdateHandler extends BaseHandler<RoomsEndpoint.Update> implements HandlerImpl<RoomsEndpoint.Update> {
    private roomsDataStore: RoomsDataStore;

    constructor(endpoint: RoomsEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomUpdateRequest, props: GatewayProps): Promise<RoomUpdateResponse> {
        const room = await this.roomsDataStore.update(req.room, { user: await props.account!.user! });
        return new RoomUpdateResponse({
            status: HTTPStatusCode.OK,
            room: await room.transform(),
        });
    }
}

export default handler;