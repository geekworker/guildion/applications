import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomEntity } from "@guildion/db";
import { RoomLeaveRequest, RoomLeaveResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomMembersDataStore from "../../datastores/RoomMembersDataStore";
import RoomsDataStore from "../../datastores/RoomsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Leave> = class RoomLeaveHandler extends BaseHandler<RoomsEndpoint.Leave> implements HandlerImpl<RoomsEndpoint.Leave> {
    private roomsDataStore: RoomsDataStore;
    private roomMembersDataStore: RoomMembersDataStore;

    constructor(endpoint: RoomsEndpoint.Leave, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomsDataStore = new RoomsDataStore({ localizer: this.localizer });
        this.roomMembersDataStore = new RoomMembersDataStore({ localizer: this.localizer });
    }

    async run(req: RoomLeaveRequest, props: GatewayProps): Promise<RoomLeaveResponse> {
        const room = await RoomEntity.findOne({
            where: {
                id: req.id,
            }
        });
        await this.roomsDataStore.validation.checkRoomState(room);
        await this.roomMembersDataStore.leave({ room: room!, user: await props.account!.user! });
        return new RoomLeaveResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;