import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomConnectRequest, RoomConnectResponse, RoomsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import RoomMemberConnectionsDataStore from "../../datastores/RoomMemberConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomsEndpoint.Connect> = class RoomConnectHandler extends BaseHandler<RoomsEndpoint.Connect> implements HandlerImpl<RoomsEndpoint.Connect> {
    private roomMemberConnectionsDataStore: RoomMemberConnectionsDataStore;

    constructor(endpoint: RoomsEndpoint.Connect, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomMemberConnectionsDataStore = new RoomMemberConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomConnectRequest, props: GatewayProps): Promise<RoomConnectResponse> {
        const results = await this.roomMemberConnectionsDataStore.create(req.id, { user: await props.account!.user!, device: props.device! });
        return new RoomConnectResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;