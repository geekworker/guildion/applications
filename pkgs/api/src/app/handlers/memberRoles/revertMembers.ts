import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MemberRoleRevertMembersRequest, MemberRoleRevertMembersResponse, MemberRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import { MemberEntity } from "@guildion/db";
import MemberRolesDataStore from "../../datastores/MemberRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MemberRolesEndpoint.RevertMembers> = class MemberRoleRevertMembersHandler extends BaseHandler<MemberRolesEndpoint.RevertMembers> implements HandlerImpl<MemberRolesEndpoint.RevertMembers> {
    private memberRolesDataStore: MemberRolesDataStore;

    constructor(endpoint: MemberRolesEndpoint.RevertMembers, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.memberRolesDataStore = new MemberRolesDataStore({ localizer: this.localizer });
    }

    async run(req: MemberRoleRevertMembersRequest, props: GatewayProps): Promise<MemberRoleRevertMembersResponse> {
        const member = await MemberEntity.findOne({ where: { id: req, } })
        await this.memberRolesDataStore.revertMembers(member!, { user: await props.account?.user! });
        return new MemberRoleRevertMembersResponse({
            status: HTTPStatusCode.OK,
            success: true
        });
    }
}

export default handler;