import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MemberRoleMakeAdminRequest, MemberRoleMakeAdminResponse, MemberRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import { MemberEntity } from "@guildion/db";
import MemberRolesDataStore from "../../datastores/MemberRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MemberRolesEndpoint.MakeAdmin> = class MemberRoleMakeAdminHandler extends BaseHandler<MemberRolesEndpoint.MakeAdmin> implements HandlerImpl<MemberRolesEndpoint.MakeAdmin> {
    private memberRolesDataStore: MemberRolesDataStore;

    constructor(endpoint: MemberRolesEndpoint.MakeAdmin, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.memberRolesDataStore = new MemberRolesDataStore({ localizer: this.localizer });
    }

    async run(req: MemberRoleMakeAdminRequest, props: GatewayProps): Promise<MemberRoleMakeAdminResponse> {
        const member = await MemberEntity.findOne({ where: { id: req, } })
        await this.memberRolesDataStore.makeAdmin(member!, { user: await props.account?.user! });
        return new MemberRoleMakeAdminResponse({
            status: HTTPStatusCode.OK,
            success: true
        });
    }
}

export default handler;