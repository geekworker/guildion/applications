import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { MemberRolesListRequest, MemberRolesListResponse, MemberRolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import MemberRolesDataStore from "../../datastores/MemberRolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<MemberRolesEndpoint.List> = class MemberRolesListHandler extends BaseHandler<MemberRolesEndpoint.List> implements HandlerImpl<MemberRolesEndpoint.List> {
    private memberRolesDataStore: MemberRolesDataStore;

    constructor(endpoint: MemberRolesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.memberRolesDataStore = new MemberRolesDataStore({ localizer: this.localizer });
    }

    async run(req: MemberRolesListRequest, props: GatewayProps): Promise<MemberRolesListResponse> {
        const memberRoles = await this.memberRolesDataStore.getsFromID(req.memberId);
        return new MemberRolesListResponse({
            status: HTTPStatusCode.OK,
            roles: await Promise.all(
                memberRoles.map(memberRole => memberRole.transform())
            ),
        });
    }
}

export default handler;