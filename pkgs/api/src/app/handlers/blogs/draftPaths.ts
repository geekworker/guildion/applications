import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { BlogsDraftPathsRequest, BlogsDraftPathsResponse, BlogsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import BlogsDataStore from '../../datastores/BlogsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<BlogsEndpoint.DraftPaths> = class BlogsDraftPathsHandler extends BaseHandler<BlogsEndpoint.DraftPaths> implements HandlerImpl<BlogsEndpoint.DraftPaths> {
    private blogsDataStore: BlogsDataStore;

    constructor(endpoint: BlogsEndpoint.DraftPaths, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.blogsDataStore = new BlogsDataStore({ localizer: this.localizer });
    }

    async run(req: BlogsDraftPathsRequest, props: GatewayProps): Promise<BlogsDraftPathsResponse> {
        const { paths } = await this.blogsDataStore.getDraftPaths();
        return new BlogsDraftPathsResponse({
            paths,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;