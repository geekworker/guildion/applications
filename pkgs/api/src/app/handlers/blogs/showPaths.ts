import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { BlogsShowPathsRequest, BlogsShowPathsResponse, BlogsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import BlogsDataStore from '../../datastores/BlogsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<BlogsEndpoint.ShowPaths> = class BlogsShowPathsHandler extends BaseHandler<BlogsEndpoint.ShowPaths> implements HandlerImpl<BlogsEndpoint.ShowPaths> {
    private blogsDataStore: BlogsDataStore;

    constructor(endpoint: BlogsEndpoint.ShowPaths, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.blogsDataStore = new BlogsDataStore({ localizer: this.localizer });
    }

    async run(req: BlogsShowPathsRequest, props: GatewayProps): Promise<BlogsShowPathsResponse> {
        const { paths } = await this.blogsDataStore.getPaths();
        return new BlogsShowPathsResponse({
            paths,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;