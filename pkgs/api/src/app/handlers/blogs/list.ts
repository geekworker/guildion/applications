import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { BlogsListRequest, BlogsListResponse, BlogsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import BlogsDataStore from '../../datastores/BlogsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<BlogsEndpoint.List> = class BlogsListHandler extends BaseHandler<BlogsEndpoint.List> implements HandlerImpl<BlogsEndpoint.List> {
    private blogsDataStore: BlogsDataStore;

    constructor(endpoint: BlogsEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.blogsDataStore = new BlogsDataStore({ localizer: this.localizer });
    }

    async run(req: BlogsListRequest, props: GatewayProps): Promise<BlogsListResponse> {
        const {
            blogs,
            paginatable,
            category,
        } = await this.blogsDataStore.gets({ categorySlug: req.categorySlug, sort: req.sort, count: req.count, offset: req.offset });
        return new BlogsListResponse({
            status: HTTPStatusCode.OK,
            blogs: await Promise.all(
                blogs.map(blog => blog.transform({ category: true }))
            ),
            paginatable,
            category: await category?.transform({ parent: true }),
        });
    }
}

export default handler;