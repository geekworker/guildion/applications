import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { BlogDraftRequest, BlogDraftResponse, BlogsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import BlogsDataStore from '../../datastores/BlogsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<BlogsEndpoint.Draft> = class BlogDraftHandler extends BaseHandler<BlogsEndpoint.Draft> implements HandlerImpl<BlogsEndpoint.Draft> {
    private blogsDataStore: BlogsDataStore;

    constructor(endpoint: BlogsEndpoint.Draft, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.blogsDataStore = new BlogsDataStore({ localizer: this.localizer });
    }

    async run(req: BlogDraftRequest, props: GatewayProps): Promise<BlogDraftResponse> {
        const { blog } = await this.blogsDataStore.draft(req.slug);
        return new BlogDraftResponse({
            status: HTTPStatusCode.OK,
            blog: await blog.transform({
                prev: true,
                next: true,
                category: true,
            }),
        });
    }
}

export default handler;