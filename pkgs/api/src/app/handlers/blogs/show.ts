import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { BlogShowRequest, BlogShowResponse, BlogsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import BlogsDataStore from '../../datastores/BlogsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<BlogsEndpoint.Show> = class BlogShowHandler extends BaseHandler<BlogsEndpoint.Show> implements HandlerImpl<BlogsEndpoint.Show> {
    private blogsDataStore: BlogsDataStore;

    constructor(endpoint: BlogsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.blogsDataStore = new BlogsDataStore({ localizer: this.localizer });
    }

    async run(req: BlogShowRequest, props: GatewayProps): Promise<BlogShowResponse> {
        const { blog } = await this.blogsDataStore.get(req.slug);
        return new BlogShowResponse({
            status: HTTPStatusCode.OK,
            blog: await blog.transform({
                prev: true,
                next: true,
                category: true,
            }),
        });
    }
}

export default handler;