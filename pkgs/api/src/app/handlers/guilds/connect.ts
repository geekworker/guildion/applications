import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildConnectRequest, GuildConnectResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import MemberConnectionsDataStore from "../../datastores/MemberConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Connect> = class GuildConnectHandler extends BaseHandler<GuildsEndpoint.Connect> implements HandlerImpl<GuildsEndpoint.Connect> {
    private memberConnectionsDataStore: MemberConnectionsDataStore;

    constructor(endpoint: GuildsEndpoint.Connect, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.memberConnectionsDataStore = new MemberConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildConnectRequest, props: GatewayProps): Promise<GuildConnectResponse> {
        const results = await this.memberConnectionsDataStore.create(req.id, { user: await props.account!.user!, device: props.device! });
        return new GuildConnectResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;