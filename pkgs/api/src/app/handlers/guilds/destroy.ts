import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildDestroyRequest, GuildDestroyResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Destroy> = class GuildDestroyHandler extends BaseHandler<GuildsEndpoint.Destroy> implements HandlerImpl<GuildsEndpoint.Destroy> {
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: GuildsEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildDestroyRequest, props: GatewayProps): Promise<GuildDestroyResponse> {
        await this.guildsDataStore.destroy(req.id, { user: await props.account!.user! });
        return new GuildDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;