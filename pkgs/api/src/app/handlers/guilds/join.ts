import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildJoinRequest, GuildJoinResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import { GuildEntity } from "@guildion/db";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import MembersDataStore from "../../datastores/MembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Join> = class GuildJoinHandler extends BaseHandler<GuildsEndpoint.Join> implements HandlerImpl<GuildsEndpoint.Join> {
    private guildsDataStore: GuildsDataStore;
    private membersDataStore: MembersDataStore;

    constructor(endpoint: GuildsEndpoint.Join, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
        this.membersDataStore = new MembersDataStore({ localizer: this.localizer });
    }

    async run(req: GuildJoinRequest, props: GatewayProps): Promise<GuildJoinResponse> {
        const guild = await GuildEntity.findOne({
            where: {
                id: req.id,
            },
        });
        await this.guildsDataStore.validation.checkGuildState(guild);
        const member = await this.membersDataStore.join({ guild: guild!, user: await props.account!.user! });
        guild!.currentMember = member;
        const memberAttributes = await member.transform({ roles: true });
        return new GuildJoinResponse({
            status: HTTPStatusCode.OK,
            guild: await guild!.transform({
                currentMember: memberAttributes,
            }),
            member: memberAttributes,
        });
    }
}

export default handler;