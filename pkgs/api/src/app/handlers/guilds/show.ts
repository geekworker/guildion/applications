import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildShowRequest, GuildShowResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Show> = class GuildShowHandler extends BaseHandler<GuildsEndpoint.Show> implements HandlerImpl<GuildsEndpoint.Show> {
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: GuildsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildShowRequest, props: GatewayProps): Promise<GuildShowResponse> {
        const {
            guild,
        } = req.isPublic ?  await this.guildsDataStore.getPublic(req.id, { 
            user: await props.account?.user,
        }) : await this.guildsDataStore.get(req.id, { 
            user: await props.account?.user,
        });
        
        return new GuildShowResponse({
            status: HTTPStatusCode.OK,
            guild: await guild.transform({ 
                currentMember: await guild.currentMember?.transform({ roles: true }),
                notificationsCount: true,
                activity: true,
                roles: true,
            }),
        });
    }
}

export default handler;