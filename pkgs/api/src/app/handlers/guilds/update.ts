import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildUpdateRequest, GuildUpdateResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Update> = class GuildUpdateHandler extends BaseHandler<GuildsEndpoint.Update> implements HandlerImpl<GuildsEndpoint.Update> {
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: GuildsEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildUpdateRequest, props: GatewayProps): Promise<GuildUpdateResponse> {
        const guild = await this.guildsDataStore.update(req.guild, { user: await props.account!.user! });
        return new GuildUpdateResponse({
            status: HTTPStatusCode.OK,
            guild: await guild.transform({ owner: true, ownerMember: true }),
        });
    }
}

export default handler;