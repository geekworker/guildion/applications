import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildEntity } from "@guildion/db";
import { GuildLeaveRequest, GuildLeaveResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import MembersDataStore from "../../datastores/MembersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Leave> = class GuildLeaveHandler extends BaseHandler<GuildsEndpoint.Leave> implements HandlerImpl<GuildsEndpoint.Leave> {
    private guildsDataStore: GuildsDataStore;
    private membersDataStore: MembersDataStore;

    constructor(endpoint: GuildsEndpoint.Leave, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
        this.membersDataStore = new MembersDataStore({ localizer: this.localizer });
    }

    async run(req: GuildLeaveRequest, props: GatewayProps): Promise<GuildLeaveResponse> {
        const guild = await GuildEntity.findOne({
            where: {
                id: req.id,
            },
        });
        await this.guildsDataStore.validation.checkGuildState(guild);
        await this.membersDataStore.leave({ guild: guild!, user: await props.account!.user! });
        return new GuildLeaveResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;