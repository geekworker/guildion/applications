import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildsListRequest, GuildsListResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.List> = class GuildsListHandler extends BaseHandler<GuildsEndpoint.List> implements HandlerImpl<GuildsEndpoint.List> {
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: GuildsEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildsListRequest, props: GatewayProps): Promise<GuildsListResponse> {
        const {
            guilds,
            paginatable,
        } = await this.guildsDataStore.gets({ 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user!,
        });
        
        return new GuildsListResponse({
            status: HTTPStatusCode.OK,
            guilds: await Promise.all(
                guilds.map(async (guild, i) => guild.transform({
                    notificationsCount: true,
                    activity: true,
                    currentMember: await guild.currentMember?.transform(),
                }))
            ),
            paginatable,
        });
    }
}

export default handler;