import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildConnectionsListRequest, GuildConnectionsListResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import MemberConnectionsDataStore from "../../datastores/MemberConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.ConnectionsList> = class GuildConnectionsListHandler extends BaseHandler<GuildsEndpoint.ConnectionsList> implements HandlerImpl<GuildsEndpoint.ConnectionsList> {
    private memberConnectionsDataStore: MemberConnectionsDataStore;

    constructor(endpoint: GuildsEndpoint.ConnectionsList, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.memberConnectionsDataStore = new MemberConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildConnectionsListRequest, props: GatewayProps): Promise<GuildConnectionsListResponse> {
        const {
            memberConnections,
            paginatable,
        } = await this.memberConnectionsDataStore.gets(req.id, {
            user: await props.account!.user!,
            count: req.count,
            offset: req.offset,
        });
        return new GuildConnectionsListResponse({
            status: HTTPStatusCode.OK,
            connections: await Promise.all(
                memberConnections.map(
                    async memberConnection => (await memberConnection.connection)!.transform()
                )
            ),
            paginatable,
        });
    }
}

export default handler;