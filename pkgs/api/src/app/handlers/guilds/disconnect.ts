import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildDisconnectRequest, GuildDisconnectResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import MemberConnectionsDataStore from "../../datastores/MemberConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Disconnect> = class GuildDisconnectHandler extends BaseHandler<GuildsEndpoint.Disconnect> implements HandlerImpl<GuildsEndpoint.Disconnect> {
    private memberConnectionsDataStore: MemberConnectionsDataStore;

    constructor(endpoint: GuildsEndpoint.Disconnect, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.memberConnectionsDataStore = new MemberConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildDisconnectRequest, props: GatewayProps): Promise<GuildDisconnectResponse> {
        const results = await this.memberConnectionsDataStore.destroyAll({ user: await props.account!.user!, device: props.device! });
        return new GuildDisconnectResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;