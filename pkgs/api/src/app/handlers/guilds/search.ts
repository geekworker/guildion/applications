import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { GuildsSearchRequest, GuildsSearchResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import GuildsDataStore from '../../datastores/GuildsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<GuildsEndpoint.Search> = class GuildsSearchHandler extends BaseHandler<GuildsEndpoint.Search> implements HandlerImpl<GuildsEndpoint.Search> {
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: GuildsEndpoint.Search, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildsSearchRequest, props: GatewayProps): Promise<GuildsSearchResponse> {
        const {
            guilds,
            paginatable,
        } = await this.guildsDataStore.search(req.query, { count: req.count, offset: req.offset, user: await props.account?.user });
        return new GuildsSearchResponse({
            status: HTTPStatusCode.OK,
            guilds: await Promise.all(
                guilds.map(async (guild, i) => guild.transform({
                    activity: true,
                    currentMember: await guild.currentMember?.transform(),
                }))
            ),
            paginatable,
        });
    }
}

export default handler;