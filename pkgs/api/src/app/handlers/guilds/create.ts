import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { GuildCreateRequest, GuildCreateResponse, GuildsEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<GuildsEndpoint.Create> = class GuildCreateHandler extends BaseHandler<GuildsEndpoint.Create> implements HandlerImpl<GuildsEndpoint.Create> {
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: GuildsEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: GuildCreateRequest, props: GatewayProps): Promise<GuildCreateResponse> {
        const guild = await this.guildsDataStore.create(req.guild, { user: await props.account!.user! });
        return new GuildCreateResponse({
            status: HTTPStatusCode.OK,
            guild: await guild.transform({ owner: true, ownerMember: true }),
        });
    }
}

export default handler;