import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { AccountAllowSessionRequest, AccountAllowSessionResponse, AccountsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import AccountsDataStore from "../../datastores/AccountsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<AccountsEndpoint.AllowSession> = class AccountAllowSessionHandler extends BaseHandler<AccountsEndpoint.AllowSession> implements HandlerImpl<AccountsEndpoint.AllowSession> {
    private accountsDataStore: AccountsDataStore;

    constructor(endpoint: AccountsEndpoint.AllowSession, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.accountsDataStore = new AccountsDataStore({ localizer: this.localizer });
    }

    async run(req: AccountAllowSessionRequest, props: GatewayProps): Promise<AccountAllowSessionResponse> {
        await this.accountsDataStore.allowSession(req.intervalSec, { user: (await props.account!.user)!, device: props.device! })
        return new AccountAllowSessionResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;