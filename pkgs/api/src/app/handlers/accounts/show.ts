import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { AccountShowRequest, AccountShowResponse, AccountsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import AccountsDataStore from "../../datastores/AccountsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<AccountsEndpoint.Show> = class AccountShowHandler extends BaseHandler<AccountsEndpoint.Show> implements HandlerImpl<AccountsEndpoint.Show> {
    private accountsDataStore: AccountsDataStore;

    constructor(endpoint: AccountsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.accountsDataStore = new AccountsDataStore({ localizer: this.localizer });
    }

    async run(req: AccountShowRequest, props: GatewayProps): Promise<AccountShowResponse> {
        return new AccountShowResponse({
            status: HTTPStatusCode.OK,
            account: await props.account!.transform({
                user: await (await props.account?.user)?.transform({ profile: true }) || true,
            }),
            success: !!props.account,
        });
    }
}

export default handler;