import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { AccountDenySessionRequest, AccountDenySessionResponse, AccountsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import AccountsDataStore from "../../datastores/AccountsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<AccountsEndpoint.DenySession> = class AccountDenySessionHandler extends BaseHandler<AccountsEndpoint.DenySession> implements HandlerImpl<AccountsEndpoint.DenySession> {
    private accountsDataStore: AccountsDataStore;

    constructor(endpoint: AccountsEndpoint.DenySession, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.accountsDataStore = new AccountsDataStore({ localizer: this.localizer });
    }

    async run(req: AccountDenySessionRequest, props: GatewayProps): Promise<AccountDenySessionResponse> {
        await this.accountsDataStore.denySession({ user: await props.account!.user!, device: props.device! })
        return new AccountDenySessionResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;