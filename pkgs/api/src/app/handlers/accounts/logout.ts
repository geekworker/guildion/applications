import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { AccountLogoutRequest, AccountLogoutResponse, AccountsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import AccountsDataStore from "../../datastores/AccountsDataStore";
import AccessTokensDataStore from "../../datastores/AccessTokensDataStore";
import UserDevicesDataStore from "../../datastores/UserDevicesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";
import { UserEntity } from "@guildion/db";

const handler: HandlerConstructor<AccountsEndpoint.Logout> = class AccountLogoutHandler extends BaseHandler<AccountsEndpoint.Logout> implements HandlerImpl<AccountsEndpoint.Logout> {
    private accountsDataStore: AccountsDataStore;
    private userDevicesDataStore: UserDevicesDataStore;

    constructor(endpoint: AccountsEndpoint.Logout, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.accountsDataStore = new AccountsDataStore({ localizer: this.localizer });
        this.userDevicesDataStore = new UserDevicesDataStore({ localizer: this.localizer });
    }

    async run(req: AccountLogoutRequest, props: GatewayProps): Promise<AccountLogoutResponse> {
        const user = await UserEntity.findOne({ where: { id: req.user.id } });
        await this.accountsDataStore.logout({ user: user!, device: props.device! });
        await this.userDevicesDataStore.logout({ user: user!, device: props.device! });
        return new AccountLogoutResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;