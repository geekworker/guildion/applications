import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { AccountAuthenticateRequest, AccountAuthenticateResponse, AccountsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import AccountsDataStore from "../../datastores/AccountsDataStore";
import AccessTokensDataStore from "../../datastores/AccessTokensDataStore";
import UserDevicesDataStore from "../../datastores/UserDevicesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";
import { UserEntity } from "@guildion/db";

const handler: HandlerConstructor<AccountsEndpoint.Authenticate> = class AccountAuthenticateHandler extends BaseHandler<AccountsEndpoint.Authenticate> implements HandlerImpl<AccountsEndpoint.Authenticate> {
    private accountsDataStore: AccountsDataStore;
    private accessTokensDataStore: AccessTokensDataStore;
    private userDevicesDataStore: UserDevicesDataStore;

    constructor(endpoint: AccountsEndpoint.Authenticate, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.accountsDataStore = new AccountsDataStore({ localizer: this.localizer });
        this.accessTokensDataStore = new AccessTokensDataStore({ localizer: this.localizer });
        this.userDevicesDataStore = new UserDevicesDataStore({ localizer: this.localizer });
    }

    async run(req: AccountAuthenticateRequest, props: GatewayProps): Promise<AccountAuthenticateResponse> {
        const { account, shouldTwoFactorAuth } = await this.accountsDataStore.authenticate({ username: req.username, device: props.device! });
        const user = await account?.user ?? await UserEntity.findOne({ where: { username: req.username } });
        if (shouldTwoFactorAuth) {
            await this.userDevicesDataStore.findOrCreate({ device: props.device!, user: user! })
            return new AccountAuthenticateResponse({
                status: HTTPStatusCode.OK,
                account: undefined,
                user: undefined,
                shouldTwoFactorAuth,
                accessToken: '',
            });
        } else if (account && user) {
            const [accessToken] = await Promise.all([
                this.accessTokensDataStore.create({ account, device: props.device! }),
                this.userDevicesDataStore.findOrCreate({ device: props.device!, user: user! })
            ]);
            await Promise.all([
                await this.userDevicesDataStore.login({ device: props.device!, user: user! }),
                await this.userDevicesDataStore.trust(props.device!.id, { device: props.device!, user: user! })
            ]);
            return new AccountAuthenticateResponse({
                status: HTTPStatusCode.OK,
                account: await account?.transform(),
                user: await user?.transform(),
                shouldTwoFactorAuth,
                accessToken,
            });
        } else {
            return new AccountAuthenticateResponse({
                status: HTTPStatusCode.OK,
                account: undefined,
                user: undefined,
                shouldTwoFactorAuth,
                accessToken: '',
            });
        }
    }
}

export default handler;