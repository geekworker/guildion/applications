import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { AccountRegisterRequest, AccountRegisterResponse, AccountsEndpoint, AppError, HTTPStatusCode, LanguageCode, ErrorType } from "@guildion/core";
import AccessTokensDataStore from "../../datastores/AccessTokensDataStore";
import AccountsDataStore from "../../datastores/AccountsDataStore";
import UserDevicesDataStore from "../../datastores/UserDevicesDataStore";
import GuildsDataStore from "../../datastores/GuildsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<AccountsEndpoint.Register> = class AccountRegisterHandler extends BaseHandler<AccountsEndpoint.Register> implements HandlerImpl<AccountsEndpoint.Register> {
    private accountsDataStore: AccountsDataStore;
    private accessTokensDataStore: AccessTokensDataStore;
    private userDevicesDataStore: UserDevicesDataStore;
    private guildsDataStore: GuildsDataStore;

    constructor(endpoint: AccountsEndpoint.Register, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.accountsDataStore = new AccountsDataStore({ localizer: this.localizer });
        this.accessTokensDataStore = new AccessTokensDataStore({ localizer: this.localizer });
        this.userDevicesDataStore = new UserDevicesDataStore({ localizer: this.localizer });
        this.guildsDataStore = new GuildsDataStore({ localizer: this.localizer });
    }

    async run(req: AccountRegisterRequest, props: GatewayProps): Promise<AccountRegisterResponse> {
        await this.accountsDataStore.validation.checkDeviceRegisterable(props.device!).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });
        const { user, account } = await this.accountsDataStore.create(req.user);
        const [accessToken, userDevice] = await Promise.all([
            this.accessTokensDataStore.create({ account, device: props.device! }),
            this.userDevicesDataStore.findOrCreate({ device: props.device!, user: user }),
        ]).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });
        const [] = await Promise.all([
            this.userDevicesDataStore.login({ device: props.device!, user: user }),
            this.userDevicesDataStore.trust(props.device!.id, { device: props.device!, user: user }),
        ]);
        return new AccountRegisterResponse({
            status: HTTPStatusCode.OK,
            account: await account.transform(),
            user: await user.transform(),
            accessToken,
        });
    }
}

export default handler;