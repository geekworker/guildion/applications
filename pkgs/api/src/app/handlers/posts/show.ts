import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { PostShowRequest, PostShowResponse, PostsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import PostsDataStore from '../../datastores/PostsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<PostsEndpoint.Show> = class PostShowHandler extends BaseHandler<PostsEndpoint.Show> implements HandlerImpl<PostsEndpoint.Show> {
    private postsDataStore: PostsDataStore;

    constructor(endpoint: PostsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.postsDataStore = new PostsDataStore({ localizer: this.localizer });
    }

    async run(req: PostShowRequest, props: GatewayProps): Promise<PostShowResponse> {
        const { post, member } = await this.postsDataStore.get(req.id, { user: await props.account?.user });
        return new PostShowResponse({
            status: HTTPStatusCode.OK,
            post: await post.transform({
                sender: true,
                files: true,
                currentMember: member,
            }),
        });
    }
}

export default handler;