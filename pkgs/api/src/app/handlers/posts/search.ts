import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { PostsSearchRequest, PostsSearchResponse, PostsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import PostsDataStore from '../../datastores/PostsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<PostsEndpoint.Search> = class PostsSearchHandler extends BaseHandler<PostsEndpoint.Search> implements HandlerImpl<PostsEndpoint.Search> {
    private postsDataStore: PostsDataStore;

    constructor(endpoint: PostsEndpoint.Search, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.postsDataStore = new PostsDataStore({ localizer: this.localizer });
    }

    async run(req: PostsSearchRequest, props: GatewayProps): Promise<PostsSearchResponse> {
        const { posts, paginatable } = await this.postsDataStore.search(req.query, { count: req.count, offset: req.offset, roomId: req.roomId, user: await props.account?.user })
        return new PostsSearchResponse({
            status: HTTPStatusCode.OK,
            posts: await Promise.all(
                posts.map(post => post.transform({
                    currentMember: post.currentMember,
                    sender: true,
                    files: true,
                }))
            ),
            paginatable,
        });
    }
}

export default handler;