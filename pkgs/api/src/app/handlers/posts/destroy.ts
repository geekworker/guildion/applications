import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { PostDestroyRequest, PostDestroyResponse, PostsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import PostsDataStore from '../../datastores/PostsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<PostsEndpoint.Destroy> = class PostDestroyHandler extends BaseHandler<PostsEndpoint.Destroy> implements HandlerImpl<PostsEndpoint.Destroy> {
    private postsDataStore: PostsDataStore;

    constructor(endpoint: PostsEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.postsDataStore = new PostsDataStore({ localizer: this.localizer });
    }

    async run(req: PostDestroyRequest, props: GatewayProps): Promise<PostDestroyResponse> {
        await this.postsDataStore.destroy(req.id, { user: await props.account?.user! })
        return new PostDestroyResponse({
            success: true,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;