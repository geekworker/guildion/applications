import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { PostsListRequest, PostsListResponse, PostsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import PostsDataStore from '../../datastores/PostsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<PostsEndpoint.List> = class PostsListHandler extends BaseHandler<PostsEndpoint.List> implements HandlerImpl<PostsEndpoint.List> {
    private postsDataStore: PostsDataStore;

    constructor(endpoint: PostsEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.postsDataStore = new PostsDataStore({ localizer: this.localizer });
    }

    async run(req: PostsListRequest, props: GatewayProps): Promise<PostsListResponse> {
        const {
            posts,
            paginatable,
            member,
        } = req.isPublic ? await this.postsDataStore.getsPublic(
            req.roomId,
            {
                user: await props.account?.user,
                postId: req.postId,
                count: req.count,
                offset: req.offset,
            }
        ) : await this.postsDataStore.gets(
            req.roomId,
            {
                user: await props.account?.user,
                postId: req.postId,
                count: req.count,
                offset: req.offset,
            }
        );
        return new PostsListResponse({
            posts: await Promise.all(
                posts.map(post => post.transform({
                    sender: true,
                    files: true,
                    currentMember: member,
                }))
            ),
            paginatable,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;