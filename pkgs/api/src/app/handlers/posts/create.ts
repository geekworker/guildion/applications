import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { PostCreateRequest, PostCreateResponse, PostsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import PostsDataStore from '../../datastores/PostsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<PostsEndpoint.Create> = class PostCreateHandler extends BaseHandler<PostsEndpoint.Create> implements HandlerImpl<PostsEndpoint.Create> {
    private postsDataStore: PostsDataStore;

    constructor(endpoint: PostsEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.postsDataStore = new PostsDataStore({ localizer: this.localizer });
    }

    async run(req: PostCreateRequest, props: GatewayProps): Promise<PostCreateResponse> {
        const post = await this.postsDataStore.create(req.post, { user: await props.account?.user! })
        return new PostCreateResponse({
            post: await post.transform({
                sender: true,
                files: true,
            }),
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;