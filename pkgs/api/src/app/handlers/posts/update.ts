import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { PostUpdateRequest, PostUpdateResponse, PostsEndpoint, HTTPStatusCode, LanguageCode, Post } from '@guildion/core';
import PostsDataStore from '../../datastores/PostsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<PostsEndpoint.Update> = class PostUpdateHandler extends BaseHandler<PostsEndpoint.Update> implements HandlerImpl<PostsEndpoint.Update> {
    private postsDataStore: PostsDataStore;

    constructor(endpoint: PostsEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.postsDataStore = new PostsDataStore({ localizer: this.localizer });
    }

    async run(req: PostUpdateRequest, props: GatewayProps): Promise<PostUpdateResponse> {
        const post = await this.postsDataStore.update(req.post, {
            user: await props.account?.user!,
        });
        return new PostUpdateResponse({
            post: await post.transform({
                files: true,
                sender: true,
            }),
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;