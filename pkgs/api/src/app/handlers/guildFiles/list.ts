import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { GuildFilesListRequest, GuildFilesListResponse, GuildFilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import GuildFilesDataStore from '../../datastores/GuildFilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<GuildFilesEndpoint.List> = class GuildFilesListHandler extends BaseHandler<GuildFilesEndpoint.List> implements HandlerImpl<GuildFilesEndpoint.List> {
    private guildFilesDataStore: GuildFilesDataStore;

    constructor(endpoint: GuildFilesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildFilesDataStore = new GuildFilesDataStore({ localizer: this.localizer });
    }

    async run(req: GuildFilesListRequest, props: GatewayProps): Promise<GuildFilesListResponse> {
        const {
            filings,
            paginatable
        } = await this.guildFilesDataStore.gets(req.id, { user: await props.account?.user!, count: req.count, offset: req.offset, types: req.types, statuses: req.statuses, });
        return new GuildFilesListResponse({
            status: HTTPStatusCode.OK,
            files: await Promise.all(
                filings.map(filing => filing.transform({ folder: true }))
            ),
            paginatable,
        });
    }
}

export default handler;