import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { GuildFileCreateRequest, GuildFileCreateResponse, GuildFilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import GuildFilesDataStore from '../../datastores/GuildFilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<GuildFilesEndpoint.Create> = class GuildFileCreateHandler extends BaseHandler<GuildFilesEndpoint.Create> implements HandlerImpl<GuildFilesEndpoint.Create> {
    private guildFilesDataStore: GuildFilesDataStore;

    constructor(endpoint: GuildFilesEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildFilesDataStore = new GuildFilesDataStore({ localizer: this.localizer });
    }

    async run(req: GuildFileCreateRequest, props: GatewayProps): Promise<GuildFileCreateResponse> {
        const {
            filing,
            presignedURL,
        } = await this.guildFilesDataStore.create(req.file, { user: await props.account?.user! })
        return new GuildFileCreateResponse({
            status: HTTPStatusCode.OK,
            file: await filing.transform({
                file: true,
                folder: true,
                presignedURL,
            }),
        });
    }
}

export default handler;