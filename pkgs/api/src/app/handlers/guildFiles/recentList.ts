import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { GuildFilesRecentListRequest, GuildFilesRecentListResponse, GuildFilesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import GuildFilesDataStore from '../../datastores/GuildFilesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<GuildFilesEndpoint.RecentList> = class GuildFilesRecentListHandler extends BaseHandler<GuildFilesEndpoint.RecentList> implements HandlerImpl<GuildFilesEndpoint.RecentList> {
    private guildFilesDataStore: GuildFilesDataStore;

    constructor(endpoint: GuildFilesEndpoint.RecentList, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.guildFilesDataStore = new GuildFilesDataStore({ localizer: this.localizer });
    }

    async run(req: GuildFilesRecentListRequest, props: GatewayProps): Promise<GuildFilesRecentListResponse> {
        const {
            filings,
            paginatable
        } = await this.guildFilesDataStore.getsRecently(req.id, { user: await props.account?.user!, count: req.count, offset: req.offset, types: req.types, statuses: req.statuses, roomId: req.roomId });
        return new GuildFilesRecentListResponse({
            status: HTTPStatusCode.OK,
            files: await Promise.all(
                filings.map(filing => filing.transform({ folder: true })),
            ),
            paginatable,
        });
    }
}

export default handler;