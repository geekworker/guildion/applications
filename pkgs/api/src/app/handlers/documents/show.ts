import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentShowRequest, DocumentShowResponse, DocumentsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import DocumentsDataStore from '../../datastores/DocumentsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentsEndpoint.Show> = class DocumentShowHandler extends BaseHandler<DocumentsEndpoint.Show> implements HandlerImpl<DocumentsEndpoint.Show> {
    private documentsDataStore: DocumentsDataStore;

    constructor(endpoint: DocumentsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentsDataStore = new DocumentsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentShowRequest, props: GatewayProps): Promise<DocumentShowResponse> {
        const { group, document, section } = await this.documentsDataStore.get(req.groupSlug, req.sectionSlug, req.slug);
        return new DocumentShowResponse({
            status: HTTPStatusCode.OK,
            document: await document.transform({
                section: await section.transform({ group: await group.transform() }),
                prev: true,
                next: true,
            }),
        });
    }
}

export default handler;