import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentsShowPathsRequest, DocumentsShowPathsResponse, DocumentsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import DocumentsDataStore from '../../datastores/DocumentsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentsEndpoint.ShowPaths> = class DocumentsShowPathsHandler extends BaseHandler<DocumentsEndpoint.ShowPaths> implements HandlerImpl<DocumentsEndpoint.ShowPaths> {
    private documentsDataStore: DocumentsDataStore;

    constructor(endpoint: DocumentsEndpoint.ShowPaths, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentsDataStore = new DocumentsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentsShowPathsRequest, props: GatewayProps): Promise<DocumentsShowPathsResponse> {
        const { paths } = await this.documentsDataStore.getPaths();
        return new DocumentsShowPathsResponse({
            paths,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;