import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { ConnectionDestroyRequest, ConnectionDestroyResponse, ConnectionsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import ConnectionsDataStore from "../../datastores/ConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<ConnectionsEndpoint.Destroy> = class ConnectionDestroyHandler extends BaseHandler<ConnectionsEndpoint.Destroy> implements HandlerImpl<ConnectionsEndpoint.Destroy> {
    private connectionsDataStore: ConnectionsDataStore;

    constructor(endpoint: ConnectionsEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.connectionsDataStore = new ConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: ConnectionDestroyRequest, props: GatewayProps): Promise<ConnectionDestroyResponse> {
        await this.connectionsDataStore.destroy(req.id);
        return new ConnectionDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;