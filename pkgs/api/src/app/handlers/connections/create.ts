import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { ConnectionCreateRequest, ConnectionCreateResponse, ConnectionsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import ConnectionsDataStore from "../../datastores/ConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<ConnectionsEndpoint.Create> = class ConnectionCreateHandler extends BaseHandler<ConnectionsEndpoint.Create> implements HandlerImpl<ConnectionsEndpoint.Create> {
    private connectionsDataStore: ConnectionsDataStore;

    constructor(endpoint: ConnectionsEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.connectionsDataStore = new ConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: ConnectionCreateRequest, props: GatewayProps): Promise<ConnectionCreateResponse> {
        const connection = await this.connectionsDataStore.create(req.connection, { deviceId: req.deviceId });
        return new ConnectionCreateResponse({
            status: HTTPStatusCode.OK,
            connection: await connection.transform(),
        });
    }
}

export default handler;