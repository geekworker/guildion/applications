import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DiscoveriesRecommendRequest, DiscoveriesRecommendResponse, DiscoveriesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import DiscoveriesDataStore from '../../datastores/DiscoveriesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DiscoveriesEndpoint.Recommend> = class DiscoveriesRecommendHandler extends BaseHandler<DiscoveriesEndpoint.Recommend> implements HandlerImpl<DiscoveriesEndpoint.Recommend> {
    private discoveriesDataStore: DiscoveriesDataStore;

    constructor(endpoint: DiscoveriesEndpoint.Recommend, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.discoveriesDataStore = new DiscoveriesDataStore({ localizer: this.localizer });
    }

    async run(req: DiscoveriesRecommendRequest, props: GatewayProps): Promise<DiscoveriesRecommendResponse> {
        const { discoveries, paginatable } = await this.discoveriesDataStore.recommend({ device: props.device!, count: req.count, offset: req.offset });
        return new DiscoveriesRecommendResponse({
            status: HTTPStatusCode.OK,
            discoveries: await Promise.all(
                discoveries.map(discovery => discovery.transform({
                    guilds: true,
                    rooms: true,
                }))
            ),
            paginatable,
        });
    }
}

export default handler;