import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentGroupShowRequest, DocumentGroupShowResponse, DocumentGroupsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import Bluebird from 'bluebird';
import DocumentGroupsDataStore from '../../datastores/DocumentGroupsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentGroupsEndpoint.Show> = class DocumentGroupShowHandler extends BaseHandler<DocumentGroupsEndpoint.Show> implements HandlerImpl<DocumentGroupsEndpoint.Show> {
    private documentGroupsDataStore: DocumentGroupsDataStore;

    constructor(endpoint: DocumentGroupsEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentGroupsDataStore = new DocumentGroupsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentGroupShowRequest, props: GatewayProps): Promise<DocumentGroupShowResponse> {
        const { group, sections } = await this.documentGroupsDataStore.get(req.slug);
        return new DocumentGroupShowResponse({
            status: HTTPStatusCode.OK,
            group: await group.transform({
                sections: await Bluebird.map(
                    sections,
                    async val => {
                        return val.section.transform({
                            documents: await Promise.all(
                                val.documents.map(d => {
                                    d.jaData = '';
                                    d.enData = '';
                                    return d.transform();
                                })
                            ),
                        })
                    },
                    { concurrency: 10 }
                ),
            }),
        });
    }
}

export default handler;