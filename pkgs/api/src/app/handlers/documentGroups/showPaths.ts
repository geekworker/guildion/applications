import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { DocumentGroupsShowPathsRequest, DocumentGroupsShowPathsResponse, DocumentGroupsEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import DocumentGroupsDataStore from '../../datastores/DocumentGroupsDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<DocumentGroupsEndpoint.ShowPaths> = class DocumentGroupsShowPathsHandler extends BaseHandler<DocumentGroupsEndpoint.ShowPaths> implements HandlerImpl<DocumentGroupsEndpoint.ShowPaths> {
    private documentGroupsDataStore: DocumentGroupsDataStore;

    constructor(endpoint: DocumentGroupsEndpoint.ShowPaths, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.documentGroupsDataStore = new DocumentGroupsDataStore({ localizer: this.localizer });
    }

    async run(req: DocumentGroupsShowPathsRequest, props: GatewayProps): Promise<DocumentGroupsShowPathsResponse> {
        const { paths } = await this.documentGroupsDataStore.getPaths();
        return new DocumentGroupsShowPathsResponse({
            status: HTTPStatusCode.OK,
            paths,
        });
    }
}

export default handler;