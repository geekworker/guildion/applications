import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { UserConnectRequest, UserConnectResponse, UsersEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import UserConnectionsDataStore from "../../datastores/UserConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<UsersEndpoint.Connect> = class UserConnectHandler extends BaseHandler<UsersEndpoint.Connect> implements HandlerImpl<UsersEndpoint.Connect> {
    private userConnectionsDataStore: UserConnectionsDataStore;

    constructor(endpoint: UsersEndpoint.Connect, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.userConnectionsDataStore = new UserConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: UserConnectRequest, props: GatewayProps): Promise<UserConnectResponse> {
        await this.userConnectionsDataStore.create({ user: await props.account!.user!, device: props.device! });
        return new UserConnectResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;