import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { UserDisconnectRequest, UserDisconnectResponse, UsersEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import UserConnectionsDataStore from "../../datastores/UserConnectionsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<UsersEndpoint.Disconnect> = class UserDisconnectHandler extends BaseHandler<UsersEndpoint.Disconnect> implements HandlerImpl<UsersEndpoint.Disconnect> {
    private userConnectionsDataStore: UserConnectionsDataStore;

    constructor(endpoint: UsersEndpoint.Disconnect, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.userConnectionsDataStore = new UserConnectionsDataStore({ localizer: this.localizer });
    }

    async run(req: UserDisconnectRequest, props: GatewayProps): Promise<UserDisconnectResponse> {
        await this.userConnectionsDataStore.destroy({ user: await props.account!.user!, device: props.device! });
        return new UserDisconnectResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;