import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { UserDestroyRequest, UserDestroyResponse, UsersEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import UsersDataStore from "../../datastores/UsersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<UsersEndpoint.Destroy> = class UserDestroyHandler extends BaseHandler<UsersEndpoint.Destroy> implements HandlerImpl<UsersEndpoint.Destroy> {
    private usersDataStore: UsersDataStore;

    constructor(endpoint: UsersEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.usersDataStore = new UsersDataStore({ localizer: this.localizer });
    }

    async run(req: UserDestroyRequest, props: GatewayProps): Promise<UserDestroyResponse> {
        await this.usersDataStore.destroy(req.id, { user: await props.account!.user! });
        return new UserDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;