import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { UserUpdateRequest, UserUpdateResponse, UsersEndpoint, HTTPStatusCode, LanguageCode, User } from "@guildion/core";
import UsersDataStore from "../../datastores/UsersDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<UsersEndpoint.Update> = class UserUpdateHandler extends BaseHandler<UsersEndpoint.Update> implements HandlerImpl<UsersEndpoint.Update> {
    private usersDataStore: UsersDataStore;

    constructor(endpoint: UsersEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.usersDataStore = new UsersDataStore({ localizer: this.localizer });
    }

    async run(req: UserUpdateRequest, props: GatewayProps): Promise<UserUpdateResponse> {
        const user = await this.usersDataStore.update(req.user, { user: await props.account!.user! });
        return new UserUpdateResponse({
            status: HTTPStatusCode.OK,
            user: await user.transform(),
        });
    }
}

export default handler;