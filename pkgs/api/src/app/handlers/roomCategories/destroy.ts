import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomCategoryDestroyRequest, RoomCategoryDestroyResponse, RoomCategoriesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import RoomCategoriesDataStore from '../../datastores/RoomCategoriesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomCategoriesEndpoint.Destroy> = class RoomCategoryDestroyHandler extends BaseHandler<RoomCategoriesEndpoint.Destroy> implements HandlerImpl<RoomCategoriesEndpoint.Destroy> {
    private roomCategoriesDataStore: RoomCategoriesDataStore;

    constructor(endpoint: RoomCategoriesEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomCategoriesDataStore = new RoomCategoriesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomCategoryDestroyRequest, props: GatewayProps): Promise<RoomCategoryDestroyResponse> {
        await this.roomCategoriesDataStore.destroy(req.id, { user: await props.account?.user! })
        return new RoomCategoryDestroyResponse({
            status: HTTPStatusCode.OK,
            success: true,
        });
    }
}

export default handler;