import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomCategoryUpdateRequest, RoomCategoryUpdateResponse, RoomCategoriesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import RoomCategoriesDataStore from '../../datastores/RoomCategoriesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomCategoriesEndpoint.Update> = class RoomCategoryUpdateHandler extends BaseHandler<RoomCategoriesEndpoint.Update> implements HandlerImpl<RoomCategoriesEndpoint.Update> {
    private roomCategoriesDataStore: RoomCategoriesDataStore;

    constructor(endpoint: RoomCategoriesEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomCategoriesDataStore = new RoomCategoriesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomCategoryUpdateRequest, props: GatewayProps): Promise<RoomCategoryUpdateResponse> {
        const roomCategory = await this.roomCategoriesDataStore.update(req.roomCategory, { user: await props.account?.user! });
        return new RoomCategoryUpdateResponse({
            status: HTTPStatusCode.OK,
            roomCategory: await roomCategory.transform(),
        });
    }
}

export default handler;