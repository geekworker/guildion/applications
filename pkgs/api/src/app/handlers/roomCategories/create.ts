import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomCategoryCreateRequest, RoomCategoryCreateResponse, RoomCategoriesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import RoomCategoriesDataStore from '../../datastores/RoomCategoriesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomCategoriesEndpoint.Create> = class RoomCategoryCreateHandler extends BaseHandler<RoomCategoriesEndpoint.Create> implements HandlerImpl<RoomCategoriesEndpoint.Create> {
    private roomCategoriesDataStore: RoomCategoriesDataStore;

    constructor(endpoint: RoomCategoriesEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomCategoriesDataStore = new RoomCategoriesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomCategoryCreateRequest, props: GatewayProps): Promise<RoomCategoryCreateResponse> {
        const roomCategory = await this.roomCategoriesDataStore.create(req.roomCategory, { user: await props.account?.user! })
        return new RoomCategoryCreateResponse({
            status: HTTPStatusCode.OK,
            roomCategory: await roomCategory.transform(),
        });
    }
}

export default handler;