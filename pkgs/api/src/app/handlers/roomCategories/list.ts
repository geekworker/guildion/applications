import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { RoomCategoriesListRequest, RoomCategoriesListResponse, RoomCategoriesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import RoomCategoriesDataStore from '../../datastores/RoomCategoriesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<RoomCategoriesEndpoint.List> = class RoomCategoriesListHandler extends BaseHandler<RoomCategoriesEndpoint.List> implements HandlerImpl<RoomCategoriesEndpoint.List> {
    private roomCategoriesDataStore: RoomCategoriesDataStore;

    constructor(endpoint: RoomCategoriesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomCategoriesDataStore = new RoomCategoriesDataStore({ localizer: this.localizer });
    }

    async run(req: RoomCategoriesListRequest, props: GatewayProps): Promise<RoomCategoriesListResponse> {
        const {
            categories,
            paginatable,
        } = req.isPublic ?
        await this.roomCategoriesDataStore.getsPublic(req.guildId, { count: req.count, offset: req.offset, user: await props.account?.user }) :
        await this.roomCategoriesDataStore.gets(req.guildId, { count: req.count, offset: req.offset, user: await props.account?.user });
        return new RoomCategoriesListResponse({
            status: HTTPStatusCode.OK,
            roomCategories: await Promise.all(
                categories.map(async category => category.transform({
                    rooms: await Promise.all(
                        category.cacheRooms.map(async room => room.transform({
                            activity: true,
                            activedBaseAt: new Date(),
                            currentMember: await room.currentMember?.transform({ roomMember: room.currentRoomMember }),
                        }))
                    )
                }))
            ),
            paginatable,
        });
    }
}

export default handler;