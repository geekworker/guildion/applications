import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoomLogsListRequest, RoomLogsListResponse, RoomLogsEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RoomLogsDataStore from "../../datastores/RoomLogsDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RoomLogsEndpoint.List> = class RoomLogsListHandler extends BaseHandler<RoomLogsEndpoint.List> implements HandlerImpl<RoomLogsEndpoint.List> {
    private roomLogsDataStore: RoomLogsDataStore;

    constructor(endpoint: RoomLogsEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.roomLogsDataStore = new RoomLogsDataStore({ localizer: this.localizer });
    }

    async run(req: RoomLogsListRequest, props: GatewayProps): Promise<RoomLogsListResponse> {
        const {
            logs,
            paginatable,
        } = await this.roomLogsDataStore.gets(req.roomId, { 
            count: req.count,
            offset: req.offset,
            user: await props.account?.user!,
            ltAt: req.getLtAt(),
            gtAt: req.getGtAt(),
        });
        
        return new RoomLogsListResponse({
            status: HTTPStatusCode.OK,
            logs: await Promise.all(
                logs.map(log => log.transform({
                    message: true,
                }))
            ),
            paginatable,
        });
    }
}

export default handler;