import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { Endpoint, Router, EndpointOfRequest, EndpointOfResponse, LanguageCode } from "@guildion/core";
import BaseHandler from "./BaseHandler";

export default interface HandlerImpl<T extends Endpoint<Router.Factory<any>, any, any>> {
    run: (req: EndpointOfRequest<T>, props: GatewayProps) => Promise<EndpointOfResponse<T>>,
}

export type Handler<T extends Endpoint<Router.Factory<any>, any, any>> = HandlerImpl<T> & BaseHandler<T>;
export type HandlerOfEndpoint<T extends Handler<any>> = T extends Handler<infer Endpoint> ? Endpoint : any;

export interface HandlerConstructor<T extends Endpoint<Router.Factory<any>, any, any>> {
    new(endpoint: T, { lang }: { lang: LanguageCode }): Handler<T>
}