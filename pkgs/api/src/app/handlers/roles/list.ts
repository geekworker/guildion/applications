import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RolesListRequest, RolesListResponse, RolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RolesDataStore from "../../datastores/RolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RolesEndpoint.List> = class RolesListHandler extends BaseHandler<RolesEndpoint.List> implements HandlerImpl<RolesEndpoint.List> {
    private rolesDataStore: RolesDataStore;

    constructor(endpoint: RolesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.rolesDataStore = new RolesDataStore({ localizer: this.localizer });
    }

    async run(req: RolesListRequest, props: GatewayProps): Promise<RolesListResponse> {
        const roles = await this.rolesDataStore.gets(req.guildId);
        
        return new RolesListResponse({
            status: HTTPStatusCode.OK,
            roles: await Promise.all(roles.map(role => role.transform())),
        });
    }
}

export default handler;