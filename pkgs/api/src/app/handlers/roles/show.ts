import { GatewayProps } from "@/app/modules/gateway/UniversalGateway";
import { RoleShowRequest, RoleShowResponse, RolesEndpoint, HTTPStatusCode, LanguageCode } from "@guildion/core";
import RolesDataStore from "../../datastores/RolesDataStore";
import BaseHandler from "../BaseHandler";
import HandlerImpl, { HandlerConstructor } from "../Handler";

const handler: HandlerConstructor<RolesEndpoint.Show> = class RoleShowHandler extends BaseHandler<RolesEndpoint.Show> implements HandlerImpl<RolesEndpoint.Show> {
    private rolesDataStore: RolesDataStore;

    constructor(endpoint: RolesEndpoint.Show, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.rolesDataStore = new RolesDataStore({ localizer: this.localizer });
    }

    async run(req: RoleShowRequest, props: GatewayProps): Promise<RoleShowResponse> {
        const role = await this.rolesDataStore.get(req.id);
        return new RoleShowResponse({
            status: HTTPStatusCode.OK,
            role: await role.transform(),
        });
    }
}

export default handler;