import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { MessageCreateRequest, MessageCreateResponse, MessagesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import MessagesDataStore from '../../datastores/MessagesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<MessagesEndpoint.Create> = class MessageCreateHandler extends BaseHandler<MessagesEndpoint.Create> implements HandlerImpl<MessagesEndpoint.Create> {
    private messagesDataStore: MessagesDataStore;

    constructor(endpoint: MessagesEndpoint.Create, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.messagesDataStore = new MessagesDataStore({ localizer: this.localizer });
    }

    async run(req: MessageCreateRequest, props: GatewayProps): Promise<MessageCreateResponse> {
        const message = await this.messagesDataStore.create(req.message, { user: await props.account?.user! })
        return new MessageCreateResponse({
            message: await message.transform({
                sender: true,
                files: true,
            }),
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;