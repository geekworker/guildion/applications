import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { MessageDestroyRequest, MessageDestroyResponse, MessagesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import MessagesDataStore from '../../datastores/MessagesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<MessagesEndpoint.Destroy> = class MessageDestroyHandler extends BaseHandler<MessagesEndpoint.Destroy> implements HandlerImpl<MessagesEndpoint.Destroy> {
    private messagesDataStore: MessagesDataStore;

    constructor(endpoint: MessagesEndpoint.Destroy, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.messagesDataStore = new MessagesDataStore({ localizer: this.localizer });
    }

    async run(req: MessageDestroyRequest, props: GatewayProps): Promise<MessageDestroyResponse> {
        await this.messagesDataStore.destroy(req.id, { user: await props.account?.user! })
        return new MessageDestroyResponse({
            success: true,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;