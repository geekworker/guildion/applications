import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { MessageUpdateRequest, MessageUpdateResponse, MessagesEndpoint, HTTPStatusCode, LanguageCode, Message } from '@guildion/core';
import MessagesDataStore from '../../datastores/MessagesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<MessagesEndpoint.Update> = class MessageUpdateHandler extends BaseHandler<MessagesEndpoint.Update> implements HandlerImpl<MessagesEndpoint.Update> {
    private messagesDataStore: MessagesDataStore;

    constructor(endpoint: MessagesEndpoint.Update, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.messagesDataStore = new MessagesDataStore({ localizer: this.localizer });
    }

    async run(req: MessageUpdateRequest, props: GatewayProps): Promise<MessageUpdateResponse> {
        const message = await this.messagesDataStore.update(req.message, {
            user: await props.account?.user!,
        });
        return new MessageUpdateResponse({
            message: await message.transform({
                files: true,
                sender: true,
            }),
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;