import { GatewayProps } from '@/app/modules/gateway/UniversalGateway';
import { MessagesListRequest, MessagesListResponse, MessagesEndpoint, HTTPStatusCode, LanguageCode } from '@guildion/core';
import MessagesDataStore from '../../datastores/MessagesDataStore';
import BaseHandler from '../BaseHandler';
import HandlerImpl, { HandlerConstructor } from '../Handler';

const handler: HandlerConstructor<MessagesEndpoint.List> = class MessagesListHandler extends BaseHandler<MessagesEndpoint.List> implements HandlerImpl<MessagesEndpoint.List> {
    private messagesDataStore: MessagesDataStore;

    constructor(endpoint: MessagesEndpoint.List, { lang }: { lang: LanguageCode }) {
        super(endpoint, { lang });
        this.messagesDataStore = new MessagesDataStore({ localizer: this.localizer });
    }

    async run(req: MessagesListRequest, props: GatewayProps): Promise<MessagesListResponse> {
        const {
            messages,
            paginatable,
        } = await this.messagesDataStore.gets(
            req.roomId,
            {
                user: await props.account?.user!,
                messageId: req.messageId,
                gtAt: req.getGtAt(),
                ltAt: req.getLtAt(),
                count: req.count,
                offset: req.offset,
            }
        )
        return new MessagesListResponse({
            messages: await Promise.all(
                messages.map(message => message.transform({
                    sender: true,
                    files: true,
                }))
            ),
            paginatable,
            status: HTTPStatusCode.OK,
        });
    }
}

export default handler;