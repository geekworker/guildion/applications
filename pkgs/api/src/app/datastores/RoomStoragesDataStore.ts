import { Localizer, File } from "@guildion/core";
import { FileEntity, FilingEntity, RoomEntity, RoomFileEntity, RoomStorageEntity } from '@guildion/db';
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomStoragesValidation from '@/app/validations/RoomStoragesValidation';
import type FilesDataStore from "./FilesDataStore";
import type GuildStoragesDataStore from "./GuildStoragesDataStore";

export default class RoomStoragesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RoomStoragesValidation;
    private $filesDataStore!: FilesDataStore;
    private $guildStoragesDataStore!: GuildStoragesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomStoragesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this.$filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$filesDataStore
    }

    get guildStoragesDataStore() {
        const Constructor = require('./GuildStoragesDataStore');
        this.$guildStoragesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$guildStoragesDataStore
    }

    async findOrCreate(room: RoomEntity): Promise<RoomStorageEntity> {
        await this.validation.checkRoomState(room);
        const guild = await room.guild;
        await this.validation.checkGuildState(guild);
        const exist = await RoomStorageEntity.findOne({
            where: {
                roomId: room.id,
            },
        });
        if (exist) {
            return exist;
        } else {
            const storage = await this.filesDataStore.findOrCreateStorage(File.generateRoomStorageKey(room.id));
            return await RoomStorageEntity.create({
                roomId: room.id,
                fileId: storage.id,
            }).save();
        }
    }

    async findOrCreateFile({ showInGuild, room, file }: { showInGuild: boolean, room: RoomEntity, file: FileEntity }): Promise<{
        filing: FilingEntity,
        roomFile: RoomFileEntity,
    }> {
        await this.validation.checkRoomState(room);
        const guild = await room.guild;
        await this.validation.checkGuildState(guild);
        const storage = await this.findOrCreate(room);
        const folder = await storage.file;
        const roomFile = await RoomFileEntity.findOne({
            where: {
                fileId: file.id,
                roomId: room.id,
            },
        }) || await RoomFileEntity.create({
            fileId: file.id,
            roomId: room.id,
        }).save();
        roomFile.showInGuild = showInGuild;
        await roomFile.save();
        const filing = await FilingEntity.findOne({
            where: {
                fileId: file.id,
                folderId: folder.id,
            },
        }) || await FilingEntity.create({
            fileId: file.id,
            folderId: folder.id,
            index: (await folder.folder)?.filesCount,
        }).save();
        await this.filesDataStore.refreshIndex(folder.id);
        return {
            roomFile,
            filing,
        }
    }
}

module.exports = RoomStoragesDataStore;