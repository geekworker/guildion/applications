import { GuildRoleTypes, Localizer, RoleType } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RolesValidation from '@/app/validations/RolesValidation';
import { RoleEntity, GuildEntity } from "@guildion/db";

export default class RolesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RolesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RolesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async gets(guildId: string): Promise<RoleEntity[]> {
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            },
        });
        await this.validation.checkGuildState(guild);
        const roles = await RoleEntity
            .getRepository()
            .createQueryBuilder('roles')
            .where("roles.guild_id = :guildId", { guildId: guild?.id })
            .orderBy('roles.created_at', 'DESC')
            .getMany();
        return roles;
    }

    async get(id: string): Promise<RoleEntity> {
        const role = await RoleEntity.findOne({
            where: {
                id,
            }
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: role?.guildId,
            }
        });
        await Promise.all([
            this.validation.checkRoleState(role),
            this.validation.checkGuildState(guild),
        ]);
        return role!;
    }

    async findOrCreateTypeRole(guild: GuildEntity, type: RoleType): Promise<RoleEntity> {
        const role = await RoleEntity.findOne({
            where: {
                guildId: guild.id,
                type,
            },
        }) || await RoleEntity.create({
            guildId: guild.id,
            type,
        }).save();
        return role;
    }

    async findOrCreateGuildRoles(guild: GuildEntity): Promise<RoleEntity[]> {
        return Promise.all(
            GuildRoleTypes.map(type => this.findOrCreateTypeRole(guild, type))
        );
    }
}

module.exports = RolesDataStore;