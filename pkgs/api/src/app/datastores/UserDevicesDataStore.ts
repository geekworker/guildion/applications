import { Localizer, UserDeviceSessionStatus, UserDeviceStatus } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type UserDevicesValidation from '@/app/validations/UserDevicesValidation';
import { DeviceEntity, UserDeviceEntity, UserEntity } from "@guildion/db";
import Bluebird from "bluebird";

export default class UserDevicesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: UserDevicesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/UserDevicesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async gets({ count, offset, user }: { count?: number, offset?: number, user: UserEntity }): Promise<{ devices:  DeviceEntity[], paginatable: boolean }> {
        await this.validation.checkUserState(user);
        const userDevices = await UserDeviceEntity.find({
            where: {
                userId: user.id,
            },
            take: count ? count + 1 : undefined,
            skip: offset,
            order: { createdAt:  'DESC' },
        });

        const last = userDevices.pop();

        return {
            devices: await Bluebird.all(userDevices.map(userDevice => DeviceEntity.findOneOrFail({ id: userDevice.deviceId }))),
            paginatable: !!last,
        }
    }

    async findOrCreate({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<UserDeviceEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        const exist = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        if (exist) {
            await this.validation.checkUserDeviceState(exist);
            return exist;
        } else {
            await this.validation.checkDeviceRegisterable(device);
            return await UserDeviceEntity.create({
                userId: user.id,
                deviceId: device.id,
                status: UserDeviceStatus.CREATED,
                sessionStatus: UserDeviceSessionStatus.LOGOUT,
            }).save();
        }
    }

    async trust(id: string, { user, device }: { user: UserEntity, device: DeviceEntity }): Promise<UserDeviceEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        const exist = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: id,
            },
        });
        await this.validation.checkUserDeviceState(exist);
        exist!.status = UserDeviceStatus.TRUSTED;
        return exist!.save();
    }

    async untrust(id: string, { user, device }: { user: UserEntity, device: DeviceEntity }): Promise<UserDeviceEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        const exist = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: id,
            },
        });
        await this.validation.checkUserDeviceState(exist);
        exist!.status = UserDeviceStatus.CREATED;
        return exist!.save();
    }

    async login({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<UserDeviceEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        const exist = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        await this.validation.checkUserDeviceState(exist);
        exist!.sessionStatus = UserDeviceSessionStatus.LOGIN;
        return exist!.save();
    }

    async logout({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<UserDeviceEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        const exist = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        exist!.sessionStatus = UserDeviceSessionStatus.LOGOUT;
        return exist!.save();
    }

    async forceLogoutDevice({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<UserDeviceEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        const exist = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        await this.validation.checkUserDeviceState(exist);
        if (exist?.sessionStatus == UserDeviceSessionStatus.LOGOUT) return exist!
        exist!.sessionStatus = UserDeviceSessionStatus.SHOULD_LOGOUT;
        return exist!.save();
    }
}

module.exports = UserDevicesDataStore;