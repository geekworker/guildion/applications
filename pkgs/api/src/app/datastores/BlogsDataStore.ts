import { LanguageCode, Localizer, BlogStatus, IS_PROD, SortDirection, SortType, BlogCategoryStatus } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type BlogsValidation from '@/app/validations/BlogsValidation';
import { BlogCategoryEntity, BlogEntity } from '@guildion/db';

export default class BlogsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: BlogsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/BlogsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async getPaths(
    ): Promise<{
        paths: { params: { slug: string }, locale: LanguageCode }[],
    }> {
        const blogs = await BlogEntity
            .getRepository()
            .createQueryBuilder('blogs')
            .where('blogs.status = :status', { status: BlogStatus.PUBLISH })
            .getMany();

        return {
            paths: [
                ...blogs.filter(d => !!d.jaTitle && !!d.jaData).map(d => {
                    return {
                        params: {
                            slug: d.slug,
                        },
                        locale: LanguageCode.Japanese,
                    }
                }),
                ...blogs.filter(d => !!d.enTitle && !!d.enData).map(d => {
                    return {
                        params: {
                            slug: d.slug,
                        },
                        locale: LanguageCode.English,
                    }
                })
            ],
        };
    }

    async getDraftPaths(
    ): Promise<{
        paths: { params: { slug: string }, locale: LanguageCode }[],
    }> {
        if (IS_PROD()) return { paths: [] };
        const blogs = await BlogEntity
            .getRepository()
            .createQueryBuilder('blogs')
            .where('blogs.status = :status', { status: BlogStatus.DRAFT })
            .getMany();

        return {
            paths: [
                ...blogs.filter(d => !!d.jaTitle && !!d.jaData).map(d => {
                    return {
                        params: {
                            slug: d.slug,
                        },
                        locale: LanguageCode.Japanese,
                    }
                }),
                ...blogs.filter(d => !!d.enTitle && !!d.enData).map(d => {
                    return {
                        params: {
                            slug: d.slug,
                        },
                        locale: LanguageCode.English,
                    }
                })
            ],
        };
    }

    async get(
        slug: string,
    ): Promise<{
        blog: BlogEntity,
    }> {
        const blog = await BlogEntity.findOneOrFail({
            where: {
                slug,
                status: BlogStatus.PUBLISH,
            },
        });
        return {
            blog,
        };
    }

    async draft(
        slug: string,
    ): Promise<{
        blog: BlogEntity,
    }> {
        const blog = await BlogEntity.findOneOrFail({
            where: {
                slug,
                status: BlogStatus.DRAFT,
            },
        });
        return {
            blog,
        };
    }

    async gets({ count, offset, categorySlug, sort }: { count?: number, offset?: number, categorySlug?: string, sort?: { direction?: SortDirection, type?: SortType } }): Promise<{ blogs: BlogEntity[], category?: BlogCategoryEntity, paginatable: boolean }> {

        const query = BlogEntity
            .getRepository()
            .createQueryBuilder('blogs')
            .where('blogs.status = :status', { status: BlogStatus.PUBLISH });

        const category = categorySlug ? await BlogCategoryEntity.findOne({
            where: {
                id: categorySlug,
                status: BlogCategoryStatus.PUBLISH,
            },
        }) : undefined;

        if (category) {
            query.andWhere('blogs.category_id = :categoryId', { categoryId: category.id });
        }

        if (sort && sort.direction) {
            query.orderBy('blogs.created_at', sort.direction);
        } else {
            query.orderBy('blogs.created_at', 'DESC');
        }

        const blogs = await query
            .limit(count ? count + 1 : count)
            .offset(offset)
            .getMany();
    
        const last = !!count && blogs.length >= count && blogs.pop();

        return {
            blogs,
            paginatable: !!last,
            category,
        }
    }
}

module.exports = BlogsDataStore;