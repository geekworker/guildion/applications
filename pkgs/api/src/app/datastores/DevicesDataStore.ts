import { AppError, DeviceAttributes, DeviceStatus, Localizer } from "@guildion/core";
import { DeviceEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type DevicesValidation from '@/app/validations/DevicesValidation';

export default class DevicesDataStore extends BaseDataStore implements DataStoreImpl {
    private Validation!: DevicesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/DevicesValidation')
        this.Validation ||= new Constructor({ localizer: this.localizer });
        return this.Validation
    }

    async register({ device }: { device: DeviceAttributes }): Promise<DeviceEntity> {
        if (!device.udid || device.udid.length == 0) throw new AppError(this.localizer.dictionary.error.common.requireDevice);

        const exist = await DeviceEntity.findOne({
            where: {
                udid: device.udid,
            },
        });

        if (exist) {
            // exist.udid = device.udid;
            // exist.os = device.os;
            // exist.model = device.model;
            exist.languageCode = device.languageCode;
            exist.countryCode = device.countryCode;
            exist.timezone = device.timezone;
            exist.onesignalNotificationId = device.onesignalNotificationId;
            exist.notificationToken = device.notificationToken;
            exist.appVersion = device.appVersion;
            const entity = await exist.save();
            return entity;
        } else {
            const entity = await DeviceEntity.create({
                udid: device.udid,
                os: device.os,
                model: device.model,
                languageCode: device.languageCode,
                countryCode: device.countryCode,
                timezone: device.timezone,
                onesignalNotificationId: device.onesignalNotificationId,
                notificationToken: device.notificationToken,
                appVersion: device.appVersion,
                status: DeviceStatus.CREATED,
            }).save();
            return entity;
        }
    }
}

module.exports = DevicesDataStore;