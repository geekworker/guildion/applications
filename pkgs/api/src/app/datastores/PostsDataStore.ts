import { Localizer, PostAttributes, PostStatus, PostType, RoomStatus, RoomType } from '@guildion/core';
import { FileEntity, MemberEntity, PostEntity, RoomEntity, RoomMemberEntity, UserEntity } from '@guildion/db';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type PostsValidation from '@/app/validations/PostsValidation';
import type PostReadsDataStore from '@/app/datastores/PostReadsDataStore';
import type PostAttachmentsDataStore from '@/app/datastores/PostAttachmentsDataStore';
import type RoomLogsDataStore from '@/app/datastores/RoomLogsDataStore';
import Bluebird from 'bluebird';

export default class PostsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: PostsValidation;
    private _postReadsDataStore!: PostReadsDataStore;
    private _postAttachmentsDataStore!: PostAttachmentsDataStore;
    private _roomLogsDataStore!: RoomLogsDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/PostsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    get postReadsDataStore() {
        const Constructor = require('@/app/datastores/PostReadsDataStore');
        this._postReadsDataStore ||= new Constructor({ localizer: this.localizer });
        return this._postReadsDataStore
    }

    get postAttachmentsDataStore() {
        const Constructor = require('@/app/datastores/PostAttachmentsDataStore');
        this._postAttachmentsDataStore ||= new Constructor({ localizer: this.localizer });
        return this._postAttachmentsDataStore
    }

    get roomLogsDataStore() {
        const Constructor = require('@/app/datastores/RoomLogsDataStore');
        this._roomLogsDataStore ||= new Constructor({ localizer: this.localizer });
        return this._roomLogsDataStore
    }

    async gets(
        roomId: string,
        { user, count, offset, postId }: { user?: UserEntity, count?: number, offset?: number, postId?: string }
    ): Promise<{
        posts: PostEntity[],
        room: RoomEntity,
        member: MemberEntity,
        paginatable: boolean,
    }> {
        await this.validation.checkUserState(user);
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        });
        await this.validation.checkRoomState(room);

        const post = postId ? await PostEntity.findOne({
            where: {
                id: postId,
                roomId: roomId,
            },
        }) : undefined;

        const member = await MemberEntity.findOne({
            where: {
                userId: user!.id,
                guildId: room?.guildId,
            },
        });

        await Promise.all([
            postId && await this.validation.checkPostState(post),
            this.validation.checkRoomState(room),
            await this.validation.checkMemberState(member),
            await this.validation.checkMemberRoomViewable({ member: member!, room: room! }),
        ]);

        const posts = await PostEntity
            .getRepository()
            .createQueryBuilder('posts')
            .where(
                postId ?
                    'posts.room_id = :roomId AND posts.parentId = :postId' :
                    'posts.room_id = :roomId AND posts.parentId IS NULL'
                ,{
                    roomId: room?.id,
                    postId,
                }
            )
            .andWhere(
                'posts.status IN (:...statuses)',
                { statuses: [PostStatus.CREATED] }
            )
            .andWhere(
                'posts.type IN (:...types)',
                { types: [PostType.PUBLIC, PostType.MEMBERS, PostType.GUESTS] }
            )
            .limit(count ? count + 1 : undefined)
            .offset(offset)
            .orderBy('posts.created_at', 'DESC')
            .getMany();

        const last = !!count && posts.length >= count && posts.pop();

        return {
            room: room!,
            member: member!,
            posts: posts,
            paginatable: !!last,
        };
    }

    async getsPublic(
        roomId: string,
        { user, count, offset, postId }: { user?: UserEntity, count?: number, offset?: number, postId?: string }
    ): Promise<{
        posts: PostEntity[],
        room: RoomEntity,
        member?: MemberEntity,
        paginatable: boolean,
    }> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        });
        await this.validation.checkRoomState(room);

        const post = postId ? await PostEntity.findOne({
            where: {
                id: postId,
                roomId: roomId,
            },
        }) : undefined;

        const member = user ? await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        }) : undefined;

        await Promise.all([
            postId && await this.validation.checkPostState(post),
            postId && !user && await this.validation.checkPostPublic(post),
            this.validation.checkRoomState(room),
            user && await this.validation.checkMemberState(member),
            user && await this.validation.checkMemberRoomViewable({ member: member!, room: room! }),
        ]);

        const posts = await PostEntity
            .getRepository()
            .createQueryBuilder('posts')
            .where(
                postId ?
                    'posts.room_id = :roomId AND posts.parentId = :postId' :
                    'posts.room_id = :roomId AND posts.parentId IS NULL'
                ,{
                    roomId: room?.id,
                    postId,
                }
            )
            .andWhere(
                'posts.status IN (:...statuses)',
                { statuses: [PostStatus.CREATED] }
            )
            .andWhere(
                'posts.type IN (:...types)',
                { types: [PostType.PUBLIC] }
            )
            .limit(count ? count + 1 : undefined)
            .offset(offset)
            .orderBy('posts.created_at', 'DESC')
            .getMany();

        const last = !!count && posts.length >= count && posts.pop();

        return {
            room: room!,
            member: member,
            posts: posts,
            paginatable: !!last,
        };
    }

    async get(
        id: string,
        { user }: { user?: UserEntity }
    ): Promise<{
        post: PostEntity,
        member?: MemberEntity,
    }> {
        const post = await PostEntity.findOne({
            where: {
                id,
            },
        });

        const room = await RoomEntity.findOne({
            where: {
                id: post?.roomId,
            },
        });
        await this.validation.checkRoomState(room);

        const member = user ? await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        }) : undefined;

        await Promise.all([
            this.validation.checkPostState(post),
            this.validation.checkRoomState(room),
            !user && await this.validation.checkPostPublic(post),
            user && await this.validation.checkMemberState(member),
            user && await this.validation.checkMemberRoomViewable({ member: member!, room: room! }),
        ]);

        return {
            post: post!,
            member,
        };
    }

    async create(attributes: PostAttributes, { user }: { user: UserEntity }) {
        const room = await RoomEntity.findOne({
            where: {
                id: attributes?.roomId,
            },
        });
        const guild = await room?.guild;
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: room?.id,
                memberId: member?.id,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkRoomMemberState(roomMember),
            this.validation.checkMemberRoomViewable({ room: room!, member: member! }),
            this.validation.checkPostBodyWithAttachments(attributes.body, attributes.files),
            this.validation.checkPostExist(attributes.id),
        ]);

        const post = await PostEntity.create({
            id: attributes.id ? attributes.id : undefined,
            senderId: member!.id,
            roomId: attributes.roomId!,
            body: attributes.body,
            status: PostStatus.CREATED,
        }).save();

        if (attributes.files && attributes.files.length > 0) {
            const files: (FileEntity | undefined)[] = await Promise.all(
                (attributes.files || []).map(
                    file => FileEntity.findOne({
                        where: {
                            id: file.id,
                        },
                    })
                )
            );
            await this.postAttachmentsDataStore.findOrCreates({
                post: post!,
                files: files.filter((val): val is FileEntity => !!val)
            });
        }

        this.roomLogsDataStore.createPostCreateLog(
            post
        ).catch(e => { console.log(e) });

        this.postReadsDataStore.findOrCreates(
            post
        ).catch(e => { console.log(e) });

        const parent = post.parentId ? await PostEntity.findOne({
            where: {
                id: post.parentId,
            },
        }) : undefined;

        if (parent) {
            await Promise.all([
                this.validation.checkPostState(post),
                this.validation.checkParentPostIsSameRoom({ post, parent }),
            ]);
            post.location = `${parent.location}/${post.id}`;
            return post.save();
        } else {
            post.location = `${post.id}`;
            return post.save();
        }
    }

    async update(attributes: Partial<PostAttributes> & { id: string }, { user }: { user: UserEntity }): Promise<PostEntity> {
        const post = await PostEntity.findOne({
            where: {
                id: attributes.id,
            },
        });
        const room = await RoomEntity.findOne({
            where: {
                id: post?.roomId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkMemberState(member),
            this.validation.checkPostState(post),
        ]);
        if (post && member) {
            await this.validation.checkPostPermission(post, member);
            if (attributes.body !== undefined) post!.body = attributes.body;
            if (attributes.type !== undefined) post!.type = attributes.type;
            if (attributes.files !== undefined) {
                const files: (FileEntity | undefined)[] = await Promise.all(
                    (attributes.files || []).map(
                        file => FileEntity.findOne({
                            where: {
                                id: file.id,
                            },
                        })
                    )
                );
                await this.postAttachmentsDataStore.findOrUpdates({
                    post: post!,
                    files: files.filter((val): val is FileEntity => !!val)
                });
            }
            await post.save();
        }
        return post!;
    }

    async destroy(id: string, { user }: { user: UserEntity }): Promise<void> {
        const post = await PostEntity.findOne({
            where: {
                id,
            },
        });
        const room = await RoomEntity.findOne({
            where: {
                id: post?.roomId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkMemberState(member),
            this.validation.checkPostState(post),
        ]);
        if (post && member) {
            await this.validation.checkPostPermission(post, member);
            post.status = PostStatus.DELETED;
            await post.save();
        }
    }

    async search(query: string, { count, offset, roomId, user }: { count?: number, offset?: number, roomId?: string, user?: UserEntity }): Promise<{ posts: PostEntity[], paginatable: boolean }> {
        const room = roomId ? await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        }) : undefined;

        const member = user && roomId ? await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        }) : undefined;

        await Promise.all([
            roomId && this.validation.checkRoomState(room),
            user && roomId && await this.validation.checkMemberState(member),
            user && roomId && await this.validation.checkMemberRoomViewable({ member: member!, room: room! }),
        ]);

        const posts = await PostEntity
            .getRepository()
            .createQueryBuilder('posts')
            .innerJoinAndSelect('posts.room', 'room', "room.status NOT IN (:...roomStatuses) AND room.type IN (:...roomTypes)", { roomStatuses: [RoomStatus.BANNED, RoomStatus.LOCKED, RoomStatus.DELETED, RoomStatus.DELETING], roomTypes: user ? [RoomType.PUBLIC, RoomType.PRIVATE, RoomType.OFFICIAL] : [RoomType.PUBLIC, RoomType.OFFICIAL] })
            .where(roomId ? "posts.room_id = :roomId" : "posts.status NOT IN (:...statuses)", { roomId, statuses: [PostStatus.DELETED, PostStatus.ARCHIVED] })
            .andWhere("posts.type IN (:...types)", { types: user ? [PostType.PUBLIC, PostType.MEMBERS] : [PostType.PUBLIC] })
            .andWhere(`
                posts.body LIKE :query OR
                room.display_name LIKE :query OR
                room.description LIKE :query
            `, { query:`%${query}%` })
            .limit(count ? count + 1 : count)
            .offset(offset)
            .getMany();
            
        const last = !!count && posts.length >= count && posts.pop();

        if (user) {
            await Bluebird.all(posts.map(async (post) => {
                const room = await RoomEntity.findOne({
                    where: {
                        id: post.roomId,
                    },
                })
                const currentMember = await MemberEntity.findOne({
                    where: {
                        userId: user.id,
                        guildId: room?.guildId,
                    },
                });
                const currentRoomMember = await RoomMemberEntity.findOne({
                    where: {
                        memberId: currentMember?.id,
                        roomId: room?.id,
                    },
                });
                post.currentMember = currentMember;
                post.currentRoomMember = currentRoomMember;
                return post;
            }))
        }

        return {
            posts,
            paginatable: !!last
        }
    }
}

module.exports = PostsDataStore;