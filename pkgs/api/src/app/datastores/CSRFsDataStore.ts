import { AppError, Localizer } from "@guildion/core";
import { Hasher } from "@guildion/node";
import { CSRFEntity, DeviceEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type CSRFsValidation from '@/app/validations/CSRFsValidation';
import { ulid } from "ulid";

export default class CSRFsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: CSRFsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const constructor = require('@/app/validations/CSRFsValidation')
        this.$validation ||= new constructor({ localizer: this.localizer });
        return this.$validation
    }

    async create({ device }: { device: DeviceEntity }): Promise<{ csrf: CSRFEntity, csrfSecret: string }> {
        this.validation.beforeCreate({ device });
        const csrfSecret = ulid();
        const {
            iv,
            salt,
            hash,
        } = Hasher.enhashPrivateKey(csrfSecret);
        await this.inactive({ device });
        const csrf = await CSRFEntity.create({
            deviceId: device.id!,
            hash,
            salt,
            iv,
            isActive: true,
        }).save();
        return { csrf, csrfSecret };
    }

    async createAndSaveSecret({ device }: { device: DeviceEntity }): Promise<{ csrf: CSRFEntity, csrfSecret: string }> {
        const { csrf, csrfSecret } = await this.create({ device });
        csrf.secret = csrfSecret;
        await csrf.save();
        return { csrf, csrfSecret };
    }

    async compare({ device, csrfSecret }: { device: DeviceEntity, csrfSecret: string }): Promise<boolean> {
        this.validation.beforeCompare({ device, csrfSecret });
        const csrfs = await CSRFEntity.find({
            where: {
                deviceId: device.id!,
                isActive: true,
            },
            take: 1,
            order: { createdAt: 'DESC' },
        });
        if (csrfs.length == 0) throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
        const csrf = csrfs[0];
        if (Hasher.compare(csrf.salt, csrf.iv, csrfSecret, csrf.hash)) {
            return true;
        } else {
            throw new AppError(this.localizer.dictionary.error.csrf.invalidToken);
        }
    }

    async inactive({ device }: { device: DeviceEntity }): Promise<boolean> {
        this.validation.beforeInactive({ device });
        await CSRFEntity
            .getRepository()
            .update({
                deviceId: device.id!
            }, {
                isActive: false,
            });
        return true;
    }
}

module.exports = CSRFsDataStore;