import BaseDataStore from "./BaseDataStore";

export default interface DataStoreImpl {
}

export type DataStore = DataStoreImpl & BaseDataStore;