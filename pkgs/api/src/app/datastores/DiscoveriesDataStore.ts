import { DiscoveryStatus, LanguageCode, Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type DiscoveriesValidation from '@/app/validations/DiscoveriesValidation';
import { DeviceEntity, DiscoveryEntity } from '@guildion/db';

export default class DiscoveriesDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: DiscoveriesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/DiscoveriesValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async recommend({ device, count, offset }: { device: DeviceEntity, count?: number, offset?: number }): Promise<{ discoveries: DiscoveryEntity[], paginatable: boolean }> {
        const results = await DiscoveryEntity
            .getRepository()
            .createQueryBuilder('discoveries')
            .where(device.languageCode  === LanguageCode.Japanese ?
                'discoveries.ja_title IS NOT NULL AND discoveries.ja_description IS NOT NULL AND discoveries.ja_data IS NOT NULL' : 
                'discoveries.en_title IS NOT NULL AND discoveries.en_description IS NOT NULL AND discoveries.en_data IS NOT NULL'
            )
            .andWhere('discoveries.status = :status', { status: DiscoveryStatus.PUBLISH })
            .limit(count ? count + 1 : undefined)
            .offset(offset)
            .getMany();

        const last = !!count && results.length >= count && results.pop();

        return {
            discoveries: results,
            paginatable: !!last,
        };
    }
}

module.exports = DiscoveriesDataStore;