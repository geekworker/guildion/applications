import { RoomMemberConnectionEntity, ConnectionEntity, UserEntity, RoomEntity, GuildEntity, MemberEntity, RoomMemberEntity, DeviceEntity, DeviceConnectionEntity, connection } from "@guildion/db";
import { Localizer, RoomMemberConnectionStatus } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomMemberConnectionsValidation from '@/app/validations/RoomMemberConnectionsValidation';
import Bluebird from "bluebird";
import { In } from "typeorm";

export default class RoomMemberConnectionsDataStore extends BaseDataStore implements DataStoreImpl {
    private Validation!: RoomMemberConnectionsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomMemberConnectionsValidation');
        this.Validation ||= new Constructor({ localizer: this.localizer });
        return this.Validation
    }

    async create(id: string,{ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<{
        roomMemberConnection: RoomMemberConnectionEntity,
        connection: ConnectionEntity,
    }> {
        const deviceConnections = await DeviceConnectionEntity
            .getRepository()
            .createQueryBuilder('device_connections')
            .innerJoin('device_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .where('device_connections.device_id = :deviceId', { deviceId: device.id })
            .orderBy('device_connections.id', 'DESC')
            .getMany();
        const deviceConnection = deviceConnections.shift();
        const connection = await deviceConnection?.connection;
        const room = await RoomEntity.findOne({
            where: {
                id,
            },
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: id,
                memberId: member?.id,
            },
        });
        await this.validation.checkConnectionState(connection);
        await this.validation.checkUserState(user);
        await this.validation.checkGuildState(guild);
        await this.validation.checkRoomState(room);
        await this.validation.checkMemberState(member);
        await this.validation.checkRoomMemberState(roomMember);

        const exist = await RoomMemberConnectionEntity.findOne({
            where: {
                connectionId: connection!.id,
                roomMemberId: roomMember!.id,
                isConnecting: true
            },
        });

        if (exist) {
            return {
                connection: connection!,
                roomMemberConnection: exist,
            };
        } else {
            await this.destroyAll({ user, device });
            const roomMemberConnection = await RoomMemberConnectionEntity.create({
                connectionId: connection!.id,
                roomMemberId: roomMember!.id,
                isConnecting: true,
                connectedAt: new Date(),
                status: RoomMemberConnectionStatus.CONNECTING,
            }).save();
            await room?.activate();
            return {
                connection: connection!,
                roomMemberConnection,
            };
        }
    }

    async destroyAll({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<boolean> {
        const deviceConnections = await DeviceConnectionEntity
            .getRepository()
            .createQueryBuilder('device_connections')
            .innerJoin('device_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .where('device_connections.device_id = :deviceId', { deviceId: device.id })
            .orderBy('device_connections.id', 'DESC')
            .getMany();
        const connections = await Bluebird.all(
            deviceConnections.map(dc => dc.connection)
        );
        await this.validation.checkUserState(user);
        const exists = connections.length > 0 ? await RoomMemberConnectionEntity
            .getRepository()
            .createQueryBuilder('room_member_connections')
            .where('room_member_connections.connection_id IN (:...ids)', { ids: connections.map(c => c.id) })
            .andWhere('room_member_connections.is_connecting = :isConnecting', { isConnecting: true })
            .getMany() : [];
        await Promise.all(
            exists.map(c => c.disconnect())
        );
        return true;
    }

    async gets(id: string, { count, offset, user }: { count?: number, offset?: number, user: UserEntity }): Promise<{ guild:  GuildEntity, room: RoomEntity, roomMemberConnections: RoomMemberConnectionEntity[], paginatable: boolean }> {
        const room = await RoomEntity.findOne({
            where: {
                id,
            },
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: id,
                memberId: member?.id,
            },
        });
        await this.validation.checkUserState(user);
        await this.validation.checkGuildState(guild);
        await this.validation.checkRoomState(room);
        await this.validation.checkMemberState(member);
        await this.validation.checkRoomMemberState(roomMember);

        const roomMemberConnections = await RoomMemberConnectionEntity
            .getRepository()
            .createQueryBuilder('room_member_connections')
            .innerJoin('room_member_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .innerJoin('room_member_connections.roomMember', 'room_member', `room_member.room_id = :roomId`, { roomId: id })
            .distinctOn(['room_member_connections.id'])
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .groupBy('member_connections.id')
            .orderBy('member_connections.id', 'ASC')
            .getMany()
        
        const last = !!count && roomMemberConnections.length >= count && roomMemberConnections.pop();

        return {
            roomMemberConnections,
            guild: guild!,
            room: room!,
            paginatable: !!last,
        }
    }
}

module.exports = RoomMemberConnectionsDataStore;