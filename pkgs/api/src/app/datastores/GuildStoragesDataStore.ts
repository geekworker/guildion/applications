import { File, Localizer } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type GuildStoragesValidation from '@/app/validations/GuildStoragesValidation';
import type FilesDataStore from "./FilesDataStore";
import { FileEntity, GuildEntity, GuildStorageEntity, GuildFileEntity, FilingEntity } from "@guildion/db";

export default class GuildStoragesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: GuildStoragesValidation;
    private _filesDataStore!: FilesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/GuildStoragesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this._filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this._filesDataStore
    }

    async findOrCreate(guild: GuildEntity): Promise<GuildStorageEntity> {
        await this.validation.checkGuildState(guild);
        const exist = await GuildStorageEntity.findOne({
            where: {
                guildId: guild.id,
            },
        });
        if (exist) {
            return exist;
        } else {
            const storage = await this.filesDataStore.findOrCreateStorage(File.generateGuildStorageKey(guild.id));
            return await GuildStorageEntity.create({
                guildId: guild.id,
                fileId: storage.id,
            }).save();
        }
    }

    async findOrCreateFile(guild: GuildEntity, file: FileEntity): Promise<FilingEntity> {
        await this.validation.checkGuildState(guild);
        const storage = await this.findOrCreate(guild);
        const folder = await storage.file;
        await GuildFileEntity.findOne({
            where: {
                fileId: file.id,
                guildId: guild.id,
            },
        }) || await GuildFileEntity.create({
            fileId: file.id,
            guildId: guild.id,
        }).save();
        const filing = await FilingEntity.findOne({
            where: {
                fileId: file.id,
                folderId: folder.id,
            },
        }) || FilingEntity.create({
            fileId: file.id,
            folderId: folder.id,
            index: (await folder.folder)?.filesCount,
        }).save();
        await this.filesDataStore.refreshIndex(folder.id);
        return filing;
    }
}

module.exports = GuildStoragesDataStore;