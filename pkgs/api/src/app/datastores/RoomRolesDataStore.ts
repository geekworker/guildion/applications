import { Localizer, RoleAttributes  } from "@guildion/core";
import { RoomEntity, RoomRoleEntity, RoleEntity, UserEntity, MemberEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomRolesValidation from '@/app/validations/RoomRolesValidation';
import type RolesDataStore from "./RolesDataStore";
import Bluebird from "bluebird";

export default class RoomRolesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RoomRolesValidation;
    private $rolesDataStore!: RolesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomRolesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get rolesDataStore() {
        const Constructor = require('./RolesDataStore');
        this.$rolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$rolesDataStore
    }

    async gets(roomId: string): Promise<RoomRoleEntity[]> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        await this.validation.checkRoomState(room);

        const roomRoles = await RoomRoleEntity
            .getRepository()
            .createQueryBuilder('room_roles')
            .innerJoinAndSelect('room_roles.role', 'role')
            .where('room_roles.room_id = :roomId', { roomId })
            .orderBy('role.priority', 'DESC')
            .getMany();

        return roomRoles;
    }

    async get({ roleId, roomId }: { roleId: string, roomId: string }): Promise<RoomRoleEntity> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const role = await RoleEntity.findOne({
            where: {
                id: roleId,
            }
        });
        const roomRole = await RoomRoleEntity.findOne({
            where: {
                id: roleId,
            }
        });
        await this.validation.checkRoomState(room);
        await this.validation.checkRoleState(role);
        await this.validation.checkRoomRoleExist(roomRole);

        return roomRole!;
    }

    async findOrCreate({ roomId, roleId }: { roomId: string, roleId: string }): Promise<RoomRoleEntity> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const role = await RoleEntity.findOne({
            where: {
                id: roleId,
                guildId: room?.guildId,
            }
        });
        await this.validation.checkRoomState(room);
        await this.validation.checkRoleState(role);
        const guild = await room!.guild;
        await this.validation.checkGuildState(guild);

        const exist = await RoomRoleEntity.findOne({
            where: {
                roomId: room?.id,
                roleId: role?.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            const roomRole = await RoomRoleEntity.create({
                roomId: room!.id,
                roleId: role!.id,
            }).save();
            return roomRole;
        }
    }

    async findOrCreates({ room, roles }: { room: RoomEntity, roles: RoleEntity[] }): Promise<RoomRoleEntity[]> {
        return await Bluebird.map(
            roles,
            (role) => this.findOrCreate({ roleId: role.id, roomId: room.id }),
            { concurrency: 10 },
        );
    }

    async create({ roomId, roleId, user }: { roomId: string, roleId: string, user: UserEntity }): Promise<RoomRoleEntity> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const role = await RoleEntity.findOne({
            where: {
                id: roleId,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            }
        });
        const guild = await room!.guild;

        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkRoleState(role),
            this.validation.checkGuildState(guild),
            this.validation.checkUserState(user),
            this.validation.checkMemberState(member),
            this.validation.checkMemberRoomManagable({ member: member!, room: room! }),
        ]);

        const exist = await RoomRoleEntity.findOne({
            where: {
                roomId: room?.id,
                roleId: role?.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            const roomRole = await RoomRoleEntity.create({
                roomId: room!.id,
                roleId: role!.id,
            }).save();
            return roomRole;
        }
    }

    async update(attributes: Partial<RoleAttributes> & { id: string, roomId: string }, { user }: { user: UserEntity }) {
        const room = await RoomEntity.findOne({
            where: {
                id: attributes.roomId,
            }
        });
        const role = await RoleEntity.findOne({
            where: {
                id: attributes.id,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            }
        });
        const exist = await RoomRoleEntity.findOne({
            where: {
                roomId: room?.id,
                roleId: role?.id,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkRoleState(role),
            this.validation.checkUserState(user),
            this.validation.checkMemberState(member),
            this.validation.checkMemberRoomManagable({ member: member!, room: room! }),
            this.validation.checkRoomRoleExist(exist),
        ]);
        return exist!.save();
    }

    async destroy({ roomId, roleId, user }: { roomId: string, roleId: string, user: UserEntity }): Promise<void> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const role = await RoleEntity.findOne({
            where: {
                id: roleId,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            }
        });
        const exist = await RoomRoleEntity.findOne({
            where: {
                roomId: room?.id,
                roleId: role?.id,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkRoleState(role),
            this.validation.checkUserState(user),
            this.validation.checkMemberState(member),
            this.validation.checkMemberRoomManagable({ member: member!, room: room! }),
            this.validation.checkRoomRoleExist(exist),
        ]);

        await RoomRoleEntity.delete(exist!.id);
    }
}

module.exports = RoomRolesDataStore;