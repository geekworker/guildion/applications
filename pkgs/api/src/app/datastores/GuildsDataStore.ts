import { MemberEntity, UserEntity, GuildEntity, FileEntity } from "@guildion/db"
import { Guild, GuildAttributes, GuildStatus, GuildType, Localizer, MemberStatus } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type GuildsValidation from '@/app/validations/GuildsValidation';
import type MembersDataStore from "./MembersDataStore";
import type RolesDataStore from "./RolesDataStore";
import type MemberRolesDataStore from "./MemberRolesDataStore";
import type RoomCategoriesDataStore from "./RoomCategoriesDataStore";
import type GuildStoragesDataStore from "./GuildStoragesDataStore";
import type FilesDataStore from "./FilesDataStore";
import Bluebird from "bluebird";
import { ulid } from "ulid";
import { seedFiles } from "@guildion/db/src/seeds/files";
import S3Adaptor from "@/app/modules/aws/s3";

export default class GuildsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: GuildsValidation;
    private $membersDataStore!: MembersDataStore;
    private $memberRolesDataStore!: MemberRolesDataStore;
    private $rolesDataStore!: RolesDataStore;
    private $roomCategoriesDataStore!: RoomCategoriesDataStore;
    private $guildStoragesDataStore!: GuildStoragesDataStore;
    private $filesDataStore!: FilesDataStore;
    private $s3Adaptor!: S3Adaptor;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/GuildsValidation')
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get membersDataStore() {
        const Constructor = require('./MembersDataStore')
        this.$membersDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$membersDataStore
    }

    get memberRolesDataStore() {
        const Constructor = require('./MemberRolesDataStore');
        this.$memberRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$memberRolesDataStore
    }

    get rolesDataStore() {
        const Constructor = require('./RolesDataStore');
        this.$rolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$rolesDataStore
    }

    get roomCategoriesDataStore() {
        const Constructor = require('./RoomCategoriesDataStore');
        this.$roomCategoriesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$roomCategoriesDataStore
    }

    get guildStoragesDataStore() {
        const Constructor = require('./GuildStoragesDataStore');
        this.$guildStoragesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$guildStoragesDataStore
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this.$filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$filesDataStore
    }

    get s3Adaptor() {
        this.$s3Adaptor ||= new S3Adaptor({ localizer: this.localizer });
        return this.$s3Adaptor
    }

    async getPublic(id: string, { user }: { user?: UserEntity }): Promise<{ guild: GuildEntity }> {
        const guild = await GuildEntity.findOne({
            where: {
                id: id,
            },
        });
        await this.validation.checkGuildState(guild);
        const member = user ? await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            },
        }) : undefined;
        guild!.currentMember = member;

        return {
            guild: guild!,
        };
    }

    async get(id: string, { user }: { user?: UserEntity }): Promise<{ guild: GuildEntity, member: MemberEntity }> {
        await this.validation.checkUserState(user);
        const guild = await GuildEntity.findOne({
            where: {
                id: id,
            },
        });
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user!.id,
            },
        });
        await this.validation.checkMemberState(member);
        guild!.currentMember = member;

        return {
            guild: guild!,
            member: member!,
        };
    }

    async gets({ count, offset, user }: { count?: number, offset?: number, user: UserEntity }): Promise<{ guilds:  GuildEntity[], paginatable: boolean }> {
        await this.validation.checkUserState(user);

        const members = await MemberEntity
            .getRepository()
            .createQueryBuilder('members')
            .innerJoinAndSelect('members.guild', 'guild', "guild.status NOT IN (:...guildStatuses)", { guildStatuses: [GuildStatus.BANNED, GuildStatus.LOCKED, GuildStatus.DELETING, GuildStatus.DELETED] })
            .where('members.user_id = :userId', { userId: user.id })
            .andWhere("members.status NOT IN (:...statuses)", { statuses: [MemberStatus.BANNED, MemberStatus.LOCKED, MemberStatus.DELETED] })
            .limit(count ? count + 1 : count)
            .offset(offset)
            .orderBy('members.index', 'ASC')
            .getMany()

        const guilds = await Bluebird.all(members.map(async (member) => {
            const guild = await member.guild;
            guild.currentMember = member;
            return guild;
        }))

        const last = !!count && guilds.length >= count && guilds.pop();

        return {
            guilds,
            paginatable: !!last,
        }
    }

    async create(attributes: GuildAttributes, { user }: { user: UserEntity }): Promise<GuildEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkGuildDisplayNameFormat(attributes.displayName);
        const profile = await FileEntity.findOne({
            where: {
                id: attributes.profileId,
            },
        });
        await this.validation.checkProfileFileState(profile);
        const guild = await GuildEntity.create({
            profileId: profile!.id,
            ownerId: user.id,
            displayName: attributes.displayName,
            description: attributes.description,
            guildname: ulid(),
            languageCode: attributes.languageCode,
            countryCode: attributes.countryCode,
            status: GuildStatus.CREATED,
            type: attributes.type == GuildType.PRIVATE ? GuildType.PRIVATE : GuildType.PUBLIC,
        }).save();

        await this.rolesDataStore.findOrCreateGuildRoles(guild);
        await this.guildStoragesDataStore.findOrCreate(guild);
        const member = await this.membersDataStore.findOrCreate({ guild, user });
        await this.memberRolesDataStore.makeMembers(member);
        await this.memberRolesDataStore.makeOwner(member);
        await this.memberRolesDataStore.makeAdmin(member, { user });
        await guild.activate();
        guild.currentMember = member;
        return guild;
    }

    async createDefault(user: UserEntity): Promise<GuildEntity> {
        const profile = seedFiles.generateGuildProfile();
        const attributes = new Guild({
            profileId: profile.id,
            displayName: this.localizer.dictionary.guild.db.defaultName(user.username),
        }).toJSON();
        return await this.create(attributes, { user });
    }

    async update(attributes: Partial<GuildAttributes> & { id: string }, { user }: { user: UserEntity }): Promise<GuildEntity> {
        await this.validation.checkUserState(user);
        attributes.displayName && await this.validation.checkGuildDisplayNameFormat(attributes.displayName);

        const exist = await GuildEntity.findOne({
            where: {
                id: attributes.id,
            },
        });
        await this.validation.checkGuildState(exist);
        const member = await MemberEntity.findOne({
            where: {
                guildId: exist?.id,
                userId: user.id,
            },
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkMemberManagable(member!);

        if (attributes.displayName !== undefined) exist!.displayName = attributes.displayName;
        if (attributes.description !== undefined) exist!.description = attributes.description;
        if (attributes.languageCode !== undefined) exist!.languageCode = attributes.languageCode;
        if (attributes.countryCode !== undefined) exist!.countryCode = attributes.countryCode;

        const guild = await exist!.save();
        guild.currentMember = member;

        return guild;
    }

    async destroy(id: string, { user }: { user: UserEntity }): Promise<void> {
        const guild = await GuildEntity.findOne({
            where: {
                id: id,
                
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: id,
                userId: user.id,
            },
        });
        await Bluebird.all([
            this.validation.checkMemberIsOwner(member!),
        ]);
        guild!.status = GuildStatus.DELETING;
        guild!.deletedAt = new Date();
        await guild?.save();
        return;
    }

    async search(query: string, { count, offset, user }: { count?: number, offset?: number, user?: UserEntity }): Promise<{ guilds: GuildEntity[], paginatable: boolean }> {
        const guilds = await GuildEntity
            .getRepository()
            .createQueryBuilder('guilds')
            .where("guilds.status NOT IN (:...statuses)", { statuses: [GuildStatus.BANNED, GuildStatus.LOCKED, GuildStatus.DELETING, GuildStatus.DELETED] })
            .andWhere("guilds.type IN (:...types)", { types: [GuildType.PUBLIC, GuildType.OFFICIAL] })
            .andWhere(`
                guilds.display_name LIKE :query OR
                guilds.description LIKE :query
            `, { query:`%${query}%` })
            .limit(count ? count + 1 : count)
            .offset(offset)
            .getMany();

        const last = !!count && guilds.length >= count && guilds.pop();

        if (user) {
            await Bluebird.all(guilds.map(async (guild) => {
                const currentMember = await MemberEntity.findOne({
                    where: {
                        userId: user.id,
                        guildId: guild.id,
                    },
                });
                guild.currentMember = currentMember;
                return guild;
            }))
        }

        return {
            guilds,
            paginatable: !!last
        }
    }

    async searchByGuildname(guildname: string, { user }: { user?: UserEntity }): Promise<{ guild: GuildEntity }> {
        const guild = await GuildEntity
            .getRepository()
            .createQueryBuilder('guilds')
            .where("guilds.status NOT IN (:...statuses)", { statuses: [GuildStatus.BANNED, GuildStatus.LOCKED, GuildStatus.DELETING, GuildStatus.DELETED] })
            .andWhere("guilds.type IN (:...types)", { types: [GuildType.PUBLIC, GuildType.OFFICIAL, GuildType.PRIVATE] })
            .andWhere(`guilds.guildname LIKE :query`, { query: guildname })
            .getOne();
        await this.validation.checkGuildState(guild);

        if (user) {
            const currentMember = await MemberEntity.findOne({
                where: {
                    userId: user.id,
                    guildId: guild?.id,
                },
            });
            guild!.currentMember = currentMember;
        }

        return {
            guild: guild!,
        }
    }
}

module.exports = GuildsDataStore;