import { Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type PostAttachmentsValidation from '@/app/validations/PostAttachmentsValidation';
import { PostEntity, FileEntity, PostAttachmentEntity } from '@guildion/db';

export default class PostAttachmentsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: PostAttachmentsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/PostAttachmentsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async findOrCreate({ post, file }: { post: PostEntity, file: FileEntity }): Promise<PostAttachmentEntity> {
        await this.validation.checkPostState(post);
        await this.validation.checkFileState(file);
        const exist = await PostAttachmentEntity.findOne({
            where: {
                postId: post.id,
                fileId: file.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            return PostAttachmentEntity.create({
                postId: post.id,
                fileId: file.id,
            }).save();
        }
    }

    async findOrCreates({ post, files }: { post: PostEntity, files: FileEntity[] }): Promise<PostAttachmentEntity[]> {
        await this.validation.checkPostState(post);
        const attachments: PostAttachmentEntity[] = []
        for (const file of files) {
            const attachment = await this.findOrCreate({ post, file });
            attachments.push(attachment);
        }
        return attachments;
    }

    async findOrUpdates({ post, files }: { post: PostEntity, files: FileEntity[] }): Promise<PostAttachmentEntity[]> {
        const attachments = await PostAttachmentEntity.find({
            where: {
                postId: post?.id,
            },
        });
        const new_attachments = await this.findOrCreates({ post: post, files });
        const new_attachment_ids = new_attachments.map(val => val.id);
        await Promise.all(
            attachments
                .filter(attachment => !new_attachment_ids.find(id => id == attachment.id))
                .map(attachment => PostAttachmentEntity.delete(attachment))
        );
        return PostAttachmentEntity.find({
            where: {
                postId: post?.id,
            },
        });
    }

}

module.exports = PostAttachmentsDataStore;