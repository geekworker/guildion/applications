import { Localizer, MessageAttributes, MessageStatus } from '@guildion/core';
import { FileEntity, MemberEntity, MessageEntity, RoomEntity, RoomMemberEntity, UserEntity } from '@guildion/db';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type MessagesValidation from '@/app/validations/MessagesValidation';
import type MessageReadsDataStore from '@/app/datastores/MessageReadsDataStore';
import type MessageAttachmentsDataStore from '@/app/datastores/MessageAttachmentsDataStore';
import type RoomLogsDataStore from '@/app/datastores/RoomLogsDataStore';

export default class MessagesDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: MessagesValidation;
    private _messageReadsDataStore!: MessageReadsDataStore;
    private _messageAttachmentsDataStore!: MessageAttachmentsDataStore;
    private _roomLogsDataStore!: RoomLogsDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/MessagesValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    get messageReadsDataStore() {
        const Constructor = require('@/app/datastores/MessageReadsDataStore');
        this._messageReadsDataStore ||= new Constructor({ localizer: this.localizer });
        return this._messageReadsDataStore
    }

    get messageAttachmentsDataStore() {
        const Constructor = require('@/app/datastores/MessageAttachmentsDataStore');
        this._messageAttachmentsDataStore ||= new Constructor({ localizer: this.localizer });
        return this._messageAttachmentsDataStore
    }

    get roomLogsDataStore() {
        const Constructor = require('@/app/datastores/RoomLogsDataStore');
        this._roomLogsDataStore ||= new Constructor({ localizer: this.localizer });
        return this._roomLogsDataStore
    }

    async gets(
        roomId: string,
        { user, count, offset, gtAt, ltAt, messageId }: { user: UserEntity, count?: number, offset?: number, gtAt?: Date, ltAt?: Date, messageId?: string }
    ): Promise<{
        messages: MessageEntity[],
        room: RoomEntity,
        member: MemberEntity,
        paginatable: boolean,
    }> {
        if (ltAt) gtAt = undefined;
        if (gtAt) ltAt = undefined;
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        });
        await this.validation.checkRoomState(room);

        const message = messageId ? await MessageEntity.findOne({
            where: {
                id: messageId,
                roomId: roomId,
            },
        }) : undefined;

        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });

        await Promise.all([
            messageId && await this.validation.checkMessageState(message),
            this.validation.checkRoomState(room),
            this.validation.checkMemberState(member),
            this.validation.checkMemberRoomViewable({ member: member!, room: room! }),
        ]);

        const messages = await MessageEntity
            .getRepository()
            .createQueryBuilder('messages')
            .where(
                messageId ?
                    'messages.room_id = :roomId AND messages.parentId = :messageId' :
                    'messages.room_id = :roomId AND messages.parentId IS NULL'
                ,{
                    roomId: room?.id,
                    messageId,
                }
            )
            .andWhere(
                gtAt ?
                    'messages.created_at >= :basedAt' :
                    'messages.created_at <= :basedAt'
                , {
                    basedAt: gtAt?.toISOString() ?? ltAt?.toISOString() ?? new Date()?.toISOString()
                }
            )
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy('messages.created_at', gtAt ? 'ASC' : 'DESC')
            .getMany();

        const last = !!count && messages.length >= count && messages.pop();

        return {
            room: room!,
            member: member!,
            messages: gtAt ? messages : messages.reverse(),
            paginatable: !!last,
        };
    }

    async create(attributes: MessageAttributes, { user }: { user: UserEntity }) {
        const room = await RoomEntity.findOne({
            where: {
                id: attributes?.roomId,
            },
        });
        const guild = await room?.guild;
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: room?.id,
                memberId: member?.id,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkRoomMemberState(roomMember),
            this.validation.checkMemberRoomViewable({ room: room!, member: member! }),
            this.validation.checkMessageBodyWithAttachments(attributes.body, attributes.files),
            this.validation.checkMessageExist(attributes.id),
        ]);

        const message = await MessageEntity.create({
            id: attributes.id ? attributes.id : undefined,
            senderId: member!.id,
            roomId: attributes.roomId!,
            body: attributes.body,
            status: MessageStatus.CREATED,
        }).save();

        if (attributes.files && attributes.files.length > 0) {
            const files: (FileEntity | undefined)[] = await Promise.all(
                (attributes.files || []).map(
                    file => FileEntity.findOne({
                        where: {
                            id: file.id,
                        },
                    })
                )
            );
            await this.messageAttachmentsDataStore.findOrCreates({
                message: message!,
                files: files.filter((val): val is FileEntity => !!val)
            });
        }

        this.roomLogsDataStore.createMessageCreateLog(
            message
        ).catch(e => { console.log(e) });

        this.messageReadsDataStore.findOrCreates(
            message
        ).catch(e => { console.log(e) });

        const parent = message.parentId ? await MessageEntity.findOne({
            where: {
                id: message.parentId,
            },
        }) : undefined;

        if (parent) {
            await Promise.all([
                this.validation.checkMessageState(message),
                this.validation.checkParentMessageIsSameRoom({ message, parent }),
            ]);
            message.location = `${parent.location}/${message.id}`;
            return message.save();
        } else {
            message.location = `${message.id}`;
            return message.save();
        }
    }

    async update(attributes: Partial<MessageAttributes> & { id: string }, { user }: { user: UserEntity }): Promise<MessageEntity> {
        const message = await MessageEntity.findOne({
            where: {
                id: attributes.id,
            },
        });
        const room = await RoomEntity.findOne({
            where: {
                id: message?.roomId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkMemberState(member),
            this.validation.checkMessageState(message),
        ]);
        if (message && member) {
            await this.validation.checkMessagePermission(message, member);
            if (attributes.body !== undefined) message!.body = attributes.body;
            if (attributes.files !== undefined) {
                const files: (FileEntity | undefined)[] = await Promise.all(
                    (attributes.files || []).map(
                        file => FileEntity.findOne({
                            where: {
                                id: file.id,
                            },
                        })
                    )
                );
                await this.messageAttachmentsDataStore.findOrUpdates({
                    message: message!,
                    files: files.filter((val): val is FileEntity => !!val)
                });
            }
            await message.save();
        }
        return message!;
    }

    async destroy(id: string, { user }: { user: UserEntity }): Promise<void> {
        const message = await MessageEntity.findOne({
            where: {
                id,
            },
        });
        const room = await RoomEntity.findOne({
            where: {
                id: message?.roomId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkMemberState(member),
            this.validation.checkMessageState(message),
        ]);
        if (message && member) {
            await this.validation.checkMessagePermission(message, member);
            message.status = MessageStatus.DELETED;
            await message.save();
        }
    }

}

module.exports = MessagesDataStore;