import { hasErrorPromise, Localizer, MessageReadStatus } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type MessageReadsValidation from '@/app/validations/MessageReadsValidation';
import Bluebird from 'bluebird';
import { MessageReadEntity, MessageEntity, RoomMemberEntity, MemberEntity, RoomEntity, UserEntity } from '@guildion/db';

export default class MessageReadsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: MessageReadsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/MessageReadsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async findOrCreate({ 
        roomMember,
        member,
        message,
    }: { 
        roomMember: RoomMemberEntity,
        member?: MemberEntity,
        message: MessageEntity
    }): Promise<MessageReadEntity | undefined> {
        member ||= await roomMember.member;
        if (await hasErrorPromise(this.validation.checkMemberState(member))) return;
        if (await hasErrorPromise(this.validation.checkRoomMemberState(roomMember))) return;

        const exist = await MessageReadEntity.findOne({
            where: {
                roomMemberId: roomMember.id,
                memberId: member.id,
                messageId: message.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            return MessageReadEntity.create({
                roomMemberId: roomMember.id,
                memberId: member.id,
                messageId: message.id,
                status: await roomMember.messageNotificatable(message) ? MessageReadStatus.NOTIFIED : MessageReadStatus.CREATED,
            }).save();
        }
    }

    async findOrCreates(message: MessageEntity): Promise<(MessageReadEntity | undefined)[]> {
        const roomMembers = await RoomMemberEntity.find({
            where: {
                roomId: message.roomId,
            },
        });
        return Bluebird.map(
            roomMembers.filter(roomMember => roomMember.memberId !== message.senderId),
            roomMember => this.findOrCreate({ roomMember, message }),
            { concurrency: 10 }
        );
    }

    async read({ 
        roomMember,
        member,
        message,
    }: { 
        roomMember: RoomMemberEntity,
        member?: MemberEntity,
        message: MessageEntity
    }): Promise<MessageReadEntity | undefined> {
        member ||= await roomMember.member;
        if (await hasErrorPromise(this.validation.checkMemberState(member))) return;
        if (await hasErrorPromise(this.validation.checkRoomMemberState(roomMember))) return;

        const exist = await MessageReadEntity.findOne({
            where: {
                roomMemberId: roomMember.id,
                memberId: member.id,
                messageId: message.id,
            },
        });

        if (exist) {
            exist.status = MessageReadStatus.CHECKED;
            return exist.save();
        } else {
            return undefined;
        }
    }

    async unread({ 
        roomMember,
        member,
        message,
    }: { 
        roomMember: RoomMemberEntity,
        member: MemberEntity | undefined | null,
        message: MessageEntity
    }): Promise<MessageReadEntity | undefined> {
        member ||= await roomMember.member;
        if (await hasErrorPromise(this.validation.checkMemberState(member))) return;
        if (await hasErrorPromise(this.validation.checkRoomMemberState(roomMember))) return;

        const exist = await MessageReadEntity.findOne({
            where: {
                roomMemberId: roomMember.id,
                memberId: member.id,
                messageId: message.id,
            },
        });

        if (exist) {
            exist.status = await roomMember.messageNotificatable(message) ? MessageReadStatus.NOTIFIED : MessageReadStatus.CREATED;
            return exist.save();
        } else {
            return undefined;
        }
    }

    async readAll(roomId: string, { user }: { user: UserEntity }): Promise<(MessageReadEntity | undefined)[]> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId,
                memberId: member?.id,
            },
        });

        await Promise.all([
            this.validation.checkMemberState(member),
            this.validation.checkRoomMemberState(roomMember),
            this.validation.checkRoomState(room),
            this.validation.checkMemberRoomViewable({ room: room!, member: member! }),
        ]);

        const unreads = await MessageReadEntity
            .getRepository()
            .createQueryBuilder('message_reads')
            .where('message_reads.status IN (:...statuses)', { statuses: [MessageReadStatus.CREATED, MessageReadStatus.NOTIFIED] })
            .where('message_reads.roomMember_id IN (:...roomMemberId)', { roomMemberId: roomMember!.id })
            .getMany()

        return await Bluebird.map(
            unreads,
            async unread => this.read({
                roomMember: roomMember!,
                member: member!,
                message: await unread.message,
            }),
            { concurrency: 10 },
        )
    }
}

module.exports = MessageReadsDataStore;