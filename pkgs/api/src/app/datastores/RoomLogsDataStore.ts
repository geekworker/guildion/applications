import { Localizer, MessageStatus, PostStatus, RoomLogType } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomLogsValidation from '@/app/validations/RoomLogsValidation';
import { UserEntity, RoomLogEntity, RoomEntity, MemberEntity, MessageEntity, PostEntity } from "@guildion/db";
import { MoreThanOrEqual, LessThanOrEqual } from "typeorm";

export default class RoomLogsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RoomLogsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomLogsValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async gets(
        roomId: string,
        { 
            user,
            count,
            offset,
            ltAt,
            gtAt,
        }: {
            user: UserEntity,
            count?: number,
            offset?: number,
            ltAt?: Date,
            gtAt?: Date,
        }
    ): Promise<{ logs: RoomLogEntity[], paginatable: boolean }> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: room?.guildId,
            },
        });

        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkUserState(user),
            this.validation.checkMemberState(member),
            this.validation.checkMemberRoomViewable({ member: member!, room: room! })
        ]);

        const logs = await RoomLogEntity
            .getRepository()
            .createQueryBuilder('room_logs')
            .where(!!gtAt ? 'room_logs.created_at >= :basedAt' : 'room_logs.created_at >= :basedAt', { basedAt: gtAt ?? ltAt ?? new Date() })
            .andWhere('room_logs.room_id = :roomId', { roomId })
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .getMany();

        const last = !!count && logs.length >= count && logs.pop();

        return {
            logs,
            paginatable: !!last,
        }
    }

    async create(log: RoomLogEntity) {
        const result = await log.save();
        return result;
    }

    async createMessageCreateLog(message: MessageEntity) {
        if (message.status != MessageStatus.CREATED) return;
        const exist = await RoomLogEntity.findOne({
            where: {
                messageId: message.id,
                type: RoomLogType.MessageCreate,
            },
        });
        if (exist) return;
        await this.create(
            RoomLogEntity.create({
                roomId: message.roomId,
                messageId: message.id,
                type: RoomLogType.MessageCreate,
            })
        );
    }

    async createPostCreateLog(post: PostEntity) {
        if (post.status != PostStatus.CREATED) return;
        const exist = await RoomLogEntity.findOne({
            where: {
                postId: post.id,
                type: RoomLogType.MessageCreate,
            },
        });
        if (exist) return;
        await this.create(
            RoomLogEntity.create({
                roomId: post.roomId,
                postId: post.id,
                type: RoomLogType.PostCreate,
            })
        );
    }
}

module.exports = RoomLogsDataStore;