import { DocumentGroupStatus, DocumentSectionStatus, DocumentStatus, Localizer, LanguageCode } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type DocumentsValidation from '@/app/validations/DocumentsValidation';
import { DocumentEntity, DocumentGroupEntity, DocumentSectionEntity } from '@guildion/db';

export default class DocumentsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: DocumentsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/DocumentsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async getPaths(
    ): Promise<{
        paths: { params: { groupSlug: string, sectionSlug: string, slug: string }, locale: LanguageCode }[],
    }> {
        const groups = await DocumentGroupEntity.find({
            where: {
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        const sections = await DocumentSectionEntity
            .getRepository()
            .createQueryBuilder('document_sections')
            .where('document_sections.groupId IN (:...groupIds)', { groupIds: groups.map(g => g.id) })
            .andWhere('document_sections.status = :status', { status: DocumentSectionStatus.PUBLISH })
            .getMany();

        const documents = await DocumentEntity
            .getRepository()
            .createQueryBuilder('documents')
            .where('documents.sectionId IN (:...sectionIds)', { sectionIds: sections.map(s => s.id) })
            .andWhere('documents.status = :status', { status: DocumentStatus.PUBLISH })
            .getMany();

        return {
            paths: [
                ...documents.filter(d => !!d.jaTitle && !!d.jaData).map(d => {
                    const s = sections.find(s => s.id === d.sectionId)!;
                    return {
                        params: {
                            groupSlug: groups.find(g => g.id === s.groupId)!.slug,
                            sectionSlug: s.slug,
                            slug: d.slug,
                        },
                        locale: LanguageCode.Japanese,
                    }
                }),
                ...documents.filter(d => !!d.enTitle && !!d.enData).map(d => {
                    const s = sections.find(s => s.id === d.sectionId)!;
                    return {
                        params: {
                            groupSlug: groups.find(g => g.id === s.groupId)!.slug,
                            sectionSlug: s.slug,
                            slug: d.slug,
                        },
                        locale: LanguageCode.English,
                    }
                })
            ],
        };
    }

    async get(
        groupSlug: string,
        sectionSlug: string,
        slug: string,
    ): Promise<{
        group: DocumentGroupEntity,
        section: DocumentSectionEntity,
        document: DocumentEntity,
    }> {
        const group = await DocumentGroupEntity.findOneOrFail({
            where: {
                slug: groupSlug,
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        const section = await DocumentSectionEntity.findOneOrFail({
            where: {
                groupId: group.id,
                slug: sectionSlug,
                status: DocumentSectionStatus.PUBLISH,
            },
        });
        const document = await DocumentEntity.findOneOrFail({
            where: {
                sectionId: section.id,
                slug,
                status: DocumentStatus.PUBLISH,
            },
        });
        return {
            group,
            section,
            document,
        };
    }
}

module.exports = DocumentsDataStore;