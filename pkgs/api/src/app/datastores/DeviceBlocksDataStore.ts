import { Localizer, UserDeviceSessionStatus, UserDeviceStatus } from "@guildion/core";
import { UserEntity, DeviceEntity, DeviceBlockEntity, UserDeviceEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type DeviceBlocksValidation from '@/app/validations/DeviceBlocksValidation';

export default class DeviceBlocksDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: DeviceBlocksValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/DeviceBlocksValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async gets({ count, offset, user }: { count?: number, offset?: number, user: UserEntity }): Promise<{ devices:  DeviceEntity[], paginatable: boolean }> {
        await this.validation.checkUserState(user);
        const devices = await DeviceEntity
            .getRepository()
            .createQueryBuilder('devices')
            .innerJoin("devices.deviceBlocks", "device_blocks", 'device_blocks.user_id = :userId', { userId: user.id })
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy('device_blocks.created_at', 'DESC')
            .getMany();

        const last = !!count && devices.length >= count && devices.pop();

        return {
            devices,
            paginatable: !!last,
        }
    }

    async findOrCreate(deviceId: string, { user }: { user: UserEntity }) {
        const device = await DeviceEntity.findOne({
            where: {
                id: deviceId,
            },
        });
        await Promise.all([
            this.validation.checkDeviceState(device),
            this.validation.checkUserState(user),
        ]);
        const exist = await DeviceBlockEntity.findOne({
            where: {
                userId: user.id,
                deviceId,
            }
        });
        if (!!exist) {
            return exist;
        } else {
            const deviceBlock = await DeviceBlockEntity.create({
                userId: user.id,
                deviceId,
            }).save();
            const userDevice = await UserDeviceEntity.findOne({
                where: {
                    userId: user.id,
                    deviceId,
                },
            });
            if (!userDevice) return deviceBlock;
            userDevice.status = UserDeviceStatus.LOCKED;
            userDevice.sessionStatus = UserDeviceSessionStatus.SHOULD_LOGOUT;
            userDevice.save();
            return deviceBlock;
        }
    }

    async destroy(deviceId: string, { user }: { user: UserEntity }): Promise<void> {
        const device = await DeviceEntity.findOne({
            where: {
                id: deviceId,
            },
        });
        await Promise.all([
            this.validation.checkDeviceState(device),
            this.validation.checkUserState(user),
        ]);
        await DeviceBlockEntity.delete({
            userId: user.id,
            deviceId,
        });
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId,
            },
        });
        if (!userDevice) return;
        userDevice.status = UserDeviceStatus.CREATED;
        userDevice.save();
        return;
    }
}

module.exports = DeviceBlocksDataStore;