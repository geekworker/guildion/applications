import { hasErrorPromise, Localizer, PostReadStatus } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type PostReadsValidation from '@/app/validations/PostReadsValidation';
import Bluebird from 'bluebird';
import { PostReadEntity, PostEntity, RoomMemberEntity, MemberEntity, RoomEntity, UserEntity } from '@guildion/db';

export default class PostReadsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: PostReadsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/PostReadsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async findOrCreate({ 
        roomMember,
        member,
        post,
    }: { 
        roomMember: RoomMemberEntity,
        member?: MemberEntity,
        post: PostEntity
    }): Promise<PostReadEntity | undefined> {
        member ||= await roomMember.member;
        if (await hasErrorPromise(this.validation.checkMemberState(member))) return;
        if (await hasErrorPromise(this.validation.checkRoomMemberState(roomMember))) return;

        const exist = await PostReadEntity.findOne({
            where: {
                roomMemberId: roomMember.id,
                memberId: member.id,
                postId: post.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            return PostReadEntity.create({
                roomMemberId: roomMember.id,
                memberId: member.id,
                postId: post.id,
                status: await roomMember.postNotificatable(post) ? PostReadStatus.NOTIFIED : PostReadStatus.CREATED,
            }).save();
        }
    }

    async findOrCreates(post: PostEntity): Promise<(PostReadEntity | undefined)[]> {
        const roomMembers = await RoomMemberEntity.find({
            where: {
                roomId: post.roomId,
            },
        });
        return Bluebird.map(
            roomMembers.filter(roomMember => roomMember.memberId !== post.senderId),
            roomMember => this.findOrCreate({ roomMember, post }),
            { concurrency: 10 }
        );
    }

    async read({ 
        roomMember,
        member,
        post,
    }: { 
        roomMember: RoomMemberEntity,
        member?: MemberEntity,
        post: PostEntity
    }): Promise<PostReadEntity | undefined> {
        member ||= await roomMember.member;
        if (await hasErrorPromise(this.validation.checkMemberState(member))) return;
        if (await hasErrorPromise(this.validation.checkRoomMemberState(roomMember))) return;

        const exist = await PostReadEntity.findOne({
            where: {
                roomMemberId: roomMember.id,
                memberId: member.id,
                postId: post.id,
            },
        });

        if (exist) {
            exist.status = PostReadStatus.CHECKED;
            return exist.save();
        } else {
            return undefined;
        }
    }

    async unread({ 
        roomMember,
        member,
        post,
    }: { 
        roomMember: RoomMemberEntity,
        member: MemberEntity | undefined | null,
        post: PostEntity
    }): Promise<PostReadEntity | undefined> {
        member ||= await roomMember.member;
        if (await hasErrorPromise(this.validation.checkMemberState(member))) return;
        if (await hasErrorPromise(this.validation.checkRoomMemberState(roomMember))) return;

        const exist = await PostReadEntity.findOne({
            where: {
                roomMemberId: roomMember.id,
                memberId: member.id,
                postId: post.id,
            },
        });

        if (exist) {
            exist.status = await roomMember.postNotificatable(post) ? PostReadStatus.NOTIFIED : PostReadStatus.CREATED;
            return exist.save();
        } else {
            return undefined;
        }
    }

    async readAll(roomId: string, { user }: { user: UserEntity }): Promise<(PostReadEntity | undefined)[]> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId,
                memberId: member?.id,
            },
        });

        await Promise.all([
            this.validation.checkMemberState(member),
            this.validation.checkRoomMemberState(roomMember),
            this.validation.checkRoomState(room),
            this.validation.checkMemberRoomViewable({ room: room!, member: member! }),
        ]);

        const unreads = await PostReadEntity
            .getRepository()
            .createQueryBuilder('post_reads')
            .where('post_reads.status IN (:...statuses)', { statuses: [PostReadStatus.CREATED, PostReadStatus.NOTIFIED] })
            .where('post_reads.roomMember_id IN (:...roomMemberId)', { roomMemberId: roomMember!.id })
            .getMany()

        return await Bluebird.map(
            unreads,
            async unread => this.read({
                roomMember: roomMember!,
                member: member!,
                post: await unread.post,
            }),
            { concurrency: 10 },
        )
    }
}

module.exports = PostReadsDataStore;