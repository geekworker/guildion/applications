import { Localizer, Device, Account, Unwrapper, AppError, AccessTokenStatus, AccountStatus, UserStatus, AdministratorRole } from "@guildion/core";
import { Tokenizer } from "@guildion/node";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type AccessTokensValidation from '@/app/validations/AccessTokensValidation';
import { AccountEntity, DeviceEntity, AccessTokenEntity } from "@guildion/db"

export default class AccessTokensDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: AccessTokensValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/AccessTokensValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation;
    }

    async create({ account, device }: { account: Account | AccountEntity, device: Device | DeviceEntity }): Promise<string> {
        await this.validation.beforeCreate({ account, device });
        const accessToken = await AccessTokenEntity.create({
            accountId: account.id!,
            deviceId: device.id!,
            status: AccessTokenStatus.ACTIVE,
        }).save();
        return await Tokenizer.sign({ id: accessToken.id }, Tokenizer.Action.accessToken);
    }

    async createAndSaveSecret({ account, device }: { account: Account | AccountEntity, device: Device | DeviceEntity }): Promise<string> {
        await this.validation.beforeCreate({ account, device });
        const accessToken = await AccessTokenEntity.create({
            accountId: account.id!,
            deviceId: device.id!,
            status: AccessTokenStatus.ACTIVE,
        }).save();
        const secret = await Tokenizer.sign({ id: accessToken.id }, Tokenizer.Action.accessToken);
        accessToken.secret = secret;
        await accessToken.save();
        return secret;
    }

    async verify(token: string, device: Device, options?: { adminRoles?: AdministratorRole[] }): Promise<AccountEntity> {
        await this.validation.beforeVerify(token);
        const decoded = await Tokenizer.verify(token, Tokenizer.Action.accessToken);
        if (!decoded.id || !Unwrapper.isString(decoded.id) || decoded.id.length == 0) throw new AppError(this.localizer.dictionary.error.accessToken.invalidToken);
        const accessToken = await AccessTokenEntity.findOne({
            where: {
                id: decoded.id,
                status: AccessTokenStatus.ACTIVE,
            },
        });
        if (!accessToken) throw new AppError(this.localizer.dictionary.error.accessToken.invalidToken);
        const account = await accessToken.account;
        const user = await account.user;
        if (
            account.status == AccountStatus.DELETED ||
            account.status == AccountStatus.LOCKED ||
            user.status == UserStatus.LOCKED ||
            user.status == UserStatus.DELETED ||
            accessToken.deviceId != device.id
        ) throw new AppError(this.localizer.dictionary.error.accessToken.invalidToken);
        if (options?.adminRoles) {
            const admin = await user.administrator;
            if (!admin || !options.adminRoles.includes(admin.role)) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        }
        return account;
    }
}

module.exports = AccessTokensDataStore;