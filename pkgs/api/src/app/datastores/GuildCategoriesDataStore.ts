import { Localizer } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type GuildCategoriesValidation from '@/app/validations/GuildCategoriesValidation';
import { GuildCategoryEntity } from "@guildion/db";

export default class GuildCategoriesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: GuildCategoriesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/GuildCategoriesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async gets(): Promise<GuildCategoryEntity[]> {
        return await GuildCategoryEntity.find({
            order: { index: 'ASC' },
        });
    }
}

module.exports = GuildCategoriesDataStore;