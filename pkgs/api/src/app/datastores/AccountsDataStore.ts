import { AccountAttributes, AccountStatus, AppError, ErrorType, Localizer, UserAttributes, UserDeviceSessionStatus, UserDeviceStatus, UserStatus } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type AccountsValidation from '@/app/validations/AccountsValidation';
import { AccountEntity, DeviceBlockEntity, DeviceEntity, FileEntity, UserDeviceEntity, UserEntity } from "@guildion/db";

export default class AccountsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: AccountsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/AccountsValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation;
    }

    async checkUsernameExists(username: string): Promise<boolean> {
        await this.validation.checkUsernameFormat(username);
        return true;
    }

    async create(user: UserAttributes): Promise<{ user: UserEntity, account: AccountEntity }> {
        await this.validation.checkUsernameFormat(user.username);
        const profile = await FileEntity.findOne({
            where: {
                id: user.profileId,
            },
        });
        await this.validation.checkProfileFileState(profile);
        const newUser = await UserEntity.create({
            profileId: profile!.id,
            username: user.username,
            displayName: user.username,
            description: user.description,
            languageCode: user.languageCode,
            countryCode: user.countryCode,
            timezone: user.timezone,
            status: UserStatus.CREATED,
        }).save();
        const account = await AccountEntity.create({
            userId: newUser.id,
            status: AccountStatus.CREATED,
        }).save();
        return { 
            user: (
                await UserEntity.findOne({
                    where: {
                        id: newUser.id,
                    },
                })
            )!,
            account 
        };
    }

    async update(attributes: Partial<AccountAttributes>, { user, device }: { user: UserEntity, device: DeviceEntity }): Promise<AccountEntity> {
        const exist = await user.account;
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            }
        });
        const deviceBlock = await DeviceBlockEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            }
        });
        await this.validation.checkAccountState(exist);
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        await this.validation.checkDeviceBlocked(deviceBlock);
        await this.validation.checkUserDeviceSessionState(userDevice);

        exist!.sessionAllowedAt = attributes.sessionAllowedAt;
        exist!.sessionAllowedIntervalSec = attributes.sessionAllowedIntervalSec;

        const account = await exist.save();

        return account;
    }

    async allowSession(sessionAllowedIntervalSec: number, { user, device }: { user: UserEntity, device: DeviceEntity }): Promise<AccountEntity> {
        const exist = await user.account;
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            }
        });
        const deviceBlock = await DeviceBlockEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            }
        });
        await this.validation.checkAccountState(exist);
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        await this.validation.checkDeviceBlocked(deviceBlock);
        await this.validation.checkUserDeviceSessionState(userDevice);

        exist!.sessionAllowedAt = new Date();
        exist!.sessionAllowedIntervalSec = sessionAllowedIntervalSec;

        const account = await exist.save();

        return account;
    }

    async denySession({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<AccountEntity> {
        const exist = await user.account;
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            }
        });
        const deviceBlock = await DeviceBlockEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            }
        });
        await this.validation.checkAccountState(exist);
        await this.validation.checkUserState(user);
        await this.validation.checkDeviceState(device);
        await this.validation.checkDeviceBlocked(deviceBlock);
        await this.validation.checkUserDeviceSessionState(userDevice);

        exist!.sessionAllowedAt = null;
        exist!.sessionAllowedIntervalSec = null;

        const account = await exist.save();

        return account;
    }

    async authenticate({ username, device }: { username: string, device: DeviceEntity }): Promise<{ account?: AccountEntity, shouldTwoFactorAuth: boolean }> {
        const user = await UserEntity.findOne({
            where: {
                username,
            },
        });
        await this.validation.checkUserState(user).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });

        const deviceBlock = await DeviceBlockEntity.findOne({
            where: {
                userId: user?.id,
                deviceId: device.id,
            },
        });
        await this.validation.checkDeviceBlocked(deviceBlock).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });

        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user?.id,
                deviceId: device.id,
            },
        });

        const account = await user!.account;
        if (userDevice?.status !== UserDeviceStatus.TRUSTED) await this.validation.checkAccountSessionState(account).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });

        if (!userDevice || userDevice?.status !== UserDeviceStatus.TRUSTED) {
            if (userDevice) await this.validation.checkUserDeviceLock(userDevice).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });;
            await this.validation.checkDeviceRegisterable(device).catch(e => { throw new AppError(e.message, { type: ErrorType.accountUsername }) });
            return { shouldTwoFactorAuth: true };
        } else {
            return {
                account,
                shouldTwoFactorAuth: false,
            }
        }
    }

    async logout({ user, device }: { user: UserEntity, device: DeviceEntity }) {
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        if (!userDevice) return;
        userDevice.sessionStatus = UserDeviceSessionStatus.LOGOUT;
        await userDevice.save();
        return;
    }
}

module.exports = AccountsDataStore;