import { FileType, Localizer, FileAttributes, ContentType, GET_CURRENT_CDN_PROXY_URL_STRING, FileExtension, FileStatus, Provider, File, FileAccessType, contentTypeToFileType } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type FilesValidation from '@/app/validations/FilesValidation';
import { FileEntity, FilingEntity, FolderEntity, ImageEntity, RoomFileEntity, UserEntity, VideoEntity } from "@guildion/db";
import { v4 as uuidv4 } from 'uuid';
import S3Adaptor from "@/app/modules/aws/s3";
import axios from "axios";
import Bluebird from "bluebird";

const getMediaDimensions = require('get-media-dimensions') as any;

export default class FilesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: FilesValidation;
    private $s3Adaptor!: S3Adaptor;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/FilesValidation')
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get s3Adaptor() {
        this.$s3Adaptor ||= new S3Adaptor({ localizer: this.localizer });
        return this.$s3Adaptor
    }

    async getSignedURL(file: FileEntity): Promise<string> {
        switch(file.accessType) {
        default:
        case FileAccessType.PUBLIC: return file.url;
        case FileAccessType.PRIVATE: return this.s3Adaptor.getSignedURLForGet({ key: file.providerKey });
        }
    }

    async getsSystem(): Promise<FileEntity[]> {
        const files = await FileEntity
            .getRepository()
            .createQueryBuilder('files')
            .where("files.is_system = :isSystem", { isSystem: true })
            .andWhere("files.type NOT IN (:...types)", { types: [FileType.Emoji, FileType.Folder] })
            .orderBy('files.created_at', 'ASC')
            .getMany();
        return files;
    }

    async get(id: string, { user }: { user: UserEntity }): Promise<FileEntity> {
        const file = await FileEntity.findOne({
            where: {
                id,
            },
        });
        await this.validation.checkUserState(user);
        await this.validation.checkFileState(file);
        await this.validation.checkFileViewable({ file: file!, user });
        return file!;
    }

    async update(attributes: Partial<FileAttributes> & { id: string }, { user }: { user: UserEntity }): Promise<FileEntity> {
        const exist = await FileEntity.findOne({
            where: {
                id: attributes.id,
            },
        });
        await this.validation.checkUserState(user);
        await this.validation.checkFileState(exist);
        await this.validation.checkFileManagable({ file: exist!, user });
        if (attributes.displayName !== undefined) exist!.displayName = attributes.displayName;
        if (attributes.description !== undefined) exist!.description = attributes.description;
        const result = await exist!.save();
        if (attributes.filingId) {
            const filing = await FilingEntity.findOne({
                where: {
                    id: attributes.filingId,
                    fileId: attributes.id,
                },
            });
            if (!filing) return result;
            if (attributes.index !== undefined && attributes.index !== null) filing.index = attributes.index;
            if (attributes.randomIndex !== undefined && attributes.randomIndex !== null) filing.randomIndex = attributes.index;
            await filing.save();
        }

        const roomFile = await RoomFileEntity.findOne({
            where: {
                fileId: attributes.id,
            },
        });
        if (roomFile) {
            if (attributes.showInGuild !== undefined) roomFile.showInGuild = attributes.showInGuild;
            await roomFile.save();
        }

        return result;
    }

    async destroy(id: string, { user }: { user: UserEntity }): Promise<void> {
        const file = await FileEntity.findOne({
            where: {
                id,
            },
        });
        await this.validation.checkUserState(user);
        await this.validation.checkFileState(file);
        await this.validation.checkFileViewable({ file: file!, user });
        file!.status = FileStatus.DELETING;
        await file?.save();
    }

    async findOrCreateStorage(prefix: string): Promise<FileEntity> {
        const exist = await FileEntity.findOne({
            where: {
                providerKey: prefix,
                type: FileType.Folder,
                isSystem: true,
                contentType: ContentType.folder,
            },
        });
        if (exist) {
            return exist;
        } else {
            const newFile = await FileEntity.create({
                url: `${GET_CURRENT_CDN_PROXY_URL_STRING()}/${prefix}`,
                type: FileType.Folder,
                contentType: ContentType.folder,
                extension: FileExtension(ContentType.folder),
            }).save();
            await FolderEntity.create({
                fileId: newFile.id,
            }).save();
            return FileEntity.create({
                ...newFile,
                displayName: '',
                description: '',
                extension: FileExtension(ContentType.folder),
                type: FileType.Folder,
                status: FileStatus.UPLOADED,
                provider: Provider.S3,
                providerKey: prefix,
                isSystem: true,
                contentType: ContentType.folder,
                contentLength: 0,
            }).save();
        }
    }
    
    async upload(fileId: string): Promise<FileEntity> {
        const file = await FileEntity.findOne({
            where: {
                id: fileId,
                status: FileStatus.UPLOADING,
                isSystem: false,
            },
        });
        return this.uploadFromEntity(file!);
    }

    async uploadFromEntity(file: FileEntity): Promise<FileEntity> {
        await this.validation.checkUploadingFileState(file);
        const url = await this.getSignedURL(file!);
        await this.validation.checkURLExistState(url);
        file!.status = FileStatus.UPLOADED;
        file = await this.syncMeta(file!);
        const result = await FileEntity.findOne({
            where: {
                id: file.id,
            },
        });
        result!.contentLength = file?.contentLength;
        result!.status = FileStatus.UPLOADED;
        return result!.save();
    }

    async getFileSize(url: string): Promise<number> {
        const res = await axios.head(url);
        const contentLength = res.headers['content-length'];
        if (!contentLength) return 0;
        return Number(contentLength) || 0;
    }

    async getFolderSize(file: FileEntity): Promise<number> {
        if (!file.isFolder) return 0;
        const folder = await file.folder;
        if (!folder) return 0;
        return folder.calcContentLength();
    }

    async syncMeta(file: FileEntity): Promise<FileEntity> {
        switch(file.type) {
        case FileType.Image: return this.syncImageMeta(file);
        case FileType.Video: return this.syncVideoMeta(file);
        default: return file;
        }
    }

    async syncVideoMeta(file: FileEntity): Promise<FileEntity> {
        if (file.type !== FileType.Video) return file;
        const video = await file.video;
        const url = await this.getSignedURL(file);
        const result = await getMediaDimensions(url, 'video');
        const durationMs = result.duration * 1000;
        const widthPx = result.width;
        const heightPx = result.height;
        const thumbnail = await FileEntity.findOne({
            where: {
                providerKey: File.generateThumbnailKey(file.providerKey),
                provider: file.provider,
            },
        });
        if (!video) {
            await VideoEntity.create({
                durationMs,
                widthPx,
                heightPx,
                fileId: file.id,
                thumbnailId: thumbnail?.id,
            }).save();
        } else {
            await VideoEntity.create({
                id: video.id,
                fileId: video.fileId,
                durationMs,
                widthPx,
                heightPx,
                thumbnailId: thumbnail?.id,
            }).save();
        }
        file.contentLength = await this.getFileSize(url);
        await this.syncFolderMetaFromFile(file);
        return file;
    }

    async syncImageMeta(file: FileEntity): Promise<FileEntity> {
        if (file.type !== FileType.Image) return file;
        const image = await file.image;
        const url = await this.getSignedURL(file);
        const result = await getMediaDimensions(url, 'image');
        const widthPx = result.width;
        const heightPx = result.height;
        if (!image) {
            await ImageEntity.create({
                fileId: file.id,
                widthPx,
                heightPx,
            }).save();
        } else {
            await ImageEntity.create({
                id: image.id,
                fileId: image.fileId,
                widthPx,
                heightPx,
            }).save();
        }
        file.contentLength = await this.getFileSize(url);
        await this.syncFolderMetaFromFile(file);
        return file;
    }

    async syncFolderMeta(file: FileEntity): Promise<FileEntity> {
        if (!file.isFolder) return file;
        const folder = await file.folder;
        if (!folder) {
            const folder = await FolderEntity.create({
                fileId: file.id,
            }).save();
            await folder.updateFilesCount();
        } else {
            await folder.updateFilesCount();
        }
        file.contentLength = await this.getFolderSize(file);
        await file.save();
        await this.refreshIndex(file.id);
        await this.refreshRandomIndex(file.id);
        return file;
    }

    async syncFolderMetaFromFile(file: FileEntity): Promise<void> {
        const filings = await FilingEntity.find({
            where: {
                fileId: file.id,
            },
        });
        await Promise.all(
            filings.map(async filing => {
                const folder = await filing.folder
                this.syncFolderMeta(folder);
            })
        );
    }

    async presign(attributes: FileAttributes): Promise<{ file: FileEntity, presignedURL: string }> {
        const type = contentTypeToFileType(attributes.contentType);
        await this.validation.checkUploadableFileType(type);
        const file = FileEntity.create({
            id: attributes.id ?? uuidv4(),
            displayName: attributes.displayName,
            description: attributes.description,
            url: `${GET_CURRENT_CDN_PROXY_URL_STRING()}/${attributes.providerKey}`,
            contentType: attributes.contentType,
            extension: FileExtension(attributes.contentType),
            type,
            accessType: attributes.accessType,
            status: FileStatus.UPLOADING,
            contentLength: 0,
            provider: Provider.S3,
            providerKey: attributes.providerKey,
        });
        const presignedURL = await this.s3Adaptor.getSignedURLForPut({ key: attributes.providerKey, contentType: file.contentType });
        return {
            file: await file.save(),
            presignedURL,
        };
    }

    async createFolder(attributes: FileAttributes): Promise<FileEntity> {
        const file = FileEntity.create({
            id: attributes.id ?? uuidv4(),
            url: `${GET_CURRENT_CDN_PROXY_URL_STRING()}/${attributes.providerKey}`,
            displayName: attributes.displayName,
            description: attributes.description,
            contentType: ContentType.folder,
            extension: FileExtension(ContentType.folder),
            type: FileType.Folder,
            accessType: attributes.accessType,
            status: FileStatus.UPLOADED,
            contentLength: 0,
            provider: Provider.S3,
            providerKey: attributes.providerKey,
        });
        const result = await file.save();
        await this.syncFolderMeta(result);
        return result;
    }

    async createPlaylist(attributes: FileAttributes): Promise<FileEntity> {
        const file = FileEntity.create({
            id: attributes.id ?? uuidv4(),
            url: `${GET_CURRENT_CDN_PROXY_URL_STRING()}/${attributes.providerKey}`,
            displayName: attributes.displayName,
            description: attributes.description,
            contentType: ContentType.playlist,
            extension: FileExtension(ContentType.playlist),
            type: FileType.Playlist,
            accessType: attributes.accessType,
            status: FileStatus.UPLOADED,
            contentLength: 0,
            provider: Provider.S3,
            providerKey: attributes.providerKey,
        });
        const result = await file.save();
        await FolderEntity.create({
            fileId: result.id,
        }).save();
        return result;
    }

    async createAlbum(attributes: FileAttributes): Promise<FileEntity> {
        const file = FileEntity.create({
            id: attributes.id ?? uuidv4(),
            url: `${GET_CURRENT_CDN_PROXY_URL_STRING()}/${attributes.providerKey}`,
            displayName: attributes.displayName,
            description: attributes.description,
            contentType: ContentType.album,
            extension: FileExtension(ContentType.album),
            type: FileType.Album,
            accessType: attributes.accessType,
            status: FileStatus.UPLOADED,
            contentLength: 0,
            provider: Provider.S3,
            providerKey: attributes.providerKey,
        });
        const result = await file.save();
        await FolderEntity.create({
            fileId: result.id,
        }).save();
        return result;
    }


    async getLastIndex(folder: FileEntity): Promise<number> {
        const count = await FilingEntity.count({
            where: {
                folderId: folder.id,
            },
        });
        return count;
    }

    async refreshIndex(folderId: string): Promise<void> {
        const filings = await FilingEntity
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.file', 'file', 'file.status = :status', { status: FileStatus.UPLOADED })
            .where('filings.folder_id = :id', { id: folderId })
            .orderBy({ 'index': 'ASC' })
            .getMany();

        await Bluebird.map(
            filings,
            (filing, i) => {
                filing.index = i;
                return filing.save();
            },
            { concurrency: 10 }
        );
    }

    async refreshRandomIndex(folderId: string): Promise<void> {
        const filings = await FilingEntity
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.file', 'file', 'file.status = :status', { status: FileStatus.UPLOADED })
            .where('filings.folder_id = :id', { id: folderId })
            .orderBy({ 'random_index': 'ASC' })
            .getMany();

        await Bluebird.map(
            filings,
            (filing, i) => {
                filing.randomIndex = i;
                return filing.save();
            },
            { concurrency: 10 }
        );
    }
}

module.exports = FilesDataStore;