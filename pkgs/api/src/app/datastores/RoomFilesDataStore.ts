import { AppError, ContentType, File, FileAccessType, FileAttributes, FileExtension, FileStatus, FileType, Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type RoomFilesValidation from '@/app/validations/RoomFilesValidation';
import type FilesDataStore from "./FilesDataStore";
import type RoomStoragesDataStore from '@/app/datastores/RoomStoragesDataStore';
import S3Adaptor from "@/app/modules/aws/s3";
import { v4 as uuidv4 } from 'uuid';
import { FilingEntity, GuildEntity, MemberEntity, RoomEntity, RoomMemberEntity, UserEntity } from '@guildion/db';

export default class RoomFilesDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: RoomFilesValidation;
    private _filesDataStore!: FilesDataStore;
    private $s3Adaptor!: S3Adaptor;
    private _roomStoragesDataStore!: RoomStoragesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomFilesValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this._filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this._filesDataStore
    }

    get roomStoragesDataStore() {
        const Constructor = require('@/app/datastores/RoomStoragesDataStore');
        this._roomStoragesDataStore ||= new Constructor({ localizer: this.localizer });
        return this._roomStoragesDataStore;
    }

    get s3Adaptor() {
        this.$s3Adaptor ||= new S3Adaptor({ localizer: this.localizer });
        return this.$s3Adaptor;
    }

    async create(attributes: FileAttributes, { user }: { user: UserEntity }) {
        const room = await RoomEntity.findOne({
            where: {
                id: attributes.roomId,
            },
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild?.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                memberId: member?.id,
                roomId: room?.id,
            },
        });
        await Promise.all([
            this.validation.checkUserState(user),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkRoomState(room),
            this.validation.checkRoomMemberState(roomMember),
        ]);
        attributes.id = attributes.id || uuidv4();
        attributes.providerKey = File.generateRoomFileKey(room!.id!, { id: attributes.id, extension: FileExtension(attributes.contentType) });

        if (attributes.type === FileType.Folder) {
            throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
        } else if (attributes.type === FileType.Album || attributes.contentType === ContentType.album) {
            const file = await this.filesDataStore.createAlbum(attributes);
            // file.accessType = FileAccessType.PRIVATE;
            const {
                filing,
                roomFile,
            } = await this.roomStoragesDataStore.findOrCreateFile({ room: room!, file, showInGuild: attributes.showInGuild ?? false });
            return {
                file,
                filing,
                roomFile,
            };
        } else if (attributes.type === FileType.Playlist || attributes.contentType === ContentType.playlist) {
            const file = await this.filesDataStore.createPlaylist(attributes);
            // file.accessType = FileAccessType.PRIVATE;
            const {
                filing,
                roomFile,
            } = await this.roomStoragesDataStore.findOrCreateFile({ room: room!, file, showInGuild: attributes.showInGuild ?? false });
            return {
                file,
                filing,
                roomFile,
            };
        } else {
            const {
                file,
                presignedURL,
            } = await this.filesDataStore.presign(attributes);
            // file.accessType = FileAccessType.PRIVATE;
            const result = await file.save();
            const {
                filing,
                roomFile,
            } = await this.roomStoragesDataStore.findOrCreateFile({ room: room!, file, showInGuild: attributes.showInGuild ?? false });
            return {
                file: result,
                filing,
                roomFile,
                presignedURL,
            };
        }
    }

    async gets(
        roomId: string,
        { user, count, offset, types, statuses }: { user: UserEntity, count?: number, offset?: number, types?: FileType[], statuses?: FileStatus[] }
    ): Promise<{
        filings: FilingEntity[],
        guild: GuildEntity,
        member: MemberEntity,
        paginatable: boolean,
    }> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        });
        await this.validation.checkRoomState(room);

        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
            },
        });
        await this.validation.checkGuildState(guild);

        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild?.id,
            },
        });
        this.validation.checkMemberState(member);

        const roomMember = await RoomMemberEntity.findOne({
            where: {
                memberId: member?.id,
                roomId: room?.id,
            },
        });

        const storage = await this.roomStoragesDataStore.findOrCreate(room!);
        const folder = await storage.file;

        await Promise.all([
            this.validation.checkRoomMemberState(roomMember),
            this.validation.checkFolderState(folder),
        ]);

        if (!statuses || statuses.length === 0) statuses = [FileStatus.UPLOADED];

        const filings = await FilingEntity
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.file', 'files', types && types.length > 0 ? 'files.status IN (:...statuses) AND files.type IN (:...types)' : 'files.status IN (:...statuses)', { statuses, types })
            .where('filings.folder_id = :folderId', { folderId: storage.fileId })
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy('filings.index', 'DESC')
            .getMany();

        const last = !!count && filings.length >= count && filings.pop();

        return {
            guild: guild!,
            member: member!,
            filings,
            paginatable: !!last,
        };
    }
}

module.exports = RoomFilesDataStore;