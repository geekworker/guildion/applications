import { AccountStatus, Localizer, UserAttributes, UserStatus } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type UsersValidation from '@/app/validations/UsersValidation';
import type FilesDataStore from "./FilesDataStore";
import { AccountEntity, FileEntity, UserEntity } from "@guildion/db";

export default class UsersDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: UsersValidation;
    private $filesDataStore!: FilesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/UsersValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this.$filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$filesDataStore
    }

    async update(attributes: Partial<UserAttributes>, { user }: { user: UserEntity }): Promise<UserEntity> {
        const exist = await UserEntity.findOne({
            where: {
                id: user.id,
            }
        });
        await this.validation.checkUserState(exist);
        if (attributes.profileId !== undefined) {
            const profile = await FileEntity.findOne({
                where: {
                    id: attributes.profileId,
                },
            });
            await this.validation.checkProfileFileState(profile);
            exist!.profileId = attributes.profileId;
        };
        if (attributes.displayName !== undefined) exist!.displayName = attributes.displayName;
        if (attributes.description !== undefined) exist!.description = attributes.description;
        if (attributes.languageCode !== undefined) exist!.languageCode = attributes.languageCode;
        if (attributes.countryCode !== undefined) exist!.countryCode = attributes.countryCode;
        if (attributes.timezone !== undefined) exist!.timezone = attributes.timezone;
        return exist!.save();
    }

    async destroy(userId: string, { user }: { user: UserEntity }): Promise<void> {
        const exist = userId == user.id ? await UserEntity.findOne({
            where: {
                id: userId,
            }
        }) : undefined;
        await this.validation.checkUserGuildOwners(user);
        await this.validation.checkUserState(user);

        const account = await AccountEntity.findOne({
            where: {
                userId: userId,
            }
        });

        exist!.status = UserStatus.DELETING;
        exist!.deletedAt = new Date();

        account!.status = AccountStatus.DELETING;
        account!.deletedAt = new Date();

        await account?.save();

        return;
    }
}

module.exports = UsersDataStore;