import { Localizer, MemberStatus } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type MemberConnectionsValidation from '@/app/validations/MemberConnectionsValidation';
import { MemberConnectionEntity, ConnectionEntity, UserEntity, MemberEntity, GuildEntity, DeviceEntity, DeviceConnectionEntity } from "@guildion/db";
import Bluebird from "bluebird";
import { In } from "typeorm";

export default class MemberConnectionsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: MemberConnectionsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/MemberConnectionsValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async create(id: string,{ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<{
        memberConnection: MemberConnectionEntity,
        connection: ConnectionEntity,
    }> {
        const deviceConnections = await DeviceConnectionEntity
            .getRepository()
            .createQueryBuilder('device_connections')
            .innerJoin('device_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .where('device_connections.device_id = :deviceId', { deviceId: device.id })
            .orderBy('device_connections.id', 'DESC')
            .getMany();
        const deviceConnection = deviceConnections.shift();
        const connection = await deviceConnection?.connection;
        const guild = await GuildEntity.findOne({
            where: {
                id,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: id,
                userId: user.id,
            },
        });
        await this.validation.checkConnectionState(connection);
        await this.validation.checkUserState(user);
        await this.validation.checkGuildState(guild);
        await this.validation.checkMemberState(member);

        const exist = await MemberConnectionEntity.findOne({
            where: {
                connectionId: connection!.id,
                memberId: member!.id,
                isConnecting: true
            },
        });

        if (exist) {
            return {
                connection: connection!,
                memberConnection: exist,
            };
        } else {
            await this.destroyAll({ user, device });
            const memberConnection = await MemberConnectionEntity.create({
                connectionId: connection!.id,
                memberId: member!.id,
                isConnecting: true,
                connectedAt: new Date(),
            }).save();
            return {
                connection: connection!,
                memberConnection,
            };
        }
    }

    async destroyAll({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<boolean> {
        const deviceConnections = await DeviceConnectionEntity
            .getRepository()
            .createQueryBuilder('device_connections')
            .innerJoin('device_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .where('device_connections.device_id = :deviceId', { deviceId: device.id })
            .orderBy('device_connections.id', 'DESC')
            .getMany();
        const connections = await Bluebird.all(
            deviceConnections.map(dc => dc.connection)
        );
        await this.validation.checkUserState(user);
        const exists = connections.length > 0 ? await MemberConnectionEntity
            .getRepository()
            .createQueryBuilder('member_connections')
            .where('member_connections.is_connecting = :isConnecting', { isConnecting: true })
            .andWhere('member_connections.connection_id IN (:...connectionIds)', { connectionIds: connections.map(c => c.id) })
            .getMany() : [];
        await Promise.all(
            exists.map(c => c.disconnect())
        );
        return true;
    }

    async gets(id: string, { count, offset, user }: { count?: number, offset?: number, user: UserEntity }): Promise<{ guild:  GuildEntity, memberConnections: MemberConnectionEntity[], paginatable: boolean }> {
        const guild = await GuildEntity.findOne({
            where: {
                id,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: id,
                userId: user.id,
            },
        });
        await this.validation.checkUserState(user);
        await this.validation.checkGuildState(guild);
        await this.validation.checkMemberState(member);

        const memberConnections = await MemberConnectionEntity
            .getRepository()
            .createQueryBuilder('member_connections')
            .innerJoin('member_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .innerJoin('member_connections.member', 'member', "member.guild_id = :guildId AND member.status NOT IN (:...statuses)", { guildId: id, statuses: [MemberStatus.BANNED, MemberStatus.LOCKED, MemberStatus.DELETED] })
            .distinctOn(['member_connections.id'])
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy('member_connections.id', 'ASC')
            .groupBy('member_connections.id')
            .getMany()
        
        const last = memberConnections.pop();

        return {
            memberConnections,
            guild: guild!,
            paginatable: !!last,
        }
    }
}

module.exports = MemberConnectionsDataStore;