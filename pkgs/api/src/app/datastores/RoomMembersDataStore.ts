import { Localizer, MemberAttributes, RoomMemberStatus } from "@guildion/core";
import { RoomMemberEntity, MemberEntity, RoomEntity, RoleEntity, MemberRoleEntity, UserEntity, GuildEntity, RoomMemberConnectionEntity, connection } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomMembersValidation from '@/app/validations/RoomMembersValidation';
import Bluebird from "bluebird";
import type MembersDataStore from "./MembersDataStore";
import type MemberRolesDataStore from "./MemberRolesDataStore";

export default class RoomMembersDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RoomMembersValidation;
    private $membersDataStore!: MembersDataStore;
    private $memberRolesDataStore!: MemberRolesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomMembersValidation')
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get membersDataStore() {
        const Constructor = require('./MembersDataStore')
        this.$membersDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$membersDataStore
    }

    get memberRolesDataStore() {
        const Constructor = require('./MemberRolesDataStore')
        this.$memberRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$memberRolesDataStore
    }

    async gets(
        roomId: string,
        { user, count, offset }:
        { user: UserEntity, count?: number, offset?: number }
    ): Promise<{
        room: RoomEntity,
        member: MemberEntity,
        roomMembers: RoomMemberEntity[],
        paginatable: boolean,
    }> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        });
        await this.validation.checkRoomState(room);
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkMemberRoomViewable({ member: member!, room: room! });

        const roomMembers = await RoomMemberEntity.find({
            where: {
                roomId: room?.id,
                status: RoomMemberStatus.CREATED,
            },
            take: count ? count + 1 : undefined,
            skip: offset,
            order: { createdAt:  'DESC' },
        });

        const last = !!count && roomMembers.length >= count && roomMembers.pop();

        return {
            room: room!,
            member: member!,
            roomMembers,
            paginatable: !!last,
        }
    }

    async getConnectings(
        roomId: string,
        { user, count, offset, gtAt, ltAt }:
        { user: UserEntity, count?: number, offset?: number, gtAt?: Date, ltAt?: Date }
    ): Promise<{
        room: RoomEntity,
        member: MemberEntity,
        roomMembers: RoomMemberEntity[],
        paginatable: boolean,
    }> {
        const room = await RoomEntity.findOne({
            where: {
                id: roomId,
            },
        });
        await this.validation.checkRoomState(room);
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkMemberRoomViewable({ member: member!, room: room! });

        const results = await connection
            .createQueryBuilder()
            .select("*")
            .from(subQuery =>
                subQuery
                    .from(RoomMemberEntity, 'room_members')
                    .select("*")
                    .distinctOn(['room_member_connections.connection_id'])
                    .innerJoinAndSelect(
                        'room_members.roomMemberConnections',
                        'room_member_connections',
                        gtAt ?
                        `room_member_connections.created_at >= :basedAt AND 
                        (
                            room_member_connections.disconnected_at >= :basedAt OR 
                            room_member_connections.is_connecting = :isConnecting
                        )` :
                        `room_member_connections.created_at <= :basedAt AND 
                        (
                            room_member_connections.disconnected_at >= :basedAt OR 
                            room_member_connections.is_connecting = :isConnecting
                        )`,
                        {
                            basedAt: gtAt?.toISOString() ?? ltAt?.toISOString() ?? new Date()?.toISOString(),
                            isConnecting: true,
                        }
                    )
                    .where('room_members.room_id = :roomId', { roomId: room!.id })
                    .andWhere('room_members.status = :status', { status: RoomMemberStatus.CREATED })
                    .orderBy('room_member_connections.created_at', 'DESC')
                    .orderBy('room_member_connections.connection_id', 'DESC'),
                "rooms"
            )
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy('room_member_connections_created_at', 'DESC')
            .getRawMany();

        const roomMembers = await Promise.all(
            results.map((result) => RoomMemberEntity.findOneOrFail({
                where: {
                    id: result.room_member_id,
                },
            }))
        );
        
        const last = !!count && roomMembers.length >= count && roomMembers.pop();

        return {
            room: room!,
            member: member!,
            roomMembers,
            paginatable: !!last,
        };
    }

    async create({ member, room }: { member: MemberEntity, room: RoomEntity }): Promise<RoomMemberEntity> {
        await this.validation.checkMemberState(member);
        await this.validation.checkRoomState(room);

        const exist = await RoomMemberEntity.findOne({
            where: {
                roomId: room.id,
                memberId: member.id,
            },
        });
        if (exist) {
            return exist.activateIfPossible();
        };

        const roomMember = await RoomMemberEntity.create({
            roomId: room.id,
            memberId: member.id,
            status: RoomMemberStatus.CREATED,
        }).save();

        return roomMember;
    }

    async update(
        attributes: Partial<MemberAttributes> & { id: string, roomId: string },
        { member, room }: { member?: MemberEntity | null, room?: RoomEntity | null }
    ): Promise<RoomMemberEntity> {
        member ||= await MemberEntity.findOne({
            where: {
                id: attributes.id,
            }
        });
        room ||= await RoomEntity.findOne({
            where: {
                id: attributes.roomId,
            }
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkRoomState(room);

        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: room!.id,
                memberId: member!.id,
            },
        });
        await this.validation.checkRoomMemberState(roomMember);

        if (attributes.isMute !== undefined) roomMember!.isMute = attributes.isMute;
        if (attributes.isMuteMentions !== undefined) roomMember!.isMuteMentions = attributes.isMuteMentions;

        return await roomMember!.save();
    }

    async destroy(id: string): Promise<void> {
        const exist = await RoomMemberEntity.findOne({
            where: {
                id,
            },
        });
        await exist?.delete();
    }

    async creates({ members, room }: { members: MemberEntity[], room: RoomEntity }): Promise<RoomMemberEntity[]> {
        return await Bluebird.map(
            members,
            (member) => this.create({ member, room }),
            { concurrency: 10 },
        );
    }

    async createsByRole({ role, room }: { role: RoleEntity, room: RoomEntity }): Promise<RoomMemberEntity[]> {
        const roleMembers = await MemberRoleEntity.find({
            where: {
                roleId: role.id,
            },
        });

        const members = await Bluebird.map(
            roleMembers,
            (roleMember) => roleMember.member,
            { concurrency: 10 }
        );

        return await this.creates({ members, room });
    }

    async createsByRoles({ roles, room }: { roles: RoleEntity[], room: RoomEntity }): Promise<RoomMemberEntity[]> {
        const deepMembers = await Bluebird.map(
            roles,
            (role) => this.createsByRole({ role, room }),
            { concurrency: 10 }
        );
        return deepMembers.flat();
    }

    async findOrCreates({ roles, members, room, owner }: { roles: RoleEntity[], members: MemberEntity[], room: RoomEntity, owner?: MemberEntity | undefined | null }): Promise<RoomMemberEntity[]> {
        owner = owner || await room.owner;

        const roomRoleMembers = await RoomMemberEntity.find({
            where: {
                roomId: room.id,
            },
        });
        const roomMembers: RoomMemberEntity[] = [];
        roomMembers.push(await this.create({ member: owner!, room }));

        if (members.length > 0) {
            roomMembers.concat(
                await this.creates({ 
                    room,
                    members,
                })
            )
        }

        if (roles.length > 0) {
            roomMembers.concat(
                await this.createsByRoles({
                    room,
                    roles,
                })
            )
        }
        
        const roomMemberIds = roomMembers.map(rm => rm.id!);
        const shouldDeletes = roomRoleMembers.filter(rrm => !roomMemberIds.includes(rrm.id));

        await Bluebird.map(
            shouldDeletes,
            rrm => this.destroy(rrm.id),
            { concurrency: 10 }
        );

        return roomMembers;
    }

    async join({
        user,
        room,
    }: {
        user: UserEntity,
        room: RoomEntity,
    }): Promise<RoomMemberEntity> {
        const guild = await GuildEntity.findOne({
            where: {
                id: room.guildId,
            }
        });
        await Promise.all([
            this.validation.checkGuildJoinState(guild),
            this.validation.checkRoomJoinState(room),
            this.validation.checkUserState(user),
            this.validation.checkRoomState(room),
            this.membersDataStore.validation.checkGuildBlockState({ guild: guild!, user }),
        ]);
        let member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            }
        });
        if (!member) {
            member = await this.membersDataStore.join({
                user,
                guild: guild!,
            });
        }
        await this.validation.checkMemberState(member);
        const roomMember = await this.create({
            member: member!,
            room: room!,
        });
        return roomMember;
    }

    async leave({
        user,
        room,
    }: {
        user: UserEntity,
        room: RoomEntity,
    }): Promise<void> {
        const guild = await GuildEntity.findOne({
            where: {
                id: room.guildId,
            }
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            }
        });
        const exist = await RoomMemberEntity.findOne({
            where: {
                roomId: room.id,
                memberId: member?.id,
            }
        });
        await Promise.all([
            this.validation.checkGuildState(guild),
            this.validation.checkRoomState(room),
            this.validation.checkUserState(user),
            this.validation.checkMemberState(member),
            exist && this.validation.checkRoomMemberIsNotOwner(exist),
        ]);
        await exist?.delete();
    }

    async kick(roomMember: RoomMemberEntity, { user }: { user: UserEntity }): Promise<void> {
        const room = await roomMember.room;
        const member = await roomMember.member;
        const sender = await MemberEntity.findOne({
            where: {
                guildId: room.guildId,
                userId: user.id,
            }
        });
        await Promise.all([
            this.validation.checkMemberRoomManagable({ member: sender!, room: room! }),
            this.validation.checkUserState(user),
            this.validation.checkRoomState(room),
            this.validation.checkMemberIsNotOwner(member),
            this.validation.checkRoomMemberIsNotOwner(roomMember),
        ]);
        await roomMember?.delete();
    }
}

module.exports = RoomMembersDataStore;