import { AppError, FileAttributes, FileStatus, Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type FilingsValidation from '@/app/validations/FilingsValidation';
import type FilesDataStore from '@/app/datastores/FilesDataStore';
import { FileEntity, FilingEntity, UserEntity } from '@guildion/db';
import Bluebird from 'bluebird';

export default class FilingsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: FilingsValidation;
    private _filesDataStore!: FilesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/FilingsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    get filesDataStore() {
        const Constructor = require('@/app/datastores/FilesDataStore');
        this._filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this._filesDataStore;
    }

    async gets(
        folderId: string,
        { user, count, offset, isRandom }: { user: UserEntity, count?: number, offset?: number, isRandom?: boolean }
    ): Promise<{
        filings: FilingEntity[],
        paginatable: boolean,
    }> {
        const folder = await FileEntity.findOne({
            where: {
                id: folderId,
            },
        });
        await this.validation.checkFolderState(folder);
        await this.validation.checkFileViewable({ file: folder!, user });

        const filings = await FilingEntity
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.file', 'files', 'files.status IN (:...statuses)', { statuses: [FileStatus.UPLOADED] })
            .where('filings.folder_id = :folderId', { folderId })
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy(isRandom ? 'filings.random_index' : 'filings.index', 'DESC')
            .getMany();

        const last = !!count && filings.length >= count && filings.pop();

        return {
            filings,
            paginatable: !!last,
        };
    }

    async create({ file, folder, user, skipFolderSync }: { file: FileEntity, folder: FileEntity, user: UserEntity, skipFolderSync?: boolean }): Promise<FilingEntity> {
        if (folder.isSystem) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        await this.validation.checkFolderState(folder);
        await this.validation.checkFileViewable({ file, user });
        await this.validation.checkFileViewable({ file: folder, user });
        await this.validation.checkFilingFormat({ file, folder });
        const filing = await FilingEntity.create({
            fileId: file.id,
            folderId: folder.id,
        }).save();
        if (!skipFolderSync) await this.filesDataStore.syncFolderMeta(folder);
        return filing;
    }

    async creates({ fileIds, folderId, user }: { fileIds: string[], folderId: string, user: UserEntity }): Promise<FilingEntity[]> {
        const folder = await FileEntity.findOne({
            where: {
                id: folderId,
            },
        });
        await this.validation.checkFolderState(folder);
        const filings = await Bluebird.map(
            fileIds,
            async fileId => {
                const file = await FileEntity.findOne({
                    where: {
                        id: fileId,
                    },
                });
                return this.create({ file: file!, folder: folder!, user, skipFolderSync: true });
            },
            { concurrency: 10 }
        );
        await this.filesDataStore.syncFolderMeta(folder!);
        return filings;
    }

    async update(attributes: Partial<FileAttributes> & { filingId: string }, { user, skipFolderSync }: { user: UserEntity, skipFolderSync?: boolean }): Promise<FilingEntity> {
        const filing = await FilingEntity.findOne({
            id: attributes.filingId,
        });
        if (!filing) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        const folder = await filing.folder;
        if (folder.isSystem) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        await this.validation.checkFolderState(folder);
        await this.validation.checkFileViewable({ file: folder, user });
        if (attributes.index !== undefined && attributes.index !== null) filing.index = attributes.index;
        if (attributes.randomIndex !== undefined && attributes.randomIndex !== null) filing.randomIndex = attributes.index;
        if (!skipFolderSync) await this.filesDataStore.syncFolderMeta(folder);
        return filing.save();
    }

    async updates(files: (Partial<FileAttributes> & { filingId: string })[], { folderId, user }: { folderId: string, user: UserEntity }): Promise<FilingEntity[]> {
        const folder = await FileEntity.findOne({
            where: {
                id: folderId,
            },
        });
        await this.validation.checkFolderState(folder);
        const filings = await Bluebird.map(
            files,
            async attr => this.update(attr, { user, skipFolderSync: true }),
            { concurrency: 10 }
        );
        await this.filesDataStore.syncFolderMeta(folder!);
        return filings;
    }

    async destroy(filingId: string, { user, skipFolderSync }: { user: UserEntity, skipFolderSync?: boolean }): Promise<void> {
        const filing = await FilingEntity.findOne({
            id: filingId,
        });
        if (!filing) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        const folder = await filing.folder;
        if (folder.isSystem) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        await this.validation.checkFolderState(folder);
        await this.validation.checkFileViewable({ file: folder, user });
        await FilingEntity.remove(filing);
        if (!skipFolderSync) await this.filesDataStore.syncFolderMeta(folder);
    }

    async destroys({ filingIds, user }: { filingIds: string[], user: UserEntity }): Promise<void> {
        await Bluebird.map(
            filingIds,
            async filingId => this.destroy(filingId, { user, skipFolderSync: false }),
            { concurrency: 10 }
        );
    }
}

module.exports = FilingsDataStore;