import { AppError, GuildType, Localizer, MemberAttributes, MemberStatus } from "@guildion/core";
import { UserEntity, GuildEntity, MemberEntity, FileEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type MembersValidation from '@/app/validations/MembersValidation';
import type MemberRolesDataStore from "./MemberRolesDataStore";
import type FilesDataStore from "./FilesDataStore";
import Bluebird from "bluebird";

export default class MembersDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: MembersValidation;
    private $memberRolesDataStore!: MemberRolesDataStore;
    private $filesDataStore!: FilesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/MembersValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get memberRolesDataStore() {
        const Constructor = require('./MemberRolesDataStore');
        this.$memberRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$memberRolesDataStore
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this.$filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$filesDataStore
    }

    async isMember({ user, guild }: { user: UserEntity, guild: GuildEntity }): Promise<boolean> {
        await this.validation.checkGuildState(guild);
        await this.validation.checkUserState(user);
        const member = await MemberEntity.findOne({ 
            where: { 
                guildId: guild.id,
                userId: user.id,
            } 
        });
        await this.validation.checkMemberState(member);
        return !!member
    }

    async gets(guildId: string, { count, offset, user }: { count?: number, offset?: number, user: UserEntity }): Promise<{ members: MemberEntity[], paginatable: boolean }> {
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            }
        });
        await this.validation.checkGuildState(guild);
        if (!guild || (guild?.type == GuildType.PRIVATE && !(await this.isMember({ guild, user })))) {
            return { members: [], paginatable: false };
        }
        const members = await MemberEntity.find({
            where: {
                guildId: guild?.id,
                status: MemberStatus.CREATED,
            },
            take: count ? count + 1 : undefined,
            skip: offset,
            order: { id: 'DESC' },
        });

        const last = !!count && members.length >= count && members.pop();

        return {
            members,
            paginatable: !!last,
        };
    }

    async findOrCreate({ user, guild }: { user: UserEntity, guild: GuildEntity }): Promise<MemberEntity> {
        await this.validation.checkUserState(user);
        await this.validation.checkGuildState(guild);

        const exist = await MemberEntity.findOne({
            where: {
                guildId: guild.id,
                userId: user.id,
            },
        });
        if (exist) {
           return exist.activateIfPossible();
        };

        const profile = await FileEntity.findOne({
            where: {
                id: user.profileId,
            },
        });
        await this.validation.checkProfileFileState(profile);

        const member = await MemberEntity.create({
            guildId: guild.id,
            userId: user.id,
            profileId: profile!.id,
            displayName: user.displayName,
            description: user.description,
            index: await this.getLastIndex(user),
            status: MemberStatus.CREATED,
        }).save();
        
        await this.memberRolesDataStore.makeMembers(member);
        await this.refreshIndex(user.id);

        return member;
    }

    async update(attributes: Partial<MemberAttributes> & { id: string }, { user }: { user: UserEntity }): Promise<MemberEntity> {
        const exist = await MemberEntity.findOne({
            where: {
                id: attributes.id,
            },
        });
        await this.validation.checkMemberState(exist);
        await this.validation.checkUserState(user);
        const guild = await GuildEntity.findOne({
            where: {
                id: exist?.guildId,
            }
        });
        await this.validation.checkGuildState(guild);

        const profile = attributes.profileId ? await FileEntity.findOne({
            where: {
                id: attributes.profileId,
            },
        }) : undefined;
        await this.validation.checkProfileFileState(profile);

        if (attributes.profileId !== undefined) exist!.profileId = attributes.profileId;
        if (attributes.displayName !== undefined) exist!.displayName = attributes.displayName;
        if (attributes.description !== undefined) exist!.description = attributes.description;
        if (attributes.index !== undefined) exist!.index = attributes.index;
        if (attributes.isMute !== undefined) exist!.isMute = attributes.isMute;
        if (attributes.isMuteMentions !== undefined) exist!.isMuteMentions = attributes.isMuteMentions;
        const member = await exist!.save();
        await this.refreshIndex(user.id);

        return member;
    }

    async destroy(id: string, { force = false, user }: { force?: boolean, user: UserEntity }): Promise<void> {
        const member = await MemberEntity.findOne({
            where: {
                id,
            },
        });
        await this.validation.checkMemberState(member);
        if (!force) {
            await this.validation.checkUserState(user);
            const guild = await GuildEntity.findOne({
                where: {
                    id: member?.guildId,
                }
            });
            await this.validation.checkGuildState(guild);
            const exist = await MemberEntity.findOne({
                where: {
                    guildId: guild!.id,
                    userId: user.id,
                },
            });
            await this.validation.checkMemberState(exist);
            if (member?.id != exist!.id) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        }
        await member?.delete();
        await this.refreshIndex(user.id);
    }

    async getLastIndex(user: UserEntity): Promise<number> {
        const count = await MemberEntity.count({
            where: {
                userId: user.id,
            },
        });
        return count;
    }

    async refreshIndex(userId: string): Promise<void> {
        const members = await MemberEntity.find({
            where: {
                userId,
            },
            order: { 'index': 'ASC' },
        });
        await Bluebird.map(
            members,
            (member, i) => {
                member.index = i;
                return member.save();
            },
            { concurrency: 10 }
        );
    }

    async join({
        user,
        guild,
    }: {
        user: UserEntity,
        guild: GuildEntity,
    }): Promise<MemberEntity> {
        await Promise.all([
            this.validation.checkGuildJoinState(guild),
            this.validation.checkUserState(user),
            this.validation.checkGuildBlockState({ guild, user }),
        ]);
        const exist = await MemberEntity.findOne({
            where: {
                guildId: guild.id,
                userId: user.id,
            }
        });
        if (exist) throw new AppError(this.localizer.dictionary.error.guild.alreadyJoinedError);
        const member = await this.findOrCreate({ user, guild });
        return member;
    }

    async leave({
        user,
        guild,
    }: {
        user: UserEntity,
        guild: GuildEntity,
    }): Promise<void> {
        const exist = await MemberEntity.findOne({
            where: {
                guildId: guild.id,
                userId: user.id,
            }
        });
        await Promise.all([
            this.validation.checkGuildState(guild),
            this.validation.checkUserState(user),
            this.validation.checkMemberState(exist),
        ]);
        await exist?.delete();
    }

    async kick(id: string, { user }: { user: UserEntity }): Promise<void> {
        const member = await MemberEntity.findOne({
            where: {
                id,
            }
        });
        const sender = await MemberEntity.findOne({
            where: {
                guildId: member!.guildId,
                userId: user.id,
            }
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: member!.guildId,
            }
        });
        await Promise.all([
            this.validation.checkMemberManagable(sender!),
            this.validation.checkUserState(user),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberIsNotOwner(member!),
        ]);
        await member?.delete();
    }
}

module.exports = MembersDataStore;