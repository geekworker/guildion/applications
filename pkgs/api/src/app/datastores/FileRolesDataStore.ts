import { Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type FileRolesValidation from '@/app/validations/FileRolesValidation';
import { FileEntity, FileRoleEntity, RoleEntity } from '@guildion/db';

export default class FileRolesDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: FileRolesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/FileRolesValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async findOrCreate({ file, role }: { file: FileEntity, role: RoleEntity }): Promise<FileRoleEntity> {
        const exist = await FileRoleEntity.findOne({
            where: {
                fileId: file.id,
                roleId: role.id,
            },
        });
        if (exist) return exist;
        return await FileRoleEntity.create({
            fileId: file.id,
            roleId: role.id,
        }).save();
    }

    async findOrUpdates({ file, roles }: { file: FileEntity, roles: RoleEntity[] }): Promise<void> {
        const exists = await FileRoleEntity.find({
            where: {
                fileId: file.id,
            },
        });
        await Promise.all(
            roles
                .filter(role => !exists.map(e => e.roleId).includes(role.id))
                .map(role => this.findOrCreate({ file, role }))
        );
        await Promise.all(
            exists
                .filter(exist => !roles.map(e => e.id).includes(exist.roleId))
                .map(exist => FileRoleEntity.remove(exist))
        );
    }
}

module.exports = FileRolesDataStore;