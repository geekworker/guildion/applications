import { AppError, ContentType, File, FileAccessType, FileAttributes, FileExtension, FileStatus, FileType, Localizer, RoomStatus } from '@guildion/core';
import { FileEntity, FilingEntity, GuildEntity, GuildFileEntity, MemberEntity, RoomEntity, RoomFileEntity, UserEntity } from '@guildion/db';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type GuildFilesValidation from '@/app/validations/GuildFilesValidation';
import type FilesDataStore from '@/app/datastores/FilesDataStore';
import type GuildStoragesDataStore from '@/app/datastores/GuildStoragesDataStore';
import S3Adaptor from "@/app/modules/aws/s3";
import { v4 as uuidv4 } from 'uuid';

export default class GuildFilesDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: GuildFilesValidation;
    private _filesDataStore!: FilesDataStore;
    private _guildStoragesDataStore!: GuildStoragesDataStore;
    private $s3Adaptor!: S3Adaptor;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/GuildFilesValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation;
    }

    get filesDataStore() {
        const Constructor = require('@/app/datastores/FilesDataStore');
        this._filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this._filesDataStore;
    }

    get guildStoragesDataStore() {
        const Constructor = require('@/app/datastores/GuildStoragesDataStore');
        this._guildStoragesDataStore ||= new Constructor({ localizer: this.localizer });
        return this._guildStoragesDataStore;
    }

    get s3Adaptor() {
        this.$s3Adaptor ||= new S3Adaptor({ localizer: this.localizer });
        return this.$s3Adaptor;
    }

    async create(attributes: FileAttributes, { user }: { user: UserEntity }) {
        const guild = await GuildEntity.findOne({
            where: {
                id: attributes.guildId,
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild?.id,
            },
        });
        await Promise.all([
            this.validation.checkUserState(user),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkMemberManagable(member!),
        ]);
        attributes.id = attributes.id || uuidv4();
        attributes.providerKey = File.generateGuildFileKey(guild!.id!, { id: attributes.id, extension: FileExtension(attributes.contentType) });
        if (attributes.type === FileType.Folder) {
            throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
        } else if (attributes.type === FileType.Album || attributes.contentType === ContentType.album) {
            const file = await this.filesDataStore.createAlbum(attributes);
            // file.accessType = FileAccessType.PRIVATE;
            const filing = await this.guildStoragesDataStore.findOrCreateFile(guild!, file);
            return {
                file,
                filing,
            };
        } else if (attributes.type === FileType.Playlist || attributes.contentType === ContentType.playlist) {
            const file = await this.filesDataStore.createPlaylist(attributes);
            // file.accessType = FileAccessType.PRIVATE;
            const filing = await this.guildStoragesDataStore.findOrCreateFile(guild!, file);
            return {
                file,
                filing,
            };
        } else {
            const {
                file,
                presignedURL,
            } = await this.filesDataStore.presign(attributes);
            // file.accessType = FileAccessType.PRIVATE;
            const result = await file.save();
            const filing = await this.guildStoragesDataStore.findOrCreateFile(guild!, file);
            return {
                file: result,
                filing,
                presignedURL,
            };
        }
    }

    async gets(
        guildId: string,
        { user, count, offset, types, statuses }: { user: UserEntity, count?: number, offset?: number, types?: FileType[], statuses?: FileStatus[] }
    ): Promise<{
        filings: FilingEntity[],
        guild: GuildEntity,
        member: MemberEntity,
        paginatable: boolean,
    }> {
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            },
        });
        await this.validation.checkGuildState(guild);

        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild?.id,
            },
        });

        const storage = await this.guildStoragesDataStore.findOrCreate(guild!);
        const folder = await storage.file;

        await Promise.all([
            this.validation.checkMemberState(member),
            this.validation.checkFolderState(folder),
        ]);

        if (!statuses || statuses.length === 0) statuses = [FileStatus.UPLOADED];

        const filings = await FilingEntity
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.file', 'files', types && types.length > 0 ? 'files.status IN (:...statuses) AND files.type IN (:...types)' : 'files.status IN (:...statuses)', { statuses, types })
            .where('filings.folder_id = :folderId', { folderId: storage.fileId })
            .take(count ? count + 1 : undefined)
            .skip(offset)
            .orderBy('filings.index', 'DESC')
            .getMany();

        const last = !!count && filings.length >= count && filings.pop();

        return {
            guild: guild!,
            member: member!,
            filings,
            paginatable: !!last,
        };
    }

    async getsRecently(
        guildId: string,
        { user, count, offset, roomId, types, statuses }: { user: UserEntity, count?: number, offset?: number, roomId?: string, types?: FileType[], statuses?: FileStatus[] }
    ): Promise<{
        filings: FilingEntity[],
        guild: GuildEntity,
        member: MemberEntity,
        paginatable: boolean,
    }> {
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            },
        });
        await this.validation.checkGuildState(guild);

        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild?.id,
            },
        });

        await this.validation.checkMemberState(member);

        if (!statuses || statuses.length === 0) statuses = [FileStatus.UPLOADED];

        const filings = await FilingEntity
            .getRepository()
            .createQueryBuilder('filings')
            .innerJoinAndSelect('filings.folder', 'folder', 'folder.status = :status', { status: FileStatus.UPLOADED })
            .innerJoinAndSelect('filings.file', 'files', types && types.length > 0 ? 'files.status IN (:...statuses) AND files.type IN (:...types)' : 'files.status IN (:...statuses)', { statuses, types })
            .where(qb => {
                const subQuery = qb.subQuery()
                    .select("room_files.file_id")
                    .from(RoomFileEntity, "room_files")
                    .innerJoin(
                        'room_files.room',
                        'room',
                        'room.status NOT IN (:...roomStatuses) AND room.guild_id = :guildId',
                        { guildId, roomStatuses: [RoomStatus.BANNED, RoomStatus.LOCKED, RoomStatus.DELETED, RoomStatus.DELETING] }
                    )
                    .where(roomId ? 'room_files.room_id = :roomId OR room_files.show_in_guild = :showInGuild' : 'room_files.show_in_guild = :showInGuild', { roomId, showInGuild: true })
                    .getQuery();
                return "filings.file_id IN " + subQuery;
            })
            .orWhere(qb => {
                const subQuery = qb.subQuery()
                    .select("guild_files.file_id")
                    .from(GuildFileEntity, "guild_files")
                    .where('guild_files.guild_id = :guildId', { guildId })
                    .getQuery();
                return "filings.file_id IN " + subQuery;
            })
            .orderBy('filings.created_at', 'DESC')
            .limit(count ? count + 1 : undefined)
            .offset(offset)
            .getMany();

        const last = !!count && filings.length >= count && filings.pop();

        return {
            guild: guild!,
            member: member!,
            filings,
            paginatable: !!last,
        };
    }
}

module.exports = GuildFilesDataStore;