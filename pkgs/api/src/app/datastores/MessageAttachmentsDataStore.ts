import { Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type MessageAttachmentsValidation from '@/app/validations/MessageAttachmentsValidation';
import { MessageEntity, FileEntity, MessageAttachmentEntity } from '@guildion/db';

export default class MessageAttachmentsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: MessageAttachmentsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/MessageAttachmentsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async findOrCreate({ message, file }: { message: MessageEntity, file: FileEntity }): Promise<MessageAttachmentEntity> {
        await this.validation.checkMessageState(message);
        await this.validation.checkFileState(file);
        const exist = await MessageAttachmentEntity.findOne({
            where: {
                messageId: message.id,
                fileId: file.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            return MessageAttachmentEntity.create({
                messageId: message.id,
                fileId: file.id,
            }).save();
        }
    }

    async findOrCreates({ message, files }: { message: MessageEntity, files: FileEntity[] }): Promise<MessageAttachmentEntity[]> {
        await this.validation.checkMessageState(message);
        const attachments: MessageAttachmentEntity[] = []
        for (const file of files) {
            const attachment = await this.findOrCreate({ message, file });
            attachments.push(attachment);
        }
        return attachments;
    }

    async findOrUpdates({ message, files }: { message: MessageEntity, files: FileEntity[] }): Promise<MessageAttachmentEntity[]> {
        const attachments = await MessageAttachmentEntity.find({
            where: {
                messageId: message?.id,
            },
        });
        const new_attachments = await this.findOrCreates({ message: message, files });
        const new_attachment_ids = new_attachments.map(val => val.id);
        await Promise.all(
            attachments
                .filter(attachment => !new_attachment_ids.find(id => id == attachment.id))
                .map(attachment => MessageAttachmentEntity.delete(attachment))
        );
        return MessageAttachmentEntity.find({
            where: {
                messageId: message?.id,
            },
        });
    }

}

module.exports = MessageAttachmentsDataStore;