import { Localizer } from "@guildion/core";

export default class BaseDataStore {
    public localizer: Localizer;

    constructor({ localizer }: { localizer: Localizer }) {
        this.localizer = localizer;
    }
}