import { Localizer } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type AdministratorsValidation from '@/app/validations/AdministratorsValidation';

export default class AdministratorsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: AdministratorsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/AdministratorsValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }
}

module.exports = AdministratorsDataStore;