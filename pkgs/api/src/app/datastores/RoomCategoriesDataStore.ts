import { Localizer, RoomCategoryAttributes, RoomStatus, RoomType } from "@guildion/core";
import { RoomCategoryEntity, GuildEntity, UserEntity, MemberEntity, RoomEntity, RoomCategoryListEntity, RoomMemberEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomCategoriesValidation from '@/app/validations/RoomCategoriesValidation';
import Bluebird from "bluebird";

export default class RoomCategoriesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RoomCategoriesValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomCategoriesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async getsPublic(
        guildId: string,
        { roomsCount, count, offset, user }: { roomsCount?: number, count?: number, offset?: number, user?: UserEntity }
    ): Promise<{ categories: RoomCategoryEntity[], guild: GuildEntity, paginatable: boolean }> {
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            },
        });
        await this.validation.checkGuildState(guild);
        const member = user ? await MemberEntity.findOne({
            where: {
                userId: user?.id,
                guildId: guild?.id,
            },
        }) : undefined;
        const categories = await RoomCategoryEntity.find({
            where: {
                guildId: guildId,
            },
            take: count ? count + 1 : undefined,
            skip: offset,
            order: { index: 'ASC' },
        });
        const results = await this.fetchRooms(categories, { guildId, count: roomsCount, isPublic: true, member });
        const last = !!count && categories.length >= count && categories.pop();

        return {
            categories: results,
            guild: guild!,
            paginatable: !!last,
        }
    }

    async gets(
        guildId: string,
        { roomsCount, count, offset, user }: { roomsCount?: number, count?: number, offset?: number, user?: UserEntity }
    ): Promise<{ categories: RoomCategoryEntity[], guild: GuildEntity, paginatable: boolean }> {
        await this.validation.checkUserState(user);
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            },
        });
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                userId: user?.id,
                guildId: guild?.id,
            },
        });
        await this.validation.checkMemberState(member);
        const categories = await RoomCategoryEntity.find({
            where: {
                guildId: guildId,
            },
            take: count ? count + 1 : undefined,
            skip: offset,
            order: { index: 'ASC' },
        });

        const results = await this.fetchRooms(categories, { guildId, count: roomsCount, member });

        const last = !!count && categories.length >= count && categories.pop();

        return {
            categories: results,
            guild: guild!,
            paginatable: !!last,
        }
    }

    async fetchRooms(categories: RoomCategoryEntity[], { guildId, count, isPublic, member }: { guildId: string, count?: number, isPublic?: boolean, member?: MemberEntity }): Promise<RoomCategoryEntity[]> {
        return await Promise.all(
            categories.map(
                async category => {
                    const lists = await RoomCategoryListEntity
                        .getRepository()
                        .createQueryBuilder('room_category_lists')
                        .innerJoinAndSelect('room_category_lists', 'room', 'room.guild_id = :guildId AND rooms.status NOT IN (:...statuses) AND rooms.type IN (:...types)', { guildId, statuses: [RoomStatus.BANNED, RoomStatus.LOCKED, RoomStatus.DELETED, RoomStatus.DELETING], types: isPublic ? [RoomType.PUBLIC, RoomType.OFFICIAL] : [RoomType.PUBLIC, RoomType.OFFICIAL, RoomType.PRIVATE] })
                        .limit(count)
                        .offset(0)
                        .orderBy({ updatedAt: 'DESC' })
                        .getMany();
                    category.cacheRooms = await Promise.all(
                        lists.map(async list => {
                            const room = await list.room;
                            if (member && room.guildId === member.guildId) {
                                room.currentMember = member;
                                room.currentRoomMember = await RoomMemberEntity.findOne({
                                    where: {
                                        roomId: room.id,
                                        memberId: member.id,
                                    }
                                })
                            }
                            return room;
                        })
                    );
                    return category;
                }
            )
        );
    }

    async get(id: string): Promise<RoomCategoryEntity> {
        const category = await RoomCategoryEntity.findOne({
            where: {
                id,
            },
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: category?.guildId,
            }
        });
        await Promise.all([
            this.validation.checkRoomCategoryState(category),
            this.validation.checkGuildState(guild),
        ]);
        return category!;
    }

    async create(attributes: RoomCategoryAttributes, { user }: { user: UserEntity }): Promise<RoomCategoryEntity> {
        const guild = await GuildEntity.findOne({
            where: {
                id: attributes.guildId,
            }
        });
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            }
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkMemberManagable(member!);
        const result = await RoomCategoryEntity.create({
            guildId: guild!.id,
            name: attributes.name,
            index: await this.getLastIndex(guild!),
        }).save();
        await this.refreshIndex(guild!.id);
        return result;
    }

    async update(attributes: Partial<RoomCategoryAttributes> & { id: string }, { user }: { user: UserEntity }): Promise<RoomCategoryEntity> {
        const category = await RoomCategoryEntity.findOne({
            where: {
                id: attributes.id,
            },
        });
        await this.validation.checkRoomCategoryState(category);
        const guild = await GuildEntity.findOne({
            where: {
                id: category!.guildId,
            }
        });
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            }
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkMemberManagable(member!);
        if (attributes.name !== undefined) category!.name = attributes.name;
        if (attributes.index !== undefined) category!.index = attributes.index;
        const result = await category!.save();
        await this.refreshIndex(guild!.id);
        return result;
    }

    async destroy(id: string, { user }: { user: UserEntity }): Promise<void> {
        const category = await RoomCategoryEntity.findOne({
            where: {
                id,
            },
        });
        await this.validation.checkRoomCategoryState(category);
        const guild = await GuildEntity.findOne({
            where: {
                id: category!.guildId,
            }
        });
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            }
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkMemberManagable(member!);
        await RoomCategoryEntity.delete(id);
        await this.refreshIndex(guild!.id);
    }

    async getLastIndex(guild: GuildEntity): Promise<number> {
        const count = await RoomCategoryEntity.count({
            where: {
                guildId: guild.id,
            },
        });
        return count;
    }

    async refreshIndex(guildId: string): Promise<void> {
        const vals = await RoomCategoryEntity.find({
            where: {
                guildId,
            },
            order: { 'index': 'ASC' },
        });
        await Bluebird.map(
            vals,
            (val, i) => {
                val.index = i;
                return val.save();
            },
            { concurrency: 10 }
        );
    }
}

module.exports = RoomCategoriesDataStore;