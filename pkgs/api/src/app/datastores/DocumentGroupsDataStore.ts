import { DocumentGroupStatus, DocumentSectionStatus, DocumentStatus, LanguageCode, Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type DocumentGroupsValidation from '@/app/validations/DocumentGroupsValidation';
import { DocumentEntity, DocumentGroupEntity, DocumentSectionEntity } from '@guildion/db';
import Bluebird from 'bluebird';

export default class DocumentGroupsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: DocumentGroupsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/DocumentGroupsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async getPaths(
    ): Promise<{
        paths: { params: { groupSlug: string }, locale: LanguageCode }[],
    }> {
        const groups = await DocumentGroupEntity.find({
            where: {
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        return {
            paths: [
                ...groups.filter(g => !!g.jaName).map(g => {
                    return {
                        params: {
                            groupSlug: g.slug,
                        },
                        locale: LanguageCode.Japanese
                    }
                }),
                ...groups.filter(g => !!g.enName).map(g => {
                    return {
                        params: {
                            groupSlug: g.slug,
                        },
                        locale: LanguageCode.English
                    }
                }),
            ],
        };
    }

    async get(
        slug: string
    ): Promise<{
        group: DocumentGroupEntity,
        sections: {
            section: DocumentSectionEntity,
            documents: DocumentEntity[]
        }[],
    }> {
        const group = await DocumentGroupEntity.findOneOrFail({
            where: {
                slug,
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        const sections = await DocumentSectionEntity.find({
            where: {
                groupId: group.id,
                status: DocumentSectionStatus.PUBLISH,
            },
            order: { 'index': 'ASC' },
        });

        const results = await Bluebird.map(
            sections,
            async section => {
                const documents = await DocumentEntity.find({
                    where: {
                        sectionId: section.id,
                        status: DocumentStatus.PUBLISH,
                    },
                    order: { 'index': 'ASC' },
                });
                return { section, documents };
            },
            { concurrency: 10 }
        );

        return {
            group,
            sections: results,
        };
    }
}

module.exports = DocumentGroupsDataStore;