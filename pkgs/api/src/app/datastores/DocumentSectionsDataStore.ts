import { DocumentGroupStatus, DocumentSectionStatus, DocumentStatus, LanguageCode, Localizer } from '@guildion/core';
import BaseDataStore from './BaseDataStore';
import DataStoreImpl from './DataStoreImpl';
import type DocumentSectionsValidation from '@/app/validations/DocumentSectionsValidation';
import { DocumentEntity, DocumentGroupEntity, DocumentSectionEntity } from '@guildion/db';
import Bluebird from 'bluebird';

export default class DocumentSectionsDataStore extends BaseDataStore implements DataStoreImpl {
    private _validation!: DocumentSectionsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/DocumentSectionsValidation');
        this._validation ||= new Constructor({ localizer: this.localizer });
        return this._validation
    }

    async getPaths(
    ): Promise<{
        paths: { params: { groupSlug: string, sectionSlug: string }, locale: LanguageCode }[],
    }> {
        const groups = await DocumentGroupEntity.find({
            where: {
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        const sections = await DocumentSectionEntity
            .getRepository()
            .createQueryBuilder('document_sections')
            .where('document_sections.groupId IN (:...groupIds)', { groupIds: groups.map(g => g.id) })
            .andWhere('document_sections.status = :status', { status: DocumentSectionStatus.PUBLISH })
            .getMany();

        return {
            paths: [
                ...sections.filter(s => !!s.jaName).map(s => {
                    return {
                        params: {
                            groupSlug: groups.find(g => g.id === s.groupId)!.slug,
                            sectionSlug: s.slug,
                        },
                        locale: LanguageCode.Japanese,
                    }
                }),
                ...sections.filter(s => !!s.enName).map(s => {
                    return {
                        params: {
                            groupSlug: groups.find(g => g.id === s.groupId)!.slug,
                            sectionSlug: s.slug,
                        },
                        locale: LanguageCode.English,
                    }
                }),
            ],
        };
    }

    async get(
        groupSlug: string,
        slug: string,
    ): Promise<{
        group: DocumentGroupEntity,
        section: DocumentSectionEntity,
        document: DocumentEntity,
    }> {
        const group = await DocumentGroupEntity.findOneOrFail({
            where: {
                slug: groupSlug,
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        const section = await DocumentSectionEntity.findOneOrFail({
            where: {
                slug,
                groupId: group.id,
                status: DocumentSectionStatus.PUBLISH,
            },
        });
        await DocumentEntity.findOneOrFail({
            where: {
                sectionId: section.id,
                status: DocumentSectionStatus.PUBLISH,
            },
        });
        const documents = await DocumentEntity.find({
            where: {
                sectionId: section.id,
                status: DocumentStatus.PUBLISH,
            },
            order: { 'index': 'ASC' },
            take: 1,
        });
        return {
            group,
            section,
            document: documents[0],
        };
    }

    async gets(
        groupSlug: string,
        { includes }: { includes: { documents?: boolean }}
    ): Promise<{
        group: DocumentGroupEntity,
        sections: {
            section: DocumentSectionEntity,
            documents: DocumentEntity[]
        }[],
    }> {
        const group = await DocumentGroupEntity.findOneOrFail({
            where: {
                slug: groupSlug,
                status: DocumentGroupStatus.PUBLISH,
            },
        });
        const sections = await DocumentSectionEntity.find({
            where: {
                groupId: group.id,
                status: DocumentSectionStatus.PUBLISH,
            },
            order: { 'index': 'ASC' },
        });

        const results = await Bluebird.map(
            sections,
            async section => {
                const documents = includes.documents ? await DocumentEntity.find({
                    where: {
                        sectionId: section.id,
                        status: DocumentStatus.PUBLISH,
                    },
                    order: { 'index': 'ASC' },
                }) : [];
                return { section, documents };
            },
            { concurrency: 10 }
        );

        return {
            group,
            sections: results,
        };
    }
}

module.exports = DocumentSectionsDataStore;