import { Localizer, RoleType } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type MemberRolesValidation from '@/app/validations/MemberRolesValidation';
import type RoomRolesDataStore from "./RoomRolesDataStore";
import type RolesDataStore from "./RolesDataStore";
import { MemberEntity, MemberRoleEntity, RoleEntity, UserEntity } from "@guildion/db";

export default class MemberRolesDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: MemberRolesValidation;
    private $roomRolesDataStore!: RoomRolesDataStore;
    private $rolesDataStore!: RolesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/MemberRolesValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get roomRolesDataStore() {
        const Constructor = require('./RoomRolesDataStore');
        this.$roomRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$roomRolesDataStore
    }

    get rolesDataStore() {
        const Constructor = require('./RolesDataStore');
        this.$rolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$rolesDataStore
    }

    async gets(member: MemberEntity): Promise<MemberRoleEntity[]> {
        await this.validation.checkMemberState(member);
        const memberRoles = await MemberRoleEntity
            .getRepository()
            .createQueryBuilder('member_roles')
            .innerJoinAndSelect('member_roles.role', 'role')
            .where('member_roles.member_id = :memberId', { memberId: member.id })
            .orderBy('role.created_at', 'ASC')
            .getMany()
        return memberRoles;
    }

    async getsFromID(memberId: string): Promise<MemberRoleEntity[]> {
        const member = await MemberEntity.findOne({
            where: {
                id: memberId,
            },
        });
        return this.gets(member!);
    }

    async makeRoles(memberRoles: MemberRoleEntity[]): Promise<RoleEntity[]> {
        return Promise.all(
            memberRoles.map(mr => mr.role)
        );
    }

    async hasType(memberId: string, type: RoleType): Promise<boolean> {
        const memberRoles = await this.getsFromID(memberId);
        const roles = await this.makeRoles(memberRoles);
        return !!roles.find(role => role.type == type);
    }

    async includeTypes(memberId: string, types: RoleType[]): Promise<boolean> {
        const results = await Promise.all(
            types.map(type => this.hasType(memberId, type))
        );
        return results.filter(result => result).length > 0;
    }

    async hasTypes(memberId: string, types: RoleType[]): Promise<boolean> {
        const results = await Promise.all(
            types.map(type => this.hasType(memberId, type))
        );
        return results.filter(result => result).length === results.length;
    }

    async findOrCreate({ member, role }: { member: MemberEntity, role: RoleEntity }): Promise<MemberRoleEntity> {
        await this.validation.checkMemberState(member);
        await this.validation.checkRoleState(role);
        if (member.guildId !== role.guildId) throw new Error(this.localizer.dictionary.error.role.notExists);

        const exist = await MemberRoleEntity.findOne({
            where: {
                memberId: member.id,
                roleId: role.id,
            },
        });

        if (exist) {
            return exist;
        } else {
            return MemberRoleEntity.create({
                memberId: member.id,
                roleId: role.id,
            }).save();
        }
    }

    async destroy({ member, role }: { member: MemberEntity, role: RoleEntity }) {
        await this.validation.checkMemberState(member);
        await this.validation.checkRoleState(role);
        if (member.guildId !== role.guildId) throw new Error(this.localizer.dictionary.error.role.notExists);
        await MemberRoleEntity.delete({ memberId: member?.id, roleId: role?.id });
    }

    async destroys({ member, roles }: { member: MemberEntity, roles: RoleEntity[] }) {
        await Promise.all(
            roles.map(role => this.destroy({ member, role }))
        );
    }

    async makeMembers(member: MemberEntity): Promise<MemberRoleEntity> {
        const guild = await member.guild;
        await this.validation.checkMemberState(member);
        await this.validation.checkGuildState(guild);
        const roles = await this.rolesDataStore.findOrCreateGuildRoles(guild);
        const role = roles.find(role => role.type === RoleType.Members)!;
        return this.findOrCreate({ member, role });
    }

    async revertMembers(member: MemberEntity, { user }: { user: UserEntity }): Promise<MemberRoleEntity> {
        const guild = await member.guild;
        const sender = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: member.guildId,
            }
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkGuildState(guild);
        await this.validation.checkMemberIsNotOwner(member);
        await this.validation.checkMemberState(sender);
        await this.validation.checkMemberManagable(sender!);
        const roles = await this.rolesDataStore.findOrCreateGuildRoles(guild);
        const role = roles.find(role => role.type === RoleType.Members)!;
        const shouldDeletes = roles.filter(role => role.type !== RoleType.Members);
        await this.destroys({ member, roles: shouldDeletes });
        return this.findOrCreate({ member, role });
    }

    async makeAdmin(member: MemberEntity, { user }: { user: UserEntity }): Promise<MemberRoleEntity> {
        const guild = await member.guild;
        const sender = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: member.guildId,
            }
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkGuildState(guild);
        await this.validation.checkMemberState(sender);
        await this.validation.checkMemberManagable(sender!);
        const roles = await this.rolesDataStore.findOrCreateGuildRoles(guild);
        const role = roles.find(role => role.type === RoleType.Admin)!;
        return this.findOrCreate({ member, role });
    }

    async makeOwner(member: MemberEntity): Promise<MemberRoleEntity> {
        const guild = await member.guild;
        await this.validation.checkMemberState(member);
        await this.validation.checkGuildState(guild);
        await this.validation.checkMemberIsOwner(member);
        const roles = await this.rolesDataStore.findOrCreateGuildRoles(guild);
        const role = roles.find(role => role.type === RoleType.Owner)!;
        return this.findOrCreate({ member, role });
    }

    // async makeTrial(member: MemberEntity): Promise<MemberRoleEntity> {
    //     // add member trial validation
    //     const guild = await member.guild;
    //     await this.validation.checkMemberState(member);
    //     await this.validation.checkGuildState(guild);
    //     const roles = await this.rolesDataStore.findOrCreateGuildRoles(guild);
    //     const role = roles.find(role => role.type === RoleType.Trial)!;
    //     const shouldDeletes = roles.filter(role => role.type !== RoleType.Trial && role.type !== RoleType.Members);
    //     await this.destroys({ member, roles: shouldDeletes });
    //     return this.findOrCreate({ member, role });
    // }

    // async makePremium(member: MemberEntity): Promise<MemberRoleEntity> {
    //     // add member trial validation
    //     const guild = await member.guild;
    //     await this.validation.checkMemberState(member);
    //     await this.validation.checkGuildState(guild);
    //     const roles = await this.rolesDataStore.findOrCreateGuildRoles(guild);
    //     const role = roles.find(role => role.type === RoleType.Premium)!;
    //     const shouldDeletes = roles.filter(role => role.type !== RoleType.Premium);
    //     await this.destroys({ member, roles: shouldDeletes });
    //     return this.findOrCreate({ member, role });
    // }
}

module.exports = MemberRolesDataStore;