import { RoleAttributes, RoleRecordAttributes, Localizer, RoomAttributes, RoomStatus, RoomType, MemberAttributes, MemberRecordAttributes, RoomMemberStatus, PostType } from "@guildion/core";
import { GuildEntity, MemberEntity, RoomEntity, RoleEntity, UserEntity, RoomMemberEntity, FileEntity, connection } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type RoomsValidation from '@/app/validations/RoomsValidation';
import type MemberRolesDataStore from "./MemberRolesDataStore";
import type RoomCategoriesDataStore from "./RoomCategoriesDataStore";
import type RoomMembersDataStore from "./RoomMembersDataStore";
import type RoomRolesDataStore from "./RoomRolesDataStore";
import type RoomStoragesDataStore from "./RoomStoragesDataStore";
import type FilesDataStore from "./FilesDataStore";
import { ulid } from "ulid";
import Bluebird from "bluebird";

export default class RoomsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: RoomsValidation;
    private $memberRolesDataStore!: MemberRolesDataStore;
    private $roomRolesDataStore!: RoomRolesDataStore;
    private $roomMembersDataStore!: RoomMembersDataStore;
    private $roomCategoriesDataStore!: RoomCategoriesDataStore;
    private $roomStoragesDataStore!: RoomStoragesDataStore;
    private $filesDataStore!: FilesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/RoomsValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    get roomMembersDataStore() {
        const Constructor = require('./RoomMembersDataStore');
        this.$roomMembersDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$roomMembersDataStore
    }

    get memberRolesDataStore() {
        const Constructor = require('./MemberRolesDataStore');
        this.$memberRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$memberRolesDataStore
    }

    get roomRolesDataStore() {
        const Constructor = require('./RoomRolesDataStore');
        this.$roomRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$roomRolesDataStore
    }

    get roomCategoriesDataStore() {
        const Constructor = require('./RoomCategoriesDataStore');
        this.$roomCategoriesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$roomCategoriesDataStore
    }

    get roomStoragesDataStore() {
        const Constructor = require('./RoomStoragesDataStore');
        this.$roomStoragesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$roomStoragesDataStore
    }

    get filesDataStore() {
        const Constructor = require('./FilesDataStore');
        this.$filesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$filesDataStore
    }

    async getPublic(id: string, { user }: { user?: UserEntity }): Promise<{ room: RoomEntity }> {
        const room = await RoomEntity.findOne({
            where: {
                id,
            },
        });
        await this.validation.checkRoomState(room);
        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
            },
        });
        await this.validation.checkGuildState(guild);
        const member = user ? await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user.id,
            },
        }) : undefined;
        const roomMember = member ? await RoomMemberEntity.findOne({
            where: {
                roomId: id,
                memberId: member?.id,
            },
        }) : undefined;
        room!.currentMember = member;
        room!.currentRoomMember = roomMember;
        return {
            room: room!,
        };
    }

    async get(id: string, { user }: { user?: UserEntity }): Promise<{ room: RoomEntity, member: MemberEntity }> {
        await this.validation.checkUserState(user);
        const room = await RoomEntity.findOne({
            where: {
                id,
            },
        });
        await this.validation.checkRoomState(room);
        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
            },
        });
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                guildId: guild?.id,
                userId: user!.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: id,
                memberId: member?.id,
            },
        });
        await this.validation.checkMemberState(member);
        await this.validation.checkRoomMemberState(roomMember);
        await this.validation.checkMemberRoomViewable({ member: member!, room: room! });
        room!.currentMember = member;
        room!.currentRoomMember = roomMember;
        return {
            room: room!,
            member: member!,
        };
    }

    async gets(
        guildId: string,
        { 
            user,
            count,
            offset,
            ltAt,
            gtAt,
        }: {
            user?: UserEntity,
            count?: number,
            offset?: number,
            ltAt?: Date,
            gtAt?: Date,
        }
    ): Promise<{ rooms: RoomEntity[], paginatable: boolean }> {
        await this.validation.checkUserState(user);
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            }
        })
        await this.validation.checkGuildState(guild);
        const member = await MemberEntity.findOne({
            where: {
                userId: user!.id,
                guildId,
            },
        });
        await this.validation.checkMemberState(member);

        const results = await connection
            .createQueryBuilder()
            .select("*")
            .from(subQuery =>
                subQuery
                    .from(RoomEntity, 'rooms')
                    .select("*")
                    .distinctOn(['room_activities.room_id'])
                    .innerJoin('rooms.roomMembers', 'room_members', 'room_members.member_id = :memberId AND room_members.status = :roomStatus', { memberId: member!.id, roomStatus: RoomMemberStatus.CREATED })
                    .innerJoinAndSelect('rooms.activities', 'room_activities', gtAt ? 'room_activities.created_at >= :basedAt' : 'room_activities.created_at <= :basedAt', { basedAt: gtAt?.toISOString() ?? ltAt?.toISOString() ?? new Date()?.toISOString() })
                    .where('rooms.guild_id = :guildId', { guildId })
                    .andWhere("rooms.status NOT IN (:...statuses)", { statuses: [RoomStatus.BANNED, RoomStatus.LOCKED, RoomStatus.DELETED, RoomStatus.DELETING] })
                    .orderBy('room_activities.created_at', 'DESC')
                    .orderBy('room_activities.room_id', 'DESC'),
                "rooms"
            )
            .limit(count ? count + 1 : undefined)
            .offset(offset)
            .orderBy('room_activities_created_at', 'DESC')
            .getRawMany();

        const rooms = await Promise.all(
            results.map(async (result) => {
                const room = await RoomEntity.findOneOrFail({
                    where: {
                        id: result.room_id,
                    },
                });
                room.currentMember = member;
                room.currentRoomMember = await RoomMemberEntity.findOne({
                    where: {
                        roomId: room.id,
                        memberId: member?.id,
                    }
                })
                return room;
            })
        );

        const last = !!count && rooms.length >= count && rooms.pop();

        return {
            rooms,
            paginatable: !!last,
        }
    }

    async getsPublic(
        guildId: string,
        { 
            user,
            count,
            offset,
            ltAt,
            gtAt,
        }: {
            user?: UserEntity,
            count?: number,
            offset?: number,
            ltAt?: Date,
            gtAt?: Date,
        }
    ): Promise<{ rooms: RoomEntity[], paginatable: boolean }> {
        const guild = await GuildEntity.findOne({
            where: {
                id: guildId,
            }
        })
        await this.validation.checkGuildState(guild);

        const member = user ? await MemberEntity.findOne({
            where: {
                userId: user!.id,
                guildId,
            },
        }) : undefined;

        const results = await connection
            .createQueryBuilder()
            .select("*")
            .from(subQuery =>
                subQuery
                    .from(RoomEntity, 'rooms')
                    .select("*")
                    .distinctOn(['room_activities.room_id'])
                    .innerJoinAndSelect('rooms.activities', 'room_activities', gtAt ? 'room_activities.created_at >= :basedAt' : 'room_activities.created_at <= :basedAt', { basedAt: gtAt?.toISOString() ?? ltAt?.toISOString() ?? new Date()?.toISOString() })
                    .where('rooms.guild_id = :guildId', { guildId })
                    .andWhere("rooms.status NOT IN (:...statuses)", { statuses: [RoomStatus.BANNED, RoomStatus.LOCKED, RoomStatus.DELETED, RoomStatus.DELETING] })
                    .andWhere('rooms.type IN (:...types)', { types: [RoomType.PUBLIC, RoomType.OFFICIAL] })
                    .orderBy('room_activities.created_at', 'DESC')
                    .orderBy('room_activities.room_id', 'DESC'),
                "rooms"
            )
            .limit(count ? count + 1 : undefined)
            .offset(offset)
            .orderBy('room_activities_created_at', 'DESC')
            .getRawMany();

        const rooms = await Promise.all(
            results.map(async (result) => {
                const room = await RoomEntity.findOneOrFail({
                    where: {
                        id: result.room_id,
                    },
                });
                room.currentMember = member;
                room.currentRoomMember = member ? await RoomMemberEntity.findOne({
                    where: {
                        roomId: room.id,
                        memberId: member?.id,
                    }
                }) : undefined;
                return room;
            })
        );

        const last = !!count && rooms.length >= count && rooms.pop();

        return {
            rooms,
            paginatable: !!last,
        }
    }

    async create(attributes: RoomAttributes, { user, guild }: { user: UserEntity, guild?: GuildEntity | undefined | null }): Promise<RoomEntity> {

        guild ||= await GuildEntity.findOne({
            where: {
                id: attributes.guildId,
            }
        });
        
        const member = await MemberEntity.findOne({
            where: { 
                userId: user.id,
                guildId: guild?.id,
            }
        });

        const profile = await FileEntity.findOne({
            where: {
                id: attributes.profileId,
            },
        });
        
        await Promise.all([
            this.validation.checkProfileFileState(profile),
            this.validation.checkUserState(user),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkRoomDisplayNameFormat(attributes.displayName),
        ]);

        const room = await RoomEntity.create({
            guildId: guild!.id,
            ownerId: member!.id,
            profileId: profile!.id,
            roomname: ulid(),
            displayName: attributes.displayName,
            description: attributes.description,
            languageCode: attributes.languageCode,
            countryCode: attributes.countryCode,
            type: attributes.type == RoomType.PRIVATE ? RoomType.PRIVATE : RoomType.PUBLIC,
            status: RoomStatus.CREATED,
        }).save();
        await this.roomStoragesDataStore.findOrCreate(room);
        const roles = attributes.roles?.map(role => RoleEntity.create({ ...(role as Omit<RoleAttributes, keyof RoleRecordAttributes>) })) || [];
        await this.roomRolesDataStore.findOrCreates({ room, roles });
        await this.roomMembersDataStore.findOrCreates({ 
            room,
            members: attributes.members?.map(member => MemberEntity.create({ ...(member as Omit<MemberAttributes, keyof MemberRecordAttributes>) })) || [],
            roles,
            owner: member,
        });
        await room.activate();
        return room;
    }

    async update(attributes: Partial<RoomAttributes> & { id: string }, { user, guild }: { user: UserEntity, guild?: GuildEntity | undefined | null }): Promise<RoomEntity> {
        const room = await RoomEntity.findOne({
            where: {
                id: attributes.id,
            }
        });

        guild ||= await GuildEntity.findOne({ 
            where: { 
                id: room?.guildId 
            } 
        });
        
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild?.id,
            }
        });

        const profile = attributes.profileId ? await FileEntity.findOne({
            where: {
                id: attributes.profileId,
            },
        }) : undefined;
        
        await Promise.all([
            this.validation.checkProfileFileState(profile),
            this.validation.checkUserState(user),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkMemberRoomManagable({ member: member!, room: room! }),
            attributes.displayName ? this.validation.checkRoomDisplayNameFormat(attributes.displayName) : undefined,
        ]);

        if (attributes.displayName !== undefined) room!.displayName = attributes.displayName;
        if (attributes.description !== undefined) room!.description = attributes.description;
        if (attributes.profileId !== undefined) room!.profileId = attributes.profileId;
        if (attributes.languageCode !== undefined) room!.languageCode = attributes.languageCode;
        if (attributes.countryCode !== undefined) room!.countryCode = attributes.countryCode;
        const result = await room!.save();
        return result;
    }

    async destroy(id: string, { user }: { user: UserEntity }): Promise<void> {
        const room = await RoomEntity.findOne({
            where: {
                id: id,
            },
        });
        const guild = await GuildEntity.findOne({
            where: {
                id: room?.guildId,
                
            },
        });
        const member = await MemberEntity.findOne({
            where: {
                guildId: room?.guildId,
                userId: user.id,
            },
        });
        const roomMember = await RoomMemberEntity.findOne({
            where: {
                roomId: id,
                memberId: member?.id,
            },
        });
        await Promise.all([
            this.validation.checkRoomState(room),
            this.validation.checkGuildState(guild),
            this.validation.checkMemberState(member),
            this.validation.checkRoomMemberState(roomMember),
            this.validation.checkMemberRoomManagable({ room: room!, member: member! }),
        ]);
        room!.status = RoomStatus.DELETING;
        room!.deletedAt = new Date();
        await room?.save();
        return;
    }

    async search(query: string, { count, offset, guildId, user }: { count?: number, offset?: number, guildId?: string, user?: UserEntity }): Promise<{ rooms: RoomEntity[], paginatable: boolean }> {
        const guild = guildId ? await GuildEntity.findOne({
            where: {
                id: guildId,
            },
        }) : undefined;

        const member = user && guildId ? await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId,
            },
        }) : undefined;

        await Promise.all([
            guildId && this.validation.checkGuildState(guild),
            user && guildId && await this.validation.checkMemberState(member),
        ]);

        const rooms = await RoomEntity
            .getRepository()
            .createQueryBuilder('rooms')
            .leftJoinAndSelect('rooms.posts', 'posts', "posts.type IN (:...postTypes)", { postTypes: [PostType.PUBLIC] })
            .where("rooms.status NOT IN (:...statuses)", { statuses: [RoomStatus.BANNED, RoomStatus.LOCKED, RoomStatus.DELETED, RoomStatus.DELETING] })
            .andWhere("rooms.type IN (:...types)", { types: user ? [RoomType.PUBLIC, RoomType.PRIVATE, RoomType.OFFICIAL] : [RoomType.PUBLIC, RoomType.OFFICIAL] })
            .andWhere(`
                rooms.display_name LIKE :query OR
                rooms.description LIKE :query OR
                posts.body LIKE :query
            `, { query:`%${query}%` })
            .limit(count ? count + 1 : count)
            .offset(offset)
            .getMany();
            
        const last = !!count && rooms.length >= count && rooms.pop();

        if (user) {
            await Bluebird.all(rooms.map(async (room) => {
                const currentMember = await MemberEntity.findOne({
                    where: {
                        userId: user.id,
                        guildId: room.guildId,
                    },
                });
                const currentRoomMember = await RoomMemberEntity.findOne({
                    where: {
                        memberId: currentMember?.id,
                        roomId: room.id,
                    },
                });
                room.currentMember = currentMember;
                room.currentRoomMember = currentRoomMember;
                return room;
            }))
        }

        return {
            rooms,
            paginatable: !!last
        }
    }
}

module.exports = RoomsDataStore;