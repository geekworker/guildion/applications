import { ConnectionAttributes, Localizer, Provider } from "@guildion/core";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type ConnectionsValidation from '@/app/validations/ConnectionsValidation';
import { ConnectionEntity, DeviceConnectionEntity, DeviceEntity } from "@guildion/db";

export default class ConnectionsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: ConnectionsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/ConnectionsValidation')
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async create(attributes: ConnectionAttributes, { deviceId }: { deviceId: string }): Promise<ConnectionEntity> {
        const connection = await ConnectionEntity.create({
            isConnecting: true,
            secWebsocketKey: attributes.secWebsocketKey,
            provider: Provider.Connect,
            connectedAt: new Date(),
            disconnectedAt: undefined,
        }).save();
        const device = await DeviceEntity.findOne({
            where: {
                id: deviceId,
            }
        });
        await this.validation.checkDeviceState(device);
        await device?.disconnect();
        await DeviceConnectionEntity.create({
            connectionId: connection.id,
            deviceId,
        }).save();
        return connection;
    }

    async destroy(id: string): Promise<void> {
        const connection = await ConnectionEntity.findOne({
            where: {
                id,
            },
        });
        await connection?.disconnect();
        return;
    }
}

module.exports = ConnectionsDataStore;