import { Localizer } from "@guildion/core";
import { ConnectionEntity, UserConnectionEntity, UserEntity, DeviceEntity, DeviceConnectionEntity } from "@guildion/db";
import BaseDataStore from "./BaseDataStore";
import DataStoreImpl from "./DataStoreImpl";
import type UserConnectionsValidation from '@/app/validations/UserConnectionsValidation';
import Bluebird from "bluebird";
import { In } from "typeorm";

export default class UserConnectionsDataStore extends BaseDataStore implements DataStoreImpl {
    private $validation!: UserConnectionsValidation;

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    get validation() {
        const Constructor = require('@/app/validations/UserConnectionsValidation');
        this.$validation ||= new Constructor({ localizer: this.localizer });
        return this.$validation
    }

    async create({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<{
        userConnection: UserConnectionEntity,
        connection: ConnectionEntity,
    }> {
        const deviceConnections = await DeviceConnectionEntity
            .getRepository()
            .createQueryBuilder('device_connections')
            .innerJoin('device_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .where('device_connections.device_id = :deviceId', { deviceId: device.id })
            .orderBy('device_connections.id', 'DESC')
            .getMany();
        const deviceConnection = deviceConnections.shift();
        const connection = await deviceConnection?.connection;
        await this.validation.checkUserState(user);
        await this.validation.checkConnectionState(connection);
        const exist = await UserConnectionEntity.findOne({
            where: {
                connectionId: connection!.id,
                userId: user.id,
                isConnecting: true
            },
        });

        if (exist) {
            return {
                connection: connection!,
                userConnection: exist,
            };
        } else {
            const userConnection = await UserConnectionEntity.create({
                connectionId: connection!.id,
                userId: user.id,
                isConnecting: true,
                connectedAt: new Date(),
            }).save();
            return {
                connection: connection!,
                userConnection,
            };
        }
    }

    async destroy({ user, device }: { user: UserEntity, device: DeviceEntity }): Promise<boolean> {
        const deviceConnections = await DeviceConnectionEntity
            .getRepository()
            .createQueryBuilder('device_connections')
            .innerJoin('device_connections.connection', 'connection', 'connection.is_connecting = :isConnecting', { isConnecting: true })
            .where('device_connections.device_id = :deviceId', { deviceId: device.id })
            .orderBy('device_connections.id', 'DESC')
            .getMany();
        const connections = await Bluebird.all(
            deviceConnections.map(dc => dc.connection)
        );
        await this.validation.checkUserState(user);
        const exists = await UserConnectionEntity.find({
            where: {
                connectionId: In(connections.map(c => c.id)),
                userId: user.id,
                isConnecting: true,
            },
        });
        await Promise.all(
            exists.map(c => c.disconnect())
        );
        return true;
    }
}

module.exports = UserConnectionsDataStore;