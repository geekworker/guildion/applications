import { ContentType, HTTPStatusCode } from "@guildion/core";
import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify";

export default async function HealthMiddleware(fastify: FastifyInstance) {
    fastify.get('/health', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.json)
            .status(HTTPStatusCode.OK)
            .send({
                success: true,
                healthStatus: 'healthy',
            })
    });
}