import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify";
import StaticMiddleware from "./static";
import HTTPMiddleware from "./http";
import HealthMiddleware from "./health";
import { ContentType, HTTPStatusCode } from "@guildion/core";

export default async function RootMiddleware(fastify: FastifyInstance) {
    fastify.register(HTTPMiddleware);
    fastify.register(StaticMiddleware);
    fastify.register(HealthMiddleware);

    fastify.get('/', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.html)
            .status(HTTPStatusCode.OK)
            .send(`Welcome to Guildion Application API`)
    });
}