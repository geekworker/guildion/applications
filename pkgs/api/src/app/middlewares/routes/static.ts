import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify";
import FastifyStatic from 'fastify-static';
import path from 'path';
import fs from 'fs';
import { ContentType } from "@guildion/core";

export default async function StaticMiddleware(fastify: FastifyInstance) {
    const cacheOptions: { [key: string]: string | boolean | number } = {
        immutable: true,
        maxAge: 86400000,
    }

    fastify.register(FastifyStatic, {
        root: path.join(__dirname, '../../../assets/images'),
        prefix: '/images',
        list: true,
        decorateReply: false,
        ...cacheOptions,
    });

    fastify.register(FastifyStatic, {
        root: path.join(__dirname, '../../../assets/icons'),
        prefix: '/icons',
        list: true,
        decorateReply: false,
        ...cacheOptions,
    });

    fastify.get('/.well-known/apple-app-site-association', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.json)
            .send(
                await fs.readFileSync(
                    path.join(
                        __dirname,
                        '../../../assets/well-known/apple-app-site-association.json'
                    )
                )
            )
    });

    fastify.get('/manifest.json', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.json)
            .send(
                await fs.readFileSync(
                    path.join(
                        __dirname,
                        '../../../assets/static/manifest.json'
                    )
                )
            )
    });

    fastify.get('/OneSignalSDKWorker.js', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.javascript)
            .send(
                await fs.readFileSync(
                    path.join(
                        __dirname,
                        '../../../assets/static/OneSignalSDKWorker.js'
                    )
                )
            )
    });

    fastify.get('/OneSignalSDKUpdaterWorker.js', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.javascript)
            .send(
                await fs.readFileSync(
                    path.join(
                        __dirname,
                        '../../../assets/static/OneSignalSDKUpdaterWorker.js'
                    )
                )
            )
    });

    fastify.get('/robots.txt', async (req: FastifyRequest, reply: FastifyReply) => {
        return reply
            .type(ContentType.txt)
            .send(
                await fs.readFileSync(
                    path.join(
                        __dirname,
                        '../../../assets/static/robots.txt'
                    )
                )
            )
    });
}