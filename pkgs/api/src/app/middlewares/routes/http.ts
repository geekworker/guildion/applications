import { IS_PROD, IS_STAGING } from "@guildion/core";
import { FastifyInstance, FastifyRequest, FastifyReply } from "fastify";
import helmet from 'fastify-helmet';

export default async function HTTPMiddleware(fastify: FastifyInstance) {
    fastify.register(require('fastify-sensible'));

    if (IS_PROD() || IS_STAGING()) {
        fastify.register(require('fastify-conditional-requests'));
        fastify.register(require('fastify-etag'));
        fastify.register(require('fastify-compress'), { 
            global: false 
        })
    }

    fastify.register(helmet);
}