import { FastifyInstance } from "fastify";
import APIV1Middleware from '@/app/middlewares/api/v1';

export default async function APIMiddleware(fastify: FastifyInstance) {
    fastify.register(APIV1Middleware, { prefix: '/v1' });
}