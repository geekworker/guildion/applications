import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import DiscoveriesRecommendHandler from "@/app/handlers/discoveries/recommend";
import { DiscoveriesEndpoint } from "@guildion/core";

export default async function V1FilesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<DiscoveriesEndpoint.Recommend>(DiscoveriesEndpoint.Recommend, fastify, DiscoveriesRecommendHandler);
}