import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import FileProfilePresignHandler from "@/app/handlers/files/profilePresign";
import FilesListHandler from "@/app/handlers/files/list";
import FileShowHandler from "@/app/handlers/files/show";
import FileUploadHandler from "@/app/handlers/files/upload";
import FileUpdateHandler from "@/app/handlers/files/update";
import FileDestroyHandler from "@/app/handlers/files/destroy";
import FileFilingsListHandler from "@/app/handlers/files/filingsList";
import FileFilingsCreateHandler from "@/app/handlers/files/filingsCreate";
import FileFilingsUpdateHandler from "@/app/handlers/files/filingsUpdate";
import FileFilingsDestroyHandler from "@/app/handlers/files/filingsDestroy";
import { FilesEndpoint } from "@guildion/core";

export default async function V1FilesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<FilesEndpoint.ProfilePresign>(FilesEndpoint.ProfilePresign, fastify, FileProfilePresignHandler);
    new UniversalGateway<FilesEndpoint.List>(FilesEndpoint.List, fastify, FilesListHandler);
    new UniversalGateway<FilesEndpoint.Show>(FilesEndpoint.Show, fastify, FileShowHandler);
    new UniversalGateway<FilesEndpoint.Update>(FilesEndpoint.Update, fastify, FileUpdateHandler);
    new UniversalGateway<FilesEndpoint.Upload>(FilesEndpoint.Upload, fastify, FileUploadHandler);
    new UniversalGateway<FilesEndpoint.Destroy>(FilesEndpoint.Destroy, fastify, FileDestroyHandler);
    new UniversalGateway<FilesEndpoint.FilingsList>(FilesEndpoint.FilingsList, fastify, FileFilingsListHandler);
    new UniversalGateway<FilesEndpoint.FilingsCreate>(FilesEndpoint.FilingsCreate, fastify, FileFilingsCreateHandler);
    new UniversalGateway<FilesEndpoint.FilingsUpdate>(FilesEndpoint.FilingsUpdate, fastify, FileFilingsUpdateHandler);
    new UniversalGateway<FilesEndpoint.FilingsDestroy>(FilesEndpoint.FilingsDestroy, fastify, FileFilingsDestroyHandler);
}