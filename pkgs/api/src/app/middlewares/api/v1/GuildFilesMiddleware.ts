import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import GuildFileCreateHandler from "@/app/handlers/guildFiles/create";
import GuildFilesListHandler from "@/app/handlers/guildFiles/list";
import GuildFilesRecentListHandler from "@/app/handlers/guildFiles/recentList";
import { GuildFilesEndpoint } from "@guildion/core";

export default async function V1FilesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<GuildFilesEndpoint.Create>(GuildFilesEndpoint.Create, fastify, GuildFileCreateHandler);
    new UniversalGateway<GuildFilesEndpoint.List>(GuildFilesEndpoint.List, fastify, GuildFilesListHandler);
    new UniversalGateway<GuildFilesEndpoint.RecentList>(GuildFilesEndpoint.RecentList, fastify, GuildFilesRecentListHandler);
}