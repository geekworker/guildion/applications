import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import AccountAuthenticateHandler from "@/app/handlers/accounts/authenticate";
import AccountRegisterHandler from "@/app/handlers/accounts/register";
import AccountLogoutHandler from "@/app/handlers/accounts/logout";
import AccountShowHandler from "@/app/handlers/accounts/show";
import AccountAllowSessionHandler from "@/app/handlers/accounts/allowSession";
import AccountDenySessionHandler from "@/app/handlers/accounts/denySession";
import { AccountsEndpoint } from "@guildion/core";

export default async function V1AccountsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<AccountsEndpoint.Authenticate>(AccountsEndpoint.Authenticate, fastify, AccountAuthenticateHandler);
    new UniversalGateway<AccountsEndpoint.Register>(AccountsEndpoint.Register, fastify, AccountRegisterHandler);
    new UniversalGateway<AccountsEndpoint.Logout>(AccountsEndpoint.Logout, fastify, AccountLogoutHandler);
    new UniversalGateway<AccountsEndpoint.Show>(AccountsEndpoint.Show, fastify, AccountShowHandler);
    new UniversalGateway<AccountsEndpoint.AllowSession>(AccountsEndpoint.AllowSession, fastify, AccountAllowSessionHandler);
    new UniversalGateway<AccountsEndpoint.DenySession>(AccountsEndpoint.DenySession, fastify, AccountDenySessionHandler);
}