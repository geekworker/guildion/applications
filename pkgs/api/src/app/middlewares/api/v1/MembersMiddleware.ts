import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import { MembersEndpoint } from "@guildion/core";
import MembersListHandler from "@/app/handlers/members/list";
import MemberKickHandler from "@/app/handlers/members/kick";
import MemberUpdateHandler from "@/app/handlers/members/update";
import MemberDestroyHandler from "@/app/handlers/members/destroy";

export default async function V1MembersMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<MembersEndpoint.List>(MembersEndpoint.List, fastify, MembersListHandler);
    new UniversalGateway<MembersEndpoint.Kick>(MembersEndpoint.Kick, fastify, MemberKickHandler);
    new UniversalGateway<MembersEndpoint.Update>(MembersEndpoint.Update, fastify, MemberUpdateHandler);
    new UniversalGateway<MembersEndpoint.Destroy>(MembersEndpoint.Destroy, fastify, MemberDestroyHandler);
}