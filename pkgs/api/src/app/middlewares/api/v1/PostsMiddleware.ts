import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import PostCreateHandler from "@/app/handlers/posts/create";
import PostUpdateHandler from "@/app/handlers/posts/update";
import PostDestroyHandler from "@/app/handlers/posts/destroy";
import PostsListHandler from "@/app/handlers/posts/list";
import PostsSearchHandler from "@/app/handlers/posts/search";
import PostShowHandler from "@/app/handlers/posts/show";
import { PostsEndpoint } from "@guildion/core";

export default async function V1PostsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<PostsEndpoint.List>(PostsEndpoint.List, fastify, PostsListHandler);
    new UniversalGateway<PostsEndpoint.Search>(PostsEndpoint.Search, fastify, PostsSearchHandler);
    new UniversalGateway<PostsEndpoint.Show>(PostsEndpoint.Show, fastify, PostShowHandler);
    new UniversalGateway<PostsEndpoint.Create>(PostsEndpoint.Create, fastify, PostCreateHandler);
    new UniversalGateway<PostsEndpoint.Update>(PostsEndpoint.Update, fastify, PostUpdateHandler);
    new UniversalGateway<PostsEndpoint.Destroy>(PostsEndpoint.Destroy, fastify, PostDestroyHandler);
}