import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import DeviceRegisterHandler from "@/app/handlers/devices/register";
import DevicesListHandler from "@/app/handlers/devices/list";
import DeviceTrustHandler from "@/app/handlers/devices/trust";
import DeviceBlockHandler from "@/app/handlers/devices/block";
import DeviceUnBlockHandler from "@/app/handlers/devices/unblock";
import DeviceBlocksListHandler from "@/app/handlers/devices/blocksList";
import { DevicesEndpoint } from "@guildion/core";

export default async function V1DevicesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<DevicesEndpoint.Register>(DevicesEndpoint.Register, fastify, DeviceRegisterHandler);
    new UniversalGateway<DevicesEndpoint.List>(DevicesEndpoint.List, fastify, DevicesListHandler);
    new UniversalGateway<DevicesEndpoint.Trust>(DevicesEndpoint.Trust, fastify, DeviceTrustHandler);
    new UniversalGateway<DevicesEndpoint.Block>(DevicesEndpoint.Block, fastify, DeviceBlockHandler);
    new UniversalGateway<DevicesEndpoint.UnBlock>(DevicesEndpoint.UnBlock, fastify, DeviceUnBlockHandler);
    new UniversalGateway<DevicesEndpoint.BlocksList>(DevicesEndpoint.BlocksList, fastify, DeviceBlocksListHandler);
}