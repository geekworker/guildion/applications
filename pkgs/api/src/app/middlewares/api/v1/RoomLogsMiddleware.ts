import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import RoomLogsListHandler from "@/app/handlers/roomLogs/list";
import { RoomLogsEndpoint } from "@guildion/core";

export default async function V1RoomLogsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<RoomLogsEndpoint.List>(RoomLogsEndpoint.List, fastify, RoomLogsListHandler);
}