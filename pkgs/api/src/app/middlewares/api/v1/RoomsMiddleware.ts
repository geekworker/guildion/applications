import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import RoomCreateHandler from "@/app/handlers/rooms/create";
import RoomUpdateHandler from "@/app/handlers/rooms/update";
import RoomDestroyHandler from "@/app/handlers/rooms/destroy";
import RoomJoinHandler from "@/app/handlers/rooms/join";
import RoomLeaveHandler from "@/app/handlers/rooms/leave";
import RoomsListHandler from "@/app/handlers/rooms/list";
import RoomsSearchHandler from "@/app/handlers/rooms/search";
import RoomConnectingMembersListHandler from "@/app/handlers/rooms/connectingMembersList";
import RoomMembersListHandler from "@/app/handlers/rooms/membersList";
import RoomMemberUpdateHandler from "@/app/handlers/rooms/memberUpdate";
import RoomMemberKickHandler from "@/app/handlers/rooms/memberKick";
import RoomShowHandler from "@/app/handlers/rooms/show";
import RoomConnectHandler from "@/app/handlers/rooms/connect";
import RoomDisconnectHandler from "@/app/handlers/rooms/disconnect";
import RoomConnectionsListHandler from "@/app/handlers/rooms/connectionsList";
import { RoomsEndpoint } from "@guildion/core";

export default async function V1RoomsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<RoomsEndpoint.List>(RoomsEndpoint.List, fastify, RoomsListHandler);
    new UniversalGateway<RoomsEndpoint.Search>(RoomsEndpoint.Search, fastify, RoomsSearchHandler);
    new UniversalGateway<RoomsEndpoint.ConnectingMembersList>(RoomsEndpoint.ConnectingMembersList, fastify, RoomConnectingMembersListHandler);
    new UniversalGateway<RoomsEndpoint.MembersList>(RoomsEndpoint.MembersList, fastify, RoomMembersListHandler);
    new UniversalGateway<RoomsEndpoint.MemberUpdate>(RoomsEndpoint.MemberUpdate, fastify, RoomMemberUpdateHandler);
    new UniversalGateway<RoomsEndpoint.MemberKick>(RoomsEndpoint.MemberKick, fastify, RoomMemberKickHandler);
    new UniversalGateway<RoomsEndpoint.Show>(RoomsEndpoint.Show, fastify, RoomShowHandler);
    new UniversalGateway<RoomsEndpoint.Create>(RoomsEndpoint.Create, fastify, RoomCreateHandler);
    new UniversalGateway<RoomsEndpoint.Update>(RoomsEndpoint.Update, fastify, RoomUpdateHandler);
    new UniversalGateway<RoomsEndpoint.Destroy>(RoomsEndpoint.Destroy, fastify, RoomDestroyHandler);
    new UniversalGateway<RoomsEndpoint.Join>(RoomsEndpoint.Join, fastify, RoomJoinHandler);
    new UniversalGateway<RoomsEndpoint.Leave>(RoomsEndpoint.Leave, fastify, RoomLeaveHandler);
    new UniversalGateway<RoomsEndpoint.Connect>(RoomsEndpoint.Connect, fastify, RoomConnectHandler);
    new UniversalGateway<RoomsEndpoint.Disconnect>(RoomsEndpoint.Disconnect, fastify, RoomDisconnectHandler);
    new UniversalGateway<RoomsEndpoint.ConnectionsList>(RoomsEndpoint.ConnectionsList, fastify, RoomConnectionsListHandler);
}