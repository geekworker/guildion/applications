import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import BlogsListHandler from "@/app/handlers/blogs/list";
import BlogShowHandler from "@/app/handlers/blogs/show";
import BlogShowPathsHandler from "@/app/handlers/blogs/showPaths";
import BlogDraftHandler from "@/app/handlers/blogs/draft";
import BlogDraftPathsHandler from "@/app/handlers/blogs/draftPaths";
import { BlogsEndpoint } from "@guildion/core";

export default async function V1BlogsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<BlogsEndpoint.List>(BlogsEndpoint.List, fastify, BlogsListHandler);
    new UniversalGateway<BlogsEndpoint.Show>(BlogsEndpoint.Show, fastify, BlogShowHandler);
    new UniversalGateway<BlogsEndpoint.ShowPaths>(BlogsEndpoint.ShowPaths, fastify, BlogShowPathsHandler);
    new UniversalGateway<BlogsEndpoint.Draft>(BlogsEndpoint.Draft, fastify, BlogDraftHandler);
    new UniversalGateway<BlogsEndpoint.DraftPaths>(BlogsEndpoint.DraftPaths, fastify, BlogDraftPathsHandler);
}