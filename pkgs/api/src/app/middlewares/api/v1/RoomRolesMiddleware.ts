import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import { RoomRolesEndpoint } from "@guildion/core";
import RoomRolesListHandler from "@/app/handlers/roomRoles/list";
import RoomRoleShowHandler from "@/app/handlers/roomRoles/show";
import RoomRoleCreateHandler from "@/app/handlers/roomRoles/create";
import RoomRoleUpdateHandler from "@/app/handlers/roomRoles/update";
import RoomRoleDestroyHandler from "@/app/handlers/roomRoles/destroy";

export default async function V1RoomRolesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<RoomRolesEndpoint.List>(RoomRolesEndpoint.List, fastify, RoomRolesListHandler);
    new UniversalGateway<RoomRolesEndpoint.Show>(RoomRolesEndpoint.Show, fastify, RoomRoleShowHandler);
    new UniversalGateway<RoomRolesEndpoint.Create>(RoomRolesEndpoint.Create, fastify, RoomRoleCreateHandler);
    new UniversalGateway<RoomRolesEndpoint.Update>(RoomRolesEndpoint.Update, fastify, RoomRoleUpdateHandler);
    new UniversalGateway<RoomRolesEndpoint.Destroy>(RoomRolesEndpoint.Destroy, fastify, RoomRoleDestroyHandler);
}