import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import { MemberRolesEndpoint } from "@guildion/core";
import MemberRolesListHandler from "@/app/handlers/memberRoles/list";
import MemberRoleMakeAdminHandler from "@/app/handlers/memberRoles/makeAdmin";
import MemberRoleRevertMembersHandler from "@/app/handlers/memberRoles/revertMembers";

export default async function V1MemberRolesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<MemberRolesEndpoint.List>(MemberRolesEndpoint.List, fastify, MemberRolesListHandler);
    new UniversalGateway<MemberRolesEndpoint.MakeAdmin>(MemberRolesEndpoint.MakeAdmin, fastify, MemberRoleMakeAdminHandler);
    new UniversalGateway<MemberRolesEndpoint.RevertMembers>(MemberRolesEndpoint.RevertMembers, fastify, MemberRoleRevertMembersHandler);
}