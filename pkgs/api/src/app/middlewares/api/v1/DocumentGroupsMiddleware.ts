import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import DocumentGroupShowHandler from "@/app/handlers/documentGroups/show";
import DocumentGroupsShowPathsHandler from "@/app/handlers/documentGroups/showPaths";
import { DocumentGroupsEndpoint } from "@guildion/core";

export default async function V1DocumentGroupsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<DocumentGroupsEndpoint.Show>(DocumentGroupsEndpoint.Show, fastify, DocumentGroupShowHandler);
    new UniversalGateway<DocumentGroupsEndpoint.ShowPaths>(DocumentGroupsEndpoint.ShowPaths, fastify, DocumentGroupsShowPathsHandler);
}