import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import UserUpdateHandler from "@/app/handlers/users/update";
import UserDestroyHandler from "@/app/handlers/users/destroy";
import UserConnectHandler from "@/app/handlers/users/connect";
import UserDisconnectHandler from "@/app/handlers/users/disconnect";
import { UsersEndpoint } from "@guildion/core";

export default async function V1UsersMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<UsersEndpoint.Update>(UsersEndpoint.Update, fastify, UserUpdateHandler);
    new UniversalGateway<UsersEndpoint.Destroy>(UsersEndpoint.Destroy, fastify, UserDestroyHandler);
    new UniversalGateway<UsersEndpoint.Connect>(UsersEndpoint.Connect, fastify, UserConnectHandler);
    new UniversalGateway<UsersEndpoint.Disconnect>(UsersEndpoint.Disconnect, fastify, UserDisconnectHandler);
}