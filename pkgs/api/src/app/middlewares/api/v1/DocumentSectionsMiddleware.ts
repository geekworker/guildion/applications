import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import DocumentSectionsListHandler from "@/app/handlers/documentSections/list";
import DocumentSectionShowHandler from "@/app/handlers/documentSections/show";
import DocumentSectionsShowPathsHandler from "@/app/handlers/documentSections/showPaths";
import { DocumentSectionsEndpoint } from "@guildion/core";

export default async function V1DocumentSectionsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<DocumentSectionsEndpoint.Show>(DocumentSectionsEndpoint.Show, fastify, DocumentSectionShowHandler);
    new UniversalGateway<DocumentSectionsEndpoint.List>(DocumentSectionsEndpoint.List, fastify, DocumentSectionsListHandler);
    new UniversalGateway<DocumentSectionsEndpoint.ShowPaths>(DocumentSectionsEndpoint.ShowPaths, fastify, DocumentSectionsShowPathsHandler);
}