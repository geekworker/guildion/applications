import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import MessageCreateHandler from "@/app/handlers/messages/create";
import MessageUpdateHandler from "@/app/handlers/messages/update";
import MessageDestroyHandler from "@/app/handlers/messages/destroy";
import MessagesListHandler from "@/app/handlers/messages/list";
import { MessagesEndpoint } from "@guildion/core";

export default async function V1MessagesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<MessagesEndpoint.List>(MessagesEndpoint.List, fastify, MessagesListHandler);
    new UniversalGateway<MessagesEndpoint.Create>(MessagesEndpoint.Create, fastify, MessageCreateHandler);
    new UniversalGateway<MessagesEndpoint.Update>(MessagesEndpoint.Update, fastify, MessageUpdateHandler);
    new UniversalGateway<MessagesEndpoint.Destroy>(MessagesEndpoint.Destroy, fastify, MessageDestroyHandler);
}