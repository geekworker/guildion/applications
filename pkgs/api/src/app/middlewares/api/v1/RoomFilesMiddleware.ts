import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import RoomFileCreateHandler from "@/app/handlers/roomFiles/create";
import RoomFilesListHandler from "@/app/handlers/roomFiles/list";
import { RoomFilesEndpoint } from "@guildion/core";

export default async function V1FilesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<RoomFilesEndpoint.Create>(RoomFilesEndpoint.Create, fastify, RoomFileCreateHandler);
    new UniversalGateway<RoomFilesEndpoint.List>(RoomFilesEndpoint.List, fastify, RoomFilesListHandler);
}