import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import GuildCreateHandler from "@/app/handlers/guilds/create";
import GuildUpdateHandler from "@/app/handlers/guilds/update";
import GuildDestroyHandler from "@/app/handlers/guilds/destroy";
import GuildJoinHandler from "@/app/handlers/guilds/join";
import GuildLeaveHandler from "@/app/handlers/guilds/leave";
import GuildsListHandler from "@/app/handlers/guilds/list";
import GuildsSearchHandler from "@/app/handlers/guilds/search";
import GuildShowHandler from "@/app/handlers/guilds/show";
import GuildConnectHandler from "@/app/handlers/guilds/connect";
import GuildDisconnectHandler from "@/app/handlers/guilds/disconnect";
import GuildConnectionsListHandler from "@/app/handlers/guilds/connectionsList";
import { GuildsEndpoint } from "@guildion/core";

export default async function V1GuildsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<GuildsEndpoint.List>(GuildsEndpoint.List, fastify, GuildsListHandler);
    new UniversalGateway<GuildsEndpoint.Show>(GuildsEndpoint.Show, fastify, GuildShowHandler);
    new UniversalGateway<GuildsEndpoint.Search>(GuildsEndpoint.Search, fastify, GuildsSearchHandler);
    new UniversalGateway<GuildsEndpoint.Create>(GuildsEndpoint.Create, fastify, GuildCreateHandler);
    new UniversalGateway<GuildsEndpoint.Update>(GuildsEndpoint.Update, fastify, GuildUpdateHandler);
    new UniversalGateway<GuildsEndpoint.Destroy>(GuildsEndpoint.Destroy, fastify, GuildDestroyHandler);
    new UniversalGateway<GuildsEndpoint.Join>(GuildsEndpoint.Join, fastify, GuildJoinHandler);
    new UniversalGateway<GuildsEndpoint.Leave>(GuildsEndpoint.Leave, fastify, GuildLeaveHandler);
    new UniversalGateway<GuildsEndpoint.Connect>(GuildsEndpoint.Connect, fastify, GuildConnectHandler);
    new UniversalGateway<GuildsEndpoint.Disconnect>(GuildsEndpoint.Disconnect, fastify, GuildDisconnectHandler);
    new UniversalGateway<GuildsEndpoint.ConnectionsList>(GuildsEndpoint.ConnectionsList, fastify, GuildConnectionsListHandler);
}