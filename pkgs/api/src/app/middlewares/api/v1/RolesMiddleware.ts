import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import { RolesEndpoint } from "@guildion/core";
import RolesListHandler from "@/app/handlers/roles/list";
import RoleShowHandler from "@/app/handlers/roles/show";

export default async function V1RolesMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<RolesEndpoint.List>(RolesEndpoint.List, fastify, RolesListHandler);
    new UniversalGateway<RolesEndpoint.Show>(RolesEndpoint.Show, fastify, RoleShowHandler);
}