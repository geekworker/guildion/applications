import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import DocumentShowHandler from "@/app/handlers/documents/show";
import DocumentsShowPathsHandler from "@/app/handlers/documents/showPaths";
import { DocumentsEndpoint } from "@guildion/core";

export default async function V1DocumentsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<DocumentsEndpoint.Show>(DocumentsEndpoint.Show, fastify, DocumentShowHandler);
    new UniversalGateway<DocumentsEndpoint.ShowPaths>(DocumentsEndpoint.ShowPaths, fastify, DocumentsShowPathsHandler);
}