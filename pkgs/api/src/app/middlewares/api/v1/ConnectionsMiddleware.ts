import { FastifyInstance } from "fastify";
import UniversalGateway from "@/app/modules/gateway/UniversalGateway";
import ConnectionCreateHandler from "@/app/handlers/connections/create";
import ConnectionDestroyHandler from "@/app/handlers/connections/destroy";
import { ConnectionsEndpoint } from "@guildion/core";

export default async function V1ConnectionsMiddleware(fastify: FastifyInstance) {
    new UniversalGateway<ConnectionsEndpoint.Create>(ConnectionsEndpoint.Create, fastify, ConnectionCreateHandler);
    new UniversalGateway<ConnectionsEndpoint.Destroy>(ConnectionsEndpoint.Destroy, fastify, ConnectionDestroyHandler);
}