import { AppError, Device, FileAttributes, isVideo, Localizer, Provider, User, ErrorType, UserStatus, UserDeviceStatus, UserDeviceSessionStatus, AccountStatus, AccountSessionStatus, GuildStatus, GuildType, MemberStatus, RoomStatus, RoomType, MessageStatus, PostStatus, FileStatus, DeviceStatus, nameKeyRegExp, FileType, isImage, transformFromExtension, RoomMemberStatus, RoleType, FileAccessType, ContentType, PostType, IS_PROD } from "@guildion/core";
import type MemberRolesDataStore from "../datastores/MemberRolesDataStore"
import { AccountEntity, ConnectionEntity, DeviceBlockEntity, DeviceEntity, UserConnectionEntity, UserDeviceEntity, UserEntity, MemberConnectionEntity, RoomMemberConnectionEntity, GuildEntity, MemberEntity, RoomMemberEntity, GuildCategoryEntity, RoomCategoryEntity, RoleEntity, RoomRoleEntity, MemberRoleEntity, RoomEntity, MessageEntity, PostEntity, FileEntity, RoomFileEntity, GuildFileEntity } from "@guildion/db";
const urlExists = require("url-exists-deep");

export default class BaseValidation {
    public static spaceRegExp = /\s/;

    public localizer: Localizer;
    private $memberRolesDataStore!: MemberRolesDataStore;

    constructor({ localizer }: { localizer: Localizer }) {
        this.localizer = localizer;
    }
    
    get memberRolesDataStore() {
        const Constructor = require("../datastores/MemberRolesDataStore");
        this.$memberRolesDataStore ||= new Constructor({ localizer: this.localizer });
        return this.$memberRolesDataStore
    }
    
    async productionGuard() {
        if (IS_PROD()) throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
    }

    async checkUsernameFormat(username: string) {
        if (username.length < User.usernameMinLimit || username.length > User.usernameMaxLimit) throw new AppError(this.localizer.dictionary.error.account.username.invalidCharactersRange, { type: ErrorType.accountUsername });
        if (BaseValidation.spaceRegExp.test(username)) throw new AppError(this.localizer.dictionary.error.account.username.invalidSpace, { type: ErrorType.accountUsername });
        if (!nameKeyRegExp.test(username)) throw new AppError(this.localizer.dictionary.error.account.username.invalidFormat, { type: ErrorType.accountUsername });
        const exist = await UserEntity.findOne({
            where: { username },
        });
        if (exist) throw new AppError(this.localizer.dictionary.error.account.username.alreadyExists, { type: ErrorType.accountUsername });
    }

    async checkUserState(user: UserEntity | undefined | null) {
        if (!user) throw new AppError(this.localizer.dictionary.error.user.notExists);
        if (user.status == UserStatus.LOCKED || user.status == UserStatus.DELETED) throw new AppError(this.localizer.dictionary.error.user.deletedError);
    }

    async checkUserDeviceState(userDevice: UserDeviceEntity | undefined | null) {
        if (!userDevice) throw new AppError(this.localizer.dictionary.error.device.relatedError);
        if (userDevice.status == UserDeviceStatus.LOCKED) throw new AppError(this.localizer.dictionary.error.device.lockedError);
    }

    async checkUserDevice(user: UserEntity, device: DeviceEntity) {
        const userDevice = await UserDeviceEntity.findOne({
            where: {
                userId: user.id,
                deviceId: device.id,
            },
        });
        await this.checkUserDeviceState(userDevice);
    }

    async checkUserDeviceLock(userDevice: UserDeviceEntity) {
        if (userDevice.status == UserDeviceStatus.LOCKED) throw new AppError(this.localizer.dictionary.error.device.lockedError);
    }

    async checkUserDeviceTrust(userDevice: UserDeviceEntity) {
        if (userDevice.status !== UserDeviceStatus.TRUSTED) throw new AppError(this.localizer.dictionary.error.device.trustedError);
    }

    async checkUserDeviceSessionState(userDevice: UserDeviceEntity | undefined | null) {
        await this.checkUserDeviceState(userDevice);
        if (userDevice?.status !== UserDeviceStatus.TRUSTED) throw new AppError(this.localizer.dictionary.error.device.trustedError);
        if (userDevice?.sessionStatus !== UserDeviceSessionStatus.LOGIN) throw new AppError(this.localizer.dictionary.error.account.notLoginError);
    }

    async checkDeviceBlocked(deviceBlock: DeviceBlockEntity | undefined | null) {
        if (!!deviceBlock) throw new AppError(this.localizer.dictionary.error.device.blockedError);
    }

    async checkDeviceState(device: DeviceEntity | undefined | null) {
        if (!device) throw new AppError(this.localizer.dictionary.error.common.requireDevice);
        if (device.status == DeviceStatus.LOCKED) throw new AppError(this.localizer.dictionary.error.device.lockedError);
    }

    async checkDeviceRegisterable(device: DeviceEntity) {
        const count = await UserDeviceEntity.count({
            where: {
                deviceId: device.id,
            },
        });
        if (count >= Device.accountMaxLimit) throw new AppError(this.localizer.dictionary.error.device.maxSessionError);
    }

    async checkAccountState(account: AccountEntity | undefined | null) {
        if (!account) throw new AppError(this.localizer.dictionary.error.user.notExists);
        if (account.status == AccountStatus.DELETED) throw new AppError(this.localizer.dictionary.error.user.deletedError);
        if (account.status == AccountStatus.LOCKED) throw new AppError(this.localizer.dictionary.error.user.lockedError);
    }

    async checkAccountSessionState(account: AccountEntity | undefined | null) {
        await this.checkAccountState(account)
        if (account?.sessionStatus == AccountSessionStatus.REJECTED) throw new AppError(this.localizer.dictionary.error.account.sessionLockedError);
    }

    async checkConnectionState(connection: ConnectionEntity | undefined | null) {
        if (!connection) throw new AppError(this.localizer.dictionary.error.connection.notExists);
        if (!connection.isConnecting || connection.disconnectedAt) throw new AppError(this.localizer.dictionary.error.connection.disconnecedError);
    }

    async checkUserConnectionState(connection: UserConnectionEntity | undefined | null) {
        if (!connection) throw new AppError(this.localizer.dictionary.error.connection.notExists);
        if (!connection.isConnecting || connection.disconnectedAt) throw new AppError(this.localizer.dictionary.error.connection.disconnecedError);
    }

    async checkMemberConnectionState(connection: MemberConnectionEntity | undefined | null) {
        if (!connection) throw new AppError(this.localizer.dictionary.error.connection.notExists);
        if (!connection.isConnecting || connection.disconnectedAt) throw new AppError(this.localizer.dictionary.error.connection.disconnecedError);
    }

    async checkRoomMemberConnectionState(connection: RoomMemberConnectionEntity | undefined | null) {
        if (!connection) throw new AppError(this.localizer.dictionary.error.connection.notExists);
        if (!connection.isConnecting || connection.disconnectedAt) throw new AppError(this.localizer.dictionary.error.connection.disconnecedError);
    }

    async checkGuildState(guild: GuildEntity | undefined | null) {
        if (!guild) throw new AppError(this.localizer.dictionary.error.guild.notExists);
        if (guild.status == GuildStatus.DELETED || guild.status == GuildStatus.DELETING) throw new AppError(this.localizer.dictionary.error.guild.deletedError);
        if (guild.status == GuildStatus.LOCKED || guild.status == GuildStatus.BANNED) throw new AppError(this.localizer.dictionary.error.guild.lockedError);
    }

    async checkGuildJoinState(guild: GuildEntity | undefined | null) {
        await this.checkGuildState(guild);
        if (guild?.type !== GuildType.OFFICIAL && guild?.type !== GuildType.PUBLIC) throw new AppError(this.localizer.dictionary.error.guild.permitionError);
    }

    async checkMemberState(member: MemberEntity | undefined | null) {
        if (!member) throw new AppError(this.localizer.dictionary.error.member.notExists);
        if (member.status == MemberStatus.LOCKED || member.status == MemberStatus.BANNED) throw new AppError(this.localizer.dictionary.error.member.lockedError);
        if (member.status == MemberStatus.DELETED) throw new AppError(this.localizer.dictionary.error.member.deletedError);
    }

    async checkRoomMemberState(member: RoomMemberEntity | undefined | null) {
        if (!member) throw new AppError(this.localizer.dictionary.error.member.notExists);
        if (member.status == RoomMemberStatus.LOCKED || member.status == RoomMemberStatus.BANNED) throw new AppError(this.localizer.dictionary.error.member.lockedError);
        if (member.status == RoomMemberStatus.DELETED) throw new AppError(this.localizer.dictionary.error.member.deletedError);
    }

    async checkGuildCategoryState(guildCategory: GuildCategoryEntity | undefined | null) {
        if (!guildCategory) throw new AppError(this.localizer.dictionary.error.category.notExists);
    }

    async checkRoomCategoryState(roomCategory: RoomCategoryEntity | undefined | null) {
        if (!roomCategory) throw new AppError(this.localizer.dictionary.error.category.notExists);
    }

    async checkRoleExist(role: RoleEntity | undefined | null) {
        if (!role) throw new AppError(this.localizer.dictionary.error.role.notExists);
    }

    async checkRoleState(role: RoleEntity | undefined | null) {
        await this.checkRoleExist(role);
    }

    async checkRoomRoleExist(role: RoomRoleEntity | undefined | null) {
        if (!role) throw new AppError(this.localizer.dictionary.error.role.notExists);
    }

    async checkMemberRoomRoleExist(role: MemberRoleEntity | undefined | null) {
        if (!role) throw new AppError(this.localizer.dictionary.error.role.notExists);
    }

    async checkRoomState(room: RoomEntity | undefined | null) {
        if (!room) throw new AppError(this.localizer.dictionary.error.room.notExists);
        if (room.status == RoomStatus.DELETED || room.status == RoomStatus.DELETING) throw new AppError(this.localizer.dictionary.error.room.deletedError);
        if (room.status == RoomStatus.LOCKED || room.status == RoomStatus.BANNED) throw new AppError(this.localizer.dictionary.error.room.lockedError);
    }
    
    async checkRoomJoinState(room: RoomEntity| undefined | null) {
        await this.checkRoomState(room);
        if (room?.type !== RoomType.PUBLIC && room?.type !== RoomType.OFFICIAL) throw new AppError(this.localizer.dictionary.error.room.permitionError);
    }

    async checkMessageState(message: MessageEntity | null | undefined) {
        if (!message) throw new AppError(this.localizer.dictionary.error.message.notExists);
        if (message.status == MessageStatus.ARCHIVED) throw new AppError(this.localizer.dictionary.error.message.archivedError);
        if (message.status == MessageStatus.DELETED) throw new AppError(this.localizer.dictionary.error.message.deletedError);
    }

    async checkMessagePermission(message: MessageEntity, member: MemberEntity) {
        if (member.id != message.senderId) throw new AppError(this.localizer.dictionary.error.role.permissionError);
    }

    async checkPostState(post: PostEntity | null | undefined) {
        if (!post) throw new AppError(this.localizer.dictionary.error.post.notExists);
        if (post.status == PostStatus.ARCHIVED) throw new AppError(this.localizer.dictionary.error.post.archivedError);
        if (post.status == PostStatus.DELETED) throw new AppError(this.localizer.dictionary.error.post.deletedError);
    }

    async checkPostPublic(post: PostEntity | null | undefined) {
        if (!post) throw new AppError(this.localizer.dictionary.error.post.notExists);
        if (post.type !== PostType.PUBLIC) throw new AppError(this.localizer.dictionary.error.post.notExists);
    }

    async checkPostPermission(post: PostEntity, member: MemberEntity) {
        if (member.id != post.senderId) throw new AppError(this.localizer.dictionary.error.role.permissionError);
    }

    async checkFileState(file: FileEntity | null | undefined) {
        if (!file) throw new AppError(this.localizer.dictionary.error.file.notExists);
        if (file.status == FileStatus.LOCKED) throw new AppError(this.localizer.dictionary.error.file.lockedError);
        if (file?.status == FileStatus.FAILED || file.status == FileStatus.UPLOADING) throw new AppError(this.localizer.dictionary.error.file.noUploadError);
        if (file.status == FileStatus.DELETED || file.status == FileStatus.DELETING) throw new AppError(this.localizer.dictionary.error.file.deletedError);
        if (file.status == FileStatus.ARCHIVED) throw new AppError(this.localizer.dictionary.error.file.archivedError);
    }

    async checkFolderState(file: FileEntity | null | undefined) {
        await this.checkFileState(file);
        if (!file?.isFolder) throw new AppError(this.localizer.dictionary.error.folder.notExists);
        const folder = await file?.folder;
        if (!folder) throw new AppError(this.localizer.dictionary.error.folder.notExists);
    }

    async checkUploadingFileState(file: FileEntity | null | undefined) {
        if (!file) throw new AppError(this.localizer.dictionary.error.file.notExists);
        if (file.status == FileStatus.LOCKED) throw new AppError(this.localizer.dictionary.error.file.lockedError);
        if (file.status == FileStatus.DELETED || file.status == FileStatus.DELETING) throw new AppError(this.localizer.dictionary.error.file.deletedError);
        if (file.status == FileStatus.ARCHIVED) throw new AppError(this.localizer.dictionary.error.file.archivedError);
    }

    async checkURLExistState(url: string) {
        const result = await urlExists(url);
        if (result == false) throw new AppError(this.localizer.dictionary.error.file.notExists);
    }

    async checkProfileFileState(file: FileEntity | null | undefined) {
        await this.checkFileState(file);
        if (!file) throw new AppError(this.localizer.dictionary.error.file.notExists);
        if (file.type !== FileType.Image || !isImage(file.contentType) || !isImage(transformFromExtension(file.extension))) throw new AppError(this.localizer.dictionary.error.file.profileTypeError);
    }

    async checkFileOwner(file: FileEntity | FileAttributes, { user }: { user: UserEntity }) {
        if (!file.isSystem && file.holderId != user.id) throw new AppError(this.localizer.dictionary.error.role.permissionError);
    }

    async checkRoomFileState(file: RoomFileEntity | null | undefined) {
        if (!file) throw new AppError(this.localizer.dictionary.error.file.notExists);
    }

    async checkFolderPlaylist(file: FileEntity) {
        if (!isVideo(file.contentType)) throw new AppError(this.localizer.dictionary.error.folder.playlist.fileFormatError);
    }

    async checkMemberIsNotOwner(member: MemberEntity) {
        const guild = await GuildEntity.findOne({
            where: {
                id: member.guildId,
                ownerId: member.userId,
            }
        })
        if (guild) throw new AppError(this.localizer.dictionary.error.member.ownerValidationError);
    }

    async checkRoomMemberIsNotOwner(roomMember: RoomMemberEntity) {
        const room = await RoomEntity.findOne({
            where: {
                id: roomMember.roomId,
                ownerId: roomMember.memberId,
            }
        })
        if (room) throw new AppError(this.localizer.dictionary.error.member.roomOwnerValidationError);
    }

    async checkMemberIsOwner(member: MemberEntity) {
        const guild = await GuildEntity.findOne({
            where: {
                id: member.guildId,
                ownerId: member.userId,
            }
        })
        if (!guild) throw new AppError(this.localizer.dictionary.error.member.notOwnerError);
    }

    async checkRoomMemberIsOwner(roomMember: RoomMemberEntity) {
        const room = await RoomEntity.findOne({
            where: {
                id: roomMember.roomId,
                ownerId: roomMember.memberId,
            }
        })
        if (!room) throw new AppError(this.localizer.dictionary.error.member.notRoomOwnerError);
    }

    async checkMemberManagable(member: MemberEntity) {
        if (!(await this.memberRolesDataStore.includeTypes(member.id, [RoleType.Admin, RoleType.Owner]))) throw new AppError(this.localizer.dictionary.error.role.permissionError);
    }

    async isMemberManagable(member: MemberEntity): Promise<boolean> {
        try {
            await this.checkMemberManagable(member);
            return true;
        } catch {
            return false;
        }
    }

    async checkMemberRoomManagable({ member, room }: { member: MemberEntity, room: RoomEntity }) {
        if (member.id == room.ownerId) return;
        const roomMember = await RoomMemberEntity.findOne({
            where: { 
                roomId: room.id,
                memberId: member.id,
            }
        });
        await this.checkRoomMemberState(roomMember);
        await this.checkMemberManagable(member);
    }

    async checkMemberRoomViewable({ member, room }: { member: MemberEntity, room: RoomEntity }) {
        const roomMember = await RoomMemberEntity.findOne({
            where: { 
                roomId: room.id,
                memberId: member.id,
            }
        });
        await this.checkRoomMemberState(roomMember);
    }

    async isMemberRoomViewable({ member, room }: { member: MemberEntity, room: RoomEntity }): Promise<boolean> {
        try {
            const roomMember = await RoomMemberEntity.findOne({
                where: { 
                    roomId: room.id,
                    memberId: member.id,
                }
            });
            await this.checkRoomMemberState(roomMember);
            return true;
        } catch {
            return false;
        }
    }
    
    async checkFileViewable({ file, user }: { file: FileEntity, user: UserEntity }) {
        if (file.accessType === FileAccessType.PUBLIC) return;
        await this.checkUserState(user);
        const guildFiles = await file.guildFiles;
        const roomFiles = await file.roomFiles;
        if (guildFiles.length > 0) {
            await Promise.all(
                guildFiles.map(guildFile => this.checkGuildFileViewable({ guildFile, user }))
            );
        } else if (roomFiles.length > 0) {
            await Promise.all(
                roomFiles.map(roomFile => this.checkRoomFileViewable({ roomFile, user }))
            );
        }
    }

    async checkFileManagable({ file, user }: { file: FileEntity, user: UserEntity }) {
        await this.checkUserState(user);
        if (file.isSystem) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        const guildFiles = await file.guildFiles;
        const roomFiles = await file.roomFiles;
        if (guildFiles.length > 0) {
            await Promise.all(
                guildFiles.map(guildFile => this.checkGuildFileManagable({ guildFile, user }))
            );
        } else if (roomFiles.length > 0) {
            await Promise.all(
                roomFiles.map(roomFile => this.checkRoomFileManagable({ roomFile, user }))
            );
        }
    }

    async checkGuildFileViewable({ guildFile, user }: { guildFile: GuildFileEntity, user: UserEntity }) {
        await this.checkUserState(user);
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guildFile.guildId,
            },
        });
        await this.checkMemberState(member);
        const file = await guildFile.file;
        const guild = await guildFile.guild;
        await this.checkFileState(file);
        await this.checkGuildState(guild);
        await this.checkMemberState(member!);
    }

    async checkGuildFileManagable({ guildFile, user }: { guildFile: GuildFileEntity, user: UserEntity }) {
        await this.checkUserState(user);
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guildFile.guildId,
            },
        });
        await this.checkMemberState(member);
        const guild = await guildFile.guild;
        await this.checkGuildState(guild);
        await this.checkMemberManagable(member!);
    }

    async checkRoomFileViewable({ roomFile, user }: { roomFile: RoomFileEntity, user: UserEntity }) {
        await this.checkUserState(user);
        const room = await roomFile.room;
        const guild = await room.guild;
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild.id,
            },
        });
        await this.checkMemberState(member);
        if (roomFile.showInGuild) {
            const file = await roomFile.file;
            await this.checkFileState(file);
            await this.checkGuildState(guild);
            await this.checkRoomState(room);
        } else {
            const roomMember = await RoomMemberEntity.findOne({
                where: {
                    roomId: roomFile.roomId,
                    memberId: member?.id,
                },
            });
            await this.checkRoomMemberState(roomMember);
            const file = await roomFile.file;
            await this.checkFileState(file);
            await this.checkGuildState(guild);
            await this.checkRoomState(room);
            await this.checkMemberState(member);
        }
    }

    async checkRoomFileManagable({ roomFile, user }: { roomFile: RoomFileEntity, user: UserEntity }) {
        await this.checkUserState(user);
        const room = await roomFile.room;
        const guild = await room.guild;
        const member = await MemberEntity.findOne({
            where: {
                userId: user.id,
                guildId: guild.id,
            },
        });
        await this.checkMemberState(member);
        if (await this.isMemberManagable(member!)) {
            await this.checkMemberState(member);
        } else {
            const roomMember = await RoomMemberEntity.findOne({
                where: {
                    roomId: roomFile.roomId,
                    memberId: member?.id,
                },
            });
            await this.checkRoomMemberState(roomMember);
            const file = await roomFile.file;
            await this.checkFileState(file);
            await this.checkRoomState(room);
            await this.checkMemberRoomManagable({ room, member: member! });
        }
    }

    async checkFilingFormat({ file, folder }: { file: FileEntity, folder: FileEntity }) {
        await this.checkFileState(file);
        await this.checkFolderState(folder);
        if (folder.type === FileType.Album) {
            const image = await file?.image;
            if (!image) throw new AppError(this.localizer.dictionary.error.folder.album.fileFormatError);
        } else if (folder.type === FileType.Playlist) {
            const video = await file?.video;
            if (!video) throw new AppError(this.localizer.dictionary.error.folder.playlist.fileFormatError);
        } else {
            throw new AppError(this.localizer.dictionary.error.role.permissionError);
        }
    }
}