import { AppError, Localizer, ReportType } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class GuildReportsValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkReportType(type: ReportType) {
        if (type != ReportType.Guild) throw new AppError(this.localizer.dictionary.error.common.internalError);
    }
}

module.exports = GuildReportsValidation;