import BaseValidation from "./BaseValidation";

export default interface ValidationImpl {
}

export type Validation = ValidationImpl & BaseValidation;