import { RoomCategoryEntity } from "@guildion/db";
import { AppError, Localizer, Room } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class RoomsValidation extends BaseValidation implements ValidationImpl {

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkRoomDisplayNameFormat(displayName: string) {
        if (displayName.length < Room.displayNameMinLimit || displayName.length > Room.displayNameMaxLimit) throw new AppError(this.localizer.dictionary.error.room.displayName.invalidCharactersRange);
    }

    async checkRoomCategoryExist({ categoryId, guildId }: { categoryId: string, guildId: string }): Promise<boolean> {
        const exist = RoomCategoryEntity.findOne({
            where: {
                id: categoryId,
                guildId: guildId,
            },
        });
        return !!exist
    }
}

module.exports = RoomsValidation;