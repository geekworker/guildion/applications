import { FileEntity, PostEntity } from "@guildion/db";
import { AppError, FileAttributes, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class PostsValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkPostBody(body: string) {
        if (body.length == 0) throw new AppError(this.localizer.dictionary.error.post.requiredError);
    }

    async checkPostBodyWithAttachments(body: string, attachments?: FileAttributes[] | FileEntity[]) {
        if (!attachments || attachments.length == 0) await this.checkPostBody(body);
    }

    async checkPostExist(id: string | null | undefined) {
        if (!id) return;
        const exist = id ? await PostEntity.findOne({
            where: {
                id,
            },
        }) : undefined;
        if (exist) throw new AppError(this.localizer.dictionary.error.post.alreadyExists);
    }

    async checkParentPostIsSameRoom({ post, parent }: { post: PostEntity,  parent: PostEntity}) {
        if (post.roomId !== parent.roomId) throw new AppError(this.localizer.dictionary.error.post.notExists);
    }
}

module.exports = PostsValidation;