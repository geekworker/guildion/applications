import { RoleEntity } from "@guildion/db";
import { AppError, Localizer, RoleType } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class RolesValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkRoleDeletable(role: RoleEntity) {
        if (role.type !== RoleType.Custom) throw new AppError(this.localizer.dictionary.error.role.notDeletableError);
    }
}

module.exports = RolesValidation;