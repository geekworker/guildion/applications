import { Localizer, Device, Account, AppError, AccountStatus, DeviceStatus } from "@guildion/core";
import { AccountEntity, DeviceEntity } from "@guildion/db";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class AccessTokensValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async beforeCreate({ account, device }: { account: Account | AccountEntity, device: Device | DeviceEntity }) {
        if (!account.id || !device.id || account.status == AccountStatus.NEW || device.status == DeviceStatus.NEW) throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
    }

    async beforeVerify(token: string) {
        if (token.length == 0) throw new AppError(this.localizer.dictionary.error.accessToken.invalidToken);
    }
}

module.exports = AccessTokensValidation;