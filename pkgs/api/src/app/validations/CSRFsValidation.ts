import { DeviceEntity } from "@guildion/db";
import { AppError, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class CSRFsValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    beforeCreate({ device }: { device: DeviceEntity }) {
        if (!device.udid || device.udid == '' || !device.id || device.id == '') throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
    }

    beforeCompare({ device, csrfSecret }: { device: DeviceEntity, csrfSecret: string }) {
        if (!device.udid || device.udid == '' || !device.id || device.id == '' || csrfSecret == '') throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
    }

    beforeInactive({ device }: { device: DeviceEntity }) {
        if (!device.udid || device.udid == '' || !device.id || device.id == '') throw new AppError(this.localizer.dictionary.error.common.invalidRequest);
    }
}

module.exports = CSRFsValidation;