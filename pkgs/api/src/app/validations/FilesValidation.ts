import { FileEntity } from "@guildion/db";
import { AppError, CDN_PROXY_URL_STRING, File, Files, FileAttributes, FileType, Localizer, Provider, safeURL } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class FilesValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkUploadableFileType(type: FileType) {
        if (
            type !== FileType.Image &&
            type !== FileType.Video
        ) throw new Error(this.localizer.dictionary.error.file.uploadableError);
    }

    async checkUploadableFolderType(type: FileType) {
        if (type !== FileType.Folder && type !== FileType.Album && type !== FileType.Playlist) throw new Error(this.localizer.dictionary.error.file.uploadableError);
    }

    async checkFileDisplayNameFormat(displayName: string) {
        if (displayName.length < File.displayNameMinLimit || displayName.length > File.displayNameMaxLimit) throw new AppError(this.localizer.dictionary.error.file.displayName.invalidCharactersRange);
    }

    async checkFileProviderKeyExist(providerKey: string) {
        if (providerKey.length < File.providerKeyMinLimit || providerKey.length > File.providerKeyMaxLimit) throw new AppError(this.localizer.dictionary.error.file.failUploadError);
        const results = await FileEntity.find({
            where: {
                providerKey,
            },
            take: 1,
        });
        if (results.length > 0) throw new AppError(this.localizer.dictionary.error.file.alreadyExists)
    }

    async checkFileURLExist(url: string) {
        if (url.length < File.urlMaxLimit || url.length > File.urlMinLimit) throw new AppError(this.localizer.dictionary.error.file.failUploadError);
        const results = await FileEntity.find({
            where: {
                url,
            },
            take: 1,
        });
        if (results.length > 0) throw new AppError(this.localizer.dictionary.error.file.alreadyExists)
    }

    async checkFileIsCDN(file: FileEntity | FileAttributes) {
        if (
            !safeURL(file!.url) ||
            !(file!.url.startsWith(CDN_PROXY_URL_STRING)) ||
            file.isSystem ||
            file.provider != Provider.S3
        ) throw new AppError(this.localizer.dictionary.error.role.permissionError);
    }
    
    async checkFileDeletable(file: FileEntity) {
        if (file.isSystem) throw new AppError(this.localizer.dictionary.error.role.permissionError);
        if (file.type == FileType.Emoji) throw new AppError(this.localizer.dictionary.error.role.permissionError);
    }

    async checkFileIsNotSystem(file: FileEntity | FileAttributes) {
        const urls = Files.getSeed().toArray().map(seed => seed.url);
        if (urls.includes(file!.url)) throw new AppError(this.localizer.dictionary.error.file.failUploadError);
    }
}

module.exports = FilesValidation;