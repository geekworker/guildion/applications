import { GuildCategoryEntity } from "@guildion/db";
import { Localizer, Guild, AppError } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class GuildsValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkGuildCategoryExist(categoryId: string): Promise<boolean> {
        const exist = GuildCategoryEntity.findOne({
            where: {
                id: categoryId,
            },
        });
        return !!exist
    }

    async checkGuildDisplayNameFormat(displayName: string) {
        if (displayName.length < Guild.displayNameMinLimit || displayName.length > Guild.displayNameMaxLimit) throw new AppError(this.localizer.dictionary.error.guild.displayName.invalidCharactersRange);
    }
}

module.exports = GuildsValidation;