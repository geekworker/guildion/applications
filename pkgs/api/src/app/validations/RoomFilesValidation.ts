import { AppError, File, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class RoomFilesValidation extends BaseValidation implements ValidationImpl {

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkFileDisplayNameFormat(displayName: string) {
        if (displayName.length < File.displayNameMinLimit || displayName.length > File.displayNameMaxLimit) throw new AppError(this.localizer.dictionary.error.file.displayName.invalidCharactersRange);
    }
}

module.exports = RoomFilesValidation;