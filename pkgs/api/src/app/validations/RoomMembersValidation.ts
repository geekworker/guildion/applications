import { RoleEntity, RoomEntity, RoomRoleEntity } from "@guildion/db";
import { Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class RoomMembersValidation extends BaseValidation implements ValidationImpl {

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }
}

module.exports = RoomMembersValidation;