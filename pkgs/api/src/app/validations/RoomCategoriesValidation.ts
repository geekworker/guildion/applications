import { AppError, Localizer, RoomCategory } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class RoomCategoriesValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkRoomCategoryNameFormat(name: string) {
        if (name.length < RoomCategory.nameMinLimit || name.length > RoomCategory.nameMaxLimit) throw new AppError(this.localizer.dictionary.error.roomCategory.name.invalidCharactersRange);
    }
}

module.exports = RoomCategoriesValidation;