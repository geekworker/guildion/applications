import { Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class SyncVisionsValidation extends BaseValidation implements ValidationImpl {

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }
}

module.exports = SyncVisionsValidation;