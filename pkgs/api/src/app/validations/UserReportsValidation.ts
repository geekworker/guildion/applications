import { AppError, Localizer, ReportType } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class UserReportsValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkReportType(type: ReportType) {
        if (type != ReportType.User) throw new AppError(this.localizer.dictionary.error.common.internalError);
    }
}

module.exports = UserReportsValidation;