import { AppError, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class ReportsValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkReportBody(body: string) {
        if (body.length == 0) throw new AppError(this.localizer.dictionary.error.common.internalError);
    }
}

module.exports = ReportsValidation;