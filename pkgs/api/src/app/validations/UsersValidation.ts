import { GuildEntity, UserEntity } from "@guildion/db";
import { AppError, Localizer, User } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class UsersValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkUserGuildOwners(user: UserEntity) {
        const guilds = await GuildEntity.find({
            where: {
                ownerId: user.id,
            },
        });
        if (guilds.length > 0) throw new AppError(this.localizer.dictionary.error.user.ownerNotDeletableError);
    }

    async checkUserDisplayNameFormat(displayName: string) {
        if (displayName.length < User.displayNameMinLimit || displayName.length > User.displayNameMaxLimit) throw new AppError(this.localizer.dictionary.error.user.displayName.invalidCharactersRange);
    }
}

module.exports = UsersValidation;