import { InviteEntity } from "@guildion/db";
import { AppError, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class InvitesValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkInviteValid(invite: InviteEntity | null | undefined) {
        if (!invite) throw new AppError(this.localizer.dictionary.error.invite.notExists);
        if (invite.expiredAt && invite.expiredAt < new Date()) throw new AppError(this.localizer.dictionary.error.invite.expiredError);
        if (invite.maxMembersCount && invite.maxMembersCount <= invite.currentMembersCount) throw new Error(
            !!invite.roomId ?
                this.localizer.dictionary.error.invite.limitRoomError :
                this.localizer.dictionary.error.invite.limitGuildError
        );
    }
}

module.exports = InvitesValidation;