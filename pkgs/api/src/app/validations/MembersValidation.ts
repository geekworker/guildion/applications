import { GuildBlockEntity, GuildEntity, UserEntity } from "@guildion/db";
import { AppError, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class MembersValidation extends BaseValidation implements ValidationImpl {
    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkGuildBlockState({ guild, user }: { guild: GuildEntity, user: UserEntity }) {
        const block = await GuildBlockEntity.findOne({
            where: {
                guildId: guild.id,
                receiverId: user.id,
            }
        });
        if (block) throw new AppError(this.localizer.dictionary.error.member.bannedError);
    }
}

module.exports = MembersValidation;