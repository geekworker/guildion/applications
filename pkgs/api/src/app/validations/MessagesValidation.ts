import { FileEntity, MessageEntity } from "@guildion/db";
import { AppError, FileAttributes, Localizer } from "@guildion/core";
import BaseValidation from "./BaseValidation";
import ValidationImpl from "./ValidationImpl";

export default class MessagesValidation extends BaseValidation implements ValidationImpl {

    constructor({ localizer }: { localizer: Localizer }) {
        super({ localizer });
    }

    async checkMessageBody(body: string) {
        if (body.length == 0) throw new AppError(this.localizer.dictionary.error.message.requiredError);
    }

    async checkMessageExist(id: string | null | undefined) {
        if (!id) return;
        const exist = id ? await MessageEntity.findOne({
            where: {
                id,
            },
        }) : undefined;
        if (exist) throw new AppError(this.localizer.dictionary.error.message.alreadyExists);
    }


    async checkMessageBodyWithAttachments(body: string, attachments?: FileAttributes[] | FileEntity[]) {
        if (!attachments || attachments.length == 0) await this.checkMessageBody(body);
    }

    async checkParentMessageIsSameRoom({ message, parent }: { message: MessageEntity,  parent: MessageEntity}) {
        if (message.roomId !== parent.roomId) throw new AppError(this.localizer.dictionary.error.message.notExists);
    }
}

module.exports = MessagesValidation;