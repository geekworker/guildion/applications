import React from 'react';

export type LayoutProps = {
    className?: string,
    style?: React.CSSProperties,
};